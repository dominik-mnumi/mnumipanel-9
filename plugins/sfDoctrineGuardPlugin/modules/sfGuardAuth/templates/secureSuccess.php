<?php use_helper('I18N') ?>

<h2><?php echo __('Oops! The page you asked for is protected and you do not have permission to access it.', null, 'sf_guard') ?></h2>

<p><?php echo sfContext::getInstance()->getRequest()->getUri() ?></p>

<h3><?php echo __('Log in below to gain access', null, 'sf_guard') ?></h3>

<?php echo get_component('sfGuardAuth', 'signin_form') ?>