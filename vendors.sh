#!/bin/sh

DIR=`php -r "echo dirname(realpath('$0'));"`
VENDOR="$DIR/lib/vendor"

# initialization
if [ "$1" = "--reinstall" -o "$2" = "--reinstall" ]; then
    rm -rf $VENDOR
fi

mkdir -p "$VENDOR" && cd "$VENDOR"

if [ ! -d $VENDOR/symfony ]
then
  echo "> Installing/Updating Symfony (compatible with PHP 5.5)"
  wget -nv https://github.com/jupeter/symfony1/archive/v1.4.20+v1.zip
  unzip -q v1.4.20+v1.zip
  mv symfony1-1.4.20-v1/ $VENDOR/symfony
  rm -rf v1.4.20+v1.zip
fi

if [ ! -d $VENDOR/Zend ]
then
  echo "> Installing/Updating Zend framework" 
  wget -nv http://security.mnumi.com/mirror/ZendFramework-1.12.0.tar.gz
  tar xzf ZendFramework-1.12.0.tar.gz
  mv $VENDOR/ZendFramework-1.12.0/library/Zend $VENDOR/Zend
  rm -rf $VENDOR/ZendFramework-1.12.0
  rm -rf $VENDOR/ZendFramework-1.12.0.tar.gz
fi

if [ ! -d $VENDOR/mysql-workbench-schema-exporter ]
then
  echo "> Installing/Updating MySQL workbench schema exporter"
  git clone https://github.com/johmue/mysql-workbench-schema-exporter.git
fi

echo "> Installing/Updating GIT submodules"
cd $DIR
git submodule update --init --quiet


## Download sahi
if [ ! -d lib/vendor/sahi ]; 
then
    echo " > Installing SAHI"
    wget "http://heanet.dl.sourceforge.net/project/sahi/sahi-v35/20110719/sahi_20110719.zip"
    mv sahi_20110719.zip lib/vendor/
    cd lib/vendor/
    unzip -q sahi_20110719.zip
    rm sahi_20110719.zip
    cd ../../
fi
echo "Installation completed."

