#!/bin/bash

if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

CURRENT_USER=$1 

if [ "$CURRENT_USER" = '' ]
then
   echo "Put correct username. "
   echo "Usage: ./inital-direcoties.sh [username]" 1>&2
   exit 1
fi

echo "Set permissions for: cache logs. user: $CURRENT_USER"
sudo setfacl -R -m u:www-data:rwx -m u:$CURRENT_USER:rwx ../log ../cache
sudo setfacl -dR -m u:www-data:rwx -m u:$CURRENT_USER:rwx ../log ../cache

