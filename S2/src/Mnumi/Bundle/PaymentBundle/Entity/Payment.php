<?php

namespace Mnumi\Bundle\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * Payment
 *
 * @ORM\Table(name="payment")
 * @ORM\Entity
 */
class Payment
{
    /**
     * Unique autoincrement ID
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"public"})
     */
    private $id;

    /**
     * Payment name
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     * @Groups({"public"})
     */
    private $name;

    /**
     * Payment label for customers
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     * @Groups({"public"})
     */
    private $label;

    /**
     * Payment description
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"public"})
     */
    private $description;

    /**
     * Is active?
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @Groups({"public"})
     */
    private $active = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="for_office", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $forOffice = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="apply_on_all_pricelist", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $applyOnAllPricelist = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="changeable", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $changeable = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="apply_on_all_carriers", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $applyOnAllCarriers = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required_credit_limit", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $requiredCreditLimit = false;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=2, nullable=true)
     * @Serializer\Exclude
     */
    private $cost;

    /**
     * @var float
     *
     * @ORM\Column(name="free_payment_amount", type="float", precision=18, scale=2, nullable=true)
     * @Serializer\Exclude
     */
    private $freePaymentAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="income_pattern", type="string", length=100, nullable=true)
     * @Serializer\Exclude
     */
    private $incomePattern;

    /**
     * @var string
     *
     * @ORM\Column(name="outcome_pattern", type="string", length=100, nullable=true)
     * @Serializer\Exclude
     */
    private $outcomePattern;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deposit_enabled", type="boolean", nullable=false)
     * @Serializer\Exclude
     */
    private $depositEnabled = false;

    public function __toString()
    {
        return $this->getId() . '=>' . $this->getName();
    }

    /**
     * Set name
     *
     * @param  string  $name
     * @return Payment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param  string  $label
     * @return Payment
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set description
     *
     * @param  string  $description
     * @return Payment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param  boolean $active
     * @return Payment
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set forOffice
     *
     * @param  boolean $forOffice
     * @return Payment
     */
    public function setForOffice($forOffice)
    {
        $this->forOffice = $forOffice;

        return $this;
    }

    /**
     * Get forOffice
     *
     * @return boolean
     */
    public function getForOffice()
    {
        return $this->forOffice;
    }

    /**
     * Set applyOnAllPricelist
     *
     * @param  boolean $applyOnAllPricelist
     * @return Payment
     */
    public function setApplyOnAllPricelist($applyOnAllPricelist)
    {
        $this->applyOnAllPricelist = $applyOnAllPricelist;

        return $this;
    }

    /**
     * Get applyOnAllPricelist
     *
     * @return boolean
     */
    public function getApplyOnAllPricelist()
    {
        return $this->applyOnAllPricelist;
    }

    /**
     * Set changeable
     *
     * @param  boolean $changeable
     * @return Payment
     */
    public function setChangeable($changeable)
    {
        $this->changeable = $changeable;

        return $this;
    }

    /**
     * Get changeable
     *
     * @return boolean
     */
    public function getChangeable()
    {
        return $this->changeable;
    }

    /**
     * Set applyOnAllCarriers
     *
     * @param  boolean $applyOnAllCarriers
     * @return Payment
     */
    public function setApplyOnAllCarriers($applyOnAllCarriers)
    {
        $this->applyOnAllCarriers = $applyOnAllCarriers;

        return $this;
    }

    /**
     * Get applyOnAllCarriers
     *
     * @return boolean
     */
    public function getApplyOnAllCarriers()
    {
        return $this->applyOnAllCarriers;
    }

    /**
     * Set requiredCreditLimit
     *
     * @param  boolean $requiredCreditLimit
     * @return Payment
     */
    public function setRequiredCreditLimit($requiredCreditLimit)
    {
        $this->requiredCreditLimit = $requiredCreditLimit;

        return $this;
    }

    /**
     * Get requiredCreditLimit
     *
     * @return boolean
     */
    public function getRequiredCreditLimit()
    {
        return $this->requiredCreditLimit;
    }

    /**
     * Set cost
     *
     * @param  float   $cost
     * @return Payment
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set freePaymentAmount
     *
     * @param  float   $freePaymentAmount
     * @return Payment
     */
    public function setFreePaymentAmount($freePaymentAmount)
    {
        $this->freePaymentAmount = $freePaymentAmount;

        return $this;
    }

    /**
     * Get freePaymentAmount
     *
     * @return float
     */
    public function getFreePaymentAmount()
    {
        return $this->freePaymentAmount;
    }

    /**
     * Set incomePattern
     *
     * @param  string  $incomePattern
     * @return Payment
     */
    public function setIncomePattern($incomePattern)
    {
        $this->incomePattern = $incomePattern;

        return $this;
    }

    /**
     * Get incomePattern
     *
     * @return string
     */
    public function getIncomePattern()
    {
        return $this->incomePattern;
    }

    /**
     * Set outcomePattern
     *
     * @param  string  $outcomePattern
     * @return Payment
     */
    public function setOutcomePattern($outcomePattern)
    {
        $this->outcomePattern = $outcomePattern;

        return $this;
    }

    /**
     * Get outcomePattern
     *
     * @return string
     */
    public function getOutcomePattern()
    {
        return $this->outcomePattern;
    }

    /**
     * Set depositEnabled
     *
     * @param  boolean $depositEnabled
     * @return Payment
     */
    public function setDepositEnabled($depositEnabled)
    {
        $this->depositEnabled = $depositEnabled;

        return $this;
    }

    /**
     * Get depositEnabled
     *
     * @return boolean
     */
    public function getDepositEnabled()
    {
        return $this->depositEnabled;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
