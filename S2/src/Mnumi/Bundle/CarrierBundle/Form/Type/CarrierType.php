<?php

namespace Mnumi\Bundle\CarrierBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CarrierType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                array('description' => 'Carrier name'
                )
            )
            ->add(
                'label',
                'text',
                array(
                    'required' => false, 
                    'description' => 'Carrier label - this is visible only for customers'
                )
            )
            ->add(
                'deliveryTime',
                'text',
                array(
                    'description' => 'Delivery time (eg. 7 days)'
                )
            )
            ->add(
                'cost',
                'text',
                array(
                    'description' => 'delivery cost'
                )
            )

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mnumi\Bundle\CarrierBundle\Entity\Carrier'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'carrier';
    }

}
