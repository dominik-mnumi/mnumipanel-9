<?php

namespace Mnumi\Bundle\CarrierBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * Carrier
 *
 * @ORM\Table(name="carrier",uniqueConstraints={@ORM\UniqueConstraint(columns={"name"})}, indexes={@ORM\Index(columns={"carrier_report_type_name"})})
 * @ORM\Entity
 */
class Carrier
{
    /**
     * Unique autoincrement ID
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"public"})
     */
    private $id;

    /**
     * Carrier name
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"public"})
     */
    private $name;

    /**
     * Carrier label for customers
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_time", type="string", length=255, nullable=false)
     * @Serializer\Exclude
     */
    private $deliveryTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $active = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="apply_on_all_pricelist", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $applyOnAllPricelist = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="tax", type="integer", nullable=true)
     * @Serializer\Exclude
     */
    private $tax = 23;

    /**
     * @var string
     *
     * @ORM\Column(name="restrict_for_cities", type="string", length=255, nullable=true)
     * @Serializer\Exclude
     */
    private $restrictForCities;

    /**
     * @var boolean
     *
     * @ORM\Column(name="require_shipment_data", type="boolean", nullable=false)
     * @Serializer\Exclude
     */
    private $requireShipmentData = '1';

    /**
     * The additional carrier cost
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=2, nullable=true)
     * @Groups({"public"})
     */
    private $cost;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=18, scale=2, nullable=true)
     * @Serializer\Exclude
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="free_shipping_amount", type="float", precision=18, scale=2, nullable=true)
     * @Serializer\Exclude
     */
    private $freeShippingAmount;

    /**
     * @var \CarrierReportType
     *
     * @ORM\ManyToOne(targetEntity="CarrierReportType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="carrier_report_type_name", referencedColumnName="name")
     * })
     */
    private $carrierReportTypeName;

    public function __toString()
    {
        return $this->getId() . '=>' . $this->getName();
    }

    /**
     * Set name
     *
     * @param  string  $name
     * @return Carrier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param  string  $label
     * @return Carrier
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set deliveryTime
     *
     * @param  string  $deliveryTime
     * @return Carrier
     */
    public function setDeliveryTime($deliveryTime)
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    /**
     * Get deliveryTime
     *
     * @return string
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * Set active
     *
     * @param  boolean $active
     * @return Carrier
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set applyOnAllPricelist
     *
     * @param  boolean $applyOnAllPricelist
     * @return Carrier
     */
    public function setApplyOnAllPricelist($applyOnAllPricelist)
    {
        $this->applyOnAllPricelist = $applyOnAllPricelist;

        return $this;
    }

    /**
     * Get applyOnAllPricelist
     *
     * @return boolean
     */
    public function getApplyOnAllPricelist()
    {
        return $this->applyOnAllPricelist;
    }

    /**
     * Set tax
     *
     * @param  integer $tax
     * @return Carrier
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return integer
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set restrictForCities
     *
     * @param  string  $restrictForCities
     * @return Carrier
     */
    public function setRestrictForCities($restrictForCities)
    {
        $this->restrictForCities = $restrictForCities;

        return $this;
    }

    /**
     * Get restrictForCities
     *
     * @return string
     */
    public function getRestrictForCities()
    {
        return $this->restrictForCities;
    }

    /**
     * Set requireShipmentData
     *
     * @param  boolean $requireShipmentData
     * @return Carrier
     */
    public function setRequireShipmentData($requireShipmentData)
    {
        $this->requireShipmentData = $requireShipmentData;

        return $this;
    }

    /**
     * Get requireShipmentData
     *
     * @return boolean
     */
    public function getRequireShipmentData()
    {
        return $this->requireShipmentData;
    }

    /**
     * Set cost
     *
     * @param  float   $cost
     * @return Carrier
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set price
     *
     * @param  float   $price
     * @return Carrier
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set freeShippingAmount
     *
     * @param  float   $freeShippingAmount
     * @return Carrier
     */
    public function setFreeShippingAmount($freeShippingAmount)
    {
        $this->freeShippingAmount = $freeShippingAmount;

        return $this;
    }

    /**
     * Get freeShippingAmount
     *
     * @return float
     */
    public function getFreeShippingAmount()
    {
        return $this->freeShippingAmount;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
