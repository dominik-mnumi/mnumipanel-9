<?php

namespace Mnumi\Bundle\CarrierBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarrierReportType
 *
 * @ORM\Table(name="carrier_report_type")
 * @ORM\Entity
 */
class CarrierReportType
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * Set description
     *
     * @param  string            $description
     * @return CarrierReportType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
