<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ProductWizard
 *
 * @ORM\Table(name="product_wizard", uniqueConstraints={@ORM\UniqueConstraint(columns={"product_id", "wizard_name"})}, indexes={@ORM\Index(columns={"product_id"})})
 * @ORM\Entity
 */
class ProductWizard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="wizard_name", type="string", length=255, nullable=false)
     */
    private $wizardName;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float", precision=18, scale=2, nullable=true)
     */
    private $height;

    /**
     * @var float
     *
     * @ORM\Column(name="width", type="float", precision=18, scale=2, nullable=true)
     */
    private $width;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude
     */
    private $product;

    /**
     * Set wizardName
     *
     * @param  string        $wizardName
     * @return ProductWizard
     */
    public function setWizardName($wizardName)
    {
        $this->wizardName = $wizardName;

        return $this;
    }

    /**
     * Get wizardName
     *
     * @return string
     */
    public function getWizardName()
    {
        return $this->wizardName;
    }

    /**
     * Set height
     *
     * @param  float         $height
     * @return ProductWizard
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set width
     *
     * @param  float         $width
     * @return ProductWizard
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set sortOrder
     *
     * @param  integer       $sortOrder
     * @return ProductWizard
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set active
     *
     * @param  boolean       $active
     * @return ProductWizard
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\Product $product
     * @return ProductWizard
     */
    public function setProduct(\Mnumi\Bundle\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
