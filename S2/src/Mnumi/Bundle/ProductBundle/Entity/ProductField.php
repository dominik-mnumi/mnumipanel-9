<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * ProductField
 *
 * @ORM\Table(name="product_field", indexes={@ORM\Index(columns={"fieldset_id"}), @ORM\Index(columns={"product_id"})})
 * @ORM\Entity
 */
class ProductField
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"ProductList"})
     *
     */
    private $id;

    /**
     * @var Fieldset
     *
     * @ORM\OneToOne(targetEntity="\Mnumi\Bundle\ProductBundle\Entity\Fieldset")
     * @ORM\JoinColumn(name="fieldset_id", referencedColumnName="id")
     * @Groups({"ProductList"})
     *
     */
    private $fieldset;

    /**
     * @var ProductFieldItem
     *
     * @ORM\OneToMany(targetEntity="\Mnumi\Bundle\ProductBundle\Entity\ProductFieldItem", mappedBy="productField")
     * @ORM\JoinColumn(name="product_field_id", referencedColumnName="id")
     * @Groups({"ProductList"})
     */
    private $fieldItems;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="fields")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @Serializer\Exclude
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Groups({"ProductList"})
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     * @Groups({"ProductList"})
     */
    private $visible = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="default_value", type="string", length=255, nullable=true)
     * @Groups({"ProductList"})
     */
    private $defaultValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean", nullable=false)
     * @Groups({"ProductList"})
     */
    private $required = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="package_order", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $packageOrder;

    public function __toString()
    {
        return $this->fieldset->getName();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fieldItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param  string       $label
     * @return ProductField
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set visible
     *
     * @param  boolean      $visible
     * @return ProductField
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set defaultValue
     *
     * @param  string       $defaultValue
     * @return ProductField
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set required
     *
     * @param  boolean      $required
     * @return ProductField
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set packageOrder
     *
     * @param  boolean      $packageOrder
     * @return ProductField
     */
    public function setPackageOrder($packageOrder)
    {
        $this->packageOrder = $packageOrder;

        return $this;
    }

    /**
     * Get packageOrder
     *
     * @return boolean
     */
    public function getPackageOrder()
    {
        return $this->packageOrder;
    }

    /**
     * Set fieldset
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\Fieldset $fieldset
     * @return ProductField
     */
    public function setFieldset(\Mnumi\Bundle\ProductBundle\Entity\Fieldset $fieldset = null)
    {
        $this->fieldset = $fieldset;

        return $this;
    }

    /**
     * Get fieldset
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\Fieldset
     */
    public function getFieldset()
    {
        return $this->fieldset;
    }

    /**
     * Add fieldItems
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\ProductFieldItem $fieldItems
     * @return ProductField
     */
    public function addFieldItem(\Mnumi\Bundle\ProductBundle\Entity\ProductFieldItem $fieldItems)
    {
        $this->fieldItems[] = $fieldItems;

        return $this;
    }

    /**
     * Remove fieldItems
     *
     * @param \Mnumi\Bundle\ProductBundle\Entity\ProductFieldItem $fieldItems
     */
    public function removeFieldItem(\Mnumi\Bundle\ProductBundle\Entity\ProductFieldItem $fieldItems)
    {
        $this->fieldItems->removeElement($fieldItems);
    }

    /**
     * Get fieldItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFieldItems()
    {
        return $this->fieldItems;
    }

    /**
     * Set product
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\Product $product
     * @return ProductField
     */
    public function setProduct(\Mnumi\Bundle\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Check is product has enabled calculation as Card
     * and order have attribute Side as Double
     *
     * @throws Exception
     * @return bool
     */
    public function isCalculateAsCardEnabled()
    {
        if ($this->getFieldset()->getName() != "COUNT") {
            return false;
        }

        if ($this->getProduct()->getCalculateAsCard() == false) {
            return false;
        }

        return true;
    }
}
