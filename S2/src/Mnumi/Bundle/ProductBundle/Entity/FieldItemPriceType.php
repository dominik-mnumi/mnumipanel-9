<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FieldItemPriceType
 *
 * @ORM\Table(name="field_item_price_type")
 * @ORM\Entity
 */
class FieldItemPriceType
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $name = '';

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
