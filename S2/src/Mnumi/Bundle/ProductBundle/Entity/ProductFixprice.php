<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductFixprice
 *
 * @ORM\Table(name="product_fixprice", indexes={@ORM\Index(columns={"product_id"}), @ORM\Index(columns={"pricelist_id"})})
 * @ORM\Entity
 */
class ProductFixprice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=18, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=2, nullable=true)
     */
    private $cost;

    /**
     * @var \Pricelist
     *
     * @ORM\OneToOne(targetEntity="\Mnumi\Bundle\CalculationBundle\Entity\PriceList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pricelist_id", referencedColumnName="id")
     * })
     */
    private $pricelist;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * Set quantity
     *
     * @param  integer         $quantity
     * @return ProductFixprice
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param  float           $price
     * @return ProductFixprice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set cost
     *
     * @param  float           $cost
     * @return ProductFixprice
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\Product $product
     * @return ProductFixprice
     */
    public function setProduct(\Mnumi\Bundle\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set pricelist
     *
     * @param  \Mnumi\Bundle\CalculationBundle\Entity\PriceList $pricelist
     * @return ProductFixprice
     */
    public function setPricelist(\Mnumi\Bundle\CalculationBundle\Entity\PriceList $pricelist = null)
    {
        $this->pricelist = $pricelist;

        return $this;
    }

    /**
     * Get pricelist
     *
     * @return \Mnumi\Bundle\CalculationBundle\Entity\PriceList
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }
}
