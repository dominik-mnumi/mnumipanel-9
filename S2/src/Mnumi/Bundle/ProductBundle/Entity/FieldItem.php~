<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * FieldItem
 *
 * @ORM\Table(name="field_item", indexes={@ORM\Index(columns={"measure_unit_id"}), @ORM\Index(columns={"field_id"})})
 * @ORM\Entity
 */
class FieldItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=3, nullable=true)
     * @Serializer\Exclude
     */
    private $cost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     * @Serializer\Exclude
     */
    private $hidden = false;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Serializer\Exclude
     */
    private $label;

    /**
     * @var \Fieldset
     *
     * @ORM\ManyToOne(targetEntity="Fieldset")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;

    /**
     * @var \MeasureUnit
     *
     * @ORM\OneToOne(targetEntity="MeasureUnit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="measure_unit_id", referencedColumnName="id")
     * })
     */
    private $measureUnit;
    
    /**
     * @var FieldItemSize
     *
     * @ORM\OneToMany(targetEntity="FieldItemSize", mappedBy="fieldItem", fetch="EAGER")
     * @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     */
    private $size;
    
    /**
     * @var FieldItemPrice
     *
     * @ORM\OneToMany(targetEntity="FieldItemPrice", mappedBy="fieldItem", fetch="EAGER")
     * @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     */
    private $price;
    
    /**
     * @var FieldItemMaterial
     *
     * @ORM\OneToMany(targetEntity="FieldItemMaterial", mappedBy="fieldItem", fetch="EAGER")
     * @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     */
    private $material;
    
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FieldItem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return FieldItem
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float 
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set hidden
     *
     * @param boolean $hidden
     * @return FieldItem
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean 
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return FieldItem
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set measureUnit
     *
     * @param integer $measureUnit
     * @return FieldItem
     */
    public function setMeasureUnit(\Mnumi\Bundle\ProductBundle\Entity\MeasureUnit $measureUnit)
    {
        $this->measureUnit = $measureUnit;

        return $this;
    }

    /**
     * Get measureUnit
     *
     * @return integer 
     */
    public function getMeasureUnit()
    {
        if($this->measureUnit)
        {
            return $this->measureUnit;
        }
        return new MeasureUnit();
    }
}
