<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ProductField
 *
 * @ORM\Table(name="product_field", indexes={@ORM\Index(columns={"fieldset_id"}), @ORM\Index(columns={"product_id"})})
 * @ORM\Entity
 */
class ProductField
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     */
    private $id;

    /**
     * @var Fieldset
     *
     * @ORM\OneToOne(targetEntity="Fieldset")
     * @ORM\JoinColumn(name="fieldset_id", referencedColumnName="id")
     * @Serializer\Exclude
     */
    private $fieldset;
    
    /**
     * @var ProductFieldItem
     *
     * @ORM\OneToMany(targetEntity="ProductFieldItem", mappedBy="productField")
     * @ORM\JoinColumn(name="product_field_id", referencedColumnName="id")
     * @Serializer\Exclude
     */
    private $fieldItems;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="fields")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @Serializer\Exclude
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Serializer\Exclude
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     * @Serializer\Exclude
     */
    private $visible = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="default_value", type="string", length=255, nullable=true)
     * @Serializer\Exclude
     */
    private $defaultValue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean", nullable=false)
     * @Serializer\Exclude
     */
    private $required = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="package_order", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $packageOrder;
    
    public function __toString()
    {
        return $this->id . '=>' . $this->fieldset->getName();
    }
    
}
