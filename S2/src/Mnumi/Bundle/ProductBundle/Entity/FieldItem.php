<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * FieldItem
 *
 * @ORM\Table(name="field_item", indexes={@ORM\Index(columns={"measure_unit_id"}), @ORM\Index(columns={"field_id"})})
 * @ORM\Entity
 */
class FieldItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     * @Groups({"ProductList"})
     */
    private $name;

    /**
     * @var float
     * @deprecated since PAN-1410
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=3, nullable=true)
     * @Serializer\Exclude
     */
    private $cost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     * @Groups({"ProductList"})
     */
    private $hidden = false;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Groups({"ProductList"})
     */
    private $label;

    /**
     * @var \Fieldset
     *
     * @ORM\ManyToOne(targetEntity="Fieldset")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;

    /**
     * @var \MeasureUnit
     *
     * @ORM\OneToOne(targetEntity="MeasureUnit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="measure_unit_id", referencedColumnName="id")
     * })
     */
    private $measureUnit;

    /**
     * @var FieldItemSize
     *
     * @ORM\OneToMany(targetEntity="FieldItemSize", mappedBy="fieldItem", fetch="EAGER")
     * @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     * @Groups({"ProductList"})
     */
    private $size;

    /**
     * @var FieldItemPrice
     *
     * @ORM\OneToMany(targetEntity="FieldItemPrice", mappedBy="fieldItem", fetch="EAGER")
     * @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     * @Groups({"ProductList"})
     */
    private $price;

    /**
     * @var FieldItemMaterial
     *
     * @ORM\OneToMany(targetEntity="FieldItemMaterial", mappedBy="fieldItem", fetch="EAGER")
     * @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     * @Groups({"ProductList"})
     */
    private $material;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param  string    $name
     * @return FieldItem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cost
     *
     * @param  float     $cost
     * @return FieldItem
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set hidden
     *
     * @param  boolean   $hidden
     * @return FieldItem
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set label
     *
     * @param  string    $label
     * @return FieldItem
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set measureUnit
     *
     * @param  integer   $measureUnit
     * @return FieldItem
     */
    public function setMeasureUnit(\Mnumi\Bundle\ProductBundle\Entity\MeasureUnit $measureUnit)
    {
        $this->measureUnit = $measureUnit;

        return $this;
    }

    /**
     * Get measureUnit
     *
     * @return integer
     */
    public function getMeasureUnit()
    {
        if ($this->measureUnit) {
            return $this->measureUnit;
        }

        return new MeasureUnit();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->size = new \Doctrine\Common\Collections\ArrayCollection();
        $this->price = new \Doctrine\Common\Collections\ArrayCollection();
        $this->material = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set field
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\Fieldset $field
     * @return FieldItem
     */
    public function setField(\Mnumi\Bundle\ProductBundle\Entity\Fieldset $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\Fieldset
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Add size
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItemSize $size
     * @return FieldItem
     */
    public function addSize(\Mnumi\Bundle\ProductBundle\Entity\FieldItemSize $size)
    {
        $this->size[] = $size;

        return $this;
    }

    /**
     * Remove size
     *
     * @param \Mnumi\Bundle\ProductBundle\Entity\FieldItemSize $size
     */
    public function removeSize(\Mnumi\Bundle\ProductBundle\Entity\FieldItemSize $size)
    {
        $this->size->removeElement($size);
    }

    /**
     * Get size
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("options")
     *
     * @return array
     */
    public function getOptions()
    {
        if ($this->getSize()->isEmpty() && $this->getName() == "- Customizable -") {
            return array(
                'customizable' => true
            );
        }
    }

    /**
     * Add price
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItemPrice $price
     * @return FieldItem
     */
    public function addPrice(\Mnumi\Bundle\ProductBundle\Entity\FieldItemPrice $price)
    {
        $this->price[] = $price;

        return $this;
    }

    /**
     * Remove price
     *
     * @param \Mnumi\Bundle\ProductBundle\Entity\FieldItemPrice $price
     */
    public function removePrice(\Mnumi\Bundle\ProductBundle\Entity\FieldItemPrice $price)
    {
        $this->price->removeElement($price);
    }

    /**
     * Get price
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add material
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItemMaterial $material
     * @return FieldItem
     */
    public function addMaterial(\Mnumi\Bundle\ProductBundle\Entity\FieldItemMaterial $material)
    {
        $this->material[] = $material;

        return $this;
    }

    /**
     * Remove material
     *
     * @param \Mnumi\Bundle\ProductBundle\Entity\FieldItemMaterial $material
     */
    public function removeMaterial(\Mnumi\Bundle\ProductBundle\Entity\FieldItemMaterial $material)
    {
        $this->material->removeElement($material);
    }

    /**
     * Get material
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterial()
    {
        return $this->material;
    }
}
