<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * Product class
 * use Doctrine\ORM\Mapping as ORM;
 *
 * @ORM\Entity
 * @ORM\Table(name="product")
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 *
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"ProductList"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Product name should not be blank")
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"ProductList"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"ProductList"})
     */
    private $description;

    /**
     * @var Category
     *
     * @ORM\OneToOne(targetEntity="\Mnumi\Bundle\RestServerBundle\Entity\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * @Groups({"ProductList"})
     *
     */
    private $category;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     * @Assert\Type("boolean")
     */
    private $active = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="external_link", type="string", length=255, nullable=true)
     */
    private $externalLink;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     * @Groups({"ProductList"})
     */
    private $photoFilename;

    /**
     * @var boolean
     *
     * @ORM\Column(name="uploader_available", type="boolean")
     */
    private $uploaderAvailable = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="file_required", type="boolean")
     */
    private $file_required = 0;

    /**
     * @var datetime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created;

    /**
     * @var datetime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated;

    /**
     * @var Workflow
     *
     * @ORM\OneToOne(targetEntity="\Mnumi\Bundle\RestServerBundle\Entity\Workflow")
     * @ORM\JoinColumn(name="workflow_id", referencedColumnName="id")
     *
     */
    private $workflow;

    /**
     * @var Field
     *
     * @ORM\OneToMany(targetEntity="\Mnumi\Bundle\ProductBundle\Entity\ProductField", mappedBy="product", fetch="EAGER")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @Groups({"ProductList"})
     *
     */
    private $fields;

    /**
     * @var Field
     *
     * @ORM\OneToMany(targetEntity="ProductFixprice", mappedBy="product", fetch="EAGER")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @Groups({"ProductList"})
     */
    private $fixprice;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     * @Groups({"ProductList"})
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="static_quantity", type="string", length=255, nullable=true)
     * @Groups({"ProductList"})
     */
    private $staticQuantity;

    /**
     * @var boolean
     *
     * @ORM\Column(name="simple_calculation", type="boolean", nullable=false)
     * @Groups({"ProductList"})
     */
    private $simpleCalculation = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_static_quantity", type="boolean", nullable=false)
     * @Groups({"ProductList"})
     */
    private $isStaticQuantity = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="calculate_as_card", type="boolean", nullable=false)
     * @Groups({"ProductList"})
     */
    private $calculateAsCard = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="tax_value", type="integer", nullable=false)
     * @Groups({"ProductList"})
     */
    private $tax_value;

    public function __toString()
    {
        return $this->getId() . '=>' . $this->getName();
    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return \Mnumi\Bundle\RestServerBundle\Entity\Workflow
     */
    public function getWorkflow()
    {
        return $this->workflow;
    }

    /**
     * @return \Mnumi\Bundle\RestServerBundle\Entity\datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return \Mnumi\Bundle\RestServerBundle\Entity\datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \Mnumi\Bundle\RestServerBundle\Entity\datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getExternalLink()
    {
        return $this->externalLink;
    }

    /**
     * @param string $externalLink
     */
    public function setExternalLink($externalLink)
    {
        $this->externalLink = $externalLink;
    }

    /**
     * @return string
     */
    public function getPhotoFilename()
    {
        return $this->photoFilename;
    }

    /**
     * @param string $photoFilename
     */
    public function setPhotoFilename($photoFilename)
    {
        $this->photoFilename = $photoFilename;
    }

    /**
     * @return boolean
     */
    public function isUploaderAvailable()
    {
        return $this->uploaderAvailable;
    }

    /**
     * @param boolean $uploaderAvailable
     */
    public function setUploaderAvailable($uploaderAvailable)
    {
        $this->uploaderAvailable = $uploaderAvailable;
    }

    /**
     * @return boolean
     */
    public function isFileRequired()
    {
        return $this->file_required;
    }

    /**
     * @param boolean $file_required
     */
    public function setFileRequired($file_required)
    {
        $this->file_required = $file_required;
    }

    /**
     * @return Mnumi\Bundle\RestServerBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Mnumi\Bundle\RestServerBundle\Entity\Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return \Mnumi\Bundle\ProductBundle\Entity\ProductField
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param \Mnumi\Bundle\ProductBundle\Entity\ProductField $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fixprice = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get uploaderAvailable
     *
     * @return boolean
     */
    public function getUploaderAvailable()
    {
        return $this->uploaderAvailable;
    }

    /**
     * Get file_required
     *
     * @return boolean
     */
    public function getFileRequired()
    {
        return $this->file_required;
    }

    /**
     * Set created
     *
     * @param  \DateTime $created
     * @return Product
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Set workflow
     *
     * @param  \Mnumi\Bundle\RestServerBundle\Entity\Workflow $workflow
     * @return Product
     */
    public function setWorkflow(\Mnumi\Bundle\RestServerBundle\Entity\Workflow $workflow = null)
    {
        $this->workflow = $workflow;

        return $this;
    }

    /**
     * Add fields
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\ProductField $fields
     * @return Product
     */
    public function addField(\Mnumi\Bundle\ProductBundle\Entity\ProductField $fields)
    {
        $this->fields[] = $fields;

        return $this;
    }

    /**
     * Remove fields
     *
     * @param \Mnumi\Bundle\ProductBundle\Entity\ProductField $fields
     */
    public function removeField(\Mnumi\Bundle\ProductBundle\Entity\ProductField $fields)
    {
        $this->fields->removeElement($fields);
    }

    /**
     * Add fixprice
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\ProductFixprice $fixprice
     * @return Product
     */
    public function addFixprice(\Mnumi\Bundle\ProductBundle\Entity\ProductFixprice $fixprice)
    {
        $this->fixprice[] = $fixprice;

        return $this;
    }

    /**
     * Remove fixprice
     *
     * @param \Mnumi\Bundle\ProductBundle\Entity\ProductFixprice $fixprice
     */
    public function removeFixprice(\Mnumi\Bundle\ProductBundle\Entity\ProductFixprice $fixprice)
    {
        $this->fixprice->removeElement($fixprice);
    }

    /**
     * Get fixprice
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFixprice()
    {
        return $this->fixprice;
    }

    /**
     * Set staticQuantity
     *
     * @param  string  $staticQuantity
     * @return Product
     */
    public function setStaticQuantity($staticQuantity)
    {
        $this->staticQuantity = $staticQuantity;

        return $this;
    }

    /**
     * Get staticQuantity
     *
     * @return string
     */
    public function getStaticQuantity()
    {
        return explode(",", $this->staticQuantity);
    }

    /**
     * Set simpleCalculation
     *
     * @param  boolean $simpleCalculation
     * @return Product
     */
    public function setSimpleCalculation($simpleCalculation)
    {
        $this->simpleCalculation = $simpleCalculation;

        return $this;
    }

    /**
     * Get simpleCalculation
     *
     * @return boolean
     */
    public function getSimpleCalculation()
    {
        return $this->simpleCalculation;
    }

    /**
     * Set isStaticQuantity
     *
     * @param  boolean $isStaticQuantity
     * @return Product
     */
    public function setIsStaticQuantity($isStaticQuantity)
    {
        $this->isStaticQuantity = $isStaticQuantity;

        return $this;
    }

    /**
     * Get isStaticQuantity
     *
     * @return boolean
     */
    public function getIsStaticQuantity()
    {
        return $this->isStaticQuantity;
    }

    /**
     * @return int
     */
    public function getTaxValue()
    {
        return $this->tax_value;
    }

    /**
     * @param int $tax_value
     */
    public function setTaxValue($tax_value)
    {
        $this->tax_value = $tax_value;
    }

    /**
     * @return boolean
     */
    public function getCalculateAsCard()
    {
        return $this->calculateAsCard;
    }

    /**
     * @param boolean $calculateAsCard
     */
    public function setCalculateAsCard($calculateAsCard)
    {
        $this->calculateAsCard = $calculateAsCard;
    }


}
