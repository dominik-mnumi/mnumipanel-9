<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * Fieldset
 *
 * @ORM\Table(name="fieldset")
 * @ORM\Entity
 */
class Fieldset
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"ProductList"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Groups({"ProductList"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     * @Groups({"ProductList"})
     */
    private $label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="changeable", type="boolean", nullable=false)
     */
    private $changeable = '1';

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     * @Groups({"ProductList"})
     */
    private $hidden = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="root_id", type="bigint", nullable=true)
     * @Serializer\Exclude
     */
    private $rootId;

    /**
     * @var integer
     *
     * @ORM\Column(name="lft", type="integer", nullable=true)
     * @Serializer\Exclude
     */
    private $lft;

    /**
     * @var integer
     *
     * @ORM\Column(name="rgt", type="integer", nullable=true)
     * @Serializer\Exclude
     */
    private $rgt;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="smallint", nullable=true)
     * @Serializer\Exclude
     */
    private $level;

    /**
     * @var Field
     *
     * @ORM\OneToMany(targetEntity="FieldItem", mappedBy="field", fetch="EAGER")
     * @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     *
     */
    private $items = null;

    public function __toString()
    {
        return $this->getId() . '=>' . $this->getName();
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set changeable
     *
     * @param  boolean  $changeable
     * @return Fieldset
     */
    public function setChangeable($changeable)
    {
        $this->changeable = $changeable;

        return $this;
    }

    /**
     * Get changeable
     *
     * @return boolean
     */
    public function getChangeable()
    {
        return $this->changeable;
    }

    /**
     * Set hidden
     *
     * @param  boolean  $hidden
     * @return Fieldset
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set rootId
     *
     * @param  integer  $rootId
     * @return Fieldset
     */
    public function setRootId($rootId)
    {
        $this->rootId = $rootId;

        return $this;
    }

    /**
     * Get rootId
     *
     * @return integer
     */
    public function getRootId()
    {
        return $this->rootId;
    }

    /**
     * Set lft
     *
     * @param  integer  $lft
     * @return Fieldset
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set rgt
     *
     * @param  integer  $rgt
     * @return Fieldset
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set level
     *
     * @param  integer  $level
     * @return Fieldset
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Add items
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItem $items
     * @return Fieldset
     */
    public function addItem(\Mnumi\Bundle\ProductBundle\Entity\FieldItem $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \Mnumi\Bundle\ProductBundle\Entity\FieldItem $items
     */
    public function removeItem(\Mnumi\Bundle\ProductBundle\Entity\FieldItem $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }
}
