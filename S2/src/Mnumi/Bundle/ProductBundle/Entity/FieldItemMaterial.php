<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FieldItemMaterial
 *
 * @ORM\Table(name="field_item_material", indexes={@ORM\Index(columns={"printsize_id"}), @ORM\Index(columns={"field_item_id"})})
 * @ORM\Entity
 */
class FieldItemMaterial
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FieldItem
     *
     * @ORM\ManyToOne(targetEntity="FieldItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     * })
     */
    private $fieldItem;

    /**
     * @var \Printsize
     *
     * @ORM\ManyToOne(targetEntity="PrintSize")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="printsize_id", referencedColumnName="id")
     * })
     */
    private $printSize;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set printsize
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\PrintSize $printsize
     * @return FieldItemMaterial
     */
    public function setPrintSize(\Mnumi\Bundle\ProductBundle\Entity\PrintSize $printSize = null)
    {
        $this->printSize = $printSize;

        return $this;
    }

    /**
     * Get printsize
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\PrintSize
     */
    public function getPrintSize()
    {
        return $this->printSize;
    }

    /**
     * Set fieldItem
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem
     * @return FieldItemMaterial
     */
    public function setFieldItem(\Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem = null)
    {
        $this->fieldItem = $fieldItem;

        return $this;
    }

    /**
     * Get fieldItem
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\FieldItem
     */
    public function getFieldItem()
    {
        return $this->fieldItem;
    }
}
