<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * ProductFieldItem
 *
 * @ORM\Table(name="product_field_item", indexes={@ORM\Index(columns={"product_field_id"}), @ORM\Index(columns={"field_item_id"})})
 * @ORM\Entity
 */
class ProductFieldItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"ProductList"})
     */
    private $id;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\ProductBundle\Entity\ProductField", inversedBy="items")
     * @ORM\JoinColumn(name="product_field_id", referencedColumnName="id")
     */
    private $productField;

    /**
     * @var \FieldItem
     *
     * @ORM\OneToOne(targetEntity="\Mnumi\Bundle\ProductBundle\Entity\FieldItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     * })
     * @Groups({"ProductList"})
     */
    private $fieldItem;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Groups({"ProductList"})
     */
    private $label;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productField
     *
     * @param  integer          $productField
     * @return ProductFieldItem
     */
    public function setProductField($productField)
    {
        $this->productField = $productField;

        return $this;
    }

    /**
     * Get productField
     *
     * @return integer
     */
    public function getProductField()
    {
        return $this->productField;
    }

    /**
     * Set fieldItem
     *
     * @param  integer          $fieldItem
     * @return ProductFieldItem
     */
    public function setFieldItem($fieldItem)
    {
        $this->fieldItem = $fieldItem;

        return $this;
    }

    /**
     * Get fieldItem
     *
     * @return integer
     */
    public function getFieldItem()
    {
        return $this->fieldItem;
    }

    /**
     * Set label
     *
     * @param  string           $label
     * @return ProductFieldItem
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
}
