<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FieldItemPrice
 *
 * @ORM\Table(name="field_item_price", indexes={@ORM\Index(columns={"field_item_id"}), @ORM\Index(columns={"pricelist_id"})})
 * @ORM\Entity
 */
class FieldItemPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="minimal_price", type="float", precision=18, scale=3, nullable=true)
     */
    private $minimalPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="maximal_price", type="float", precision=18, scale=3, nullable=true)
     */
    private $maximalPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="plus_price", type="float", precision=18, scale=3, nullable=true)
     */
    private $plusPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="plus_price_double", type="float", precision=18, scale=3, nullable=true)
     */
    private $plusPriceDouble;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=3, nullable=true)
     */
    private $cost;

    /**
     * @var \FieldItem
     *
     * @ORM\ManyToOne(targetEntity="FieldItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     * })
     */
    private $fieldItem;

    /**
     * @var \Mnumi\Bundle\CalculationBundle\Entity\Pricelist
     *
     * @ORM\OneToOne(targetEntity="\Mnumi\Bundle\CalculationBundle\Entity\PriceList")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pricelist_id", referencedColumnName="id")
     * })
     */
    private $pricelist;

    /**
     * @var FieldItemSize
     *
     * @ORM\OneToMany(targetEntity="FieldItemPriceQuantity", mappedBy="fieldItemPrice", fetch="EAGER")
     * @ORM\JoinColumn(name="field_item_price_id", referencedColumnName="id")
     */
    private $quantity;

    /**
     * Set minimalPrice
     *
     * @param  float          $minimalPrice
     * @return FieldItemPrice
     */
    public function setMinimalPrice($minimalPrice)
    {
        $this->minimalPrice = $minimalPrice;

        return $this;
    }

    /**
     * Get minimalPrice
     *
     * @return float
     */
    public function getMinimalPrice()
    {
        return $this->minimalPrice;
    }

    /**
     * Set maximalPrice
     *
     * @param  float          $maximalPrice
     * @return FieldItemPrice
     */
    public function setMaximalPrice($maximalPrice)
    {
        $this->maximalPrice = $maximalPrice;

        return $this;
    }

    /**
     * Get maximalPrice
     *
     * @return float
     */
    public function getMaximalPrice()
    {
        return $this->maximalPrice;
    }

    /**
     * Set plusPrice
     *
     * @param  float          $plusPrice
     * @return FieldItemPrice
     */
    public function setPlusPrice($plusPrice)
    {
        $this->plusPrice = $plusPrice;

        return $this;
    }

    /**
     * Get plusPrice
     *
     * @return float
     */
    public function getPlusPrice()
    {
        return $this->plusPrice;
    }

    /**
     * Set plusPriceDouble
     *
     * @param  float          $plusPriceDouble
     * @return FieldItemPrice
     */
    public function setPlusPriceDouble($plusPriceDouble)
    {
        $this->plusPriceDouble = $plusPriceDouble;

        return $this;
    }

    /**
     * Get plusPriceDouble
     *
     * @return float
     */
    public function getPlusPriceDouble()
    {
        return $this->plusPriceDouble;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pricelist
     *
     * @param  \Mnumi\Bundle\CalculationBundle\Entity\\Pricelist $pricelist
     * @return FieldItemPrice
     */
    public function setPricelist(\Mnumi\Bundle\CalculationBundle\Entity\Pricelist $pricelist = null)
    {
        $this->pricelist = $pricelist;

        return $this;
    }

    /**
     * Get pricelist
     *
     * @return \Mnumi\Bundle\CalculationBundle\Entity\Pricelist
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }

    /**
     * Set fieldItem
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem
     * @return FieldItemPrice
     */
    public function setFieldItem(\Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem = null)
    {
        $this->fieldItem = $fieldItem;

        return $this;
    }

    /**
     * Get fieldItem
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\FieldItem
     */
    public function getFieldItem()
    {
        return $this->fieldItem;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quantity = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add quantity
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItemPriceQuantity $quantity
     * @return FieldItemPrice
     */
    public function addQuantity(\Mnumi\Bundle\ProductBundle\Entity\FieldItemPriceQuantity $quantity)
    {
        $this->quantity[] = $quantity;

        return $this;
    }

    /**
     * Remove quantity
     *
     * @param \Mnumi\Bundle\ProductBundle\Entity\FieldItemPriceQuantity $quantity
     */
    public function removeQuantity(\Mnumi\Bundle\ProductBundle\Entity\FieldItemPriceQuantity $quantity)
    {
        $this->quantity->removeElement($quantity);
    }

    /**
     * Get quantity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set cost
     *
     * @param float $cost
     * @return FieldItemPrice
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }
}
