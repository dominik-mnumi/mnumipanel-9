<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FieldItemPriceQuantity
 *
 * @ORM\Table(name="field_item_price_quantity", indexes={@ORM\Index(columns={"field_item_price_type_name"}), @ORM\Index(columns={"field_item_price_id"})})
 * @ORM\Entity
 */
class FieldItemPriceQuantity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float", precision=18, scale=4, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $quantity = '0.0000';

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=18, scale=3, nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type = 'amount';

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=3, nullable=false)
     */
    private $cost = '0.000';

    /**
     * @var \FieldItemPrice
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="FieldItemPrice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_item_price_id", referencedColumnName="id")
     * })
     */
    private $fieldItemPrice;

    /**
     * @var \FieldItemPriceType
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="FieldItemPriceType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_item_price_type_name", referencedColumnName="name")
     * })
     */
    private $fieldItemPriceTypeName;

    /**
     * Set price
     *
     * @param  float                  $price
     * @return FieldItemPriceQuantity
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set type
     *
     * @param  string                 $type
     * @return FieldItemPriceQuantity
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set cost
     *
     * @param  float                  $cost
     * @return FieldItemPriceQuantity
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set id
     *
     * @param  integer                $id
     * @return FieldItemPriceQuantity
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param  float                  $quantity
     * @return FieldItemPriceQuantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set fieldItemPriceTypeName
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItemPriceType $fieldItemPriceTypeName
     * @return FieldItemPriceQuantity
     */
    public function setFieldItemPriceTypeName(\Mnumi\Bundle\ProductBundle\Entity\FieldItemPriceType $fieldItemPriceTypeName)
    {
        $this->fieldItemPriceTypeName = $fieldItemPriceTypeName;

        return $this;
    }

    /**
     * Get fieldItemPriceTypeName
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\FieldItemPriceType
     */
    public function getFieldItemPriceTypeName()
    {
        return $this->fieldItemPriceTypeName;
    }

    /**
     * Set fieldItemPrice
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItemPrice $fieldItemPrice
     * @return FieldItemPriceQuantity
     */
    public function setFieldItemPrice(\Mnumi\Bundle\ProductBundle\Entity\FieldItemPrice $fieldItemPrice)
    {
        $this->fieldItemPrice = $fieldItemPrice;

        return $this;
    }

    /**
     * Get fieldItemPrice
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\FieldItemPrice
     */
    public function getFieldItemPrice()
    {
        return $this->fieldItemPrice;
    }
}
