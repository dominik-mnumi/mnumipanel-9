<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\ProductBundle\Tests\Form\Type;

use Mnumi\Bundle\ProductBundle\Entity\Product;
use Mnumi\Bundle\ProductBundle\Form\Type\ProductType;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * ProductTypeTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ProductTypeTest extends TypeTestCase
{
    /**
     * @dataProvider getValidTestData
     */
    public function testSubmitValidData($formData)
    {
        $object = new Product();

        $form = $this->factory->create(new ProductType(), $object);

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertTrue($form->isValid());
        $this->assertEquals($object, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    public function getValidTestData()
    {
        return array(
            array(
                'formData' => array(
                    'name' => 'name',
                    'description' => 'description',
                    'active' => 'active',
                ),
            ),
            array(
                'formData' => array(),
            ),
        );
    }
}
