<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\ProductBundle\DataTransformer;

use Mnumi\Bundle\ProductBundle\Entity\Product;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
/**
 * SlugToIdTransformer class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class SlugToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms a value from the original representation to a transformed representation.
     *
     * This method is called on two occasions inside a form field:
     *
     * 1. When the form field is initialized with the data attached from the datasource (object or array).
     * 2. When data from a request is submitted using {@link Form::submit()} to transform the new input data
     *    back into the renderable format. For example if you have a date field and submit '2009-10-10'
     *    you might accept this value because its easily parsed, but the transformer still writes back
     *    "2009/10/10" onto the form field (for further displaying or other purposes).
     *
     * This method must be able to deal with empty values. Usually this will
     * be NULL, but depending on your implementation other empty values are
     * possible as well (such as empty strings). The reasoning behind this is
     * that value transformers must be chainable. If the transform() method
     * of the first value transformer outputs NULL, the second value transformer
     * must be able to process that value.
     *
     * By convention, transform() should return an empty string if NULL is
     * passed.
     *
     * @param Product $product
     *
     * @return mixed The value in the transformed representation
     *
     */
    public function transform($product)
    {
        if ($product === null) {
            return '';
        }

        return $product->getSlug();
    }

    /**
     * Transforms a value from the transformed representation to its original
     * representation.
     *
     * This method is called when {@link Form::submit()} is called to transform the requests tainted data
     * into an acceptable format for your data processing/model layer.
     *
     * This method must be able to deal with empty values. Usually this will
     * be an empty string, but depending on your implementation other empty
     * values are possible as well (such as empty strings). The reasoning behind
     * this is that value transformers must be chainable. If the
     * reverseTransform() method of the first value transformer outputs an
     * empty string, the second value transformer must be able to process that
     * value.
     *
     * By convention, reverseTransform() should return NULL if an empty string
     * is passed.
     *
     * @param  mixed                                                           $slug
     * @throws \Symfony\Component\Form\Exception\TransformationFailedException
     * @internal param mixed $value The value in the transformed representation
     *
     * @return Product The value in the original representation
     *
     */
    public function reverseTransform($slug)
    {
        if (!$slug) {
            return null;
        }

        $product = $this->om
            ->getRepository('MnumiProductBundle:Product')
            ->findOneBy(array('slug' => $slug))
        ;

        if (null === $product) {
            throw new TransformationFailedException(sprintf(
                'An product with slug "%s" does not exist!',
                $slug
            ));
        }

        return $product;
    }
}
