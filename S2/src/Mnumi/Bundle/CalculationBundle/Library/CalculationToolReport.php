<?php

namespace Mnumi\Bundle\CalculationBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class CalculationToolReport
{
    /** @var array of CalculationToolPriceItem objects */
    protected $priceItems;

    /** @var array of CalculationToolOtherPriceItem objects */
    protected $otherPriceItems;

    protected $factor;
    protected $count;
    protected $quantity;
    protected $sides;
    protected $size;
    protected $sizeMetric;
    protected $printSizeWidth;
    protected $printSizeHeight;
    protected $printerWidth;
    protected $printerHeight;
    protected $printerName;
    protected $measureType;
    protected $fixedPrice;
    protected $pricelistId;
    protected $itemNbPerPage;
    protected $summaryPriceNet;
    protected $priceTax;
    protected $summaryPriceGross;
    protected $linearMetre;
    protected $squareMetre;

    public function getPriceItems()
    {
        return $this->priceItems;
    }

    public function getOtherPriceItems()
    {
        return $this->otherPriceItems;
    }

    public function getFactor()
    {
        return $this->factor;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getSides()
    {
        return $this->sides;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getSizeMetric()
    {
        return $this->sizeMetric;
    }

    public function getPrintSizeWidth()
    {
        return $this->printSizeWidth;
    }

    public function getPrintSizeHeight()
    {
        return $this->printSizeHeight;
    }

    public function getPrinterWidth()
    {
        return $this->printerWidth;
    }

    public function getPrinterHeight()
    {
        return $this->printerHeight;
    }

    public function getPrinterName()
    {
        return $this->printerName;
    }

    public function getMeasureType()
    {
        return $this->measureType;
    }

    public function getFixedPrice()
    {
        return $this->fixedPrice;
    }

    public function getPricelistId()
    {
        return $this->pricelistId;
    }

    public function getItemNbPerPage()
    {
        return $this->itemNbPerPage;
    }

    public function getSummaryPriceNet()
    {
        return $this->summaryPriceNet;
    }

    public function getPriceTax()
    {
        return $this->priceTax;
    }

    public function getSummaryPriceGross()
    {
        return $this->summaryPriceGross;
    }

    public function getLinearMetre()
    {
        return $this->linearMetre;
    }

    public function getSquareMetre()
    {
        return $this->squareMetre;
    }

    public function addPriceItem(CalculationToolPriceItem $priceItem)
    {
        $this->priceItems[] = $priceItem;
    }

    public function addOtherPriceItem(CalculationToolOtherPriceItem $priceItem)
    {
        $this->otherPriceItems[] = $priceItem;
    }

    public function setOtherPriceItems($otherPriceItems)
    {
        $this->otherPriceItems = $otherPriceItems;
    }

    public function setPriceItems($priceItems)
    {
        $this->priceItems = $priceItems;
    }

    public function setFactor($factor)
    {
        $this->factor = $factor;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setSides($sides)
    {
        $this->sides = $sides;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function setSizeMetric($sizeMetric)
    {
        $this->sizeMetric = $sizeMetric;
    }

    public function setPrintSizeWidth($printSizeWidth)
    {
        $this->printSizeWidth = $printSizeWidth;
    }

    public function setPrintSizeHeight($printSizeHeight)
    {
        $this->printSizeHeight = $printSizeHeight;
    }

    public function setPrinterWidth($printerWidth)
    {
        $this->printerWidth = $printerWidth;
    }

    public function setPrinterHeight($printerHeight)
    {
        $this->printerHeight = $printerHeight;
    }

    public function setPrinterName($printerName)
    {
        $this->printerName = $printerName;
    }

    public function setMeasureType($measureType)
    {
        $this->measureType = $measureType;
    }

    public function setFixedPrice($fixedPrice)
    {
        $this->fixedPrice = $fixedPrice;
    }

    public function setPricelistId($pricelistId)
    {
        $this->pricelistId = $pricelistId;
    }

    public function setItemNbPerPage($itemNbPerPage)
    {
        $this->itemNbPerPage = $itemNbPerPage;
    }

    public function setSummaryPriceNet($summaryPriceNet)
    {
        $this->summaryPriceNet = $summaryPriceNet;
    }

    public function setPriceTax($priceTax)
    {
        $this->priceTax = $priceTax;
    }

    public function setSummaryPriceGross($summaryPriceGross)
    {
        $this->summaryPriceGross = $summaryPriceGross;
    }

    public function setLinearMetre($linearMetre)
    {
        $this->linearMetre = $linearMetre;
    }

    public function setSquareMetre($squareMetre)
    {
        $this->squareMetre = $squareMetre;
    }

    public function setProperties($properties)
    {
        foreach ($properties as $name => $value) {

            $setterName = 'set' . ucfirst($name);

            if (method_exists($this, $setterName)) {
                $this->$setterName($value);
            }
        }
    }

    /**
     * Gets measure of unit
     *
     * @return string
     */
    public function getMeasureOfUnit()
    {
        // if price per count or quantity
        if(!in_array($this->getMeasureType(),
                        array('price_linear_metre', 'price_square_metre')))
        {
            return 'sheet';
        } else {
            // @TO CHECK
            // if price per linear
            if ($this->getMeasureType() == 'price_linear_metre') {
                return '';
            }
            // if price per square
            elseif ($this->getMeasureType() == 'price_square_metre') {
                return 'm2';
            }
        }
    }

    public function toArray()
    {
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setCamelizedAttributes(array('MeasureOfUnit'));
        $serializer = new Serializer(array($normalizer));

        $normalized = $normalizer->normalize($this);
        unset($normalized['otherPriceItems']);

        $priceItems = array();
        $otherPriceItemsList = array();

        foreach ($this->getPriceItems() as $item) {
            $priceItems[$item->getName()] = $normalizer->normalize($item);
        }

        $otherPriceItems = $this->getOtherPriceItems();

        if ($otherPriceItems) {
            foreach ($otherPriceItems as $item) {
                $otherPriceItemsList[$item->getKey()] = $normalizer->normalize($item);
            }
        }

        $priceItems['OTHER'] = $otherPriceItemsList;

        $normalized['priceItems'] = $priceItems;

        return $normalized;

    }
}
