<?php

namespace Mnumi\Bundle\CalculationBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class CalculationToolOtherPriceItem extends CalculationToolPriceItem
{
    private $id;
    private $hidden;
    private $key;

    public function getId()
    {
        return $this->id;
    }

    public function getHidden()
    {
        return $this->hidden;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }
}
