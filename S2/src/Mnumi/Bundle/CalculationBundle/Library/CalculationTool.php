<?php

namespace Mnumi\Bundle\CalculationBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\CalculationBundle\Library\Exception\CalculationInvalidArgumentException;
use Mnumi\Bundle\CalculationBundle\Library\Exception\CalculationMissingCustomSizeException;

/**
 * calculationTool help to calculate price for order
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class CalculationTool implements CalculationToolInterface
{
    public static $customizable = '- Customizable -';
    protected $sizeMetric = 'mm';
    public static $amountType = 'amount';
    public static $percentType = 'percent';
    protected $fieldArray;
    protected $quantity;
    protected $count;
    protected $factor;
    protected $measureType;
    protected $costMeasureType;
    protected $itemNbPerPage;
    protected $printSizeWidth;
    protected $printSizeHeight;
    protected $printerWidth;
    protected $printerHeight;
    protected $printerName;
    protected $pricelistId;
    protected $priceTax;

    /** @var CalculationFieldItem */
    protected $material;

    /** @var CalculationFieldItem */
    protected $print;

    /** @var CalculationFieldItem */
    protected $sides;

    /** @var CalculationFieldItem */
    protected $size;
    protected $summaryPriceNet;
    protected $fixedPrice;
    protected $other;
    protected $printSize;

    /** @var \CalculationProductData */
    protected $calculationProductData;
    // metric sizes
    public static $metricFactor = array(
        'mm' => 1000,
        'cm' => 100,
        'm' => 1,
    );

    public static $price_simplex = 'price_simplex';
    public static $price_duplex = 'price_duplex';
    public static $price_page = 'price_page';
    public static $price_copy = 'price_copy';
    public static $price_linear_metre = 'price_linear_metre';
    public static $price_square_metre = 'price_square_metre';
    public static $price_item = 'price_item';

    public static $priceTypeWithoutSimplexAndDuplexArr = array('price_page', 'price_copy',
        'price_linear_metre', 'price_square_metre',
        'price_item'
    );

    /** @var CalculationToolReport */
    protected $report;

    public function __construct($calculationProductData)
    {
        $this->calculationProductData = $calculationProductData;
        $priceTax = $this->calculationProductData->priceTax;

        if (!is_numeric($priceTax) || $priceTax > 1 || $priceTax < 0) {
            throw new \Exception('Incorrect price tax value: ' . $priceTax . '. It should be float between 0 and 1.');
        }

        $this->priceTax = $priceTax;
    }

    /**
     * @param  array      $inputArray  Include:
     *                                 - COUNT
     *                                 - QUANTITY
     *                                 - MATERIAL
     *                                 - SIZE
     *                                 - PRINT
     * @param  int        $pricelistId Pricelist ID number (for prevent to many REST requests)
     * @throws \Exception
     *
     * Some values can be missed, that is why prepareFieldArray() is necessary
     */
    public function initialize(array $inputArray, $pricelistId)
    {
        // sets pricelist id
        $this->pricelistId = $pricelistId;

        $defaultFields = $this->getCalculationProductData()->getDefaultFields();

        //gets field array
        $fieldArray = $this->prepareFieldArray($defaultFields, $inputArray);

        //gets count
        $this->count = $fieldArray['COUNT']['value'];

        if ($this->count < 1) {
            throw new \Exception('Count is not valid.');
        }

        //gets quantity
        $this->quantity = $fieldArray['QUANTITY']['value'];

        if ($this->quantity < 1) {
            throw new \Exception('Quantity is not valid.');
        }

        //gets material
        $this->material = $this->getCalculationField('MATERIAL')->get($fieldArray['MATERIAL']['value']);

        $printSize = $this->material->getPrintSize();

        $this->printerWidth = $printSize->getWidth();
        $this->printerHeight = $printSize->getHeight();
        $this->printerName = $printSize->getPrinterName();

        //gets size
        $this->size = $this->getCalculationField('SIZE')->get($fieldArray['SIZE']['value']);

        if (!$this->size) {
            throw new \Exception('Missing size object: ' . $fieldArray['SIZE']['value']);
        }

        // metric size - validate
        // if set but not valid metric
        if (isset($fieldArray['SIZE']['metric']['value']) && !in_array($fieldArray['SIZE']['metric']['value'], array_keys(CalculationFactor::$metricSize))) {
            throw new \Exception('Metric size is not valid.');
        }

        $calculationSize = $this->size;

        // if customizable and at least one parameter is not defined (width or height)
        if ((self::$customizable == $calculationSize->getName())
                && (!isset($fieldArray['SIZE']['metric']['value']))
                && ((empty($fieldArray['SIZE']['width']['value']) || empty($fieldArray['SIZE']['height']['value'])))) {
            throw new CalculationMissingCustomSizeException('Fill size in form: width, height and metric');
        }

        // if customizable sets custom size
        if (self::$customizable != $calculationSize->getName()) {
            $itemSize = $calculationSize->getItemSize();
            $this->printSizeWidth = $itemSize->getWidth();
            $this->printSizeHeight = $itemSize->getHeight();
        } else {
            // custom size
            $this->sizeMetric = $fieldArray['SIZE']['metric']['value'];
            $this->printSizeWidth = $fieldArray['SIZE']['width']['value'];
            $this->printSizeHeight = $fieldArray['SIZE']['height']['value'];
        }

        //gets material
        $this->sides = $this->getCalculationField('SIDES')->get($fieldArray['SIDES']['value']);

        if (!$this->sides) {
            throw new \Exception('Missing sides object: ' . $fieldArray['SIDES']['value']);
        }

        //gets print
        $this->print = $this->getCalculationField('PRINT')->get($fieldArray['PRINT']['value']);

        if (!$this->print) {
            throw new \Exception('Missing print object: ' . $fieldArray['PRINT']['value']);
        }

        //gets factor
        list($this->itemNbPerPage, $this->factor) = $this->getFactor();

        // gets measure type
        $this->measureType = $this->getCalculationProductData()->getPriceMeasureUnit();

        // gets cost measure type
        $this->costMeasureType = $this->getCalculationProductData()->getMeasureUnit();

        if (is_bool($this->priceTax) && $this->priceTax == false) {
            throw new \Exception('Price tax is not valid.');
        }

        $this->other = isset($fieldArray['OTHER']) ? $fieldArray['OTHER'] : false;

        if ($fieldArray['COUNT']['value'] == null || $fieldArray['QUANTITY']['value'] == null || $fieldArray['SIZE']['value'] == null || $fieldArray['SIDES']['value'] == null || $fieldArray['PRINT']['value'] == null) {
            throw new \Exception('The mandatory field not filled, to create Calculation.');
        }
    }

    /**
     * Fetch report as array
     */
    public function getReport()
    {
        $this->report = new CalculationToolReport();

        $calculationSides = $this->sides;
        $calculationSize = $this->size;

        $this->calculateComponentPriceArray();

        $reportProperties = array(
            'factor' => $this->factor,
            'count' => array(
                'label' => $this->getFieldsetLabel('count'),
                'value' => $this->count,
            ),
            'quantity' => array(
                'label' => $this->getFieldsetLabel('quantity'),
                'value' => $this->quantity,
            ),
            'sides' => array(
                'label' => $this->getFieldsetLabel('sides'),
                'fieldLabel' => $calculationSides->getName(),
            ),
            'size' => array(
                'label' => $this->getFieldsetLabel('size'),
                'fieldLabel' => $calculationSize->getName(),
            ),
            'sizeMetric' => $this->sizeMetric,
            'printSizeWidth' => $this->printSizeWidth,
            'printSizeHeight' => $this->printSizeHeight,
            'printerWidth' => $this->printerWidth,
            'printerHeight' => $this->printerHeight,
            'printerName' => $this->printerName,
            'measureType' => $this->measureType,
            'fixedPrice' => ($this->fixedPrice) ? 1 : 0,
            'pricelistId' => $this->pricelistId,
            'itemNbPerPage' => $this->itemNbPerPage,
            'summaryPriceNet' => $this->getSummaryPriceNet(),
            'priceTax' => $this->priceTax,
            'summaryPriceGross' => $this->getSummaryPriceGross(),
            'linearMetre' => $this->getPriceLinearMetre(),
            'squareMetre' => $this->getPriceSquareMetre()
        );

        $this->report->setProperties($reportProperties);

        return $this->report;
    }

    public function fetchReport()
    {
        return $this->getReport()->toArray();
    }

    /**
     * Returns metric factor based on size metric.
     *
     * E.g.:
     * Input: mm
     * Output: 1000
     *
     * @param  string  $sizeMetric
     * @return integer
     */
    public static function getMetricFactor($sizeMetric)
    {
        return self::$metricFactor[$sizeMetric];
    }

    /**
     * Returns size in proper metric basing on $paramArr.
     *
     * Array:
     * $params = array('printSizeWidth' => $printSizeWidth,
     *                 'printSizeHeight' => $printSizeHeight,
     *  	       'metricFactor' => $metricFactor,
     *  	       'count' => $count,
     *  	       'quantity' => $quantity);
     *
     * @return string
     */
    protected function getPriceLinearMetre()
    {
        $printSizeHeight = $this->printSizeHeight;
        $printSizeWidth = $this->printSizeWidth;
        $metricFactor = self::getMetricFactor($this->sizeMetric);

        $linearMetre = 2 * $printSizeHeight / $metricFactor + 2 * $printSizeWidth / $metricFactor;

        return $this->count * $this->quantity * $linearMetre;
    }

    /**
     * Returns size in proper metric basing on $paramArr.
     *
     * @return string
     */
    protected function getPriceSquareMetre()
    {
        $printSizeHeight = $this->printSizeHeight;
        $printSizeWidth = $this->printSizeWidth;
        $metricFactor = self::getMetricFactor($this->sizeMetric);

        $squareMetre = $printSizeHeight / $metricFactor * $printSizeWidth / $metricFactor;

        return $this->count * $this->quantity * $squareMetre;
    }

    /**
     * Returns size in proper metric basing on $paramArr.
     *
     * @return string
     */
    public function getPricePage()
    {
        return $this->factor;
    }

    /**
     * Returns size in proper metric basing on $paramArr.
     *
     * @return string
     */
    public function getPriceCopy()
    {
        return $this->quantity;
    }

    /**
     * Returns size in proper metric basing on $paramArr.
     *
     * @return string
     */
    public function getPriceItem()
    {
        return $this->count * $this->quantity;
    }

    /**
     * Returns factor. How many printer printsizes is in size. Attribute 'SIZE' in fieldArray is mandatory.
     *
     * example: Printer A3 - SIZE - A4; FACTOR: 0.5
     * @return array($itemNbPerPage, $factor)
     * @throws \Exception
     */
    protected function getFactor()
    {
        if(!$this->printSizeWidth || !$this->printSizeHeight){
            throw new \Mnumi\Bundle\CalculationBundle\Library\Exception\CalculationMissingCustomSizeException('Fields: width and height are required');
        }
        $result = CalculationFactor::count(
                        $this->printerWidth, $this->printerHeight, $this->printSizeWidth, $this->printSizeHeight, $this->count * $this->quantity, $this->sizeMetric);

        $this->itemNbPerPage = $result['itemNbPerPage'];

        if ($result['factor'] <= 0) {
            throw new \Mnumi\Bundle\CalculationBundle\Library\Exception\CalculationInvalidArgumentException('countFactor method returned 0');
        }

        return array($result['itemNbPerPage'], $result['factor']);
    }

    /**
     * Returns price of component
     *
     * @param  string $componentName e.g. 'MATERIAL'
     * @return float
     */
    protected function getComponentPrice($componentName)
    {
        return $this->getComponentPriceByType($componentName, 'Price');
    }

    /**
     * Returns cost of component
     *
     * @param  string $componentName e.g. 'MATERIAL'
     * @return float
     */
    protected function getComponentCost($componentName)
    {
        return $this->getComponentPriceByType($componentName, 'Cost');
    }

    /**
     * Returns component price or cost
     *
     * @param  string     $componentName e.g. 'MATERIAL'
     * @param  string     $type          Value cost or price
     * @throws \Exception
     * @return float
     */
    private function getComponentPriceByType($componentName, $type)
    {
        if (!in_array($type, array('Price', 'Cost'))) {
            throw new \Exception('Incorrect type');
        }

        if (!in_array($componentName, array('MATERIAL', 'PRINT')) && !preg_match('/^OTHER_/', $componentName)) {
            throw new \Exception('Incorrect component name: ' . $componentName);
        }

        $typeMethod = 'get' . $type;
        $sumPrice = (float) 0;

        $calculationField = $this->getCalculationFieldItem($componentName);
        $calculationKeys = $this->getCalculationTypes($componentName);

        $calculationFieldItem = false;

        if ($calculationField) {

            $defaultPriceListId = $this->getCalculationProductData()->getDefaultPriceListId();

            $calculationFieldItem = $calculationField->get($this->pricelistId) ?:
                                    $calculationField->get($defaultPriceListId);

            /**
             * Conditional for checking if price/cost per count/quantity/sheet
             * has been set for the given pricelist. This could be false
             * if no one has been specified.
             */
            if ($calculationFieldItem) {

                if (in_array($componentName, array('MATERIAL', 'PRINT')) && $type == 'Cost') {

                    $range = ($this->getCostMeasureType() == 'price_square_metre')
                        ? $this->getPriceSquareMetre()
                        : $this->getPricePage();
                    $price = $calculationFieldItem->getCost();

                    if ($this->sides->getName() == 'Double' && $componentName != 'MATERIAL') {
                        $price *= 2;
                    }

                    $sumPrice += ($range * $price);

                } else {

                    /**
                     * Base Cost of the given field
                     * DISCLAIMER: This is not cost calculated from *given range in pricelist*
                     * but the *global cost per pricelist*
                     */
                    if ($type == 'Cost') {
                        $sumPrice += $calculationFieldItem->getCost();
                    }
                    if ($type === 'Price') {
                        if ($componentName == 'PRINT') {
                            $sumPrice += ($this->sides->getName() == 'Double') ? $calculationFieldItem->getPlusPriceDouble() : $calculationFieldItem->getPlusPrice()
                            ;
                        } else { // MATERIAL, OTHER
                            $sumPrice += $calculationFieldItem->getPlusPrice();
                        }
                    }

                    // foreach price type summarize price
                    foreach ($calculationKeys as $key => $rec) {
                        $price = (float) 0;

                        if (!is_string($key)) {
                            $key = $rec;
                        }

                        $range = $this->getRange($rec);

                        $calculationFieldItemPrice = $calculationFieldItem->getPrice($key, $range);

                        if ($calculationFieldItemPrice) {
                            if ($type === 'Cost') {
                                $price = $calculationFieldItemPrice->getCost();
                            } else {
                                $price = $calculationFieldItemPrice->$typeMethod();

                                // if price range is percent then gets default price and multiplies by this percent
                                if ($calculationFieldItemPrice->getPriceType() == self::$percentType) {
                                    $defaultCalculationFieldItem = $calculationField->get($defaultPriceListId);

                                    $defaultItemPrice = ($defaultCalculationFieldItem) ? $defaultCalculationFieldItem->getPrice(
                                        $key,
                                        $range
                                    ) : (float) 0;

                                    $defaultPrice = ($defaultItemPrice) ? $defaultItemPrice->$typeMethod() : (float) 0;

                                    $price *= $defaultPrice / 100;
                                }
                            }

                        }

                        $sumPrice += ($range * $price);
                    }
                }
            }
        }

        // check min/max condition
        if ($calculationFieldItem && $type == 'Price') {
            if ($calculationFieldItem->getPriceMinimal() > 0 && $sumPrice < $calculationFieldItem->getPriceMinimal()) {
                $sumPrice = $calculationFieldItem->getPriceMinimal();
            }

            if ($calculationFieldItem->getPriceMaximal() > 0 && $sumPrice > $calculationFieldItem->getPriceMaximal()) {
                $sumPrice = $calculationFieldItem->getPriceMaximal();
            }
        }

        // returns final component price
        $price = round($sumPrice, 2);

        return $price;
    }

    /**
     * Get list of available calculation type for component
     *
     * @param  string $componentName
     * @return array
     */
    private function getCalculationTypes($componentName)
    {
        if (preg_match('/^OTHER_/', $componentName)) {
            $calculationKeys = self::$priceTypeWithoutSimplexAndDuplexArr;
        } else {
            switch ($componentName) {
                case 'MATERIAL':
                    $calculationKeys = array($this->measureType);
                    break;
                case 'PRINT':
                    $calculationSide = $this->sides;

                    $priceType = ($calculationSide->getName() == 'Single') ? self::$price_simplex : self::$price_duplex
                    ;
                    $calculationKeys = array(
                        $priceType => $this->measureType
                    );
                    break;
            }
        }

        return $calculationKeys;
    }

    /**
     * Get Calculation field object
     *
     * @param $componentName
     * @return CalculationField|null
     */
    private function getCalculationField($componentName)
    {
        $calculationField = $this->getCalculationProductData()
                ->getField($componentName);

        return $calculationField;
    }

    /**
     * Get Calculation field item object
     *
     * @param  string                    $componentName
     * @return bool|CalculationFieldItem
     */
    private function getCalculationFieldItem($componentName)
    {
        if (preg_match('/^OTHER_/', $componentName)) {
            preg_match('/^OTHER_(.*)$/', $componentName, $match);

            $priceKey = (int) $this->other[$match[1]]['value'];
            $calculationField = $this->getCalculationField($componentName)->get($priceKey);
        } else {
            switch ($componentName) {
                case 'MATERIAL':
                    $calculationField = $this->material;
                    break;
                case 'PRINT':
                    $calculationField = $this->print;
                    break;
            }
        }

        return $calculationField;
    }

    /**
     * Get range by name
     *
     * @param  string $name
     * @return float
     */
    private function getRange($name)
    {
        $methodName = 'get' . self::camelize($name);
        $range = $this->$methodName();

        return $range;
    }

    public static function camelize($id)
    {
        return strtr(ucwords(strtr($id, array('_' => ' ', '.' => '_ ', '\\' => '_ '))), array(' ' => ''));
    }

    /**
     * @return mixed
     */
    public function getCostMeasureType()
    {
        return $this->costMeasureType;
    }

    /**
     * Returns summary price.
     *
     * @return integer
     */
    protected function getSummaryPriceNet()
    {
        //if empty try calculate
        if ($this->summaryPriceNet == 0) {
            $this->calculateComponentPriceArray();
        }

        return round($this->summaryPriceNet, 2);
    }

    /**
     * Returns summary price.
     *
     * @return integer
     */
    protected function getSummaryPriceGross()
    {
        $netPrice = $this->getSummaryPriceNet();

        return round($netPrice + ($netPrice * $this->priceTax), 2);
    }

    /**
     * Returns fieldset label by $type
     *
     * @param  string     $type
     * @return string     ('material', 'count', 'quantity', 'print', 'size', 'sides')
     * @throws \Exception
     */
    protected function getFieldsetLabel($type)
    {
        $availableTypes = array('material', 'count', 'quantity', 'print', 'size', 'sides');

        if (!in_array($type, $availableTypes)) {
            throw new \Exception('Wrong field type');
        }

        $type = strtoupper($type);

        return $this->getCalculationProductData()->getField($type)->getLabel();
    }

    /**
     * Get fixed price for product object
     *
     * @return float|bool
     */
    private function getFixedPrice()
    {
        $calculationProductData = $this->getCalculationProductData();
        $priceListFixPriceCollection = $calculationProductData->getPriceListFixPriceCollection();

        $fixedPrice = $priceListFixPriceCollection->get($this->pricelistId);

        if (!$fixedPrice instanceof PriceListFixPrice) {
            $fixedPrice = $priceListFixPriceCollection->get(
                    $calculationProductData->getDefaultPriceListId()
            );
        }

        if (!$fixedPrice instanceof PriceListFixPrice) {
            return false;
        }

        return $fixedPrice->get($this->quantity)->getPrice();
    }

    /**
     * Calculates all components array.
     */
    private function calculateComponentPriceArray()
    {
        $calculationProductData = $this->getCalculationProductData();

        $this->summaryPriceNet = (float) 0;

        $materialPriceItem = new CalculationToolPriceItem();
        $materialPriceItem->setType('MATERIAL');
        $materialPriceItem->setName('MATERIAL');
        $materialPriceItem->setFieldLabel($this->material->getLabel());
        $materialPriceItem->setLabel($this->getFieldsetLabel('material'));

        $printPriceItem = new CalculationToolPriceItem();
        $printPriceItem->setType('PRINT');
        $printPriceItem->setName('PRINT');
        $printPriceItem->setFieldLabel($this->print->getLabel());
        $printPriceItem->setLabel($this->getFieldsetLabel('print'));

        // checks fixed price for current pricelist
        if ($this->getCalculationProductData()->hasFixedPrice()) {
            $this->fixedPrice = true;

            $materialPrice = (float) 0;
            $printPrice = ($this->getFixedPrice() * $this->quantity);
        }
        // otherwise calculate normally
        else {
            $this->fixedPrice = false;

            $materialPrice = $this->getComponentPrice('MATERIAL');
            $printPrice = $this->getComponentPrice('PRINT');
        }

        // gets material and print cost
        $materialCost = $this->getComponentCost('MATERIAL');
        $printCost = $this->getComponentCost('PRINT');

        $materialPriceItem->setPrice($materialPrice);
        $materialPriceItem->setCost($materialCost);

        $printPriceItem->setPrice($printPrice);
        $printPriceItem->setCost($printCost);

        $this->report->addPriceItem($materialPriceItem);
        $this->report->addPriceItem($printPriceItem);

        $this->summaryPriceNet += $materialPrice;
        $this->summaryPriceNet += $printPrice;

        //OTHER's
        if ($this->other) {
            $emptyFieldId = $this->calculationProductData->getEmptyFieldId();
            $otherFinishLabel = $calculationProductData->getOtherFinishLabel();
            foreach ($this->other as $key => $rec) {
                $otherKey = 'OTHER_' . $key;
                $calculationField = $this->getCalculationField($otherKey);
                $calculationFieldItem = $this->getCalculationFieldItem($otherKey);

                $calculationFieldItemLabel = ($calculationFieldItem) ? $calculationFieldItem->getName() : '-';

                // if exist
                if (array_key_exists('value', $rec) && (!empty($rec['value'])) && $rec['value'] != $emptyFieldId) {
                    $value = is_numeric($rec['value']) ? $rec['value'] : 0;

                    $otherPriceItem = new CalculationToolOtherPriceItem();
                    $otherPriceItem->setName('OTHER');
                    $otherPriceItem->setPrice($this->getComponentPrice($otherKey));
                    $otherPriceItem->setCost($this->getComponentCost($otherKey));
                    $otherPriceItem->setLabel($otherFinishLabel);
                    $otherPriceItem->setFieldLabel($calculationFieldItemLabel);
                    $otherPriceItem->setHidden((bool) $calculationField->getHidden());
                    $otherPriceItem->setKey($key);
                    $otherPriceItem->setId($value);
                    $otherPriceItem->setType('OTHER');

                    $this->report->addOtherPriceItem($otherPriceItem);

                    $this->summaryPriceNet += $otherPriceItem->getPrice();
                }
            }
        }
    }

    /**
     * @return \CalculationProductData
     */
    private function getCalculationProductData()
    {
        return $this->calculationProductData;
    }

    protected function isSquareMetterMesureType()
    {
        return in_array($this->measureType, array('price_linear_metre', 'price_square_metre'));
    }

    /**
     * Returns prepared field array.
     *
     * @param  array      $defaultFields
     * @param  array      $inputArray
     * @return array      returns prepared field array after merging with default values
     * @throws \Exception
     */
    private function prepareFieldArray($defaultFields, array $inputArray)
    {
        //combine default values and values which are sent
        $fieldArray = self::mergeOrderDefaultFieldsetArrays($defaultFields, $inputArray);

        if (!array_key_exists('SIZE', $inputArray)) {
            throw new \Exception('Input field "SIZE" required.');
        }

        if (!array_key_exists('width', $inputArray['SIZE']) || !array_key_exists('height', $inputArray['SIZE'])) {
            return $fieldArray;
        }

        // sets SIZE fields
        $fieldArray['SIZE']['width']['value'] = $inputArray['SIZE']['width']['value'];
        $fieldArray['SIZE']['height']['value'] = $inputArray['SIZE']['height']['value'];
        $fieldArray['SIZE']['metric']['value'] = (isset($inputArray['SIZE']['metric']['value'])) ? $inputArray['SIZE']['metric']['value'] : $this->sizeMetric
        ;

        return $fieldArray;
    }

    /**
     * Returns array of fieldset after merging
     * 2 dimensional array include fields:
     * - name
     * - value
     *
     * This array is ready to calculate check process.
     *
     * @param  array $defaultFields
     * @param  array $fields
     * @return array
     */
    public static function mergeOrderDefaultFieldsetArrays(array $defaultFields = array(), array $fields = array())
    {
        foreach ($defaultFields as $key => &$rec) {
            foreach ($fields as $key2 => $rec2) {
                if ($key == 'OTHER') {
                    continue;
                }
                if (empty($rec2['value'])) {
                    continue;
                }
                //if sent name match - overwrite default value
                if ($rec['name'] == $rec2['name']) {
                    $rec['value'] = $rec2['value'];
                }
            }

            //if still empty - unset from array
            if (empty($rec['value'])) {
                unset($defaultFields[$key]);
            }
        }

        //add other elements
        if (key_exists('OTHER', $fields)) {
            $defaultFields['OTHER'] = $fields['OTHER'];
        }

        return $defaultFields;
    }

}
