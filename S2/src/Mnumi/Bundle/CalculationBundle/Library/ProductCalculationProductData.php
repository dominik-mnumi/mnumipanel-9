<?php
namespace Mnumi\Bundle\CalculationBundle\Library;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductCalculationProductData class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ProductCalculationProductData
{
    /**
     * @var Product
     */
    private $product;
    private $om;
    public $priceTax;

    public function __construct($om, \Mnumi\Bundle\ProductBundle\Entity\Product $product, $defaultPriceTax)
    {
        $this->product = $product;
        $this->om = $om;

        $priceTax = $product->getTaxValue();

        if ($priceTax == null) {
            $priceTax = $defaultPriceTax;
        }
        $this->priceTax = (float) $priceTax / 100;
    }

    public function getCalculationProductDataObject()
    {
        $databaseProductData = new DoctrineProductCalculationProductData($this->om, $this->product, $this->priceTax);
        $data = $databaseProductData->getCalculationProductDataObject();

        return $data;
    }
}
