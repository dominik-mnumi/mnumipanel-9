<?php
namespace Mnumi\Bundle\CalculationBundle\Library\FieldItem;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FieldItemPrice class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationFieldItemPrice
{
    /** @var string Field item price type name */
    private $type;

    /** @var float */
    private $quantity;

    /** @var float */
    private $price;

    /** @var string */
    private $priceType = 'amount';

    /** @var float */
    private $cost;

    public function __construct($type, $quantity, $price, $cost, $priceType = 'amount')
    {
        $this->type = $type;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->cost = $cost;
        $this->priceType = $priceType;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return (float) $this->quantity;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return (float) $this->price;
    }

    /**
     * @return string
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return (float) $this->cost;
    }

}
