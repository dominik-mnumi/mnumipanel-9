<?php
namespace Mnumi\Bundle\CalculationBundle\Library\FieldItem;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Field class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationField implements \Countable, \IteratorAggregate, \Serializable
{
    /** @var string */
    private $type;

    /** @var string */
    private $label;

    /**
     * @var CalculationFieldItem[]
     */
    private $data;

    /**
     * @var bool
     */
    private $hidden;

    /**
     * @param string $type
     * @param string $label
     * @param bool   $hidden
     */
    public function __construct($type, $label, $hidden)
    {
        $this->type = $type;
        $this->label = $label;
        $this->hidden = $hidden;
    }

    /**
     * Add product field item to collection
     *
     * @param CalculationFieldItem $record
     */
    public function add(CalculationFieldItem $record)
    {
        $primaryKey = $record->getId();

        $this->data[$primaryKey] = $record;
    }

    /**
     * Get product field item by ID
     *
     * @param  int                        $primaryKey
     * @return bool|\CalculationFieldItem
     */
    public function get($primaryKey)
    {
        if (!array_key_exists($primaryKey, $this->data)) {
            return false;
        }

        return $this->data[$primaryKey];

    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }
    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     *                     <b>Traversable</b>
     */
    public function getIterator()
    {
        $data = $this->data;

        return new \ArrayIterator($data);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        $vars = get_object_vars($this);

        return serialize($vars);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param  string $serialized <p>
     *                            The string representation of the object.
     *                            </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $array = unserialize($serialized);

        foreach ($array as $name => $values) {
            $this->$name = $values;
        }
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     *             </p>
     *             <p>
     *             The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->data);
    }
}
