<?php
namespace Mnumi\Bundle\CalculationBundle\Library;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PriceListFixPriceCollection class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class PriceListFixPriceCollection implements \Countable, \IteratorAggregate, \Serializable
{
    /**
     * @var PriceListFixPrice[] $data an array containing the PriceListFixPrice collection
     */
    protected $data = array();

    /**
     * Adds a PriceListFixPrice record to collection
     *
     * @param  PriceListFixPrice    $record record to be added
     * @throws OutOfBoundsException
     * @return boolean
     */
    public function add(PriceListFixPrice $record)
    {
        $primaryKey = $record->getPriceListId();

        if ($this->contains($primaryKey)) {
            throw new \OutOfBoundsException("Pricelist " . $primaryKey. " already exist");
        }

        $this->data[$primaryKey] = $record;
    }

    /**
     * Checks whether an element by Price List Id is contained in the collection
     *
     * @param  int  $primaryKey PriceList ID
     * @return bool
     */
    public function contains($primaryKey)
    {
        return isset($this->data[$primaryKey]);
    }

    /**
     * Get element by Price List ID
     *
     * @param  int                    $primaryKey Price List ID
     * @return PriceListFixPrice|bool
     */
    public function get($primaryKey)
    {
        if (!$this->contains($primaryKey)) {
            return null;
        }

        return $this->data[$primaryKey];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     *                     <b>Traversable</b>
     */
    public function getIterator()
    {
        $data = $this->data;

        return new \ArrayIterator($data);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        $vars = get_object_vars($this);

        return serialize($vars);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param  string $serialized <p>
     *                            The string representation of the object.
     *                            </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $array = unserialize($serialized);

        foreach ($array as $name => $values) {
            $this->$name = $values;
        }
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     *             </p>
     *             <p>
     *             The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->data);
    }
}
