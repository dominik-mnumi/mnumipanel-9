<?php
namespace Mnumi\Bundle\CalculationBundle\Library;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
use Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationField;
use Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationFieldItem;
use Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationFieldItemPrice;
use Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationFieldItemPriceCollection;
use Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationFieldItemSize;
use Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationPrintSize;
/**
 * DoctrineProductCalculationProductData class
 * Prepare CalculationProductData based on Doctrine 2.x Product entity
 *
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
class DoctrineProductCalculationProductData
{
    private $om;
    private $product;
    public $priceTax;

    public function __construct($om, \Mnumi\Bundle\ProductBundle\Entity\Product $product, $priceTax)
    {
        $this->product = $product;
        $this->om = $om;
        $this->priceTax = $priceTax;
    }

    /**
     * @return CalculationProductData
     */
    public function getCalculationProductDataObject()
    {
        $defaultPriceListId = $this->getRepository('MnumiCalculationBundle:PriceList')->findOneBy(array('name' => 'Default'))->getId();
        $emptyFieldId = $this->getRepository('MnumiProductBundle:FieldItem')->findOneBy(array('name' => '--------'))->getId();
        $otherFinishLabel = $this->getRepository('MnumiProductBundle:Fieldset')->findOneBy(array('name' => 'OTHER'))->getLabel();
        $defaultFields = $this->getProductFieldsets();

        $calculationProductData = new CalculationProductData($defaultFields, $defaultPriceListId, $emptyFieldId, $otherFinishLabel, $this->priceTax);

        $calculationProductData = $this->prepareFields($calculationProductData);
        $calculationProductData = $this->prepareFixedPrice($calculationProductData);

        return $calculationProductData;
    }

    private function prepareFixedPrice(CalculationProductData $calculationProductData)
    {
        $priceListCollection = new PriceListFixPriceCollection();
        $calculationProductData->setPriceListFixPriceCollection($priceListCollection);

        /** @var ProductFixprice $price */
        foreach ($this->getProduct()->getFixprice() as $price) {
            $priceListId = $price->getPricelist()->getId();

            if ($priceListCollection->contains($priceListId)) {
                $priceListFixPrice = $priceListCollection->get($priceListId);
            } else {
                $priceListFixPrice = new PriceListFixPrice($priceListId);
                $priceListCollection->add($priceListFixPrice);
            }

            $priceListFixPrice->add(
                new FixPrice($price->getQuantity(), $price->getPrice(), $price->getCost())
            );
        }

        return $calculationProductData;
    }

    private function prepareFields(CalculationProductData $calculationProductData)
    {
        $fields = $this->getProduct()->getFields();
        $productFields = array();
        $productFieldsIds = array();

        foreach ($fields as $field) {
            $productFieldsIds[$field->getId()] = $field;
            $productFields[$field->getFieldset()->getName()] = $field;
        }

        /** @var ProductField $field */
        foreach ($fields as $field) {
            $type = $field->getFieldset()->getName();

            if (in_array($type, array('GENERAL', 'FIXEDPRICE', 'WIZARD'))) {
                continue;
            }

            $label = $field->getLabel();

            $fieldType = ($type == 'OTHER') ? 'OTHER_' . $field->getId() : $type;

            $calculationField = new CalculationField($fieldType, $label, !$field->getVisible());
            $calculationProductData->addField($calculationField);

            if (in_array($type, array('COUNT', 'QUANTITY'))) {
                continue;
            }

            if ($type == 'OTHER') {
                $fieldItems = $productFieldsIds[$field->getId()]->getFieldItems();
//                $otherId++;
            } else {
                $fieldItems = $productFields[$type]->getFieldItems();
            }

            /** @var ProductFieldItem $productItem */
            foreach ($fieldItems as $fieldItem) {
                $fieldItem = $fieldItem->getFieldItem();

                $calculationFieldItem = new CalculationFieldItem(
                    $fieldItem->getId(),
                    $fieldItem->getName(),
                    $fieldItem->getLabel(),
                    $fieldItem->getCost(),
                    $fieldItem->getMeasureUnit()->getName()
                );

                $calculationField->add($calculationFieldItem);

                if ($type == 'SIZE') {
                    /** @var FieldItemSize $fieldItemSize */
                    $fieldItemSize = $fieldItem->getSize()->first();

                    if (!$fieldItemSize) {
                        $fieldItemSize = $fieldItem->getSize()->last();
                    }

                    if ($fieldItemSize) {
                        $calculationFieldItemSize = new CalculationFieldItemSize(
                            $fieldItemSize->getWidth(),
                            $fieldItemSize->getHeight()
                        );

                        $calculationFieldItem->setItemSize($calculationFieldItemSize);
                    }
                }

                if ($type == 'MATERIAL') {
                    /** @var FieldItemMaterial $fieldItemMaterial */
                    $fieldItemMaterial = $fieldItem->getMaterial()->first();

                    if ($fieldItemMaterial) {
                        $printSize = $fieldItemMaterial->getPrintSize();

                        if (!$printSize instanceof \Mnumi\Bundle\ProductBundle\Entity\PrintSize) {
                            throw new Exception('Print Size obj object does not exist');
                        }

                        $calculationPrintSize = new CalculationPrintSize(
                            $printSize->getWidth(),
                            $printSize->getHeight(),
                            $printSize->getId()
                        );

                        $calculationFieldItem->setPrintSize($calculationPrintSize);
                    }
                }

                /** @var FieldItemPrice $fieldItemPrice */
                foreach ($fieldItem->getPrice() as $fieldItemPrice) {
                    $calculationFieldCollection = new CalculationFieldItemPriceCollection(
                        $fieldItemPrice->getPricelist(),
                        $fieldItemPrice->getMinimalPrice(),
                        $fieldItemPrice->getMaximalPrice(),
                        $fieldItemPrice->getPlusPrice(),
                        $fieldItemPrice->getPlusPriceDouble(),
                        $fieldItemPrice->getCost()
                    );
                    $calculationFieldItem->add($calculationFieldCollection);

                    /** @var FieldItemPriceQuantity $quantity */
                    foreach ($fieldItemPrice->getQuantity() as $quantity) {
                        $calculationFieldCollection->addPrice(new CalculationFieldItemPrice(
                                $quantity->getFieldItemPriceTypeName()->getName(),
                                $quantity->getQuantity(),
                                $quantity->getPrice(),
                                $quantity->getCost(),
                                $quantity->getType()
                            )
                        );
                    }
                }
            }
        }

        return $calculationProductData;
    }

    public function getProductFieldsets()
    {
        $productFieldsetColl = $this->getProduct()->getFields();

        //creates default array
        $defaultFields = array();
        $otherIndex = 0;
        foreach ($productFieldsetColl as $rec) {
            if ($rec->getFieldset()->getName() != "GENERAL") {
                $fieldsetName = $rec->getFieldset()->getName();

                $item = array(
                    'name' => $fieldsetName,
                    'value' => $rec->getDefaultValue(),
                );

                //other should not be in default array because it is not mandatory
                if ('OTHER' == $fieldsetName) {
                    $defaultFields['OTHER'][$otherIndex] = $item;
                    $otherIndex++;
                } else {
                    $defaultFields[$fieldsetName] = $item;
                }
            }
        }

        return $defaultFields;
    }

    /**
     * @return \Product
     */
    protected function getProduct()
    {
        return $this->product;
    }

    private function getRepository($class)
    {
        return $this->om->getRepository($class);
    }
}
