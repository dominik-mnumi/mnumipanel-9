<?php

namespace Mnumi\Bundle\CalculationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pricelist
 *
 * @ORM\Table(name="pricelist", uniqueConstraints={@ORM\UniqueConstraint(columns={"name"})})
 * @ORM\Entity
 */
class PriceList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="changeable", type="boolean", nullable=false)
     */
    private $changeable = true;

    /**
     * Set name
     *
     * @param  string    $name
     * @return Pricelist
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set changeable
     *
     * @param  boolean   $changeable
     * @return Pricelist
     */
    public function setChangeable($changeable)
    {
        $this->changeable = $changeable;

        return $this;
    }

    /**
     * Get changeable
     *
     * @return boolean
     */
    public function getChangeable()
    {
        return $this->changeable;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
