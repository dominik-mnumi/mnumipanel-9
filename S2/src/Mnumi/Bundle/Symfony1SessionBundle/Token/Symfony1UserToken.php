<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\Symfony1SessionBundle\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;


/**
 * Symfony1UserToken class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class Symfony1UserToken extends AbstractToken
{
    public function __construct(array $roles = array())
    {
        parent::__construct($roles);

        // If the user has roles, consider it authenticated
        $this->setAuthenticated(count($roles) > 0);
    }

    /**
     * Returns the user credentials.
     *
     * @return mixed The user credentials
     */
    public function getCredentials()
    {
        return $this->getRoles();
    }

    /**
     * Get user id
     *
     * @return int
     */
    public function getUserId()
    {
        return str_replace('#', '', $this->getUser());
    }
}