<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\Symfony1SessionBundle\Security\Firewall;

use Mnumi\Bundle\Symfony1SessionBundle\Token\Symfony1UserToken;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

/**
 * Symfony1Request class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class Symfony1Listener implements ListenerInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var AuthenticationManagerInterface
     */
    private $authenticationManager;

    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function handle(GetResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            // don't do anything if it's not the master request
            return;
        }

        /** @var SessionInterface $session */
        $session = $event->getRequest()->getSession();
        $session->start();

        if(!$this->getUserId()) {
            $this->tokenStorage->setToken(null);

            return;
        }

        $unauthenticatedToken = new Symfony1UserToken(
            $this->getCredentials()
        );
        $unauthenticatedToken->setUser('#'.$this->getUserId());

        try {
            $token = $this->authenticationManager->authenticate($unauthenticatedToken);
            $this->tokenStorage->setToken($token);
            return;
        }  catch (AuthenticationException $failed) {
            $response = new Response();
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            $event->setResponse($response);

        }

        // By default deny authorization
        $response = new Response();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);
        $event->setResponse($response);
    }

    /**
     * Load credentials from Session
     *
     * @return array
     */
    private function getCredentials()
    {
        return $this->get("symfony/user/sfUser/credentials");
    }

    /**
     * Get User ID from session
     *
     * @return int|null
     */
    private function getUserId()
    {
        $attribute = $this->get("symfony/user/sfUser/attributes");

        return isset($attribute['sfGuardSecurityUser']['user_id'])
            ? (int) $attribute['sfGuardSecurityUser']['user_id']
            : null
        ;
    }

    /**
     * Load session data by key
     *
     * @param  string $key
     * @return mixed
     */
    private function get($key)
    {
        $session = isset($_SESSION) ? $_SESSION : array();

        return array_key_exists($key, $session) ? $session[$key] : null;
    }
}
