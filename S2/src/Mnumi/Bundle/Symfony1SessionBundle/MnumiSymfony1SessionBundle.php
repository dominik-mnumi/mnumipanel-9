<?php

namespace Mnumi\Bundle\Symfony1SessionBundle;

use Mnumi\Bundle\Symfony1SessionBundle\Factory\Symfony1Factory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MnumiSymfony1SessionBundle extends Bundle
{
    /**
     * Build method
     *
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new Symfony1Factory());
    }
}
