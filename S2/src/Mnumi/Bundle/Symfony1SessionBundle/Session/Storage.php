<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\Symfony1SessionBundle\Session;

use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;

/**
 * Symfony1SessionStorage class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class Storage extends PhpBridgeSessionStorage
{

}
