<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Tests;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * RestWebTestCase class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class RestWebTestCase extends WebTestCase
{
    /** @var Client */
    public $client;

    public function setUp()
    {
        $auth = array(
            'PHP_AUTH_USER' => $this->getWebApiKey(),
            'PHP_AUTH_PW'   => $this->getWebApiKey(),
        );
        $this->client = static::createClient(array(), $auth);
        $this->runCommand("doctrine:schema:drop", array("--force" => true));
        $this->runCommand("doctrine:schema:create");
        $this->runCommand("cache:warmup");
        //$this->runCommand("doctrine:fixtures:load", array("-n" => true));
    }

    private function getWebApiKey()
    {
        return '9LDAvTuD7i53Ef5yXEOabAvt';
    }

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
