<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Tests\Controller;

use Mnumi\Bundle\RestServerBundle\Tests\RestWebTestCase;

/**
 * OrderControllerTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class WizardControllerTest extends RestWebTestCase
{
    public function testGetWizardAction()
    {
        $client = $this->getClient();
        $this->runCommand("doctrine:fixtures:load", array("--no-interaction" => true));

        $crawler = $client->request(
                'GET', '/api/products/calendar-12-pages/wizard.json'
        );

        $response = $client->getResponse();
        // Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());
    }
}
