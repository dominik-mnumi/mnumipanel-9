<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Tests\Controller;

use Mnumi\Bundle\RestServerBundle\Tests\RestWebTestCase;

/**
 * OrderControllerTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class UserControllerTest extends RestWebTestCase
{
    public function testPostUserAction()
    {
        $client = $this->getClient();
        $this->loadFixtures(array('Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadApiData'));

        $crawler = $client->request(
                'POST', '/api/users.json',
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => "application/json"),
                '{"firstName":"Test","lastName":"test","emailAddress":"dominik.labudzinski@mnumi.com","username":"dominik.labudzinski@mnumi.com","salt":"a7520e1f6ffe03e7664101cfdef0ca20","password":"2450f01e47d23079b66b3edff22bcfe4"}'
        );

        $response = $client->getResponse();
           print_r($response);
        // Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());
    }

}
