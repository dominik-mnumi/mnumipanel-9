<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Tests\Controller;

use Mnumi\Bundle\RestServerBundle\Tests\RestWebTestCase;

/**
 * OrderControllerTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class PaymentControllerTest extends RestWebTestCase
{
    public function testPostPaymentsAction()
    {
        $client = $this->getClient();
        $this->loadFixtures(array('Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadApiData'));

        $crawler = $client->request(
                'POST', '/api/payments.json',
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => "application/json"),
                '{"name":"Test","label":"Prestashop: Test"}'
        );

        $response = $client->getResponse();
        // Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());
    }

    public function testGetPaymentsAction()
    {
        $client = $this->getClient();
        $this->loadFixtures(
                array('Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadApiData',
                    'Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadPaymentsData'
                )
        );

        $crawler = $client->request(
                'GET', '/api/payments.json'
        );

        $response = $client->getResponse();

        //   Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());

        $data = json_decode($response->getContent(), true);

        $this->assertTrue(count($data) >= 1);

        foreach ($data as $order) {
            // Assert that the "slug", "name" exist in the product data
            $this->assertArrayHasKey('id', $order);
            $this->assertArrayHasKey('name', $order);
            $this->assertArrayHasKey('label', $order);
        }
    }

}
