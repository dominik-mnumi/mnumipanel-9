<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Tests\Controller;

use Mnumi\Bundle\RestServerBundle\Tests\RestWebTestCase;

/**
 * OrderControllerTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ClientControllerTest extends RestWebTestCase
{
    public function testPostClientAction()
    {
        $client = $this->getClient();
        $this->loadFixtures(
            array(
                'Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadApiData',
                'Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadUserData'
            )
        );

        $crawler = $client->request(
                'POST', '/api/clients.json',
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => "application/json"),
                '{"client":{"fullname":"test2","city":"Warszawa","postcode":"00-000","street":"Kaskadowa 8","country":"PL","creditLimitAmount":"0","users":{"1":{"user":"testtest"}}}}'
        );

        $response = $client->getResponse();
        var_dump($response);
        // Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());
    }

}
