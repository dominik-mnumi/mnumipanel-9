<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Tests\Controller;

use Mnumi\Bundle\RestServerBundle\Tests\RestWebTestCase;

/**
 * OrderControllerTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class OrderControllerTest extends RestWebTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->runCommand("doctrine:fixtures:load", array("--no-interaction" => true));
    }

    public function testPostAction()
    {
        $client = $this->getClient();

        $crawler = $client->request(
                'POST',
                '/api/orders.json',
                array(),
                array(),
                array(
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => "application/json"
                ),
                '{"order":{
                    "deliveryName":"test",
                    "deliveryStreet":"Kaskadowa 8",
                    "deliveryCity":"Warszawa",
                    "deliveryPostcode":"00-001",
                    "deliveryCountry":"PL",
                    "client":"Test test",
                    "user":"dominik.labudzinski@mnumi.com",
                    "wantInvoice":false,
                    "paymentStatusName":"paid",
                    "payment":"PayU",
                    "carrier":"UPS",
                    "items":[{
                        "name":"TestCase",
                        "priceNet":"353.94",
                        "totalAmount":"353.94",
                        "shopName":"D7HN9x7klrjXMH00Pl754FZ",
                        "product":"calendar-12-pages",
                          "attributes":[
                            {"field":"COUNT","value":"12"},
                            {"field":"QUANTITY","value":"1"},
                            {"field":"SIZE","value":"7"},
                            {"field":"MATERIAL","value":"1"},
                            {"field":"PRINT","value":"11"},
                            {"field":"SIDES","value":"3"},
                            {"field":"OTHER","value":"20"},
                            {"field":"OTHER","value":"17"}
                          ],
                          "customSize":null
                    }]
                  }
                }'
        );

        $response = $client->getResponse();
        // Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());

        $post = json_decode($response->getContent(), true);

        $client = $this->getClient();
        $crawler = $client->request(
                'GET', '/api/orders/' . $post['id'] . '.json'
        );

        $response = $client->getResponse();

        //   Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());

        $order = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('id', $order);
        $this->assertArrayHasKey('delivery_street', $order);

        $this->assertArrayHasKey('items', $order);

        $this->assertTrue(count($order['items']) >= 1);
        foreach ($order['items'] as $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('name', $item);

            $this->assertArrayHasKey('attributes', $item);

            $this->assertTrue(count($item['attributes']) >= 1);
            foreach ($item['attributes'] as $attribute) {
                $this->assertArrayHasKey('id', $attribute);
                $this->assertArrayHasKey('value', $attribute);
                $this->assertArrayHasKey('field', $attribute);
            }
        }

    }

    public function testGetOrdersAction()
    {
        $client = $this->getClient();
        $crawler = $client->request(
                'GET', '/api/orders.json'
        );

        $response = $client->getResponse();

        //   Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());

        $data = json_decode($response->getContent(), true);

        $this->assertTrue(count($data) >= 1);

        foreach ($data as $order) {

            // Assert that the "slug", "name" exist in the product data
            $this->assertArrayHasKey('id', $order);
            $this->assertArrayHasKey('delivery_street', $order);
        }
    }

}
