<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Tests\Controller;

use Mnumi\Bundle\RestServerBundle\Tests\RestWebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * OrderControllerTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ProductControllerTest extends RestWebTestCase
{
    public function testGetAction()
    {
        $client = $this->getClient();

        $key = $this->getWebApiKey();

        $crawler = $client->request(
            'GET',
            'http://mnumipanel.loc/app_dev.php/api/products/calendar-12-pages.json',
            array('key' => $key,),
            array(),
            array('CONTENT_TYPE' => 'application/json')
        );

        $response = $client->getResponse();

        // Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());

        // Assert that the "Content-Type" header is "application/json"
        $this->assertTrue(
            $response->headers->contains(
                'Content-Type',
                'application/json'
            )
        );

        $data = json_decode($response->getContent(), true);

        // Assert that response is correct JSON
        $this->assertTrue($data !== false);

        // Assert that the "product" exist in the data
        $this->assertArrayHasKey('product', $data);

        $productData = $data['product'];

        // Assert that the "slug", "name" exist in the product data
        $this->assertArrayHasKey('slug', $productData);
        $this->assertArrayHasKey('name', $productData);
    }

    private function getWebApiKey()
    {
        return '2xsSF3oMTP5I3uZdER4DJxji9tr';
    }
}
