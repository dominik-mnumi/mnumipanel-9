<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Tests\Controller;

use Mnumi\Bundle\RestServerBundle\Tests\RestWebTestCase;

/**
 * OrderControllerTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CarrierControllerTest extends RestWebTestCase
{
    public function testPostCarrierAction()
    {
        $client = $this->getClient();
        $this->loadFixtures(array('Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadApiData'));

        $crawler = $client->request(
                'POST', '/api/carriers.json',
                array(),
                array(),
                array('CONTENT_TYPE' => 'application/json', 'HTTP_ACCEPT' => "application/json"),
                '{"name":"Test Dev","label":"Prestashop: Test Dev","deliveryTime":"Odbiór w sklepie"}'
        );

        $response = $client->getResponse();
        // Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());
    }

    public function testGetCarriersAction()
    {
        $client = $this->getClient();
        $this->loadFixtures(
                array('Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadApiData',
                    'Mnumi\Bundle\RestServerBundle\DataFixtures\ORM\LoadCarriersData'
                )
        );

        $crawler = $client->request(
                'GET', '/api/carriers.json'
        );

        $response = $client->getResponse();

        //   Assert that the response status code is 2xx
        $this->assertTrue($response->isSuccessful());

        $data = json_decode($response->getContent(), true);

        $this->assertTrue(count($data) >= 1);

        foreach ($data as $order) {
            // Assert that the "slug", "name" exist in the product data
            $this->assertArrayHasKey('id', $order);
            $this->assertArrayHasKey('name', $order);
            $this->assertArrayHasKey('label', $order);
        }
    }

}
