<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Security;

use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\RestServerBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * ApiKeyUserProvider class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ApiKeyUserProvider implements UserProviderInterface
{
    /** @var \Doctrine\Common\Persistence\ObjectManager $em */
    private $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function getUsernameForApiKey($apiKey)
    {
        $webapiKey = $this->em->getRepository("MnumiRestServerBundle:WebapiKey")->find($apiKey);

        if (!$webapiKey) {
            throw new UsernameNotFoundException('Invalid API key.');
        }

        return 'admin';
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @see UsernameNotFoundException
     *
     * @return \Symfony\Component\Security\Core\User\UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     *
     */
    public function loadUserByUsername($username)
    {
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        $user = $this->em->getRepository("MnumiRestServerBundle:User")
            ->findOneByUsername($username);

        if (!$user) {
            throw new UsernameNotFoundException('Count not find: ' . $username);
        }

        return $user;
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    /**
     * Whether this provider supports the given user class
     *
     * @param string $class
     *
     * @return Boolean
     */
    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}
