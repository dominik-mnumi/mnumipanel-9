<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Handler;

use Mnumi\Bundle\ClientBundle\Entity\Client;
use Mnumi\Bundle\OrderBundle\Entity\Order;
use Mnumi\Bundle\RestServerBundle\Exception\InvalidFormException;

/**
 * OrderHandler class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class OrderHandler extends ObjectHandler
{
    /**
     * Create a new Object.
     *
     * @param array $parameters
     *
     * @return Object
     */
    public function post(array $parameters)
    {
        $entityType = $this->entityType;
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->formFactory->create(
            $entityType,
            $this->createEntity(),
            array(
                'method' => 'POST',
            )
        );
        $data = $parameters[$form->getName()];

        $form->submit($data, false);
        if ($form->isValid()) {
            /** @var Order $order */
            $order = $form->getData();

            if ($data['user']) {
                $user = $this->om->createQuery('SELECT u FROM Mnumi\Bundle\RestServerBundle\Entity\User u WHERE u.username = :username')
                                ->setParameters(array(
                                    'username' => $data['user']

                                ))->getResult();
                $order->setUser($user[0]);
            } else {
                throw new InvalidFormException('The order must be placed by the user.');
            }

            if (!$data['client']) {
                throw new InvalidFormException('The order must be placed by the customer.');
            }

            /** @var Client $client */
            $client = $this->om->createQuery('SELECT c FROM Mnumi\Bundle\ClientBundle\Entity\Client c WHERE c.fullname = :fullname')
                ->setParameters(array(
                        'fullname' => $data['client']
                    ))->getResult();
            $order->setClient($client[0]);

            if ($data['payment']) {
                $payment = $this->om->createQuery('SELECT p FROM Mnumi\Bundle\PaymentBundle\Entity\Payment p WHERE p.name = :name')
                                ->setParameters(array(
                                    'name' => $data['payment']
                                ))->getResult();
                $order->setPayment($payment[0]);
            } else {
                throw new InvalidFormException('Payment method is required');
            }

            if ($data['carrier']) {
                $carrier = $this->om->createQuery('SELECT c FROM Mnumi\Bundle\CarrierBundle\Entity\Carrier c WHERE c.name = :name')
                                ->setParameters(array(
                                    'name' => $data['carrier']
                                ))->getResult();
                $order->setCarrier($carrier[0]);
            } else {
                throw new InvalidFormException('Carrier is required');
            }

            if ($data['paymentStatusName']) {
                $order->setPaymentStatusName($this->om->createQuery('SELECT ps FROM Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus ps WHERE ps.name = :name')
                                ->setParameters(array(
                                    'name' => $data['paymentStatusName']
                                ))->getSingleResult());
            } else {
                throw new InvalidFormException('Payment status is required');
            }

            foreach ($order->getItems() as $id => $item) {
                $tmp = array();
                $custom = array();
                $attrTmp = array();

                $order->removeItem($item);
                $item->setProduct($this->om->createQuery('SELECT p FROM Mnumi\Bundle\ProductBundle\Entity\Product p WHERE p.slug = :name')
                               ->setParameters(array(
                                   'name' => (string) $item->getProduct()
                               ))->getSingleResult());
                if ($item->getTaxValue() === null) {
                    $tax = $item->getProduct()->getTaxValue();

                    // if product tax is not set, use default tax value
                    if ($tax === null) {
                        $tax = $this->getTax();
                    }

                    // if client has WNT UE, set tax as 0
                    if ($order->getClient()->getWntUe()) {
                        $tax = 0;
                    }

                    $item->setTaxValue($tax);
                }

                $fields = $this->om
                    ->createQuery('SELECT pf FROM Mnumi\Bundle\ProductBundle\Entity\ProductField pf INNER JOIN pf.fieldset f '
                            . 'WHERE pf.product = :product')
                    ->setParameters(array(
                        'product' => (string) $item->getProduct()
                    ))->getResult();

                foreach ($fields as $field) {
                    $tmp[$field->getFieldset()->getName()] = $field;
                    if ($field->getFieldset()->getName() == "OTHER") {
                        $custom[$field->getId()] = $field;
                    }
                }
                $attributes = $item->getAttributes();

                foreach ($data['items'][$id]['attributes'] as $key => $attr) {
                    $attrTmp[$attr['value']] = $key;
                }
                if (!$item->getAttributes()->isEmpty()) {
                    foreach ($attributes as $attribute) {
                        if ($attribute->getField() == "GENERAL" || $attribute->getField() == "FIXEDPRICE") {
                            $item->removeAttribute($attribute);
                        } elseif ($attribute->getField() != "OTHER") {
                            if (!empty($tmp[$attribute->getField()])) {
                                $attribute->setField($tmp[$attribute->getField()]);
                            }
                        } elseif ($attribute->getField() == "OTHER") {
                            if (!empty($custom[$attrTmp[$attribute->getValue()]])) {
                                $attribute->setField($custom[$attrTmp[$attribute->getValue()]]);
                            }
                        }
                    }
                }
                $order->addItem($item);
            }
            $this->om->persist($order);
            $this->om->flush();

            return $order;
        }

        throw new InvalidFormException('Invalid submitted data: ' . (string) $form->getErrors(true, false));
    }
}
