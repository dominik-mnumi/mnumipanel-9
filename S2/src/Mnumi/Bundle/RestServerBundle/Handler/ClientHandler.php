<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Handler;

use Mnumi\Bundle\RestServerBundle\Exception\InvalidFormException;

/**
 * OrderHandler class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ClientHandler extends ObjectHandler
{
    /**
     * Create a new Object.
     *
     * @param array $parameters
     *
     * @return Object
     */
    public function post(array $parameters)
    {
        $object = $this->createEntity();
        $entityType = $this->entityType;
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->formFactory->create(
            $entityType, $object, array(
                'method' => 'POST',
            )
        );

        $data = $parameters[$form->getName()];
        foreach ($data['users'] as &$user) {
            $userTmp = $this->om->getRepository('MnumiRestServerBundle:User')->findOneBy(array('username' => $user['user']));
            if (!is_object($userTmp)) {
                throw new \Exception('Invalid user: ' . $user['user']);
            }
            $user['user'] = $userTmp;
        }
        $form->submit($data, false);

        if ($form->isValid()) {
            $object = $form->getData();
            $client = $object;
            $users = $client->getUsers();
            foreach ($users as &$user) {
                $user->setClientUserPermission(
                        $this->om->getRepository('MnumiClientBundle:ClientUserPermission')->find(1)
                );
                $user->setClient($client);
            }
            $this->om->persist($client);
            $this->om->flush();

            return $client;
        }
        throw new InvalidFormException('Invalid submitted data: ' . (string) $form->getErrors(true, false));
    }

}
