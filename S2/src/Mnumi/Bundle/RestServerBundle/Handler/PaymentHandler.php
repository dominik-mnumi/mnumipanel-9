<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Handler;
use Mnumi\PaymentBundle\DQL\ReplaceFunction as replace;

/**
 * OrderHandler class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class PaymentHandler extends ObjectHandler
{
    public function getBySlug($slug)
    {
        $em = $this->getRepository();

        $query = $em->createQueryBuilder('MnumiPaymentBundle:Payment');

        $result = $query->where("REPLACE(MnumiPaymentBundle:Payment.name,' ','') LIKE REPLACE(:name,' ','') ")
                ->setParameter('name', '%' . $slug . '%')
                ->getQuery()
                ->getOneOrNullResult();

        return $result;
    }
}
