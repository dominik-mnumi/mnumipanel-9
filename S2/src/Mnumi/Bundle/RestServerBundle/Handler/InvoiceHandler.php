<?php

/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Handler;

/**
 * InvoiceHandler class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class InvoiceHandler extends ObjectHandler
{
}
