<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Handler;

/**
 * OrderHandler class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class OrderItemHandler extends ObjectHandler
{
    /**
     * Get a list of OrderItems.
     *
     * @param int $id     id of the parent Order
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function getAll($id, $limit = 5, $offset = 0)
    {
        return $this->getRepository()->findBy(array('order' => $id), null, $limit, $offset);
    }
}
