<?php
namespace Mnumi\Bundle\RestServerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'firstname',
            'text',
                array(
                    'description' => 'User first name',
                )
            )
            ->add(
                'lastname',
                'text',
                array(
                    'description' => 'User last name',
                ))
            ->add(
                'emailAddress',
                'text',
                array(
                    'description' => 'User email address',
                ))
            ->add(
                'username',
                'text',
                array(
                    'description' => 'User name',
                ))
            ->add(
                'salt',
                'text',
                array(
                    'description' => 'Password salt',
                ))
            ->add(
                'password',
                'number',
                array(
                    'description' => 'Password',
                ))
            ->add(
                'roles',
                'collection', array(
                    'type' => new UserRoleType(),
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => false,
                    'description' => 'Role array'
                )
            )
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user';
    }
}