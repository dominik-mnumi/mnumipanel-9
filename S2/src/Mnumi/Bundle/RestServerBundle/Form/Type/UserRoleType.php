<?php
namespace Mnumi\Bundle\RestServerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserRoleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            'text',
                array(
                    'description' => 'Role name',
                )
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_role';
    }
}