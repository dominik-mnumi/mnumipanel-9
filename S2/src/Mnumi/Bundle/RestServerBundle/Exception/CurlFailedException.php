<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Exception;

/**
 * CurlFailedException class
 *
 * This exception should be used when curl operation failed for whatever reason.
 * It means, that returned error code was non-zero, or curl_exec returned false.
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class CurlFailedException extends \RuntimeException
{
    /**
     * @param $message Additional message for exception
     * @param $ch      Curl Handler, used for grabbing Curl error.
     */
    public function __construct($message, $ch)
    {
        parent::__construct(sprintf('%s: %s', $message, curl_error($ch)));
    }
}
