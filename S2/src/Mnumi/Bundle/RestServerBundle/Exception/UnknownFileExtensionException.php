<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Exception;

/**
 * UnknownFileExtensionException class
 *
 * This exception should be used when there was a problem with guessing the
 * extension.
 * It means, that there is none set in the files filename, and different methods
 * of checking it, didn't provide sensible data.
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class UnknownFileExtensionException extends \RuntimeException
{
}
