<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Mnumi\Bundle\ProductBundle\Entity\ProductField;

/**
 * @Annotations\NamePrefix("api_product_field_")
 */
class FieldController extends FOSRestController
{
    /**
     * Get fields for product.
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when not found"
     *  },
     *  requirements = {
     *      {
     *          "name"="product",
     *          "dataType"="string",
     *          "requirement"="[\w\d\-\_]+",
     *          "description"="Product slug"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "array<Mnumi\Bundle\ProductBundle\Entity\ProductField>",
     *      "groups"={"list"}
     *  }
     * )
     * @Annotations\View(
     *  templateVar="attribute"
     * )
     *
     * @Method({"GET"})
     *
     * @param $product
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return array
     */
    public function getProductFieldsAction($product, Request $request, ParamFetcherInterface $paramFetcher)
    {
        try {
            $product = $this->getDoctrine()->getManager()->getRepository('MnumiProductBundle:Product')->findOneBy(
                array('slug' => $product)
            );
            if ($product) {
                /** @var ProductField[] $fields */
                $fields = $this->getDoctrine()->getManager()->getRepository('MnumiProductBundle:ProductField')->findBy(
                    array('product' => $product)
                );
            }
        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }
        if(!$fields)
        {
            throw new NotFoundHttpException("Not found");
        }
        return $this->view($fields);
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.field.handler');
    }

}
