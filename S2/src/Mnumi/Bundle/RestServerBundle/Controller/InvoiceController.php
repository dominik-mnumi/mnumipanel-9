<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use Mnumi\Bundle\InvoiceBundle\Form\Type\InvoiceType;
use Mnumi\Bundle\OrderBundle\Entity\Order;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Annotations\NamePrefix("api_invoice_")
 */
class InvoiceController extends FOSRestController
{
    
    
    
    /**
     * Get attributes for product.
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when not found",
     *  },
     *  requirements = {
     *      {
     *          "name" = "id",
     *          "required" = "false",
     *          "dataType" = "integer",
     *          "description" = "Invoice ID"
     *      }
     *  },
     *  tags = {
     *      "stable"
     *  }
     * )
     *
     * @param $id
     *
     * @return array
     */
    public function getInvoiceAction($id)
    {
        return $this->view($this->getHandler()->get(
           $id
        ));
    }

    /**
     * Send invoice PDF file
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  authenticationRoles={
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *     200 = "Requested when success",
     *     302 = "Requested when same file already exist",
     *     400 = "Requested when error",
     *     405 = "Requested when method not allowed",
     *     404 = "Requested when order has not been found",
     *     415 = "Requested when the uploaded file is not in PDF format",
     *     424 = "Requested when missing dependencies"
     *  },
     *  parameters = {
     *      {
     *          "required" = true,
     *          "name" = "number",
     *          "dataType" = "string",
     *          "description" = "Invoice number"
     *      },
     *      {
     *          "required" = true,
     *          "name" = "filename",
     *          "dataType" = "string",
     *          "description" = "Invoice file name"
     *      },
     *      {
     *          "required" = true,
     *          "name"="file",
     *          "dataType"="file",
     *          "description"="Invoice file content"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     */
    public function postInvoiceAction($order, Request $request)
    {
        $data = $this->parseMultipartContent($request->getContent());
        $number = $data['number'];
        $filename = $data['filename'];
        if(!$filename || !$number || (strpos($filename, '.pdf') === false) || empty($file))
        {
            throw new HttpException(424);
        }
        /** @var Order $order */
        $order = $this->getDoctrine()->getManager()->getRepository('MnumiProductBundle:Product')->find($order);
        if (!$order) {
            throw new HttpException(404, 'Order not found');
        }
        $invoicePath = $this->container->getParameter('kernel.root_dir') . '/../../data/invoice/' . $order->getId();

        $file = $data['file'];

        /**
         * Accept oly PDF file
         */
        $f = finfo_open();
        $mimeType = finfo_buffer($f, $file, PATHINFO_EXTENSION);
        $filesystem = new Filesystem();

        if ($filesystem->exists($invoicePath)) {
            $finder = new Finder();
            $finder->files()->in($invoicePath);
            /** @var \Symfony\Component\Finder\SplFileInfo $item */
            foreach ($finder as $item) {
                if (md5($item->getContents()) == md5($file)) {
                    throw new HttpException(302);
                }
            }
            if (strpos(strtolower($mimeType), 'pdf') === false) {
                throw new HttpException(415);
            }
        }
        /**
         * Create directory and file for order
         */
        try {
            $filesystem->mkdir($invoicePath);
            $filesystem->dumpFile($invoicePath . '/' . $filename, $file);
        } catch (IOException $e)
        {
            throw new HttpException(400);
        }
        return View::create('File uploaded', 200);
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.invoice.handler');
    }

    protected function parseMultipartContent($raw_data)
    {
        $boundary = substr($raw_data, 0, strpos($raw_data, "\r\n"));

        // Fetch each part
        $parts = array_slice(explode($boundary, $raw_data), 1);
        $data = array();

        foreach ($parts as $part) {
            // If this is the last part, break
            if ($part == "--\r\n") break;

            // Separate content from headers
            $part = ltrim($part, "\r\n");
            list($raw_headers, $body) = explode("\r\n\r\n", $part, 2);

            // Parse the headers list
            $raw_headers = explode("\r\n", $raw_headers);
            $headers = array();
            foreach ($raw_headers as $header) {
                list($name, $value) = explode(':', $header);
                $headers[strtolower($name)] = ltrim($value, ' ');
            }

            // Parse the Content-Disposition to get the field name, etc.
            if (isset($headers['content-disposition'])) {
                $filename = null;
                preg_match(
                    '/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/',
                    $headers['content-disposition'],
                    $matches
                );
                list(, $type, $name) = $matches;
                isset($matches[4]) and $filename = $matches[4];

                // handle your fields here
                switch ($name) {
                    // this is a file upload
                    case 'userfile':
                        file_put_contents($filename, $body);
                        break;

                    // default for all other files is to populate $data
                    default:
                        $data[$name] = substr($body, 0, strlen($body) - 2);
                        break;
                }
            }

        }
        return $data;
    }
}
