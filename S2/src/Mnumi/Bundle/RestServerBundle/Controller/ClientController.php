<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Mnumi\Bundle\RestServerBundle\Exception\InvalidFormException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Annotations\NamePrefix("api_client_")
 */
class ClientController extends FOSRestController
{
    /**
     * Get a single client
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Client not found"
     *  },
     *  filters = {
     *      {"name"="city", "dataType"="string", "description"="City"},
     *      {"name"="street", "dataType"="string", "description"="Street"},
     *      {"name"="postcode", "dataType"="string", "description"="Post code"},
     *      {"name"="taxId", "dataType"="string", "description"="VAT number"},
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "array<Mnumi\Bundle\ClientBundle\Entity\Client>",
     *      "groups"={"public"}
     *  }
     * )
     *
     * @Method({"GET"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @internal param \FOS\RestBundle\Request\ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getClientsAction(Request $request)
    {
        /** @var \Doctrine\Common\Collections\ArrayCollection $data */
        $data = $this->getHandler()->getBy($request->query->all());
        if(!$data->isEmpty())
        {
            return $this->view($data);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Create a client
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  authenticationRoles={
     *      "ROLE_USER"
     *  },
     *  input = {
     *      "class" = "Mnumi\Bundle\ClientBundle\Form\Type\ClientType",
     *      "name"=""
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\ClientBundle\Entity\Client",
     *      "groups"={"public"}
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @Annotations\View(templateVar="post")
     */
    public function postClientAction(Request $request)
    {
        try {
            return $this->getHandler()->post(
                $request->request->all()
            );
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.client.handler');
    }

}
