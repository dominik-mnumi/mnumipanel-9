<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends FOSRestController
{
    /**
     * Dummy hello method
     *
     * @Route("/hello")
     * @Template()
     */
    public function indexAction()
    {
        $data = array(
            'name' => (string) $this->getUser(),
        );

        $view = $this->view($data, 200)
            ->setTemplate("MnumiRestServerBundle:Default:index.html.twig")
            ->setTemplateVar('users')
        ;

        return $this->handleView($view);
    }
}
