<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Annotations\NamePrefix("api_product_")
 */
class ProductController extends FOSRestController
{
    /**
     * Get a single product by slug
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when not found"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\ProductBundle\Entity\Product"
     *  },
     *  requirements = {
     *      {
     *          "name"="product",
     *          "dataType"="string",
     *          "requirement"="[\w\d\-\_]+",
     *          "description"="Product slug"
     *      }
     *  }
     * )
     *
     * @Method({"GET"})
     * @Template()
     */
    public function getProductAction($product)
    {
        $product = $this->getOr404($product);

        return $this->view($product);
    }

    /**
     * Get a all products
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when not found"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "array<Mnumi\Bundle\ProductBundle\Entity\Product> as products"
     *  }
     * )
     *
     * @Method({"GET"})
     * @Annotations\View(serializerGroups={"ProductList"})
     */
    public function getProductsAction()
    {
        try {
            $products = $this->getDoctrine()->getManager()->getRepository('MnumiProductBundle:Product')->findAll();

        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }
        if (!$products)
        {
            throw new NotFoundHttpException('Not found');
        }
        return ($products);
        
    }

    /**
     * Fetch a Object or throw an 404 Exception.
     *
     * @param $slug
     * @throws NotFoundHttpException
     * @internal param mixed $id
     *
     * @return Order
     *
     */
    protected function getOr404($slug)
    {
        if (!($page = $this->getHandler()->getOneBy(array('slug' => $slug)))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$slug));
        }

        return $page;
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.product.handler');
    }
}
