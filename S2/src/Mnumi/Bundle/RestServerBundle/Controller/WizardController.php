<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotations\NamePrefix("api_wizard_")
 */
class WizardController extends FOSRestController
{
    /**
     * Get wizard for product.
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  deprecated = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     * @Annotations\View(
     *  templateVar="attribute"
     * )
     *
     * @Method({"GET"})
     *
     * @param $product
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getProductWizardAction($product, Request $request, ParamFetcherInterface $paramFetcher)
    {
        $product = $this->getDoctrine()->getManager()->getRepository('MnumiProductBundle:Product')->findOneBy(array('slug' => $product));
        if ($product) {
            $wizard = $this->getDoctrine()->getManager()->getRepository('MnumiProductBundle:ProductWizard')->findBy(array('product' => $product));

            return $this->view($wizard);
        }
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.wizard.handler');
    }

}
