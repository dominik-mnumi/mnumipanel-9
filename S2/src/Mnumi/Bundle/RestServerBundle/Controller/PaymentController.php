<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Annotations\NamePrefix("api_payment_")
 */
class PaymentController extends FOSRestController
{
    /**
     * Get Payment by name or all.
     *
     * @ApiDoc(
     *   resource=true,
     *   authentication = true,
     *   authenticationRoles = {
     *      "ROLE_USER"
     *   },
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when the payment not found"
     *   },
     *   parameters = {
     *       {
     *           "name" = "name",
     *           "required" = false,
     *           "dataType" = "string",
     *           "description" = "Name of Payment"
     *       }
     *  },
     *  output = {
     *      "class" = "array<Mnumi\Bundle\PaymentBundle\Entity\Payment>",
     *      "groups" = {
     *          "public"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Annotations\View(
     *  templateVar="carrier"
     * )
     *
     * @Method({"GET"})
     *
     * @param Request $request the request object
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return array
     */
    public function getPaymentsAction(Request $request)
    {
        try {
            if ($request->get('name')) {
                $payment = $this->getHandler()->getBySlug(
                    $request->get('name')
                );
            } else {
                $payment = $this->getHandler()->all('id', 'ASC', null, null);
            }
        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }
        if($payment)
        {
            return $payment;
        } else {
            throw new NotFoundHttpException('Payment not found');
        }
    }

    /**
     * Add new Payment
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when create",
     *      302 = "Returned when exists",
     *      406 = "Can't add the payment"
     *  },
     *  input = {
     *      "class" = "Mnumi\Bundle\PaymentBundle\Form\Type\PaymentType",
     *      "name" = ""
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\PaymentBundle\Entity\Payment"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @Annotations\View(templateVar="post")
     */
    public function postPaymentsAction(Request $request)
    {
        $name = $request->request->get('name');
        if(!$name)
        {
            throw new BadRequestHttpException();
        }
        try {
            $payment = $this->getHandler()->getBy(
                array('name' => $name)
            );
            if (!$payment->isEmpty()) {
                return $this->view($payment[0], Codes::HTTP_FOUND);
            }
            return $this->getHandler()->post(
                $request->request->all()
            );
        } catch (\Exception $exception) {
            throw new HttpException(406);
        }
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.payment.handler');
    }

}
