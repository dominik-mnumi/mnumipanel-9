<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Mnumi\Bundle\RestServerBundle\Exception\UnknownFileExtensionException;
use Mnumi\Bundle\RestServerBundle\Exception\CurlFailedException;
use Symfony\Component\Filesystem\Filesystem;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Mnumi\Bundle\OrderBundle\Entity\OrderItemFile;
use Mnumi\Bundle\RestServerBundle\Handler\OrderItemHandler;

/**
 * @Annotations\NamePrefix("api_order_item_")
 */
class OrderItemController extends FOSRestController
{
    /**
     * Update order item status
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  parameters = {
     *      {
     *          "name" = "orderStatusName",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "Status name from MnumiCore"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"PATCH"})
     *
     * @Annotations\View(templateVar="patch")
     */
    public function patchOrderItemStatusAction($id, Request $request)
    {
        try {
            $data = $request->request->all();
            $order = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:OrderItem')->find($id);
            if($order !== null && $data !== null)
            {
                $newObject = $this->getHandler()->patch(
                    $order,
                    $data
                );
                return $newObject;
            }
            throw new HttpException(400, 'Parameters can not be empty');
        } catch (\Exception $exception) {
            throw new HttpException(400);
        }
    }

    /**
     * Add File to the OrderItem.
     *
     * @ApiDoc(
     *   resource=true,
     *   authentication = true,
     *   authenticationRoles = {
     *      "ROLE_USER"
     *   },
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when the order item is not found"
     *   },
     *   parameters = {
     *       {
     *           "name" = "url",
     *           "required" = true,
     *           "dataType" = "string",
     *           "description" = "URL to fetch file from"
     *       }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @throws NotFoundHttpException
     */
    public function postOrderItemFileAction($id, Request $request)
    {
        $orderItem = $this->getOr404($id);

        $url = $request->get('url');

        if ($url === null) {
            throw new NotFoundHttpException(sprintf('The url has not been set.'));
        }

        try {
            $fetchedFile = $this->fetchFileFromUrl($url);
            $extension = $fetchedFile->guessExtension();

            if ($extension === null) {
                throw new UnknownFileExtensionException(
                    sprintf('Extension for file: %s cannot be guessed', $fetchedFile->getFilename())
                );
            }

            $dataDir = $this->container->getParameter('data_dir');

            $em = $this->getDoctrine()->getManager();

            $orderItem->preparePath($dataDir);
            $filePath = $dataDir . '/' . $orderItem->getFilepath();
            $file = $fetchedFile->move($filePath, $fetchedFile->getFilename() . '.' . $extension);

            $orderItemFile = new OrderItemFile();
            $orderItemFile->setOrderItem($orderItem);
            $orderItemFile->setFilename($file->getFilename());
            $orderItemFile->setMetadataFromFile($file);

            $em->persist($orderItemFile);
            $em->flush();
        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }

        return $orderItemFile;
    }

    /**
     * Add File to the OrderItem.
     *
     * @ApiDoc(
     *   resource=true,
     *   authentication = true,
     *   authenticationRoles = {
     *      "ROLE_USER"
     *   },
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when the OrderItem is not found"
     *  },
     *  deprecated = true,
     *  parameters = {
     *      {
     *          "name" = "id",
     *          "required" = true,
     *          "dataType" = "int",
     *          "description" = "OrderItem ID"
     *      },
     *      {
     *          "name" = "url",
     *          "required" = true,
     *          "dataType" = "string",
     *          "description" = "URL to fetch file from"
     *      }
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @throws NotFoundHttpException
     */
    public function postOrderItemFileFetchAction(Request $request)
    {
        $id = $request->get('id');
        return $this->postOrderItemFileAction($id, $request);

    }

    /**
     * @param  string $url
     * @return File
     */
    protected function fetchFileFromUrl($url)
    {
        $filepath = tempnam(sys_get_temp_dir(), "RemoteOrderItemFile-");
        $fs = new Filesystem();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        $fp = fopen($filepath, 'w+');

        curl_setopt($ch, CURLOPT_FILE, $fp);

        if (curl_exec($ch) === false) {
            throw new CurlFailedException('Fetching file failed', $ch);
        }

        curl_close($ch);
        fclose($fp);

        return new File($filepath);
    }

    /**
     * Fetch an OrderItem or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return OrderItem
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($orderItem = $this->getHandler()->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }

        return $orderItem;
    }

    /**
     * @return OrderItemHandler
     */
    protected function getHandler()
    {
        return $this->container->get('rest.orderitem.handler');
    }
}
