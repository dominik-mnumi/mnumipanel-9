<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Annotations\NamePrefix("api_user_")
 */
class UserController extends FOSRestController
{

    /**
     * Search user by email address or get all
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when not found"
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\RestServerBundle\Entity\User",
     *      "groups" = {"public"}
     *  },
     *  parameters = {
     *      {
     *          "name" = "emailAddress",
     *          "dataType" = "string",
     *          "required" = false,
     *          "description" = "User Email address"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     * @Method({"GET"})
     * @Template()
     */
    public function getUsersAction(Request $request)
    {
        try {
            $emailAddress = $request->get('emailAddress', null);
            
            if ($emailAddress) {
                $data = array(
                    'emailAddress' => $emailAddress,
                );
                $return = $this->getHandler()->getOneBy($data);
            } else {
                $return = $this->getHandler()->all();
            }
        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }
        if($return) {
            return $this->view($return);
        }
        throw new NotFoundHttpException("Not found");
    }
    /*
    public function getUsersAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        return $this->getHandler()->getBy(
            $paramFetcher->all()
        );
    }
    */
    /**
     * Create new user
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when not found"
     *  },
     *  input = {
     *      "class" = "Mnumi\Bundle\RestServerBundle\Form\Type\UserType",
     *      "name" = ""
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\RestServerBundle\Entity\User",
     *      "groups" = {"public"}
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @Annotations\View(templateVar="post")
     */
    public function postUserAction(Request $request)
    {
        try {
            $newObject = $this->getHandler()->post(
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $newObject->getId(),
                '_format' => $request->get('_format')
            );

            return $newObject;
           return $this->routeRedirectView('api_user_post_user', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.user.handler');
    }
}
