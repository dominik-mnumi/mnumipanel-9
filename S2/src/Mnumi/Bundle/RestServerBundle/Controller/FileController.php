<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Controller;

use Mnumi\Bundle\OrderBundle\Entity\OrderItem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Class FileController
 * @package Mnumi\Bundle\RestServerBundle\Controller
 */
class FileController extends Controller
{
    /**
     * @param Request $request
     */
    public function getFileAction(Request $request)
    {
        $hash = $request->get('hash');
        $id = $request->get('id');
        /** @var OrderItem $item */
        $item = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:OrderItem')->find($id);
        if($item) {
            if(md5($item->getCreatedAt()->getTimestamp()) == $hash) {
                $url = str_replace($hash . '/', '', $request->getPathInfo());

                $filename = realpath($this->container->getParameter('data_dir') . $url);
                if ($filename !== false) {
                    // Generate response
                    $response = new Response();

                    // Set headers
                    $response->headers->set('Cache-Control', 'private');
                    $response->headers->set('Content-type', mime_content_type($filename));
                    $response->headers->set(
                        'Content-Disposition',
                        'attachment; filename="' . basename($filename) . '";'
                    );
                    $response->headers->set('Content-length', filesize($filename));

                    // Send headers before outputting anything
                    $response->sendHeaders();

                    $response->setContent(readfile($filename));
                }
            }
        }
        return new Response("File not found",404);
    }
} 