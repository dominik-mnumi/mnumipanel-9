<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Annotations\NamePrefix("api_category_")
 */
class CategoryController extends FOSRestController
{
    /**
     * Get all categories as a flat array
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = {
     *          "Returned when error",
     *          "Returned when not found"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "\Mnumi\Bundle\RestServerBundle\Entity\Category"
     *  }
     * )
     *
     * @Method({"GET"})
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @return array
     */
    public function getCategoryAction()
    {
        try {
            $category = $this->getDoctrine()->getManager()->getRepository('MnumiRestServerBundle:Category')->findAll();
        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }

        return $this->view($category);
    }

}
