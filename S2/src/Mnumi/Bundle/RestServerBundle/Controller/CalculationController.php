<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\HttpFoundation\Request;
use \Mnumi\Bundle\CalculationBundle\Library\ProductCalculationProductData;
use \Mnumi\Bundle\CalculationBundle\Library\CalculationTool;

/**
 * @Annotations\NamePrefix("api_calculation_")
 */
class CalculationController extends FOSRestController
{
    /**
     * Get the calculation of the product for the customer
     *
     * Todo:
     *
     * - Create CalculationType
     * - Create output data container
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when error"
     *  },
     *  requirements = {
     *      {
     *          "name" = "product",
     *          "dataType" = "string",
     *          "requirement" = "[\w\d\-\_]+",
     *          "description" = "Product slug"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c",
     *      "to do"
     *  }
     * )
     *
     * @Method({"POST"})
     * @Template()
     */
    public function postCalculationAction($product, Request $request)
    {
        if ($request->request->all()) {
            $data = $request->request->all();
            if (isset($data['SIZE'])) {
                if ($request->request->get('width')) {
                    $data['SIZE']['width']['value'] = $request->request->get('width');
                } elseif ($request->request->get('height')) {
                    $data['SIZE']['height']['value'] = $request->request->get('height');
                }
                $data['SIZE']['metric']['value'] = ( $request->request->get('metric') ) ? $request->request->get('metric') : 'mm';
            }
            $priceReportArr = $this->doCalculate(
                    $product,
                    $data,
                    $this->getPriceListId(
                            strval($data['user']),
                            strval($data['client'])
                    )
            );

            return $this->view(
                    $this->getWntUeVatCalculation(
                            strval($data['user']),
                            strval($data['client']),
                            $priceReportArr)
                    );
        }
    }

    private function getWntUeVatCalculation($userName, $clientName, $priceReportArr)
    {
        if ($userName !== "" && $clientName !== "") {
            $user = $this->getDoctrine()->getManager()->getRepository('MnumiRestServerBundle:User')->findOneBy(array('username' => $userName));
            if ($user) {
                $client = $user->findClient($clientName);
                if ($client !== null) {
                    if ($client->getWntUe()) {
                        $priceReportArr['summaryPriceGross'] = $priceReportArr['summaryPriceNet'];
                        $priceReportArr['priceTax'] = 0;
                    }
                }
            }
        }

        return $priceReportArr;
    }

    /**
     * Get PriceListId if client and user are correct
     *
     * @param  string $userName
     * @param  string $clientName
     * @return mixed
     */
    private function getPriceListId($userName, $clientName)
    {
        $priceListId = null;
        if ($userName !== "" && $clientName !== "") {
            $user = $this->getDoctrine()->getManager()->getRepository('MnumiRestServerBundle:User')->findOneBy(array('username' => $userName));
            if ($user) {
                $client = $user->findClient($clientName);
                if ($client !== null) {
                    $priceListId = $client->getPricelist();
                }
            }
        }

        return $priceListId;
    }

    /**
     * Get calculation for product by price list
     *
     * @param  string $productName
     * @param  array  $data
     * @param  int    $priceListId
     * @return array
     */
    private function doCalculate($productName, $data, $priceListId = null)
    {
        $product = $this->getDoctrine()->getManager()
                ->getRepository('MnumiProductBundle:Product')
                ->findOneBy(array('slug' => $productName));

        $productData = new ProductCalculationProductData(
                $this->getDoctrine()->getManager(), $product, $this->container->getParameter('price_tax') * 100
        );
        $calculationTool = new CalculationTool($productData->getCalculationProductDataObject());

        if ($priceListId === null) {
            $priceListId = $productData->getCalculationProductDataObject()->getDefaultPriceListId();
        }
        $calculationTool->initialize($data, $priceListId);

        return $calculationTool->fetchReport();
    }

}
