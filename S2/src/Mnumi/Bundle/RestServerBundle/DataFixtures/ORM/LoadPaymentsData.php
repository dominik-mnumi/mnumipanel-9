<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\PaymentBundle\Entity\Payment;

class LoadPaymentsData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        $carrier = new Payment();
        $carrier->setName('PayU');
        $carrier->setLabel('PayU');
        $manager->persist($carrier);

        $carrier = new Payment();
        $carrier->setName('cash on delivery');
        $carrier->setLabel('Cash on delivery');
        $manager->persist($carrier);

        $carrier = new Payment();
        $carrier->setName('card payment');
        $carrier->setLabel('card payment');
        $manager->persist($carrier);

        $carrier = new Payment();
        $carrier->setName('cash payment');
        $carrier->setLabel('cash payment');
        $manager->persist($carrier);

        $carrier = new Payment();
        $carrier->setName('transfer (21 days)');
        $carrier->setLabel('transfer (21 days)');
        $manager->persist($carrier);

        $carrier = new Payment();
        $carrier->setName('barter');
        $carrier->setLabel('barter');
        $manager->persist($carrier);

        $carrier = new Payment();
        $carrier->setName('compensation');
        $carrier->setLabel('compensation');
        $manager->persist($carrier);

        $carrier = new Payment();
        $carrier->setName('gratis');
        $carrier->setLabel('gratis');
        $manager->persist($carrier);

        $carrier = new Payment();
        $carrier->setName('payment on account');
        $carrier->setLabel('payment on account');
        $manager->persist($carrier);

        $paymentStatus = new \Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus();
        $paymentStatus->setName('paid');
        $manager->persist($paymentStatus);

        $paymentStatus = new \Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus();
        $paymentStatus->setName('waiting');
        $manager->persist($paymentStatus);

        $manager->flush();
    }

    public function getOrder()
    {
        return 300;
    }
}
