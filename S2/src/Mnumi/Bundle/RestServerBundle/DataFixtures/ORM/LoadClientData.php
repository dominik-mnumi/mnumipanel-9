<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\RestServerBundle\Entity\User;

class LoadClientData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        $repository = $manager->getRepository('MnumiRestServerBundle:User');
        $user = $repository->findOneBy(array('username' => 'testtest'));

        $client = new \Mnumi\Bundle\ClientBundle\Entity\Client();
        $client->setFullname("Test");
        $client->setCity("Warszawa");
        $client->setPostcode("00-001");
        $client->setStreet("Kaskadowa 8");
        $client->setCountry("PL");
        $client->setCreditLimitAmount("0");
        $client->addUser($user);
        $manager->persist($client);

        $client = new \Mnumi\Bundle\ClientBundle\Entity\Client();
        $client->setFullname("Test 2");
        $client->setCity("Warszawa");
        $client->setPostcode("00-001");
        $client->setStreet("Kaskadowa 10");
        $client->setCountry("PL");
        $client->setCreditLimitAmount("0");
        $manager->persist($client);

        $manager->flush();
    }

    public function getOrder()
    {
        return 450;
    }
}
