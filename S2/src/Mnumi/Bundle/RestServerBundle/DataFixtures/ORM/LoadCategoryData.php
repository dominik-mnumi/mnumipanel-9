<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Main");
        $manager->persist($category);

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Catalogs");
        $manager->persist($category);

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Canvas Prints");
        $manager->persist($category);

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Calendars");
        $manager->persist($category);

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Bussiness Cards");
        $manager->persist($category);

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Brochures");
        $manager->persist($category);

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Boolkets");
        $manager->persist($category);

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Short Run");
        $manager->persist($category);

        $category = new \Mnumi\Bundle\RestServerBundle\Entity\Category();
        $category->setName("Bulk");
        $manager->persist($category);

        $manager->flush();
    }

    public function getOrder()
    {
        return 500;
    }
}
