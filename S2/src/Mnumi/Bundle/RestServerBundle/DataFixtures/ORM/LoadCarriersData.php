<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\CarrierBundle\Entity\Carrier;

class LoadCarriersData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        $carrier = new Carrier();
        $carrier->setName('Courier');
        $carrier->setLabel('Courier');
        $carrier->setDeliveryTime('x days');
        $manager->persist($carrier);

        $carrier = new Carrier();
        $carrier->setName('Post office');
        $carrier->setLabel('Post office');
        $carrier->setDeliveryTime('x days');
        $manager->persist($carrier);

        $carrier = new Carrier();
        $carrier->setName('Collection');
        $carrier->setLabel('Collection');
        $carrier->setDeliveryTime('x days');
        $manager->persist($carrier);

        $carrier = new Carrier();
        $carrier->setName('Shipping to customers');
        $carrier->setLabel('Shipping to customers');
        $carrier->setDeliveryTime('x days');
        $manager->persist($carrier);

        $carrier = new Carrier();
        $carrier->setName('Poczta Polska');
        $carrier->setLabel('Poczta Polska');
        $carrier->setDeliveryTime('x days');
        $manager->persist($carrier);

        $carrier = new Carrier();
        $carrier->setName('UPS');
        $carrier->setLabel('UPS');
        $carrier->setDeliveryTime('x days');
        $manager->persist($carrier);

        $manager->flush();
    }

    public function getOrder()
    {
        return 350;
    }
}
