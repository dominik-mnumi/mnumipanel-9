<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //$em = $this->container->get('doctrine')->getEntityManager('default');

        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        $repository = $manager->getRepository('MnumiRestServerBundle:Category');
        $category = $repository->findOneBy(array('name' => 'Calendars'));

        $product = new \Mnumi\Bundle\ProductBundle\Entity\Product();
        $product->setName("Calendar - 12 pages");
        $product->setCategory($category);
        $product->setSlug("calendar-12-pages");

        $repository = $manager->getRepository('MnumiProductBundle:Fieldset');
        $fieldsets = $repository->findAll();

        foreach ($fieldsets as $fieldset) {
            $productField = new \Mnumi\Bundle\ProductBundle\Entity\ProductField();
            $productField->setProduct($product);
            $productField->setFieldset($fieldset);
            $manager->persist($productField);
        }

        $manager->persist($product);

        $wizard = new \Mnumi\Bundle\ProductBundle\Entity\ProductWizard();
        $wizard->setWizardName('dz6qoUngne');
        $wizard->setSortOrder(1);
        $wizard->setActive(1);
        $wizard->setProduct($product);
        $manager->persist($wizard);
        $manager->flush();
    }

    public function getOrder()
    {
        return 550;
    }
}
