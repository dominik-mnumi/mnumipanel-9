<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\RestServerBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        $user = new User();
        $user->setEmailAddress('test@yopmail.com');
        $user->setFirstName('Jan');
        $user->setLastName('Kowalski');
        $user->setPassword('test');
        $user->setUsername("testtest");
        //$user->setRoles(1);
        $user->setSalt('test');
        $manager->persist($user);

        $manager->flush();
    }

    public function getOrder()
    {
        return 200;
    }
}
