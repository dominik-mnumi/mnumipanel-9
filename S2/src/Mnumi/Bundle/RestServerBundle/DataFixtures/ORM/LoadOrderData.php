<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\OrderBundle\Entity\Order;
use Mnumi\Bundle\OrderBundle\Entity\OrderItem;
use Mnumi\Bundle\RestServerBundle\Entity\User;

class LoadOrderData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        $repository = $manager->getRepository('MnumiClientBundle:Client');
        $client = $repository->findOneBy(array('fullname' => 'Test'));
        $repository = $manager->getRepository('MnumiRestServerBundle:User');
        $user = $repository->findOneBy(array('username' => 'testtest'));

        $repository = $manager->getRepository('MnumiPaymentBundle:PaymentStatus');
        $paymentStatus = $repository->findOneBy(array('name' => 'paid'));

        $order = new Order();
        $order->setClient($client);
        $order->setToBook(0);
        $order->setUser($user);
        $order->setWantInvoice(0);
        $order->setClientAddress(1);
        $order->setPaymentStatusName($paymentStatus);
        $order->setDeliveryStreet('street 123');

        $orderItem = new OrderItem();
        $orderItem->setClient($client);
        $orderItem->setOrder(1);
        $orderItem->setName('test');
        $orderItem->setUser($user);
        $orderItem->setTaxValue(1);

        $order->addItem($orderItem);

        $manager->persist($order);
        $manager->flush();
    }

    public function getOrder()
    {
        return 600;
    }
}
