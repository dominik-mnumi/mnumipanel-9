<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\RestServerBundle\Entity\GuardGroup;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        /**
         * GROUPS
         */
        $group = new GuardGroup();
        $group->setDescription('Administrator');
        $group->setName('admin');
        $group->setUpdated(new \DateTime());
        $manager->persist($group);

        $group = new GuardGroup();
        $group->setDescription('Office');
        $group->setName('office');
        $group->setUpdated(new \DateTime());
        $manager->persist($group);

        $group = new GuardGroup();
        $group->setDescription('Finance');
        $group->setName('finance');
        $group->setUpdated(new \DateTime());
        $manager->persist($group);

        $group = new GuardGroup();
        $group->setDescription('ClientActionsFunctional');
        $group->setName('ClientActionsFunctional');
        $group->setUpdated(new \DateTime());
        $manager->persist($group);

        $group = new GuardGroup();
        $group->setDescription('Graphic');
        $group->setName('graphic');
        $group->setUpdated(new \DateTime());
        $manager->persist($group);

        $group = new GuardGroup();
        $group->setDescription('Configuration');
        $group->setName('configuration');
        $group->setUpdated(new \DateTime());
        $manager->persist($group);

        $group = new GuardGroup();
        $group->setDescription('Lite');
        $group->setName('lite');
        $group->setUpdated(new \DateTime());
        $manager->persist($group);

        $manager->flush();
    }

    public function getOrder()
    {
        return 250;
    }

}
