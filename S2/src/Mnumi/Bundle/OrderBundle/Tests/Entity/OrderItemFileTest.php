<?php

namespace Mnumi\Bundle\OrderBundle\Tests\Entity;

use Mnumi\Bundle\OrderBundle\Entity\OrderItem;
use Mnumi\Bundle\OrderBundle\Entity\OrderItemFile;

class OrderItemFileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Prepares OrderItem entity filled with default values.
     * It is a helper function for better code clarity of tests.
     */
    protected function prepareOrderItem()
    {
        $orderItem = new OrderItem();
        $class = new \ReflectionClass('Mnumi\Bundle\OrderBundle\Entity\OrderItem');

        $reflectionId = $class->getProperty('id');
        $reflectionId->setAccessible(true);
        $reflectionId->setValue($orderItem, 1);

        return $orderItem;
    }

    /**
     * Test checks if the metadata has been set correctly, it means that it is
     * discoverable by the ::hasMetadataAttribute() method.
     */
    public function testOrderItemFileMetadataSetting()
    {
        $key = 'format';
        $value = 'PDF';
        $orderItem = $this->prepareOrderItem();

        $orderItemFile = new OrderItemFile();
        $orderItemFile->setOrderItem($orderItem);
        $orderItemFile->setMetadataAttribute($key, $value);

        $this->assertTrue($orderItemFile->hasMetadataAttribute($key));
    }

    /**
     * Test checks if the metadata value has been set correctly and its value
     * wasn't changed during the operation.
     */
    public function testOrderItemFileMetadataGetting()
    {
        $key = 'format';
        $value = 'PDF';
        $orderItem = $this->prepareOrderItem();

        $orderItemFile = new OrderItemFile();
        $orderItemFile->setOrderItem($orderItem);
        $orderItemFile->setMetadataAttribute($key, $value);
        $orderItemFile->getMetadataAttribute($key);

        $this->assertEquals($value, $orderItemFile->getMetadataAttribute($key));
    }

    /**
     * Test checks if the metadata value has been removed correctly, it means
     * that and its key-value pair doesn't exist after the remove operation and
     * is not discoverable by the ::hasMetadataAttribute() method.
     */
    public function testOrderItemFileMetadataRemoving()
    {
        $key = 'format';
        $value = 'PDF';
        $orderItem = $this->prepareOrderItem();

        $orderItemFile = new OrderItemFile();
        $orderItemFile->setOrderItem($orderItem);
        $orderItemFile->setMetadataAttribute($key, $value);
        $orderItemFile->deleteMetadataAttribute($key);

        $this->assertFalse($orderItemFile->hasMetadataAttribute($key));
    }
}
