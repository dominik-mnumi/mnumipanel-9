<?php

namespace Mnumi\Bundle\OrderBundle\Tests\Entity;

use Mnumi\Bundle\OrderBundle\Entity\OrderItem;

class OrderItemTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks if filepath returned by the OrderItem::getFilePath returns
     * relative filepath same as in the old core: Order::getFilePath.
     *
     * The only difference between them is that the old version returns filepath
     * prefixed with data_dir configuration variable. New version does not use
     * it, due the architecture differences and lack of sfConfig singleton in
     * Symfony2.
     */
    public function testCheckIfFilePathIsBackCompatible()
    {
        $year = 2014;
        $month = 11;
        $day = '03';
        $orderItemId = 1;

        $pattern = "files/$year/$month/$day/$orderItemId/";

        $creationDate = new \DateTime("$year-$month-$day 12:00:30");

        $orderItem = new OrderItem();
        $class = new \ReflectionClass('Mnumi\Bundle\OrderBundle\Entity\OrderItem');

        $reflectionId = $class->getProperty('id');
        $reflectionId->setAccessible(true);
        $reflectionId->setValue($orderItem, $orderItemId);

        $class->getMethod('setCreatedAt')->invokeArgs($orderItem, array($creationDate));
        $result = $class->getMethod('getFilePath')->invoke($orderItem);

        $this->assertEquals($pattern, $result);
    }
}
