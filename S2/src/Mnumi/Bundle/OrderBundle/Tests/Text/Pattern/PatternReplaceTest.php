<?php

namespace Mnumi\Bundle\OrderBundle\Tests\Text\Pattern;

use Mnumi\Bundle\OrderBundle\Text\Pattern\PatternReplace;

class PatternReplaceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks convert is work correctly
     */
    public function testConvert()
    {
        $testClass = new TestClass();

        $patternReplace = new PatternReplace($testClass);

        $this->assertEquals('value1 static text', $patternReplace->convert('{method1} static text'));
        $this->assertEquals('static text value2', $patternReplace->convert('static text {method2}'));
        $this->assertEquals('static text camelCaseValue', $patternReplace->convert('static text {camel_case}'));
        $this->assertEquals('customValue123', $patternReplace->convert('{custom:123}'));
        $this->assertEquals('customValueAbc', $patternReplace->convert('{custom:Abc}'));
    }

    /**
     * @expectedException \Mnumi\Bundle\OrderBundle\Text\Pattern\PatternReplaceException
     */
    public function testConvertException()
    {
        $testClass = new TestClass();
        $patternReplace = new PatternReplace($testClass);
        $patternReplace->convert('{nonexistmethod}');
    }
}

class TestClass {

    public function getMethod1()
    {
        return 'value1';
    }

    public function getCamelCase()
    {
        return 'camelCaseValue';
    }

    public function getMethod2()
    {
        return 'value2';
    }

    public function getCustom($name)
    {
        return 'customValue'.$name;
    }
}