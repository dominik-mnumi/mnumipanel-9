<?php

namespace Mnumi\Bundle\OrderBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderItemType extends AbstractType
{
    private $options;

    public function __construct($options = null)
    {
        $this->options = $options;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('description' => 'Item name'))
            ->add('notice', null, array('description' => 'Notice for item'))
            ->add('client', 'text', array(
                'data_class' => 'Mnumi\Bundle\ClientBundle\Entity\Client'
            ))
            ->add('user', 'text', array(
                'data_class' => 'Mnumi\Bundle\RestServerBundle\Entity\User'
            ))
            ->add('informations', null, array('description' => 'Information about this item form customer'))
            ->add('product', 'text', array(
                'data_class' => 'Mnumi\Bundle\ProductBundle\Entity\Product'
            ))
            ->add('priceBlock', null, array('description' => 'Price block'))
            ->add('orderStatusName', null, array('description' => 'Order item status'))
            ->add('pricelistId', null, array('description' => 'Order item price list id from MnumiCore'))
            ->add('discountValue', null, array('description' => 'Discount value'))
            ->add('discountType', null, array('description' => 'Discount type'))
            ->add('priceNet', null, array('description' => 'Net price for item'))
            ->add('baseAmount', null, array('description' => 'Base item amount'))
            ->add('totalAmount', null, array('description' => 'Total item amount'))
            ->add('shopName', null, array('description' => 'The name of the shop where the purchase was made.'))
            ->add('taxValue', 'integer', array(
                    'required'  => false,
                    'data' => null,
                    'description' => 'Tax value (eg. 23, 8)'
            ))
            ->add('attributes', 'collection', array(
                'type' => new OrderItemAttributeType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('customSize', 'collection', array(
                'type' => new CustomSizeType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mnumi\Bundle\OrderBundle\Entity\OrderItem'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'order_item';
    }

}
