<?php

namespace Mnumi\Bundle\OrderBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderItemAttributeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('value', 'text')
                ->add('field', 'text', array(
                    'data_class' => 'Mnumi\Bundle\ProductBundle\Entity\ProductField'
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Mnumi\Bundle\OrderBundle\Entity\OrderItemAttribute'
        ));
    }
    /**
     * @return string
     */
    public function getName()
    {
        return 'order_item_attribute';
    }

}
