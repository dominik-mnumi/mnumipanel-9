<?php

namespace Mnumi\Bundle\OrderBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class FieldsetType extends AbstractType
{
    private $options;

    public function __construct($options = null)
    {
        $this->options = $options;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fooTransformer = new FooToStringTransformer($this->options);
        $builder->addModelTransformer($fooTransformer);
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'fieldset';
    }
}
