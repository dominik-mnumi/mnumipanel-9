<?php

namespace Mnumi\Bundle\OrderBundle\Form\DataTransformer;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderPackageStatusTransformer
 *
 * @author dominik
 */
use JMS\SecurityExtraBundle\Security\Util\String;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\OrderBundle\Entity\OrderPackageStatus;

class OrderPackageStatusTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (OrderPackageStatus) to a string
     *
     * @param OrderPackageStatus|null $orderPackageStatus
     * @return String
     */
    public function transform($orderPackageStatus)
    {
        if (null === $orderPackageStatus) {
            return "";
        }

        return $orderPackageStatus->getOrderPackageStatusName();
    }

    /**
     * Transforms a string  to an object (OrderPackageStatus).
     *
     * @param string $id
     *
     * @return OrderPackageStatus|null
     *
     * @throws TransformationFailedException if object (OrderPackageStatus) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $OrderPackageStatus = $this->om
            ->getRepository('MnumiOrderBundle:OrderPackageStatus')
            ->findOneById($id);

        if (null === $OrderPackageStatus) {
            throw new TransformationFailedException(sprintf(
                'An issue with number "%s" does not exist!', $id
            ));
        }
        return $OrderPackageStatus;
    }
}