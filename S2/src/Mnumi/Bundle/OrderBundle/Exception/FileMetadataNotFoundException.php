<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\OrderBundle\Exception;

/**
 * FileMetadataNotFoundException class
 *
 * This exception should be used when the expected OrderItemFile metadata has
 * not been found.
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class FileMetadataNotFoundException extends \RuntimeException
{
}
