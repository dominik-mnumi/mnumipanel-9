<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\OrderBundle\Exception;

/**
 * FileMetadataBadNumberOfCharacteristicsException class
 *
 * This exception should be used when the OrderItemFile metadata has different
 * count of the characteristics than it is expected. It means that every
 * metadata should have following four characteristics:
 *   1. format
 *   2. colorspace
 *   3. widthPx
 *   4. heightPx
 *
 * If anyone of above characteristics does not exists, this exception should be
 * thrown.
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class FileMetadataBadNumberOfCharacteristicsException extends \RuntimeException
{
}
