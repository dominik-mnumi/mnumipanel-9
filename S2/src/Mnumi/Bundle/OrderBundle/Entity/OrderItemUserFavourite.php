<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemUserFavourite
 *
 * @ORM\Table(name="order_user_favourite", indexes={@ORM\Index(name="fk_order_user_favourite_order1_idx", columns={"order_id"}), @ORM\Index(columns={"user_id"})})
 * @ORM\Entity
 */
class OrderItemUserFavourite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderItemId;

}
