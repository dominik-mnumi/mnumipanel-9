<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemCost
 *
 * @ORM\Table(name="order_cost", indexes={@ORM\Index(name="fk_order_cost_order1_idx_idx", columns={"order_id"}), @ORM\Index(name="fk_order_cost_field_item1_idx_idx", columns={"field_item_id"})})
 * @ORM\Entity
 */
class OrderItemCost
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="field_item_id", type="integer", nullable=false)
     */
    private $fieldItemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderItemId;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=2, nullable=false)
     */
    private $cost;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="float", precision=18, scale=2, nullable=false)
     */
    private $quantity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

}
