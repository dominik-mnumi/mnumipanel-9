<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemWorklog
 *
 * @ORM\Table(name="order_worklog", indexes={@ORM\Index(name="fk_order_worklog_order1_idx", columns={"order_id"}), @ORM\Index(columns={"user_id"})})
 * @ORM\Entity
 */
class OrderItemWorklog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderItemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="work", type="string", length=100, nullable=false)
     */
    private $work;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="integer", nullable=true)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="notice", type="text", nullable=true)
     */
    private $notice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

}
