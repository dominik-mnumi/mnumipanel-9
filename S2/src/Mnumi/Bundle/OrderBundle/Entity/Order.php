<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\Groups;

/**
 * Order (Old Package)
 *
 * @ORM\Table(name="order_packages", indexes={@ORM\Index(columns={"payment_id"}), @ORM\Index(columns={"client_address_id"}), @ORM\Index(columns={"payment_status_name"}), @ORM\Index(columns={"client_id"}), @ORM\Index(columns={"order_package_status_name"}), @ORM\Index(columns={"carrier_id"}), @ORM\Index(columns={"rest_session_id"}), @ORM\Index(columns={"report_despatch_id"}), @ORM\Index(columns={"coupon_name"}), @ORM\Index(columns={"user_id"})})
 * @ORM\Entity
 */
class Order
{
    /**
     * Unique autoincrement ID
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"public"})
     */
    private $id;

    /**
     * Name of delivery (eg. London Office)
     * @var string
     *
     * @ORM\Column(name="delivery_name", type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $deliveryName;

    /**
     * Country for delivery
     * @var string
     *
     * @ORM\Column(name="delivery_country", type="string", length=150, nullable=true)
     * @Groups({"public"})
     */
    private $deliveryCountry;

    /**
     * Street for delivery
     * @var string
     *
     * @ORM\Column(name="delivery_street", type="string", length=180, nullable=true)
     * @Groups({"public"})
     */
    private $deliveryStreet;

    /**
     * City for delivery
     * @var string
     *
     * @ORM\Column(name="delivery_city", type="string", length=80, nullable=true)
     * @Groups({"public"})
     */
    private $deliveryCity;

    /**
     * Post code for delivery
     * @var string
     *
     * @ORM\Column(name="delivery_postcode", type="string", length=10, nullable=true)
     * @Groups({"public"})
     */
    private $deliveryPostcode;

    /**
     * Order description
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"public"})
     */
    private $description;

    /**
     * Name of invoice (eg. Mnumi sp. z o.o.)
     * @var string
     *
     * @ORM\Column(name="invoice_name", type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $invoiceName;

    /**
     * Streef for invoice
     * @var string
     *
     * @ORM\Column(name="invoice_street", type="string", length=180, nullable=true)
     * @Groups({"public"})
     */
    private $invoiceStreet;

    /**
     * City for invoice
     * @var string
     *
     * @ORM\Column(name="invoice_city", type="string", length=80, nullable=true)
     * @Groups({"public"})
     */
    private $invoiceCity;

    /**
     * Post code for invoice
     * @var string
     *
     * @ORM\Column(name="invoice_postcode", type="string", length=10, nullable=true)
     * @Groups({"public"})
     */
    private $invoicePostcode;

    /**
     * Tax value (eg. 23, 8)
     * @var string
     *
     * @ORM\Column(name="invoice_tax_id", type="string", length=20, nullable=true)
     * @Groups({"public"})
     */
    private $invoiceTaxId;

    /**
     * Infoice info
     * @var string
     *
     * @ORM\Column(name="invoice_info", type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $invoiceInfo;

    /**
     * Shelf ID
     * @var integer
     *
     * @ORM\Column(name="shelf_id", type="integer", nullable=true)
     * @Groups({"public"})
     */
    private $shelfId;

    /**
     * Is it to be booked?
     * @var boolean
     *
     * @ORM\Column(name="to_book", type="boolean", nullable=false)
     * @Groups({"public"})
     */
    private $toBook = false;

    /**
     * Booked date
     * @var \DateTime
     *
     * @ORM\Column(name="booked_at", type="datetime", nullable=true)
     * @Groups({"public"})
     */
    private $bookedAt;

    /**
     * Does the customer want to invoice?
     * @var boolean
     *
     * @ORM\Column(name="want_invoice", type="boolean", nullable=false)
     * @Groups({"public"})
     */
    private $wantInvoice = false;

    /**
     * Order sent at date
     * @var \DateTime
     *
     * @ORM\Column(name="send_at", type="datetime", nullable=true)
     * @Groups({"public"})
     */
    private $sendAt;

    /**
     * Transport number
     * @var string
     *
     * @ORM\Column(name="transport_number", type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $transportNumber;

    /**
     * Hanger number
     * @var integer
     *
     * @ORM\Column(name="hanger_number", type="integer", nullable=true)
     * @Groups({"public"})
     */
    private $hangerNumber;

    /**
     * Order create date
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Groups({"public"})
     */
    private $createdAt;

    /**
     * Order update date
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Groups({"public"})
     */
    private $updatedAt;

    /**
     * Package status name (eg. closed, waiting)
     * @var \OrderPackageStatus
     *
     * @ORM\ManyToOne(targetEntity="OrderPackageStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_package_status_name", referencedColumnName="name")
     * })
     * @Groups({"public"})
     */
    private $orderPackageStatusName;

    /**
     * Carrier object
     * @var \Carrier
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\CarrierBundle\Entity\Carrier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     * })
     * @Groups({"public"})
     */
    private $carrier;

    /**
     * Client address
     * @var integer
     *
     * @ORM\Column(name="client_address_id", type="integer", nullable=true)
     * @Groups({"public"})
     */
    private $clientAddress;

    /**
     * Client object
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\ClientBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @Groups({"public"})
     */
    private $client;

    /**
     * Coupon name
     * @var string
     *
     * @ORM\Column(name="coupon_name", type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $couponName;

    /**
     * Payment object
     * @var \Payment
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\PaymentBundle\Entity\Payment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     * })
     * @Groups({"public"})
     */
    private $payment;

    /**
     * Order payment status
     * @var \PaymentStatus
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_status_name", referencedColumnName="name")
     * })
     * @Groups({"public"})
     */
    private $paymentStatusName;

    /**
     * @var integer
     *
     * @ORM\Column(name="report_despatch_id", type="integer", nullable=true)
     */
    private $reportDespatch;

    /**
     * @var string
     *
     * @ORM\Column(name="rest_session_id", type="string", length=255, nullable=true)
     */
    private $restSession;

    /**
     * Order user object
     * @var \Mnumi\Bundle\RestServerBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\RestServerBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Groups({"public"})
     */
    private $user;

    /**
     * Order items
     * @ORM\OneToMany(targetEntity="OrderItem", fetch="EAGER", mappedBy="order", cascade={"all"})
     * @ORM\JoinColumn(name="order_package_id", referencedColumnName="id")
     * @Groups({"public"})
     */
    private $items;

    /**
     * Set deliveryName
     *
     * @param  string        $deliveryName
     * @return OrderPackages
     */
    public function setDeliveryName($deliveryName)
    {
        $this->deliveryName = $deliveryName;

        return $this;
    }

    /**
     * Get deliveryName
     *
     * @return string
     */
    public function getDeliveryName()
    {
        return $this->deliveryName;
    }

    /**
     * Set deliveryCountry
     *
     * @param  string        $deliveryCountry
     * @return OrderPackages
     */
    public function setDeliveryCountry($deliveryCountry)
    {
        $this->deliveryCountry = $deliveryCountry;

        return $this;
    }

    /**
     * Get deliveryCountry
     *
     * @return string
     */
    public function getDeliveryCountry()
    {
        return $this->deliveryCountry;
    }

    /**
     * Set deliveryStreet
     *
     * @param  string        $deliveryStreet
     * @return OrderPackages
     */
    public function setDeliveryStreet($deliveryStreet)
    {
        $this->deliveryStreet = $deliveryStreet;

        return $this;
    }

    /**
     * Get deliveryStreet
     *
     * @return string
     */
    public function getDeliveryStreet()
    {
        return $this->deliveryStreet;
    }

    /**
     * Set deliveryCity
     *
     * @param  string        $deliveryCity
     * @return OrderPackages
     */
    public function setDeliveryCity($deliveryCity)
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    /**
     * Get deliveryCity
     *
     * @return string
     */
    public function getDeliveryCity()
    {
        return $this->deliveryCity;
    }

    /**
     * Set deliveryPostcode
     *
     * @param  string        $deliveryPostcode
     * @return OrderPackages
     */
    public function setDeliveryPostcode($deliveryPostcode)
    {
        $this->deliveryPostcode = $deliveryPostcode;

        return $this;
    }

    /**
     * Get deliveryPostcode
     *
     * @return string
     */
    public function getDeliveryPostcode()
    {
        return $this->deliveryPostcode;
    }

    /**
     * Set description
     *
     * @param  string        $description
     * @return OrderPackages
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set invoiceName
     *
     * @param  string        $invoiceName
     * @return OrderPackages
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;

        return $this;
    }

    /**
     * Get invoiceName
     *
     * @return string
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * Set invoiceStreet
     *
     * @param  string        $invoiceStreet
     * @return OrderPackages
     */
    public function setInvoiceStreet($invoiceStreet)
    {
        $this->invoiceStreet = $invoiceStreet;

        return $this;
    }

    /**
     * Get invoiceStreet
     *
     * @return string
     */
    public function getInvoiceStreet()
    {
        return $this->invoiceStreet;
    }

    /**
     * Set invoiceCity
     *
     * @param  string        $invoiceCity
     * @return OrderPackages
     */
    public function setInvoiceCity($invoiceCity)
    {
        $this->invoiceCity = $invoiceCity;

        return $this;
    }

    /**
     * Get invoiceCity
     *
     * @return string
     */
    public function getInvoiceCity()
    {
        return $this->invoiceCity;
    }

    /**
     * Set invoicePostcode
     *
     * @param  string        $invoicePostcode
     * @return OrderPackages
     */
    public function setInvoicePostcode($invoicePostcode)
    {
        $this->invoicePostcode = $invoicePostcode;

        return $this;
    }

    /**
     * Get invoicePostcode
     *
     * @return string
     */
    public function getInvoicePostcode()
    {
        return $this->invoicePostcode;
    }

    /**
     * Set invoiceTaxId
     *
     * @param  string        $invoiceTaxId
     * @return OrderPackages
     */
    public function setInvoiceTaxId($invoiceTaxId)
    {
        $this->invoiceTaxId = $invoiceTaxId;

        return $this;
    }

    /**
     * Get invoiceTaxId
     *
     * @return string
     */
    public function getInvoiceTaxId()
    {
        return $this->invoiceTaxId;
    }

    /**
     * Set invoiceInfo
     *
     * @param  string        $invoiceInfo
     * @return OrderPackages
     */
    public function setInvoiceInfo($invoiceInfo)
    {
        $this->invoiceInfo = $invoiceInfo;

        return $this;
    }

    /**
     * Get invoiceInfo
     *
     * @return string
     */
    public function getInvoiceInfo()
    {
        return $this->invoiceInfo;
    }

    /**
     * Set shelfId
     *
     * @param  integer       $shelfId
     * @return OrderPackages
     */
    public function setShelfId($shelfId)
    {
        $this->shelfId = $shelfId;

        return $this;
    }

    /**
     * Get shelfId
     *
     * @return integer
     */
    public function getShelfId()
    {
        return $this->shelfId;
    }

    /**
     * Set toBook
     *
     * @param  boolean       $toBook
     * @return OrderPackages
     */
    public function setToBook($toBook)
    {
        $this->toBook = $toBook;

        return $this;
    }

    /**
     * Get toBook
     *
     * @return boolean
     */
    public function getToBook()
    {
        return $this->toBook;
    }

    /**
     * Set bookedAt
     *
     * @param  \DateTime     $bookedAt
     * @return OrderPackages
     */
    public function setBookedAt($bookedAt)
    {
        $this->bookedAt = $bookedAt;

        return $this;
    }

    /**
     * Get bookedAt
     *
     * @return \DateTime
     */
    public function getBookedAt()
    {
        return $this->bookedAt;
    }

    /**
     * Set wantInvoice
     *
     * @param  boolean       $wantInvoice
     * @return OrderPackages
     */
    public function setWantInvoice($wantInvoice)
    {
        $this->wantInvoice = $wantInvoice;

        return $this;
    }

    /**
     * Get wantInvoice
     *
     * @return boolean
     */
    public function getWantInvoice()
    {
        return $this->wantInvoice;
    }

    /**
     * Set sendAt
     *
     * @param  \DateTime     $sendAt
     * @return OrderPackages
     */
    public function setSendAt($sendAt)
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    /**
     * Get sendAt
     *
     * @return \DateTime
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * Set transportNumber
     *
     * @param  string        $transportNumber
     * @return OrderPackages
     */
    public function setTransportNumber($transportNumber)
    {
        $this->transportNumber = $transportNumber;

        return $this;
    }

    /**
     * Get transportNumber
     *
     * @return string
     */
    public function getTransportNumber()
    {
        return $this->transportNumber;
    }

    /**
     * Set hangerNumber
     *
     * @param  integer       $hangerNumber
     * @return OrderPackages
     */
    public function setHangerNumber($hangerNumber)
    {
        $this->hangerNumber = $hangerNumber;

        return $this;
    }

    /**
     * Get hangerNumber
     *
     * @return integer
     */
    public function getHangerNumber()
    {
        return $this->hangerNumber;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime     $createdAt
     * @return OrderPackages
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime     $updatedAt
     * @return OrderPackages
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paymentStatusName
     *
     * @param  \Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus $paymentStatusName
     * @return OrderPackages
     */
    public function setPaymentStatusName($paymentStatusName = null)
    {
        $this->paymentStatusName = $paymentStatusName;

        return $this;
    }

    /**
     * Get paymentStatusName
     *
     * @return \Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus
     */
    public function getPaymentStatusName()
    {
        return $this->paymentStatusName;
    }

    /**
     * Set reportDespatch
     *
     * @return OrderPackages
     */
    public function setReportDespatch($reportDespatch = null)
    {
        $this->reportDespatch = $reportDespatch;

        return $this;
    }

    /**
     * Get reportDespatch
     *
     */
    public function getReportDespatch()
    {
        return $this->reportDespatch;
    }

    /**
     * Set restSession
     *
     * @return OrderPackages
     */
    public function setRestSession($restSession = null)
    {
        $this->restSession = $restSession;

        return $this;
    }

    /**
     * Get restSession
     *
     */
    public function getRestSession()
    {
        return $this->restSession;
    }

    /**
     * Set user
     *
     * @param $user
     * @return OrderPackages
     */
    public function setUser($user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Mnumi\Bundle\RestServerBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set payment
     *
     * @param $payment
     * @return OrderPackages
     */
    public function setPayment($payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return \Mnumi\Bundle\PaymentBundle\Entity\Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set couponName
     *
     * @return OrderPackages
     */
    public function setCouponName($couponName = null)
    {
        $this->couponName = $couponName;

        return $this;
    }

    /**
     * Get couponName
     *
     * @return \Mnumi\Bundle\CouponBundle\Entity\Coupon
     */
    public function getCouponName()
    {
        return $this->couponName;
    }

    /**
     * Set carrier
     *
     * @param $carrier
     * @return OrderPackages
     */
    public function setCarrier($carrier = null)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier
     *
     * @return \Mnumi\Bundle\CarrierBundle\Entity\Carrier
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set clientAddress
     *
     * @param $clientAddress
     * @return OrderPackages
     */
    public function setClientAddress($clientAddress = null)
    {
        $this->clientAddress = $clientAddress;

        return $this;
    }

    /**
     * Get clientAddress
     *
     * @return \Mnumi\Bundle\ClientBundle\Entity\ClientAddress
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * Set client
     *
     * @param $client
     * @return OrderPackages
     */
    public function setClient($client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Mnumi\Bundle\ClientBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set orderPackageStatusName
     *
     * @return OrderPackages
     */
    public function setOrderPackageStatusName($orderPackageStatusName = null)
    {
        $this->orderPackageStatusName = $orderPackageStatusName;

        return $this;
    }

    /**
     * Get orderPackageStatusName
     *
     * @return
     */
    public function getOrderPackageStatusName()
    {
        return $this->orderPackageStatusName;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add item
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\OrderItem $item
     * @return Order
     */
    public function addItem(\Mnumi\Bundle\OrderBundle\Entity\OrderItem $item)
    {
        $this->items->add($item);
        $item->setOrder($this);
        $item->setClient($this->getClient());
        $item->setUser($this->getUser());

        return $this;
    }

    /**
     * Remove item
     *
     * @param \Mnumi\Bundle\OrderBundle\Entity\OrderItem $item
     */
    public function removeItem(\Mnumi\Bundle\OrderBundle\Entity\OrderItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return OrderItem[]
     */
    public function getItems()
    {
        return $this->items;
    }
}
