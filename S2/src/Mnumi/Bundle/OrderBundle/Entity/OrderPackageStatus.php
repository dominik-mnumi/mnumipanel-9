<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderPackageStatus
 *
 * @ORM\Table(name="order_package_status")
 * @ORM\Entity
 */
class OrderPackageStatus
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="changeable", type="boolean", nullable=false)
     */
    private $changeable = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * Set title
     *
     * @param  string             $title
     * @return OrderPackageStatus
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set changeable
     *
     * @param  boolean            $changeable
     * @return OrderPackageStatus
     */
    public function setChangeable($changeable)
    {
        $this->changeable = $changeable;

        return $this;
    }

    /**
     * Get changeable
     *
     * @return boolean
     */
    public function getChangeable()
    {
        return $this->changeable;
    }

    /**
     * Set icon
     *
     * @param  string             $icon
     * @return OrderPackageStatus
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
