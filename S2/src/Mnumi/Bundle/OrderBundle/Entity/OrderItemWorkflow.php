<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemWorkflow
 *
 * @ORM\Table(name="order_workflow", indexes={@ORM\Index(name="fk_order_workflow_order_status1_idx", columns={"current_status"}), @ORM\Index(name="fk_order_workflow_order_status2_idx", columns={"next_status"}), @ORM\Index(name="fk_order_workflow_workflow1_idx", columns={"workflow_id"})})
 * @ORM\Entity
 */
class OrderItemWorkflow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="current_status", type="string", length=255, nullable=false)
     */
    private $currentStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="next_status", type="string", length=255, nullable=true)
     */
    private $nextStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="backward", type="boolean", nullable=true)
     */
    private $backward;

    /**
     * @var integer
     *
     * @ORM\Column(name="workflow_id", type="integer", nullable=false)
     */
    private $workflowId;

}
