<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CustomSize
 *
 * @ORM\Table(name="custom_size", indexes={@ORM\Index(columns={"order_id"})})
 * @ORM\Entity
 */
class CustomSize
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="width", type="float", precision=18, scale=2, nullable=true)
     */
    private $width;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float", precision=18, scale=2, nullable=true)
     */
    private $height;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="OrderItem", inversedBy="customSize")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     *
     */
    private $orderItem;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set width
     *
     * @param  float      $width
     * @return CustomSize
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param  float      $height
     * @return CustomSize
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set orderItem
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem
     * @return CustomSize
     */
    public function setOrderItem(\Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem = null)
    {
        $this->orderItem = $orderItem;

        return $this;
    }

    /**
     * Get orderItem
     *
     * @return \Mnumi\Bundle\OrderBundle\Entity\OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }
}
