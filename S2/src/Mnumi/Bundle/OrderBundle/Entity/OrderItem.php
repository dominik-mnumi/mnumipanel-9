<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Mnumi\Bundle\ProductBundle\Entity\Product;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * OrderItem (Old Order)
 *
 * @ORM\Table(name="orders",
 *            indexes={@ORM\Index(columns={"client_id"}),
 *                     @ORM\Index(columns={"order_status_name"}),
 *                     @ORM\Index(columns={"order_package_id"}),
 *                     @ORM\Index(columns={"product_id"}),
 *                     @ORM\Index(columns={"tax_value"}),
 *                     @ORM\Index(columns={"pricelist_id"}),
 *                     @ORM\Index( columns={"shop_name"}),
 *                     @ORM\Index(columns={"user_id"}),
 *                     @ORM\Index(columns={"editor_id"})
 *                  })
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class OrderItem
{
    /**
     * Unique autoincrement ID
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Item name
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Groups({"public"})
     */
    private $name;

    /**
     * Notice for item
     * @var string
     *
     * @ORM\Column(name="notice", type="text", nullable=true)
     * @Groups({"public"})
     */
    private $notice;

    /**
     * Net price for item
     * @var float
     *
     * @ORM\Column(name="price_net", type="float", precision=18, scale=2, nullable=false)
     * @Groups({"public"})
     */
    private $priceNet = 0;

    /**
     * Tax value (eg. 23, 8)
     * @var integer
     *
     * @ORM\Column(name="tax_value", type="integer", nullable=false)
     * @Groups({"public"})
     */
    private $taxValue = 23;

    /**
     * @var \Mnumi\Bundle\ClientBundle\Entity\Client
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\ClientBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @Serializer\Exclude
     */
    private $client;

    /**
     * @var \Mnumi\Bundle\RestServerBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\RestServerBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Serializer\Exclude
     */
    private $user;

    /**
     * Order item status
     * @var string
     *
     * @ORM\Column(name="order_status_name", type="string", length=255, nullable=false)
     * @Groups({"public"})
     */
    private $orderStatusName = 'new';

    /**
     * Order ID
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="items")
     * @ORM\JoinColumn(name="order_package_id", referencedColumnName="id")
     * @Serializer\Exclude
     */
    private $order;

    /**
     * Product object
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\ProductBundle\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * @Serializer\SerializedName("product")
     * @Serializer\Accessor(getter="getProductSlug")
     * @Serializer\Type("string")
     * @Groups({"public"})
     */
    private $product;

    /**
     *
     * @var string
     */
    private $productSlug;

    /**
     * @var integer
     *
     * @ORM\Column(name="shelf_id", type="integer", nullable=true)
     * @Serializer\Exclude
     */
    private $shelfId;

    /**
     * @var integer
     *
     * @ORM\Column(name="editor_id", type="bigint", nullable=true)
     * @Serializer\Exclude
     */
    private $editorId;

    /**
     * @var string
     *
     * @ORM\Column(name="informations", type="text", nullable=true)
     * @Serializer\Exclude
     */
    private $informations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="price_block", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $priceBlock;

    /**
     * @var integer
     *
     * @ORM\Column(name="pricelist_id", type="integer", nullable=true)
     * @Serializer\Exclude
     */
    private $pricelistId;

    /**
     * Discount value
     * @var float
     *
     * @ORM\Column(name="discount_value", type="float", precision=18, scale=2, nullable=true)
     * @Groups({"public"})
     */
    private $discountValue;

    /**
     * Discount type
     * @var string
     *
     * @ORM\Column(name="discount_type", type="string", length=255, nullable=true)
     * @Groups({"public"})
     */
    private $discountType;

    /**
     * The name of the shop where the purchase was made.
     * @var string
     *
     * @ORM\Column(name="shop_name", type="string", length=30, nullable=true)
     * @Groups({"public"})
     */
    private $shopName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accepted_at", type="datetime", nullable=true)
     * @Serializer\Exclude
     */
    private $acceptedAt = null;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $updatedAt;

    /**
     * Base item amount
     * @var float
     *
     * @ORM\Column(name="base_amount", type="float", precision=18, scale=2, nullable=false)
     * @Groups({"public"})
     */
    private $baseAmount = '0.00';

    /**
     * Traders item amount
     * @var float
     *
     * @ORM\Column(name="traders_amount", type="float", precision=18, scale=2, nullable=false)
     * @Groups({"public"})
     */
    private $tradersAmount = '0.00';

    /**
     * Discount item amount
     * @var float
     *
     * @ORM\Column(name="discount_amount", type="float", precision=18, scale=2, nullable=false)
     * @Groups({"public"})
     */
    private $discountAmount = '0.00';

    /**
     * Total item amount
     * @var float
     *
     * @ORM\Column(name="total_amount", type="float", precision=18, scale=2, nullable=false)
     * @Groups({"public"})
     */
    private $totalAmount = '0.00';

    /**
     * Base item cost
     * @var float
     *
     * @ORM\Column(name="base_cost", type="float", precision=18, scale=2, nullable=false)
     * @Groups({"public"})
     */
    private $baseCost = '0.00';

    /**
     * Custom item cost
     * @var float
     *
     * @ORM\Column(name="custom_cost", type="float", precision=18, scale=2, nullable=false)
     * @Groups({"public"})
     */
    private $customCost = '0.00';

    /**
     * Total item cost
     * @var float
     *
     * @ORM\Column(name="total_cost", type="float", precision=18, scale=2, nullable=false)
     * @Groups({"public"})
     */
    private $totalCost = '0.00';

    /**
     * External ID
     * @var integer
     *
     * @ORM\Column(name="external_id", type="integer", nullable=false)
     * @Groups({"public"})
     */
    private $externalId = 0;

    /**
     * Attributes object
     * @ORM\OneToMany(targetEntity="OrderItemAttribute", fetch="EAGER", mappedBy="orderItem", cascade={"all"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * @Groups({"public"})
     */
    private $attributes;

    /**
     * Custom size object
     * @ORM\OneToMany(targetEntity="CustomSize", fetch="EAGER", mappedBy="orderItem", cascade={"all"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * @Groups({"public"})
     */
    private $customSize;

    /**
     * Order item files if exist
     * @var OrderItemFile[]
     *
     * @ORM\OneToMany(targetEntity="OrderItemFile", fetch="EAGER", mappedBy="orderItem", cascade={"all"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * @Groups({"public"})
     */
    private $files;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attributes = new ArrayCollection();
        $this->customSize = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * @param string $notice
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;
    }

    /**
     * @return float
     */
    public function getPriceNet()
    {
        return $this->priceNet;
    }

    /**
     * @param float $priceNet
     */
    public function setPriceNet($priceNet)
    {
        $this->priceNet = $priceNet;
    }

    /**
     * @return int
     */
    public function getTaxValue()
    {
        return $this->taxValue;
    }

    /**
     * @param int $taxValue
     */
    public function setTaxValue($taxValue)
    {
        $this->taxValue = $taxValue;
    }

    /**
     * @return int
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param int $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     */
    public function getOrderStatusName()
    {
        return $this->orderStatusName;
    }

    /**
     * @param string $orderStatusName
     */
    public function setOrderStatusName($orderStatusName)
    {
        $this->orderStatusName = $orderStatusName;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Product
     */
    public function getProductSlug()
    {
        return $this->getProduct()->getSlug();
    }

    /**
     * @param  Product   $product
     * @return OrderItem
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return int
     */
    public function getEditorId()
    {
        return $this->editorId;
    }

    /**
     * @param int $editorId
     */
    public function setEditorId($editorId)
    {
        $this->editorId = $editorId;
    }

    /**
     * @return string
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * @param string $informations
     */
    public function setInformations($informations)
    {
        $this->informations = $informations;
    }

    /**
     * @return boolean
     */
    public function getPriceBlock()
    {
        return $this->priceBlock;
    }

    /**
     * @param boolean $priceBlock
     */
    public function setPriceBlock($priceBlock)
    {
        $this->priceBlock = $priceBlock;
    }

    /**
     * @return int
     */
    public function getPricelistId()
    {
        return $this->pricelistId;
    }

    /**
     * @param int $pricelistId
     */
    public function setPricelistId($pricelistId)
    {
        $this->pricelistId = $pricelistId;
    }

    /**
     * @return float
     */
    public function getDiscountValue()
    {
        return $this->discountValue;
    }

    /**
     * @param float $discountValue
     */
    public function setDiscountValue($discountValue)
    {
        $this->discountValue = $discountValue;
    }

    /**
     * @return string
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    /**
     * @param string $discountType
     */
    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set shelfId
     *
     * @param  integer   $shelfId
     * @return OrderItem
     */
    public function setShelfId($shelfId)
    {
        $this->shelfId = $shelfId;

        return $this;
    }

    /**
     * Get shelfId
     *
     * @return integer
     */
    public function getShelfId()
    {
        return $this->shelfId;
    }

    /**
     * Set shopName
     *
     * @param  string    $shopName
     * @return OrderItem
     */
    public function setShopName($shopName)
    {
        $this->shopName = $shopName;

        return $this;
    }

    /**
     * Get shopName
     *
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * Set acceptedAt
     *
     * @param  \DateTime $acceptedAt
     * @return OrderItem
     */
    public function setAcceptedAt($acceptedAt)
    {
        $this->acceptedAt = $acceptedAt;

        return $this;
    }

    /**
     * Get acceptedAt
     *
     * @return \DateTime
     */
    public function getAcceptedAt()
    {
        return $this->acceptedAt;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return OrderItem
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime $updatedAt
     * @return OrderItem
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Set baseAmount
     *
     * @param  float     $baseAmount
     * @return OrderItem
     */
    public function setBaseAmount($baseAmount)
    {
        $this->baseAmount = $baseAmount;

        return $this;
    }

    /**
     * Get baseAmount
     *
     * @return float
     */
    public function getBaseAmount()
    {
        return $this->baseAmount;
    }

    /**
     * Set tradersAmount
     *
     * @param  float     $tradersAmount
     * @return OrderItem
     */
    public function setTradersAmount($tradersAmount)
    {
        $this->tradersAmount = $tradersAmount;

        return $this;
    }

    /**
     * Get tradersAmount
     *
     * @return float
     */
    public function getTradersAmount()
    {
        return $this->tradersAmount;
    }

    /**
     * Set discountAmount
     *
     * @param  float     $discountAmount
     * @return OrderItem
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    /**
     * Get discountAmount
     *
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * Set totalAmount
     *
     * @param  float     $totalAmount
     * @return OrderItem
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set baseCost
     *
     * @param  float     $baseCost
     * @return OrderItem
     */
    public function setBaseCost($baseCost)
    {
        $this->baseCost = $baseCost;

        return $this;
    }

    /**
     * Get baseCost
     *
     * @return float
     */
    public function getBaseCost()
    {
        return $this->baseCost;
    }

    /**
     * Set customCost
     *
     * @param  float     $customCost
     * @return OrderItem
     */
    public function setCustomCost($customCost)
    {
        $this->customCost = $customCost;

        return $this;
    }

    /**
     * Get customCost
     *
     * @return float
     */
    public function getCustomCost()
    {
        return $this->customCost;
    }

    /**
     * Set totalCost
     *
     * @param  float     $totalCost
     * @return OrderItem
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * Get totalCost
     *
     * @return float
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set externalId
     *
     * @param  integer   $externalId
     * @return OrderItem
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return integer
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Add attribute
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\OrderItemAttribute $attribute
     * @return Order
     */
    public function addAttribute(\Mnumi\Bundle\OrderBundle\Entity\OrderItemAttribute $attribute)
    {
        $this->attributes->add($attribute);
        $attribute->setOrderItem($this);

        return $this;
    }

    /**
     * Remove attribute
     *
     * @param \Mnumi\Bundle\OrderBundle\Entity\OrderItems $attribute
     */
    public function removeAttribute(\Mnumi\Bundle\OrderBundle\Entity\OrderItemAttribute $attribute)
    {
        $this->attributes->removeElement($attribute);
    }

    /**
     * Get attributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Add customsizes
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\CustomSize $customsize
     * @return OrderItem
     */
    public function addCustomSize(\Mnumi\Bundle\OrderBundle\Entity\CustomSize $customsize)
    {
        $this->customSize->add($customsize);
        $customsize->setOrderItem($this);

        return $this;
    }

    /**
     * Remove customsizes
     *
     * @param \Mnumi\Bundle\OrderBundle\Entity\CustomSize $customsize
     */
    public function removeCustomSize(\Mnumi\Bundle\OrderBundle\Entity\CustomSize $customsize)
    {
        $this->customSize->removeElement($customsize);
    }

    /**
     * Get customsizes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomsize()
    {
        return $this->customSize;
    }

    /**
     * Add File
     *
     * @param OrderItemFile $file
     *
     * @return OrderItem
     */
    public function addFile(OrderItemFile $file)
    {
        $file->setOrderItem($this);
        $this->files->add($file);

        return $this;
    }

    /**
     * Remove file
     *
     * @param OrderItemFile $file
     */
    public function removeFile(OrderItemFile $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set accepted date for this order
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function prePersist()
    {
        if($this->getOrderStatusName() == 'new' &&
                is_null($this->getAcceptedAt())
        )
        {
            $this->setAcceptedAt(new \DateTime("now"));
        }
    }

    /**
     * Returns relative files path for the given OrderItem
     * This method was ported from S1 Order Class.
     *
     * @return string Path to directory containing Files of the current OrderItem
     */
    public function getFilePath()
    {
        return sprintf('files/%s/%d/',
            $this->getCreatedAt()->format('Y/m/d'),
            $this->getId()
        );
    }

    /**
     * Prepares files directory structure for OrderItem files.
     *
     * @param  $dataDir Prefix for relative path, which should point to data directory
     * @throws IOException
     */
    public function preparePath($dataDir)
    {
        $fs = new Filesystem();
        $filepath = $dataDir . '/' . $this->getFilePath();

        if (!$fs->exists($filepath)) {
            $fs->mkdir($filepath, 0777);
        }
    }
}
