<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Mnumi\Bundle\OrderBundle\Exception\FileMetadataNotFoundException;
use Mnumi\Bundle\OrderBundle\Exception\FileMetadataBadNumberOfCharacteristicsException;

/**
 * OrderItemFile (Old File)
 *
 * @ORM\Table(name="file", indexes={@ORM\Index(name="fk_file_order1_idx", columns={"order_id"})})
 * @ORM\Entity
 */
class OrderItemFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="notice", type="text", nullable=true)
     */
    private $notice;

    /**
     * @var string
     *
     * @ORM\Column(name="metadata", type="text", nullable=true)
     */
    private $metadata;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Exclude
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     * @Serializer\Exclude
     */
    private $updatedAt;

    /**
     * @var OrderItem
     *
     * @ORM\ManyToOne(targetEntity="OrderItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude
     */
    private $orderItem;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param  string $filename
     * @return File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set notice
     *
     * @param  string $notice
     * @return File
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Get notice
     *
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * Set metadata
     *
     * @param  string $metadata
     * @return File
     */
    protected function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return File
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime $updatedAt
     * @return File
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set order
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem
     * @return File
     */
    public function setOrderItem(\Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem = null)
    {
        $this->orderItem = $orderItem;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Mnumi\Bundle\OrderBundle\Entity\OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * Checks if metadata attribute exists.
     *
     * @param  string  $name name of the metadata attribute to check for
     * @return boolean
     */
    public function hasMetadataAttribute($name)
    {
        try {
            $this->getMetadataAttribute($name);

            return true;
        } catch (\Exception $e) {
            // Do nothing, return false
        }

        return false;
    }

    /**
     * Gets metadata attribute value.
     *
     * @param  string                        $name name of the metadata attribute to change
     * @return string
     * @throws FileMetadataNotFoundException
     */
    public function getMetadataAttribute($name)
    {
        $metadata = $this->getMetadata();

        if (preg_match('/('.$name.':)[\s]?\'(.*)\'(\n)/', $metadata, $matches)) {
            return $matches[2];
        }

        throw new FileMetadataNotFoundException(sprintf(
            "OrderItemFile metadata: %s, for OrderItem: %d has not been found",
            $name,
            $this->getOrderItem()->getId()
        ));
    }

    /**
     * Replaces or adds new attribute to metadata (in yaml format).
     *
     * @param string $name
     * @param string $value
     */
    public function setMetadataAttribute($name, $value)
    {
        $metadata = $this->getMetadata();

        if ($this->hasMetadataAttribute($name)) {
            $metadata = preg_replace('/('.$name.':)[\s]?\'(.*)\'(\n)/', '$1 \''.$value.'\'$3', $metadata);
        } else {
            $metadata .= $name.': \''.$value."'\n";
        }

        $this->setMetadata($metadata);
    }

    /**
     * Deletes metadata attribute with the given name.
     *
     * @param string $attribute
     */
    public function deleteMetadataAttribute($name)
    {
        $metadata = $this->getMetadata();

        if ($this->hasMetadataAttribute($name)) {
            $metadata = preg_replace('/('.$name.'(.*)\n)/', '', $metadata);

            $this->setMetadata($metadata);
        }
    }

    /**
     * Sets OrderItemFile metadata based on a given File.
     * Metadata are stored in yml format.
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file
     *
     * @throws ProcessFailedException                          when identifying file metadata couldn't be executed successfully
     * @throws FileMetadataBadNumberOfCharacteristicsException when returned number of metadatas is different than expected
     */
    public function setMetadataFromFile(\Symfony\Component\HttpFoundation\File\File $file)
    {
        $filepath = $file->getRealPath();

        $process = new Process(sprintf('identify -format "%s" %s[0]', '%m:%[colorspace]:%w:%h', $filepath));
        $process->mustRun();

        $metadatas = explode(':', $process->getOutput());

        if (count($metadatas) < 4) {
            throw new FileMetadataBadNumberOfCharacteristicsException(sprintf("There must be 4 metadatas. Returned: %d", count($metadata)));
        }

        $this->setMetadataAttribute('format', $metadatas[0]);
        $this->setMetadataAttribute('colorspace', $metadatas[1]);
        $this->setMetadataAttribute('widthPx', $metadatas[2]);
        $this->setMetadataAttribute('heightPx', $metadatas[3]);

        $nrOfPages = 1;
        if ($metadatas[0] == 'PDF') {
            $nrOfPages = trim(exec('pdfinfo '.$filepath.' | grep Pages | cut -d ":" -f 2'));
        }

        $this->setMetadataAttribute('nrOfPages', $nrOfPages);
    }
}
