<?php

namespace Mnumi\Bundle\OrderBundle\Text\Pattern;


/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PatternReplaceException class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class PatternReplaceException extends \Exception
{

} 