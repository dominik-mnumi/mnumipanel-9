<?php

namespace Mnumi\Bundle\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * ClientUser
 *
 * @ORM\Table(name="client_user",
 *     uniqueConstraints={@ORM\UniqueConstraint(columns={"client_id", "user_id"})},
 *     indexes={@ORM\Index(columns={"client_id"}),
 *              @ORM\Index(columns={"client_user_permission_id"}),
 *              @ORM\Index(columns={"user_id"})
 * })
 * @ORM\Entity
 */
class ClientUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var \ClientUserPermission
     *
     * @ORM\ManyToOne(targetEntity="ClientUserPermission")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_user_permission_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude
     */
    private $clientUserPermission = 1;

    /**
     * @var \Mnumi\Bundle\RestServerBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\RestServerBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function __toString()
    {
        return '';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param  \Mnumi\Bundle\RestServerBundle\Entity\User $user
     * @return ClientUser
     */
    public function setUser(\Mnumi\Bundle\RestServerBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Mnumi\Bundle\RestServerBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set clientUserPermission
     *
     * @param  \Mnumi\Bundle\ClientBundle\Entity\ClientUserPermission $clientUserPermission
     * @return ClientUser
     */
    public function setClientUserPermission(\Mnumi\Bundle\ClientBundle\Entity\ClientUserPermission $clientUserPermission = null)
    {
        $this->clientUserPermission = $clientUserPermission;

        return $this;
    }

    /**
     * Get clientUserPermission
     *
     * @return \Mnumi\Bundle\ClientBundle\Entity\ClientUserPermission
     */
    public function getClientUserPermission()
    {
        return $this->clientUserPermission;
    }

    /**
     * Set client
     *
     * @param  \Mnumi\Bundle\ClientBundle\Entity\Client $client
     * @return ClientUser
     */
    public function setClient(\Mnumi\Bundle\ClientBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Mnumi\Bundle\ClientBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
