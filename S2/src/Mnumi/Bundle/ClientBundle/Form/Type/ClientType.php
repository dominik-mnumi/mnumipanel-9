<?php

namespace Mnumi\Bundle\ClientBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
                    'fullname',
                    'text',
                    array(
                        'description' => 'Client full name',
                    )
                )
                ->add(
                    'city',
                    'text',
                    array(
                        'description' => 'City name',
                    ))
                ->add(
                    'postcode',
                    'text',
                    array(
                        'description' => 'Post code',
                    ))
                ->add(
                    'street',
                    'text',
                    array(
                        'description' => 'Street',
                    ))
                ->add(
                    'country',
                    'text',
                    array(
                        'description' => 'Country (eg. PL, GB, DE)',
                    ))
                ->add(
                    'creditLimitAmount',
                    'number',
                    array(
                        'description' => 'Client credit limit amount',
                    ))
                ->add(
                    'users',
                    'collection', array(
                        'type' => new ClientUserType(),
                        'prototype' => true,
                        'allow_add' => true,
                        'allow_delete' => false,
                        'description' => 'Users array'
                    )
                )
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'client';
    }
}
