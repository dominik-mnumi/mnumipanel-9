<?php
/**
 * Created by IntelliJ IDEA.
 * User: dominik
 * Date: 19.03.15
 * Time: 09:49
 */
namespace Panel\OAuth2Bundle\Controller;

use FOS\OAuthServerBundle\Entity\AccessToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ResourceController extends Controller
{
    /**
     * Get user data by access_token
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getAction(Request $request)
    {
        if($request->get('access_token'))
        {
            /** @var \Panel\OAuth2Bundle\Entity\OAuth2AccessToken $token */
            $token = $this->getDoctrine()->getRepository('Panel\OAuth2Bundle\Entity\OAuth2AccessToken')->findOneByToken($request->get('access_token'));

            if (!$token) {
                throw new AccessDeniedException('Token is required');
            }
            
            if ($token->getExpiresAt() < time())
            {
                throw new AccessDeniedException('Token expired');
            }

            $user = $token->getUser();
            if (!$user instanceof UserInterface) {
                throw new AccessDeniedException('This user does not have access to this section.');
            }

            $json = array();
            if($user)
            {
                $json = array (
                    'name' => $user->getFirstName().' '.$user->getLastName().' ('.md5($request->getSchemeAndHttpHost()).')',
                    'email' => $user->getEmailAddress()
                );
            }
        } else {
            $json = array("error" => "token is required");
            
        }
        return new JsonResponse($json);
    }
}