<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Mnumi\Bundle\RestServerBundle\MnumiRestServerBundle(),
            new Mnumi\Bundle\Symfony1SessionBundle\MnumiSymfony1SessionBundle(),
            new Mnumi\Bundle\ProductBundle\MnumiProductBundle(),
            new Mnumi\Bundle\OrderBundle\MnumiOrderBundle(),
            new Mnumi\Bundle\UserBundle\MnumiUserBundle(),
            new Mnumi\Bundle\ClientBundle\MnumiClientBundle(),
            new Mnumi\Bundle\CarrierBundle\MnumiCarrierBundle(),
            new Mnumi\Bundle\PaymentBundle\MnumiPaymentBundle(),
            new Mnumi\Bundle\AttributeBundle\MnumiAttributeBundle(),
            new Mnumi\Bundle\CalculationBundle\MnumiCalculationBundle(),
            new Mnumi\Bundle\WizardBundle\MnumiWizardBundle(),
            new Mnumi\Bundle\InvoiceBundle\MnumiInvoiceBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),

            /**
             * OAuth2
             */
            new FOS\OAuthServerBundle\FOSOAuthServerBundle(),
            new Panel\OAuth2Bundle\PanelOAuth2Bundle(),

        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
//            $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Liip\FunctionalTestBundle\LiipFunctionalTestBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getBaseAppDir().'/config/config_'.$this->getEnvironment().'.yml');
    }

    public function getCacheDir()
    {
        return dirname($this->getBaseAppDir()).'/../cache/'.$this->getEnvironment().'/cache';
    }

    public function getLogDir()
    {
        return dirname($this->getBaseAppDir()).'/../log/'.$this->getEnvironment().'/logs';
    }

    /**
     * Get base app directory
     *
     * @return string
     */
    private function getBaseAppDir()
    {
        return (isset($_SERVER["MNUMICORE_ROOT"]))
            ? $_SERVER["MNUMICORE_ROOT"].'/../S2/app'
            : __DIR__
        ;
    }
}
