<table class="table invoice_table">
    <thead>
        <tr>
            <th style="width: 1%"><?php echo __('No.'); ?></th>
            <th><?php echo __('Name of product / service'); ?></th>
            <th style="width: 5%"><?php echo __('Qty'); ?></th>
            <th style="width: 5%"><?php echo __('Unit'); ?></th>          
            <th style="width: 10%"><?php echo __('Price net'); ?></th> 
            <th style="width: 10%"><?php echo __('Net value'); ?></th>                                      
            <th style="width: 10%"><?php echo __('Discount'); ?></th>  
            <th style="width: 10%"><?php echo __('Net value (discount)'); ?></th>
            <th style="width: 5%"><?php echo __('Tax'); ?></th>  
            <th style="width: 10%"><?php echo __('VAT value'); ?></th>     
            <th style="width: 10%"><?php echo __('Total'); ?></th>
            <th style="width: 1%"><?php echo __('Actions'); ?></th>
        </tr>
    </thead>  
    <tbody class="items">
        <?php foreach($form['item'] as $key => $invoiceItemForm): ?>
        <tr class="invoice_item_row <?php echo $invoiceItemForm['id']->getValue() && $form->getObject()->isCorrection() ? 'correction-row' : 'added-item-row'; ?>" style="display: <?php if(!$form['item'][$key]['valid']->getValue()) echo 'none'; ?>;">
            <td class="item-no-td"></td>
            <td>
                <p>
                    <?php echo $invoiceItemForm['id']->render(); ?>
                    <?php echo $invoiceItemForm['valid']->render(); ?>
                    <?php echo $invoiceItemForm['description']->render(); ?>
                </p>
            </td>
            <td><p><?php echo $invoiceItemForm['quantity']->render(); ?></p></td> 
            <td><p><?php echo $invoiceItemForm['unit_of_measure']->render(); ?></p></td>                                            
            <td class="item_calculate_price_net_1_item"><p><?php echo $invoiceItemForm['price_net']->render(); ?></p></td>  
            <td class="item_calculate_price_net"></td> 
            <td class="item_calculate_price_discount"><p><?php echo $invoiceItemForm['price_discount']->render(); ?></p></td>
            <td  class="item_calculate_price_net_after_discount"></td>
            <td class="item_calculate_tax"><p><?php echo $invoiceItemForm['tax_value']->render(); ?></p></td> 
            <td class="item_calculate_tax_amount"></td>
            <td class="item_calculate_price_gross"></td>
            <td class="item_actions ">
                <?php echo image_tag("icons/fugue/cross-circle.png", 
                    array('width' => 16, 
                        'height' => 16, 
                        'class' => 'item-delete-button')); ?>          
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<?php if(!$form->getObject()->hasChildren()) : ?>
<div style="text-align: right;">
    <button id="item-add-button" type="button"><?php echo __('Add entry'); ?></button>
</div>
<?php endif ?>