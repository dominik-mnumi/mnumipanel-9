<article class="container_12 invoice_message_container">
    <ul class="message error grid_12">
        <li><?php echo __('Unable to create Invoice, because there are unfinished Orders assigned to it.') ?>
            <?php foreach($invoiceItems as $item):?>
                <div>
                    <a title="<?php echo $item->getDescription();?>" href="<?php echo url_for('orderListEdit', array('id' => $item->getOrderId()))?>">
                        <?php echo $item->getDescription();?>
                    </a>
                </div>
            <?php endforeach;?>
        </li>

        <li class="close-bt"></li>
    </ul>
</article>
<div class="clear"></div>