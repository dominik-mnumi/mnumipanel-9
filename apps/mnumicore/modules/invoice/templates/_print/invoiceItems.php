<table cellspacing="0" class="invoice-items-list">
    <tbody>
        <!-- invoice items header -->
        <?php include_partial('invoice/print/invoiceItemHeader',
                array('isCorrection' => $isCorrection)); ?>

        <!-- invoice items rows -->
        <?php foreach($invoiceObj->getInvoiceItems() as $key => $rec): ?>
        <?php include_partial('invoice/print/invoiceItemRow',
                array('invoiceItemObj' => $rec, 'key' => $key + 1)); ?>
        <?php endforeach; ?>
        <!-- total prices -->
        <?php if(isset($showTotal)): ?>
        <?php include_partial('invoice/print/totalPrices',
                array('invoiceObj' => $invoiceObj,
                    'isCorrection' => $isCorrection,
                    'groupPriceArr' => $groupPriceArr)); ?>
        <?php endif; ?>
    </tbody>
</table>
