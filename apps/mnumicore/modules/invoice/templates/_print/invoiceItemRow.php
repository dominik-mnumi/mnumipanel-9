<tr>
    <td><?php echo $key; ?></td>
    <td><?php echo $invoiceItemObj->getDescription(); ?></td>
    <td class="td-center"><?php echo $invoiceItemObj->getQuantity(); ?> <?php echo $invoiceItemObj->getUnit()->getTranslatedName(); ?></td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $invoiceItemObj->getPriceNet())); ?></td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $invoiceItemObj->getPriceNetTotal())); ?></td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $invoiceItemObj->getPriceDiscount())); ?></td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $invoiceItemObj->getPriceNetTotalAfterDiscount())); ?></td>
    <td class="td-center"><?php echo $invoiceItemObj->getTaxValue(); ?></td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $invoiceItemObj->getTaxAmount())); ?></td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $invoiceItemObj->getPriceGrossTotal())); ?></td>
</tr>
