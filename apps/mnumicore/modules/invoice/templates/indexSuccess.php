<?php echo include_partial('dashboard/message'); ?>

<article class="container_12">
<?php include_partial('global/filterForm', $tableOptions) ?>
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('Invoices'); ?>              
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>
