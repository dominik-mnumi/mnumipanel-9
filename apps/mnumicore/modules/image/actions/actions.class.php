<?php

/**
 * image actions.
 *
 * @package    mnumicore
 * @subpackage image
 * @author     Adam Marchewicz <adam.marchewicz@itme.eu>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class imageActions extends sfActions
{
   /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    { 
        $fileName    = $request->getParameter('fileName');
        $type        = $request->getParameter('type');
        $width       = $request->getParameter('width');
        $orginalFile = 'uploads/'.$type.'/default/'.$fileName;
        $fileDir     = 'uploads/'.$type.'/'.$width.'/';
        $filePath    = $fileDir.$fileName;
        
        // if there is no original file
        if(!file_exists($orginalFile))
        {
            $this->forward404();
        }

        // if default file then response
        if($this->getContext()->getRouting()->getCurrentRouteName() == 'imageHandleDefault')
        {
            $img = new sfImage($orginalFile);
        
            $response = $this->getResponse();
            $response->setContentType($img->getMIMEType());
            $response->setContent($img);
            
            return sfView::NONE;
        }
        
        $this->setLayout(false);
        
        if(!file_exists($filePath))
        {
            // if dir does not exist - create it
            if(!is_dir($fileDir))
            {
                mkdir($fileDir);
                chmod($fileDir, 0777);
            }
            
            $thumbnail = new sfImage($orginalFile, mime_content_type($orginalFile));
            $thumbnail->thumbnail($width, $width, 'scale');
            $thumbnail->saveAs($filePath);
        }
        
        $img = new sfImage($filePath);
        
        $response = $this->getResponse();
        $response->setContentType($img->getMIMEType());
        $response->setContent($img);
        
        return sfView::NONE;
    }
}
