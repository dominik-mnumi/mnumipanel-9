<script type="text/javascript">
/**
 * Group create form.
 */ 
$("#dialog-create-group-click").click(function() 
{                          
    $.modal(
    {
        content: $("#dialog-create-group-form"),
        title: '<?php echo __('Add role'); ?>',
        maxWidth: 700,
        maxHeight: 450,
        resizable: false,
        onOpen: function()
        {
            var inputSelector = $(".modal-content #dialog-create-group-form input[type=text]");
            
            var interval = setInterval(
            function()
            {
                if(!inputSelector.first().is(":focus"))
                {
                    $(inputSelector).first().focus();
                    clearInterval(interval);
                }
            }
            , 
            700);
        },
        buttons: 
        {
            '<?php echo __('Save'); ?>': function(win)
            {
                var form = $(".modal-content #dialog-create-group-form");
                var dataInput = form.serialize();
                var dataOutput;
                var uri =  form.attr('action');
                var method = form.attr('method');
                $.ajax(
                {
                    type: method,
                    url: uri,
                    data: dataInput,
                    dataType: 'json',
                    error: function(xhr, ajaxOptions, thrownError)
                    {
                        alert('<?php echo __('There was error. Please try again later.') ?>');
                    },
                    success: function(dataOutput)
                    {
                        if('success' == dataOutput.result.status)
                        {         
                            if(0 != dataOutput.result.error.errorArray)
                            {
                                var errorlist = renderError(dataOutput.result.error.errorArray);

                                $(".modal-content #dialog-create-group-form .validateTips").html(errorlist);
                            }
                            else
                            {
                                win.closeModal();
                                
                                var href = "<?php echo url_for('permission'); ?>"
                                window.location.href = href;               
                            }        
                        }       
                    }
                });
            },
            '<?php echo __('Close'); ?>': function(win) 
            {
                win.closeModal();
            }
        }
    });      
});

/**
 * Group edit form.
 */
$(".dialog-edit-group-click").click(function() 
{     
    // first gets edit form content
    $.ajax(
    {
        type: 'GET',
        url: '<?php echo url_for('reqPermissionGetEditForm'); ?>',
        data: { groupId: $(this).attr('data-group-id') },
        error: function(xhr, ajaxOptions, thrownError)
        {
            alert('<?php echo __('There was error. Please try again later.') ?>');
        },
        success: function(dataOutput)
        {
            $("#editFormContainer").html(dataOutput);
            
            // opens modal
            $.modal(
            {
                content: $("#dialog-edit-group-form"),
                title: '<?php echo __('Edit role'); ?>',
                maxWidth: 700,
                maxHeight: 450,
                resizable: false,
                onOpen: function()
                {
                    var inputSelector = $(".modal-content #dialog-edit-group-form input[type=text]");

                    var interval = setInterval(
                    function()
                    {
                        if(!inputSelector.first().is(":focus"))
                        {
                            $(inputSelector).first().focus();
                            clearInterval(interval);
                        }
                    }
                    , 
                    700);
                },
                buttons: 
                {
                    '<?php echo __('Save'); ?>': function(win) 
                    {
                        var form = $(".modal-content #dialog-edit-group-form");
                        var dataInput = form.serialize();
                        var dataOutput;
                        var uri =  form.attr('action');
                        var method = form.attr('method');
                        $.ajax(
                        {
                            type: method,
                            url: uri,
                            data: dataInput,
                            dataType: 'json',
                            error: function(xhr, ajaxOptions, thrownError)
                            {
                                alert('<?php echo __('There was error. Please try again later.') ?>');
                            },
                            success: function(dataOutput)
                            {
                                if('success' == dataOutput.result.status)
                                {         
                                    if(0 != dataOutput.result.error.errorArray)
                                    {
                                        var errorlist = renderError(dataOutput.result.error.errorArray);

                                        $(".modal-content #dialog-edit-group-form .validateTips").html(errorlist);
                                    }
                                    else
                                    {
                                        win.closeModal();

                                        var href = "<?php echo url_for('permission'); ?>"
                                        window.location.href = href;               
                                   }
                                }
                            }
                        });
                    },
                    '<?php echo __('Close'); ?>': function(win) 
                    {
                        win.closeModal();
                    }
                }
            });
        }
    });
});
</script>