<div id="tab-fix-<?php echo $key; ?>" class="tab_fixprice_subcontent">
    <?php if(isset($defaultPricelist) && $defaultPricelist != $key) : ?>
    <div style="float: right;">
        <button type="button" class="red delete_fixedprice_subtab"><?php echo __('Remove this tab'); ?></button>
    </div>
    <?php endif;?>
    <?php if(isset($rec)): ?>
    <br />
    <div class="table_container"<?php if($defaultPricelist == $key) { echo ' style="margin-top: 5px;"'; } ?>>
        <table class="table fix_price_table">
            <thead>
                <tr>
                    <th><?php echo __('No. of packages'); ?></th>
                    <th><?php echo __('Package price'); ?></th>
                    <th><?php echo __('Action'); ?></th>
                </tr>
            </thead>
            <?php foreach($rec['fixpriceList'][$key] as $rec2): ?>
            <tr style="<?php if($rec2['price']->getValue() == '' && $rec2['quantity']->getValue() == ''): echo 'display: none;'; endif; ?>">
                <td>
                    <?php echo $rec2['id']->render(); ?>
                    <?php echo $rec2['pricelist_id']->render(); ?>
                    <?php echo $rec2['quantity']->render(); ?>
                </td>
                <td>
                    <?php echo $rec2['price']->render(); ?>
                </td>
                <td>
                    <img class="del_fixedprice_row" height="16" width="16" src="/images/icons/fugue/cross-circle.png">
                </td>
            </tr>
            <?php endforeach; ?>

        </table>
        <div class="message">
            <?php echo __('Maximum number of new entries'); ?> 20
        </div>
    </div>

    <a href="#&tab-8&tab-fix-<?php echo $key; ?>" class="add_new_fixprice_row"><?php echo __('Add new'); ?></a>
    <?php endif; ?>
</div>