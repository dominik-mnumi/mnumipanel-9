<div class="tabs-content" id="tab-<?php echo $key; ?>">
    <?php echo $rec->renderHiddenFields(); ?>
    <p class="required">
        <?php echo $rec['name']->renderLabel(); ?>
        <span>
            <?php echo $rec['name']->render(array('class' => 'full-width')); ?>
            <span class=""></span>
        </span>
    </p>

    <p>
        <?php echo $rec['description']->renderLabel(); ?>
        <span>
            <?php echo $rec['description']->render(); ?>
            <span class=""></span>
        </span>
    </p>

    <p>
        <?php echo $rec['category_id']->renderLabel(); ?>
        <span>
            <?php echo $rec['category_id']->render(array(
                'class' => 'select2 medium-width',
                'data-placeholder' => __('Select category'),
            )); ?>
            <span class=""></span>
        </span>
    </p>
    <p>
        <?php echo $rec['photo']->renderLabel(); ?>
        <span>
            <?php echo $rec['photo']->render(); ?>
            <span class=""></span>
        </span>
    </p>
    <p>
        <?php echo $rec['note']->renderLabel(); ?>
        <span>
            <?php echo $rec['note']->render(array('class' => 'full-width')); ?>
            <span class=""></span>
        </span>
    </p>
    <p>
        <?php echo $rec['external_link']->renderLabel(); ?>
        <span>
            <?php echo $rec['external_link']->render(array('class' => 'full-width')); ?>
            <span class=""></span>
        </span>
    </p>
    <p>
        <?php echo $rec['slug']->renderLabel(); ?>
        <span>
            <?php echo $rec['slug']->render(array('class' => 'full-width')); ?>
            <span class=""></span>
        </span>
    </p>
    <p>
        <?php echo $rec['active']->render(); ?>
        <?php echo $rec['active']->renderLabel(); ?>
    </p>

    <fieldset>
        <legend><?php echo __('Quantity'); ?></legend>

        <p>
            <?php echo $rec['is_static_quantity']->render(array('class' => 'package_order_checkbox_product_form')); ?>
            <?php echo $rec['is_static_quantity']->renderLabel(); ?>
        </p>

        <p>
            <?php echo $rec['simple_calculation']->render(array('class' => 'package_order_checkbox_product_form')); ?>
            <?php echo $rec['simple_calculation']->renderLabel(); ?>
        </p>

        <?php $displayQuantityOptions = (
            (bool) $rec['is_static_quantity']->getValue() ||
            (bool) $rec['simple_calculation']->getValue()
        ); ?>
        <p class="package_order_product_form" style="<?php if(!$displayQuantityOptions): ?>display: none; <?php endif; ?>padding-left: 15px;">
            <?php echo $rec['static_quantity_options']->renderLabel(); ?>
            <?php echo $rec['static_quantity_options']->render(array(
                'class' => 'select2 medium-width',
                'placeholder' => __('Amount...'),
            ), array(
            )); ?>
        </p>
    </fieldset>

    <p>
        <?php echo $rec['uploader_available']->render(); ?>
        <?php echo $rec['uploader_available']->renderLabel(); ?>
    </p>
    <p id="file_required" <?php if(!$rec['uploader_available']->getValue()) echo 'style="display: none;"' ?>>
        <?php echo $rec['file_required']->render(); ?>
        <?php echo $rec['file_required']->renderLabel(); ?>
    </p>

    <p>
        <?php echo $rec['tax_value']->renderLabel(); ?>
        <span>
            <?php echo $rec['tax_value']->render(array(
                    'class' => 'select2 medium-width',
                    'data-placeholder' => __('Choose tax rate'),
                )); ?>
            <span class=""></span>
        </span>
    </p>

    <div id="shops">
        <?php echo $rec['custom_shops']->renderLabel(); ?>
        <?php echo $rec['custom_shops']->render(); ?>
    </div>

</div>


						
