<?php echo include_partial('dashboard/message'); ?>
<form action="" method="post" id="complex_form" enctype="multipart/form-data">
    <div class="clearfix grey-bg" id="control-bar">
        <div class="container_12">
            <div class="float-right">
                <a href="<?php echo url_for('product'); ?>"><button class="red" type="button"><?php echo __('Discard'); ?></button></a>
                <button type="button" onclick="submit();"><img height="16" width="16" src="/images/icons/fugue/tick-circle.png"><?php echo __('Save'); ?></button>
            </div>
        </div>
    </div>
    <article class="container_12">
        <section class="grid_4">
            <?php echo include_component('product', 'navigation', array('catId' => $sf_user->getTreeNavigationId())); ?>
        </section>
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content form">
                    <h1><?php echo __('Edit category'); ?></h1>

                    <fieldset class="grey-bg">
                        <?php echo $categoryEditForm->renderHiddenFields(); ?>
                        <p class="required">
                            <?php echo $categoryEditForm['name']->renderError(); ?>
                            <?php echo $categoryEditForm['name']->renderLabel(); ?>
                            <?php echo $categoryEditForm['name']->render(array('class' => 'full-width')); ?>
                        </p>
                        <p>
                            <?php echo $categoryEditForm['photo']->renderLabel(); ?>
                            <?php echo $categoryEditForm['photo']->render(); ?>
                        </p>
                        <p>
                            <?php echo $categoryEditForm['active']->renderLabel(); ?>
                            <?php echo $categoryEditForm['active']->render(); ?>
                        </p>
                    </fieldset>
                </div>
            </div>
        </section>
    </article>
</form>

<!-- category form -->
<?php include_component('product', 'categoryForm') ?>

<!-- sets slot -->
<?php extendSlot('actionJavascript') ?>
<?php include_partial('product/createJavascript'); ?>
<?php end_slot() ?>
