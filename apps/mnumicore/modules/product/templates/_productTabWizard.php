<div class="tabs-content" id="tab-<?php echo $key; ?>">
    <?php echo $rec['_csrf_token']->render(); ?>
    <?php echo $rec->renderGlobalErrors(); ?>
    <?php if($rec->isNew()): ?>
    <?php echo __('Changes were made in "Size" tab. To create new "Wizard" save is necessary.'); ?>
    <?php else: ?>    
    <?php include_partial('product/wizard/wizardContent', 
            array('rec' => $rec,
                'categoryRootObj' => $categoryRootObj,
                'productForm' => $productForm)); ?>
    <?php endif; ?>
</div>
