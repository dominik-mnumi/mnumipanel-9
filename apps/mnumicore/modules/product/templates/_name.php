<span <?php if(!$row->getActive() || !$row->isActiveInCategory()) echo 'class="inactive-product"' ?>>
    <?php 
    if(!$row->getActive())
    {
       echo stripText($row->getName(), 24).' ('.__('inactive').')';
    }
    else
    {
        echo stripText($row->getName(), 34);
    }
    ?>
</span>