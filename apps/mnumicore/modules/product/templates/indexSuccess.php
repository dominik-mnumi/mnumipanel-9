<?php echo include_partial('dashboard/message'); ?>

<article class="container_12">
    <section class="grid_4">
        <?php echo include_component('product', 'navigation', array('catId' => $sf_user->getTreeNavigationId())); ?>
    </section>
    <section class="grid_8">
        <div class="block-border">
            <div class="block-content form" name="table_form" id="table_form">
                <h1>
                    <?php echo __('Category'); ?>: <?php echo __($catObj->getName()); ?>
                    <?php if ($sf_user->hasCredential('product_create')): ?>
                        <a href="<?php echo url_for('@productAdd'); ?>">
                            <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16">
                            <?php echo __('Add product'); ?>
                        </a>
                    <?php endif; ?>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>

<!-- category form -->
<?php include_component('product', 'categoryForm') ?>

<!-- sets slot -->
<?php extendSlot('actionJavascript') ?>
<?php include_partial('product/createJavascript'); ?>
<?php end_slot() ?>


