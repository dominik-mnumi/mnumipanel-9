<div class="tabs-content" id="tab-<?php echo $key; ?>">
    <?php echo $rec->renderHiddenFields(); ?>
    <p>
        <?php echo $rec['visible']->render(array('class' => 'visible_form_content_checkbox')); ?>
        <?php echo $rec['visible']->renderLabel(); ?>
    </p>
    <div class="visible_form_content" style="<?php echo ($rec['visible']->getValue()) ? '' : 'display: none'?>">
        <p>
            <?php echo $rec['change_label_checkbox']->render(); ?>
            <?php echo $rec['change_label_checkbox']->renderLabel(); ?>
        </p>
        <?php if($rec['change_label_checkbox']->getValue()): ?>
        <p class="change_label_product_form" style="padding-left: 15px;">
            <span>
                <?php echo $rec['label']->render(array('class' => 'medium-width')); ?>
                <span class=""></span>
            </span>
        </p>
        <?php else: ?>  
        <p class="change_label_product_form" style="display: none; padding-left: 15px;">
            <span>
                <?php echo $rec['label']->render(array('class' => 'medium-width')); ?>
                <span class=""></span>
            </span>
        </p>
        <?php endif; ?>
        <p>
            <?php echo $rec['product_field_item']->renderLabel(); ?>
            <span class="relative">
                <?php echo $rec['product_field_item']->render(array('class' => 'multiselect medium-width')); ?>
                <span class=""></span>
            </span>
        </p>

        <p class="available_values_button_p">
            <a href="<?php echo url_for('settingsField', $rec->getFieldset()); ?>" target="settings">
                <?php echo __('Manage options'); ?>
            </a>
        </p>
    </div>
    <p class="default_value_p">
        <?php echo $rec['default_value']->renderLabel(); ?>
        <span class="relative">
            <?php echo $rec['default_value']->render(array('class' => 'medium-width')); ?>
            <span class=""></span>
        </span>
    </p>   
</div>
