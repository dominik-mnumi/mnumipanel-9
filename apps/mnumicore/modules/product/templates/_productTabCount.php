<div class="tabs-content" id="tab-<?php echo $key; ?>">
    <?php echo $rec->renderHiddenFields(); ?>
    <?php echo $rec->renderGlobalErrors(); ?>
    <p>
        <?php echo $rec['visible']->render(array('class' => 'visible_form_content_checkbox')); ?>
        <?php echo $rec['visible']->renderLabel(); ?>
    </p>
    <div class="visible_form_content" style="<?php echo ($rec['visible']->getValue()) ? '' : 'display: none'?>">
        <p>
            <?php echo $rec['change_label_checkbox']->render(); ?>
            <?php echo $rec['change_label_checkbox']->renderLabel(); ?>
        </p>
        <?php if($rec['change_label_checkbox']->getValue()): ?>
        <p class="change_label_product_form" style="padding-left: 15px;">
            <span>
                <?php echo $rec['label']->render(array('class' => 'medium-width')); ?>
                <span class=""></span>
            </span>
        </p>
        <?php else: ?>      
        <p class="change_label_product_form" style="display: none; padding-left: 15px;">
            <span>
                <?php echo $rec['label']->render(array('class' => 'medium-width')); ?>
                <span class=""></span>
            </span>
        </p>
        <?php endif; ?>      
    </div>

    <p class="default_value_p">
        <?php echo $rec['default_value']->renderLabel(); ?>
        <span class="relative">
                <?php echo $rec['default_value']->render(array('class' => 'medium-width')); ?>
            <span class=""></span>
        </span>
    </p>
    <fieldset>
        <legend><?php echo __('Limitation of the entered value'); ?></legend>
        <p>
            <?php echo $rec['min']->renderLabel(); ?>
            <?php echo $rec['min']->render(); ?>
        </p>
        <p>
            <?php echo $rec['max']->renderLabel(); ?>
            <?php echo $rec['max']->render(); ?>
        </p>
    </fieldset>
    <div class="calculate_as_card">
        <?php echo $productForm['calculate_as_card']->renderLabel(); ?>
        <span class="relative">
            <?php echo $productForm['calculate_as_card']->render(); ?>
            <span class=""></span>
        </span>
    </div>
</div>