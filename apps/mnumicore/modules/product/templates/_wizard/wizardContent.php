<?php include_partial('global/alertModalConfirm'); ?>
<?php $sortedWizardColl = $rec->getObject()->getProduct()->getProductSortedWizards(); ?>

<!-- if wizard exists then show option -->
<?php if($sortedWizardColl->count()): ?>
<p>
    <?php echo $productForm['wizard_open']->renderLabel(); ?>
    <span class="relative">
        <?php echo $productForm['wizard_open']->render(); ?>
    </span>
</p>
<?php endif; ?>

<div class="box">
    <p class="mini-infos color-blue bold">
        <?php echo __('You can change order of projects by dragging them in different place.'); ?>
    </p>
</div>

<div class="table_container margin-top10">
    <!-- table with wizards -->  
    <table id="wizard-table" class="table fix_price_table">           
        <thead>
            <tr>
                <th><?php echo __('Sort order'); ?></th>
                <th><?php echo __('Preview'); ?></th>
                <th style="width: 45px;"><?php echo __('Active'); ?></th>
                <th style="width: 45px;"><?php echo __('Action'); ?></th>
            </tr>
        </thead>
    
        <?php foreach($rec['wizard'] as $key => $productWizardForm): ?>
        <tr style="<?php if($productWizardForm['wizard_name']->getValue() == ''){ echo 'display: none;'; } ?>">
            <td class="dragHandle">
                <div class="handler"></div>
            </td>
            <td class="wizard-name-td text-center">
                    <?php if($productWizardForm['wizard_name']->getValue()): ?>
                    <?php $url = WizardManager::getInstance()->generatePreviewUrl(
                        $productWizardForm['wizard_name']->getValue(), 100, 100); ?>
                    <div style="height: 100px; background: center center url('<?php echo $url; ?>') no-repeat;" title="<?php echo __('Sort order'); ?>"></div>
                    <br/>
                    <?php echo $productWizardForm['wizard_name']->getValue(); ?>
                    <?php endif; ?>
                    <?php echo $productWizardForm->renderHiddenFields(); ?>
            </td>
            <td class="text-centered">
            <?php echo $productWizardForm['active']->render() ?>
            </td>
            <td class="wizard-link-td">
                <?php if(isset($sortedWizardColl[$key])): ?>
                <?php echo image_tag('/images/icons/fugue/cross-circle.png', 
                                array('width' => 16, 
                                    'height' => 16, 
                                    'class' => 'wizard-delete'
                                    ,'id' => 'wizard-delete'.$key)); ?>

                <?php echo image_tag('/images/icons/wizard_24.png', 
                                array('width' => 24, 
                                    'height' => 24, 
                                    'class' => 'wizard-edit')); ?>     
                
                <?php echo image_tag('/images/icons/copy.png', 
                                array('width' => 16, 
                                    'height' => 16, 
                                    'class' => 'wizard-copy',
                                    'title' => __('Copy project to other product'))); ?>         
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>

    <div class="wizard-add-button-container">
        <button type="button" id="dialog-add-wizard-click">
            <?php echo __('Add wizard'); ?>
        </button>      
    </div>
    <div class="clear"></div>
    <?php include_partial('product/wizard/initWizardForm', array('rec' => $rec)); ?>
    <?php include_partial('product/wizard/copyWizardForm', 
            array('rec' => $rec,
                'categoryRootObj' => $categoryRootObj)); ?>
</div>

<!-- sets slot -->
<?php extendSlot('actionJavascript'); ?>
<?php include_partial('product/wizard/wizardJavascript'); ?>
<?php end_slot() ?>
