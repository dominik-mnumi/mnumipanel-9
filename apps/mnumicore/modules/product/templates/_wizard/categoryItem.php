<?php if($node->getProductsCount(true) > 0): ?>
<li class="closed">
    <?php if($node->getNode()->getAncestors()->count() == 1): ?>
    <b class="toggle"></b>
    <?php endif; ?>
    <span><?php echo $node->getName(); ?></span>
    <?php if($node->getNode()->hasChildren()): ?>
    <ul class="with-icon">
        <?php foreach($node->getNode()->getChildren() as $node2): ?>
        <?php include_partial('product/wizard/categoryItem',
                array('node' => $node2)); ?>
        <?php endforeach; ?>

	    <?php foreach($node->getAllProducts(false) as $node3): ?>
	    <li>
		    <a href="#" class="copy-wizard-to-product <?= !$node3->getActive() ? 'inactive-product' : ''; ?>" data-product="<?php echo $node3->getId(); ?>"><?php echo $node3->getName(); ?></a>
	    </li>
	    <?php endforeach; ?>
    </ul>
    <?php else: ?>
    <ul class="with-icon">
        <?php foreach($node->getAllProducts() as $node3): ?>
        <li>
            <a href="#" class="copy-wizard-to-product <?= !$node3->getActive() ? 'inactive-product' : ''; ?>" data-product="<?php echo $node3->getId(); ?>"><?php echo $node3->getName(); ?></a>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</li>
<?php endif ?>