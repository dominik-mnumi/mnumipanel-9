<form id="edit-wizard-form" action="<?php echo WizardManager::getEditUrl(); ?>" 
      method="post" 
      style="display: none;" 
      target="MnumiWizard" 
      onsubmit="javascript:popupWizardWindow();">
    <input id="edit-parameter" type="hidden" name="parameter">
    <input id="edit-signature" type="hidden" name="signature">
    <input id="edit-id" type="hidden" name="id">
    <input id="display" type="hidden" name="display" value="manage" />
</form>
