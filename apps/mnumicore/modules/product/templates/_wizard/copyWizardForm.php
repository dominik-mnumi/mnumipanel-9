<div id="dialog-copy-wizard-form" title="<?php echo __('Copy project to other product'); ?>" style="display: none;">
    <div class="loader margin-top50 margin-bottom50">
        <?php echo image_tag('/images/loader.gif'); ?>                               
    </div>
    <div class="copy-wizard-content-form">
        <input type="hidden" name="parameter" id="wizard-copy-parameter" value="" />
        <input type="hidden" name="parameter" id="wizard-copy-signature" value="" />
        <div class="block-border">
            <div class="block-content form no-border padding-top-20px">  
                <!-- category product tree -->
                <?php if($categoryRootObj->getNode()->isRoot()): ?>
                <ul class="collapsible-list with-bg">
                    <!-- main categories -->
                    <?php foreach($categoryRootObj->getNode()->getChildren() as $node): ?>
                    <?php include_partial('product/wizard/categoryItem', 
                            array('node' => $node)); ?>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>   
            </div>  
        </div>
    </div>
</div>

