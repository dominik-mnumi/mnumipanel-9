<script type="text/javascript">
    /**
     * Function adds new row with values in wizard table.
     */
    function addWizard(projectName, wizardPreviewUrl)
    {
        var tr = $("#wizard-table tr:hidden").first();
        tr.show();
        tr.find(".wizard-name-input").val(projectName);
        tr.find(".wizard-valid-input").val(1);
        tr.find(".wizard-name-td").css({ 'background-image': 'url(' + wizardPreviewUrl + ')' }); 
        tr.find(".wizard-link-td").html(
            '<?php echo image_tag('/images/icons/fugue/cross-circle.png', 
                            array('width' => 16, 
                                  'height' => 16,
                                  'class' => 'wizard-delete')); ?>' + 
            '<?php echo image_tag('/images/icons/wizard_24.png', 
                            array('width' => 24, 
                                  'height' => 24,
                                  'class' => 'wizard-edit')); ?>' +
            '<?php echo image_tag('/images/icons/copy.png', 
                                array('width' => 16, 
                                    'height' => 16, 
                                    'class' => 'wizard-copy',
                                    'title' => __('Copy project to other product'))); ?>'); 
                                          
        reorder();
    }
    
    /**
     * Executes refresh action (after wizard edit).
     */
    function editWizard()
    {
        // refreshes image preview
        $("#wizard-table .wizard-name-td").each(function(index)
        {
            var src = $(this).css('background-image');
            $(this).css('background-image', src);
        });
    }
    
    /**
     * Deletes row and sets valid input to 0.
     */
    function deleteWizard(selectorTr)
    {
        selectorTr.find(".wizard-valid-input").val(0);
        selectorTr.find(".wizard-name-input").val('');
        selectorTr.hide();
        
        // moves tr at the end of table
        var parentSelector = selectorTr.parents("tbody");
        selectorTr.appendTo(parentSelector);

        // reorders wizards
        reorder();
    }
    
    /**
     * Reorders wizards.
     */
    function reorder()
    {
        var wizardIndex = 1;
        $("#wizard-table tbody tr:visible").each(
        function()
        {
            $(this).find(".wizard-order").val(wizardIndex);
            wizardIndex++;
        });
    }

    function isInt(x) 
    {
        // if empty
        if(!x)
        {
            return false;
        }
        
        var y = parseInt(x);
        if(isNaN(y)) 
        {
            return false;
        }
        return x == y && x.toString() == y.toString();
    }

    function parseNumber(x)
    {
        return parseFloat(x.replace(",", "."));
    }

    function isNumber(x)
    {
        // if empty
        if(!x)
        {
            return false;
        }
        x = parseNumber(x);

        var y = parseFloat(x);
        if(isNaN(y))
        {
            return false;
        }
        return x == y && x.toString() == y.toString();
    }

    // reorders wizards
    reorder();

    $(function($)
    {
    	/**
         * Validates dynamically inputs od wizard init form.
         */
        function validateWizardForm()
        {
            // get dimensions and prepares other input values
            var width = $(".modal-content #product_field_1_wizard_size_width").val();
            var height = $(".modal-content #product_field_1_wizard_size_height").val();
            var cropTop = $(".modal-content #product_field_1_wizard_crop_top").val();
            var cropBottom = $(".modal-content #product_field_1_wizard_crop_bottom").val();
            var cropLeft = $(".modal-content #product_field_1_wizard_crop_left").val();
            var cropRight = $(".modal-content #product_field_1_wizard_crop_right").val();
 
            var html = '';
            if(!isInt(width))
            {
                html += '<li><?php echo __('Field "%field%" is invalid. Integer type required', array('%field%' => __('Width'))); ?></li>';
            }
            
            if(!isInt(height))
            {
                html += '<li><?php echo __('Field "%field%" is invalid. Integer type required', array('%field%' => __('Height'))); ?></li>';
            }
            
            if(!isNumber(cropTop))
            {
                html += '<li><?php echo __('Field "%field%" is invalid. Number type required', array('%field%' => __('Top'))); ?></li>';
            }
            
             if(!isNumber(cropBottom))
            {
                html += '<li><?php echo __('Field "%field%" is invalid. Number type required', array('%field%' => __('Bottom'))); ?></li>';
            }
            
            if(!isNumber(cropLeft))
            {
                html += '<li><?php echo __('Field "%field%" is invalid. Number type required', array('%field%' => __('Left'))); ?></li>';
            }
            
            if(!isNumber(cropRight))
            {
                html += '<li><?php echo __('Field "%field%" is invalid. Number type required', array('%field%' => __('Right'))); ?></li>';
            }

            if(html == '')
            {
                $(".modal-content #wizard-form-error-ul").hide(); 
                return true;    
            }

            // sets messages in container
            $(".modal-content #wizard-form-error-ul").html(html);
            $(".modal-content #wizard-form-error-ul").show();   
            return false; 
        }
        
        function submitEncodedNewWizardParameter()
        {
            // get dimensions and prepares other input values
            var width = parseInt($(".modal-content #product_field_1_wizard_size_width").val());
            var height = parseInt($(".modal-content #product_field_1_wizard_size_height").val());
            var cropTop = parseNumber($(".modal-content #product_field_1_wizard_crop_top").val());
            var cropBottom = parseNumber($(".modal-content #product_field_1_wizard_crop_bottom").val());
            var cropLeft = parseNumber($(".modal-content #product_field_1_wizard_crop_left").val());
            var cropRight = parseNumber($(".modal-content #product_field_1_wizard_crop_right").val());
            
            var method = 'POST';
            var uri = '<?php echo url_for('reqGetEncodedWizardParameter'); ?>';
             
            // prepares data input
            var cutLines =
                cropLeft + ' ' +
                cropBottom + ' ' +
                parseFloat(width - cropRight) + ' ' +
                parseFloat(height - cropTop)
            ;
            var dataInput = { 'parameter': { 'width': width, 
                                             'height': height,                                      
                                             'cutlines': cutLines,
                                             'action': 'initE',
                                             'backUrl': '<?php echo WizardManager::getNewBackUrl(); ?>' }};

            $.ajax({
                type: method,
                url: uri,
                data: dataInput,
                dataType: 'json',
                error: function(xhr, ajaxOptions, thrownError)
                {
                    alert('<?php echo __('There was error. Please try again later.') ?>');
                },
                success: function(dataOutput)
                {                   
                    submitNewWizardForm(dataOutput);
                }
            });
        }    
        
        /**
         * Submits new wizard form.
         */
        function submitNewWizardForm(dataOutput)
        {
            $("#new-parameter").val(dataOutput['parameter']);
            $("#new-signature").val(dataOutput['signature']);

            if ('id' in dataOutput)
            {
              $("#new-id").val(dataOutput['id']);
            }
             
            // submits
            $("#new-wizard-form").submit();        
        }

        // Initialise the table
        $("#wizard-table").tableDnD(
        {
            onDrop: reorder,
            dragHandle: ".dragHandle"
        });

        // wizard add form
        $("#dialog-add-wizard-click").click(function() 
        {                         
            $.modal({
                content: $("#dialog-add-wizard-form"),
                title: '<?php echo __('Add wizard'); ?>',
                maxWidth: 850,
                maxHeight: 550,
                resizable: false,           
                buttons: 
                {
                    '<?php echo __('Add wizard'); ?>': function(win) 
                    {
                    	if(validateWizardForm())
                        {
                            submitEncodedNewWizardParameter();
                            win.closeModal();
                        }
                    },
                    '<?php echo __('Close'); ?>': function(win) 
                    {
                        win.closeModal();
                    }
                }});
        }); 
        
        // wizard dialog when new wizard is unavailable
        $("#dialog-add-error-wizard-click").live('click', function() 
        {                         
            $.modal({
                content: '<?php echo __('Changes were made in "Size" tab. To create new "Wizard" save is necessary.'); ?>',
                title: '<?php echo __('Add wizard'); ?>',
                maxWidth: 700,
                maxHeight: 450,
                resizable: false,           
                buttons: 
                {                   
                    '<?php echo __('Close'); ?>': function(win) 
                    {
                        win.closeModal();
                    }
                }});
        });   
        
        /**
         * Deletes row on click action.
         */
        $(".wizard-delete").live('click', function()
        {
            
        	if(!alertModalConfirm('<?php echo __('Delete item') ?>', '<h3><?php echo __('Do you really want to delete this item?') ?></h3>', 500, $(this).attr('id'), true))
        	{
            	return false;
        	}
        	deleteWizard($(this).parents("tr"));
        	confirmResults = false;
        });
        
        /**
         * Removes link from add wizard when changes in size select.
         */
        $("#product_field_4_product_field_item").change(function()
        {
            $("#dialog-add-wizard-click").unbind();
            $("#dialog-add-wizard-click").attr("id", "dialog-add-error-wizard-click");
        });

        /**
         * Services list "opened" and "closed".
         */
        $(".collapsible-list li").click(function(e)
        {
	        // stops propagation for other then a
	        if(e.target.tagName != 'A')
	        {
	            e.stopPropagation();
	        }

            if($(this).hasClass('closed'))
            {
                $(this).removeClass('closed');
                $(this).addClass('open');
            }
            else
            {
                $(this).removeClass('open');
                $(this).addClass('closed');
            }
        });

    });
    
    /**
     * Wizard edit
     */
    function submitEncodedEditWizardParameter(projectName)
    {       
        var method = 'POST';
        var uri = '<?php echo url_for('reqGetEncodedWizardParameter'); ?>';

        // prepares data input
        var dataInput = { 'parameter': { 'orderId': projectName,
                                         'action': 'editE',
                                         'backUrl': '<?php echo WizardManager::getEditBackUrl(); ?>' }};

        $.ajax({
            type: method,
            url: uri,
            data: dataInput,
            dataType: 'json',
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert('<?php echo __('There was error. Please try again later.') ?>');
            },
            success: function(dataOutput)
            {    
                $("#edit-parameter").val(dataOutput['parameter']);
                $("#edit-signature").val(dataOutput['signature']);

                if ('id' in dataOutput)
                {
                  $("#edit-id").val(dataOutput['id']);
                }
                
                // submits
                $("#edit-wizard-form").submit();   
            }
        });
    }   
    
    $(".wizard-edit").live('click', function() 
    {   
        var projectName = $(this).parents("tr").find(".wizard-name-input").val();
        submitEncodedEditWizardParameter(projectName);
    }); 


    /**
     * Services "click" event on copy icon.
     */
    $(document).on('click', '.wizard-copy', function(e) 
    {   
        // keep productName as global variable
    	$.projectName = $(this).parents("tr").find(".wizard-name-input").val();
    	
        // sets values to form
        $.modal(
        {
           content: $("#dialog-copy-wizard-form"),
           title: '<?php echo __('Copy project to other product'); ?>',
           maxWidth: 850,
           maxHeight: 550,
           resizable: false,           
           buttons: 
           {             
                '<?php echo __('Close'); ?>': function(win) 
                {
                   win.closeModal();
                }
           }
        });
    }); 

    /**
     * Services wizard copy on selected product.
     */
    $(document).on('click', '.copy-wizard-to-product', function(e)
    {
        e.preventDefault();

        $(".modal-content #dialog-copy-wizard-form .copy-wizard-content-form").hide();
        $(".modal-content #dialog-copy-wizard-form .loader").show();

        // gets url to wizard copy
        var dataInput = {
                projectName: $.projectName,
                productId: $(this).attr('data-product')
        }

        $.ajax({
            type: 'POST',
            url: '<?php echo url_for('reqWizardCopy'); ?>',
            data: dataInput,
            dataType: 'json',
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert('<?php echo __('There was error. Please try again later.') ?>');
            },
            success: function(dataOutput)
            {
                window.location.reload();
            }
        });
    });
</script>
