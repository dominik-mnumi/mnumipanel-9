<script type="text/javascript" language="javascript">
    var toJsSelectFixpriceList = new Array();
    toJsSelectFixpriceList['selectPriceCancel'] = '<?php echo __('Cancel'); ?>';
    toJsSelectFixpriceList['selectPriceOk'] = '<?php echo __('Add fixed price list'); ?>';
    toJsSelectFixpriceList['selectPricelist'] = '<?php echo __('Select fixed price list'); ?>';
    

    var pricelistId;
    var productId;

    //fixed price add new tab
    function addFixedPriceTab()
    {
        var html = $.ajax({
            type: 'GET',
            url: '<?php echo url_for('product/reqAddFixedPriceColl') ?>/nb/' + nb + '/pricelistId/' + pricelistId + '/productId/' + productId,
            async: false
        }).responseText;
        return html;
    }


</script>

<div id="dialog-fixprice-selectlist-form" title="<?php echo __('Select fixed price list'); ?>" class="xxxx" style="display: none;">
    <p class="validateTips"></p>
    <form action="" method="post">
        <?php echo $fixpriceSelectForm['_csrf_token']->render(); ?>
        <fieldset>
            <table>              
                <tr>
                    <td>
                        <?php echo $fixpriceSelectForm['productId']->render(); ?>
                        <?php echo __($fixpriceSelectForm['selectFixpriceList']->renderLabel()); ?>
                    </td>
                    <td><?php echo $fixpriceSelectForm['selectFixpriceList']->render(); ?></td>
                </tr>                               
                <tr>
                    <td colspan="2">
                        <input id="selectFixpriceSubmitButton" style="display: none;" type="submit" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>



<script type="text/javascript" language="javascript">
    $(document).ready(function() {
	  $('.tabs .fix_price_li a').each(function(){
		  if($(this).attr('pricelistId')){
			  $("#selectFixpriceList").children("option[value='" + $(this).attr('pricelistId') + "']").hide();
		  }
	  });
	});

</script>





