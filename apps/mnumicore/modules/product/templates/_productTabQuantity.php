<div class="tabs-content" id="tab-<?php echo $key; ?>">
    <?php echo $rec->renderHiddenFields(); ?>
    <?php echo $rec->renderGlobalErrors(); ?>
    <p>
        <?php echo $rec['visible']->render(array('class' => 'visible_form_content_checkbox')); ?>
        <?php echo $rec['visible']->renderLabel(); ?>
    </p>
    <div class="visible_form_content" style="<?php echo ($rec['visible']->getValue()) ? '' : 'display: none'?>">  
        <p>
            <?php echo $rec['change_label_checkbox']->render(); ?>
            <?php echo $rec['change_label_checkbox']->renderLabel(); ?>
        </p>

        <p class="change_label_product_form" style="<?php if(!$rec['change_label_checkbox']->getValue()): ?>display: none; <?php endif; ?>padding-left: 15px;">
            <span>
                <?php echo $rec['label']->render(array('class' => 'medium-width')); ?>
                <span class=""></span>
            </span>
        </p>
    </div>
    <p class="default_value_p">
        <?php echo $rec['default_value']->renderLabel(); ?>
        <span>
            <?php echo $rec['default_value']->render(array('class' => 'medium-width')); ?>
            <span class=""></span>
        </span>
    </p>
    <fieldset>
        <legend><?php echo __('Limitation of the entered value'); ?></legend>
        <p>
            <?php echo $rec['min']->renderLabel(); ?>
            <?php echo $rec['min']->render(); ?>
        </p>
        <p>
            <?php echo $rec['max']->renderLabel(); ?>
            <?php echo $rec['max']->render(); ?>
        </p>
    </fieldset>
</div>