<li class="">
    <?php if(!$rec->__children->count()): ?>
    <span class=""></span>
    <a href="#" class="parent" val="<?php echo $rec->getId(); ?>"><span><?php echo $rec->getName(); ?></span></a>
    <?php else: ?>
    <span class="toggle"></span>
    <a href="#" class="folder parent" val="<?php echo $rec->getId(); ?>"><span><?php echo $rec->getName(); ?></span></a>
    <ul>
        <?php foreach($rec->__children as $rec2): ?>
        <!-- subcategories -->
        <?php echo include_partial('chooseParentCategoryNavigation', array('rec' => $rec2)); ?>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</li>