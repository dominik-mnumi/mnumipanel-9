<ul class="tabs js-tabs same-height">
    <?php $i = 0 ?>
    <?php foreach($rec['fixpriceList'] as $k => $r): ?>
    <li class="fix_price_li current">
        <a pricelistId ="<?php echo $k ?>" 
           href="#tab-fix-<?php echo  $k ?>" 
           title="<?php echo __($pricelists[$k]); ?>">
            <?php echo __($pricelists[$k]); ?>
        </a>
    </li>
    <?php $i = ($k > $i) ? $k : $i  ?>
    <?php endforeach; ?>
    
    <?php $j = $i +11; $i++; ?>
    <?php for($i; $i < $j; $i++): ?>
    <li class="fix_price_li" style="display: none;">
        <a href="#tab-fix-<?php echo $i; ?>" title=""></a>
    </li>
    <?php endfor; ?>
    <li class="with-margin">
        <a id="dialog-fixprice-selectlist-click" href="#" title="<?php echo __('Add tab'); ?>">
            <?php echo image_tag('icons/add.png', 
                    array('width' => 16, 
                        'height' => 16)); ?>
        </a>
    </li>
</ul>