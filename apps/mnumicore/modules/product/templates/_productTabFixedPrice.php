<?php echo $rec['id']->render(); ?>
<?php echo $rec['_csrf_token']->render(); ?>
<?php echo $rec->renderGlobalErrors(); ?>
<div class="tabs-content" id="tab-<?php echo $key; ?>">
    <?php echo include_partial('productTabFixedPriceList', 
            array('key' => 1, 
                'rec' => $rec, 
                'pricelists' => $pricelists ? $pricelists : null)); ?>
    <?php $i = 0 ?>
    <div class="tabs-content">
        <?php foreach($rec['fixpriceList'] as $k => $r) : ?>
        <?php echo include_partial('productTabFixedPriceSubtabContent', 
                array('key' => $k, 
                    'rec' => $rec,
                    'defaultPricelist' => $defaultPricelist ? $defaultPricelist : null)); ?>
        <?php $i = ($k > $i) ? $k : $i  ?>
        <?php endforeach ?> 
        
        <?php $j = $i +11; $i++; ?>
        <script type="text/javascript" language="javascript">
            var nb = <?php echo $i?>;
        </script>
        <?php for($i; $i < $j; $i++): ?>
        <?php echo include_partial('productTabFixedPriceSubtabContent', array('key' => $i)); ?>
        <?php endfor; ?>        
    </div>
</div>