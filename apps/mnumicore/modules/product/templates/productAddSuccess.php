<script type="text/javascript">
    var emptyValueId = "<?php echo $emptyValueId; ?>" 
</script>
<?php echo include_partial('productDialogSelectFixPriceList', array('fixpriceSelectForm' => $fixpriceSelectForm)); ?>
<?php echo include_partial('dashboard/message'); ?>
<form action="" method="post" id="complex_form" enctype="multipart/form-data">
    <div class="clearfix grey-bg" id="control-bar">
        <div class="container_12">
            <?php echo include_partial('dashboard/formActions', array(
                'route' => 'product',
                'saveAndAdd' => $sf_user->hasCredential('product_create'),
                'withSave' => $sf_user->hasCredential('product_create')
            )); ?>
        </div>
    </div>
    
    <!-- info message -->
    <?php echo include_partial('dashboard/message'); ?>
    
    <article class="container_12">
        <section class="grid_4">
            <?php echo include_component('product', 'navigation', array('catId' => $sf_user->getTreeNavigationId())); ?>
        </section>
        <section class="grid_8">
                <div class="block-border">
                    <div class="block-content form">
                        <h1><?php echo __('Add product'); ?></h1>
                        <?php echo include_partial('formWarningMessage', array('formArray' => $productFormArray)); ?>
                        <div class="columns">
                            <div class="col200pxL-left">
                                <ul class="side-tabs js-tabs same-height">
                                    <?php echo include_partial('productTabList', array('productFormArray' => $productFormArray)); ?>
                                    <li id="new_tab" class="icon-tab with-margin"><a title="<?php echo __('Add tab'); ?>" href="javascript:void(0)"><img height="16" width="16" src="/images/icons/add.png"></a></li>
                                </ul>
                            </div>
                            <div class="col200pxL-right">
                                <?php echo include_partial('productTabContent', 
                                        array('productFormArray' => $productFormArray,
                                            'pricelists' => $pricelists ? $pricelists : null,
                                            'defaultPricelist' => $defaultPricelist ? $defaultPricelist : null,
                                            'categoryRootObj' => $categoryRootObj)); ?>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </article>
</form>

<!-- category form -->
<?php include_component('product', 'categoryForm') ?>

<!-- sets slot -->
<?php extendSlot('actionJavascript') ?>
<?php include_partial('product/createJavascript'); ?>
<?php end_slot() ?>
