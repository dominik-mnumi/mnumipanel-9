<div id="dialog-add-category-form" title="<?php echo __('Add category'); ?>" style ="display: none;">
    <p class="validateTips"></p>
    <form action="<?php echo url_for('productCategoryAdd'); ?>" enctype="multipart/form-data" method="post">
        <?php echo $categoryAddForm['_csrf_token']->render(); ?>
        <fieldset>
            <table>              
                <tr>
                    <td><?php echo $categoryAddForm['name']->renderLabel(); ?></td>
                    <td><?php echo $categoryAddForm['name']->render(array('class' => 'text ui-widget-content ui-corner-all', 'id' => 'catName')); ?></td>
                </tr>                
                <tr>
                    <td colspan="2" style="width: 300px;">
                        <?php echo include_partial('product/chooseParentCategoryAddForm', array('treeColl' => $treeColl)); ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo $categoryAddForm['photo']->renderLabel(); ?></td>
                    <td><?php echo $categoryAddForm['photo']->render(); ?></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php echo $categoryAddForm['parentId']->render(array('id' => 'catParent')); ?>
                        <input id="addCategorySubmitButton" style="display: none;" type="submit" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>









