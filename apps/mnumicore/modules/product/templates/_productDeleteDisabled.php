<?php if($row->canDelete()): ?>
<a class="without-tip delete" title="skasuj" onclick="alertModalConfirm('<?php echo __('Delete item'); ?>', '<h3><?php echo __('Do you really want to delete this item?'); ?></h3>', 500, this.href); return false;" href="<?php echo url_for('productDelete', $row); ?>">
    <?php echo image_tag('icons/fugue/cross-circle.png', array('width' => 16, 'height' => 16)); ?>
</a>
<?php else: ?>
<a class="without-tip delete" title="skasuj" onclick="infoModal('<?php echo __('Delete item'); ?>', '<h3><?php echo __plural('Product cannot be removed because 1 order is currently in progress and using this product.', 'Product cannot be removed because @count orders are currently in progress and using this product.', array('@count' => $row->getOrdersInProcess())); ?></h3>', 500); return false;" href="<?php echo url_for('productDelete', $row); ?>">
    <?php echo image_tag('icons/fugue/cross-circle.png', array('width' => 16, 'height' => 16)); ?>
</a>
<?php endif; ?>
