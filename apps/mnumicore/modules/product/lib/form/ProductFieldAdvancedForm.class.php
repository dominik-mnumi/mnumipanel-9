<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductField form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFieldAdvancedForm extends ProductFieldForm
{
    protected $fieldItemArr;

    public function configure()
    {
        parent::configure();

        $this->loadFieldItemWidget();
        $this->setPostValidatior();

        $this->useFields(array('visible', 'change_label_checkbox', 'label', 
            'required', 'product_field_item', 'default_value'));
    }

    /**
     * @return array
     */
    protected function loadFieldItemWidget()
    {
        $fieldItemColl = $this->getOption('fieldItemColl')
            ? : $this->getObject()
                ->getFieldset()
                ->getFieldItemQuery()
                ->execute();

        $this->setFieldItemArr(FieldItemTable::getFieldItemArray($fieldItemColl));
        $fieldItemArr = $this->getFieldItemArr();

        $this->setWidget(
            'product_field_item',
            new sfWidgetFormDoctrineChoiceGroupedI18N(
                array(
                    'multiple' => true,
                    'model' => 'FieldItem',
                    'group_by' => 'Fieldset',
                    'collection' => $fieldItemColl
                ), // show all field items (with Hidden to: ex. "--Customization--" item)
                array('style' => 'width: 400px; height: 350px;')
            )
        );
        $relationQuery =  $this->getObject()
            ->getProductFieldItemWithFieldItemRelationQuery();
        if($relationQuery !== false) {
            // prepares selected choices with relations
            $productFieldItemArr = ProductFieldItemTable::getFieldItemArray(
                $relationQuery->execute()
            );

            $this->getWidget('product_field_item')->setDefault(array_keys($productFieldItemArr));
            $this->getWidget('product_field_item')->setLabel('Available values');

            $this->setWidget(
                'default_value',
                new sfWidgetFormChoice(
                    array(
                        'choices' => ($this->getObject()->isVisible())?$productFieldItemArr:$fieldItemArr,
                    )
                )
            );
        }

        // validators
        $this->setValidator(
            'product_field_item',
            new sfValidatorChoice(
                array(
                    'choices' => array_keys($this->getFieldItemArr()),
                    'multiple' => true
                ),
                array(
                    'required' => $this->getFieldset()->getLabel() . ' - ' . sfContext::getInstance()->getI18N()->__(
                            '"Available values" are required'
                        )
                )
            )
        );
    }

    /**
     * @return mixed
     */
    public function getFieldItemArr()
    {
        return $this->fieldItemArr;
    }

    /**
     * @param mixed $fieldItemArr
     */
    public function setFieldItemArr($fieldItemArr)
    {
        $this->fieldItemArr = $fieldItemArr;
    }



    protected function setPostValidatior()
    {
        //post validator
        $this->validatorSchema->setPostValidator(
            new sfValidatorAnd(
                array(
                    //checkLabel inherited
                    new sfValidatorCallback(array('callback' => array($this, 'checkLabel'))),
                    new sfValidatorCallback(array('callback' => array($this, 'canDefaultValue')))
                )
            )
        );
    }

    protected function doSave($con = null)
    {
        parent::doSave($con);
        /**
         * If visible get all selected fields with empty.
         */
        if($this['visible']->getValue()) {
            // checks if not required (allows empty field)
            $productFieldItemArrayForm = ($this->getValue('product_field_item')) ? $this->getValue('product_field_item')
                : array();
        } else {
            $productFieldItemArrayForm = ($this->getValue('default_value')) ? array($this->getValue('default_value'))
                : array();
        }

        /**
         * Remove all does not used fields and add new.
         */
        $this->getObject()->replaceItems($productFieldItemArrayForm);

        return $this->getObject();
    }

    public function canDefaultValue($validator, $values)
    {
        $availableArray = $values['product_field_item'];
        $emptyFieldId = FieldItemTable::getInstance()->getEmptyField()->getId();
        
        // if only one or more options then empty option added
        if(count($values['product_field_item']) >= 1 
                && empty($values['required'])
                && !empty($values['visible']))
        {   
            $availableArray = array('-1' => $emptyFieldId);
            $availableArray = $availableArray + $values['product_field_item'];
        }

        $i18N = sfContext::getInstance()->getI18N();
        // if required and default value = empty value
        if(!empty($values['required']) 
                && !empty($values['visible'])
                && in_array($emptyFieldId, $values['product_field_item']))
        {
            $error = new sfValidatorError($validator, $i18N->__('%fieldsetLabel% - Field "Available values" is required and it cannot have empty value', 
                    array('%fieldsetLabel%' => $this->getFieldsetLabel())));
            throw new sfValidatorErrorSchema($validator, array('type' => $error));
        }

        // if not visible and default value = empty value
        if(empty($values['visible']) && $values['default_value'] == $emptyFieldId)
        {
           $error = new sfValidatorError($validator, $i18N->__('%fieldsetLabel% - Field "Default value" is required and it cannot have empty value', 
                    array('%fieldsetLabel%' => $this->getFieldsetLabel())));
            throw new sfValidatorErrorSchema($validator, array('type' => $error));
        }
        if($values['visible']) {
            if (!empty($values['default_value']) && !empty($availableArray)) {
                if (!in_array($values['default_value'], $availableArray)) {
                    $error = new sfValidatorError($validator, 'Error');
                    throw new sfValidatorErrorSchema($validator, array('type' => $error));
                }
            }
        }
        return $values;

    }//end canDefaultValue
}

//end class ProductFieldAdvancedForm
