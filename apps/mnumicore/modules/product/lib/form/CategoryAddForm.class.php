<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Add category form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CategoryAddForm extends CategoryForm
{

    public function configure()
    {
        parent::configure();

        $this->setWidget('parentId', new sfWidgetFormInputHidden(
                array('default' => CategoryTable::getInstance()->findOneByName(CategoryTable::$main)->getId())));
        $this->setValidator('parentId', new sfValidatorDoctrineChoice(array('model' => 'Category', 'column' => 'id', 'required' => true)));
        $this->getWidget('parentId')->setLabel('Choose parent category');      

        $this->useFields(array('name', 'parentId', 'photo'));
    }

    protected function doSave($con = null)
    {
        //gets parent category
        $parentCategory = CategoryTable::getInstance()->find($this->getValue('parentId'));

        //creates new child
        $catChild = new Category();
        $catChild->setName($this->getValue('name'));

        //insert
        $catChild->getNode()->insertAsLastChildOf($parentCategory);

        //parent::doSave();
    }

    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);
        $object->setPhoto(str_replace($this->defaultPath, '', $object->getPhoto()));
        return $object;
    }
}

