<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Select pricelist form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFixpriceSelectListForm extends sfForm
{
    public function configure()
    {
        if($this->getOption('productId'))
        {
            $this->setWidget('productId', new sfWidgetFormInputHidden(array('default' => $this->getOption('productId'))));            
        }
        else
        {
            $this->setWidget('productId', new sfWidgetFormInputHidden());
        }
        
        $this->setValidator('productId', new sfValidatorPass());

        $this->setWidget('selectFixpriceList', new sfWidgetFormDoctrineChoice(
                array('model' => 'Pricelist', 
                    'query' => PricelistTable::getInstance()->getSelectFixPriceListQuery(), 
                    'add_empty' => sfContext::getInstance()->getI18N()->__('Choose pricelist'))));
        $this->getWidget('selectFixpriceList')->setLabel('Choose fixed price list');
        $this->setValidator('selectFixpriceList', new sfValidatorDoctrineChoice(
                array('model' => 'Pricelist', 
                    'query' => PricelistTable::getInstance()->getSelectFixPriceListQuery(), 
                    'column' => 'id', 
                    'required' => true)));
        
        $this->useFields(array('selectFixpriceList'));
    }
  
}
