<?php

/**
 * settings actions.
 *
 * @package    mnumicore
 * @subpackage settings
 */
class productComponents extends sfComponents
{

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeNavigation()
    {
        // view objects
        $this->treeColl = CategoryTable::getInstance()
                ->getTreeQuery(null, null, null, true)
                ->execute(array(), Doctrine_Core::HYDRATE_RECORD_HIERARCHY);

        $this->languages = sfConfig::get('app_cultures_enabled');
        $this->default = MnumiI18n::getCulture();
    }

    public function executeCategoryForm()
    {
        // view objects           
        // gets hierarchical category tree
        $this->treeColl = CategoryTable::getInstance()
                ->getTreeQuery()
                ->execute(array(), Doctrine_Core::HYDRATE_RECORD_HIERARCHY);

        $this->categoryAddForm = new CategoryAddForm();
    }

}
