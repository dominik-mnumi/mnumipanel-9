<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * product actions.
 *
 * @package    mnumicore
 * @subpackage product
 * @author     Marek Balicki
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class productActions extends tablesActions
{

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        //prepares template objects
        $this->categorySelect($request);

        //view objects
        $this->catObj;

        $this->displayTableFields = array('Name' => array('partial' => 'product/name'));
        $this->displayGridFields = array(
            'Name' => array('destination' => 'name', 'partial' => 'product/name'),
            'CategoryName' => array('destination' => 'keywords')
        );

        // if want to view products for specific category
        if($this->catObj->getName() != CategoryTable::$main)
        {
            $this->customQuery = ProductTable::getInstance()->createQuery('c')->where('c.category_id = ?',
                    $this->catObj->getId());
        }

        $this->sortableFields = array('name');
        $this->modelObject = 'Product';
        $this->tableView = 'grid';

        $this->prepareAvailableActions();

        $this->tableOptions = $this->executeTable();

        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }

    /**
     * Saves and redirects to main page
     *
     * @param sfWebRequest $request
     */
    public function executeSave(sfWebRequest $request)
    {
        $this->saveEnvironment($request);

        $this->redirect('@product');
    }

    /**
     * Sets main category and redirects to main page
     *
     * @param sfWebRequest $request
     */
    public function executeClear(sfWebRequest $request)
    {
        $this->redirect('@productCategory?id='.CategoryTable::getInstance()
                        ->findOneByName(CategoryTable::$main)->getId());
    }

    /**
     * Executes add category action
     *
     * @param sfRequest $request A request object
     */
    public function executeCategoryAdd(sfWebRequest $request)
    {
        //add form
        $categoryAddForm = new CategoryAddForm();

        if($this->processCategoryForm($request, $categoryAddForm))
        {
            $this->getUser()->setFlash('info_data',
                    array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));

            // clear view cache
            $this->categoryClearCache();
        }
        else
        {
            $this->getUser()->setFlash('info_data',
                    array(
                'message' => 'Category could not be created. Please try again.',
                'messageType' => 'msg_error',
            ));
        }

        // clear cache
        //redirect to main page
        return $this->redirect('@product');
    }

    /**
     * Executes delete category action
     *
     * @param sfRequest $request A request object
     */
    public function executeCategoryEdit(sfWebRequest $request)
    {
        $categoryObj = $this->getRoute()->getObject();
        $this->forward404Unless($categoryObj);

        //gets sorted tree for form
        $rootObj = CategoryTable::getInstance()->getTreeSorted()->fetchRoot();

        //add category form
        $categoryAddForm = new CategoryAddForm();

        //edit category form
        $categoryEditForm = new CategoryEditForm($categoryObj);

        if($this->processCategoryForm($request, $categoryEditForm))
        {
            // clear view cache
            $this->categoryClearCache();

            $this->getUser()->setFlash('info_data',
                    array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));

            //redirect to main page
            return $this->redirect('@product');
        }

        //view objects
        $this->rootObj = $rootObj;
        $this->categoryAddForm = $categoryAddForm;
        $this->categoryEditForm = $categoryEditForm;
    }

    /**
     * Executes delete category action
     *
     * @param sfRequest $request A request object
     */
    public function executeCategoryDelete(sfWebRequest $request)
    {
        $categoryObj = $this->getRoute()->getObject();
        $this->forward404Unless($categoryObj);

        //delete form        
        $categoryDeleteForm = new CategoryDeleteForm();

        //prepare params because form is out generated
        $paramArray['_csrf_token'] = $categoryDeleteForm['_csrf_token']->getValue();
        $paramArray['id'] = $categoryObj->getId();

        //sets params
        $request->setParameter('category', $paramArray);

        if($this->processCategoryForm($request, $categoryDeleteForm))
        {
            // clear view cache
            $this->categoryClearCache();

            $this->getUser()->setFlash('info_data',
                    array(
                'message' => 'Deleted successfully.',
                'messageType' => 'msg_success',
            ));
        }
        else
        {
            $this->getUser()->setFlash('info_data',
                    array(
                'message' => 'Category could not be deleted. Please try again.',
                'messageType' => 'msg_error',
            ));
        }

        //redirect to main page
        return $this->redirect('@product');
    }

    /**
     * Executes add product action
     *
     * @param sfRequest $request A request object
     */
    public function executeProductAdd(sfWebRequest $request)
    {
        $this->executeProductEdit($request);
    }

    /**
     * Duplicate order
     * @param sfWebRequest $request
     */
    public function executeDuplicate(sfWebRequest $request)
    {
        $productObj = $this->getRoute()->getObject();
        $productObj = $productObj->copy(true);

        if($productObj->isValid())
        {
            $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
            $conn->beginTransaction();

            // copy image
            $image = $productObj->getPhoto();
            if($image != "")
            {
                $extension = substr($image, strrpos($image, '.'));
                $fileDir = ProductTable::getDefaultImagePath(true);
                $newfile = $filename = sha1($image.microtime().rand()).$extension;
                if(copy($fileDir.'/'.$image, $fileDir.'/'.$newfile))
                {
                    $productObj->setPhoto($newfile);
                }
                else
                {
                    $productObj->setPhoto(null);
                }

            }

            $productObj->save();
            $conn->commit();

            $this->redirect('productEdit', $productObj);
        }
    }

    /**
     * Executes edit product action
     *
     * @param sfRequest $request A request object
     */
    public function executeProductEdit(sfWebRequest $request)
    {
        // get pricelist list
        $this->pricelists = PricelistTable::getInstance()->asArray();
        $this->defaultPricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();

        if($request->hasParameter('slug'))
        {
            //gets product
            $productObj = $this->getRoute()->getObject();
            $this->forward404Unless($productObj);

            //sets navigation id
            $this->getUser()->setTreeNavigationId($productObj->getCategoryId());
        }
        elseif($request->hasParameter('previous'))
        {
            //gets product to duplicate
            $productObj = ProductTable::getInstance()->find($request->getParameter('previous'));
            $this->forward404Unless($productObj);

            $productObj = $productObj->copy(true);

            // creates object copy with proper 'copy' prefix
            $i = 1;
            $copySlug = 'copy'.$i.$productObj->getSlug();
            while(ProductTable::getInstance()->findOneBySlug($copySlug))
            {
                $i++;
                $copySlug = 'copy'.$i.$productObj->getSlug();
            }

            $productObj->setSlug($copySlug);
            $productObj->save();
            $this->redirect($this->generateUrl('productEdit', $productObj));
        }

        //fixprice select list form
        $fixpriceSelectForm = new ProductFixpriceSelectListForm(
                null, 
                isset($productObj) ? array('productId' => $productObj->getId()) : array());

        //gets all default fieldsets
        $productFormArray = FieldsetTable::getDefaultFieldsetForms(
                isset($productObj) ? $productObj : null,
                isset($productObj) ? $this->getUser()->getTreeNavigationId() : null);

        if($request->isMethod('post'))
        {
            $fieldsetKeys = array();

            $emptyField = FieldItemTable::getInstance()->getEmptyField();

            //first bind and valid
            $valid = true;
            foreach($productFormArray as $key => $rec)
            {
                $paramArray = $request->getParameter($rec->getName());
                $filesArray = $request->getFiles($rec->getName());

                $rec->bind($paramArray, $filesArray);

                if(!$rec->isValid())
                {
                    $valid = false;
                }

                if(!$rec instanceof ProductFieldOtherForm)
                {
                    continue;
                }

                $fieldItems = array();

                // special validation: other values
                // $fieldItem: get list of selected fieldItems 
                if(!array_key_exists('visible', $paramArray))
                {
                    $fieldItems[] = $paramArray['default_value'];
                }
                else
                {
                    if(!array_key_exists('product_field_item', $paramArray))
                    {
                        continue;
                    }

                    $fieldItems = $paramArray['product_field_item'];
                }

                foreach($fieldItems as $itemId)
                {
                    if($itemId == $emptyField->getId())
                    {
                        continue;
                    }

                    if(!in_array($itemId, $fieldsetKeys))
                    {
                        // add key to list
                        $fieldsetKeys[] = $itemId;
                        continue;
                    }

                    $valid = false;

                    $duplicatedField = FieldItemTable::getInstance()->find($itemId);

                    $error = new sfValidatorString(
                                    array(), array('invalid' => 'There was duplicated option in available values (duplicate "%name%" field)')
                    );

                    $rec->getErrorSchema()->addError(
                            new sfValidatorError($error, 'invalid', array('name' => $duplicatedField->getName())),
                            'global'
                    );
                }
            }

            //if all forms are valid than save
            if($valid == true)
            {
                //cut first element and make save, because we need product object
                //to other forms
                $productFormTemp = array_shift($productFormArray);
                $productObj = $productFormTemp->save();
                foreach($productFormArray as $key => $rec)
                {
                    $rec->setProduct($productObj);
                    $rec->save();
                }
                $this->getUser()->setFlash('info_data',
                        array(
                    'message' => 'Saved successfully.',
                    'messageType' => 'msg_success',
                ));

                if($request->getParameter('saveAndAdd'))
                {
                    $this->redirect('@productAdd?previous=' . $productObj->getId());
                }

                // clear product cache
                $calculationProductData = new ProductCalculationProductData($productObj);
                $calculationProductData->clearCache();
                // clear category cache
                $this->categoryClearCache();

                $this->redirect('productEdit', $productObj);
            }
        }

        // view objects
        $this->productFormArray = $productFormArray;
        $this->fixpriceSelectForm = $fixpriceSelectForm;
        $this->emptyValueId = FieldItemTable::getInstance()->getEmptyField()->getId();

        // gets root category
        $this->categoryRootObj = CategoryTable::getInstance()->getTreeSorted()->fetchRoot();
    }

    /**
     * Executes delete product action
     *
     * @param sfRequest $request A request object
     */
    public function executeProductDelete(sfWebRequest $request)
    {
        //gets product
        $productObj = $this->getRoute()->getObject();
        if($productObj->canDelete())
        {
            $productObj->delete();

            $this->getUser()->setFlash('info_data',
                    array(
                'message' => 'Deleted successfully.',
                'messageType' => 'msg_success',
            ));
        }
        else
        {
            $this->getUser()->setFlash('info_data',
                    array(
                'message' => 'Deleted unsuccessfully. This product could not be deleted.',
                'messageType' => 'msg_error',
            ));
        }

        //redirects to main page
        $this->redirect('product');
    }

    //AJAX SECTION
    public function executeReqAddFixedPriceColl(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());
        $number = intval($request->getParameter("nb"));
        $pricelistId = intval($request->getParameter("pricelistId"));
        $productId = intval($request->getParameter("productId"));

        //gets product
        if(!empty($productId))
        {
            $productObj = ProductTable::getInstance()->find($productId);

            $productFixpriceCollForm = new ProductFixpriceCollForm(array(),
                            array('pricelistObj' => PricelistTable::getInstance()->find($pricelistId),
                                'productFixpriceColl' => $productObj->getProductFixpriceByPriceListId($pricelistId)));
        }
        else
        {
            $productFixpriceCollForm = new ProductFixpriceCollForm(array(),
                            array('pricelistObj' => PricelistTable::getInstance()->find($pricelistId)));
        }

        //embed this collection in form
        $productForm = new ProductFixpriceCustomForm();
        $productForm->addPricelistColl($productFixpriceCollForm, $number);
        $productForm->getWidgetSchema()->setNameFormat($productForm->getName().'_8[%s]');

        return $this->renderPartial('fixedPriceNewColl',
                        array('productForm' => $productForm, 'key' => $number));
    }

    /**
     * Calculates pice in fixed price form.
     *
     * @param sfWebRequest $request
     */
    public function executeReqFixedPriceCalculate(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());

        return sfView::NONE;
    }

    /**
     * Returns prepared encoded array response.
     *
     * @param sfWebRequest $request
     */
    public function executeReqGetEncodedWizardParameter(sfWebRequest $request)
    {
        // gets parameters 
        $parameter = $request->getParameter('parameter');

        // creates signature object (mandatory to create encoded array to send back)
        $signatureObj = new SignatureManager(WizardManager::getSecretKey());

        // encodes parameter
        $encodedParameter = $signatureObj->encodeParameter($parameter);

        // generates signature
        $signature = $signatureObj->generateSignature($parameter);

        // prepares response
        $preparedResponse['parameter'] = $encodedParameter;
        $preparedResponse['signature'] = $signature;
        
        // id (optional)
        $wizardId = WizardManager::getId(); 
        if($wizardId)
        {
            $preparedResponse['id'] = $wizardId;
        }

        return $this->renderText(json_encode($preparedResponse));
    }

    /**
     * Returns prepared encoded array response.
     *
     * @param sfWebRequest $request
     */
    public function executeAddWizard(sfWebRequest $request)
    {
        $wizardName = $request->getParameter('wizard_name');

        $this->setLayout(false);

        // view objects
        $this->wizardName = $wizardName;
        $this->wizardPreviewUrl = WizardManager::getInstance()->generatePreviewUrl($wizardName,
                100, 100);
    }

    /**
     * Executes editWizard action.
     *
     * @param sfWebRequest $request
     */
    public function executeEditWizard(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    /**
     * Executes copyWizard action.
     *
     * @param sfWebRequest $request
     */
    public function executeReqWizardCopy(sfWebRequest $request)
    {
        $projectName = $request->getParameter('projectName');

        // copies wizard
        $newProjectName = WizardManager::getInstance()->copyWizard($projectName);

        // gets product object
        $productObj = ProductTable::getInstance()->find($request->getParameter('productId'));

        // adds new wizard to product
        $productObj->addWizard($newProjectName);

        $this->getUser()->setFlash('info_data',
            array(
                'message' => 'Project "%wizardName%" has been successfully copied to product "%productName%".',
                'messageType' => 'msg_success',
                'messageParam' => array('%wizardName%' => $projectName, '%productName%' => $productObj->getName())
            )
        );

        return $this->renderText(
            json_encode(
                array('result' => array('status' => 'success', 'orderId' => $newProjectName))
            )
        );
    }

    /**
     * Process form save. Returns true or false.
     *
     * @param sfWebRequest $request
     * @param sfForm $form
     * @return boolean
     */
    protected function processCategoryForm(sfWebRequest $request, sfForm $form)
    {
        if($request->hasParameter($form->getName()))
        {
            $paramArray = $request->getParameter($form->getName());

            if($form->isMultipart())
            {
                $fileArray = $request->getFiles($form->getName());
                $form->bind($paramArray, $fileArray);
            }
            else
            {
                $form->bind($paramArray);
            }

            if($form->isValid())
            {
                $form->save();

                $form->getObject();
                return true;
            }
            return false;
        }
    }

    /**
     * Prepares objects for category template such as pagerLayout etc.
     *
     * @param sfWebRequest $request
     */
    protected function categorySelect(sfWebRequest $request)
    {
        //gets current category
        $catId = $this->getUser()->getTreeNavigationId();
        $this->catObj = null;

        if($catId !== null)
        {
            $this->catObj = CategoryTable::getInstance()->find($catId);
        }
        if(!$this->catObj)
        {
            $this->catObj = CategoryTable::getInstance()->getTree()->fetchRoot();
        }

        if(!$this->catObj)
        {
            throw new Exception('Could not get category object.');
        }
    }

    /**
     *
     * @param Doctrine_Query $query
     * @return boolean
     */
    protected function addFilterQuery($query)
    {
        //if null then do nothing
        $filterArray = $this->getUser()->getFilters();

        if(null == $filterArray)
        {
            return true;
        }
    }

    /**
     *
     * @param Doctrine_Query $query
     * @return boolean
     */
    protected function addSortQuery($query)
    {
        $permittedColumnArray = array('name');

        //if array null then do nothing
        $sortArray = $this->getUser()->getSort();

        if(array(null, null) !== $sortArray && in_array($sortArray[0],
                        $permittedColumnArray))
        {
            $query->orderBy('p.'.$sortArray[0].' '.$sortArray[1]);
        }

        return true;
    }

    /**
     * Method save state of environment
     *
     * @param sfWebRequest $request
     */
    private function saveEnvironment(sfWebRequest $request)
    {
        //sets category id
        if($request->hasParameter('id'))
        {
            //gets paramater from GET        
            $catId = $request->getParameter('id');
            $this->getUser()->setTreeNavigationId($catId);
        }

        //sets sort
        if($request->hasParameter('sort') && $request->hasParameter('order'))
        {
            //gets paramater from GET
            $sortArray[] = $request->getParameter('sort');
            $sortArray[] = $request->getParameter('order');
            $this->getUser()->setSort($sortArray);
        }

        //sets page
        if($request->hasParameter('page'))
        {
            $page = $request->getParameter('page');
            $this->getUser()->setPage($page);
        }

        //sets records per page
        if($request->hasParameter('recPerPage'))
        {
            $recPerPage = $request->getParameter('recPerPage');
            $this->getUser()->setRecPerPage($recPerPage);
        }
    }

    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Product';
        parent::executeDeleteMany($request);
    }

    protected function categoryClearCache()
    {
        if($cache = $this->getContext()->getViewCacheManager())
        {
            $cache->remove('@sf_cache_partial?module=product&action=_navigation&sf_cache_key=*');
        }
    }

    /**
     * Prepares available for current user actions, such as: Edit, Delete...
     */
    protected function prepareAvailableActions()
    {
        $user = $this->getUser();

        // custom edit action because we want to change default route for it
        if ($user->hasCredential(array('product_create', 'product_viewList'), false)) {
            $this->actions['edit'] = array(
                'label' => 'Edit',
                'route' => 'productEdit',
                'icon' => '/images/icons/fugue/pencil.png',
                'attributes' => array('title' => 'Edit')
            );
        }
        else {
            unset($this->actions['edit']);
        }

        if ($user->hasCredential(array('product_delete'), false)) {
            $this->actions['delete'] = array(
                'icon' => '/images/icons/fugue/cross-circle.png',
                'partial' => 'product/productDeleteDisabled'
            );
        }
        else {
            unset($this->actions['delete']);
        }

        if ($user->hasCredential(array('product_duplicate'), false)) {
            $this->actions['duplicate'] = array(
                'label' => 'Duplicate',
                'route' => 'productDuplicate',
                'icon' => '/images/icons/fugue/cards-address.png',
                'attributes' => array(
                    'title' => 'Duplicate',
                    'onclick' => isset($actionParams['attributes']['onclick']) ? $actionParams['attributes']['onclick'] : 'alertModalConfirm(\''.$this->getContext()->getI18N()->__('Duplicate item').'\',\'<h3>'.$this->getContext()->getI18N()->__('Do you really want to duplicate this item?').
                        '</h3>\', 500, this.href); return false;'
                ),
            );
        }
        else {
            unset($this->actions['duplicate']);
        }
    }
}
