<?php

/**
 * dashboard components.
 *
 * @package    mnumicore
 * @subpackage dashboard
 */
class dashboardComponents extends sfComponents
{
    /**
     * Executes navi action
     *
     * @param sfWebRequest $request A request object
     */
    public function executeMainNavi(sfWebRequest $request)
    {
        $contextObj = $this->getContext();
        
        // view objects
        $this->navigatorObj = new Navigator(
                $this->getUser(), 
                $request->getParameter('module'),
                $request->getParameter('action'),
                $this->getContext()->getRouting()->getRoutes(),
                new sfActionCredentialsGetter($contextObj),
                $this->getController());

        $this->module = $this->getModuleName();
        $this->action = $request->getParameter('action');
    }

    /**
     * @param sfWebRequest $request
     */
    public function executeNewsTimeline(sfWebRequest $request)
    {
        $this->json = array();

        $cacheFile = mnumicoreConfiguration::getTwitterFile();

        if($json = @file_get_contents($cacheFile)) {
            $this->json = $json;
        }
    }
    
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeStatusBar(sfWebRequest $request)
    {
        $module_name = $this->getRequest()->getParameter('module');
        $action_name = $this->getRequest()->getParameter('action');

        $path = null;
        $customObject = null;
        if($module_name == 'product')
        {
            $catId = sfContext::getInstance()->getUser()->getTreeNavigationId();

            $catObj = CategoryTable::getInstance()->find($catId);
            
            if($catObj)
            {
                $path = $catObj->getNode()->getAncestors();
                $path[] = $catObj;
                unset($path[0]);
            }
        }
        
        if($module_name == 'client' && in_array($action_name, array('edit', 'attachment', 'delivery', 'user')))
        {
            $customObject = array('object' => ClientTable::getInstance()->find($this->getRequest()->getParameter('id')), 'title' => 'Fullname');
        }

        //view objects
        $this->module_name = $module_name;
        $this->action_name = $action_name;
        $this->path = $path;
        $this->customObject = $customObject;
    }
}
