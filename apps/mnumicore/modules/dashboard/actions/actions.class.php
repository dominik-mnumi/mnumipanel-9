<?php

/**
 * dashboard actions.
 *
 * @package    mnumicore
 * @subpackage dashboard
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class dashboardActions extends tablesActions
{

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->waitingOrders = OrderTable::getInstance()->getDashboardWaitingOrders();
        $waitingOrdersIds = array();
        foreach($this->waitingOrders as $order)
        {
            $waitingOrdersIds[] = $order->getId();
        }

        $this->newOrders = OrderTable::getInstance()->getDashboardNewOrders($waitingOrdersIds);
                            
        $this->printedOrders = OrderTable::getInstance()->getDashboardInProgressOrders();

        if($request->isXmlHttpRequest())
        {
            return $this->renderText($this->table);
        }
    }

    /**
     * Empty action for extending session
     *
     * @param sfWebRequest $request
     * @return int
     */
    public function  executePing(sfWebRequest $request)
    {
        return sfView::NONE;
    }

    /**
     * Shows 404 error.
     *
     * @param sfWebRequest $request
     */
    public function executeError404(sfWebRequest $request)
    {
        $this->setLayout('error');
    }
    
    /**
     * Old browser.
     *
     * @param sfWebRequest $request
     */
    public function executeOldBrowser(sfWebRequest $request)
    {

    }
}
