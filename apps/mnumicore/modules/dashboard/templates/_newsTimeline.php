<a href="javascript:void(0)" class="button" title=""><img src="/images/twitter.png" width="16" height="16"> <strong></strong></a>
<div id="comments-list" class="result-block">
    <span class="arrow"><span></span></span>

    <ul class="small-files-list twitter-news">
        <?php foreach(json_decode($sf_data->getRaw('json')) as $item): ?>
            <li>
                <small>
                    <a href="http://twitter.com/<?php echo $item->user; ?>" target="_blank"><?php echo $item->user; ?></a>,
                    <?php echo $item->friendly_date; ?> <?php echo __('ego'); ?>
                </small>
                </br>
                <?php echo $item->text; ?>
            </li>
        <?php endforeach; ?>
    </ul>
    <p id="twitter-info" class="result-info">
        <a href="http://twitter.com/MnumiWydania" target="__blank">
            <?php echo __('The latest from Mnumi (%%MnumiReleases%%) on Twitter', array(
                '%%MnumiReleases%%' => '@MnumiWydania',
            )); ?>
        </a>
    </p>
</div>