<div id="status-bar">
    <div class="container_12">
        <ul id="status-infos">
            <li class="spaced">
                <?php echo __('Logged in as'); ?>:
                <strong><?php echo link_to($sf_user->getGuardUser()->getFriendlyUsername($sf_user->getGuardUser()->__toString()), '@userListEdit?id='.$sf_user->getGuardUser()->getId()); ?></strong>
            </li>
            <li>
                <?php include_component('dashboard', 'newsTimeline'); ?>
            </li>
            <li><?php echo link_to(__('Logout'), '@sf_guard_signout', 'class="button red"'); ?></li>
        </ul>
        <ul id="breadcrumb">
            <li><?php echo link_to(__('Home'), '@homepage'); ?></li>
            <li>
                <?php
                // second level:
                $title = __(sfConfig::get('mod_' . strtolower($module_name) . '_breadcrumb_title', ucfirst($module_name)));
                $href = sfConfig::get('mod_' . strtolower($module_name) . '_breadcrumb_link', '@homepage');
                
                // if 'this' is declared in module.yml then href = current url
                if($href == 'this') $href = $sf_request->getUri();
                echo link_to($title, $href); ?>
            </li>
            <?php if(sfConfig::has('mod_' . strtolower($module_name) . '_breadcrumb_' . $action_name . '_title')): ?>
            <li>
            <?php
                // third level:
                $title = __(sfConfig::get('mod_' . strtolower($module_name) . '_breadcrumb_' . $action_name . '_title', ucfirst($module_name)));
                $href = sfConfig::get('mod_' . strtolower($module_name) . '_breadcrumb_' . $action_name . '_link', '@homepage');
                
                // if 'this' is declared in module.yml then href = current url
                if($href == 'this') $href = $sf_request->getUri();
                echo link_to($title, $href); ?>
            </li>
            <?php endif; ?>
                
            <!-- product category path -->
            <?php if(count($path) > 0): ?>
            <?php foreach($path as $rec): ?>
                <li>
                <?php
                    $title = $rec->getName();
                    $href = '@productCategory?id='.$rec->getId();
                    echo link_to($title, $href); ?>
                </li>
            <?php endforeach; ?>
            <?php endif; ?>
            
            <!-- Custom object (ex client name) -->
            <?php if($customObject): ?>
                <li>
                <?php  
                    $get = 'get'.$customObject['title'];
                    $obj = $customObject['object'];
                    $title = $obj->$get();
                    $href = $obj->getEditUrl();
                    echo link_to($title, $href); ?>
                </li>
            <?php endif; ?>
            
        </ul>
    </div>
</div>
