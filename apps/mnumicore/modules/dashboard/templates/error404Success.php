<!--[if lt IE 9]><div class="ie"><![endif]-->
<!--[if lt IE 8]><div class="ie7"><![endif]-->

<h1>404</h1>
<p>Page not found</p>
<section>

    <ul class="action-tabs on-form with-children-tip children-tip-left">
        <li><a href="javascript:history.back()" title="Go back"><img src="../images/icons/fugue/navigation-180.png" width="16" height="16"></a></li>
    </ul>

    <ul class="action-tabs right on-form with-children-tip children-tip-right">
        <li><a href="<?php echo url_for('homepage'); ?>" title="Go to homepage"><img src="../images/icons/fugue/home.png" width="16" height="16"></a></li>
    </ul>

    <form class="block-content no-title dark-bg form" method="post" action="">
        <input type="text" name="s" id="s" value=""> &nbsp;
        <button type="submit">Search</button>
    </form>

</section>

<!--[if lt IE 8]></div><![endif]-->
<!--[if lt IE 9]></div><![endif]-->