<ul class="container_12">
    <?php foreach($navigatorObj->getMenu() as $rec): ?>
    <?php if($rec->isVisible()): ?>
    <li class="<?php echo $rec->getClass(); ?><?php if($rec->isCurrent()) { echo ' current'; } ?>">
        <a href="<?php echo $rec->getRoute() == '#' ? $rec->getRoute() : url_for($rec->getRoute()); ?>" title="<?php echo __($rec->getTitle()); ?>">
            <?php echo __($rec->getTitle()); ?>
        </a>
        <?php if($rec->getRoute() == '#'): ?>
        <ul>
            <?php foreach($rec->getSubmenu() as $rec2): ?>
            <?php if($rec2->isVisible()): ?>
            <li class="<?php if($rec2->isCurrent()) { echo 'current'; } ?><?php if($rec2->hasSubmenuWithCredential()) { echo ' with-menu'; } ?>">
                <a href="<?php echo url_for($rec2->getRoute()); ?>">
                    <?php echo __($rec2->getTitle()); ?>
                </a>
                <?php if($rec2->hasSubmenuWithCredential()): ?>
                <div class="menu">
                    <?php echo image_tag('menu-open-arrow.png', 
                            array('width' => 16,
                                'height' => 16)); ?>  
                    <?php include_partial('dashboard/submenuNavi', 
                            array('rec' => $rec2)); ?>        
                </div>
                <?php endif; ?>
            </li>    
            <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
    </li>
    <?php endif; ?>
    <?php endforeach; ?> 
</ul>
