<form action="<?php echo url_for('invoiceCostRecordEdit', $invoiceForm->getObject()); ?>" method="POST">

    <?php echo include_partial('dashboard/formActions', 
            array('route' => '@invoiceCostEdit?id='.$invoiceForm->getObject()->getClient()->getId())); ?>
    <?php echo include_partial('dashboard/message'); ?>
    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content form">
                    <h1><?php echo __('Invoice cost edit'); ?></h1>

                <?php echo $invoiceForm['_csrf_token']->render(); ?>

                <p class="required">
                    <?php echo $invoiceForm['invoice_number']->renderLabel(); ?>
                    <?php echo $invoiceForm['invoice_number']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $invoiceForm['invoice_date']->renderLabel(); ?>
                    <?php echo $invoiceForm['invoice_date']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $invoiceForm['payment_date']->renderLabel(); ?>
                    <?php echo $invoiceForm['payment_date']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $invoiceForm['notice']->renderLabel(); ?>
                    <?php echo $invoiceForm['notice']->render(); ?>
                <p>
                <p class="required">
                    <?php if(isset($invoiceForm['client'])): ?>
                        <?php echo $invoiceForm['client']->renderLabel(); ?>
                        <?php echo $invoiceForm['client']->render(); ?>
                    <?php endif ?>
                    <?php echo $invoiceForm['client_id']->render(); ?>
                </p>                               
                <p  class="required">
                    <?php echo $invoiceForm['amount']->renderLabel(); ?>
                    <?php echo $invoiceForm['amount']->render(); ?>
                <p>

                </div>
            </div>
        </section>
    </article>
</form>