<?php echo __('Invoice cost number'); ?>;<?php echo __('Client name'); ?>;<?php echo __('User number'); ?>;<?php echo __('User firstname and lastname'); ?>;<?php echo __('Date of issue'); ?>;<?php echo __('Payment date'); ?>;<?php echo __('Paid at'); ?>;<?php echo __('Amount'); ?>;<?php echo __('Paid amount'); ?>;<?php echo __('Balance')."\n"; ?>
<?php foreach($invoiceCostColl as $invoiceCost): ?>
<?php echo $invoiceCost->getInvoiceNumber(); ?>;<?php echo $invoiceCost->getClient()->getFullname(); ?>;<?php echo $invoiceCost->getUser()->getId(); ?>;<?php echo $invoiceCost->getUser()->getFirstName().' '.$invoiceCost->getUser()->getLastName(); ?>;<?php echo $invoiceCost->getInvoiceDate(); ?>;<?php echo $invoiceCost->getPaymentDate(); ?>;<?php echo $invoiceCost->getPaymentAt(); ?>;<?php echo $invoiceCost->getAmount(); ?>;<?php echo $invoiceCost->getPayedAmount(); ?>;<?php echo $invoiceCost->getBalance()."\n"; ?>
<?php endforeach; ?>

