<div name="payInvoices">
    <table cellspacing="0" width="100%" name="payInvoicesTable" class="simple-table">
            <tr>
                <th>
                    
                </th>
                <th>
                    <?php echo __('Invoice number'); ?>
                </th>
                <th>
                    <?php echo __('Amount'); ?>
                </th>
                <th>
                    <?php echo __('Payment date'); ?>
                </th>
            </tr>
            <?php $anyInvoice = 0; ?>
            <?php foreach ($row->getInvoiceCosts() as $invoice): ?>
                    <?php if ($invoice->getBalance() > 0): ?>
                    <tr>
                        <?php $anyInvoice++; ?>
                        <td>
                            <input type="checkbox" name="invoices[]" value="<?php echo $invoice->getId(); ?>" /> 
                        </td>
                        <td>
                            <?php echo $invoice->getInvoiceNumber(); ?>
                        </td>
                        <td>
                            <?php echo include_partial('amount', array('row' => $invoice)); ?>
                        </td>
                        <td>
                            <?php echo $invoice->getPaymentDate(); ?>
                        </td>
                    </tr>
                    <?php endif; ?>
            <?php endforeach; ?> 
    </table>
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$invoiceCostPay)
            && $anyInvoice): ?>
        <button name="payBtn" type="submit"><?php echo __('Pay selected'); ?></button>
    <?php endif; ?>
</form>

