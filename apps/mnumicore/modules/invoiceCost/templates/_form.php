<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('invoiceCost/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
    <table>
        <tfoot>
            <tr>
                <td colspan="2">
                    <?php echo $form->renderHiddenFields(false) ?>
                    &nbsp;<a href="<?php echo url_for('invoiceCost/index') ?>">Back to list</a>
                    <?php if (!$form->getObject()->isNew()): ?>
                    &nbsp;<?php echo link_to('Delete', 'invoiceCost/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
                    <?php endif; ?>
                    <input type="submit" value="Save" />
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php echo $form->renderGlobalErrors() ?>
            <tr>
                <th><?php echo $form['invoice_number']->renderLabel() ?></th>
                <td>
                <?php echo $form['invoice_number']->renderError() ?>
                <?php echo $form['invoice_number'] ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['invoice_date']->renderLabel() ?></th>
                <td>
                <?php echo $form['invoice_date']->renderError() ?>
                <?php echo $form['invoice_date'] ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['payment_date']->renderLabel() ?></th>
                <td>
                <?php echo $form['payment_date']->renderError() ?>
                <?php echo $form['payment_date'] ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['payment_at']->renderLabel() ?></th>
                <td>
                <?php echo $form['payment_at']->renderError() ?>
                <?php echo $form['payment_at'] ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['user_id']->renderLabel() ?></th>
                <td>
                <?php echo $form['user_id']->renderError() ?>
                <?php echo $form['user_id'] ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['notice']->renderLabel() ?></th>
                <td>
                <?php echo $form['notice']->renderError() ?>
                <?php echo $form['notice'] ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['client_id']->renderLabel() ?></th>
                <td>
                <?php echo $form['client_id']->renderError() ?>
                <?php echo $form['client_id'] ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['created_at']->renderLabel() ?></th>
                <td>
                <?php echo $form['created_at']->renderError() ?>
                <?php echo $form['created_at'] ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['updated_at']->renderLabel() ?></th>
                <td>
                <?php echo $form['updated_at']->renderError() ?>
                <?php echo $form['updated_at'] ?>
                </td>
            </tr>
        </tbody>
    </table>
</form>
