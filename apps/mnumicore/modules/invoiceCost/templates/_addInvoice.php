<form action="<?php echo url_for('invoiceCostAdd', 
        array('model' => 'InvoiceCost', 
            'type' => 'new')); ?>" method="POST" id="addClientForm" style="display: none;">
    <p class="validateTips"></p>
    <?php echo $addInvoiceForm['_csrf_token']->render(); ?>
    <div class="block-border">
        <div class="block-content form">
            <p class="required">
                <?php echo $addInvoiceForm['invoice_number']->renderLabel(); ?>
                <?php echo $addInvoiceForm['invoice_number']->render(); ?>
            <p>
            <p class="required">
                <?php echo $addInvoiceForm['invoice_date']->renderLabel(); ?>
                <span class="input-type-text">
                    <?php echo $addInvoiceForm['invoice_date']->render(); ?>
                </span>
            <p>
            <p class="required">
                <?php echo $addInvoiceForm['payment_date']->renderLabel(); ?>
                <span class="input-type-text">
                    <?php echo $addInvoiceForm['payment_date']->render(); ?>
                </span>
            <p>
            <p class="required">
                <?php echo $addInvoiceForm['notice']->renderLabel(); ?>
                <?php echo $addInvoiceForm['notice']->render(); ?>
            <p>
            <p class="required">
                <?php if(isset($addInvoiceForm['client'])): ?>
                    <?php echo $addInvoiceForm['client']->renderLabel(); ?>
                    <?php echo $addInvoiceForm['client']->render(); ?>
                <?php endif ?>
                <?php echo $addInvoiceForm['client_id']->render(); ?>
            </p>                               
            <p  class="required">
                <?php echo $addInvoiceForm['amount']->renderLabel(); ?>
                <?php echo $addInvoiceForm['amount']->render(); ?>
            <p>
        </div>
    </div>
</form>
