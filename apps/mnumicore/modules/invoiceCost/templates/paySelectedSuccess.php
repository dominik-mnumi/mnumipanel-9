<article class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <form action="<?php echo url_for('invoiceCostPaySelectedProcess'); ?>" method="POST" id="payForm" class="block-content form" name="filters" class="no-margin">
                    <h1><?php echo __('Pay invoices'); ?></h1>
                    <div class="columns">     
                        <div class="colx3-left">
                            <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th>
                                            <?php echo __('For what'); ?>
                                        </th>
                                        <th>
                                            <?php echo __('How much'); ?>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>   
                                <?php foreach ($invoiceColl as $invoice): ?>                               
                                <tr>
                                    <td>
                                        <input type="hidden" name="invoicesId[]" value="<?php echo $invoice->getId(); ?>" />
                                        <?php echo $invoice->getInvoiceNumber(); ?>
                                    </td>
                                    <td>
                                        <?php echo $invoice->getAmount(); ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td class="vertical-align-middle">
                                        <b><?php echo __('Total cost'); ?></b>
                                    </td>
                                    <td class="vertical-align-middle">
                                        <input type="text" id="payValue" name="payValue" value="<?php echo $totalCost; ?>" />&nbsp; zł
                                        <button type="button" id="payBtn"><?php echo __('Pay'); ?></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="colx3-right-double">
                            <fieldset class="grey-bg">
                                <legend><?php echo __('Details'); ?></legend>
                                <div id="invoice-cost-details-container">
                                    <?php include_partial('invoiceCost/transferDetails', 
                                            array('clientObj' => $clientObj,
                                                'invoiceCount' => $invoiceColl->count(),
                                                'title' => $title)); ?>
                                </div>
                            </fieldset>
                        </div>
                    </div>   
                    
                    <?php if($sf_user->hasCredential('client_generalInformationEdit')): ?>
                    <div class="text-right">
                        <button type="button" id="dialog-edit-client-general-click">
                            <?php echo __('Change customer data'); ?>
                        </button>
                    </div>
                    <?php endif; ?>
                </form>
            </div>
        </div>
    </section>
</article>

<?php if($sf_user->hasCredential('client_generalInformationEdit')): ?>
<?php echo include_partial('client/edit/form/generalForm', array('form' => $clientEditForm)); ?>
<?php endif; ?>

<script type="text/javascript">
function refreshClientForm()
{
    $("#EditClientGeneralForm_fullname").val($("#invoice-cost-details-fullname").html().trim());
    $("#EditClientGeneralForm_street").val($("#invoice-cost-details-street").html().trim());
    $("#EditClientGeneralForm_postcodeAndCity").val($("#invoice-cost-details-postcode").html().trim());
    $("#EditClientGeneralForm_account").val($("#invoice-cost-details-account").html().trim());
}
    
jQuery(document).ready(function($) 
{  
    $('#payBtn').click(function()
    {
        $('#payForm').submit();
    });

    <?php if($sf_user->hasCredential('client_generalInformationEdit')): ?>
    //client edit general information form
    $("#dialog-edit-client-general-click").click(function() 
    {
        $.modal({
            focus: function(event, ui) {alert('sdfd'); },
            content: $("#dialog-edit-client-general-form"),          
            title: '<?php echo __('General information'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-edit-client-general-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
            },
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-edit-client-general-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-edit-client-general-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();

                                    $.get('<?php echo url_for('reqClientTransferDetails', 
                                            array('clientId' => $clientObj->getId())); ?>',
                                    function(html)
                                    {
                                        // gets title before new data
                                        var title = $("#invoice-cost-details-title").html();
                                        
                                        $("#invoice-cost-details-container").html(html);
                                        
                                        // set title
                                        $("#invoice-cost-details-title").html(title);
                                        
                                        refreshClientForm();
                                    });                  
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
        
    });
    <?php endif; ?>
});
</script>
