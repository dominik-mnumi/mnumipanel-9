<table class="table invoice-cost-transfer-details" width="100%">
    <tr>
        <th class="width25proc">
            <?php echo __('Title'); ?>
        </th>
        <td id="invoice-cost-details-title">
            <?php echo __('Paid for'); ?>
            <?php echo format_number_choice(
                '[0]No invoices|[1]One invoice|(1,+Inf]%count% invoices',
                array('%count%' => $invoiceCount),
                $invoiceCount
            ); ?>:
            <?php echo $title; ?>
        </td>
    </tr>
    <tr>
        <th>
            <?php echo __('Name'); ?>
        </th>
        <td id="invoice-cost-details-fullname">
            <?php echo $clientObj->getFullname(); ?>
        </td>
    </tr>
    <tr>
        <th>
            <?php echo __('Street'); ?>
        </th>
        <td id="invoice-cost-details-street">
            <?php echo $clientObj->getStreet(); ?>
        </td>
    </tr>
    <tr>
        <th class="special-table-th">
            <?php echo __('Postcode/City'); ?>
        </th>
        <td id="invoice-cost-details-postcode">
            <?php echo $clientObj->getPostcodeAndCity(); ?>
        </td>
    </tr>
    <tr>
        <th class="special-table-th">
            <?php echo __('Account'); ?>
        </th>
        <td id="invoice-cost-details-account">
            <?php echo $clientObj->getAccount(); ?>
        </td>
    </tr>
    
</table>