<?php

/**
 * InvoiceCostClient form (removes client field).
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceCostClientForm extends InvoiceCostForm
{

    public function configure()
    {
        parent::configure();
        
        $this->useFields(array('invoice_number', 'invoice_date', 'payment_date',
            'notice', 'user_id', 'client_id', 'amount'));
    }

}
