<?php

/**
 * CouponMany form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CouponManyForm extends CouponForm
{
    public function configure()
    {
         parent::configure();
         unset($this['name'], $this['one_time_usage']);
         
         // how many coupons should be generated
         $this->setWidget('count', new sfWidgetFormInputText(array(),array('class' => 'full-width')));
         $this->setValidator('count', new sfValidatorInteger(array('required' => true)));
         $this->getWidget('count')->setLabel('Number of generated coupons');
         
         // reset post validator from parent class
         $this->validatorSchema->setPostValidator(new sfValidatorPass());
    }
    
    /**
     * Saves many coupons
     */
    public function save($conn = null)
    {
        $values = $this->getValues();
        
        $couponArrayPricelistForm = ($this->getValue('custom_pricelists')) ? $this->getValue('custom_pricelists') : array();
        $couponArrayProductForm = ($this->getValue('custom_products')) ? $this->getValue('custom_products') : array();

        for($i = 0; $i < $values['count']; $i++)
        {
            $coupon = new Coupon();
            $coupon->setName(CouponTable::getInstance()->generateUniqueName());
            $coupon->setOneTimeUsage(true);
            $coupon->fromArray($values);
            $coupon->setApplyForAllPricelist(in_array('__ALL__', $couponArrayPricelistForm));
            $coupon->setApplyForAllProducts(in_array('__ALL__', $couponArrayProductForm));
            $coupon->save();
        }
    }
}
