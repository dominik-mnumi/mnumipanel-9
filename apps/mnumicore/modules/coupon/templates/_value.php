<?php if($row->getType() == CouponTable::$amount): ?>
<?php echo $sf_user->formatCurrency($row->getValue()); ?>
<?php else: ?>
<?php echo $row->getValue(); ?>%
<?php endif; ?>

<?php if($row->getUsed()): ?>
(<?php echo __('used'); ?>)
<?php endif; ?>
