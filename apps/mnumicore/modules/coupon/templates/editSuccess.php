<form id="coupon_form" method = "post" class="form" action="<?php echo $form->isNew() ? '' : url_for('couponEdit', $form->getObject())?>">
    <?php echo $form->renderHiddenFields() ?>

    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'coupon')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content">
                    <?php $form->renderHiddenFields() ?>
                    <h1>
                    <?php if(isset($form['count'])): ?>
                        <?php echo __('Create many coupons') ?>
                    <?php elseif($form->isNew()): ?>
                        <?php echo __('Create coupon') ?>
                    <?php else: ?>
                        <?php echo __('Edit coupon') ?>
                    <?php endif; ?>
                    </h1>
                    
                    <?php if(isset($form['name'])): ?>
                    <p>
                        <?php echo $form['name']->renderError(); ?>
                        <?php echo $form['name']->renderLabel(); ?>
                        <?php echo $form['name']->render(); ?>
                    </p>
                    <?php endif ?>
                    
                    <?php if(isset($form['count'])): ?>
                    <p>
                        <?php echo $form['count']->renderError(); ?>
                        <?php echo $form['count']->renderLabel(); ?>
                        <?php echo $form['count']->render(); ?>
                    </p>
                    <?php endif; ?>

                    <p>
                        <?php echo $form['type']->renderError(); ?>
                        <?php echo $form['type']->renderLabel(); ?>
                        <?php echo $form['type']->render(); ?>
                    </p>
                    
                    <p>
                        <?php echo $form['value']->renderError(); ?>
                        <?php echo $form['value']->renderLabel(); ?>
                        <?php echo $form['value']->render(); ?>
                    </p>
                    
                    <div id="products">
                        <?php echo $form['custom_products']->renderError(); ?>
                        <?php echo $form['custom_products']->renderLabel(); ?>
                        <?php echo $form['custom_products']->render(); ?>
                    </div>
                    <br/>
                    <div id="pricelists">
                        <?php echo $form['custom_pricelists']->renderError(); ?>
                        <?php echo $form['custom_pricelists']->renderLabel(); ?>
                        <?php echo $form['custom_pricelists']->render(); ?>
                    </div>
                    
                    <p class="margin-top10">
                        <?php echo $form['expire_at']->renderError(); ?>
                        <?php echo $form['expire_at']->renderLabel(); ?>
                        <span class="input-type-text margin-right relative">
                        <?php echo $form['expire_at']->render(); ?>
                        </span>
                    </p>
                    
                    <?php if(isset($form['one_time_usage'])): ?>
                    <p>
                        <?php echo $form['one_time_usage']->renderError(); ?>
                        <?php echo $form['one_time_usage']->renderLabel(); ?>
                        <?php echo $form['one_time_usage']->render(); ?>
                    </p>
                    <?php endif; ?>
                    
                    <p>
                        <?php echo $form['used']->renderError(); ?>
                        <?php echo $form['used']->renderLabel(); ?>
                        <?php echo $form['used']->render(); ?>
                    </p>
                </div>
            </div>
        </section>
    </article>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        function pricelistSelect()
        {
            if(!$("[id$='_custom_pricelists___ALL__']").is(':checked'))
            {
                $('#pricelists .checkbox_list li:not(:first)').show();
            }
            else
            {
                $('#pricelists .checkbox_list li:not(:first)').hide();
            }
        }
        $("[id$='_custom_pricelists___ALL__']").click(function() {
            pricelistSelect();
        });
        pricelistSelect();

        
        function productSelect()
        {
            if(!$("[id$='_custom_products___ALL__']").is(':checked'))
            {
                $('#products .checkbox_list li:not(:first)').show();
            }
            else
            {
                $('#products .checkbox_list li:not(:first)').hide();
            }
        }
        $("[id$='_custom_products___ALL__']").click(function() {
        	productSelect();
        });
        
        productSelect();
     
    });
</script>