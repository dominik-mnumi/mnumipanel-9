<?php

/**
 * coupon actions.
 *
 * @package    mnumicore
 * @subpackage coupon
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class couponActions extends tablesActions
{
   /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    {
        $this->displayTableFields = array(
            'Name' => array(),
            'Value' => array('partial' => 'value'),
            'ExpireAt' => array('label' => 'Expiry date'),
            'CouponProducts' => array(
                'label' => 'Products',
                'partial' => 'products'),
            'CouponPricelist' => array(
                'label' => 'Pricelists',
                'partial' => 'pricelists')
        );

        $this->modelObject = 'Coupon';
        $this->availableViews = array('table');
        
        $this->actions['exportToCsv'] = array('manyRoute' => 'couponListExportCsv',
            'label' => 'Export to CSV (selected)');
        $this->actions['exportToCsvAll'] = array('manyRoute' => 'couponListExportCsvAll',
            'label' => 'Export to CSV (all)');
        
        $this->customQuery = CouponTable::getInstance()->getTableRecordsQuery();
        $this->filterForm = new CouponFormFilter();

        $this->tableOptions = $this->executeTable();
        
        if($request->isXmlHttpRequest())
        {
          return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }
    
    /**
     * Executes create action
     *
     * @param sfRequest $request A request object
     */
    public function executeCreate(sfWebRequest $request)
    {
        $this->form = new CouponForm();
        $this->proceedForm($this->form, $request);
        $this->setTemplate('edit');
    }
    
    /**
     * Executes createMany action
     *
     * @param sfRequest $request A request object
     */
    public function executeCreateMany(sfWebRequest $request)
    {
        $this->form = new CouponManyForm();
        $this->proceedForm($this->form, $request);
        $this->setTemplate('edit');
    }
    
    /**
     * Executes edit action
     *
     * @param sfRequest $request A request object
     */
    public function executeEdit(sfWebRequest $request)
    {
        $this->form = new CouponForm($this->getRoute()->getObject());
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes delete action
     *
     * @param sfRequest $request A request object
     */
    public function executeDelete(sfWebRequest $request)
    {
        $this->getRoute()->getObject()->delete();
        $this->getUser()->setFlash('info_data', array(
            'message' => 'Deleted successfully.',
            'messageType' => 'msg_success',
        ));
        $this->redirect('@coupon');
    }
    
    /**
     * Proceed form data
     *
     * @param object $form
     * @param sfRequest $request A request object
     */
    private function proceedForm($form, $request)
    {
        if(!$request->isMethod('post'))
        {
            return;
        }
      
        $form->bind($request->getParameter($form->getName()));
      
        if($form->isValid())
        {
            $form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            
            //if many coupons was generated
            if(!$form->getObject()->getName())
            {
                $this->redirect('@coupon');
            }
            else
            {
                $this->redirect('@couponEdit?name=' . $form->getObject()->getName());
            }
        }
    }
    
    /**
     * Executes delete action
     *
     * @param sfRequest $request A request object
     */
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Coupon';
        parent::executeDeleteMany($request);
    }
    
    /** 
    * Export coupon list as file.
    *
    * @param sfWebRequest $request
    */
    public function executeExportCouponList(sfWebRequest $request)
    {
        $sfFormat = $request->getParameter('sf_format');
        
        $timeStart = microtime(true);
        if($request->getParameter('type', '') == 'all')
        {
           $this->customQuery = CouponTable::getInstance()->getTableRecordsQuery();
           $this->modelObject = 'Coupon';

            $this->filterForm = new CouponFormFilter();

           $this->executeTable();
          $coupons = $this->getQuery()->execute();
        }
        else
        {
          $couponNames = $request->getParameter('selected');
          
          if($couponNames == '')
          {
              $this->getUser()->setFlash('info_data', array(
                  'messageType' => 'msg_error',
                  'message' => 'Please select some items.',
              ));
              $this->redirect('@coupon');
          }
          $coupons = CouponTable::getInstance()->getCouponByNames($request->getParameter('selected'));
          
        }
        $timeEnd = microtime(true);
        
        $items = array();
        foreach($coupons as $coupon)
        {
          $items[] = array(
              'name' => $coupon->getName(),
              'type' => $coupon->getType(),
              'value' => $coupon->getValue(),
              'apply_for_all_products' => $coupon->getApplyForAllProducts(),
              'apply_for_all_pricelist' => $coupon->getApplyForAllPricelist(),
              'expire_at' => $coupon->getExpireAt(),
              'one_time_usage' => $coupon->getOneTimeUsage(),
              'used' => $coupon->getUsed()
          );
        }
      
        $columnsHeaders = array(
            'name' => 'Coupon name',
            'type' => 'Type',
            'value' => 'Value',
            'apply_for_all_products' => 'Apply for all products',
            'apply_for_all_pricelist' => 'Apply on all pricelists',
            'expire_at' => 'Expire at',
            'one_time_usage' => 'One time usage',
            'used' => 'Used'
        );
      
        $content = $this->getPartial('global/exportList',
            array('items' => $items,
                'buildTime' => $timeEnd - $timeStart,
                'columnsHeaders' => $columnsHeaders));
      
        $response = $this->getResponse();
        $response->setContentType('text/'.$sfFormat);
        $response->setHttpHeader('Content-Disposition',
            'attachment; filename="couponListTo'
            .ucfirst($sfFormat)
            .'_'.date("Y-m-d_G:i:s").'.'.$sfFormat.'"');
      
        $response->setContent($content);
      
        return sfView::NONE;
    }
}
