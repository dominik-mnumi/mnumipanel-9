<?php use_helper('allTimeBack') ?>
<?php $result = $sf_data->getRaw('result') ?>
<?php 
// translate messages
if(isset($result['message']) && $result['message'] == 'wrong_status')
{
    $result['message'] = array('type' => 'error', 'text' => __('Could not do this operation. Wrong order status: ') . __($object->getOrderStatusName()));
}

//proper createdAt date viewing
if(isset($result['worklog']['previousWorklogs']))
{
    foreach($result['worklog']['previousWorklogs'] as $k => $worklog)
    {
        $result['worklog']['previousWorklogs'][$k]['createdAt'] = allTimeBack($worklog['createdAt']) .  __('ago');
    }
}
  
?>
<?php echo json_encode($result) ?>