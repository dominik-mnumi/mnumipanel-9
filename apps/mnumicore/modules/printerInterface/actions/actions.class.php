<?php

/**
 * printerInterface actions.
 *
 * @package    mnumicore
 * @subpackage printerInterface
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class printerInterfaceActions extends sfActions
{
   /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    {
        $this->form = new OrderWorklogForm();
    }
    
    /**
     * Executes barcode action
     *
     * @param sfRequest $request A request object
     */
    public function executeBarcode(sfWebRequest $request)
    {
        $barcodePrinter = new BarcodePrinter($request->getParameter('barcode'));
        $type = $barcodePrinter->getTableNameFromType();
        
        //if type is user request is for authorize
        if($type == 'sfGuardUserTable')
        {
            $this->result = array('auth' => $this->getUser()->authorizeUsingPasswordSaltAndId($barcodePrinter->getId(), $barcodePrinter->getAdditionalNumber()));
            $this->object = $barcodePrinter->getObject();
            return sfView::SUCCESS;
        }
        // save new worklog
        elseif($type == 'OrderTable')
        {
            if(!$this->getUser()->isAuthorizedUsingPasswordSaltAndId())
            {
                throw new Exception('You have to be authorized to create new worklog.');
            }
            $this->result = $barcodePrinter->orderWorklog($this->getUser()->getAuthorizedUsingPasswordSaltAndIdUserId());
            $this->object = $barcodePrinter->getObject();
            return sfView::SUCCESS;
        }
        
        throw new Exception('This type of barcode is not supported in printerInterface.');
    }
    
    /**
     * Executes worklogSave action
     *
     * @param sfRequest $request A request object
     */
    public function executeWorklogSave(sfWebRequest $request)
    {
        $worklogParams = $request->getParameter('OrderWorklogForm');
        $worklog = OrderWorklogTable::getInstance()->find($worklogParams['id']);
        
        if(!$worklog)
        {
            throw new Exception('You try to edit worklog that does not exist.');
        }
        
        $this->form = new OrderWorklogForm($worklog);
        
        // throw exception if user is not authorized or does not have permissions
        if(!$this->getUser()->isAuthorizedUsingPasswordSaltAndId() || ($this->getUser()->getAuthorizedUsingPasswordSaltAndIdUserId() != $worklog->getUserId()))
        {
            throw new Exception('You dont have permissions to change this orderWorklog.');
        }
        $this->form->bind($worklogParams);
        
        if($this->form->isValid())
        {
            $this->form->save();
            
            $this->previousWorklogs = array();
            foreach($this->form->getObject()->getOrder()->getSortedOrderWorklogs() as $worklog)
            {
                $this->previousWorklogs[] = array(
                    'username'  => $worklog->getSfGuardUser()->__toString(),
                    'value'     => $worklog->getValue(),
                    'notice'    => $worklog->getNotice(),
                    'createdAt' => $worklog->getCreatedAt()
                );
            }
        }
        $this->setLayout(false);
    }
    
    /**
     * Executes removeAuthorization action
     *
     * @param sfRequest $request A request object
     */
    public function executeRemoveAuthorization(sfWebRequest $request)
    {
        $this->getUser()->removeAuthorizationUsingPasswordSaltAndId();
        return sfView::NONE;
    }
}
