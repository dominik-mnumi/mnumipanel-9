<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * MnumiQL form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class MnumiQLForm extends sfForm
{
    public function configure()
    {        
        $this->setWidget('query', new sfWidgetFormTextarea(
                        array(),
                        array('rows' => '10', 'class' => 'full-width')));
        
        $this->useFields(array('query'));
    } 
}

