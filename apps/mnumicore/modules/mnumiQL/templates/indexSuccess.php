<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <form id="mnumiql-form" class="block-content form" action="<?php echo url_for('mnumiQL'); ?>" method="post">
                <h1><?php echo __('Advanced search'); ?></h1>
                <div class="block-controls height-25px"></div>       
                
                <div class="columns">
                    <div class="col200pxL-left">
                        <dl class="accordion">
                            <dt><?php echo __('Tables'); ?></dt>
                            <dd class="scroll-box-200px-height">
                                <?php foreach(MnumiQLClauseFrom::$availableModelArr as $rec): ?>
                                <p><a href="#" class="add-phrase-to-textarea textarea-content"><?php echo $rec; ?></a></p>
                                <?php endforeach; ?>    
                            </dd>

                            <dt class="scroll-box-200px-height"><?php echo __('Clauses'); ?></dt>
                            <dd>
                                <?php foreach(MnumiQLClause::$querySectionArr as $rec): ?>
                                <p><a href="#" class="add-phrase-to-textarea textarea-content"><?php echo $rec; ?></a></p>
                                <?php endforeach; ?>    
                            </dd>

                            <dt><?php echo __('Filter operators'); ?></dt>
                            <dd class="scroll-box-200px-height">
                                <?php foreach(MnumiQLClauseWhere::$operatorArr as $rec): ?>
                                <p><a href="#" class="add-phrase-to-textarea textarea-content"><?php echo $rec; ?></a></p>
                                <?php endforeach; ?>
                            </dd> 
                            <dt><?php echo __('Query examples'); ?></dt>
                            <dd class="scroll-box-200px-height">
                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('Active users'); ?></a>
                                    <span class="textarea-content hidden">FROM SfGuardUser WHERE is_active = 1</span>
                                </p>
                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('Users who in a given period of time ordered more than 5 orders for the amount over 5,000 (see e-mail and phone)'); ?></a>
                                    <span class="textarea-content hidden">SELECT User.username, UserContact.value 
FROM UserContact INNER JOIN UserContact.User User LEFT JOIN User.Orders Orders
WHERE DATE_FORMAT(Orders.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Orders.created_at, '%Y-%m-%d') < '2014-10-10'
GROUP BY User.id, UserContact.id HAVING COUNT(Orders.id) > 5 AND SUM(Orders.price_net) > 5000</span>
                                </p>
                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('Sales of the product in a given period of time, the number of orders and total sales'); ?></a>
                                    <span class="textarea-content hidden">SELECT Product.name, COUNT(Orders.id), SUM(Orders.price_net)
FROM Product INNER JOIN Product.Orders Orders
WHERE DATE_FORMAT(Orders.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Orders.created_at, '%Y-%m-%d') < '2014-10-10'
GROUP BY Product.id</span>
                                </p>
                                
                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('Number of orders and for how much at a time'); ?></a>
                                    <span class="textarea-content hidden">SELECT COUNT(Order.id), SUM(Order.price_net)
FROM Order
WHERE DATE_FORMAT(Order.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Order.created_at, '%Y-%m-%d') < '2014-10-10'</span>
                                </p>
                                
                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('Sales invoices from the period of time, specifying the method of payment, the amount of net / gross, the status of paid / unpaid'); ?></a>
                                    <span class="textarea-content hidden">SELECT Invoice.number, Invoice.invoice_type_name, Invoice.price_net, Invoice.price_gross, Invoice.invoice_status_name, Payment.label
FROM Invoice INNER JOIN Invoice.Payment Payment
WHERE DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') < '2014-10-10'</span>
                                </p>
                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('All orders in the given period of time, of customers with the given tag'); ?></a>
                                    <span class="textarea-content hidden">SELECT Order.id, Order.price_net, Order.total_cost, Client.fullname
FROM Order LEFT JOIN Order.Client Client LEFT JOIN Client.ClientTags ClientTag
WHERE DATE_FORMAT(Order.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Order.created_at, '%Y-%m-%d') < '2014-10-10' AND ClientTag.name = 'tag'</span>
                                </p>

                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('All customers and their addressess'); ?></a>
                                    <span class="textarea-content hidden">SELECT Client.id, Client.fullname, Client.city, Client.postcode, Client.street, Client.country, Client.tax_id, Client.created_at, Client.updated_at, ClientAddress.city, ClientAddress.postcode, ClientAddress.street FROM ClientAddress
INNER JOIN ClientAddress.Client Client</span>
                                </p>

                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('E-mails and details of customers who bought product with unique name: banners'); ?></a>
                                    <span class="textarea-content hidden">SELECT User.first_name, User.last_name, User.username, UserContact.value
FROM UserContact INNER JOIN UserContact.User User LEFT JOIN User.Orders Orders LEFT JOIN Orders.Product Product
WHERE Product.name = 'banners'</span>
                                </p>

                                <p>
                                    <a href="#" class="add-phrase-to-textarea example"><?php echo __('The sum of the amounts of invoices from a given date, broken down into their payment status and type'); ?></a>
                                    <span class="textarea-content hidden">SELECT Invoice.invoice_type_name, Invoice.invoice_status_name, COUNT(Invoice.id), SUM(Invoice.price_net), SUM(Invoice.price_gross)
FROM Invoice
WHERE DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') >= '2014-01-01' 
GROUP BY Invoice.invoice_type_name, Invoice.invoice_status_name</span>
                                </p>

                            </dd> 
                        </dl>

                    </div>
                    <div class="col200pxL-right padding-left20px">
                        <?php echo $form['query']->render(); ?>  
                        <div id="hint" style="display: none;">
                            <?php echo image_tag('loader.gif', 
                                array('width' => 40,
                                    'height' => 40)); ?>
                            <ul>
                                
                            </ul>                            
                        </div>
                    </div>
                </div>
                             
                <div class="margin-top10 text-right">
                    <button><?php echo __('Ask'); ?></button>
                </div>
            </form>
        </div> 
    </section>    

    <?php if(isset($coll)): ?>
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content">
                <h1><?php echo __('Results'); ?></h1>
                <div class="block-controls">
                    <div class="controls-buttons">
                        <ul class="controls-buttons"></ul>
                        <br>
                    </div>
                </div>
                    
                <div class="no-margin">    
                    <?php if(isset($columnToDisplayArr)): ?>
                    <table class="table" cellspacing="0" width="100%">
                        <?php include_partial('tableHeader', array('columnToDisplayArr' => $columnToDisplayArr)); ?>
                        <?php foreach($coll as $rec): ?>
                        <tr>
                            <?php foreach($columnToDisplayArr as $key2 => $rec2): ?>
                            <td>
                                <?php echo $rec->get($key2); ?>
                            </td>
                            <?php endforeach; ?>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php endif; ?>
                </div>             
            </div>
        </div> 
    </section> 
    <?php endif; ?>
</article>



<!-- sets slot -->
<?php extendSlot('actionJavascript'); ?>
<?php include_partial('mnumiQL/javascript'); ?>
<?php end_slot(); ?>
