<script type="text/javascript">
$(document).ready(function($) 
{
    /**
     * Shows hint loader.
     */
    function showHintLoader()
    {
        $("#hint").show();
        $("#hint img").show();
    }
    
    /**
     * Hides hint loader.
     */
    function hideHintLoader()
    {
        $("#hint").hide();
        $("#hint img").hide();
    }
    
    /**
     * Adds phrase (SELECT, FROM or table name) to current query.
     */
    $(".add-phrase-to-textarea").click(function(e)
    {
        e.preventDefault();
        
        if($(this).hasClass('example'))
        {
            $("#query").val('');
        }

        // insert text in caret place
        insertAtCaret('query', $(this).parent().find(".textarea-content").text());
    });
    
    /**
     * Requests about hint after dot char.
     */
    $("#query").keypress(function(e)
    {  
        var code = (e.keyCode ? e.keyCode : e.which); 
        if(code == 46) 
        { 
            var textbox = $("#query");
            var end = textbox.getSelection().end;
            var result = /\S+$/.exec(this.value.slice(0, end));
            var key = result ? result[0] : null;

            var url = '<?php echo url_for('reqColumnHint'); ?>';
          
            showHintLoader();
            $("#hint ul").html('');
          
            $.ajax(
            {
                type: 'POST',
                url: url,
                data: { key: key },
                dataType: 'json',
                error: function(xhr, ajaxOptions, thrownError)
                {
                    alert('<?php echo __('There was error. Please try again later.') ?>');
                },
                success: function(dataOutput)
                {
                    if(dataOutput.length)
                    { 
                        $.each(dataOutput, function(index, element)
                        {
                            $("#hint ul").append('<li><a href="#">' + element + '</a></li>');
                        });    
                        
                        $("#hint img").hide();
                    }
                    else
                    {
                        hideHintLoader();
                    }
                }
            });
        }
        else
        {
            hideHintLoader();
        }
    });
    
    /** 
     * Hides hint on esc.
     */
    $("#query").keyup(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which); 
        if(code == 27) 
        { 
            $("#hint").hide();           
        }
    });  
    
    /** 
     * Hides hint when href clicked.
     */
    $("#hint").on("click", "#hint a", function(e)
    {
        e.preventDefault();

        // insert text in caret place
        insertAtCaret('query', $(this).text());
        
        $("#hint").hide();
    });
});


</script>