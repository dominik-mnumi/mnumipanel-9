<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once(dirname(__FILE__) . '/../lib/BasesfGuardAuthActions.class.php');

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: actions.class.php 23319 2009-10-25 12:22:23Z Kris.Wallsmith $
 */
class sfGuardAuthActions extends BasesfGuardAuthActions
{

    public function executeSignin($request)
    {
        $user = $this->getUser();

        if($user->isAuthenticated())
        {
            return $this->redirect('@homepage');
        }

        //login form
        $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
        $form = new $class();

        //forgot pass form
        $forgotPassForm = new myGuardRequestForgotPasswordForm();

        if($request->isMethod('post') && $request->getParameter('signin'))
        {
            $form->bind($request->getParameter('signin'));
            if($form->isValid())
            {
                $values = $form->getValues();
 
                // checks if current user can login in panel
                // if not redirect to security page
                $signInResult = $this->getUser()->signin($values['user'], array_key_exists('remember', $values) ? $values['remember'] : false);
                if(!$signInResult)
                {
                    return $this->forward('sfGuardAuth', 'secure');
                }
                
                // always redirect to a URL set in app.yml
                // or to the referer
                // or to the homepage
                $signinUrl = sfConfig::get('app_sf_guard_plugin_success_signin_url', $user->getReferer($request->getReferer()));

                return $this->redirect('' != $signinUrl ? $signinUrl : '@homepage');
            }
            // set last failed login
            else
            {
                $values = $request->getParameter('signin');
                $user = sfGuardUserTable::getInstance()->retrieveByUsernameOrEmailAddress($values['username']);
                if($user)
                {
                    $user->getProfile()->setLastFailedLogin(date('Y-m-d H:i:s'))->save();
                }
            }
        }
        else if($request->isMethod('post') && $request->getParameter('forgot_password'))
        {
            $forgotPassForm->bind($request->getParameter('forgot_password'));
            if($forgotPassForm->isValid())
            {                
                $forgotPassObj = $forgotPassForm->save();
                $userObj = SfGuardUserTable::getInstance()->find($forgotPassObj->getUserId());

                // triggers the event
                $this->dispatcher->notify(new sfEvent($this,
                        'user.password.lost',
                        array('obj' => $userObj)));
                
                $this->getUser()->setFlash('notice', 'Thank you. You should receive an email with instructions shortly.');
                return $this->redirect('homepage');            
            }
        }
        else
        {
            if($request->isXmlHttpRequest())
            {
                $this->getResponse()->setHeaderOnly(true);
                $this->getResponse()->setStatusCode(401);

                return sfView::NONE;
            }

            // if we have been forwarded, then the referer is the current URL
            // if not, this is the referer of the current request
            $user->setReferer($this->getContext()->getActionStack()->getSize() > 1 ? $request->getUri() : $request->getReferer());

            $module = sfConfig::get('sf_login_module');
            if($this->getModuleName() != $module)
            {
                return $this->redirect($module . '/' . sfConfig::get('sf_login_action'));
            }

            $this->getResponse()->setStatusCode(401);
        }

        //view objects
        $this->form = $form;
        $this->forgotPassForm = $forgotPassForm;
    }

    
}
