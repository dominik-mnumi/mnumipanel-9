<div class="login-bg">
    <!-- The template uses conditional comments to add wrappers div for ie8 and ie7 - just add .ie, .ie7 or .ie6 prefix to your css selectors when needed -->
    <!--[if lt IE 9]><div class="ie"><![endif]-->
    <!--[if lt IE 8]><div class="ie7"><![endif]-->	
    <section id="login-block">
    	<div class="block-border">
              <div class="block-content">
                  <!--
                  IE7 compatibility: if you want to remove the <h1>,
                  add style="zoom:1" to the above .block-content div
                  -->
                  <h1><?php echo __('Permissions')?></h1>
                  <h2><?php echo __("The page you asked for is secure and you do not have proper credentials.")?></h2>
              </div>
          </div>
    </section>
</div>
