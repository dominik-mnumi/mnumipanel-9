<?php 
try
{
    $server->handle(); //handle rest server
}
catch (Exception $e)
{
    header('Content-Type: text/xml;');
    echo 
    '<?xml version="1.0" encoding="UTF-8"?> 
     <exception>
      <status>failed</status> 
      <code>1</code>
      <message>'.$e->getMessage().'</message>
     </exception>';
}