<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * REST server handler.
 *
 * @author     Maciej Lisowski
 */
class apiActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('text/xml');

        //init Rest server
        $this->server = new Zend_Rest_Server;
        $this->server->setClass('RestServerAdapter'); //set rest adapter class
        //$adapter = new restAdapter;
        $this->server->setEncoding('UTF-8'); //set encoding

        // different view for development enviroment
        if(sfConfig::get('sf_environment') == 'dev')
        {
            return 'DevSuccess';
        }
    }

}
