<?php
/**
 * Arguments:
 * - withSave (default always, if set and false then remove),
 * - withReminder (default never, if set and true then add),
 * - route
 * - routeBack => ...,
 * - routeCancel => ...;
 * 
 * If routeBack or routeCancel does not exist then overwrite 
 * them by route parameter.
 */
?>

<?php if(!isset($routeBack)): ?>
<?php $routeBack = $route; ?>
<?php endif; ?>

<?php if(!isset($routeCancel)): ?>
<?php $routeCancel = $route; ?>
<?php endif; ?>

<div class="grey-bg clearfix" id="control-bar" style="opacity: 1;">
    <div class="container_12">
        <div class="float-left">
            <?php echo link_to(image_tag('/images/icons/fugue/navigation-180.png'). ' ' .__('Back'), 
                    substr($routeBack, 0, 1) == '@' ? $routeBack : '@'.$routeBack, 
                    array('class' => 'big-button', 
                        'title' => __('Back'))); ?>
        </div>

        <div class="float-right">
            <button id="notification-preview" class="grey" type="button">
                <?php echo __('Preview'); ?>
            </button>
            <button class="red" type="button" onclick="document.location.href='<?php echo url_for($routeCancel); ?>'" ><?php echo __('Discard') ?></button> 
            <?php if(!isset($withSave) || $withSave): ?>
            <button type="submit" id="saveAndReturn"><img width="16" height="16" src="/images/icons/fugue/tick-circle.png"> <?php echo __('Save') ?></button>
            <?php if(isset($saveAndAdd) && $saveAndAdd): ?>
            <button type="submit" id="saveAndAddButton"><img width="16" height="16" src="/images/icons/fugue/tick-circle.png"><?php echo __('Save and add') ?></button>
            <input type = "hidden" name="saveAndAdd" id="saveAndAdd" value="0" />
            <script type="text/javascript" language="javascript">
                $(document).ready(function() {
                    $('#saveAndAddButton').click(function(){
                        $('#saveAndAdd').val('1');
                        return true;
                    });
                });
            </script>
            <?php endif; ?>
            <?php endif; ?>
        </div>     
    </div>
</div>

<script type="text/javascript">
    /**
     * Services preview action.
     */
    $("#notification-preview").click(function()
    {
        // updates textarea (from tinyMCE)
        $("#notification_content").html(tinyMCE.activeEditor.getContent({format : 'raw'}));

        var form = $("#new_field_category_form");
        var dataInput = form.serialize();
        $.ajax(
        {
            url: '<?php echo url_for('settingsNotificationPreview'); ?>',
            type: 'POST',
            data: dataInput,
            success: function(response)
            {
                var generator = window.open(
                    '',
                    'popup',
                    'height=600,width=700,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');

                generator.document.write(response);
                generator.document.close();
            }
        });
    });

</script>