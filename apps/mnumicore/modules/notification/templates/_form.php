<?php echo $form->renderGlobalErrors(); ?>

<?php echo $form['notification_template_id']->renderError(); ?>
<p class="required">
    <?php echo $form['notification_template_id']->renderLabel(); ?>
    <?php echo $form['notification_template_id']->render(); ?>
</p>

<?php echo $form['notification_type_id']->renderError(); ?>
<p class="required">
    <?php echo $form['notification_type_id']->renderLabel(); ?>
    <?php echo $form['notification_type_id']->render(); ?>
</p>


<?php echo $form['title']->renderError(); ?>
<p class="required">
    <?php echo $form['title']->renderLabel(); ?>
    <?php echo $form['title']->render(); ?>
</p>

<?php echo $form['bcc']->renderError(); ?>
<p>
    <?php echo $form['bcc']->renderLabel(); ?>
    <?php echo $form['bcc']->render(); ?>
</p>

<?php echo $form['content']->renderError(); ?>
<p class="required">    
    <?php echo $form['content']->renderLabel(); ?>        
    <?php echo $form['content']->render(); ?>                                    
</p> 