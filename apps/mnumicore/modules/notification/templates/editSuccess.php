<form id="new_field_category_form" method = "post" class="form"  action="<?php echo $form->isNew() ? url_for('settingsNotificationAdd') : url_for('settingsNotificationEdit', $form->getObject()) ?>">
    <?php echo $form->renderHiddenFields() ?>

    <?php echo include_partial('notification/formActions',
            array('route' => 'settingsNotification')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <?php echo include_component('notification', 'navigation'); ?>
        </section>
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo $form->isNew() ? __('Create notification') : __('Edit notification'); ?></h1>
                    <?php include_partial('form', array('form' => $form)); ?>

                    <?php include_component('notification', 'shortcodeInfo'); ?>
                </div>
            </div>
        </section>
    </article>
</form>

