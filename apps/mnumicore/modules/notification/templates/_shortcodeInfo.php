<h2><?php echo __('Tags which you can use in this e-mail'); ?>:</h2>

<dl class="accordion margin-top20">
    <dt>
        <?php echo __('Basic tags'); ?>
    </dt>
    <dd>
        <?php foreach($globalTags as $key => $rec2): ?>
            <p>
                <input type="text" value="{{<?php echo $key; ?>}}" readonly="" /> <?php echo __($rec2['desc']); ?>
            </p>
        <?php endforeach; ?>
    </dd>

    <?php foreach($templates as $rec): ?>
        <?php
        $notification = $rec->getNotification();
        if(! $notification->hasLocalShortcodes()) continue;
        ?>
    <dt>
        <?php echo __($rec->getName()); ?>
    </dt>

    <dd>
        <?php foreach($notification->getLocalShortcodes() as $key => $rec2): ?>
        <p>
            <input type="text" value="{{<?php echo $key; ?>}}" readonly="" /> <?php echo __($rec2['desc']); ?>
        </p>
        <?php endforeach; ?>
    </dd>
    <?php endforeach; ?>
</dl>

<script type="text/javascript">
    jQuery(document).ready(function(){
        $(".accordion input").click(function() {
            $(this).select();
        });

    });
</script>