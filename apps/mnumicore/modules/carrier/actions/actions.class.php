<?php

/**
 * carrier actions.
 *
 * @package    mnumicore
 * @subpackage carrier
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class carrierActions extends tablesActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {   
      $this->displayTableFields = array('Name' => array('translate' => true),
          'Label' => array('translate' => true));
      $this->displayGridFields = array(
          'Name'  => array('destination' => 'name', 'translate' => true), 
          'DeliveryTime'  => array('destination' => 'keywords')
      );
      $this->sortableFields = array('name');
      $this->modelObject = 'Carrier';
      
      $this->tableOptions = $this->executeTable();
      
      if($request->isXmlHttpRequest())
      {
          return $this->renderPartial('global/tableHtml', $this->tableOptions);
      }
  }
  

    public function executeCreate(sfWebRequest $request)
    {
        $this->form = new CarrierForm();
        $this->proceedForm($this->form);
        $this->setTemplate('edit');
    }
    
    public function executeDelete(sfWebRequest $request)
    {
        $this->getRoute()->getObject()->delete();
        $this->getUser()->setFlash('info_data', array(
            'message' => 'Deleted successfully.',
            'messageType' => 'msg_success',
        ));
        $this->redirect('@settingsCarrier');
    }
    
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Carrier';
        parent::executeDeleteMany($request);
    }

    public function executeEdit(sfWebRequest $request)
    {
        $this->form = new CarrierForm($this->getRoute()->getObject());
        $this->proceedForm($this->form);
    }
    
    private function proceedForm($form)
    {
        if(!$this->getRequest()->isMethod('post'))
        {
            return;
        }

        $this->form->bind($this->getRequest()->getParameter($this->form->getName()));

        if($this->form->isValid())
        {
            $this->form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            $this->redirect('@settingsCarrierEdit?id='.$this->form->getObject()->id);
        }
    }
}
