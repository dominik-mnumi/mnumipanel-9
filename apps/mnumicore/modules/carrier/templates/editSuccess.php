<form id="new_field_category_form" method = "post" class="form" 
      action="<?php echo $form->isNew() ? url_for('settingsCarrierAdd') : url_for('settingsCarrierEdit', $form->getObject()) ?>">
          <?php echo $form->renderHiddenFields() ?>

    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'settingsCarrier')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo $form->isNew() ? __('Create carrier') : $form['name']->getValue() ?></h1>
                    <?php if($form->hasErrors()): ?>
                    <?php foreach($form->getFormFieldSchema() as $formField): ?>
                    <?php echo __($formField->renderError()); ?>
                    <?php endforeach; ?>
                    <?php endif ?>
                    <p>
                        <?php echo $form['name']->renderLabel() ?>
                        <?php echo $form['name']->render() ?>
                    </p>
                    <p>
                        <?php echo $form['label']->renderLabel() ?>
                        <?php echo $form['label']->render() ?>
                    </p>
                    <p>
                        <?php echo $form['delivery_time']->renderLabel() ?>
                        <?php echo $form['delivery_time']->render() ?>
                    </p>
                    <p>
                        <?php echo $form['active']->render() ?>
                        <?php echo $form['active']->renderLabel() ?>
                    </p>
                    <p>
                        <?php echo $form['require_shipment_data']->render() ?>
                        <?php echo $form['require_shipment_data']->renderLabel() ?>
                    </p>
                    <p>
                        <?php echo $form['tax']->renderLabel() ?>
                        <?php echo $form['tax']->render() ?>
                    </p>
                    <p>
                        <?php echo $form['restrict_for_cities']->renderLabel() ?>
                        <?php echo $form['restrict_for_cities']->render() ?>
                    </p>                 
                    
                    <?php echo $form['custom_pricelists']->renderLabel() ?>
                    <?php echo $form['custom_pricelists']->render() ?>

                    <p>
                        <?php echo $form['cost']->renderLabel() ?>
                        <?php echo $form['cost']->render() ?>
                    </p>
                    <p>
                        <?php echo $form['price']->renderLabel() ?>
                        <?php echo $form['price']->render() ?>                      
                    </p>
                    <p>
                        <?php echo $form['free_shipping_amount']->renderLabel() ?>
                        <?php echo $form['free_shipping_amount']->render() ?>
                    </p>

                </div>
            </div>
        </section>
    </article>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        function pricelistSelect()
        {
            if(!$('#carrier_custom_pricelists___ALL__').is(':checked'))
            {
                $('.checkbox_list li:not(:first)').show();
            }
            else
            {
                $('.checkbox_list li:not(:first)').hide();
            }
        }
        $('#carrier_custom_pricelists___ALL__').click(function() {
            pricelistSelect();
        });
        pricelistSelect();

        //carrier weight form interior mechanism
        $(".add_new_carrier_weight_row").live('click', function()
        {
            $(this).prev("table").children("tbody").children("tr:hidden").first().show();
        });

        $(".del_carrier_weight_row").live('click',
            function()
            {
                $(this).parents("tr").remove();
            });
     
    });
</script>