<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorNumberExtended
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki
 */
class sfValidatorCommaString extends sfValidatorRegex
{

    /**
     * Extend doClean method. Replacing last "," by ""
     *
     * @param string $value
     */
    protected function doClean($value)
    {
        $value = trim($value);

        $aReplace = array(',,,,,' => ',', ',,,,' => ',', ',,,' => ',', ',,' => ',',
            '     ' => ' ', '    ' => ' ', '   ' => ' ', '  ' => ' ');

        $value = str_replace(array_keys($aReplace), array_values($aReplace), $value);


        if(',' == substr($value, -1))
        {
            $value = substr($value, 0, -1);
        }

        return parent::doClean($value);
    }
}