<article class="container_12 bulk_update">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content">
                <form action="" method="post">
                <h1>
                    <?php echo __('Bulk update'); ?>
                    <?php echo sprintf(__('Step %s of %s'), 2, 3); ?>:
                    <?php echo __('Choose type of change'); ?>

                </h1>


                 <table class="table full-width">
                     <thead>
                     <tr>
                         <th><?php echo __('Available change actions'); ?></th>
                         <th><?php echo __('Affected settings'); ?></th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php if(count($fieldItemsWithoutPricelist) > 0): ?>
                     <tr>
                         <td>
                             <?php echo $form['operation']->render(array('only_choice' => 'new')); ?>
                         </td>
                         <td>
                             <?php $i=0; foreach($fieldItemsWithoutPricelist as $fieldItem):
                                 echo ( ($i>0) ? ', ' : '') . $fieldItem->getName();
                                 $i++; if($i > 20) break; endforeach; ?>
                         </td>
                     </tr>
                     <?php endif; ?>
                     <?php if(count($fieldItemsWithPricelist) > 0): ?>
                     <tr>
                         <td>
                             <?php echo $form['operation']->render(array('only_choice' => 'exist')); ?>
                         </td>
                         <td>
                             <?php $i=0; foreach($fieldItemsWithPricelist as $fieldItem):
                                   echo ( ($i>0) ? ', ' : '') . $fieldItem->getName();
                                 $i++; if($i > 20) break; endforeach; ?>
                         </td>
                     </tr>
                     <?php endif; ?>
                     </tbody>
                 </table>
                <?php echo $form->renderHiddenFields(); ?>

                <p>
                    <input type="submit" value="<?php echo __('Next'); ?> >>" />
                    <?php echo link_to(__('Cancel'), '@settingsFieldItemBulkUpdateCancel'); ?>
                </p>

                </form>
            </div>
        </div>
    </section>
</article>