<article class="container_12 bulk_update">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content">
                <form action="" method="post">
                    <h1>
                        <?php echo __('Bulk update'); ?>
                        <?php echo sprintf(__('Step %s of %s'), 2, 3); ?>:
                        <?php echo __('Update pricelist in the settings'); ?>

                    </h1>

                <h3>
                    <?php echo sprintf(__('Update pricelist <strong>%s</strong> for <strong>%d</strong> setting(s)'),
                        $pricelist, $count); ?>:
                </h3>

                <?php echo $form; ?>
                <span><?php echo __('For example: "5" means 5%, "-10" means -10%'); ?></span>

                <p>
                    <input type="submit" value="<?php echo __('Next'); ?> >>" />
                    <?php echo link_to(__('Cancel'), '@settingsFieldItemBulkUpdateCancel'); ?>
                </p>

                </form>
            </div>
        </div>
    </section>
</article>
