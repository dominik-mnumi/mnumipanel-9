<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */ 
class settingBulkUpdateForm extends sfForm
{
    public function configure()
    {
        $options = array(
            'settingsFieldItemBulkUpdateEditPricelist' => 'Update pricelist or create new one from settings',
            'settingsFieldItemBulkDeletePricelist' => 'Delete pricelist from settings',
        );

        $this->setWidget('pricelist', new sfWidgetFormDoctrineChoice(array(
            'model' => 'Pricelist',
            'expanded' => true,
            'query' => PricelistTable::getInstance()->getSelectFixPriceListQuery(),
            'order_by' => array('name', 'asc'),
            'label' => 'Pricelist, that you want use'
        )));

        $this->setWidget('action', new sfWidgetFormSelectRadio(array(
            'choices' => $options
        )));

        $this->setValidator('action', new sfValidatorChoice(array(
            'choices' => array_keys($options),
        )));

        $this->setValidator('pricelist', new sfValidatorDoctrineChoice(array(
            'model' => 'Pricelist',
            'query' => PricelistTable::getInstance()->getSelectFixPriceListQuery(),
        )));

        $this->widgetSchema->setNameFormat('settingBulkUpdate[%s]');
        $this->widgetSchema->setFormFormatterName('specialList');

        // add a post validator
        $this->validatorSchema->setPostValidator(
            new sfValidatorCallback(array('callback' => array($this, 'checkMeasureUnit')))
        );
    }

    /**
     * Check if all selected settings have same Measure Unit
     *
     * @param $validator
     * @param $values
     * @return mixed
     * @throws sfValidatorError
     */
    public function checkMeasureUnit($validator, $values)
    {
        if($values['action'] != 'settingsFieldItemBulkUpdateEditPricelist')
        {
            return $values;
        }

        $selected = FieldItemTable::getInstance()->findByIds(
            $this->getOption('selected')
        );

        if(count($selected) == 0)
        {
            return $values;
        }

        $defaultMeasure = $selected[0]->getMeasureUnitId();

        foreach($selected as $item)
        {
            if($defaultMeasure != $item->getMeasureUnitId())
            {
                throw new sfValidatorError($validator,
                    'The selected settings have different measure unit. Could not update or create pricelist. ');
            }
        }

        return $values;
    }
}
