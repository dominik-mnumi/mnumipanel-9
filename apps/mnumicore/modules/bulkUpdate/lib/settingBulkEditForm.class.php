<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */ 
class settingBulkEditForm extends sfForm
{
    public function configure()
    {
        $this->setWidget('price_change', new sfWidgetFormInput(array(
            'label' => 'Change price (percent)'
        )));

        $this->setValidator('price_change', new sfValidatorNumber(array(
            'min' => -1000,
            'max' => 1000,
        )));

        $this->widgetSchema->setNameFormat('settingBulkEdit[%s]');
        $this->widgetSchema->setFormFormatterName('specialList');
    }
}
