<?php

/**
 * reportPdf actions.
 *
 * @package    mnumicore
 * @subpackage reportPdf
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class reportPdfActions extends tablesActions
{
    public function preExecute()
    {
        if(sfConfig::get('sf_environment') == 'prod')
        {
            $this->getResponse()->setContentType('application/pdf');
        }
    }

    /**
     * Renders technical card
     *
     * @param sfWebRequest $request
     * @return template
     */
    public function executeOrderReport(sfWebRequest $request)
    {
        /** @var Order $orderObj */
        $orderObj = $this->getRoute()->getObject();

        // view objects
        $this->userObj            = $orderObj->getsfGuardUser();
        $this->orderObj           = $orderObj;
        $this->clientObj          = $orderObj->getClient();
        $this->orderPackageObj    = $orderObj->getOrderPackage();
        $this->carrierObj         = $this->orderPackageObj->getCarrier();
        $orderAttributes          = $orderObj->getOrderAttributes();
        $this->thumbSize = 450;

        $this->fileHost = (sfConfig::get('sf_environment') == 'dev')
            ? ($request->isSecure() ? 'https://' : 'http://') . $request->getHost()
            : 'file://';

        $this->fileMethod = (sfConfig::get('sf_environment') == 'dev')
            ? 'getUri'
            : 'getThumbFullFilePathWithExtension';

        foreach($orderObj->getFiles() as $file) {
            $file->createThumb($this->thumbSize, $this->thumbSize);
        }

        // get attributes - print
        $this->printAttr = array();

        foreach (array("count", "quantity", "size", "material", "print", "sides") as $field) {
            $this->printAttr[] = $orderObj->getAttribute($field);
        }
        $this->orderAttributesChunk = array();

        // get attributes - other
        $i = 6;
        $orderAttributeIds = array_keys($orderObj->getOthersOrderAttributesAsArray());
        foreach($orderAttributeIds as $attributeId) {
            $attribute = $orderObj->getAttribute($attributeId);
            /*
             * If product field is set as required and there is only one option
             * selected, field is displayed in order as a checkbox. When this
             * checkbox is disabled by employee, field in order is soft-deleted
             * so it is not accessible without hacking Doctrine code responsible
             * for fetching the field.
             * As a bugfix do not display this field. It is turned off so it
             * won't be a problem in PDF report.
             */
            if (!is_null($attribute) && ($attribute !== false)) {
                /* Add order attributes in 3 elemental chunks */
                $row = floor($i++/3);
                $this->orderAttributesChunk[$row][] = $attribute;
            }
        }

        $this->first = 2;

        // generate thumb for first images
        $i=0;
        /** @var File $fileObj */
        foreach($orderObj->getFiles() as $fileObj)
        {
            if($i > 1) continue;

            $fileObj->getThumbPath();
            $i++;
        }

        // load calculation
        $report = $orderObj->calculate();

        $calcuationDecorator = new CalculationDecorator($this->getContext()->getI18N());
        $this->calculation = $calcuationDecorator->getMeasureSize($report);
    }

    /**
     * Render Invoice document
     *
     * @param sfWebRequest $request
     */
    public function executeInvoice(sfWebRequest $request)
    {
        // view objects
        $this->invoices = array($this->getRoute()->getObject());
        $this->notice = sfConfig::get('app_invoice_notice');
        $this->duplicate = ($request->getParameter('type') == 'duplicate');

        $this->getUser()->setCulture(sfConfig::get('sf_default_culture', 'en'));
    }

    /**
     * Render Invoices document
     *
     * @param sfWebRequest $request
     * @return string
     */
    public function executeInvoices(sfWebRequest $request)
    {
        $this->modelObject = 'Invoice';

        $this->executeTable();

        // view objects
        $this->invoices = $this->getQuery()->execute();
        $this->notice = sfConfig::get('app_invoice_notice');
        $this->duplicate = ($request->getParameter('type') == 'duplicate');

        $this->getUser()->setCulture(sfConfig::get('sf_default_culture', 'en'));

        $this->setTemplate('invoice');
    }


    /**
     * Executes rest print action based on token.
     *
     * @param sfRequest $request A request object
     */
    public function executeRestInvoice(sfWebRequest $request)
    {
        $this->forward404If($request->getParameter('token') != sfConfig::get('sf_csrf_secret'));

        $this->executeInvoice($request);

        $this->setTemplate('invoice');
    }

    /**
     * Render sale report
     *
     * @param sfRequest $request A request object
     */
    public function executeSaleReport(sfWebRequest $request)
    {
        $this->index = 1;
        $this->page = 1;
        $this->taxes = TaxTable::getInstance()->findAll();

        $reportParameters = $request->getParameter(InvoiceSaleReport::BASE_FILTER_NAME_FORMAT);
        $invoiceSaleReport = new InvoiceSaleReport($reportParameters);

        $this->invoiceSaleReportArray = $invoiceSaleReport->toArray();
        $this->invoices = $invoiceSaleReport->getInvoices();
        $this->reportSum = $invoiceSaleReport->getSum();
        $this->currencyCode = CurrencyTable::getInstance()->findOneById($reportParameters['currency_id'])->getCode();
        $this->rowsPerPage = 30;
        $this->pagesCount = ceil(count($this->invoices) / $this->rowsPerPage);
    }
}
