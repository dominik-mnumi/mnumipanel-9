<?php
$orderName = ($orderObj->getName() == $orderObj->getProduct()->getName())
    ? $orderObj->getName()
    : sprintf('%s (%s)', $orderObj->getName(), $orderObj->getProduct()->getName())
;
?>
<div class="order">
    <table class="report">
        <tr>
            <td id="logo">
                <?php if(sfConfig::has('app_company_data_seller_logo_path')): ?>
                    <?php echo image_tag(sfConfig::get('app_company_data_seller_logo_path')); ?>
                <?php endif; ?>

                <h1><?php echo sprintf(__('Order <strong>%d</strong>: %s, added at %s'),
                        $orderObj->getId(),
                        $orderName,
                        format_datetime($orderObj->getAcceptedAt())
                    ); ?> </h1>
                <h2>
                    (<?php echo sprintf(__('in package %s'), $orderPackageObj); ?>: <?php
                        $i=0;
                        foreach($orderPackageObj->getOrders() as $item)
                        {
                            if($i>0) echo ", ";
                            echo $item->getId();
                            $i++;
                        }
                    ?>)
                </h2>
            </td>
            <td id="barcode">
                <img src="<?php echo BarcodeTool::getUrl('Order', $orderObj->getId()); ?>" />
                <h1><?php echo __('Report card'); ?></h1>
            </td>
        </tr>
    </table>

    <table class="frame">
        <tr>
            <td class="client">
                <h3><?php echo __('Customer'); ?></h3>
                <p>
                    <?php echo $clientObj->getFullname(); ?><br/>
                    <?php echo $clientObj->getPricelist()->getName(); ?>

                    <?php if($clientObj->getCreditLimitAmount() > 0): ?>
                        <?php echo __('with credit limit'); ?>
                        <?php echo $sf_user->formatCurrency($clientObj->getCreditLimitAmount()); ?>
                    <?php endif; ?>
                    (<?php echo __('balance'); ?>: <?php echo $sf_user->formatCurrency($clientObj->getBalance()); ?>)
                </p>

                <p>
                    <?php echo $userObj->__toString(); ?><br/>
                    <?php foreach($userObj->getUserContacts() as $contact): ?>
                        <?php echo $contact; ?><br/>
                    <?php endforeach; ?>
                </p>

                <?php if($orderObj->getInformations() != ''): ?>
                <hr/>
                <h3><?php echo __('Order notice'); ?></h3>
                <p class="notice"><?php echo $orderObj->getInformationsWrap(); ?></p>
                <?php endif; ?>

                <hr/>
                <h3><?php echo __('Print'); ?></h3>

                <?php if($orderObj->getProduct()->getNote() != ""): ?>
                <p class="note"><?php echo $orderObj->getProduct()->getNote(); ?></p>
                <?php endif; ?>

                <table class="table report report-attribute print-attribute">
                    <tr>
                        <?php echo include_partial('orderAttributeLabel', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[0])); ?>
                        <th>&nbsp;</th>
                        <?php echo include_partial('orderAttributeLabel', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[1])); ?>
                        <?php echo include_partial('orderAttributeLabel', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[2])); ?>
                        <th><?php echo __('Sheets/meters'); ?></th>
                    </tr>
                    <tr>
                        <?php echo include_partial('orderAttributeValue', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[0])); ?>
                        <td>x</td>
                        <?php echo include_partial('orderAttributeValue', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[1])); ?>
                        <?php echo include_partial('orderAttributeValue', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[2])); ?>
                        <td><?php echo $calculation; ?></td>
                    </tr>
                </table>

                <table class="table report report-attribute print-attribute">
                    <tr>
                        <?php echo include_partial('orderAttributeLabel', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[3])); ?>
                        <?php echo include_partial('orderAttributeLabel', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[4])); ?>
                    </tr>
                    <tr>
                        <?php echo include_partial('orderAttributeValue', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[3])); ?>
                        <?php echo include_partial('orderAttributeValue', array('orderObj' => $orderObj, 'attributeObj' => $printAttr[4])); ?>
                    </tr>
                </table>

                <table class="table report report-attribute print-attribute">
                    <tr>
                        <?php echo include_partial('orderAttributeLabel', array('attributeObj' => $printAttr[5])); ?>
                    </tr>
                    <tr>
                        <?php echo include_partial('orderAttributeValue', array('attributeObj' => $printAttr[5])); ?>
                    </tr>
                </table>


                <?php if(count($orderAttributesChunk) > 0): ?>
                    <hr/>
                    <h3><?php echo __('Finish'); ?></h3>

                    <?php foreach($orderAttributesChunk as $chunk): ?>
                            <table class="table report report-attribute">
                                <tr>
                                    <?php
                                    foreach($chunk as $orderAttr):
                                        echo include_partial('orderAttributeLabel', array('attributeObj' => $orderAttr));
                                    endforeach;
                                    ?>
                                </tr>
                                <tr>
                                    <?php
                                    foreach($chunk as $orderAttr):
                                        echo include_partial('orderAttributeValue', array('attributeObj' => $orderAttr));
                                    endforeach;
                                    ?>
                                </tr>
                                <tr>
                                    <?php foreach($chunk as $orderAttr): ?>
                                        <td class="timelog">
                                        ........................................<br/>
                                        <?php echo __('Who work, time spend'); ?>
                                        </td>
                                    <?php endforeach; ?>
                                </tr>
                            </table>
                    <?php endforeach; ?>
                <?php endif; ?>
            </td>
            <td class="sidebar">
                <?php
                $i=0;
                $nb = count($orderObj->getFiles()) + count($orderObj->getWizardOrderAttributes());

                if ($nb > 0): ?>
                <div id="attachments">
                    <?php foreach($orderObj->getFiles() as $fileObj): ?>
                    <p<?php echo $i > $first ? ' class="rest"' : ''; ?>>
                        <?php if($i == $first): ?>
                            <h6>+ <?php echo sprintf(__('%s files'), ($nb - $first)); ?></h6>
                            </p>
                            <?php break; ?>
                        <?php endif; ?>

                        <?php if($i < $first): ?>
                        <img src="<?php echo $fileHost . $fileObj->$fileMethod($thumbSize, $thumbSize); ?>" />
                        <?php endif; ?>
                        <h5><?php echo $fileObj->getFilename(); ?></h5>
                        <?php if($fileObj->getNotice() != ''): ?>
                            <span><?php echo $fileObj->getNotice(); ?></span>
                        <?php endif; ?>
                    </p>
                    <?php $i++; endforeach; ?>
                    <?php foreach($orderObj->getWizardFileInfoArray() as $wizardObj): ?>
                    <p<?php echo $i > $first ? ' class="rest"' : ''; ?>>
                        <?php if($i < $first): ?>
                            <img src="<?php echo $wizardObj['urlPreview']; ?>" />
                        <?php endif; ?>
                        <h5><?php echo $wizardObj['value']; ?></h5>
                    </p>
                    <?php $i++; endforeach; ?>
                </div>
                <?php endif; ?>

                <div id="designer" class="line">
                    <h3><?php echo __('Designer/Seller'); ?></h3>

                    <?php if(count($orderObj->getOrderTraders()) > 0): ?>
                    <h5><?php echo __('Calculation'); ?></h5>
                    <table>
                        <?php foreach($orderObj->getOrderTraders() as $trader): ?>
                        <tr>
                            <td><?php echo $trader->getSfGuardUser()->__toString(); ?></td>
                            <td class="trader-hours"><?php echo (float) $trader->getQuantity(); ?>h x <?php echo $sf_user->formatCurrency($trader->getPrice()); ?> </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php endif; ?>

                    <div class="cost">
                    <h5><?php echo __('Costs'); ?></h5>
                    <p>
                        ............. <strong>h</strong>
                        <strong>x</strong>
                        .................... <strong><?php echo $sf_user->getCurrencySymbol(); ?></strong>
                    <strong>=</strong>
                    ............................  </p>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table id="footer">
                    <tr>
                        <td>
                            <h3><?php echo __('Shippment'); ?></h3>
                            <p class="shipment"><?php echo $orderPackageObj->getCarrier()->__toString(); ?></p>
                            <p><?php echo $orderPackageObj->getDeliveryName(); ?></p>
                            <p><?php echo $orderPackageObj->getDeliveryStreet(); ?></p>
                            <p>
                                <?php echo $orderPackageObj->getDeliveryPostcode(); ?>
                                <?php echo $orderPackageObj->getDeliveryCity(); ?>
                            </p>
                            <?php if($orderPackageObj->getDescription() != ''): ?>
                                <p><?php echo $orderPackageObj->getDescription(); ?></p>
                            <?php endif; ?>
                        </td>
                        <td>
                            <h3><?php echo __('Invoice'); ?></h3>
                            <p><?php echo $orderPackageObj->getInvoiceName(); ?></p>
                            <p><?php echo $orderPackageObj->getInvoiceStreet(); ?></p>
                            <p>
                                <?php echo $orderPackageObj->getInvoicePostcode(); ?>
                                <?php echo $orderPackageObj->getInvoiceCity(); ?>
                            </p>
                            <p><?php echo __('Tax ID'); ?>: <?php echo $orderPackageObj->getInvoiceTaxId(); ?></p>
                        </td>
                        <td id="payment">
                            <h3><?php echo __('Payment'); ?></h3>
                            <h5><?php echo __('Payment method'); ?></h5>
                            <p>
                                <?php echo __($orderPackageObj->getPaymentStatus()); ?>,
                                <?php echo __($orderPackageObj->getPayment()->__toString()); ?>
                            </p>
                            <h5><?php echo __('Gross price'); ?></h5>
                            <p class="gross"><?php echo $sf_user->formatCurrency($orderObj->getTotalAmountGross()); ?> <?php echo __('gross'); ?></p>
                            <p><small><?php echo $sf_user->formatCurrency($orderObj->getTotalAmount()); ?> <?php echo __('net'); ?></small></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <p id="footer-bar"><?php echo sprintf(__('Printed at %s, by %s using MnumiPanel %s'), format_date(time(), 'F'), $sf_user->__toString(), ProjectConfiguration::getRepositoryVersion()); ?></p>

</div>
