<form id="new_field_category_form" method = "post" class="form" action="<?php echo $form->isNew() ? url_for('settingsPrintsizeAdd') : url_for('settingsPrintsizeEdit', $form->getObject()); ?>">
    <?php echo $form->renderHiddenFields() ?>

    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'settingsPrintsize')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <?php echo include_component('settings', 'navigation', array('id' => null)); ?>
        </section>

        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <h1>
                        <?php echo $form->isNew() ? __('Create print size') : __('Print size'); ?>
                    </h1>
                    <?php echo $form->renderHiddenFields() ?>
                    <?php include_partial('dashboard/formErrors', array('form' => $form)); ?>
          
                    <table class = "field_item_table">
                        <tr>
                            <td>
                                <?php echo $form['width']->renderLabel(); ?>
                            </td>
                            <td></td>
                            <td>
                                <?php echo $form['height']->renderLabel(); ?>
                            </td>
                            <td></td>
                        </tr>
                        <tr>                    
                            <td>
                                <?php echo $form['width']->render(); ?>
                            </td>
                            <td>
                                x
                            </td>
                            <td>
                                <?php echo $form['height']->render(); ?>
                            </td>
                            <td>
                                <?php echo sfConfig::get('app_size_metric', 'mm'); ?>
                            </td>    
                        </tr>
                    </table>
                </div>
            </div>
        </section>
    </article>
</form>