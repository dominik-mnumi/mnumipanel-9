<?php echo include_partial('dashboard/message'); ?>

<article class="container_12">
    <section class="grid_4">
        <?php echo include_component('settings', 'navigation', array('id' => null)); ?>
    </section>
    <section class="grid_8">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('Group'); ?>: <?php echo __('Print size'); ?>
                    <?php echo link_to(image_tag(
                                          'icons/fugue/plus-circle-blue.png', 
                                          array('alt' => __('Add pricelist'), 
                                              'width' => 16, 
                                              'height' => 16)).' '.__('add'),
                                       'settingsPrintsizeAdd'); ?>  
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>
