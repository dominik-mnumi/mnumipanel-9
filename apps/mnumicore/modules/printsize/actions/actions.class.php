<?php

/**
 * printer actions.
 *
 * @package    mnumicore
 * @subpackage printsize
 * @author     Adam Marchewicz <adam.marchewicz@itme.eu>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class printsizeActions extends tablesActions
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        
      $this->displayTableFields = array('Width' => array(),
          'Height' => array());
      $this->displayGridFields = array(
          'Width'  => array('destination' => 'name'),
      );
      $this->sortableFields = array('width', 'height');
      $this->modelObject = 'Printsize';
      
      $this->tableOptions = $this->executeTable();
      
      if($request->isXmlHttpRequest())
      {
          return $this->renderPartial('global/tableHtml', $this->tableOptions);
      }
    }
    
    public function executeCreate(sfWebRequest $request)
    {
        $form = new PrintsizeForm();
        $this->proceedForm($form, $request);
        $this->setTemplate('edit');
        
        // view objects
        $this->form = $form;
    }
    
    public function executeEdit(sfWebRequest $request)
    {
        $form = new PrintsizeForm($this->getRoute()->getObject());
        $this->proceedForm($form, $request);
        
        // view objects
        $this->form = $form;
    }
    
    public function executeDelete(sfWebRequest $request)
    {
        //gets printsize object
        $printsizeObj = $this->getRoute()->getObject(); 
        if($printsizeObj->canDelete())
        {
            $printsizeObj->delete();
            
            $this->getUser()->setFlash('info_data', array(
            'message' => 'Deleted successfully.',
            'messageType' => 'msg_success',
            ));
        }
        else
        {
            $this->getUser()->setFlash('info_data', array(
            'message' => 'Deleted unsuccessfully.',
            'messageType' => 'msg_error',
            ));
        }

        $this->redirect('settingsPrintsize');
    }

    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Printsize';
        parent::executeDeleteMany($request);
    }
    
    /**
     * Processes form and redirects if valid.
     * 
     * @param sfForm $form
     * @param sfWebRequest $request
     * @return \sfForm 
     */
    private function proceedForm(sfForm $form, sfWebRequest $request)
    {
        if(!$request->isMethod('post'))
        {
            return;
        }

        $form->bind($request->getParameter($form->getName()));

        if($form->isValid())
        {
            $form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            $this->redirect('settingsPrintsizeEdit', $form->getObject());
        }
    }

}
