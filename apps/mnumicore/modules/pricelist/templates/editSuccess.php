<form id="new_field_category_form" method = "post" class="form" 
      action="<?php echo $form->isNew() ? url_for('settingsPricelistAdd') : url_for('settingsPricelistEdit',$form->getObject()); ?>">
    <?php echo $form->renderHiddenFields() ?>

    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'settingsPricelist')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <?php echo include_component('settings', 'navigation', array('id' => null)); ?>
        </section>

        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo $form->isNew() ? __('Create pricelist') : $form['name']->getValue() ?></h1>
                    <?php echo $form; ?>
                </div>
            </div>
        </section>
    </article>
</form>