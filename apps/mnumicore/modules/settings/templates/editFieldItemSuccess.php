<?php $visibleFields = $sf_data->getRaw('visibleFields'); ?>

<form id = "new_field_category_form" method = "post" action = "<?php echo $form->isNew() ? url_for('settingsCreateFieldItem', $fieldset) : url_for('settingsFieldItemEdit',$form->getObject())?>">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'settings', 
                'saveAndAdd' => true)); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <?php echo include_component('settings', 'navigation', array('id' => isset($id) ? $id : null)); ?>
        </section>

        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php echo $form['_csrf_token']->render(); ?>
                    <?php echo $form['field_id']->render(); ?>

                    <?php $form_number = 0; ?>
                    <h1>
                        <?php echo __($typeLabel); ?>:
                        <?php echo $form->isNew() ? __('Create setting') : stripText($form['name']->getValue(), 50); ?>
                    </h1>

                    <?php if($form->hasErrors()): ?>
                    <?php foreach($form->getFormattedErrors() as $error): ?>
                    <ul class="message error no-margin">
                        <li><?php echo __($error); ?></li>
                    </ul>
                    <?php endforeach ?>
                    <?php endif; ?>
                    <table class="field_item_table">
                        <tr>
                            <td><?php echo $form['name']->renderLabel(); ?>: </td>
                            <td>
                                <?php echo $form['name']->render(array('class' => 'full-width')); ?>

                                <?php if(in_array('label', $visibleFields)): ?>
                                <p class="description"><?php echo __('Available only for office'); ?></p>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php if(in_array('label', $visibleFields)): ?>
                        <tr>
                            <td><?php echo $form['label']->renderLabel(); ?>: </td>
                            <td>
                                <?php echo $form['label']->render(array('class' => 'full-width')); ?>
                                <p class="description"><?php echo __('Visible in shop'); ?></p>
                            </td>
                        </tr>
                        <?php endif; ?>

                        <?php if(in_array('measure_unit', $visibleFields)): ?>
                            <tr>
                                <td><?php echo $form['measure_unit_id']->renderLabel() ?>: </td>
                                <td><?php echo $form['measure_unit_id']->render() ?><br/></td>
                            </tr>
                        <?php endif; ?>

                        <?php if(in_array('field_item_material', $visibleFields)): ?>
                        <?php $fieldItemMaterialForm = $form['material']; ?> 
                        <?php echo $fieldItemMaterialForm->renderHiddenFields(); ?>        
                        <tr>
                            <td><?php echo $fieldItemMaterialForm['printsize_id']->renderLabel(); ?>: </td>
                            <td>
                                <?php echo $fieldItemMaterialForm['printsize_id']->render(); ?> <?php echo sfConfig::get('app_size_metric', 'mm'); ?><br/>
                                <p class="description"><?php echo __('Print size is used for measure unit \'Per page\' only'); ?></p>
                            </td>
                        </tr>
                        <?php endif; ?>

                        <?php if(in_array('field_item_size', $visibleFields)): ?>
                        <?php $fieldItemSizeForm = $form['size']; ?>
                        <?php echo $fieldItemSizeForm->renderHiddenFields(); ?>
                        <tr>
                            <td><?php echo $fieldItemSizeForm['width']->renderLabel(); ?>: </td>
                            <td>
                                <?php echo $fieldItemSizeForm['width']->render() ?> x <?php echo $fieldItemSizeForm['height']->render(); ?>
                                <?php echo sfConfig::get('app_size_metric', 'mm'); ?>
                                <p class="description"><?php echo __('gross value (inc. bleeds)'); ?></p>
                            </td>
                        </tr>
                        <?php endif; ?>
                    </table>
                    
                    <br/>

                    <?php if(array_key_exists('pricelist', $visibleFields)): ?>
                    <div id="pricelist">
                        <div id="pricelist-tab">
                            <ul class="tabs js-tabs same-height">
                                <?php foreach($pricelists as $id => $name): ?>
                                <?php if($id == $defaultPricelist->getId()): ?>
                                <li><a href="#tab-pricelist-<?php echo $id; ?>"><?php echo __('Default pricelist'); ?></a></li>
                                <?php else: ?>
                                <li<?php if(!in_array($id, $sf_data->getRaw('enabledPricelistIds'))) echo ' style="display:none;"'; ?>><a href="#tab-pricelist-<?php echo $id; ?>"><?php echo $name; ?></a></li>
                                <?php endif; ?>
                                <?php endforeach; ?>

                                <li class="with-margin">
                                    <a title="Dodaj zakładkę" href="#" id="add_tab"><img width="16" height="16" src="/images/icons/add.png"></a>
                                </li>
                            </ul>
                            <div class="tabs-content form" id="pricelist-content">
                                <?php foreach($pricelists as $id => $name): ?>
                                <div id="tab-pricelist-<?php echo $id; ?>"<?php if(!in_array($id, $sf_data->getRaw('enabledPricelistIds')) && ($id != $defaultPricelist->getId())) echo ' style="display:none;"'; ?>>
                                    <div class="enable">
                                        <?php if($id != $defaultPricelist->getId()): ?>
                                        <div style="float: right;">
                                            <a class="big-button red removepricelist" type="button" href="#tab-pricelist-<?php echo $id; ?>"><?php echo __('Remove pricelist'); ?></a>
                                            <input type="hidden" name="pricelist[<?php echo $id; ?>][delete]" class="tab-delete" value="0" />
                                        </div>
                                        <?php endif; ?>

                                        <?php include_partial('field_item_price_'.$type, array(
                                            'id' => $id,
                                            'item' => $item[$id],
                                            'itemPrice' => $itemPrice[$id]
                                        )); ?>
                                    </div>  
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>	
                    </div>
                    <?php endif; ?>

                    <br/>

                    <?php if(in_array('availability', $visibleFields) &&
                        in_array('availability_alert', $visibleFields) &&
                        in_array('availability_unit_id', $visibleFields) &&
                        in_array('stock_number', $visibleFields)): ?>
                        <?php include_partial('settings/depository/fieldset', array(
                            'availability' => $form['availability'],
                            'availability_alert' => $form['availability_alert'],
                            'availability_unit_id' => $form['availability_unit_id'],
                            'stock_number' => $form['stock_number'],
                        )); ?>
                    <?php endif; ?>

                    <div id="dialog" style="display: none;">
                        <div style="text-align: center;">
                            <h3><?php echo __('Please select pricelist'); ?></h3><br/>
                            <select id="selectPricelist">
                                <?php foreach($pricelists as $id => $name): ?>
                                <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
</form>

<!-- create form -->
<div id="new_field_category" class = "new_field_category" style = "display: none;">
    <?php include_component('settings', 'createFieldset') ?>
</div>

<!-- sets js slot -->
<?php extendSlot('actionJavascript') ?>
<script type="text/javascript">    
var i = 1;
jQuery(document).ready(function($)
{
    $('#add_tab').click(function() 
    {
        var pricelist = $('#pricelist-tab .tabs').children(':has(:hidden)');
        $('select#selectPricelist').html('<option></option>');
        $(pricelist).each(function() {
            var value = $(this).children().attr('href').substr(1);
            $('select#selectPricelist').append('<option value="' + value + '">' + $(this).text() + '</option>');
        });

        if(($(pricelist).length) > 0)
        {
            $.modal({
                content: $("#dialog"),
                title: '<?php echo __('Please select pricelist'); ?>',
                maxWidth: 300,
                resizable: false,
                buttons: {
                    '<?php echo __('Close'); ?>': function(win) {
                        win.closeModal();
                    }
                }
            });
        }
        else
        {
            alertModal('<?php echo __('Pricelists error') ?>','<h3><?php echo __('No more pricelists available.') ?></h3>', 500);
        }

    });

    $('select#selectPricelist').bind('change', function() {
        if($(this).val() != '')
        {
            var id = $(this).children(':selected').val();
            var tab = $('a[href="#' + id + '"]').parent();

            tab.show();
            $('#modal').closeModal();
        }
    });

    $('tbody tr td input[class^=price_range_price]').bind('change', function() {
        if($(this).parent().parent().next().html())
        {
            $(this).parent().parent().next().show();
        }
        });

    $('tbody tr td .delete_price_range').bind('click', function() {
            $(this).parent().parent().find('input').val('');
            $(this).parent().parent().hide();
    });

    <?php if($form->getOption('switchPrices')): ?>
    function selectPrices()
    {
        if(1 == $('#field_item_measure_unit_id option:selected').val() 
            || 'Per sheet' == $('#field_item_measure_unit_id option:selected').html()
            || 'Arkusz' == $('#field_item_measure_unit_id option:selected').html())
        { 
            $('.price_square_metre').parent().hide();
            $('.price_page').parent().show();
            $('.price_page').each(function () {
                if($(this).parent().next().find('input').val()){
                    $(this).val($(this).parent().next().find('input').val());
                }
            });
            $('.price_square_metre').val('');

            $('.header_price_square_metre').hide();
            $('.header_price_page').show();

        }
        else
        {
            $('.price_page').parent().hide();
            $('.price_square_metre').parent().show();
            $('.price_square_metre').each(function () {
                if($(this).parent().prev().find('input').val()){
                    $(this).val($(this).parent().prev().find('input').val());
                }
            });
            $('.price_page').val('');

            $('.header_price_page').hide();
            $('.header_price_square_metre').show();
        }
    }

    $('#field_item_measure_unit_id').change(function() {
        selectPrices();
    });

    selectPrices();

    <?php endif ?>

    $('.removepricelist').bind('click', function(event)
    {     
        event.preventDefault();

        var tab = $(this).parent().parent().parent();
        var id = tab.attr('id');
        $('#pricelist-tab .tabs a[href=#' + id + ']').parent().hide().remove('current');
        $('#pricelist-tab .tabs li:first a:first').trigger('click');

        tab.find(".tab-delete").val(1);
    });

    // select first tab
    window.location.hash = "#tab-pricelist-<?php echo $defaultPricelist->getId(); ?>";
});
</script>
<?php end_slot() ?>
