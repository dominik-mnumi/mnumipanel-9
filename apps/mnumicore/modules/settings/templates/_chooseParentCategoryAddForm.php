<script type="text/javascript" src="/js/settings.functions.js"></script>

<div class="block-border">
    <div class="block-content">
        <h1><?php echo __('Choose parent category'); ?></h1>
        
        <div class="content-columns left30">      
            <div class="content-left" style="width: 100%">
                <div class="with-head">             
                    <div class="dark-grey-gradient with-padding" style="height:200px; overflow: auto">
                        <?php foreach($treeColl as $rec) : ?>
                        <?php if($rec->getId() == $rec->getRootId()): ?>
                        <ul class="arbo with-title" style = "margin: 0px;">
                            <li class="">
                                <a href="#" class="title-computer toggle parent" val="<?php echo $rec->getId(); ?>"><span><?php echo __($rec); ?></span></a>
                                <ul>
                                    <!-- main categories -->                
                                    <?php foreach($rec->__children as $rec2): ?>
                                    <?php echo include_partial('settings/chooseParentCategoryNavigation', array('rec' => $rec2)); ?>
                                    <?php endforeach; ?>                    
                                </ul>
                            </li>
                        </ul>
                        <?php endif; ?>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>         
        </div>        
    </div>
</div>