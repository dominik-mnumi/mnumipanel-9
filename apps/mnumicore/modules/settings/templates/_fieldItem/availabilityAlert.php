<?php if($row->getAvailabilityAlert() === null): ?>
    <?php echo __('Not applicable'); ?>
<?php else: ?>
    <?php echo $row->getAvailabilityAlert(); ?>
<?php endif; ?>