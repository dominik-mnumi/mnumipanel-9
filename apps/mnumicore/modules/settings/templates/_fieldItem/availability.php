<?php if($row->getAvailability() === null): ?>
    <?php echo __('Not applicable'); ?>
<?php else: ?>
    <?php echo $row->getAvailability(); ?>
<?php endif; ?>