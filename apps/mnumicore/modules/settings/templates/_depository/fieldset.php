<div id="depository">
    <div id="depository-tab">
        <ul class="tabs js-tabs same-height">
            <li>
                <a href="#tab-depository"><?php echo __('Depository'); ?></a>
            </li>
        </ul>
        <div class="tabs-content form" id="depository-content">
            <div id="tab-depository">
                <table class="field_item_table">
                    <tr>
                        <td><?php echo $stock_number->renderLabel(); ?></td>
                        <td><?php echo $stock_number->render(); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $availability_unit_id->renderLabel(); ?></td>
                        <td><?php echo $availability_unit_id->render(); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $availability->renderLabel(); ?></td>
                        <td>
                            <?php echo $availability->render(); ?>
                            <p class="description">
                                <?php echo __('Actual stock.'); ?> 
                                <?php echo __('Empty field means, that function is not used.'); ?>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $availability_alert->renderLabel(); ?></td>
                        <td>
                            <?php echo $availability_alert->render(); ?>
                            <p class="description">
                                <?php echo __('When the stock is less than this value, display a warning.'); ?> 
                                <?php echo __('Empty field means, that function is not used.'); ?>
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div> 
