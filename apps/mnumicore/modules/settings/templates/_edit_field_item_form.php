<?php use_helper('stripText', 'openModal') ?>

<?php echo $form['_csrf_token']->render(); ?>
<?php echo $form['field_id']->render(); ?>

<?php $form_number = 0; ?>
<h1><?php echo $form->isNew() ? __('Create settings') : stripText($form['name']->getValue(), 50) ?></h1>

<?php if($form->hasErrors()): ?>
<?php foreach($form->getFormattedErrors() as $error): ?>
<ul class="message error no-margin">
    <li><?php echo __($error); ?></li>
</ul>
<?php endforeach ?>
<?php endif; ?>

<table class = "field_item_table">
    <tr>
        <td><?php echo $form['name']->renderLabel(); ?>: </td>
        <td><?php echo $form['name']->render(); ?><br/></td>
    </tr>
    <?php if(isset($form['label'])): ?>
    <tr>
        <td><?php echo $form['label']->renderLabel(); ?>: </td>
        <td><?php echo $form['label']->render(); ?><br/></td>
    </tr>
    <?php endif; ?>
    <?php if(isset($form['cost'])): ?>
    <tr>
        <td><?php echo $form['cost']->renderLabel(); ?>: </td>
        <td><?php echo $form['cost']->render(); ?><br/></td>
    </tr>
    <?php endif ?>
    <?php if(isset($form['measure_unit_id'])): ?>
    <tr>
        <td><?php echo $form['measure_unit_id']->renderLabel() ?>: </td>
        <td><?php echo $form['measure_unit_id']->render() ?><br/></td>
    </tr>
    <?php endif ?>

    <?php if(isset($form['newFieldPrintsize']) || isset($form['FieldPrintsize'])): ?>
    <?php $printsizeForms = (($form->isNew()) ? array($form['newFieldPrintsize']): $form['FieldPrintsize']) ?>
    <?php foreach($printsizeForms as $printsizeForm) :  ?>
    <?php echo $printsizeForm->renderHiddenFields() ?>
    <tr>
        <td><?php echo $printsizeForm['width']->renderLabel() ?>: </td>
        <td><?php echo $printsizeForm['width']->render() ?> x <?php echo $printsizeForm['height']->render() ?><br/></td>
    </tr>
    <tr>
        <td><?php echo $printsizeForm['printer_id']->renderLabel() ?>: </td>
        <td><?php echo $printsizeForm['printer_id']->render() ?><br/></td>
    </tr>
    <?php endforeach ?>
    <?php endif; ?>
</table>
<br/>
