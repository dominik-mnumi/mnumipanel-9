<div class="price grid_6">
    <fieldset>
        <legend><?php echo __('Price net'); ?></legend>

        <div>
            <label for="pricelist[<?php echo $id; ?>][minimal_price]" class="float-left grid_6">
                <?php echo __('Minimal price'); ?>
                <p class="description">
                    <?php echo __('Minimal price customer has to pay for this setting'); ?>.
                </p>
            </label>
            <input name="pricelist[<?php echo $id; ?>][minimal_price]" class="float-right grid_5" type="text" value="<?php echo $item['min']; ?>" />
        </div>
        <div class="clear"></div>

        <div>
            <label for="pricelist[<?php echo $id; ?>][maximal_price]" class="float-left grid_6">
                <?php echo __('Maximal price'); ?>
                <p class="description">
                    <?php echo __('Maximal price customer will pay for this setting'); ?>.
                </p>
            </label>
            <input name="pricelist[<?php echo $id; ?>][maximal_price]" class="float-right grid_5" type="text" value="<?php echo $item['max']; ?>" />
        </div>
        <div class="clear"></div>

        <div>
            <label for="pricelist[<?php echo $id; ?>][plus_price]" class="float-left grid_6">
                <?php echo __('Plus price'); ?>
                <p class="description">
                    <?php echo __('Additional price for this setting'); ?>.
                </p>
            </label>
            <input name="pricelist[<?php echo $id; ?>][plus_price]" class="float-right grid_5" type="text" value="<?php echo $item['plus']; ?>" />
        </div>
        <div class="clear"></div>

        <p class="description">
            <?php echo __('If the sum exceeds the maximum price, the amount will be reduced to the maximum value.'); ?>
        </p>
    </fieldset>
</div>

<div class="cost grid_6">
    <fieldset>
        <legend><?php echo __('Cost'); ?></legend>

        <div>
            <label for="pricelist[<?php echo $id; ?>][cost]" class="float-left grid_6">
                <?php echo __('Base cost'); ?>
                <p class="description"><?php echo __('Base cost of this setting'); ?></p>
            </label>
            <input name="pricelist[<?php echo $id; ?>][cost]" class="float-right grid_5" type="text" value="<?php echo $item['cost']; ?>" /></td>
        </div>
        <div class="clear"></div>
    </fieldset>
</div>
<div class="clear"></div>
<div id="priceAdder" class="price price-tables-add">
    <hr />
    <?php echo __('Add'); ?>:
    <select class="priceType">
        <option value="">- - -</option>
        <option value="price_item">
            <?php echo __('Per item'); ?>
        </option>
        <option value="price_page">
            <?php echo __('Per sheet'); ?>
        </option>
        <option value="price_copy">
            <?php echo __('Per copy'); ?>
        </option>
        <option value="price_linear_metre">
            <?php echo __('Per metre'); ?>
        </option>
        <option value="price_square_metre">
            <?php echo __('Per square metre'); ?>
        </option>
    </select> 
    <input type="hidden" class="pricelistId" value="<?php echo $id; ?>" />
    <hr />
</div>

<div class="price-tables"></div>

<script type="text/template" id="price-content">
    <div class="price-<%= options.name %> price-table price">
        <h3>
            <%= options.label %>
            <a class="deletePriceTable" title="<?php echo __('Delete'); ?>">
               <img src="/images/icons/fugue/cross-small.png" width="16" height="16" />
            </a>
        </h3>
        <table class="table">
            <thead>
                <tr>
                    <th>
                        <% if(options.name == 'price_page') { %>
                        <?php echo __('Sheet quantity'); ?>
                        <% } else if(options.name == 'price_copy') { %>
                        <?php echo __('Quantity'); ?>
                        <% } else if(options.name == 'price_item') { %>
                        <?php echo __('Items quantity'); ?>
                        <% } else if(options.name == 'price_linear_metre') { %>
                        <?php echo __('Linear meters quantity'); ?>
                        <% } else if(options.name == 'price_square_metre') { %>
                        <?php echo __('Square meters quantity'); ?>
                        <% } %>  
                    </th>
                    <th><?php echo __('Price'); ?></th>
                    <th><?php echo __('Cost'); ?></th>
                    <th><?php echo __('Action'); ?></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <!-- /* backbone adding new row */ -->
            </tfoot>
        </table>
    </div>
</script>

<script type="text/template" id="price-item">
   <td><input type="text" size="6" class="field-quantity" name="pricelist[<%= this.options.pricelistId %>][type][<%= this.options.name %>][<%= this.options.item_no %>][quantity]" value="<%= parseFloat(quantity) %>" <%= (this.options.item_no == 0) ? 'readonly="readonly"' : '' %>/></td>
   <td><input type="text" size="6" class="field-price" name="pricelist[<%= this.options.pricelistId %>][type][<%= this.options.name %>][<%= this.options.item_no %>][price]" value="<%= price %>" /></td>
    <td><input type="text" size="6" class="field-cost" name="pricelist[<%= this.options.pricelistId %>][type][<%= this.options.name %>][<%= this.options.item_no %>][cost]" value="<%= cost %>" /></td>
   <td>
   <% if(this.options.item_no != 0 && this.options.item_no != this.options.totalRowsNo - 1) { %>
       <a class="delete" title="<?php echo __('Delete'); ?>">
           <img src="/images/icons/fugue/cross-circle.png" width="16" height="16">
       </a>
   <% } %>
   </td>
</script>

<!-- gets field item price -->
<script type="text/javascript">
jQuery(document).ready(function($)
{   
    priceRangeViewColl['<?php echo $id; ?>'] = new Array();        
    priceRangeViewValueColl['<?php echo $id; ?>'] = new Array();

    <?php $coll1 = json_encode(sfOutputEscaperGetterDecorator::unescape($itemPrice[0])); ?>
    <?php $coll2 = json_encode(sfOutputEscaperGetterDecorator::unescape($itemPrice[1])); ?>
    <?php $coll3 = json_encode(sfOutputEscaperGetterDecorator::unescape($itemPrice[2])); ?>
    <?php $coll4 = json_encode(sfOutputEscaperGetterDecorator::unescape($itemPrice[3])); ?>
    <?php $coll5 = json_encode(sfOutputEscaperGetterDecorator::unescape($itemPrice[4])); ?>


    priceRangeViewValueColl['<?php echo $id; ?>']['<?php echo FieldItemPriceTypeTable::$price_page; ?>'] = <?php echo $coll1; ?>;
    priceRangeViewValueColl['<?php echo $id; ?>']['<?php echo FieldItemPriceTypeTable::$price_copy; ?>'] = <?php echo $coll2; ?>;
    priceRangeViewValueColl['<?php echo $id; ?>']['<?php echo FieldItemPriceTypeTable::$price_item; ?>'] = <?php echo $coll3; ?>;
    priceRangeViewValueColl['<?php echo $id; ?>']['<?php echo FieldItemPriceTypeTable::$price_linear_metre; ?>'] = <?php echo $coll4; ?>;
    priceRangeViewValueColl['<?php echo $id; ?>']['<?php echo FieldItemPriceTypeTable::$price_square_metre; ?>'] = <?php echo $coll5; ?>;

    <?php if(json_decode($coll1)): ?>
    addPriceRangeView('<?php echo FieldItemPriceTypeTable::$price_page; ?>', '<?php echo __('Per sheet'); ?>', '<?php echo $id; ?>');
    <?php endif; ?>
    <?php if(json_decode($coll2)): ?>
    addPriceRangeView('<?php echo FieldItemPriceTypeTable::$price_copy; ?>', '<?php echo __('Per copy'); ?>', '<?php echo $id; ?>');
    <?php endif; ?>
    <?php if(json_decode($coll3)): ?>
    addPriceRangeView('<?php echo FieldItemPriceTypeTable::$price_item; ?>', '<?php echo __('Per item'); ?>', '<?php echo $id; ?>');
    <?php endif; ?>
    <?php if(json_decode($coll4)): ?>
    addPriceRangeView('<?php echo FieldItemPriceTypeTable::$price_linear_metre; ?>', '<?php echo __('Per linear metre'); ?>', '<?php echo $id; ?>');
    <?php endif; ?>
    <?php if(json_decode($coll5)): ?>
    addPriceRangeView('<?php echo FieldItemPriceTypeTable::$price_square_metre; ?>', '<?php echo __('Per square metre'); ?>', '<?php echo $id; ?>');
    <?php endif; ?>



});
</script>
