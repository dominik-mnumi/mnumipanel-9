<?php use_helper('openModal'); ?>

<div class="block-border">
    <div class="block-content">
        <h1><?php echo __('Settings groups'); ?>
        <a href="#" id ="add_field_category"><img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt = "Add field"> <?php echo __('add'); ?></a>
        </h1>

        <ul class="tree-list with-bg">
            <!-- main categories -->
            <?php foreach($treeColl as $node): ?>
                <?php include_partial('dashboard/navigationItem', array(
                    'node' => $node,
                    'cat_id' => $id,
                    'url_prefix' => 'settingsField'
                )); ?>
            <?php endforeach; ?>
            <li class="closed">
                <span><?php echo link_to(__('Price list'), '@settingsPricelist'); ?></span>
            </li>
            <li class="closed">
                <span><?php echo link_to(__('Print size'), 'settingsPrintsize'); ?></span>
            </li>
        </ul>
    </div>
</div>
<?php use_helper('changeLanguage');getChangeLanguage($languages, $default); ?>
<?php include_partial('settings/create_javascript') ?>


