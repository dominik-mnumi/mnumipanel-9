<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_12">
        <form class="block-content form" method="post">
            <h1><?php echo __('License'); ?></h1>

            <h2><?php echo __('License information'); ?></h2>

            <p><?php echo __('This page shows your current licensing information. You can use the Update License form to update the license Mnumi is running with.'); ?></p>

            <?php if($correctLicense): ?>
            <ul class="simple-list with-icon icon-info">
                <li><?php echo __('Organization'); ?>: <strong><?php echo $organisation; ?></strong></li>
                <li><?php echo __('Date Purchased'); ?>: <strong><?php echo $purchases; ?></strong></li>
                <li>
                    <?php echo __('License Type'); ?>: <strong><?php echo $product; ?>: <?php echo __($licenseType); ?></strong>
                    <?php if(!$hasExpired): ?>
                    (<?php echo __('expires on'); ?> <strong class="<?php echo $expiresClass; ?>"><?php echo $expires; ?></strong>)
                    <?php else: ?>
                    (<strong class="<?php echo $expiresClass; ?>"><?php echo __('expired'); ?>: <?php echo $expires; ?></strong>)
                    <?php endif; ?>
                </li>
                <li>
                    IP: <strong><?php echo $ip; ?></strong> 
                    <?php if($serverIp != $ip): ?> 
                    (<?php echo __('your server IP is different') ?>: <strong class="wrongIp"><?php echo $serverIp; ?></strong>)
                    <?php endif; ?>
                </li>
            </ul>
            <?php else: ?>
            <ul class="simple-list with-icon icon-info">
                <li><strong><?php echo __('Your license key is invalid or not set.'); ?></strong></li>
            </ul>
            <?php endif; ?>

            <h2><?php echo __('Update License'); ?></h2>
            <p><?php echo __('Copy and paste the license key below. You can access your license key on <a href="%website%" target="_blank">My Account</a>.', array('%website%' => 'http://my.mnumi.com/')); ?></p>

            <fieldset class="required">
                <?php echo $form; ?>
            </fieldset>
            <p>
                <button type="submit" id="filterBtn"><?php echo __('Add'); ?></button>
            </p>
        </form>
    </section>
</article>
