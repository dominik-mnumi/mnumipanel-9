<form action="" method="post" id="complex_form">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'settings', 
                'saveAndAdd' => true)); ?>
    <article class="container_12">
        <section class="grid_4">
            <?php echo include_component('settings', 'navigation', array('id' => isset($id) ? $id : null)); ?>
        </section>
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content form">
                    <h1><?php echo __('Edit field'); ?></h1>

                    <fieldset class="grey-bg">
                        <?php echo $fieldsetEditForm->renderHiddenFields(); ?>
                        <p class="required">
                            <?php echo $fieldsetEditForm['label']->renderLabel(); ?>
                            <?php echo $fieldsetEditForm['label']->render(); ?>
                            <?php echo $fieldsetEditForm['label']->renderError(); ?>
                        </p>
                    </fieldset>
                </div>
            </div>
        </section>
    </article>
</form>

<!-- create form -->
<div id="new_field_category" class = "new_field_category" style = "display: none;">
    <?php include_component('settings', 'createFieldset') ?>
</div>