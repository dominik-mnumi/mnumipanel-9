<script type="text/javascript">
jQuery(document).ready(function($)
{
    $('#add_field_category').click(function() 
    {
        $.modal({
        content: $("#new_field_category"),
        title: '<?php echo __('Create settingss group') ?>',
        maxWidth: 500,
        buttons: {
            '<?php echo __('Add field') ?>': function(win) { 
                var data = $('.modal-content #new_field_category_form').serialize();
        var uri =  $('.modal-content #new_field_category_form').attr('action');
        var method =  $('.modal-content #new_field_category_form').attr('method');

        $.ajax(
        {
            type: method,
            url: uri,
            data: data,
            dataType: 'html',
            success: function(value)
            {
                if(value.length < 3)
                {
                    win.closeModal();
                    window.location.href = '<?php echo url_for('@settings') ?>';
                }
                else
                {
                    win.setModalContent(value);
                }
            }
            }); 
        },
        '<?php echo __('Close') ?>': function(win) { win.closeModal(); }
        }
        });
    });

    /** Used for Others **/
    $('#priceAdder select.priceType').change(function()
    {
        var select = $(this).parent().children('select.priceType');
        var type = select.val();

        if(select.val() == '')
        {
            return false;
        }
        var pricelistId = $(this).parent().children('input.pricelistId').val();
        var option = $(this).parent().children('select.priceType').first().children(':selected');

        // for IE hidden option is clickable
        if(option.css("display") == 'none')
        {
            $.modal(
                {
                    content: '<br /><h3><?php echo __('This option has been already added once'); ?></h3>',
                    title: '<?php echo __('Information'); ?>',
                    buttons:
                    {
                        'Ok': function(win)
                        {
                            win.closeModal();
                        }
                    }
                });

            // selects first option
            $(this).find('option:first').attr('selected', 'selected');
            return false;
        }

        addPriceRangeView(type, option.html(), pricelistId);

        select.val($(this).parent().children('select option:first').val()); // select first option

        if($(this).parent().children('select.priceType option').not('.hidden').length == 1)
        {
            $(this).parent().hide();
        }

        return false;
    });
});
</script>