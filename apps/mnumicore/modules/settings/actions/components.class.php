<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * settings actions.
 *
 * @package    mnumicore
 * @subpackage settings
 */
class settingsComponents extends sfComponents
{

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeNavigation(sfWebRequest $request)
    {
        // gets hierarchical category tree
        $this->treeColl = FieldsetTable::getInstance()
                ->getTreeQuery()
                ->execute(array(), Doctrine_Core::HYDRATE_RECORD_HIERARCHY);
        
        $this->languages = sfConfig::get('app_cultures_enabled');
        $this->default = MnumiI18n::getCulture();
    }
    
    public function executeCreateFieldset()
    {     
        // gets hierarchical category tree
        $this->treeColl = FieldsetTable::getInstance()
                ->getTreeQuery()
                ->execute(array(), Doctrine_Core::HYDRATE_RECORD_HIERARCHY);
        
        $this->fieldCategoryForm = new FieldsetForm();
    }

}
