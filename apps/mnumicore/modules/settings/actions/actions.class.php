<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * settings actions.
 *
 * @package    mnumicore
 * @subpackage settings
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class settingsActions extends complexTablesActions
{

    /**
     * Executes index action. Shows field categories and fieldsets (if category was selected)
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $lastFieldset = $this->getUser()->getAttribute('last_fieldset', false, 'settings');

        if($lastFieldset)
        {
            return $this->redirect('@settingsFieldItem?id='. $lastFieldset);
        }

        // If there was no $lastFieldset viewed, display depository view
        $this->fieldset = FieldsetTable::getInstance()->getOtherFieldset();

        parent::settingsList($request);

        $this->actions = array();
        $this->tableOptions = $this->executeTable();

        if ($request->isXmlHttpRequest()) {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }

    /**
     * Executes index action
     *
     * @param sfWebRequest $request A request object
     */
    public function executeSystemStatus(sfWebRequest $request)
    {
        $reports = sfConfig::get(Report::$config, array());

        // create view objects
        $this->reportArray = array();

        foreach(array_keys($reports) as $report)
        {
            if(!$report::visible()) continue;

            $rec = new $report();
            $this->reportArray[$rec->getStatus()][$report] = $rec;
        }

        $this->reportSequence = array('red', 'yellow', 'green');
    }

    public function executeShow(sfWebRequest $request)
    {
        /** @var Fieldset $fieldset */
        $fieldset = $this->getRoute()->getObject();

        $this->getUser()->setAttribute('last_fieldset', $fieldset->getId(), 'settings');

        $this->customQuery = FieldsetTable::getFieldItemsByRootIdsQuery(
            (array) $fieldset->getFieldsetIds()
        );
        $this->customRouting = 'settingsFieldItem';
        $this->customRoutingParameters = array('id' => $fieldset->getId());
        $this->displayTableFields = array(
            'Name' => array('clickable' => true)
        );
        $this->displayGridFields = array(
            'Name'  => array('destination' => 'name')
        );

        $this->modelObject = 'FieldItem';
        $this->fieldset = $fieldset;

        if($fieldset->getRoot()->getName() == 'MATERIAL' || $fieldset->getRoot()->getName() == 'PRINT')
        {
            $this->displayTableFields = array(
                'Name' => array('clickable' => true),
                'Label' => array(),
                'Cost' => array()
            );
            $this->sortableFields = array('name', 'label', 'cost');
        }
        // OTHER fieldset
        elseif($fieldset->getRoot()->getName() != 'SIZE')
        {
            // replace view with depository one using FieldItems from current Fieldset
            parent::settingsList($request);
        }
        else
        {
            array('Name' => array('clickable' => true));
        }

        if(in_array($fieldset->getRoot()->getName(), array('MATERIAL', 'PRINT', 'OTHER')))
        {
            $this->actions += array(
                'bulkUpdate' => array(
                    'manyRoute' => 'settingsFieldItemBulkUpdate',
                    'label' => 'Bulk update',
                ),
            );
        }

        $this->tableOptions = $this->executeTable();

        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }

    /**
     * Executes changeFieldsetsPerPage action. Enabling to change fieldsets per page.
     *
     * @param sfRequest $request A request object
     */
    public function executeChangeFieldsetsPerPage(sfWebRequest $request)
    {
        $perPageForm = new SettingsPerPageForm();
        $perPageForm->bind($request->getParameter('settingsPerPage'));
        if ($perPageForm->isValid())
        {
            $parameters = $request->getParameter('settingsPerPage');
            $this->getUser()->setAttribute('settingsPerPage', $parameters['options']);
            $this->redirect('settings/index?id=' . $parameters['categoryId']);
        }
        else
        {
            $this->forward404();
        }
    }

    /**
     * Executes deleteFieldCategory action. Delete field category if is empty.
     *
     * @param sfRequest $request A request object
     */
    public function executeDeleteField(sfWebRequest $request)
    {
        $fieldset = $this->getRoute()->getObject();

        if (!$fieldset->canDelete())
        {
            $this->forward404($fieldset->id);
        }

        $lastFieldset = $this->getUser()->getAttribute('last_fieldset', null, 'settings');

        if($lastFieldset = $fieldset->getId())
        {
            $this->getUser()->setAttribute('last_fieldset', null, 'settings');
        }

        $fieldset->getNode()->delete();

        $this->getUser()->setAttribute('last_fieldset', null, 'settings');
        
        $this->getUser()->setFlash('info_data', array(
            'message' => 'Deleted successfully.',
            'messageType' => 'msg_success',
        ));           
        
        $this->redirect('settings/index');
    }

    /**
     * Executes create action. Create new Field Category.
     *
     * @param sfRequest $request A request object
     */
    public function executeCreate(sfWebRequest $request)
    {
        $form = new FieldsetForm();
        
        $this->treeColl = FieldsetTable::getInstance()
                ->getTreeQuery()
                ->execute(array(), Doctrine_Core::HYDRATE_RECORD_HIERARCHY);

        if($request->isMethod('post'))
        {  
            $form->bind($request->getParameter($form->getName()));
            if($form->isValid())
            {
                $this->form_saved = true;
                $this->getUser()->setFlash('info_data', array(
                    'message' => 'Settings group created successfully.',
                    'messageType' => 'msg_success',
                ));
                $form->save();
                $this->redirect('@settingsNotificationEdit?id=' . $form->getObject()->id);
            }
        }

        $this->fieldCategoryForm = $form;
    }

    /**
     * Executes createFieldItem action. Create new Field Item.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeCreateFieldItem(sfWebRequest $request)
    {
        $field = $this->getRoute()->getObject();

        $this->fieldItemObj = null; // used in editing only

            // create
        $this->fieldset = $field;
        $form_name = SettingsForm::getFieldItemFormClassName($field);

        $printsizeId = $this->getUser()->getFlash('printsize_id');

        $this->form = new $form_name(null,
            ($printsizeId) ? array('printsize_id' => $printsizeId) : array()
        );
        $this->form->setDefault('field_id', $field->getId());
        $this->enabledPricelistIds = array();

        $this->defaultPricelist = PricelistTable::getInstance()->getDefaultPricelist();
        $this->pricelists = PricelistTable::getInstance()->asArray();

        // get field item price
        $type = $this->fieldset->getType();

        $this->itemPrice = array();
        foreach($this->pricelists as $id => $price)
        {
            $this->itemPrice[$id] = ($type != 'other')
                ? array()
                : array(
                    array(),array(),array(),array(),array(),
                );
        }

        $this->prepareEditFieldItem($this->form);

        $this->setTemplate('editFieldItem');
    }

    /**
     * Executes editFieldItem action. Create new Field Item.
     *
     * @param sfRequest $request A request object
     */
    public function executeEditFieldItem(sfWebRequest $request)
    {
        $field = $this->getRoute()->getObject();

        $this->fieldItemObj = $field;

        // editing
        $this->fieldset = $field->getFieldset();
        $form_name = SettingsForm::getFieldItemFormClassName($this->fieldset);
        $this->form = new $form_name($field);
        $this->enabledPricelistIds = $field->getUsedPricelistIds();

        $this->defaultPricelist = PricelistTable::getInstance()->getDefaultPricelist();
        $this->pricelists = PricelistTable::getInstance()->asArray();

        // get field item price
        $type = $this->fieldset->getType();

        $this->item = array();
        $this->itemPrice = array();
        foreach($this->pricelists as $id => $price)
        {
            /** @var FieldItemPrice $fieldItemPriceObj */
            $fieldItemPriceObj = $field->getFieldItemPrice($id, true);

            if(!$fieldItemPriceObj)
            {
                $this->itemPrice[$id] = ($type != 'other')
                    ? array()
                    : array(
                        array(),array(),array(),array(),array(),
                    );

                $this->item[$id] = array(
                    'min' => null,
                    'max' => null,
                    'plus' => null,
                    'plus_double' => null,
                    'cost' => null,
                );
                continue;
            }

            switch ($type)
            {
                case 'material':
                    $this->itemPrice[$id] = $fieldItemPriceObj->getPriceColl(
                        $field->getMeasureUnit()->getFormalName()
                    );
                    break;
                case 'print':
                    $this->itemPrice[$id] = $fieldItemPriceObj->getPriceColl(
                        FieldItemPriceTypeTable::$price_simplex
                    );
                    break;
                case 'other':
                    $this->itemPrice[$id] = array(
                        $fieldItemPriceObj->getPriceColl(FieldItemPriceTypeTable::$price_page),
                        $fieldItemPriceObj->getPriceColl(FieldItemPriceTypeTable::$price_copy),
                        $fieldItemPriceObj->getPriceColl(FieldItemPriceTypeTable::$price_item),
                        $fieldItemPriceObj->getPriceColl(FieldItemPriceTypeTable::$price_linear_metre),
                        $fieldItemPriceObj->getPriceColl(FieldItemPriceTypeTable::$price_square_metre),

                    );
                    break;
                default:
                    throw new Exception('Unknown type: ' . $type);
            }

            $this->item[$id] = array(
                'min' => $fieldItemPriceObj->getMinimalPrice(),
                'max' => $fieldItemPriceObj->getMaximalPrice(),
                'plus' => $fieldItemPriceObj->getPlusPrice(),
                'plus_double' => $fieldItemPriceObj->getPlusPriceDouble(),
                'cost' => $fieldItemPriceObj->getCost(),
            );
        }



        // prepare form
        $this->prepareEditFieldItem($this->form);
    }
    
    /**
     * Prepare edit/create field item action
     * 
     * @param <sfForm> Form
     */
    protected function prepareEditFieldItem($form)
    {
        // list of available form fields
        $fields = array(
            'size' => array('field_item_size'),
            'material' => array('label', 'measure_unit', 'field_item_material', 'pricelist' => array('min')),
            'print' => array('label', 'measure_unit', 'pricelist' => array('min', 'max')),
            'other' => array(
                'pricelist' => array('min', 'max'),
                'availability', 'availability_alert', 'availability_unit_id', 'stock_number', //depository
            ),
        );

        $root = $this->fieldset->getRoot();
        $type = $root->getName();
        $this->typeLabel = $root->getLabel();
        $this->type = $this->fieldset->getType();
        $this->visibleFields = $fields[$this->type];

        // this section initialises some javascript objects which requires
        // specific kind of tags (not for all fieldsets).
        if(in_array($type, array('MATERIAL', 'PRINT', 'OTHER')))
        {
            $this->getResponse()->addJavascript('settings.js', 'last');
        }

    	$this->processForm($this->form);
    }

    private function processForm($form)
    {
        if($this->getRequest()->isMethod('post'))
        {
            $fieldItemArr = $this->getRequest()->getParameter('field_item', array());
            $pricelistArr = $this->getRequest()->getParameter('pricelist', array());

            if(!empty($pricelistArr))
            {
                $form->setOption('pricelistArr', $pricelistArr);
            }
            $form->bind($fieldItemArr);
            if($form->isValid())
            {
                $form->save();
                $this->clearProductCache($form->getObject());

                $this->getUser()->setFlash('info_data', array(
                    'message' => 'Saved successfully.',
                    'messageType' => 'msg_success',
                ));

                if($this->getRequest()->getParameter('saveAndAdd'))
                {
                    if(isset($fieldItemArr['FieldItemMaterial']))
                    {
                        foreach($fieldItemArr['FieldItemMaterial'] as $material)
                        {
                            if(isset($material['printsize_id']))
                            {
                                $this->getUser()->setFlash('printsize_id', $material['printsize_id']);
                            }
                        }
                    }
                    $this->redirect('@settingsCreateFieldItem?id='.$form->getObject()->field_id);
                }

                $this->redirect('@settingsFieldItemEdit?id=' . $form->getObject()->id);
            }
        }
    }

    /**
     * Clear cache for all product data
     * @param FieldItem $fieldItem
     */
    protected function clearProductCache(FieldItem $fieldItem)
    {
        /** @var ProductFieldItem $productFieldItem */
        foreach($fieldItem->getProductFieldItems() as $productFieldItem)
        {
            $product = $productFieldItem->getProductField()->getProduct();
            $productData = new ProductCalculationProductData($product);
            $productData->clearCache();
        }
    }

    /**
     * Executes deleteFieldItem action. Delete field item.
     *
     * @param sfRequest $request A request object
     */
    public function executeDeleteFieldItem(sfWebRequest $request)
    {
        $fieldItem = Doctrine::getTable('FieldItem')->find($request->getParameter('id'));
        $this->forward404Unless($fieldItem);

        $orders = OrderAttributeTable::getInstance()->getOrderIdsByFieldItemId($fieldItem->getId(), 5);

        // check if setting could be deleted
        if($orders['count'] !== 0){
            $message = sprintf($this->getContext()->getI18N()->__('This setting has %s open orders: %s'), $orders['count'], implode(', ', $orders['values']));
            if($orders['count'] > 5){
                $message.='...';
            }
            $this->getUser()->setFlash('info_data', array(
                    'message' => $message,
                    'messageType' => 'msg_error',
                ));
        } else {

            $fieldItem->delete();
            $this->getUser()->setFlash('info_data', array(
                    'message' => 'Deleted successfully.',
                    'messageType' => 'msg_success',
                ));
        }

        $fieldset = $fieldItem->getFieldset();

        $this->getUser()->setFlash('info_data', array(
            'message' => 'Deleted successfully.',
            'messageType' => 'msg_success',
        ));
        $this->redirect('settingsField', $fieldset);
    }

    /**
     * selectFieldItemFormByFieldset() select form for field item depends on fieldset
     *
     * @param Fieldset $fieldset Fieldset Object
     * @param FieldItem $fieldItem FieldItem Object
     * return Object $form symfony form
     */
    public function selectFieldItemFormByFieldset($fieldset, $fieldItem = null)
    {
        $fieldset = $fieldset->getRoot();
        $fieldset_key = ucfirst($fieldset->name);

        $class_name = 'Settings' . $fieldset_key . 'Form';

        if (class_exists($class_name))
        {
            return new $class_name($fieldItem);
        }

        throw new Exception('Wrong class name: ');
    }
    
    /**
     * Executes Edit field action
     *
     * @param sfRequest $request A request object
     */
    public function executeFieldsetEdit(sfWebRequest $request)
    {
        $fieldsetObj = $this->getRoute()->getObject();
        $this->forward404Unless($fieldsetObj);

        $fieldsetEditForm = new FieldsetEditForm($fieldsetObj);

        $this->processFieldsetForm($request, $fieldsetEditForm);    

        $this->fieldsetEditForm = $fieldsetEditForm;
    }
    
    private function processFieldsetForm($request, $form)
    {
        if ($this->getRequest()->isMethod('post'))
        {
            $form->bind($request->getParameter('fieldset'));
            if ($form->isValid())
            {
                $form->save();
                $this->getUser()->setFlash('info_data', array(
                    'message' => 'Saved successfully.',
                    'messageType' => 'msg_success',
                ));
                $this->redirect('@settings');
            }
        }
    }
    
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'FieldItem';
        
        $customParams = array();
        foreach($this->getRequest()->getParameterHolder()->getAll() as $key => $param)
        {
            if($key != 'selected' && $key != 'module' && $key != 'action')
            {
                $customParams[$key] = $param;
            }
        }
        $this->customRoutingParameters = $customParams;
        parent::executeDeleteMany($request);
    }
    
    public function executeLicense(sfWebRequest $request)
    {
        $this->form = new LicenseForm();
        $filename = priceTool::getLicenceKeyPath();
        
        if(file_exists($filename) && !is_writable($filename))
        {
            $this->getUser()->setFlash('info_data', array(
                'messageType' => 'info_red',
                'message' => 'File %filename% is not writeable. To update license key you need change permissions.',
                'messageParam' => array('%filename%' => $filename)
            ));
        }
        
        if($request->getMethod() == 'POST')
        {
            $this->form->bind($request->getParameter($this->form->getName()));
            
            if($this->form->isValid())
            {
                $key = $this->form->getValue('license');
                file_put_contents($filename, $key);
                
                $this->getUser()->setFlash('info_data', array(
                    'messageType' => 'info_green',
                    'message' => 'Saved successfully.',
                ));
                
                $this->redirect('@license');
            }
        }
        
        $information = priceTool::checkKey();
        
        if($information === false)
        {
            $this->correctLicense = false;
            return sfView::SUCCESS;
        }
        
        $this->correctLicense = true;
        
        $expires = (integer) $information['expire_at'];
        
        $this->organisation = (string) $information['name'];
        
        // defined in licence server ip
        $this->serverIp = (string) $information['ip'];
        $this->type = (string) $information['type'];
        $this->purchases = (string) date('Y-m-d', (string) $information['created_at']);
        $this->product = (string) $information['product'];
        
        $this->expiresClass = strtotime('+60 day') < $expires ? 'expiredSoon' : '';
        $this->expires = date('Y-m-d, H:i', $expires);
        $this->hasExpired = (time() > $expires);
        $this->licenseType = priceTool::$licenseType[(string) $information['type']];
        
        // client ip
        $this->ip = $_SERVER['REMOTE_ADDR'];
        
        if($this->hasExpired)
        {
            $this->getUser()->setFlash('info_data', array(
                'messageType' => 'info_red',
                'message' => 'Your Mnumi License subsciption was expired. Please contact with Mnumi support.'
            ));
        }
    }

}
