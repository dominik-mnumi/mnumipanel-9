<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ImportPaymentInvoiceForm imports payments.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class ImportPaymentInvoiceForm extends BaseForm
{
    public function configure()
    {
        $this->myUserObj = $this->getOption('myUserObj');
        
        // gets unpaid invoices
        $unpaidInvoiceColl = InvoiceTable::getInstance()->getInvoicesToPay();
        
        // gets choice array
        $choiceArr = $this->getChoiceArr($unpaidInvoiceColl);

        // widgets
        $this->setWidget('id',
                new sfWidgetFormChoice(array(
                    'choices' => $choiceArr)));
       
        // validators
        $this->setValidator('id',
                new sfValidatorChoice(array(
                    'choices' => array_keys($choiceArr))));
         
        // sets default messages
        $this->setMessages();
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
    }
    
    /**
     * Returns prepared choice array.
     * 
     * @param Doctrine_Collection $coll
     * @return array 
     */
    private function getChoiceArr($coll)
    {
        $choiceArr = array(0 => 'none');
        foreach($coll as $rec)
        {
            $choiceArr[$rec->getId()] = $rec->getName().'; '
                    .$rec->getDateTimeObject('created_at')->format('Y-m-d').'; '
                    .$rec->getPriceGross().' '.$this->myUserObj->getCurrencySymbol();
        }
        
        return $choiceArr;
    }
}