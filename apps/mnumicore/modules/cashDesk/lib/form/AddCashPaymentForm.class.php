<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CashKpForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class AddCashPaymentForm extends CashDeskForm
{

    public function configure()
    {
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        // text field for client autocomplete
        $this->setWidget('client',
                new sfWidgetFormInputText(
                        array('label' => 'Customer')));
        $this->setWidget('client_id', new sfWidgetFormInputHidden());

        // gets payment and type formatted array
        $paymentAndTypeOptArr = CashDeskTable::getInstance()
                ->getPaymentAndTypeOptions();
        $this->setWidget('paymentAndType',
                new sfWidgetFormChoice(
                        array('choices' => $paymentAndTypeOptArr,
                            'label' => 'Choose type and payment')));

        // sets css classes for all used widget
        $this->setCssClasses('full-width');

        $this->setValidator('client_id',
                new sfValidatorDoctrineChoice(
                        array('model' => $this->getRelatedModelName('Client'),
                            'required' => true),
                        array('required' => 'Please select customer')));
        $this->setValidator('value',
                new sfValidatorNumberExtended(array('required' => true, 'min' => 1),
                        array('required' => 'Please specify payment amount', 'invalid' => 'Incorrect value', 'min' => 'Value must be a positive number.')));
        $this->setValidator('description',
                new sfValidatorString(array('required' => true),
                        array('required' => 'Please specify description of payment')));
        $this->setValidator('client',
                new sfValidatorString(array('required' => false)));
        $this->setValidator('paymentAndType',
                new sfValidatorChoice(
                        array('choices' => CashDeskTable::getInstance()
                                    ->getPaymentAndTypeKeyOptions($paymentAndTypeOptArr),
                            'required' => true)));

        $this->useFields(array('client_id', 'value', 'description', 'client', 'paymentAndType'));
    }

    /**
     * Updates CashDesk object.
     * 
     * @param type $values
     * @return Doctrine_Record
     */
    public function updateObject($values = null)
    {
        $values = $this->getValues();

        $object = parent::updateObject($values);

        // gets payment and type
        $paymentAndType = $values['paymentAndType'];
        $paymentAndType = explode('_', $paymentAndType);
        
        $object->setNumber(CashDeskTable::getInstance()
                ->getNextNumber($paymentAndType[0], $paymentAndType[1]));
        
        $object->setType($paymentAndType[0]);
        $object->setPaymentId($paymentAndType[1]);
        
        // calculates balance depends on type
        if($paymentAndType[0] == 'IN')
        {
            $balance = CashDeskTable::getInstance()
                    ->getLastBalance($paymentAndType[1]) + $values['value'];
        }
        elseif($paymentAndType[0] == 'OUT')
        {
            $object->setValue($values['value'] * (-1));
            $balance = CashDeskTable::getInstance()
                    ->getLastBalance($paymentAndType[1]) - $values['value'];
        }
        else
        {
            throw new Exception('This type of payment was not implemented yet.');
        }
        $object->setBalance($balance);

        return $object;
    }
}