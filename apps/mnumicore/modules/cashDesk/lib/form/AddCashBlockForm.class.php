<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Cash 'block' form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class AddCashBlockForm extends CashDeskForm
{

    public function configure()
    {
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $paymentOptArr = PaymentTable::getInstance()->getDepositOptions();
        $this->setWidget('payment_id',
                new sfWidgetFormChoice(
                        array('choices' => $paymentOptArr,
                            'label' => 'Choose type and payment')));

        // sets css classes for all used widget
        $this->setCssClasses('full-width');

        // validators
        $this->setValidator('value',
                new sfValidatorNumberExtended(
                        array(),
                        array('invalid' => 'Incorrect value')));
        $this->setValidator('description',
                new sfValidatorString(
                        array('required' => true)));
        $this->setValidator('payment_id',
                new sfValidatorChoice(
                        array('choices' => array_keys($paymentOptArr),
                            'required' => true)));

        // sets required messages for all used validators
        $this->setMessages();

        $this->useFields(array('value', 'description', 'payment_id'));
    }

    /**
     * Updates CashDesk object.
     * 
     * @param type $values
     * @return Doctrine_Record
     */
    public function updateObject($values = null)
    {
        // gets values
        $values = $this->getValues();

        $object = parent::updateObject($values);

        $object->setType('BLOCK');
        $object->setValue($values['value'] * (-1));

        $object->setNumber(CashDeskTable::getInstance()->getNextNumber('BLOCK', $values['payment_id']));
        $object->setBalance(CashDeskTable::getInstance()->getLastBalance($values['payment_id']) - $values['value']);

        return $object;
    }
}