<script type="text/javascript">
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskPaymentEdit)): ?>
    // js hack for cashDesk edit header
    $("#dialog-add-block-click")
    .css("left", $("#dialog-add-kp-click").position().left + $("#dialog-add-kp-click").width()
    + 24);
    <?php endif; ?>
 
    // js hack for cashDesk edit header (import payments)
    $("#import-payments-click")
    .css("left", $("#dialog-add-block-click").position().left + $("#dialog-add-block-click").width()
    + 24);
 
    /**
     * Renders errors list depending on json return array.
     */
    function renderError(errorArray)
    {
        var errorlist = '<ul class="message error no-margin">';
        for(i in errorArray)
        {
            errorlist += '<li>' + errorArray[i] + '</li>';
        }
        errorlist += '</ul>';
        
        return errorlist;
    }

    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskPaymentEdit)): ?>
    // KP add form
    $("#dialog-add-kp-click").click(function() 
    {                     
        $.modal({
            content: $('#dialog-add-kp-form'),
            title: '<?php echo __('Add payment'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-add-kp-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
            	initializeAutocomplete($(".modal-content #AddCashPaymentForm_client"), "<?php echo url_for('@userClientAutocomplete'); ?>");
                
            },
            buttons: 
                {
                '<?php echo __('Add payment'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-add-kp-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-add-kp-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();
                                    
                                    var href = "<?php echo url_for('@cashDesk'); ?>";
                                    window.location.href = href;               
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
    });
    <?php endif; ?>
    
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskBlockEdit)
                && PaymentTable::getInstance()->isAnyDepositEnabled()): ?>
    // BLOCK add form
    $("#dialog-add-block-click").click(function() 
    {                     
        $.modal({
            content: $('#dialog-add-block-form'),
            title: '<?php echo __('Add advance payment'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-add-block-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);         
            },
            buttons: 
            {
                '<?php echo __('Add advance payment'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-add-block-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-add-block-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();
                                    
                                    var href = "<?php echo url_for('@cashDesk'); ?>";
                                    window.location.href = href;               
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
    });
    <?php endif; ?>
</script>

