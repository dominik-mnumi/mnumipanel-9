<div class="height-40px"></div>
<h3>
    <?php echo __('List of payments'); ?>    
</h3>

<table class="table margin-top10">
    <thead>
        <tr>
            <th class="black-cell sorting_disabled" style="width: 16px; "></th>
            <th><?php echo __('Date of payment'); ?></th>
            <th><?php echo __('Title'); ?></th>
            <th><?php echo __('Amount'); ?></th>
            <th><?php echo __('Matched invoices'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($coll as $key => $rec): ?>
        <?php if($rec->hasMatchedInvoices()): ?>

        <!-- prepares row form with invoices collection -->
        <?php $importPaymentRowFormObj = new ImportPaymentRowForm(
                        array(), 
                        array('active' => $rec->getAmount() == $rec->getTotalAmountOfMatchedInvoices(),
                            'rowId' => $key,
                            'invoiceColl' => $rec->getMatchedInvoices(),
                            'myUserObj' => $sf_user)); ?>
        <tr>
            <td>
                <?php echo $importPaymentRowFormObj[$key]->renderHiddenFields(); ?>
                <?php echo $importPaymentRowFormObj[$key]['active']->render(); ?>
            </td>
            <td><?php echo $rec->getDate(); ?></td>
            <td><?php echo $rec->getTitle(); ?></td>
            <td><?php echo $sf_user->formatCurrency($rec->getAmount()); ?></td>
            <td>   
                <?php foreach($importPaymentRowFormObj[$key]['invoiceColl'] as $rec2): ?>
                <span>                        
                    <?php echo $rec2['id']->render(); ?>    
                </span>    
                <br />   
                <?php endforeach; ?>

                <br />
                <?php if($rec->getAmount() == $rec->getTotalAmountOfMatchedInvoices()): ?>
                <span class="color-green">
                    <?php echo image_tag('icons/fugue/tick-circle.png'); ?>
                    <?php echo __('Total amount of matched invoices'); ?>:
                    <?php echo $sf_user->formatCurrency($rec->getTotalAmountOfMatchedInvoices()); ?>
                </span>
                <?php else: ?>
                <span class="color-red">
                    <?php echo image_tag('icons/fugue/cross-circle.png'); ?>
                    <?php echo __('Total amount of matched invoices'); ?>: 
                    <?php echo $sf_user->formatCurrency($rec->getTotalAmountOfMatchedInvoices()); ?>
                </span>
                <?php endif; ?>
            </td>
        </tr>
        <?php endif; ?>
        <?php endforeach; ?>
    </tbody>
</table>

  


