<?php use_helper('permission'); ?>
<?php echo include_partial('dashboard/message'); ?>

<article class="container_12">
    <?php include_partial('global/filterForm', $tableOptions) ?>
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('Payments'); ?>
                    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskPaymentEdit)): ?>
                    <a href="#" id="dialog-add-kp-click">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add payment'); ?>"> <?php echo __('Add payment'); ?>
                    </a>
                    <?php endif; ?>
                    
                    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskBlockEdit)
                            && PaymentTable::getInstance()->isAnyDepositEnabled()): ?>
                    <a href="#" id="dialog-add-block-click">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add advance payment'); ?>"> <?php echo __('Add advance payment'); ?>
                    </a>
                    <?php endif; ?>
                    
                    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskImportPayment)): ?>
                    <a href="<?php echo url_for('cashImport'); ?>" id="import-payments-click">
                        <?php echo __('Import payments'); ?>
                    </a>
                    <?php endif; ?>
                </h1>
            <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
 
        <!-- if user has permission to report access -->
        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskReportAccess)): ?>
            <?php include_partial('cashReport/reqReport', array('payments' => $payments, 'cashReports' => $cashReports)); ?>
        <?php endif; ?>
    </section>
</article>
<?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskPaymentEdit)): ?>
<?php echo include_partial('cashDesk/form/paymentForm', array('form' => $formArray[0])); ?>
<?php endif; ?>

<?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskBlockEdit)
            && PaymentTable::getInstance()->isAnyDepositEnabled()): ?>
<?php echo include_partial('cashDesk/form/blockForm', array('form' => $formArray[1])); ?>
<?php endif; ?>

<?php echo include_partial('createJavascript'); ?>

<script type="text/javascript">
$('.search-div .form').on('click','#filterBtn', function()
{
    $(".loader").show();
    $("#reports-container").hide();

    $.ajax(
    {
        url: "<?php echo url_for('reqReport') ?>",
        type: "POST",
        data: $("#simple_form").serialize(),
        success: function(html)
        {
            $("#reports-container").replaceWith(html);

            $(".loader").hide();
            $("#reports-container").show();
        }
    });
}); 

<?php if($sf_user->hasCredential(sfGuardPermissionTable::$cashDeskReportAccess)): ?>
$('#reports-container').on('click', '.filter-daily-docs-click', function(e)
{
    e.preventDefault();

    var paymentId = $(this).attr('data-payment-id');
    var date = $(this).attr('data-date');

    $('#cashDesk_filters_payment_id').val(paymentId);
    $('#cashDesk_filters_from_date').val(date);
    $('#cashDesk_filters_to_date').val(date);

    $("#filterBtn").trigger('click');
}); 
<?php endif; ?>
</script>