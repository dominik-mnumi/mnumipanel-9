<?xml version="1.0" encoding="UTF-8"?>
<items title="<?php echo __('Kp/Kw list'); ?>">
    <?php foreach($objectColl as $object): ?>
    <item>
        <id><?php echo $object->getId();?></id>
        <number><?php echo $object->getNumber() ;?></number>
        <type><?php echo $object->getType(); ?></type>
        <description><?php echo $object->getDescription(); ?></description>
        <value><?php echo $object->getValue(); ?></value>
        <balance><?php echo $object->getBalance(); ?></balance>    
    </item>
    <?php endforeach; ?>
</items>
