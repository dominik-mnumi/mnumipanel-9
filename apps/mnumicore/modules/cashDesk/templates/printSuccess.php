<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title><?php echo __('Number').': '.$dataObj->getName(); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style type="text/css"> 
            @media print 
            { 
                #printer 
                {
                    display: none;
                }
            }
        </style>
        <link rel="stylesheet" href="/css/print-kp-kw.css" type="text/css" media="print, screen" />
    </head>
    <body>       
        <div id="printer">
            <a class="control" href="#" onclick="window.print(); return false;">
                <?php echo image_tag('icons/web-app/32/Print.png') ?>
            </a>
        </div>  
        <?php include_partial('cashDesk/docPrint', array('dataObj' => $dataObj)); ?>
    </body>
</html>
