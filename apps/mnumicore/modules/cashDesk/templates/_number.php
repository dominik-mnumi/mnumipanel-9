<?php if($row->getPaymentId()): ?>
<?php echo $row->getPayment(); ?>
 - 
<?php endif ?>
<?php echo $row->getName(); ?>
<?php if($row->getInvoiceId()): ?>
    (<a href="<?php echo url_for('clientInvoiceEdit', array('client_id' => $row->getInvoice()->getClient()->getId(), 'id' => $row->getInvoice()->getId())); ?>">
        <?php echo $row->getInvoice()->getName() ?>
    </a>)
<?php endif ?>