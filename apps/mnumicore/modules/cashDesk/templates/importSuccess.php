<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <div class="block-border">
        <div class="block-content">
            <h1>
                <?php echo __('Import payments'); ?>             
            </h1>
            <?php include_partial('dashboard/formErrors', 
                    array('form' => $uploadPaymentFileFormObj)); ?>
            
            <form method="post" 
                  enctype="multipart/form-data"
                  class="form"
                  action="<?php echo url_for('cashImport'); ?>">
                <div>
                    <?php echo $uploadPaymentFileFormObj->renderHiddenFields(); ?>
                    <?php echo $uploadPaymentFileFormObj['file']->render(); ?>
                </div>
                <div class="margin-top10">
                    <?php echo $uploadPaymentFileFormObj['encoding']->renderLabel(); ?>
                    <?php echo $uploadPaymentFileFormObj['encoding']->render(); ?>
                    <button><?php echo __('Load'); ?></button>
                </div>
            </form>
            
            <?php if(isset($coll)): ?>
            <form method="post" 
                  id="payment-import-list"
                  class="form"  
                  action="<?php echo url_for('cashImport'); ?>">
 
                <?php include_partial('cashDesk/importFormTable', 
                        array('coll' => $coll)); ?>

                <div class="text-right">
                    <button><?php echo __('Import payments'); ?></button>
                </div>
            
            </form>
            <?php endif; ?>
        </div>     
    </div>
</article>
