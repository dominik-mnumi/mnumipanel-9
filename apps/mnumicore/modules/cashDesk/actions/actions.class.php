<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * cashDesk actions.
 *
 * @package    mnumicore
 * @subpackage cashDesk
 * @author     Adam Marchewicz <adam.marchewicz@itme.eu>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cashDeskActions extends complexTablesActions
{

    /**
     * Executes index action.
     *
     * @param sfWebRequest $request A request object
     * @return mixed
     */
    public function executeIndex(sfWebRequest $request)
    {
        parent::cashDeskList($request);

        $this->actions = array(
            'print' => array('partial' => 'print'));

        $this->tableOptions = $this->executeTable();

        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }

        // prepares forms
        $formArray[0] = new AddCashPaymentForm();
        $formArray[1] = new AddCashBlockForm();

        /* By default display reports for all types of payment */
        $payments = PaymentTable::getInstance()->getCashReportPayments();

        $this->formArray = $formArray;
        $this->payments  = $payments;
        $this->cashReports = CashReportTable::getInstance()->getDateSortedReports();
    }

    /**
     * Executes delete action. Deletes CashDesk object.
     *
     * @param sfRequest $request A request object
     */
    public function executeDelete(sfWebRequest $request)
    {
        $cashDeskObj = $this->getRoute()->getObject();

        if($cashDeskObj->delete())
        {
            $this->getUser()->setFlash('info_data', array(
                'message'     => 'Deleted successfully.',
                'messageType' => 'msg_success',
            ));
        }
        $this->redirect('cashDesk');
    }

    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'CashDesk';
        parent::executeDeleteMany($request);
    }

    /**
     * Executes form process for new KP
     *
     * @param sfRequest $request A request object
     */
    public function executeEditNewKpForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }

    /**
     * Executes form process for new BLOCK
     *
     * @param sfRequest $request A request object
     */
    public function executeEditNewBlockForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }

    /**
     * Export KP and KW to XML format
     * 
     * @param sfRequest $request A request object
     */
    public function executeExportKpKwXml(sfWebRequest $request)
    {
        // gets selected ids
        $idArray = $request->getParameter('selected', array());

        // gets collection of selected ids
        $objectColl = CashDeskTable::getInstance()->getSelectedCaskDesk($idArray);

        // generates xml content
        $xmlContent = $this->getPartial('exportKpKwXml', array('objectColl' => $objectColl));

        // sets response
        $response = $this->getResponse();
        $response->setContentType('text/xml');
        $response->setHttpHeader('Content-Disposition', "attachment; filename=".CashDeskTable::getInstance()->generateXmlFilename());
        $response->setContent($xmlContent);

        return sfView::NONE;
    }

    /**
     * Prints kp an kw.
     * 
     * @param sfRequest $request A request object
     */
    public function executePrint(sfWebRequest $request)
    {
        // view objects
        $this->dataObj = $this->getRoute()->getObject();
    }

    /**
     * Imports payments.
     * 
     * @param sfRequest $request A request object
     */
    public function executeImport(sfWebRequest $request)
    {
        $uploadPaymentFileFormObj = new UploadPaymentFileForm();

        // if method post
        if($request->isMethod('post'))
        {
            $paramArr = $request->getParameter($uploadPaymentFileFormObj->getName());

            // if upload file posted
            if(!empty($paramArr))
            {
                $fileArr = $request->getFiles($uploadPaymentFileFormObj->getName());

                $uploadPaymentFileFormObj->bind($paramArr, $fileArr);

                // if $uploadPaymentFileFormObj form is valid
                if($uploadPaymentFileFormObj->isValid())
                {
                    $filename = $uploadPaymentFileFormObj->save();
     
                    $this->getUser()->setFlash('info_data', array(
                        'message' => 'File loaded successfully.',
                        'messageType' => 'msg_success'));
                    
                    $paymentImportAdapterCsvObj = new PaymentImportAdapterCsv($filename, $paramArr['encoding']);
   
                    // view objects
                    $this->coll = $paymentImportAdapterCsvObj->getRowColl();
                }
            }

            // if imported
            if($request->hasParameter('ImportPaymentRowForm'))
            {
                $parameterArr = $request->getParameter('ImportPaymentRowForm');

                // if success
                if($this->importPaymentsAndSetInvoicesPaid($parameterArr))
                {
                    $this->redirect('cashImport');
                }
            }
        }

        // view objects
        $this->uploadPaymentFileFormObj = $uploadPaymentFileFormObj;
    }

    /**
     * Foreach selected row sets invoices as paid and sets payment.
     * 
     * @param array $parameterArr
     * @return boolean
     * @throws Exception
     */
    protected function importPaymentsAndSetInvoicesPaid(array $parameterArr)
    {
        $invoiceToPayIdArr = array();

        // foreach payment row
        foreach($parameterArr as $rec)
        {
            // if checked active
            if(!empty($rec['active']))
            {
                // foreach invoice from row
                foreach($rec['invoiceColl'] as $rec2)
                {
                    // gets unique invoices
                    if($rec2['id'] &&
                            !in_array($rec2['id'], $invoiceToPayIdArr))
                    {
                        $invoiceToPayIdArr[] = $rec2['id'];
                    }
                }
            }
        }

        // gets invoice collection basing on sent data
        $invoiceColl = InvoiceTable::getInstance()->getInvoiceByIds($invoiceToPayIdArr);
   
        $paymentObj = PaymentTable::getInstance()
                    ->findOneByName(PaymentTable::$transfer21Days);
  
        // sets transaction
        $conn = Doctrine_Manager::connection();
        try
        {    
            $conn->beginTransaction();
            
            foreach($invoiceColl as $rec)
            {
                $obj = clone($rec);                
                $obj->setPayment($paymentObj);     
                $obj->setInvoiceStatusName(InvoiceStatusTable::$paid);                 
                $obj->save();
            }

            $conn->commit();
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new Exception($e->getMessage());
        }

        // messages
        if($invoiceColl->count())
        {
            $this->getUser()->setFlash('info_data', array(
                'message'     => 'Selected rows imported successfully. Matched invoices set as paid. Payment documents were generated.',
                'messageType' => 'msg_success'));
            
            return true;
        }
        else
        {
            $this->getUser()->setFlash('info_data', array(
                'message'     => 'No invoices to pay.',
                'messageType' => 'msg_success'));
            
            return false;
        }  
    }

}
