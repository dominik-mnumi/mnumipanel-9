<span class="favourite-order-span 
    <?php echo $row->isFavouriteOf($sf_user->getGuardUser()->getId()) 
            ? 'favourite-order-active' 
            : 'favourite-order-inactive' ?>" 
      data-favourite-url="<?php echo url_for('reqFavouriteOrder'); ?>"
      data-order-id="<?php echo $row->getId(); ?>">
</span>
