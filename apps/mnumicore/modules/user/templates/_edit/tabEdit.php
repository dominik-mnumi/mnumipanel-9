<?php use_helper('allTimeBack') ?>
<div id="tab-edit" class="with-margin">
    <fieldset>
        <legend><?php echo __('General information'); ?></legend>
        <div class="columns">
            <div class="colx3-left data_container">
               <div class="text_container big_text"><?php echo $userObj->getFirstName(); ?> <?php echo $userObj->getLastName(); ?>
                   <?php if($userObj->getIsActive() === false):?>
                       (<?php echo __('Inactive'); ?>)
                   <?php endif; ?>
               </div>
               <div class="text_container">
                   <a href="mailto:<?php echo $userObj->getEmailAddress(); ?>"><?php echo $userObj->getEmailAddress(); ?></a>
                   <?php if(!$userObj->isUsernameGenerated()): ?>
                       , <?php echo $userObj->getFriendlyUsername() ?>
                   <?php endif ?> 
               </div>
               <div class="small_text">
                   <?php if($userObj->getProfile()->getLastFailedLogin()): ?>
                   <?php echo __('Last failed login'); ?>: 
                   <span title = "<?php echo $userObj->getProfile()->getLastFailedLogin() ?>">
                       <?php echo allTimeBack($userObj->getProfile()->getLastFailedLogin()) ?>
                   </span>
                   <?php endif; ?>
               </div>
               <div class="small_text">
                   <?php if($userObj->getLastLogin()): ?>
                   <span title = "<?php echo $userObj->getLastLogin() ?>">
                       <?php echo __('Last login'); ?>: <?php echo allTimeBack($userObj->getLastLogin()) ?>
                   </span>
                   <?php endif; ?>
               </div>
               <div class="small_text">
                   <span title = "<?php echo $userObj->getLastLogin() ?>">
                       <?php echo __('Loyalty points'); ?>: <?php echo $userObj->getAvailablePointsCount() ?> - 
                       <?php echo $sf_user->formatCurrency($userObj->getAvailablePointsValue()) ?> 
                   </span>
               </div>
            </div>
            <div class="colx3-right">
                <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userGeneralInformation)): ?>
                <button type="button" id="dialog-edit-user-general-click"><?php echo __('Change'); ?></button><br/><br/>
                <?php endif; ?>
                <a href="<?php echo url_for('@userPrintCard?id='.$userObj->getId()) ?>" target="_blank">
                    <button type="button"><?php echo __('Print user card') ?></button>
                </a>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Contact data'); ?></legend>
        <div class="columns">
            <div class="colx3-left data_container">
                <div class="text_container">
                    <?php if(0 < $userObj->getUserContacts()->count()): ?>
                    <?php foreach($userObj->getUserContacts() as $rec): ?>
                    <div class="text_container">
                        <?php echo $rec->getValue(); ?>                    
                    </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                    <?php echo __('No contact data'); ?>
                    <?php endif; ?>
                </div>   
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userContact)): ?>
            <div class="colx3-right">
                <button id="dialog-edit-user-contact-data-click" type="button"><?php echo __('Change'); ?></button>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Password'); ?></legend>
        <div class="columns">
            <div class="colx3-left data_container">
                <div class="text_container big_text">
                    ******  
                </div>
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userPasswordChange)): ?>
            <div class="colx3-right">
                <button type="button" id="dialog-edit-user-password-click"><?php echo __('Change'); ?></button>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Office permissions'); ?></legend>
        <div class="columns">
            <div class="colx3-left data_container">
                <div class="text_container">
                    <?php if(0 < $userObj->getSfGuardUserGroup()->count()): ?>
                    <?php foreach($userObj->getSfGuardUserGroup() as $rec): ?>
                    <?php $permissionArray[] = $rec->getGroup()->getDescription(); ?>
                    <?php endforeach; ?>
                    <?php if(!empty($permissionArray)): ?>
                    <?php echo implode(', ', $permissionArray); ?>
                    <?php endif; ?>
                    <?php else: ?>
                    <?php echo __('No permission group'); ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userOfficePermissionChange)): ?>
            <div class="colx3-right">
                <button type="button" id="dialog-edit-user-permission-click"><?php echo __('Change'); ?></button>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Connected with companies'); ?></legend>
        <div class="columns">
            <div class="colx3-left data_container">      
                <div class="text_container">
                    <?php if(0 < $userObj->getClientUsers()->count()): ?>
                    <?php foreach($userObj->getClientUsers() as $rec): ?>
                    <div class="text_container">
                        <a href="<?php echo url_for('@clientListEdit?id='.$rec->getClient()->getId()); ?>">
                            <?php echo $rec->getClient()->getFullname(); ?>&nbsp;                   
                        </a>
                        -&nbsp;<?php echo $rec->getClientUserPermission()->getDescription(); ?>
                    </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                    <?php echo __('No customers'); ?>
                    <?php endif; ?>
                </div>   
               
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userEdit)
                    && $sf_user->hasCredential(sfGuardPermissionTable::$clientEdit)): ?>
            <div class="colx3-right">
                <a href="<?php echo url_for('userClientList', $userObj); ?>">
                    <button type="button"><?php echo __('Change'); ?></button>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Notification'); ?></legend>
        <?php echo include_partial('user/edit/form/notificationForm', array('form' => $formArray[2])); ?>       
    </fieldset>   
</div>
