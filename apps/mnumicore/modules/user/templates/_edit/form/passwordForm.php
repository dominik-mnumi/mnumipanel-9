<form id="dialog-edit-user-password-form" action="<?php echo url_for('reqEditUserPasswordForm', 
        array('model' => 'sfGuardUser', 
            'type' => 'edit')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('Change password'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                                                    
                <p  class="required">
                    <?php echo $form['password']->renderLabel(); ?>
                    <?php echo $form['password']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['password_again']->renderLabel(); ?>
                    <?php echo $form['password_again']->render(); ?>
                <p>                                          
            </div>
        </div>
    </div>
</form>