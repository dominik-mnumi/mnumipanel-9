<form id="dialog-edit-user-general-form" action="<?php echo url_for('reqEditUserGeneralForm', 
        array('model' => 'sfGuardUser', 
            'type' => 'edit')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('Edit user'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                      
                <p class="required">
                    <?php echo $form['username']->renderLabel(); ?>
                    <?php echo $form['username']->render(); ?>
                </p>                               
                <p  class="required">
                    <?php echo $form['first_name']->renderLabel(); ?>
                    <?php echo $form['first_name']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['last_name']->renderLabel(); ?>
                    <?php echo $form['last_name']->render(); ?>
                <p>               
                <p class="required">
                    <?php echo $form['email_address']->renderLabel(); ?>
                    <?php echo $form['email_address']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['is_active']->renderLabel(); ?>
                    <?php echo $form['is_active']->render(); ?>
                <p>
            </div>
        </div>
    </div>
</form>