<form id="dialog-add-loyalty-points-form" style="display: none;" action="<?php echo url_for('reqAddLoyaltyPoints', 
        array('model' => 'LoyaltyPoint', 
            'type' => 'new')); ?>" method="post">
    <p class="validateTips"></p>
    <?php echo $form['_csrf_token']->render(); ?>    
    <?php echo $form['user_id']->render(); ?>
    <?php echo $form['status']->render(); ?>
    <div class="block-content form no_border" style="padding-top: 0px;">
        <p class="required">
            <?php echo $form['points']->renderLabel(); ?>
            <?php echo $form['points']->render(); ?>
        </p>
        <p class="required">
            <?php echo $form['description']->renderLabel(); ?>
            <?php echo $form['description']->render(); ?>
        </p>
    </div>    
</form>