<form id="dialog-edit-user-notification-form" action="<?php echo url_for('reqEditUserNotificationCollForm', 
        array('model' => 'sfGuardUser', 
            'type' => 'edit')); ?>" enctype="multipart/form-data" method="post">
    <?php echo $form['_csrf_token']->render(); ?>    
    <?php echo $form['id']->render(); ?>
    <?php $contactArrayForm =  $form->getEmbeddedForm('Contacts'); ?>
    <?php $obj =  $form->getObject(); ?>
    <div class="text_container data_container">
        <?php if(0 < $obj->getUserContacts()->count()): ?>
        <table>
            <?php foreach($contactArrayForm->getEmbeddedForms() as $key => $contactForm): ?>
            <tr>   
                <td style="width: 200px">
                    <div class="text_container">
                        <?php echo $form['Contacts'][$key]->renderLabelName(); ?>
                        (<?php echo $obj->UserContacts[$key]->getNotificationType()->getName(); ?>)
                    </div>
                </td>
                <td>
                    <?php echo $form['Contacts'][$key]['id']->render(); ?>
                    <?php echo $form['Contacts'][$key]['send_notification']->render(); ?>
                </td>
            </tr>    
            <?php endforeach; ?>
        </table>
        <?php else: ?>
        <?php echo __('No contact data'); ?>
        <?php endif; ?>
    </div>    
</form>

<!-- if no permission then unbind input -->
<?php if(!$sf_user->hasCredential(sfGuardPermissionTable::$userNotification)): ?>
<script type="text/javascript">
jQuery(document).ready(function($) 
{ 
    $('#dialog-edit-user-notification-form .mini-switch-replace').unbind('click');  
});
</script>
<?php endif; ?>