<form id="dialog-add-new-client-form" action="<?php echo url_for('reqEditUserNewClientForm', 
        array('model' => 'ClientSfGuardUserProfile', 
            'type' => 'new')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('Customer'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">    
                <p class="required">
                    <label><?php echo __('User'); ?></label>               
                    <?php echo $form->getObject()->getUser()->__toStringFullData(); ?>
                </p> 
                <p class="required">                
                    <?php echo $form['client']->renderLabel(); ?>
                    <?php echo $form['client']->render(); ?>
                    <?php echo $form['client_id']->render(); ?>
                    <?php echo $form['user_id']->render(); ?>
                </p>    
                <p class="required">
                    <?php echo $form['client_user_permission_id']->renderLabel(); ?>
                    <?php echo $form['client_user_permission_id']->render(); ?>
                </p>  
            </div>
        </div>
    </div>
</form>

