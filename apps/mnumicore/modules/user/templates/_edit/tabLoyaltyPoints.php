<fieldset id="tab-loyalty-points">
    <legend><?php echo __('Loyalty points') ?></legend>
    <div class="columns">
        <div class="colx3-left data_container">      
            <div class="text_container">
            <section style="width: 400px;">
                <div class="block-border"><div class="block-content no-title">
                    <ul class="simple-list with-icon">
                        <?php foreach($types as $type): ?>
                        <li class="icon-points-<?php echo $type?>">
                            <span>&nbsp;&nbsp;<?php echo __(ucfirst($type)) ?>: <?php echo $userObj->getPointsCount($type) ?></span>
                        </li>
                        <?php endforeach ?>
                    </ul>
                </div></div>
            </section>
            </div>
        </div>
        <div class="colx3-right">
            <button class="gray" onclick="window.location = '<?php echo url_for('userLoyaltyPoints', $userObj)?>'" type="button"><?php echo __('Details') ?></button>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userLoyaltyPointsAdding)): ?>
            <button id="dialog-add-loyalty-points-click"><?php echo __('Add loyalty points') ?></button>
            <?php endif; ?>
        </div>
    </div>
</fieldset>