<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('User customers'); ?>: <?php echo $userObj->__toStringFullData();  ?>
                    <a id="dialog-add-new-client-click" href="#">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add customer')  ?>"> <?php echo __('add'); ?>
                    </a>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>
<?php echo include_partial('user/edit/form/clientNewForm', array('form' => $formArray[0])); ?>
<?php echo include_partial('javascript'); ?>