<div class="card">
    <?php echo __('Cardholder') ?><br/>
    <div><?php echo $userObj ?></div>
    <?php echo __('Agency') ?><br/>
    <div><?php echo sfConfig::get('app_company_data_seller_name') ?></div>
    <?php echo __('Valid from') ?><br/>
    <div><?php echo format_date(time(), 'D') ?></div>
    <div> <img src="<?php echo url_for('reportBarcode', array('code' => $userObj->getBarcodeForCard()), true); ?>" /></div>
</div>