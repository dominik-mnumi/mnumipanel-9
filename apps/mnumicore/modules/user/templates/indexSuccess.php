<?php include_partial('dashboard/message'); ?>
<article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content form" >
                <h1>
                    <?php echo __('Users'); ?>
                    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userCreate)): ?>
                    <a id="dialog-add-user-click" href="#">                        
                        <?php echo image_tag('icons/fugue/plus-circle-blue.png',
                                array('width' => 16,
                                    'height' => 16,
                                    'title' => __('Add customer'))); ?>
                        <?php echo __('add'); ?>                          
                    </a>
                    <?php endif; ?>
                </h1> 
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
                </div>
            </div>
        </section>
</article>
<?php include_partial('user/new/form', array('form' => $newUserForm)); ?>
<?php include_partial('javascript'); ?>