<!-- for switches  -->
<?php if($reloadJs): ?>
    <script type="text/javascript" src="/js/common.js"></script>
<?php endif; ?>
<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form no_top_padding">
                <h1>
                    <?php echo __('User'); ?>: <?php echo $userObj->__toStringFullData(); ?>
                </h1>
                    
                <div class=""></div>
                
                <?php include_partial('user/edit/tabList'); ?>
                <?php include_partial('user/edit/contentTab', array('userObj' => $userObj,
                    'formArray' => $formArray, 'types' => $types)); ?>              
            </div>
        </div>
    </section>
</article>
<?php echo include_partial('user/edit/form/generalForm', array('form' => $formArray[0])); ?>
<?php echo include_partial('user/edit/form/passwordForm', array('form' => $formArray[1])); ?>
<!-- $formArray[2] goes to contentTab -->
<?php echo include_partial('user/edit/form/contactForm', array('form' => $formArray[3])); ?>
<?php echo include_partial('user/edit/form/permissionForm', array('form' => $formArray[4])); ?>
<?php echo include_partial('user/edit/form/loyaltyPointsForm', array('form' => $formArray[5])); ?>
<?php echo include_partial('editJavascript', array('userObj' => $userObj)) ?>
<?php echo include_partial('javascript', array('userObj' => $userObj)); ?>