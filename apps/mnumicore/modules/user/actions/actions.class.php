<?php

/**
 * user actions.
 *
 * @package    mnumicore
 * @subpackage user
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class userActions extends complexTablesActions
{
    public function executeSetLang(sfWebRequest $request)
    {
        sfContext::getInstance()->getUser()->setAttribute('default_culture', $this->getRequest()->getParameter('lang'));
        return sfView::NONE;
    }
    /**
     * Executes index action. Shows users list.
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->actions = array('edit' => array());
        $this->displayTableFields = array(
            'FriendlyUsername'  => array('label' => 'Username', 'partial' => 'username'),
            'FirstAndLastName'  => array('label' => 'Name', 'partial' => 'firstAndLastName'),
            'Permissions'       => array('label' => 'Permissions', 'partial' => 'permissions'),
            'UpdatedAt'         => array('label' => 'Updated at', 'partial' => 'updatedAt'),
            'CreatedAt'         => array('label' => 'Created at', 'partial' => 'createdAt')
        );
        $this->displayGridFields = array(
            'Username'  => array('destination' => 'name', 'label' => 'Username'), 
            'CreatedAt'  => array('destination' => 'keywords', 'label' => 'Created at')
        );
        $this->sortableFields = array('username', 'first_name', 'last_name', 'created_at', 'updated_at');
        $this->modelObject = 'sfGuardUser';

        // sets custom query
        $this->customQuery = sfGuardUserTable::getInstance()->getSortedUsersWithoutSpecialsQuery();
         
        $this->tableOptions = $this->executeTable();

        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
            
        //new user form
        $newUserForm = new AddUserForm();

        //view objects
        $this->newUserForm = $newUserForm;
    }
    
    /**
     * Executes edit action. Edits user.
     *
     * @param sfRequest $request A request object
     */
    public function executeEdit(sfWebRequest $request)
    {       
        $userObj = $this->getRoute()->getObject();

        //sets current client id
        $this->getUser()->setAttribute('id', $userObj->getId(), 'user');
       
        $formArray[] = new EditUserGeneralForm($userObj);
        $formArray[] = new EditUserPasswordForm($userObj); 
        $formArray[] = new EditUserNotificationCollForm($userObj);
        $formArray[] = new EditUserContactDataCollForm($userObj);
        $formArray[] = new EditUserPermissionCollForm($userObj);
        $formArray[] = new LoyaltyPointForm(null, array('user_id' => $userObj->getId()));

        //view objects
        $this->userObj = $userObj;
        $this->formArray = $formArray;
        $this->reloadJs = $request->getParameter('reload');
        $this->types = LoyaltyPointTable::$types;
    }
    
    /**
     * Executes delete action. Deletes user.
     *
     * @param sfRequest $request A request object
     */
    public function executeDelete(sfWebRequest $request)
    {
        $userObj = $this->getRoute()->getObject();
        $currentUserObj = $this->getUser()->getGuardUser();
        $userObj->delete();

        // if user deletes itself
        if($currentUserObj->getId() == $userObj->getId())
        {
            $this->redirect('sf_guard_signout');
        }
        
        $this->getUser()->setFlash('info_data', array(
                        'messageType' => 'msg_success',
                        'message' => 'Deleted successfully.',
                        'messageParam' => array()));   

        $this->redirect('userList');
    }
    
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'sfGuardUser';
        parent::executeDeleteMany($request);
    }
    
    /**
     * User action. 
     *
     * @param sfRequest $request A request object
     */
    public function executeClient(sfWebRequest $request)
    {
        $userObj = $this->getRoute()->getObject();

        // gets sql
        $this->customQuery = ClientUserTable::getInstance()
                ->createClientUserTableQueryByUserId($userObj->getId());

        $this->customRoutingParameters = array('id' => $userObj->getId());

        $this->displayTableFields = array(
            'Fullname' => array('label' => 'Name'),
            'PermissionDescription' => array('label' => 'Permission'),
            'ClientUpdatedAt' => array('label' => 'Updated at'),
            'ClientCreatedAt' => array('label' => 'Created at')            
        );
        $this->displayGridFields = array(
            'Fullname' => array('destination' => 'name', 'label' => 'Name'),
            'ClientCreatedAt' => array('destination' => 'keywords', 'label' => 'Created at')
        );

        $this->sortableFields = array('fullname', 'permission_description',
            'client_updated_at', 'client_created_at');

        $this->modelObject = 'ClientUser';
        
        $this->tableOptions = $this->executeTable();
      
        if($request->isXmlHttpRequest() && !$request->hasParameter('forcePost'))
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }

        // prepares clientProfileObj
        $clientUserObj = new ClientUser();
        $clientUserObj->setUser($userObj);
        $formArray[0] = new EditUserNewClientForm($clientUserObj);

        //view objects
        $this->userObj = $userObj;
        $this->formArray = $formArray;

        // ajax request - only view
        if($request->hasParameter('forcePost'))
        {
            $this->setLayout(false);
        }
    }

    /**
     * Executes user edit action.
     *
     * @param sfRequest $request A request object
     */
    public function executeClientEdit(sfWebRequest $request)
    {
        $clientUserObj = $this->getRoute()->getObject();

        $formArray[0] = new EditUserEditClientForm($clientUserObj);

        if($request->isMethod('post'))
        {
            $formArray[0]->bind($request->getParameter($formArray[0]->getName()), $request->getFiles($formArray[0]->getName()));

            if($formArray[0]->isValid())
            {
                $formArray[0]->save();
                $this->getUser()->setFlash('info_data', array(
                        'messageType' => 'msg_success',
                        'message' => 'Saved successfully.',
                        'messageParam' => array()));  
                $this->redirect('userClientList', $clientUserObj->getUser());
            }
        }

        // view objects
        $this->clientUserObj = $clientUserObj;
        $this->formArray = $formArray;
    }

    /**
     * Executes delete action. Deletes user.
     *
     * @param sfRequest $request A request object
     */
    public function executeClientDelete(sfWebRequest $request)
    {
        $clientUserObj = $this->getRoute()->getObject();
        $clientUserObj->delete();

        $this->getUser()->setFlash('info_data', array(
                        'messageType' => 'msg_success',
                        'message' => 'Deleted successfully.',
                        'messageParam' => array()));  
        $this->redirect('userClientList', $clientUserObj->getUser());
    }

    public function executeClientDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'ClientUser';

        $userId = $this->getUser()->getAttribute('id', null, 'user');
        $this->customRoutingParameters = array('id' => $userId);

        parent::executeDeleteMany($request);
    }

    /**
     * Returns json data for autocomplete.
     * 
     * @param sfWebRequest $request 
     */
    public function executeReqAutocomplete(sfWebRequest $request)
    {
        $return = null;

        if($request->isXmlHttpRequest())
        {
            if($request->isMethod('get'))
            {
                $key = $request->getParameter('q');

                $query = ClientTable::getInstance()->createClientQuery($key);
                $coll = $query->execute();

                foreach($coll as $rec)
                {
                    $return .= "$rec->keystring|$rec->id\n";
                }
            }
        }

        sfConfig::set('sf_web_debug', false);

        return $this->renderText($return);
    }
    
    // ajax form actions
    public function executeReqAddUserForm(sfWebRequest $request)
    {
        parent::executeRequestAjaxProcessForm($request);

        // decode ajax json content
        $ajaxResponseDecoded = json_decode($this->getResponse()->getContent(), true);
        
        if(!$ajaxResponseDecoded['result']['error']['errorCount'])
        {
            $userObj = sfGuardUserTable::getInstance()
                    ->find($ajaxResponseDecoded['result']['id']);

            if($userObj)
            {
                // triggers the event
                $this->dispatcher->notify(new sfEvent($this,
                                'user.create',
                                array('obj' => $userObj)));
            }
        }
        
        return sfView::NONE;
    }
    
    public function executeReqEditUserGeneralForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }
   
    public function executeReqEditUserNewClientForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }
    
    public function executeReqEditUserPasswordForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }
    
    public function executeReqEditUserContactDataCollForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }
    
    public function executeReqEditUserNotificationCollForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }
    
    public function executeReqEditUserPermissionCollForm(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }
    
    public function executeReqAddLoyaltyPoints(sfWebRequest $request)
    {
        return parent::executeRequestAjaxProcessForm($request);
    }
    
    /**
     * Services favourite order.
     * 
     * @param sfWebRequest $request
     * @return json 
     */
    public function executeReqFavouriteOrder(sfWebRequest $request)
    {
        if($request->isXmlHttpRequest())
        {
            $orderObj = OrderTable::getInstance()
                    ->find($request->getParameter('orderId'));
            
            if($orderObj)
            {
                $orderId = $orderObj->getId();
                $userId = $this->getUser()->getGuardUser()->getId();
                
                $orderUserFavouriteObj = $orderObj->isFavouriteOf($userId);
                
                // if order user favourite exists then deletes
                if($orderUserFavouriteObj)
                {
                    $orderUserFavouriteObj->delete();
                }
                // otherwise create
                else
                {
                    $orderUserFavouriteObj = new OrderUserFavourite();
                    $orderUserFavouriteObj->setOrderId($orderId);
                    $orderUserFavouriteObj->setUserId($userId);
                    $orderUserFavouriteObj->save();
                }
                
                return $this->renderText(json_encode(array('result' => 'success')));
            }      
        }
        
        return $this->renderText(json_encode(array('result' => 'failed')));
    }
    
    /**
     * Executes loyalty points list.
     *
     * @param sfRequest $request A request object
     */
    public function executeLoyaltyPoints(sfWebRequest $request)
    {
        $this->userObj = $this->getRoute()->getObject();
        
        $status = $request->getParameter('type');
        $this->points = $this->userObj->getPoints($status);
        
        $this->types = LoyaltyPointTable::$types;
    }
    
    /**
     * Executes notificationTab
     *
     * @param sfRequest $request A request object
     */
    public function executeNotificationTab(sfWebRequest $request)
    {
        $userObj = $this->getRoute()->getObject();
        parent::notificationMessageList($request);
    
        $this->customRoutingParameters = array('id' => $userObj->getId());
        $this->customQuery = NotificationMessageTable::getInstance()->createQuery('nm')->where('nm.user_id = ?', $userObj->getId());
        
        $this->tableOptions = $this->executeTable();
        
        return $this->renderPartial('global/table', array('tableOptions' => $this->tableOptions));
    }
    
    /**
     * Executes orderTab
     *
     * @param sfRequest $request A request object
     */
    public function executeOrderTab(sfWebRequest $request)
    {
        $userObj = $this->getRoute()->getObject();
        parent::orderList($request);
      
        $this->customRoutingParameters = array('id' => $userObj->getId());
        $this->customQuery = OrderTable::getInstance()->getLatestQuery()
                ->andWhere('user_id = ?', $userObj->getId());
        
        // custom edit action because we want to change default route for it
        $this->actions = array(
            'edit' => array('label' => 'Edit',
                'route' => 'orderListEdit',
                'icon' => '/images/icons/fugue/pencil.png',
                'attributes' => array('title' => 'Edit')),
            'favourite' => array('partial' => 'favouriteOrder')        
        );
      
        $this->tableOptions = $this->executeTable();
      
        return $this->renderPartial('global/table', 
                array('tableOptions' => $this->tableOptions));
    }
    
    /**
     * Executes print user card action
     *
     * @param sfRequest $request A request object
     */
    public function executePrintCard(sfWebRequest $request)
    {
      $this->userObj = $this->getRoute()->getObject();
    }
}
