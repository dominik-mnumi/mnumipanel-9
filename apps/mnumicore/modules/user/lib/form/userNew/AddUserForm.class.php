<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended sfGuardUserForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class AddUserForm extends sfGuardUserForm
{
    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
        
        $this->setValidator('username', new sfValidatorRegexUsername(
            array('pattern' => '/^[a-z0-9\-\_\.]+$/',
                'required' => false),
            array('invalid' => 'Field "username" can contain only letters, numbers, ".", "-" and "_"')));
     
        //sets post validators
        $this->getValidatorSchema()->setPostValidator(
                new sfValidatorAnd(array(
                    new sfValidatorDoctrineUnique(
                        array('model' => 'sfGuardUser', 
                              'column' => 'username'),
                        array('invalid' => 'This "Username" already exist')),
                    new sfValidatorDoctrineUnique(array('model' => 'sfGuardUser', 
                              'column' => 'email_address'),
                        array('invalid' => 'This "Email address" already exist')),
                    new sfValidatorSchemaCompare(
                        'password', 
                        sfValidatorSchemaCompare::EQUAL, 
                        'password_again', 
                        array(), 
                        array('invalid' => 'The two passwords must be the same'))
                )));
        
        $this->getValidator('username')->setOption('required', false);
        
        $this->useFields(array('username', 'first_name', 'last_name',
            'email_address', 'password', 'password_again'));
    }

    public function updateObject($values = null)
    {
        $object = parent::updateObject();

        return $object;
    }
    
    public function save($con = null) 
    {
        $object = parent::save($con);

        $profile = new SfGuardUserProfile();
        $profile->setUserId($this->getObject()->getId());
        $profile->save();

        return $object;
    }
  
}

