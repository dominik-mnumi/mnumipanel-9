<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ContactForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditUserPermissionForm extends sfGuardUserGroupForm
{
    public function configure()
    {        
        parent::configure();
        
        $this->setWidget('value', new sfWidgetFormInputCheckbox());                
        $this->setValidator('value', new sfValidatorBoolean(
                array('required' => false)));
       
        $this->useFields(array('user_id', 'group_id', 'value'));
        
    }
}

