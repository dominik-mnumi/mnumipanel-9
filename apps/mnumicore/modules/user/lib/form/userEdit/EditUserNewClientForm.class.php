<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ClientSfGuardUserProfileForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */

class EditUserNewClientForm extends ClientUserForm
{

    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $this->getWidget('client_user_permission_id')->setAttribute('class', 'full-width');

        // validators
        $this->setValidator('client', new sfValidatorString(
                        array('required' => false)));
        
        $this->useFields(array('client', 'client_user_permission_id',
            'client_id', 'user_id'));
    }


}

