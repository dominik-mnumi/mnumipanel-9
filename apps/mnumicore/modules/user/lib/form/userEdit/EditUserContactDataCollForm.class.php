<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ContactForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditUserContactDataCollForm extends sfGuardUserForm
{
    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
     
        // create form collection
        $formColl = new sfForm();
        
        // sets current notification type array
        $notificationTypeExistArray = array();
        
        // gets all existent user contacts
        $userContactColl = $this->getObject()->getUserContacts();
      
        $i = 0;
        foreach($userContactColl as $rec)
        {
            $userContactForm = new EditUserContactDataForm($rec);
            $formColl->embedForm($i, $userContactForm);
                
            $notificationTypeExistArray[] = $rec->getNotificationTypeId();
            
            $i++;
        }
                
        // gets all notification types
        $notificationTypeColl = NotificationTypeTable::getInstance()->findAll();
        
        // all notification type id array
        foreach($notificationTypeColl as $rec)
        {
            $notificationTypeAllArray[] = $rec->getId();
        }
       
        // gets difference
        $newNotificationTypeArray = array_diff($notificationTypeAllArray, $notificationTypeExistArray);
        
        // foreach notification type which not in user contact
        foreach($newNotificationTypeArray as $rec)
        {
            $userContactForm = new EditUserContactDataForm();
            
            // sets default
            $userContactForm->setDefault('notification_type_id', $rec);

            // sets notification to object for label
            $userContactForm->getObject()->setNotificationTypeId($rec);
            
            $formColl->embedForm($i, $userContactForm);
            
            $i++;
        }

        // embeds UserContacts
        $this->embedForm('Contacts', $formColl);
        
        $this->useFields(array('Contacts'));
       
    }
    
    public function saveEmbeddedForms($con = null, $forms = null)
    {
        $forms = $this->embeddedForms['Contacts']->embeddedForms;
        foreach($forms as $key => $form)
        {        
            // if email then unset (email cannot be edited or deleted from this form)
            if($form->getObject()->getNotificationType()->getName() == NotificationTypeTable::$email)
            {
                unset($forms[$key]);
                continue;
            }
            
            $form->getObject()->setUser($this->getObject());
            
            // synchronize entries
            if($form->getObject()->getValue() == '')
            {
                $form->getObject()->delete();
                unset($forms[$key]);
            }         
        }
        
        parent::saveEmbeddedForms($con, $forms);
        
    }
    
    /**
     * Unbinds email values from contact data. Preserve to changing email.
     * 
     * @param array $taintedValues
     * @param array $taintedFiles 
     */
    public function bind(array $taintedValues = null, array $taintedFiles = null)
    {
        // foreach user contact with notification email
        foreach($taintedValues['Contacts'] as $key => $rec)
        {
            if($rec['notification_type_id'] == NotificationTypeTable::getInstance()->findOneByName(NotificationTypeTable::$email))
            {
                unset($taintedValues['Contacts'][$key]);
                break;
            }
        }
        
        parent::bind($taintedValues, $taintedFiles);
    }
 
}

