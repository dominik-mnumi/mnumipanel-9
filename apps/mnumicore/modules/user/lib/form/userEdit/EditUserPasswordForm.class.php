<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended sfGuardUserForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditUserPasswordForm extends sfGuardUserForm
{
    public function configure()
    {
        parent::configure();
 
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
     
        $this->useFields(array('password', 'password_again'));

        //sets post validator
        $this->getValidatorSchema()->setPostValidator(
                new sfValidatorAnd(array(
                    new sfValidatorSchemaCompare(
                            'password', 
                            sfValidatorSchemaCompare::EQUAL, 
                            'password_again', 
                            array(), 
                            array('invalid' => 'The two passwords must be the same')),
                   /* 
                    * if admin password to protect first
                    * 
                    * new sfValidatorCallback(array(
                            'callback' => array($this, 'validCurrentPassword'))) */
                    )));
  
    }
    
    /**
     * Check if old password match
     * 
     * @param sfValidator $validator
     * @param array $values
     * @return array 
     */
    public function validCurrentPassword($validator, $values)
    {
        $obj = $this->getObject();

        if($obj->checkPassword($values['password_old']))
        {  
            $error = new sfValidatorError($validator, 'Field "Current password" does not match');
            throw new sfValidatorErrorSchema($validator, array('password_old' => $error));
        }
        return $values;
    }
 
}

