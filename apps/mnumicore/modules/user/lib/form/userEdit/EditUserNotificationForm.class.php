<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ContactForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditUserNotificationForm extends UserContactForm
{
    public function configure()
    {
        parent::configure();

        $this->getWidget('send_notification')->setAttribute('id', 'send_notification_'.$this->getObject()->getId());
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
            
        $this->useFields(array('send_notification'));
       
    }
}

