<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ClientSfGuardUserProfileForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditUserEditClientForm extends ClientUserForm
{

    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
        $this->useFields(array('client_user_permission_id', 'user_id', 'client_id'));
    }

}
