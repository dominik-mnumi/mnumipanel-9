<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended sfGuardUserForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditUserGeneralForm extends sfGuardUserForm
{
    public function configure()
    {
        parent::configure();
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $this->getWidget('is_active')->setLabel('Active');
     
        $this->useFields(array('username', 'first_name', 'last_name',
            'email_address', 'is_active'));
        
        $this->setValidator('username', new sfValidatorRegexUsername(
            array('pattern' => '/^[a-z0-9\-\_\.]+$/',
                'required' => false),
            array('invalid' => 'Field "username" can contain only letters, numbers, ".", "-" and "_"')));
        
        $this->getValidatorSchema()->setPostValidator(
                new sfValidatorAnd(array(
                    new sfValidatorDoctrineUnique(
                        array('model' => 'sfGuardUser', 
                              'column' => 'username'),
                        array('invalid' => 'This "Username" already exist')),
                    new sfValidatorDoctrineUnique(array('model' => 'sfGuardUser', 
                              'column' => 'email_address'),
                        array('invalid' => 'This "Email address" already exist')),
                    new sfValidatorSchemaCompare(
                        'password', 
                        sfValidatorSchemaCompare::EQUAL, 
                        'password_again', 
                        array(), 
                        array('invalid' => 'The two passwords must be the same'))
                )));
        
        // if username is generated set empty field
        if($this->getObject()->isUsernameGenerated())
        {
            $this->getWidget('username')->setAttributes(array_merge(array('value' => ''), $this->getWidget('username')->getAttributes()));
        }
    }
    
    
    public function updateObject($values = null)
    {
        if(!$values)
        {
            $values = $this->getValues();
        }
        
        //if username is generated and form input is empty set previous username
        if($this->getObject()->isUsernameGenerated() && !$this->getValue('username'))
        {
            $values['username'] = $this->getObject()->getUsername();
        }
        
        $object = parent::updateObject($values);
    }
 
}

