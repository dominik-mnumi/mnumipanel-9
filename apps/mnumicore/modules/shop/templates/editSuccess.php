<form id="new_field_category_form" method = "post" class="form"
      action="<?php echo $form->isNew() ? url_for('settingsShopAdd') : url_for('settingsShopEdit', $form->getObject())?>">
    <?php echo $form->renderHiddenFields() ?>

    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'settingsShop')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content form">
                    <?php if($form->isNew()): ?>
                    <h1><?php echo __('Create API key'); ?></h1>

                    <?php else: ?>
                    <h1><?php echo __('Edit API key'); ?></h1>
                    <p>
                        <?php echo $form['name']->renderError(); ?>
                        <?php echo $form['name']->renderLabel(); ?>
                        <?php echo $form['name']->getValue(); ?>
                    </p>
                    <?php endif; ?>

                    <p>
                        <?php echo $form['description']->renderError(); ?>
                        <?php echo $form['description']->renderLabel(); ?>
                        <?php echo $form['description']->render(); ?>
                    </p>
                    <p>
                        <?php echo $form['valid_date']->renderError(); ?>
                        <?php echo $form['valid_date']->renderLabel(); ?>
                        <span class="input-type-text">
                            <?php echo $form['valid_date']->render(); ?>
                        </span>
                    </p>
                    <p>
                        <?php echo $form['host']->renderError(); ?>
                        <?php echo $form['host']->renderLabel(); ?>
                        <?php echo $form['host']->render(); ?>
                    </p>
                    <p>
                        <?php echo $form['type']->renderError(); ?>
                        <?php echo $form['type']->renderLabel(); ?>
                        <?php echo $form['type']->render(); ?>
                    </p>
                    
                </div>
            </div>
        </section>
    </article>
</form>