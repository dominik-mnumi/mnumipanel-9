<?php echo __('Hey'); ?>,
<br /><br />
<?php echo __('You have requested a new password.'); ?>
<br /><br />
<?php echo __('You can change your password here:'); ?> <br />
<h2><a href="<?php echo $tokenLink; ?>"><?php echo $tokenLink; ?></a></h2>
<br /><br />
<?php echo __("If you did not request a new password, another user may have tried to log in trying to use your password or email by mistake."); ?>
<br /><br />
<?php echo __('For more information, contact your administrator.'); ?>
<br /><br />
<?php echo __(''); ?>,
<br /><br />
<?php echo __('Mnumi system'); ?>