<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title><?php echo __('Monthly report'); ?>: <?php echo $date; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style type="text/css"> 
            @media print 
            { 
                #printer 
                {
                    display: none;
                }
            }
        </style>
    </head>
    <body>
        <div id="printer">
            <a class="control" href="#" onclick="window.print(); return false;">
                <?php echo image_tag('icons/web-app/32/Print.png') ?>
            </a>
        </div> 
        <?php foreach($dailyReportColl as $rec): ?>
        <?php include_partial('cashReport/dailyPrint', array('reportObj' => $rec)); ?>
        <?php endforeach; ?>
    </body>
</html>