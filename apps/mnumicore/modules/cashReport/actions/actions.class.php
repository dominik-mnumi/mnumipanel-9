<?php

/**
 * cashReport actions.
 *
 * @package    mnumicore
 * @subpackage cashReport
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cashReportActions extends sfActions
{

    /**
     * Executes reqReport action.
     *
     * @param sfRequest $request A request object
     */
    public function executeReqReport(sfWebRequest $request)
    {
        $cashDeskFilterParam = $request->getParameter('cashDesk_filters');

        /** By default get all payments */
        $payments = array();
        if ($cashDeskFilterParam['payment_id']) {
            $payments[] = PaymentTable::getInstance()->findOneById($cashDeskFilterParam['payment_id']);
        }
        else {
            $payments = PaymentTable::getInstance()->getCashReportPayments();
        }

        /** By default get all dates */
        $reportDate = null;
        if (($cashDeskFilterParam['from_date'] == $cashDeskFilterParam['to_date']) &&
            ($cashDeskFilterParam['from_date'] != ''))
        {
            $reportDate = $cashDeskFilterParam['from_date'];
        }
        $cashReports = CashReportTable::getInstance()->getDateSortedReports($cashDeskFilterParam['payment_id']);

        return $this->renderPartial('reqReport',
                array('payments' => $payments,
                    'reportDate' => $reportDate,
                    'cashReports' => $cashReports));
    }
    
    public function executeDailyPrint(sfWebRequest $request)
    {
        // gets report object
        $reportObj = $this->getRoute()->getObject();
        
        // view objects
        $this->reportObj = $reportObj;
    }
    
    public function executeMonthlyPrint(sfWebRequest $request)
    {
        // gets payment id
        $paymentId = $request->getParameter('paymentId');
        
        // if payment id == all then set payment id as null
        if($paymentId == 'all')
        {
            $paymentId = null;
        }
        
        // gets date (year and month)
        $date = $request->getParameter('date');
        
        // view objects
        $this->date = $date;
        $this->dailyReportColl = CashReportTable::getInstance()
                ->getDailyReportsOfYearAndMonth($paymentId, $date);
    }
    
    public function executeMonthlyDocsPrint(sfWebRequest $request)
    {
        // gets payment id
        $paymentId = $request->getParameter('paymentId');
        
        // if payment id == all then set payment id as null
        if($paymentId == 'all')
        {
            $paymentId = null;
        }
        
        // gets date (year and month)
        $date = $request->getParameter('date');

        // view objects
        $this->date = $date;
        $this->cashDeskColl = CashDeskTable::getInstance()->getMonthlyDocs($paymentId, $date);
    }

}
