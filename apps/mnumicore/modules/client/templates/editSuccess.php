<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <?php include_partial('client/edit/notices', array('notices' => $clientObj->getClientNotices())) ?>
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form no_top_padding">
                <h1>
                    <?php echo __('Customer'); ?>: <?php echo $clientObj->getFullname(); ?> 
                    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderCreate)): ?>
                    <a id="dialog-new-order-click" href="#">                  
                        <?php echo image_tag('/images/icons/fugue/plus-circle-blue.png', 
                                array('width' => 16, 'height' => 16)); ?>
                        <?php echo __('Add order'); ?>
                    </a>       
                    <?php endif; ?>
                </h1>
                <div></div>
                <?php include_partial('client/edit/tabList', array(
                    'clientObj' => $clientObj,
                    'defaultDisableAdvancedAccountancy' => $defaultDisableAdvancedAccountancy)); ?>
                <?php include_partial('client/edit/contentTab', array('clientObj' => $clientObj)); ?>
                <?php include_partial('client/edit/form/orderNewForm', array('rootObj' => $rootObj,
                                                                             'clientObj' => $clientObj,
                                                                             'form' => $clientEditFormArray[3],
                                                                             'creatingDisabled'=>$creatingDisabled)); ?>
               
            </div>
        </div>
    </section>
</article>

<?php echo include_partial('client/edit/form/generalForm', array('form' => $clientEditFormArray[0])); ?>
<?php echo include_partial('client/edit/form/creditForm', array('form' => $clientEditFormArray[1])); ?>
<?php echo include_partial('client/edit/form/tagForm', array('form' => $clientEditFormArray[2])); ?>
<?php echo include_partial('javascript'); ?>

<script type="text/javascript">
    tagJson = <?php echo $tagJson;?>;
</script>

