<script type="text/javascript">
    // start objects
    var tagJson = new Array();
 
    /**
     * Renders errors list depending on json return array.
     */
    function renderError(errorArray)
    {
        var errorlist = '<ul class="message error no-margin">';
        for(i in errorArray)
        {
            errorlist += '<li>' + errorArray[i] + '</li>';
        }
        errorlist += '</ul>';
        
        return errorlist;
    }

    /**
     * New order click from client interface. 
     */
    $(".modal-content #dialog-new-order-form .client-new-order").live('click', function(e)
    {
        e.preventDefault();
        
        var clientId = $(this).attr("data-client");
        var productId = $(this).attr("data-product");
        var userId = $(".modal-content #dialog-new-order-form #order_user_id").val();
        var href = $(this).attr("href");

        window.location = href + '?clientId=' + clientId + '&productId=' + productId + '&userId=' + userId;
    });
 
    // js hack for client edit header
    $(".block-content .h1").css("left", $(".block-content h1").width() + 35);
  
    function initializeTagIt()
    {
        // TagIt
        $(".modal-content #client_tags").tagit({
            availableTags: tagJson,
            singleField: true,
            singleFieldNode: $('.modal-content #EditClientTagForm_tagContainer')
        });
           
        var inputSelector = $(".modal-content #dialog-edit-client-tag-form input[type=text]");

        var interval = setInterval(
        function()
        {
            if(!inputSelector.first().is(":focus"))
            {
                $(inputSelector).first().focus();
                clearInterval(interval);
            }
        }
        , 
        700);          
    }
     
    // initialize tabs
    hideAllShowCurrent();

    $(".controls-tabs li a").click(function()
    {
        hideAllShowCurrent(this);
    });

    //client edit general information form
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientGeneralInformationEdit)): ?>
    $("#dialog-edit-client-general-click").click(function() 
    {
        $.modal({
            focus: function(event, ui) {alert('sdfd'); },
            content: $("#dialog-edit-client-general-form"),          
            title: '<?php echo __('General information'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-edit-client-general-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
            },
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-edit-client-general-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-edit-client-general-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();

                                    $.get(window.location,
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );                  
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
        
    });
    <?php endif; ?>
    
    //client edit credit form
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientCreditEdit)): ?>
    $("#dialog-edit-client-credit-click").click(function() 
    {
        $.modal({
            content: $("#dialog-edit-client-credit-form"),
            title: '<?php echo __('Credits'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-edit-client-credit-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
            },
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-edit-client-credit-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-edit-client-credit-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();

                                    $.get(window.location,
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );                  
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        }); 
    });
    <?php endif; ?>

    //client edit tag form
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientTagEdit)): ?>
    $("#dialog-edit-client-tag-click").click(function() 
    {     
        $.modal({
            content: $("#dialog-edit-client-tag-form"),
            title: '<?php echo __('Tags'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: initializeTagIt,
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-edit-client-tag-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-edit-client-tag-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();

                                    $.get(window.location,
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );                  
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });   
    });
    <?php endif; ?>
         
    //client edit attachment new form
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientAttachmentEdit)): ?>
    $("#dialog-add-new-attachment-click").click(function() 
    {      
        $.modal({
            content: $("#dialog-add-new-attachment-form"),
            title: '<?php echo __('Attachment'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-add-new-attachment-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
            },
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-add-new-attachment-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    
                    //prepares options
                    var options = { 
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput) 
                        {   
                            if('success' == dataOutput.result.status)
                            {        
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-add-new-attachment-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();    
                                    $.get("?forcePost=1",
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );     
                                         
                                }
                                
                            }
                        } 
                    }; 
                    
                    //inititializes
                    $(form).ajaxForm(options); 
                    
                    //submits
                    form.submit();           
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
        
    });
    <?php endif; ?>
    
    // client edit delivery new form
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientDeliveryEdit)): ?>
    $("#dialog-add-new-delivery-click").click(function() 
    {      
        $.modal({
            content: $("#dialog-add-new-delivery-form"),
            title: '<?php echo __('Delivery address'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-add-new-delivery-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
            },
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-add-new-delivery-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);
                                    
                                    $(".modal-content #dialog-add-new-delivery-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();    
                                    $.get("?forcePost=1",
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );                  
                                }
                                
                            }
                           
                        }
                    });       
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
        
    });
    <?php endif; ?>
   
    // client add new order
    $("#dialog-new-order-click").click(function(e)
    {
        e.preventDefault();

        // initializes list two times. Hack for secondary modal window reference.
        initializeList();
        $.modal({
            content: $("#dialog-new-order-form"),
            title: '<?php echo __('Add order'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,  
            onOpen: initializeList,
            buttons: 
                { 
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
        
    });
  
    // initializes autocomplete for user
    initializeAutocomplete($("#dialog-add-edit-user-form #EditClientEditUserForm_userProfile"), "<?php echo url_for('@clientUserAutocomplete'); ?>");
    
    // client edit user new form
    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientTraderEdit)): ?>
    $("#dialog-add-new-user-click").click(function() 
    {      
        $.modal({
            content: $("#dialog-add-new-user-form"),
            title: '<?php echo __('User'); ?>',
            maxWidth: 700,
            maxHeight: 450,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-add-new-user-form input[type=text]");
                
                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
                
                // initializes autocomplete for user
                initializeAutocomplete($(".modal-content #EditClientNewUserForm_user"), "<?php echo url_for('@clientUserAutocomplete'); ?>");
            },
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-add-new-user-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('There was error. Please try again later.') ?>');
                        },
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);
                                    
                                    $(".modal-content #dialog-add-new-user-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();    
                                    $.get("?forcePost=1",
                                    function(html)
                                    {
                                        $("#sf_content").html(html);
                                    }
                                );                  
                                }
                                
                            }
                           
                        }
                    });       
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });        
    });  
    <?php endif; ?>
</script>

