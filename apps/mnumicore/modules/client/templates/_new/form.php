<div id="dialog-add-client-form" style="display: none;">
    <div class="block-controls">
        <ul class="controls-buttons">
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userCreate)): ?>   
            <li><a id="invitation-button" href="#"><?php echo __('Invitations'); ?></a></li>
            <?php endif; ?>
            <li><a id="company-button" class="current" href="#"><?php echo __('Company'); ?></a></li>
            <li><a id="person-button" href="#"><?php echo __('Private individual'); ?></a></li>
        </ul>
    </div>    
    <p class="validateTips"></p>

    <!-- new client form -->
    <form id="new-client-form" action="<?php echo url_for('reqAddClientForm', 
            array('model' => 'Client', 
                'type' => 'new')); ?>" enctype="multipart/form-data" method="post">
      
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['company_checkbox']->render(); ?>
        <div class="block-border">        
            <div class="block-content form no_border">   
                <p class="required">
                    <label class="company-fullname" for="AddClientForm_fullname"><?php echo __('Full Company name'); ?></label>
                    <label class="person-fullname" style="display: none;" for="AddClientForm_fullname"><?php echo __('Name and surname'); ?></label>
                    <?php echo $form['fullname']->render(); ?>
                </p>   
                <p class="required">
                    <?php echo $form['country']->renderLabel(); ?>
                    <?php echo $form['country']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['street']->renderLabel(); ?>
                    <?php echo $form['street']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['postcodeAndCity']->renderLabel(); ?>
                    <?php echo $form['postcodeAndCity']->render(); ?>
                <p>               
                <p class="tax-id-p required">
                    <?php echo $form['tax_id']->renderLabel(); ?>
                    <?php echo $form['tax_id']->render(); ?>
                <p>
                <p>
                    <?php echo $form['account']->renderLabel(); ?>
                    <?php echo $form['account']->render(); ?>
                <p>
                
                <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userCreate)): ?>    
                <p>
                    <?php echo $form['user_checkbox']->render(); ?>
                    <?php echo $form['user_checkbox']->renderLabel(); ?>                   
                </p>  
                <div class="client-new-user">               
                    <p class="required">
                        <?php echo $form['user_name_surname']->renderLabel(); ?>
                        <?php echo $form['user_name_surname']->render(); ?>
                    </p> 
                    <p class="required">
                        <?php echo $form['user_email']->renderLabel(); ?>
                        <?php echo $form['user_email']->render(); ?>
                    </p> 
                    <p class="required">
                        <?php echo $form['user_password']->renderLabel(); ?>
                        <?php echo $form['user_password']->render(); ?>
                    </p> 
                    <p class="required">
                        <?php echo $form['user_password_again']->renderLabel(); ?>
                        <?php echo $form['user_password_again']->render(); ?>
                    </p> 
                </div> 
                <?php endif; ?>
            </div>
        </div>
    </form>

    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$userCreate)): ?>   
    <!-- user invitation form -->
    <form id="user-invitation-form" action="<?php echo url_for('reqUserInvitationForm', 
            array('model' => 'sfGuardUser', 
                'type' => 'new')); ?>" enctype="multipart/form-data" method="post" 
                style="display: none;">
      
        <?php echo $form2['_csrf_token']->render(); ?>
        <div class="block-border">        
            <div class="block-content form no_border">                             
                <p class="required">
                    <?php echo $form2['email_address']->renderLabel(); ?>
                    <?php echo $form2['email_address']->render(); ?>
                <p>            
            </div>
        </div>
    </form>
    <?php endif; ?>
</div>

<!-- sets js slot -->
<?php extendSlot('actionJavascript') ?>
<script type="text/javascript">
    /**
     * Services display content for company.
     */
    function showCompanyForm()
    {
        $(".modal-content .validateTips").html('');
        
        $(".modal-content #user-invitation-form").hide();
        $(".modal-content #new-client-form").show();
        
        $(".modal-content .controls-buttons a").removeClass("current");
        $(".modal-content #company-button").addClass("current");      
        
        // cleans user name when company button clicked
        $(".modal-content #AddClientForm_user_name_surname").val("");
        
        // sets company hidden input to true
        $(".modal-content #AddClientForm_company_checkbox").val(1);
        
        $(".modal-content .person-fullname").hide(); 
        $(".modal-content .company-fullname").show();   
        
        
        $(".modal-content .tax-id-p").show();
    }
    
    /**
     * Services display content for person.
     */
    function showPersonForm()
    {
        $(".modal-content .validateTips").html('');
        
        $(".modal-content #user-invitation-form").hide();
        $(".modal-content #new-client-form").show();
        
        $(".modal-content .controls-buttons a").removeClass("current");
        $(".modal-content #person-button").addClass("current");
        
        // sets company hidden input to true
        $(".modal-content #AddClientForm_company_checkbox").val(0);
        
        $(".modal-content .person-fullname").show();
        $(".modal-content .company-fullname").hide();       
        
        $(".modal-content .tax-id-p").hide();
    }
    
    /**
     * Services display content for invitation.
     */
    function showInvitationForm()
    {
        $(".modal-content .validateTips").html('');
        
        $(".modal-content #new-client-form").hide();
        $(".modal-content #user-invitation-form").show();
       
        $(".modal-content .controls-buttons a").removeClass("current");
        $(".modal-content #invitation-button").addClass("current");
    }
    
    jQuery(document).ready(function($) 
    {  
        /**
         * Company button click event.
         */
        $(".modal-content #company-button").live("click", function()
        {
            showCompanyForm();
        });
        
        /**
         * Person button click event.
         */
        $(".modal-content #person-button").live("click", function()
        {
            showPersonForm();
        });
        
        /**
         * Invitation button click event.
         */
        $(".modal-content #invitation-button").live("click", function()
        {
            showInvitationForm();
        });
        
        /**
         * New user button click event.
         */
        $(".modal-content #AddClientForm_user_checkbox").live("click", function()
        {
            $(".modal-content .client-new-user").toggle();
        });
        
        /**
         * Sets user name the same as client name.
         */
        $(".modal-content #AddClientForm_fullname").live("change", function()
        {
            if($(".modal-content #person-button").hasClass("current"))
            {
                $(".modal-content #AddClientForm_user_name_surname")
                .val($(".modal-content #AddClientForm_fullname").val());
            }
        });
    });
</script>
<?php end_slot() ?>