<?php use_helper('allTimeBack'); ?>
<ul class="blocks-list">
<?php foreach($row->getClientUsers() as $clientUser): ?>
<li>
    <a href="<?php echo url_for('userListEdit', $clientUser->getUser()) ?>" class="float-left">
    <?php echo $clientUser->getUser()->getFirstName() ?> <?php echo $clientUser->getUser()->getLastName() ?>
        <?php if($clientUser->getUser()->getIsActive() === false):?>
            (<?php echo __('Inactive'); ?>)
        <?php endif; ?>
    (<?php echo $clientUser->getUser()->getEmailAddress() ?>
    <?php if(!$clientUser->getUser()->isUsernameGenerated()): ?>
        , <?php echo $clientUser->getUser()->getFriendlyUsername() ?>
    <?php endif ?>
    ) 
    </a>
    <ul class="tags float-right">
        <?php $lastLogin = $clientUser->getUser()->getLastLogin(); ?>
        <li class="tag-time" title="<?php echo __('last successful login'); ?>"><?php echo allTimeBack($clientUser->getUser()->getLastLogin(), __('Never')); ?></li>
    	<li class="tag-user" title="<?php echo __('e-mail'); ?><?php echo !$clientUser->getUser()->isUsernameGenerated() ? ', '.__('username') : '' ?>">
    	<?php echo $clientUser->getUser()->getEmailAddress() ?>
    	<?php if(!$clientUser->getUser()->isUsernameGenerated()): ?>
    	    , <?php echo $clientUser->getUser()->getFriendlyUsername() ?>
    	<?php endif ?>
    	</li>
    	<li class="tag-calendar-month" title="<?php echo __('created at'); ?>"><?php echo allTimeBack($clientUser->getUser()->getCreatedAt()); ?></li>
    </ul>
    
 </li>
<?php endforeach ?>
</ul>