<?php include_partial('global/alertModalConfirm') ?>

<?php if($sf_user->hasFlash('info_data_unfinished')): ?>
    <?php include_partial('invoice/alertUnfinishedOrders', array('invoiceItems' => $invoiceItems)) ?>
<?php endif;?>

<div id="send_invoice_form" style="display: none; text-align: left;">
    <div class="send-errors"></div>
    <br/>
    <form action="<?php echo url_for("invoiceSendEmailInvoice"); ?>" method="post" id="form_send">
        <?php echo $sendInvoiceForm->renderHiddenFields(); ?>
        <b><?php echo $sendInvoiceForm["to"]->renderLabel(); ?></b><br/>
        <?php echo $sendInvoiceForm["to"]->render(); ?>
        <?php echo $sendInvoiceForm["to"]->renderError(); ?>
        <br /><br />
        <b><?php echo $sendInvoiceForm["subject"]->renderLabel(); ?></b><br/>
        <?php echo $sendInvoiceForm["subject"]->getWidget()->render('send_invoice[subject]',
            __('Invoice').': '.$invoiceObj->getName()); ?>
        <?php echo $sendInvoiceForm["subject"]->renderError(); ?>
        <br /><br />
        <h3><?php echo __("Email preview"); ?></h3><br/>
        <?php echo $sendInvoiceForm["invoice"]->render(); ?>
        <?php echo $sendInvoiceForm["invoice"]->renderError(); ?>
        <br />
    </form>
</div>

<form id="invoice-form" action="<?php echo ($form->isNew()) 
        ? url_for('clientInvoiceAdd', $invoiceObj->getClient()) 
        : url_for('clientInvoiceEdit', 
                array('client_id' => $invoiceObj->getClient()->getId(), 
                    'id' => $invoiceObj->getId())); ?>" method="post" accept-charset="utf-8">
    <?php include_partial('invoice/formActions', 
            array('route' => '@clientListEdit?id='.$invoiceObj->getClient()->getId().'#&tab-invoices',                
                'routeBack' => $routeBack,
                'withSave' => $form->getObject()->canEdit($sf_user),
                'form' => $form)); ?>
    <?php include_partial('dashboard/message'); ?>
    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content form no_top_padding">
                    <h1>
                        <?php if($form->isNew()): ?>
                        <?php echo __('New invoice'); ?>                   
                        <?php else: ?>
                            <?php echo __($form->getObject()->getType()); ?>
                            <?php echo $form->getObject()->getName(); ?>
                        <?php endif; ?>
                        <?php if(!$form->getObject()->canEdit($sf_user)): ?>
                        (<?php echo __('read only') ?>)                   
                        <?php endif; ?>            
                    </h1>
                    <div></div>
                    <?php include_partial('client/edit/tabList', array(
                        'clientObj' => $invoiceObj->getClient(),
                        'currentTab' => 'tab-invoices',
                        'defaultDisableAdvancedAccountancy' => $defaultDisableAdvancedAccountancy)); ?>

                    <div class="invoice-error">
                        <?php echo include_partial('invoice/formWarningMessage', array('form' => $form)); ?>
                    </div>
                        
                    <?php echo $form->renderHiddenFields(); ?>
                    <div style="clear: both; height: 10px;"></div>

                    <!-- form fields -->
                    <div class="columns">
                        <div class="colx3-left">
                            <div class="columns">                               
                                <p class="required">
                                    <?php echo $form['full_address']->renderLabel(); ?>
                                    <?php echo $form['full_address']->render(); ?>
                                </p>
                              
                                <table>
                                    <tr>
                                        <td>
                                            <?php echo __('Example'); ?>:
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <?php echo __('Name'); ?></br>
                                            <?php echo __('Address'); ?></br>
                                            xx-xxx <?php echo __('City'); ?></br>
                                            <?php echo __('Belgium'); ?> (<?php echo __('when other country'); ?>)
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <table class="invoice_table">
                                <tr>
                                    <td class="width33"> 
                                        <p>
                                            <?php echo $form['client_tax_id']->renderLabel(); ?> 
                                        </p>    
                                    </td>
                                    <td class="vertical-align-middle">
                                        <?php echo $form['client_tax_id']->render(); ?> 
                                    </td>
                                </tr>
                            </table>           
                        </div>
                        <div class="colx3-center">
                            <table class="invoice_table">
                                <tr>
                                    <td class="width33">
                                        <?php echo $form['invoice_type_name']->renderLabel(); ?>
                                    </td>
                                    <td colspan="2" class="vertical-align-middle">
                                        <?php echo $form['invoice_type_name']->render(); ?>
                                    </td>
                                </tr>

                                <?php if(!$form->isNew()): ?>
                                <tr>
                                    <td class="width33">        
                                        <label><?php echo __('Status'); ?></label>                                                                                     
                                    </td>
                                    <td class="width33">
                                            <?php if($form->getObject()->getInvoiceStatus()->getName() == InvoiceStatusTable::$paidFromReceipt): ?>
                                                <?php echo __('paid from receipt'); ?>
                                                <?php echo link_to($form->getObject()->getInvoice(), 'clientInvoiceEdit', $form->getObject()->getInvoice()); ?>
                                            <?php elseif($form->getObject()->getInvoiceStatus()->getName() == InvoiceStatusTable::$paidFromPreliminaryInvoice): ?>
                                                <?php echo __('paid from preliminary invoice'); ?>:<br/>
                                                <?php echo link_to($form->getObject()->getInvoice(), 'clientInvoiceEdit', $form->getObject()->getInvoice()); ?>
                                            <?php elseif($form->getObject()->getInvoiceStatus()->getName() == InvoiceStatusTable::$paidFromInvoice): ?>
                                                <?php echo __('paid from invoice'); ?>:<br/>
                                                <?php foreach($form->getObject()->getChildren() as $child): ?>
                                                    <?php echo link_to($child, 'clientInvoiceEdit', $child); ?>
                                                <?php endforeach;?>
                                            <?php else: ?>
                                                <?php echo __($form->getObject()->getInvoiceStatus()->getName()); ?>
                                                <?php if($form->getObject()->hasChildren()): ?>
                                                    <br/>
                                                    <?php foreach($form->getObject()->getChildren() as $child): ?>
                                                        <?php if($child->getInvoiceTypeName() == InvoiceTypeTable::$RECEIPT): ?>
                                                            <?php echo __('created receipt'); ?>:
                                                        <?php elseif($child->getInvoiceTypeName() == InvoiceTypeTable::$PRELIMINARY_INVOICE): ?>
                                                            <?php echo __('created preliminary invoice'); ?>:
                                                        <?php elseif($child->getInvoiceTypeName() == InvoiceTypeTable::$INVOICE): ?>
                                                            <?php echo __('created invoice'); ?>:
                                                        <?php endif; ?>
                                                        <?php echo link_to($child, 'clientInvoiceEdit', $child); ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                    </td>
                                    <td class="width33">
                                        <p class="required">
                                            <?php foreach($form->getObject()->getAvailableStatuses() as $status): ?>
                                                <?php $options = array('method' => 'put') ?>
                                                <?php $color = 'gray'; ?>
                                                <?php if($status->getName() == 'unpaid' && $form->getObject()->canUnPay($sf_user)): ?>
                                                    <?php echo link_to('<span class="button '.$color.'" style="margin:3px;">'.__($status->getTitle()).'</span>',
                                                        'invoiceChangeStatus',
                                                        array('id' => $form->getObject()->getId(),
                                                            'invoice_new_status' => $status->getName()),
                                                        $options); ?>
                                                <?php endif;?>

                                                <?php if($form->getObject()->canEdit($sf_user)): ?>
                                                    <?php
                                                        if($status->getName() == 'canceled')
                                                        {
                                                            $options['id'] = 'button-confirm';
                                                            $options['onclick'] = 'if(!alertModalConfirm(\''.__($status->getTitle()).'\', \'<h3>'.__('Are you sure you want to cancel this invoice?').'</h3>\', 500, this.id, true)) return false;';
                                                            $color = 'red';
                                                        }
                                                    ?>
                                                    <?php if(!in_array($status->getName(), array('paid', 'unpaid'))): ?>
                                                        <?php echo link_to('<span class="button '.$color.'" style="margin:3px;">'.__($status->getTitle()).'</span>',
                                                            'invoiceChangeStatus',
                                                            array('id' => $form->getObject()->getId(),
                                                                'invoice_new_status' => $status->getName()),
                                                            $options); ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </p>
                                    </td>
                                </tr>
                                <?php endif; ?>

                                <?php if(!$form->isNew()): ?>
                                <tr>
                                    <td class="width33">
                                        <label><?php echo __('Date of issue'); ?></label>    
                                    </td>
                                    <td class="width33">
                                        <?php echo $form->getObject()->getSellAt(); ?>
                                    </td>
                                    <td class="width33"></td>
                                </tr>
                                <?php endif; ?>

                                <tr>
                                    <td class="width33">
                                        <p class="required">
                                            <?php echo $form['payment_date_at']->renderLabel(); ?>                               
                                        </p>  
                                    </td>
                                    <td class="width33">
                                        <span class="input-type-text margin-right relative date-span" id="payment-date-at-span">
                                            <?php echo $form['payment_date_at']->render(); ?>
                                        </span>                       
                                    </td>   
                                    <td class="width33"></td>
                                </tr>

                                <!-- payed field -->
                                <tr>
                                    <td class="width33">
                                        <?php echo $form['payed_at']->renderLabel(); ?> 
                                    </td>
                                    <td class="width33">
                                    <span class="input-type-text margin-right relative">
                                        <?php echo $form['payed_at']->render() ?>
                                    </span>
                                    </td>
                                    <td class="width33">
                                        <?php  if(!$form->isNew()):
                                                $status = $form->getObject()->getPaidStatus();
                                                if(($status && $status->getName() == 'paid')): ?>
                                                    <button id="invoice-pay-status-a" type="button"<?php if(isset($allowForcePayment) && $allowForcePayment === true): ?> class="force"<?php endif; ?>>
                                                        <?php echo __('Mark as paid'); ?>
                                                        <?php if(isset($allowForcePayment) && $allowForcePayment === true): ?>
                                                            - <?php echo mb_strtoupper(__('force')); ?>
                                                        <?php endif; ?>
                                                    </button>
                                                <?php endif;?>
                                        <?php endif; ?>
                                    </td>
                                </tr>

                                <!-- service_realized_at field -->
                                <tr>
                                    <td class="width33">
                                        <?php echo $form['service_realized_at']->renderLabel(); ?>
                                    </td>
                                    <td class="width33">
                                    <span class="input-type-text margin-right relative" id="service-realization-date">
                                        <?php echo $form['service_realized_at']->render() ?>
                                    </span>
                                    </td>
                                </tr>

                                <?php if($form->getObject()->getPrintAt() != null || $form->getObject()->getFiscalPrintNote() != null): ?>
                                <!-- print date information -->
                                <tr>
                                    <td class="width33">
                                        <label><?php echo __('Date of print'); ?></label>
                                    </td>
                                    <td class="width33" colspan="2">
                                        <span class="margin-right relative">
                                            <?php if($form->getObject()->getFiscalPrintNote() != null): ?>
                                                <ul class="message error no-margin">
                                                    <li>
                                                        <?php echo __('An error occurred'); ?>
                                                        <br/>
                                                        <?php echo $form->getObject()->getFiscalPrintNote(); ?>
                                                    </li>
                                                </ul>
                                            <?php else: ?>
                                                <?php echo $form->getObject()->getPrintAt(); ?>
                                            <?php endif; ?>
                                        </span>
                                    </td>
                                </tr>
                                <?php endif; ?>
                            </table>
                        </div>
                        <div class="colx3-right">

                            <table class="invoice_table">
                                <tr>
                                    <td class="width33">
                                        <p>
                                            <?php echo $form['payment_id']->renderLabel(); ?>
                                        </p>
                                    </td>
                                    <td class="vertical-align-middle">
                                        <?php echo $form['payment_id']->render(); ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="width33">
                                        <p>
                                            <?php echo $form['currency']->renderLabel(); ?>
                                        </p>
                                    </td>
                                    <td class="vertical-align-middle">
                                        <?php echo $form['currency']->render(); ?>
                                    </td>
                                </tr>
                            </table>

                            <div style="width: 50%; margin: 50px auto 0 auto;">
                                <table class="total_calculation">
                                    <tr>
                                        <td><label><?php echo __('Total net price'); ?>:</label></td>
                                        <td class="total_calc_td invoice_price_net_total"></td>        
                                    </tr>   
                                    <tr>
                                        <td><label><?php echo __('VAT'); ?>:</label></td>
                                        <td class="total_calc_td invoice_price_tax_total"></td>
                                    </tr>
                                    <tr>
                                        <td><label><?php echo __('Total gross price'); ?>:</label></td>
                                        <td class="total_calc_td invoice_price_gross_total"></td>
                                    </tr>         
                                    <tr>
                                        <td><label><?php echo __('Loyalty points'); ?>:</label></td>
                                        <td class="total_calc_td">
                                            <?php echo $form->getObject()->getTotalLoyaltyPoints(); ?> 
                                        </td>
                                    </tr> 
                                </table>
                            </div>
                        </div>
                    </div>
          
                    <p align="right">
                        <!-- changelog and print -->
                        <?php if(!$form->isNew()): ?>
                        <button type="button" id="changelog" class="grey"><?php echo __('Changelog') ?></button>
                        <?php endif; ?>
                        
                        <?php if($form->getObject()->canCorrectionAction()): ?>
                        <button id="invoice-correction-a" class="" type="button"><?php echo __('Invoice correction') ?></button>
                        <?php endif; ?>
                            
                        <!-- generate from preliminary -->
                        <?php if($form->getObject()->canCloneAction()): ?>
                        <button id="invoice-clone-a" class="" type="button"><?php echo __('Create invoice from the Preliminary Invoice') ?></button>
                        <?php endif; ?>
                        
                        <!-- create invoice from receipt -->
                        <?php if($form->getObject()->canInvoiceFromReceiptAction()): ?>
                        <button id="invoice-from-receipt-a" 
                            <?php if($form->getObject()->canChangeReceiptToInvoice()){ ?>
                                class="grey"
                            <?php }else{ ?>
                                disabled="disabled"
                            <?php } ?> 
                            type="button"><?php echo __('Copy to Invoice') ?></button>
                        <?php endif; ?>
                    </p>

                    <div style="clear: both"></div>  

                    <div>
                        <!-- invoice items -->
                        <?php include_partial('invoice/invoiceItemTable', array('form' => $form)); ?>
                        <p> 
                            <?php echo $form['notice']->renderLabel(); ?> 
                            <?php echo $form['notice']; ?>
                            <?php echo $form['notice']->renderError(); ?>
                        </p>
                        <p>
                            <?php echo $form['internal_note']->renderLabel(); ?>
                            <?php echo $form['internal_note']; ?>
                            <?php echo $form['internal_note']->renderError(); ?>
                        </p>
                    </div>          
                </div>
            </div>
        </section>
    </article>
</form>
<div id="changelog-content"></div>

<!-- sets slot -->
<?php extendSlot('actionJavascript'); ?>

<?php echo include_partial('order/modalLoader') ?>

<script type="text/javascript">
var allowZeroedPrices = <?= $allowZeroedPrices; ?>;
var invoiceIsPaid = <?= $isPaid; ?>;
var isCorrection = '<?php echo $form->getObject()->isCorrection(); ?>';
if(isCorrection)
{
    var parentNetAmount = '<?php echo $form->getObject()->getInvoice()->getPriceNet(); ?>';
    var parentVatAmount = '<?php echo $form->getObject()->getInvoice()->getPriceTax(); ?>';
    var parentGrossAmount = '<?php echo $form->getObject()->getInvoice()->getPriceGross(); ?>';
    var parentItemCount = '<?php echo $form->getObject()->getInvoice()->getInvoiceItems()->count(); ?>';
}

function saveInvoice(callback, sync, type)
{       
    callback = typeof callback !== 'undefined' ? callback : false;
    sync = typeof sync !== 'undefined' ? sync : false;
    
    var form = $('#invoice-form');
    var data = form.serializeArray();
    var saveUrl = '<?php echo url_for('reqInvoiceSave'); ?>';

    $.ajax(
    {
        url: saveUrl,
        type: 'POST',
        dataType: 'json',
        data: data,
        async: sync,  
        success: function(result)
        {
            if(result.status != 'success')
            {
                $('.invoice-error').html(renderError(result.errors));
                
                // hides loaders
                buttonLoader($('.invoice-print-a'), false);
                buttonLoader($('#invoice-pay-status-a'), false);
                buttonLoader($('#invoice-correction-a'), false);
            }
            else
            {
                // callback section
                if(callback)
                {
                    callback(type);
                }
            }    
        }
    });
}

/**
 * Opens new window with invoice to print.
 */
function printInvoice(type)
{
    var typeLabel = (type != undefined) ? '?type=' + type : '';

    // problem with _blank target
    window.open('<?php echo url_for('invoicePrint', $form->getObject()); ?>' + typeLabel);
}

/**
 * Creates correction invoice.
 */
function correctionInvoice()
{
    // problem with _blank target
    window.open('<?php echo url_for('invoiceClone', 
            array(
                'id' => $form->getObject()->getId(), 
                'status' => InvoiceTypeTable::$CORRECTION_INVOICE)); ?>', 
                        '_parent'); 
}

/**
 * Creates invoice from preliminary.
 */
function cloneInvoice()
{
    // problem with _blank target
    window.open('<?php echo url_for('invoiceClone', 
            array(
                'id' => $form->getObject()->getId(), 
                'status' => InvoiceTypeTable::$INVOICE))?>', 
                        '_parent'); 
}

/**
 * Creates invoice from receipt invoice.
 */
function invoiceFromReceipt()
{
    // problem with _blank target
    window.open('<?php echo url_for('invoiceFromReceipt', 
            $form->getObject()); ?>', 
            '_parent'); 
}

/**
 * Add printer to fiscalizing
 */
function fiscalPrintReceipt()
{
    $.ajax(
        {
            url: '<?php echo url_for('invoiceFiscalPrint', $form->getObject()); ?>',
            type: 'POST',
            async: true,
            success: function(result)
            {
                window.location.reload();
            }
        });


}

/**
 * Changes invoice status to paid.
 */
function payInvoice()
{
    // problem with _blank target
    window.open('<?php echo url_for('invoiceChangeStatus', 
            array('id' => $form->getObject()->getId(),             
                'invoice_new_status' => InvoiceStatusTable::$paid)); ?>',
                        '_parent'); 
}

/**
 * Changes invoice status to paid.
 */
function payInvoiceForce()
{
    // problem with _blank target
    window.open('<?php echo url_for('invoiceChangeStatus',
            array('id' => $form->getObject()->getId(),
                'invoice_new_status' => InvoiceStatusTable::$paid, 'force' => true)); ?>',
                        '_parent');
}


/**
 * Shows loader on button.
 */
function buttonLoader(selector, turnOn)
{
    if(turnOn)
    {
        $(selector).prepend('<?php echo image_tag('loader.gif',
                        array('width' => 18,
                            'height' => 18,
                            'style' => 'margin-right: 5px;')); ?>');
    }
    else
    {
        $(selector).find('img').remove();
    }
}

function sendEmail()
{
    $(".modal-content .send-errors").html("");

    var form = $(".modal-content #form_send");
    var data = form.serialize();
    var uri =  form.attr("action");
    var method =  form.attr("method");
    $.ajax(
        {
            type: method,
            url: uri,
            data: data,
            dataType: "json",
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert("<?php echo __("There was error. Please try again later.") ?>");
            },
            success: function(data)
            {
                modalFooterLoader(false);
                if(!data.result.success)
                {
                    var errors = data.result;

                    $(".modal-content .send-errors").html(renderError(errors));
                }
                else
                {
                    displayAlert($(".modal-content #send_invoice_form"), "success", data.result.message);
                    $("#modal .block-footer button:first").remove();
                }
            }
        });
}

/**
 * Adds new item.
 */
function addNewItem()
{
    var selector = $(".invoice_item_row:hidden").first();
    
    // if no hidden invoice item then save first
    if(selector.length == 0)
    {
        $("#saveAndReturn").click();
    }

    selector.find(".invoice_item_valid").val(1);

    if(isCorrection)
    {
        selector.find(".item_description").val('<?php echo __('Added'); ?>: ');
    }

    if($(".invoice_item_row:hidden").length != $(".invoice_item_row").length){
       selector.find(".item_tax").val($(".item_tax").first().val());
    }

    selector.first().show();
}

jQuery(document).ready(function($) 
{       
    <?php if(!$form->getObject()->canEdit($sf_user)
            || $form->getObject()->isPaid()): ?>
    disableInvoiceFormInputs();
    <?php endif; ?>

    $('#changelog').click(function()
    {
        $.modal({
            content: $("#changelog-content"),
            title: "<?php echo __('Changelog'); ?>",
            maxWidth: 700,
            maxHeight: 350,
            resizable: false,
            onOpen: function()
            {
                $.ajax({
                url: "<?php echo url_for('@invoiceChangelog?id='.$form->getObject()->getId()) ?>",
                success: function(result)
                {
                    $('.modal-content #changelog-content').html(result);
                },
                error: function(xhr, ajaxOptions, thrownError)
                {
                        $('.modal-content #changelog-content').html('<?php echo __('Error while getting changelog for invoice.') ?>');
                }});
                $('.modal-content #changelog-content').html('');
            },
            buttons:
            {
                '<?php echo __('Close') ?>': function(win) 
                {
                    win.closeModal();
                }
            }}); 
        }
    );



    $("#send-invoice").click(function()
    {
        modalEmailForm(
            $("#send_invoice_form"),
            "<?php echo __("Send Invoice"); ?>",
            "email_invoice_textarea_modal",
            function() {
                $('.modal-content #email_invoice_textarea').attr('id', 'email_invoice_textarea_modal');
            },
            function() {
                sendEmail();
            },
            "<?php echo __("Send"); ?>",
            "<?php echo __("Close"); ?>"
        );
    });

    $('.invoice-print-a, .invoice-duplicate-a').click(function(e)
    { 
        e.preventDefault();
        invoiceAction(printInvoice, $(this).attr('id'));
    });

    $('.fiscal-print-a').click(function(e)
    {
        e.preventDefault();
        invoiceAction(fiscalPrintReceipt, $(this).attr('id'));
    });
    
    $('#invoice-clone-a').click(function(e)
    { 
        e.preventDefault();
        invoiceAction(cloneInvoice, $(this).attr('id'));
    });

    function invoiceAction(calbackFunction, type)
    {
        <?php if($form->getObject()->canEdit($sf_user)): ?>
        buttonLoader(this, true);
        saveInvoice(calbackFunction, false, type);
        <?php else: ?>
        calbackFunction(type);
        <?php endif; ?>
    }
    
    $('#invoice-correction-a').click(function(e)
    { 
        e.preventDefault();

        invoiceAction(correctionInvoice);
    });
    
    $('#invoice-from-receipt-a').click(function(e)
    { 
        e.preventDefault();

        invoiceAction(invoiceFromReceipt);
    });
    
    $('#invoice-pay-status-a').click(function(e)
    { 
        e.preventDefault();

        var action = ($(this).hasClass('force')) ? payInvoiceForce : payInvoice;

        invoiceAction(action);
    });

    // add invoice item
    $("#item-add-button").click(function()
    {
        addNewItem();
        recalculateNoItems();
        renderCalculations();
    });

    // delete invoice item
    $(".item-delete-button").click(function()
    {
        var selector = $(this).parents("tr");
        var itemRowNr = selector.index() + 1;

        // if invoice is correction and deleted row existed before then show 'Resignation'
        // and do not remove this row
        if(isCorrection && !selector.hasClass("added-item-row") && itemRowNr <= parentItemCount)
        {  
            // new price (minus)
            var newPrice = parseFloat(selector.find(".item_price_net").val() * -1).toFixed(2);
            selector.find(".item_price_net").val(newPrice);

            // item description
            var newDesc = '<?php echo __('Resignation'); ?>: ' + selector.find(".item_description").val();
            selector.find(".item_description").val(newDesc);
        }
        else
        {
            selector.find(".invoice_item_valid").val(0);
            selector.hide();
        }
        
        recalculateNoItems();
        
        if($(".invoice_item_row:visible").length == 0)
        {
            $(".total_calculation").hide();
        }

        // renders calculations
        renderCalculations();
    });
});
</script>
<?php end_slot(); ?>
