<form id="dialog-edit-client-general-form" action="<?php echo url_for('reqEditClientGeneralForm', 
        array('model' => 'Client', 
            'type' => 'edit')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('General information'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                      
                <p class="required">
                    <?php echo $form['fullname']->renderLabel(); ?>
                    <?php echo $form['fullname']->render(); ?>
                </p>  
                <p class="required">
                    <?php echo $form['country']->renderLabel(); ?>
                    <?php echo $form['country']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['street']->renderLabel(); ?>
                    <?php echo $form['street']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['postcodeAndCity']->renderLabel(); ?>
                    <?php echo $form['postcodeAndCity']->render(); ?>
                <p>               
                <p>
                    <?php echo $form['tax_id']->renderLabel(); ?>
                    <?php echo $form['tax_id']->render(); ?>
                <p>
                <p>
                    <?php echo $form['account']->renderLabel(); ?>
                    <?php echo $form['account']->render(); ?>
                <p>
                <p>
                    <?php echo $form['wnt_ue']->renderLabel(); ?>
                    <?php echo $form['wnt_ue']->render(); ?>
                <p>
            </div>
        </div>
    </div>
</form>

