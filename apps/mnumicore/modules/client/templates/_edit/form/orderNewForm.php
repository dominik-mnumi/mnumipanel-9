<form id="dialog-new-order-form" action="" enctype="multipart/form-data" method="post" style="display: none;">
    <div>
        <p class="validateTips"></p>
        <div class="block-border">
            <div class="block-content no_border form no-padding">
                <?php if($creatingDisabled): ?>
                <h2><?php echo __("Orders creation has been suspended"); ?></h2>
                <p><?php echo __("Please contact with your administrator and check")?> <?php echo link_to('license page', '@license'); ?>.</p>
                <?php elseif($clientObj->getClientUsers()->count()): ?>
                <!-- user select -->
                <div class="columns">
                    <div class="colx3-left">
                        <p class="required">
                            <?php echo $form['user_id']->renderLabel(); ?>
                        </p>
                    </div>
                    
                    <div class="colx3-right-double">
                        <p>
                            <?php echo $form['user_id']->render(); ?>
                        </p>
                    </div>
                </div>  
                
                <!-- category product tree -->
                <?php if($rootObj->getNode()->isRoot()): ?>
                <ul class="collapsible-list with-bg">
                    <!-- main categories -->
                    <?php foreach($rootObj->getNode()->getChildren() as $node): ?>
                    <?php include_partial('client/edit/categoryItem', array('node' => $node,
                                                                            'clientObj' => $clientObj)); ?>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>   
                
                <!-- if no client users -->
                <?php else: ?>
                <h2><?php echo __("To add an order for a customer, it must have a user assigned.")?></h2>
                <?php endif; ?>
            </div>
        </div>
    </div>
</form>

