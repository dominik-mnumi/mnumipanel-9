<div title="<?php echo __('Delivery address'); ?>">
    <p class="validateTips"></p>
    <?php echo $form['_csrf_token']->render(); ?>
    <?php echo $form['id']->render(); ?>
    <div class="block-border">
        <div class="block-content form no_border">                      
            <p class="required">
                <?php echo $form['fullname']->renderLabel(); ?>
                <?php echo $form['fullname']->render(); ?>
            </p>  
            <p class="required">
                <?php echo $form['country']->renderLabel(); ?>
                <?php echo $form['country']->render(); ?>
            </p>    
            <p class="required">
                <?php echo $form['postcodeAndCity']->renderLabel(); ?>
                <?php echo $form['postcodeAndCity']->render(); ?>
            </p>    
            <p class="required">
                <?php echo $form['street']->renderLabel(); ?>
                <?php echo $form['street']->render(); ?>
            </p>  
        </div>
    </div>
</div>


