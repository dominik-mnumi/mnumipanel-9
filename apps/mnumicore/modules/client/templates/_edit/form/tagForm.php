<form id="dialog-edit-client-tag-form" action="<?php echo url_for('reqEditClientTagForm', 
        array('model' => 'ClientTag', 
            'type' => 'new')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('Tags'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                      
                <p>                   
                    <?php echo $form['tagContainer']->renderLabel(); ?>
                    <ul id="client_tags"></ul>
                    <?php echo $form['tagContainer']->render(); ?>
                </p>                                                                  
            </div>
        </div>
    </div>
</form>

