<div class="block-controls">					
    <ul class="controls-tabs js-tabs same-height with-children-tip">
        <li title="<?php echo __('Customer edit'); ?>">
            <a href="<?php echo url_for('clientListEdit', $clientObj); ?>#tab-edit" title="<?php echo __('Customer edit'); ?>">
                <img src="/images/icons/order/user.png" width="24" height="24">                               
            </a>
        </li>
        <li title="<?php echo __('Customer orders'); ?>">
            <a href="<?php echo url_for('clientListEdit', $clientObj); ?>#&tab-orders" title="<?php echo __('Customer orders'); ?>">
                <img src="/images/icons/order/order-user.png" width="24" height="24">
            </a>
        </li>
        <li class="current" title="<?php echo __('Customer invoices'); ?>">
            <a href="<?php echo url_for('clientListEdit', $clientObj); ?>#tab-invoices" title="<?php echo __('Customer invoices'); ?>">
                <img src="/images/icons/order/invoice-user.png" width="24" height="24">
            </a>
        </li>
    </ul>
</div>