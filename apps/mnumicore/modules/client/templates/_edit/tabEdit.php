<?php use_helper('permission'); ?>
<?php //Please do not change ids that match /*_container/ ?>
<div id="tab-edit" class="with-margin">
    <fieldset class="no-padding-bottom">
        <legend><?php echo __('General information'); ?></legend>
        <div class="columns" id="edit_client_general_information_container">
            <div class="colx3-left data_container">
                <div class="text_container big_text"><?php echo $clientObj->getFullname(); ?></div>
                <div class="text_container"><?php echo $sf_user->getCountry($clientObj->getCountry()); ?></div>
                <div class="text_container"><?php echo $clientObj->getStreet(); ?></div>
                <div class="text_container"><?php echo $clientObj->getPostcode(); ?> <?php echo $clientObj->getCity(); ?></div>
                
                <?php if($clientObj->getTaxId()): ?>
                <div class="text_container"><?php echo __('Tax ID'); ?>: <?php echo $clientObj->getTaxId(); ?></div>
                <?php endif; ?>
                
                <?php if($clientObj->getAccount()): ?>
                <div class="text_container"><?php echo __('Bank account'); ?>: <?php echo $clientObj->getAccount(); ?></div>
                <?php endif; ?>

                <?php if($clientObj->getWntUe()): ?>
                <div class="text_container ueflag"><?php echo __('Intra-Community acquisition of goods'); ?></div>
                <?php endif; ?>

            </div>    
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientGeneralInformationEdit)): ?>
            <div class="colx3-right">                
                <button type="button" id="dialog-edit-client-general-click"><?php echo __('Change'); ?></button>                             
            </div>
            <?php endif ?>
        </div>
    </fieldset>
    
    <fieldset>
        <legend><?php echo __('Credits'); ?></legend>
        <div class="columns" id="edit_client_credits_container">
            <div class="colx3-left data_container">
                <div class="text_container">
                    <?php echo $clientObj->getPricelist()->getName(); ?>
                    (<?=$clientObj->getPricelist()->getCurrency()->getCode();?>)
                </div>
                &nbsp;
                <div class="text_container float-left">
                    <?php echo __('Allowed debt') ?>: <?php echo $sf_user->formatCurrency($clientObj->getCreditLimitAmount()); ?>
                </div>
                <div class="text_container float-left last_text">
                    <?php echo __('Balance'); ?>:&nbsp;<?php echo $sf_user->formatCurrency($clientObj->getBalance()); ?>
                </div>  
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientCreditEdit)): ?>
            <div class="colx3-right">
                <button type="button" id="dialog-edit-client-credit-click"><?php echo __('Change'); ?></button>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Attachments'); ?></legend>
        <div class="columns" id="edit_client_attachments_container">
            <div class="colx3-left data_container">
                <div class="text_container">
                    <?php $attachmentCount = $clientObj->getClientAttachments()->count(); ?>
                    <?php if(0 < $attachmentCount): ?>                    
                    <div class="text_container">
                        <?php foreach($clientObj->getClientAttachments() as $key => $rec): ?>
                        <a href="<?php echo url_for('@clientAttachmentDownload?id='.$rec->getId()); ?>">
                            <?php echo $rec->getFilename(); ?>                          
                        </a> 
                        <?php if($attachmentCount - 1 != $key): ?>
                        ,
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </div>                    
                    <?php else: ?>
                    <?php echo __('No attachments'); ?>
                    <?php endif; ?>
                </div>                
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientAttachmentEdit)): ?>
            <div class="colx3-right">
                <a href="<?php echo url_for('attachmentList', $clientObj); ?>">
                    <button type="button"><?php echo __('Change'); ?></button>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Deliveries'); ?></legend>
        <div class="columns" id="edit_client_deliveries_container">
            <div class="colx3-left data_container">
                <div class="text_container">
                    <?php if(0 < $clientObj->getClientAddresses()->count()): ?>
                    <?php foreach($clientObj->getClientAddresses() as $rec): ?>
                    <div class="text_container">
                        <?php echo $rec->getFullname(); ?>: <?php echo $rec->getPostcode(); ?> <?php echo $rec->getCity(); ?>, <?php echo $rec->getStreet(); ?>                         
                    </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                    <?php echo __('No deliveries'); ?>
                    <?php endif; ?>
                </div>                
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientDeliveryEdit)): ?>
            <div class="colx3-right">
                <a href="<?php echo url_for('deliveryList', $clientObj); ?>">
                    <button type="button"><?php echo __('Change'); ?></button>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Tags'); ?></legend>
        <div class="columns">
            <div class="colx3-left data_container" id="edit_client_tags_container">
                <div class="text_container">
                    <?php if(0 != $clientObj->getClientTags()->count()): ?>
                    <?php foreach($clientObj->getClientTags() as $rec): ?>
                    <?php $tagArray[] = $rec->getName(); ?>              
                    <?php endforeach; ?>
                    <?php echo implode(', ', $tagArray); ?>
                    <?php else: ?>
                    <?php echo __('No tags'); ?>
                    <?php endif; ?>
                </div>                
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientTagEdit)): ?>
            <div class="colx3-right">
                <button id="dialog-edit-client-tag-click" type="button"><?php echo __('Change'); ?></button>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Users'); ?></legend>
        <div class="columns">
            <div class="colx3-left data_container">
                <div class="text_container">
                    <?php if(0 < $clientObj->getClientUsers()->count()): ?>
                    
                    <?php $permissionArr = array(ClientUserPermissionTable::$admin,
                        ClientUserPermissionTable::$user); ?>
                    <?php foreach($permissionArr as $permission): ?>
                    <!-- permissions -->
                    <?php $clientUserColl = $clientObj->getClientUsersByPermission($permission); ?>
                    <?php if($clientUserColl->count()): ?> 
                    <div class="text_container client-users-container">
                        <?php echo __($clientUserColl->getFirst()->getClientUserPermission()->getDescription()); ?>:<br /><br /> 

                        <?php foreach($clientUserColl as $rec): ?>  
                        <span>- 
                            <a href="<?php echo url_for('@userListEdit?id='.$rec->getUser()->getId()); ?>">
                                <?php echo $rec->getUser()->__toString(); ?>                   
                            </a>: <?php echo $rec->getUser()->getContactsString(); ?> 
                        </span> 
                        <br />
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>    
                    <?php endforeach; ?>
           
                    <?php else: ?>
                    <?php echo __('No users'); ?>
                    <?php endif; ?>
                </div>                
            </div>
            <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientTraderEdit)): ?>
            <div class="colx3-right">
                <a href="<?php echo url_for('clientUserList', $clientObj); ?>">
                    <button type="button"><?php echo __('Change'); ?></button>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __('Additional information'); ?></legend>
        <div class="columns">
            <div class="colx3-left data_container">
                <?php if('' != $clientObj->getLastOrderId()): ?>
                <div class="text_container">
                    <?php echo __('Last order'); ?>:&nbsp;<?php echo $clientObj->getLastOrderId(); ?>                 
                </div>  
                <?php endif; ?>
                <?php $orderSummary = $clientObj->getSummaryOrdersInRealisation(); ?>
                <?php if($orderSummary->getCount() > 0): ?>
                    <div class="text_container">
                        <?php echo __('In realization'); ?>:
                        <?php echo __plural('1 order', '@count orders', array('@count' => $orderSummary->getCount())); ?>,
                        <?php echo __('amount'); ?>
                        <?php echo $sf_user->formatCurrency($orderSummary->getAmount()); ?> <?php echo __('net'); ?>
                    </div>
                <?php endif; ?>
                <?php $invoiceSummary = $clientObj->getSummaryNonPaidInvoices(); ?>
                <?php if($invoiceSummary->getCount() > 0): ?>
                    <div class="text_container">
                        <?php echo __('Unpaid invoices'); ?>:
                        <?php echo format_number_choice(
                            '[0]No invoices|[1]One invoice|(1,+Inf]%count% invoices',
                            array('%count%' => $invoiceSummary->getCount()),
                            $invoiceSummary->getCount()
                        ); ?>

                        <?php echo __('amount'); ?>
                        <?php echo $sf_user->formatCurrency($invoiceSummary->getAmount()); ?> <?php echo __('net'); ?>
                    </div>
                <?php endif; ?>
                <div class="text_container">
                    <?php echo __('Customer created'); ?>:&nbsp;<?php echo $clientObj->getCreatedAt(); ?>
                </div>
            </div>
            <div class="colx3-right">
                
            </div>
        </div>
    </fieldset>
</div>
