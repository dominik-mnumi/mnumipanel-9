<?php if($node->getProductsCount(true)) : ?>
<li class="closed">
    <?php if(1 == $node->getNode()->getAncestors()->count()): ?>
    <b class="toggle"></b>
    <?php endif; ?>
    <span><?php echo $node->getName(); ?></span>
    <?php if($node->getNode()->hasChildren()): ?>
    <ul class="with-icon">
        <?php foreach($node->getNode()->getChildren() as $node): ?>
        <?php include_partial('client/edit/categoryItem', array('node' => $node, 'clientObj' => $clientObj)); ?>
        <?php endforeach; ?>    

        <!-- display products in current category -->
        <?php $parentNodeObj = $node->getNode()->getParent(); ?>
        <?php foreach($parentNodeObj->getAllActiveProducts(false) as $node): ?>
        <li>
            <a class="client-new-order" data-client="<?php echo $clientObj->getId(); ?>" data-product="<?php echo $node->getId(); ?>" href="<?php echo url_for('clientNewOrder'); ?>"><?php echo $node->getName(); ?></a>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php else: ?>
    <ul class="with-icon">
        <?php foreach($node->getAllActiveProducts() as $node): ?>
        <li>
            <a class="client-new-order" data-client="<?php echo $clientObj->getId(); ?>" data-product="<?php echo $node->getId(); ?>" href="<?php echo url_for('clientNewOrder'); ?>"><?php echo $node->getName(); ?></a>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</li>
<?php endif ?>