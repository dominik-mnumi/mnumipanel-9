<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('Customers'); ?>
                    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientCreate)): ?>
                    <a id="dialog-add-client-click" href="#">
                        <?php echo image_tag('icons/fugue/plus-circle-blue.png',
                                array('width' => 16,
                                    'height' => 16)); ?>
                         <?php echo __('add'); ?>
                    </a>
                    <?php endif; ?>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>

<?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientCreate)): ?>
<?php echo include_partial('client/new/form', 
        array('form' => $newClientForm,
              'form2' => $userInvitationForm)); ?>
<?php endif; ?>

<!-- sets js slot -->
<?php extendSlot('actionJavascript'); ?>
<script type="text/javascript">
    jQuery(document).ready(function($) 
    {  
        // disables enter key
        $("#dialog-add-client-form input").keypress(function(e)
        {
            return disableEnterKey(e);
        });
 
        // client add form
        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientCreate)): ?>
        $("#dialog-add-client-click").click(function() 
        {                          
            $.modal({
                content: $("#dialog-add-client-form"),
                title: '<?php echo __('Add customer'); ?>',
                maxWidth: 700,
                maxHeight: 450,
                resizable: false,
                onOpen: function()
                {
                    var inputSelector = $(".modal-content #dialog-add-client-form input[type=text]");
                
                    var interval = setInterval(
                    function()
                    {
                        if(!inputSelector.first().is(":focus"))
                        {
                            $(inputSelector).first().focus();
                            clearInterval(interval);
                        }
                    }
                    , 
                    700);
                },
                buttons: 
                {
                    '<?php echo __('Add customer'); ?>': function(win)
                    {
                        // if add client form is visible then gets add client form
                        // otherwise gets invitation form
                        if(!$(".modal-content #invitation-button").hasClass("current"))
                        {
                            var form = $(".modal-content #new-client-form");
                        }
                        else
                        {
                            var form = $(".modal-content #user-invitation-form");
                        }

                        var dataInput = form.serialize();
                        var dataOutput;
                        var uri =  form.attr('action');
                        var method = form.attr('method');
                        $.ajax(
                        {
                            type: method,
                            url: uri,
                            data: dataInput,
                            dataType: 'json',
                            error: function(xhr, ajaxOptions, thrownError)
                            {
                                alert('<?php echo __('There was error. Please try again later.') ?>');
                            },
                            success: function(dataOutput)
                            {
                                if('success' == dataOutput.result.status)
                                {         
                                    if(0 != dataOutput.result.error.errorArray)
                                    {
                                        var errorlist = renderError(dataOutput.result.error.errorArray);

                                        $(".modal-content #dialog-add-client-form .validateTips").html(errorlist);
                                    }
                                    else
                                    {
                                        win.closeModal();
                                    
                                        var href = "<?php echo url_for('@clientEditAjaxRedirect'); ?>" + dataOutput.result.id;
                                        window.location.href = href;               
                                    }
                                
                                }
                           
                            }
                        });
                    },
                    '<?php echo __('Close'); ?>': function(win) 
                    {
                        win.closeModal();
                    }
                }
            });      
        });
        <?php endif; ?>
    });
</script>
<?php end_slot(); ?>