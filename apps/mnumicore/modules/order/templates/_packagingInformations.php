<?php use_helper('OrderPackage'); ?>
<?php $orderPackage = $orderObj->getOrderPackage(); ?>

<legend style= "display: block;"><?php echo __('Packaging information'); ?></legend>

<div id="package-info-loader" class="loader margin-top50 margin-bottom50">
    <?php echo image_tag('/images/loader.gif'); ?>                               
</div>

<div id="package-info-container" class="ms-container">                 
    <?php if($orderObj->hasPackage()): ?>
    <p>
        <label for="orders_added_to"><?php echo __('Package'); ?> 
            <?php echo link_to(order_package_name($orderPackage), 'clientPackageDetails', $orderPackage); ?>
            (<?php echo __plural('1 order', '@count orders', array('@count' => $orderPackage->getOrders()->count())); ?>): </label>
        <table class="table sortable package-orders-table">
        <thead><tr>
            <th><?php echo __('Order') ?></th>
        </tr></thead>
        <tbody>
            <?php foreach($orderPackage->getOrders() as $row): ?>
            <tr>
                <td<?php echo ($orderObj->getId() == $row->getId()) ? ' class="current"' : ''; ?>> 
                    <?php echo link_to( $row->getId().'. '. $row->getName(), 'orderListEdit', $row); ?> <span>(<?php echo __($row->getOrderStatus()); ?>)</span>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
    </p>
    <p>
        <small><strong><?php echo __('Delivery address') ?></strong>:</small><br/>
        <?php if($orderPackage->hasCarrier()): ?>
            <b><?php echo $orderPackage->getCarrier(); ?></b><br/>
        <?php endif; ?>    
        <?php if($orderPackage->getCarrier()->getRequireShipmentData()): ?>
        <?php echo $orderPackage->getDeliveryStreet(); ?><br/>
        <?php echo $orderPackage->getDeliveryCity(); ?><br/>
        <?php echo $orderPackage->getDeliveryPostcode(); ?>
        <?php if($sf_user->getCountryCode() != $orderPackage->getDeliveryCountry()): ?>
        <br/><?php echo $sf_user->getCountry($orderPackage->getDeliveryCountry()); ?>
        <?php endif; ?>  
        <?php endif; ?>  
    </p>

    <?php if($orderPackage->getDeliveryAt()): ?>
        <p>
            <small><strong><?php echo __('Delivery date') ?></strong>:</small><br/>
            <?php echo format_date($orderPackage->getDeliveryAt(), 'i'); ?>
        </p>
    <?php endif; ?>

    <p>
        <?php if($orderPackage->hasPayment()): ?>
        <small><strong><?php echo __('Payment status') ?></strong>:</small><br/>
            <?php echo $orderPackage->getPayment(); ?> (<?php echo __($orderPackage->getPaymentStatusName()) ?>)
        <?php endif ?>
    </p>
    <?php endif ?>
    
    <?php if(count($orderPackage->getCashDesks())): ?>
    <br/>
    <div class="task with-legend">
        <div class="legend">
            <?php echo __('Package payments') ?>
        </div>
        <div>
            <ul class="mini-blocks-list">
                <?php foreach($orderPackage->getCashDesks() as $cashDesk): ?>
                <li>
                    <a href="<?php echo url_for('cashDeskPrint', $cashDesk) ?>" class="float-left">
                        <?php echo $cashDesk->getClientName() ?> <?php echo $cashDesk->getFormattedNumber() ?>
                        <?php if($cashDesk->getPaymentId()): ?>
                            (<?php echo $cashDesk->getPayment() ?>)
                        <?php endif ?>
                    </a>
                    <span style="float:right"><?php echo $cashDesk->getValue() ?> <?php echo $sf_user->getCurrencySymbol() ?></span>
                </li>
                <?php endforeach ?>
            </ul>                   
        </div>
    </div>
    <br/>
    <?php endif; ?>

    <p style='text-align:center'>

        <?php
        /** @var Order $orderObj */
        if($orderObj->hasPackage() && !$orderObj->isUneditable() && !$orderObj->getOrderPackage()->isSent()): ?>
        <a href="#" id="dialog-edit-package-click" class="button">
            <?php echo __('Delivery and Payment'); ?>
        </a>&nbsp;&nbsp;
        <?php endif ?>

        <?php if(
            !in_array($orderObj->getOrderStatusName(), array(OrderStatusTable::$deleted, OrderStatusTable::$draft))
            &&
            (!$orderPackage || !$orderPackage->isCompleted())
            && !$orderObj->isUneditable()
        ): ?>
        <a href="#move-to-new-package-button" 
           id="move-to-new-package-button" 
           class="button grey">
            <?php echo __('Move to new Package'); ?>
        </a>           
        <?php elseif($orderObj->getOrderStatusName() == OrderStatusTable::$draft): ?>
            <ul class="message warning no-margin">
                <li><?php echo __('To move to new Package, press \'Create new order\''); ?></li>
            </ul>
        <?php endif; ?>
    </p>
</div>         

<script type="text/javascript">
jQuery(document).ready(function($) 
{
    /**
     * Requests move to new package action.
     */
    $("#move-to-new-package-button").click(function()
    {   
        showLoaderAndDisableButtons();
        
        $("#package-info-loader").show();
        $("#package-info-container").hide();
        
        $.ajax(
        {
            url: '<?php echo url_for('orderNewPackage', $orderObj); ?>',
            type: 'POST',
            dataType: 'json',
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert('<?php echo __('There was error. Please try again later.') ?>');
            },
            success: function(dataOutput)
            {
                $("#packagingInformation").html(dataOutput.packageInfo);
                $("#package-form-container").html(dataOutput.packageForm);
                $("#package-info-loader").hide();
                $("#package-info-container").show();
                $(".next-step").removeAttr("disabled");
                
                // saves order
                saveOrder(null, function(){
                    $("#dialog-edit-package-click").click();
                });
            }
        });
    });
});
</script>
