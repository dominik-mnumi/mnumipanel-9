<script type="text/javascript">

/**
 * Saves order and allows on callback function.
 */
function saveOrder(selector, callback)
{
    showLoaderAndDisableButtons();
    
    var form = $('#order_form');
    var data = form.serializeArray();
    var saveUrl = form.attr('action');
    var nextStatus = '';

    //clear error list, stop previous fade out
    $('#order_error_message').html('').stop();

    //if rel is set then save + change order status
    if(selector && $(selector).attr('rel'))
    {
        data.push({name: 'id', value: $(selector).attr('data-id').substring(5)});
        
        nextStatus = $(selector).attr('rel');
        data.push({name: 'nextStatus', value: nextStatus});
    }

    $.ajax(
    {
        url: saveUrl,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function(result)
        {
            if(result.type == 'success')
            {
                //set workflow buttons
                $('#steps').html(result.buttons);

                //change status icon
                $('.order-status-icon').attr('src', result.icon_url);
                $('form#order_form').attr('action', result.formActionUrl);
                $('form#order_form').data('orderstatus', result.currentStatus);
                $('form#order_form').data('is-uneditable', result.isUneditable);

                closeOpenOrderEdition(result.isUneditable);
   
                notify(result.message);        
            }
            else
            {
                $.each(result.errors, function(key, val)
                {
                    $('#order_error_message').append('<ul class=\"message error no-margin\"><li>'+val+'<\/li><\/ul>');
                });
                
                $('#order_error_message').show();
                $('#order_error_message').delay(15000).fadeOut('slow');
                $('#form_changed').val('');

                // hides loader
                $("#order-form-actions-loader").hide();

                // unblocks status change
                $(".order-status-menu-button").removeClass('disabled');
                
                notify('<b class="red-text">' + result.message +'</b>');
            }

            $('#form_changed').val('');
            $("#order-form-actions-loader").hide();

            // callback section
            if(callback)
            {
                callback();
            }
            // if callback not defined then refresh package info 
            // (avoid break when redirection)
            else
            {
                // if status is defined refresh
                if(nextStatus != '')
                {           
                    // refreshes package info
                    refreshPackageInfo();
                }
            }
        }
    });
    
    return false;
}

 /**
  * Moves order to shelf then redirects to user orders (invoicing) 
  * or to package edit (when not all orders are ready).
  * 
  * @returns void
  */
function toInvoicing()
{
   $.ajax(
   {
       type: 'GET',
       url: '<?php echo url_for('orderToShelf', $order); ?>',
       data: '',
       dataType: 'json',
       error: function(xhr, ajaxOptions, thrownError)
       {
           alert('<?php echo __('An error occurred 2'); ?>' + xhr);
       },
       success: function(dataOutput)
       {
           $('#order_message').append('<li><?php echo __('Added correctly'); ?></li>');
           $('#order_message').show();
           $('#order_to_book').hide();

           window.location.href = dataOutput.result.redirectUrl;
       }
   });
}

/**
 * Shows loader, disables and removes save-form class to avoid duplicate status change.
 */
function showLoaderAndDisableButtons()
{
    // unbinds click event from user
    $(".order-status-menu-button").unbind('click');
    
    // shows loader
    $("#order-form-actions-loader").show();

    // blocks status change
    $(".order-status-menu-button").addClass('disabled');
    
    // hides menu
    $(".order-status-menu-button ul").hide();

    // shows loader
    $("#order-form-actions-loader").show();

    $('#form_changed').val('');

    // blocks status change
    $(".next-step").attr('disabled', true);
}

/**
 * Refreshes package data.
 */
function refreshPackageInfo()
{
    $("#package-info-loader").show();
    $("#package-info-container").hide();

    $.ajax(
    {
        url: '<?php echo url_for('orderPackageInfo', $order); ?>',
        type: 'POST',
        dataType: 'json',
        error: function(xhr, ajaxOptions, thrownError)
        {
            alert('<?php echo __('There was error. Please try again later.') ?>');
        },
        success: function(dataOutput)
        {
            $("#packagingInformation").html(dataOutput.packageInfo);
            $("#package-form-container").html(dataOutput.packageForm);
            $("#package-info-loader").hide();
            $("#package-info-container").show();
            $(".next-step").removeAttr("disabled");

            <?php if($order->getOrderPackage()->wasModified() == false): ?>
            $("#dialog-edit-package-click").click();
            <?php endif; ?>
        }
    });
}

function closeOpenOrderEdition(close) {

    if(close)
    {        
        $('#order_form').addClass('order-uneditable');        
        $('.order-uneditable input[type=text], .order-uneditable textarea, .order-uneditable select').prop('disabled', 'disabled');
    }
    else
    {   
        $('.order-uneditable input[type=text], .order-uneditable textarea, .order-uneditable select').prop('disabled', false);
        $('#order_form').removeClass('order-uneditable');
        
        $("[data-onlyclosed]").each(function(){
            $(this).find("option[value="+ $(this).data('onlyclosed') +"]").remove();
        });        
    }

}

jQuery(document).ready(function($) 
{
    /**
     * Adds client notice. 
     */
    $('#add_reminder_btn').click(function() 
    {
        $.modal(
        {
            content: $("#dialog-order-notice-form"),
            title: "<?php echo __('Add reminder'); ?>",
            maxWidth: 700,
            maxHeight: 350,
            resizable: false,
            onOpen: function()
            {
                var inputSelector = $(".modal-content #dialog-order-notice-form input[type=text]");

                var interval = setInterval(
                function()
                {
                    if(!inputSelector.first().is(":focus"))
                    {
                        $(inputSelector).first().focus();
                        clearInterval(interval);
                    }
                }
                , 
                700);
            },
            buttons: 
            {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-order-notice-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');

                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alert('<?php echo __('An error occurred'); ?>');
                        },
                        success: function(dataOutput)
                        {
                            if (dataOutput.status == 2)
                            {
                                //alert('<?php echo __('Notice have to be a number'); ?>');
                            } 
                            else if (dataOutput.status == 1)
                            {
                                //alert('<?php echo __('Reminder added succesfully'); ?>');
                            }
                            else
                            {
                                //alert('<?php echo __('Reminder already added'); ?>');
                            }

                            $('#order_message').parent().prepend(
                            '<ul class="message success grid_12"><li>'
                                + dataOutput.notice +
                                '</li><li class="close-bt-order"></li>  <input type="hidden" id="order_notice_id" value="' + dataOutput.id + '" /></ul>'
                        );
                            win.closeModal();
                        }
                    });
                },
                'Close': function(win) 
                {
                    win.closeModal();
                }
            }
        }); 
    });

    /**
     * Duplicate order
     */
    $('#duplicate_order_btn').click(function() 
    {
        $.modal(
        {
            content: '<?php echo __('Do you really want to duplicate this order?') ?>',
            title: "<?php echo __('Duplicate order') ?>",
            maxWidth: 700,
            maxHeight: 350,
            resizable: false,
            buttons:
            {
                '<?php echo __('Confirm') ?>': function(win) 
                {
                    $(this).attr('disabled', true);

                    $.ajax(
                    {
                        type: 'post',
                        url: '<?php echo url_for('@orderDuplicate?id='.$order->getId()) ?>',
                        dataType: 'json',
                        success: function(dataOutput)
                        {
                                $('#order_message').html('<li><a href="<?php if(sfConfig::get('sf_environment') != 'prod'){ echo $sf_request->getScriptName(); } ?>/order/'+dataOutput.result.id+'"><?php echo __('Go to duplicated order') ?></a></li>');
                                $('#order_message').show();
                                win.closeModal();

                        }
                    });
                },
                '<?php echo __('Close') ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
        return false;
    });
    
    $('#order_to_book').live("click", function()
    {
        $.ajax(
        {
            type: 'POST',
            url: '<?php echo url_for('orderToBook',
                        array('id' => $order->getId())); ?>',
            data: '',
            dataType: 'json',
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert('<?php echo __('An error occurred'); ?>'+xhr);
            },
            success: function(dataOutput)
            {
                $('#order_message').append('<li><?php echo __('Added correctly'); ?></li>');
                $('#order_message').show();
                $('#order_to_book').hide();
            }
        });
    });
    
    $('#changelog').click(function () 
    {
        $.modal(
        {
            content: $("#changelog-content"),
            title: "<?php echo __('Changelog'); ?>",
            maxWidth: 700,
            maxHeight: 350,
            resizable: false,
            onOpen: function()
            {
                $.ajax(
                {
                    url: "<?php echo url_for('@orderChangelog?id='.$order->getId()); ?>",
                    success: function(result)
                    {
                        $('.modal-content #changelog-content').html(result);
                    },
                    error: function(xhr, ajaxOptions, thrownError)
                    {
                        $('.modal-content #changelog-content').html('<?php echo __('Error while getting changelog for order.') ?>');
                    }
                });
                $('.modal-content #changelog-content').html('');
            },
            buttons:
            {
                '<?php echo __('Close') ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        }); 
    });
	
    // BLOCKING AND CUSTOM PRICES
    function flagPriceBlocking() 
    {
        // asks calculation from server
        askCalculation();
        
        if($('input[name="order[price_block]"]').is(':checked'))
        {
            $('.calculation-table input.price').removeAttr('readonly');
            $('.calculation-table input.price').first().focus();
            $('.system-price-td').show();
        }
        else
        {
            $('.calculation-table input.price').attr('readonly', 'readonly');
            $('.system-price-td').hide();       
        }
    }

    $('#order_price_block').change(flagPriceBlocking);
	
    //  END OF BLOCKING AND CUSTOM PRICES

    window.onbeforeunload = function(){ if($('#form_changed').val()){ return '<?php echo __('Do you want to leave without saving changes?') ?>';} }

    $("#order_form").live('change', function()
    {
        $('#form_changed').val(true);
    });

    $('button[type="submit"]').click(function() 
    {
        $('#form_changed').val('');
        return true;
    });

    flagPriceBlocking();

    // order informations background
    $('#order_informations').parent().addClass('informations-field');

    flagPriceBlocking();

    // order informations background
    $('#order_informations').parent().addClass('informations-field');

    $('#toBookButton').click(function(e) 
    {
    	e.preventDefault();
    	href = $(this).attr('href');
    	$.ajax({
            type: 'GET',
            url: href,
            success: function()
            {
                $('#toBookButton').remove();
            }
        });
    });

    <?php if($order->getOrderPackage()->wasModified() == false): ?>
    $("#dialog-edit-package-click").click();
    <?php endif; ?>
    
    closeOpenOrderEdition(<?= $order->isUneditable() ? 'true' : 'false'; ?>);
        
});
</script>
