<?php foreach($form as $formField): ?>
    <!-- if diff than OTHER -->
    <?php if('other' != $formField->getName()): ?>
    <?php if('hidden' == $formField->getWidget()->getOption('type')): ?>
    <?php echo $formField->render(); ?>
    <?php else: ?>   
    <!-- if not hidden and not other and special (width and height) -->
    <?php if('size_width' == $formField->getName()): ?> 
    <?php $sizeWidthWidget = $formField; ?>
    
    <!-- gets size_height -->
    <?php $sizeHeightWidget = $form['size_height']; ?>

    <table class="size_form">
        <tr>
            <td>
                <?php echo $sizeWidthWidget->renderLabel(); ?>
            </td>
            <td></td>   
            <td>
                <?php echo $sizeHeightWidget->renderLabel(); ?>
            </td>
        </tr>
        <tr>
            <td>
                <nobr><?php echo $sizeWidthWidget->render(); ?> <?php echo sfConfig::get('app_size_metric', 'mm'); ?></nobr>
            </td>
            <td style="width:10px;"></td>
            <td>
                <nobr><?php echo $sizeHeightWidget->render(); ?> <?php echo sfConfig::get('app_size_metric', 'mm'); ?></nobr>
            </td>
        </tr>
    </table>

    <!-- move pointer to next field (size schema: size select->width->height) -->
    <?php $form->next(); ?>
    
    <!-- if not hidden, not other, not special -->
    <?php else: ?>
    <p>
        <?php echo $formField->renderError(); ?>
        <?php echo $formField->renderLabel(); ?>
        <?php echo $formField->render(); ?>
    </p>
    <?php endif; ?>
    <?php endif; ?>
    <!-- if OTHER -->
    <?php elseif('other' == $formField->getName()): ?>

    <!-- foreach OTHER -->
    <?php foreach($formField  as $formField2): ?>
    
    <!-- if OTHER is hidden -->
    <?php if('hidden' == $formField2->getWidget()->getOption('type')): ?>
    <?php echo $formField2->render(); ?>

    <!-- if OTHER is checkbox -->
    <?php elseif('checkbox' == $formField2->getWidget()->getOption('type')): ?>
    <p class="one-line-input grey-bg small-margin">
        <?php echo $formField2->renderError(); ?>
        <?php echo $formField2->renderLabel(); ?>
        <?php echo $formField2->render(); ?>
    </p>

    <!-- if OTHER is select -->
    <?php else: ?>
    <p>
        <?php echo $formField2->renderError(); ?>
        <?php echo $formField2->renderLabel(); ?>
        <?php echo $formField2->render(); ?>
    </p>
    <?php endif; ?>
    <?php endforeach; ?>
    <?php endif; ?>
<?php endforeach; ?>