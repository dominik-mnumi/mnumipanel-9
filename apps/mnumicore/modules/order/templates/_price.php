<?php echo $sf_user->formatCurrency($row->getTotalAmount()) ?>
<br/>
<strong><?php echo $sf_user->formatCurrency($row->getTotalAmountGross()) ?> <?php echo __('gross'); ?></strong>

<?php if(isset($attributes['traderPrice']) && $attributes['traderPrice']): ?>
<?php $tradeUserArr = $row->getTradeUserArr(); ?>
<?php foreach($tradeUserArr as $rec): ?>
<?php $userTraderColl = $row->getUserTraders($rec); ?>
<br/>
<br/>
<strong><?php echo $userTraderColl->getFirst()->getSfGuardUser(); ?>:</strong>

<?php foreach($userTraderColl as $rec2): ?>
<?php echo $rec2->getQuantity(); ?>h x <?php echo $sf_user->formatCurrency($rec2->getPrice()); ?>  = <?php echo $sf_user->formatCurrency($rec2->getTotalPriceNet()); ?>
<br />
<?php endforeach; ?>
<strong><?php echo __('Profit'); ?>:</strong> <?php echo $sf_user->formatCurrency($row->getTraderProfitNet($rec)); ?> + <?php echo + $row->getTaxValue(); ?>%
<?php endforeach; ?>
<?php endif; ?>