<div class="clientName">
    <?php if($row->getClientId()): ?>
    <?php echo link_to($row->getClient(), 'clientListEdit', $row->getClient()); ?> (<?php echo link_to($row->getsfGuardUser(), 'userListEdit', $row->getsfGuardUser()); ?>)
    <?php else: ?>
    <?php echo __('none'); ?>
    <?php endif; ?>
</div>
<?php if($row->hasPackage()): ?>
<?php $package = $row->getOrderPackage(); ?>
<?php if($package->hasCarrier()): ?>
<ul class="keywords">
    <li class="orange-keyword"><?php echo $package->getCarrier(); ?></li>
</ul>
<?php if($package->getCarrier()->getRequireShipmentData()): ?>
<small>    
    <?php echo $package->getDeliveryStreet(); ?><br/>
    <?php echo $package->getDeliveryPostcode(); ?>, <?php echo $package->getDeliveryCity(); ?> 
    <?php if($sf_user->getCountryCode() != $package->getDeliveryCountry()): ?>
    <br/><?php echo $sf_user->getCountry($package->getDeliveryCountry()); ?>
    <?php endif; ?>
</small>
<?php elseif (!is_null($package->getHangerNumber())): ?>
<small> 
    <strong><?php echo __('Numbered hanger tag'); ?></strong>: <?php echo $package->getHangerNumber(); ?>
</small>
<?php endif; ?>
<?php endif; ?>
    
<?php endif; ?>