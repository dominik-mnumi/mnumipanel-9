<br/>
<div id="send_calculation_form" style="display: none; text-align: left;">
    <div class="send-errors"></div>
    <br/>
    <form action="<?php echo url_for("orderSendEmailCalculation"); ?>" method="post" id="form_send">
        <?php echo $form->renderHiddenFields(); ?>
        <b><?php echo $form["to"]->renderLabel(); ?></b><br/>
        <?php echo $form["to"]->render(); ?>
        <?php echo $form["to"]->renderError(); ?>
        <br /><br />
        <b><?php echo $form["subject"]->renderLabel(); ?></b><br/>
        <?php echo $form["subject"]->getWidget()->render('send_calculation[subject]',
                __('Calculation from').' '.format_datetime(time())); ?>
        <?php echo $form["subject"]->renderError(); ?>
        <br /><br />
        <h3><?php echo __("Email preview"); ?></h3><br/>
        <?php echo $form["calculation"]->render(); ?>
        <?php echo $form["calculation"]->renderError(); ?>
        <br />        
    </form>

    <?php if($form->getObject()->getOrderStatusName() == OrderStatusTable::$draft): ?>
        <p>
            <b><?php echo __("Status of this order will be changed from 'Draft' to 'Calculation'"); ?></b>
        </p>
    <?php endif; ?>
</div>


<script type="text/template" id="email-message-form"><?php echo nl2br(Cast::html2Text(sfOutputEscaper::unescape($calculationSendTemplate))); ?></script>


<!-- sets slot -->
<?php extendSlot("actionJavascript") ?>

<script type="text/javascript">
    /**
     * Refreshes email content.
     */
    function refreshEmailBody()
    {
        var content = _.template($('#email-message-form').html(), {collection: calcItems, calculation: CalculationApp });
		$(".modal-content #email_calculation_textarea").html(content);
    }
    
    function sendEmail()
    {
        $(".modal-content #email_calculation").val($(".modal-content #calculation_details").html());
        $(".modal-content .send-errors").html("");   

        var form = $(".modal-content #form_send");
        var data = form.serialize();
        var uri =  form.attr("action");
        var method =  form.attr("method");
        $.ajax(
        {
            type: method,
            url: uri,
            data: data,
            dataType: "json",
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert("<?php echo __("There was error. Please try again later.") ?>");
            },
            success: function(data)
            {
                modalFooterLoader(false);
                if(!data.result.success)
                {
                    var errors = data.result;

                    $(".modal-content .send-errors").html(renderError(errors));
                }
                else
                {
                    displayAlert($(".modal-content #send_calculation_form"), "success", data.result.message);
                    $("#modal .block-footer button:first").remove();
                }
            }
        });  
    }
    
    jQuery(document).ready(function($) 
    {   
        /**
         * Send button action (form shows).
         */
        $("#sendBtn").click(function() 
        {
            modalEmailForm(
                $("#send_calculation_form"),
                "<?php echo __("Send calculation"); ?>",
                "email_calculation_textarea_modal",
                function() {
                    refreshEmailBody();
                    $('.modal-content #email_calculation_textarea').attr('id', 'email_calculation_textarea_modal');
                },
                function() {
                    saveOrder(null, sendEmail);
                },
                "<?php echo __("Send"); ?>",
                "<?php echo __("Close"); ?>"
            );
        });
    });
</script>
<?php echo include_partial('order/modalLoader') ?>
<?php end_slot() ?>