<%= collection.getOrderName() %>
<%= collection.getSizeInformation() %>
<%= collection.getIssueInformation() %>
<%= collection.getQuantityInformation() %>

<% collection.each(function(item) { if(item.get('price') > 0){ %>
 - <%= item.get('label') %> - <%= item.get('price') %> <?php echo $sf_user->getCurrencySymbol(); ?>
<% } }); %> 

<?php echo strtoupper(__('Total')); ?>: <%= collection.getSummary() %> <?php echo $sf_user->getCurrencySymbol(); ?> (<%= collection.getSummaryGross() %> <?php echo $sf_user->getCurrencySymbol(); ?> <?php echo __('incl. VAT'); ?>)
<% if(collection.getDiscount() > 0){ %><?php echo __('Discount') ?>: <%= collection.getDiscount() %> <?php echo $sf_user->getCurrencySymbol(); ?><% } %> 