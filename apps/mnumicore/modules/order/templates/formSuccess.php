<?php $discount = $sf_data->getRaw('discount'); ?>
<?php $hasCostCredential = $sf_user->hasCredential(sfGuardPermissionTable::$orderCost); ?>
<input type="hidden" id="form_changed" value="">
<?php include_partial('global/alertModalConfirm'); ?>
<?php //used for calculation ?>
<input type="hidden" id="order_product_id" value="<?php echo $product->getId() ?>">
<input type="hidden" id="order_client_id" value="<?php echo $client->getId() ?>">
<input type="hidden" id="order_id" value="<?php echo $order_id; ?>">

<form id="order_form"  
      data-orderStatus="<?php echo $form->getObject()->getOrderStatusName(); ?>"
      data-is-uneditable="<?php echo $form->getObject()->isUneditable() ? 'true' : 'false'; ?>"
      method = "post" 
      class="form" 
      action="<?php echo $formActionUrl; ?>">
          <?php echo $form->renderHiddenFields() ?>          
          <?php echo include_partial('order/formActions', array('orderObj' => $form->getObject())) ?>
          <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <div class="order-error-list" id="order_error_message"></div>
        <ul class="message warning no-margin info-container-green" id="order_message" style="display: none;"></ul>
        <?php if($form->getObject()->getClient()): ?>
            <?php include_partial('client/edit/notices', array('notices' => $form->getObject()->getClient()->getClientNotices())) ?>
        <?php endif ?>
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content" style="">
                    <?php include_partial('client/edit/tabList', array(
                        'clientObj' => $form->getObject()->getClient(),
                        'currentTab' => 'tab-orders',
                        'defaultDisableAdvancedAccountancy' => $defaultDisableAdvancedAccountancy)); ?>
                    <h1>
                    	<img class="order-status-icon" src="<?php echo $order->getOrderStatusIcon() ?>" title="<?php echo __($order->getOrderStatusIconTitle()) ?>" />
                    	<span class="name"><?php echo $form->getObject()->getEditLabel(); ?></span> (<?php echo __('product'); ?>: <span id="product-name"><?php echo trim($form->getObject()->getProduct()); ?></span>)
                    </h1>
                        
                    <div class="content-columns left30">
                        <div class="content-columns-sep"></div>
                        <div class="content-left status-<?php echo $form->getObject()->getOrderStatusName(); ?>">
                            <fieldset class="grey-bg required" style="margin: 10px;">
                                <legend style= "display: block;"><?php echo __('Add files') ?></legend>
                                <div id="uploader-loader" class="loader">
                                    <?php echo image_tag('/images/loader.gif'); ?>
                                </div>
                                <div id="uploader-interface">                                   
                                    <div class="more-views">
                                        <ul id="attachedFiles" class="mini-blocks-list"></ul>
                                        <a href="<?php echo url_for('@fileZip?id='.$form->getObject()->getId()) ?>" id="dowload-zip" style="display: none;" target="_blank">
                                            <img width="16" height="16" class="picto" src="/images/icons/fugue/arrow-curve-000-left.png"> 
                                            <?php echo __('Download .zip package') ?>
                                        </a>
                                    </div>                                   
                                    <div class="ms-container" id="plupload-container">
                                        <div>
                                            <ul id="filelist" class="mini-blocks-list"></ul>
                                        </div>
                                        
                                        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderFile)): ?>
                                        <br />
                                        <button type="button" id="pickfiles"><?php echo __('Add files'); ?></button>
                                        
                                        <!-- WIZARD forms (new and edit) -->
                                        <?php if($wizardEnabled): ?>
                                        <?php include_partial('order/wizard',
                                                array('wizardId' => $wizardId)); ?>
                                        <?php endif; ?>
                                        
                                        <br/>
                                        <button type="button" id="stop_upload" style="display: none;"><?php echo __('Stop') ?></button>
                                        <?php endif; ?>
                                    </div>    
                                </div>
                            </fieldset>
                            <div id="order_fields">
                                <fieldset class="form-fieldset">
                                    <legend style= "display: block;"><?php echo __('Fill order') ?></legend>
                                    <?php echo include_partial('fields', array('form' => $form['attributes'])); ?>
                                    <div>
                                        <img src="/images/icons/fugue/flag.png" class="order-info-flag" />
                                        <?php echo $form['informations']->renderLabel() ?><br/>
                                        <?php echo $form['informations']->render() ?>
                                    </div>
                                </fieldset>
                            </div>

                            <?php if(!$form->isNew()): ?>                           
                            <fieldset id="packagingInformation" class="" style="margin: 10px;">                                                    
                                <?php include_partial('packagingInformations', array('orderObj' => $form->getObject())); ?>
                            </fieldset>
                            <?php endif ?>
                        </div>
                        <div class="content-right">
                            <br/><br/>
                            <?php include_partial('clientInfo', array('client' => $client, 'order' => $form->getObject()))?>
                            
                            <div id="title_area">
                                <div id="newTitle" style = "display: none;">
                                    <input type="text" id="newTitleInput" class= "newTitleInput" maxlength="100" />
                                    <?php echo image_tag('icons/fugue/tick-circle.png', array('id' => 'acceptTitle')) ?> 
                                    <?php echo image_tag('icons/fugue/cross-circle.png', array('id' => 'cancelTitle')) ?>
                                </div>

                                <h2 class="bigger<?php echo $form->getObject()->getCreatedAt() != $form->getObject()->getUpdatedAt() ? ' title-changed' :''; ?>" style="margin-bottom: 10px; cursor:pointer;" id="title">
                                    <?php echo $form->isNew() ? $product->name : $form['name']->getValue() ?>                       
                                </h2>
                            </div>
                            
                            <div class="gallery-preview">
                                <a class="prev" href="#" style="display: none;"><img width="16" height="16" src="/images/icons/fugue/navigation-180.png"></a>
                                <a class="next" href="#" style="display: none;"><img width="16" height="16" src="/images/icons/fugue/navigation.png"></a>
                                <div id="def_image" style="display: none;">
                                    <img src="/images/no-preview.jpg">
                                </div>
                                <div id="preview-loader" class="loader" style="display:block;">
                                    <?php echo image_tag('/images/loader.gif'); ?>
                                </div>
                                <div id="item_photos"></div>                                                                                    
                            </div>

                            <fieldset class="with-legend calculation-container">
                            	<legend class="large"><?php echo __('Calculation') ?></legend>
         
                                <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderTrader)): ?>
                                <fieldset class="no-margin no-bottom-border no-bottom-padding order-trader-calculation">
                                    <legend><?php echo __('Designer/Seller') ?></legend>
                                    <div id="trader_app" class="input-with-button"></div>                                   
                                </fieldset>
                                <?php endif; ?>    
                                
                                <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderPricelistChange)): ?>
                                <!-- order pricelist -->
                                <fieldset class="no-margin no-bottom-border no-bottom-padding order-pricelist-calculation">
                                    <legend><?php echo __('Pricelist for order') ?></legend>
                                    <div class="order-pricelist-div">
                                        <?php echo $form['pricelist_id']->renderLabel() ?>
                                        <?php echo $form['pricelist_id']->render() ?>
                                    </div>
                                </fieldset>
                                <?php endif; ?>

	                            <?php if($hasCostCredential): ?>
	                            <fieldset class="no-margin no-bottom-border no-bottom-padding cost-calculation">
		                            <legend><?php echo __('Costs') ?></legend>
		                            <div id="cost_app" class="input-with-button"></div>
	                            </fieldset>
	                            <?php endif; ?>

	                            <div id="calculation_details" class="order-package-content">
                                    <fieldset class="no-margin">
                                        <legend><?php echo __('Price') ?></legend>	
                                        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderBlockPrice)): ?>
                                        <p class="one-line-input grey-bg small-margin block-price-calculation">
                                            <?php echo $form['price_block']->renderLabel() ?>
                                            <?php echo $form['price_block']->render(array('class' => 'mini-switch')) ?>
                                        </p>
                                        <?php endif; ?>
		                                
                                        <?php //include_component('order', 'calculation', array('order' => $order, 'postArray' => $order->getCalculationFields())); ?>
                                        <table class="calculation-table">
                                            <thead>
                                                <tr>
                                                    <th class="text-left padding-left5px">
                                                        <?php echo __('Label'); ?>
                                                    </th>
	                                                <th class="system-cost-td amount"<?php if(!$hasCostCredential): ?> style="display: none;"<?php endif; ?>>
		                                                <?php echo __('Costs'); ?>
	                                                </th>
                                                    <th class="system-price-td amount">
                                                        <?php echo __('Price generated by system'); ?>
                                                    </th>
                                                    <th class="amount">
                                                        <?php echo __('Order price'); ?>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="js-calculation"></tbody>
                                            <tfoot id="calculation-sum"></tfoot>
                                        </table>
                                        <div id="calculation_error" class="error-message hidden"></div>
                                    </fieldset>
                                </div>
                                <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderSendCalculation)): ?>
                                <div id="calculation-btn-container">
                                    <?php if(isset($calculationSendTemplate)): ?>
                                    <button type="button" id="sendBtn"><?php echo __('Send'); ?></button>
                                    <?php else: ?>
                                    <span class="red-text">
                                        <?php echo __('There is no "Calculation" template - click %arg1% to set it',
                                                array('%arg1%' => '<a href="'.url_for('settingsNotification').'">'.__('here').'</a>')); ?>
                                    </span>
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>

                            </fieldset>                          
                            <div style="text-align: center; padding-top:15px;">
                                <p id="save-message" style="display:none;">
                                    <b class="red-text"><?php echo __('Before printing please save') ?></b>
                                <p>
                                    <button type="button" class="red" id="reportBtn"><?php echo __('Report card'); ?></button>
                                </p>

                                <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderViewChangelog)): ?>
                                <p>
                                    <button type="button" id="changelog" class="grey"><?php echo __('Changelog') ?></button>
                                </p>
                                <?php endif; ?>

                                <div id="changelog-content"></div>
                            </div>
                                                    
                        </div>       
                    </div>             
                </div>
            </div>
        </section>
    </article>
</form>

<!-- backbone templates -->
<script type="text/template" id='client-traders-form'>    
    <div>
        <select class="trader_name" name="_trader" id="trader">
            <option value=""><?php echo __('Select customer trader');?></option>
            <% for(var i = collection.length - 1; i >= 0; i--){ %>
            <option value="<%= collection[i].id %>"><%= collection[i].name %></option>
            <% }; %>
        </select>
        <input class="quantity" name="_quantity" id="quantity" value="1" />h 
        <input class="price" name="_price" id="price" value="<?php echo sfConfig::get('app_trader_hour_rate', 100); ?>" /><?php echo $sf_user->getCurrencySymbol(); ?>/h
        <input class="description" name="_description" id="description" />
        <button class="small add"><?php echo __('Add'); ?></button>
        <div id="trader_error" class="hidden">
            <?php echo __('Please choose customer trader') ?>
        </div>
        <div id="trader_list"></div>       
    </div>     
</script>

<script type="text/template" id='client-row'>
	<div>
		<%= name %>:
		<%= quantity %> x
		<%= price %> zł/h =
		<strong><%= price * quantity %></strong> zł
		<%= description %>
		<button class="small delete red"><?php echo __('Delete'); ?></button>
	</div>
</script>

<script type="text/template" id='cost-form'>
	<div>
		<select class="cost-name" name="_cost">
			<option value=""><?php echo __('Select finish'); ?></option>
			<% for(var i = collection.length - 1; i >= 0; i--){ %>
			<option value="<%= collection[i].id %>"><%= collection[i].name %></option>
			<% }; %>
		</select>
		<input class="quantity" name="_quantity" id="quantity" value="1" />h
		<input class="price" name="_price" id="price" /><?php echo $sf_user->getCurrencySymbol(); ?>/h
		<input class="description" name="_description" id="description" />
		<button class="small add"><?php echo __('Add'); ?></button>
		<div id="cost-name-error" class="hidden">
			<?php echo __('Select finish') ?>
		</div>
		<div id="cost-list"></div>
	</div>
</script>

<script type="text/template" id='cost-row'>
	<div>
		<%= name %>:
		<%= quantity %> x
		<%= cost %> <?php echo $sf_user->getCurrencySymbol(); ?>/h =
		<strong><%= cost * quantity %></strong> <?php echo $sf_user->getCurrencySymbol(); ?>
		<%= description %>
		<button class="small delete red"><?php echo __('Delete'); ?></button>
	</div>
</script>
        	
<script type="text/template" id="calculation-item">
    <td class="label">
        <% if(type == 'trader' || type == 'cost') { %>
        <span class="trader price_label"><%= label %></span>
        <% } else { %>
        <input type="text" name="price<%= id %>[label]" value="<%= label %>" class="price price_label small" <%= (!$('#order_price_block').is(':checked')) ? 'readonly="readonly"' : '' %> />
        <% } %>
    </td>
	<td class="system-cost-td amount"<?php if(!$hasCostCredential): ?> style="display: none;"<?php endif; ?>>
		<% if(cost != '') { %>
		<% if(type == 'cost') { %>
		<span class="price_amount"><%= cost %> <?php echo $sf_user->getCurrencySymbol(); ?></span>
		<% } else { %>
		<input type="text" name="price<%= id %>[cost]" value="<%= cost %>" class="price price_cost small" <%= !$('#order_price_block').is(':checked') || type == 'cost' ? 'readonly="readonly"' : '' %> />
		<?php echo $sf_user->getCurrencySymbol(); ?>
		<% } %>
		<% } %>
	</td>
    <td class="amount system-price-td<%= !$('#order_price_block').is(':checked') ? ' hidden' : '' %>">
        <% if(price != '') { %>
        <% if(type == 'trader') { %>
        <span class="trader price_amount"><%= price %> <?php echo $sf_user->getCurrencySymbol(); ?></span>
        <% } else { %>
        <input type="text" name="price<%= id %>[systemPrice]" value="<%= systemPrice %>" readonly="readonly" class="price_system_amount small" /> 
        <?php echo $sf_user->getCurrencySymbol(); ?>
        <% } %>
        <% } %>
    </td>
    <td class="amount">
        <% if(price != '') { %>
        <% if(type == 'trader') { %>
        <span class="trader price_amount"><%= price %> <?php echo $sf_user->getCurrencySymbol(); ?></span>
        <% } else { %>
        <input type="text" name="price<%= id %>[price]" value="<%= price %>" class="price price_amount small" <%= (!$('#order_price_block').is(':checked')) ? 'readonly="readonly"' : '' %> /> 
        <?php echo $sf_user->getCurrencySymbol(); ?>
        <% } %>
        <% } %>
    </td>
</script>
        		
<script type="text/template" id="calculation-summary">
    <td class="price_label">
        <?php echo __('Total'); ?> 
    </td>
	<td class="system-cost-td amount"<?php if(!$hasCostCredential): ?> style="display: none;"<?php endif; ?>>
		<%= summary_cost %> <?php echo $sf_user->getCurrencySymbol(); ?><br />
		<%= symmary_cost_gross %> <?php echo $sf_user->getCurrencySymbol(); ?>
        <?php echo __('incl. VAT'); ?>
        <small>(<?php echo __('tax'); ?>: <?php echo $form->getObject()->getTaxValue(); ?>%)</small><br />
	</td>
    <td class="amount">
        <input type="hidden" name="order[system_price_net]" value="<%= summary_system_basic_price %>" />
        <%= summary_system_price %> <?php echo $sf_user->getCurrencySymbol(); ?><br />
        <%= symmary_system_gross %> <?php echo $sf_user->getCurrencySymbol(); ?>
        <?php echo __('incl. VAT'); ?>
        <small>(<?php echo __('tax'); ?>: <?php echo $form->getObject()->getTaxValue(); ?>%)</small><br />
        <?php if($discount): ?>
        <?php echo __('Discount') ?> <?php echo $discount['value']; ?> 
        <?php echo ($discount['type'] == CouponTable::$percent) ? '%' : $sf_user->getCurrencySymbol(); ?><br/>
        <?php endif; ?>
    </td>
    <td class="amount system-price-td<%= !$('#order_price_block').is(':checked') ? ' hidden' : '' %>">
        <input type="hidden" name="order[price_net]" value="<%= summary_basic_price %>" />
        <%= summary_price %> <?php echo $sf_user->getCurrencySymbol(); ?><br />
        <%= summary_gross %> <?php echo $sf_user->getCurrencySymbol(); ?>
        <?php echo __('incl. VAT'); ?>
        <small>(<?php echo __('tax'); ?>: <?php echo $form->getObject()->getTaxValue(); ?>%)</small><br />
        <?php if($discount): ?>
        <?php echo __('Discount') ?> <?php echo $discount['value']; ?> 
        <?php echo ($discount['type'] == CouponTable::$percent) ? '%' : $sf_user->getCurrencySymbol(); ?><br/>
        <?php endif; ?>
    </td>
</script>

<!-- attachment list -->
<script type="text/template" id='attachment-list'>  
    
    <!-- foreach file -->
    <% for(var i = 0; i < attachments.fileInfoArray.length; i++){ %>
    
    <li class="file-li">
        <a title="<%= attachments.fileInfoArray[i].filename %>" class="previewList" href="#simple_form">
            <%= getShorterFilename(attachments.fileInfoArray[i].filename) %>
        </a>
        <b>
            <% if(attachments.fileInfoArray[i].error) { %>
            (<span class="red-text"><%= attachments.fileInfoArray[i].error %></span>)
            <% } else { %>
            100%
            <% } %>
        </b>
        &nbsp;
        <button class="small download" val="<%= attachments.fileInfoArray[i].id %>" data-path="<%= attachments.fileInfoArray[i].hotfolderFilepath %>" href="#" type="button"><?php echo __('Download'); ?></button>
        
        <!-- if not preview -->    
        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderFile)): ?>
        <% if(!attachments.preview) { %>
        <button class="small notice" id="<%= attachments.fileInfoArray[i].id %>" href="#" type="button"><?php echo __('Notice')?></button>
        <a href="#" class="del_item" id="item_<%= attachments.fileInfoArray[i].id %>">
            <img width="16" height="16" src="/images/icons/fugue/cross-circle.png" />
        </a>
        <% } %>
        <br/>
        <textarea class="notice-text" rel="<%= attachments.fileInfoArray[i].id %>"><%= attachments.fileInfoArray[i].notice %></textarea>
        <div class="notice-div"><%= attachments.fileInfoArray[i].notice %></div>
        <?php endif; ?>
    </li>

    <% }; %>
    
    <!-- add hr if wizards exist -->
    <% if(attachments.wizardInfoArray.length) { %>
    <hr class="wizard-files-hr" />
    <% } %>
    
    <!-- foreach wizard -->
    <% for(var i = 0; i < attachments.wizardInfoArray.length; i++){ %>
    <li class="wizard-li">
        <a class="previewList" href="#simple_form" ><?php echo __('Project').' '; ?>
            <%= attachments.wizardInfoArray[i].no %>
        </a>
        &nbsp;&nbsp;&nbsp;
        <button type="button" href="#" onclick="getWizardPdfQueueAdd(this, '<%= attachments.wizardInfoArray[i].parameterPdf %>', 
            '<%= attachments.wizardInfoArray[i].signaturePdf %>');" 
            class="small">
            <?php echo __('Download'); ?>
        </button>
        &nbsp;
        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderFile)): ?>
        <a href="#" class="edit_wiz" onclick="submitEditForm('<%= attachments.wizardInfoArray[i].parameterEdit %>',
        '<%= attachments.wizardInfoArray[i].signatureEdit %>');">
            <img width="18" height="18" src="/images/icons/wizard_24.png" />
        </a>
        &nbsp;
        <a href="#" class="del_wiz" id="<%= attachments.wizardInfoArray[i].value %>">
            <img width="16" height="16" src="/images/icons/fugue/cross-circle.png" />
        </a>
        <?php endif; ?>
        <input type="hidden" name="order[wizard][<%= attachments.wizardInfoArray[i].no %>]" value="<%= attachments.wizardInfoArray[i].value %>" />
        <span class="wizard-info-li"></span>
        <?php echo image_tag('loader.gif', array(
            'width' => 16, 
            'height' => 16,
            'class' => 'wizard-loader-img')); ?>
    </li>
    <% }; %>
    
    <!-- local file server -->
    <?php if($localFileServerEnabled): ?>
    <% if(localServerAttachments.fileInfoArray.length) { %>
    <li class="box">
        <p class="mini-infos color-blue bold">
            <?php echo __('Files added on local server'); ?>
        </p>
    </li>
    <% }; %>
    
    <!-- foreach file -->
    <% for(var i = 0; i < localServerAttachments.fileInfoArray.length; i++){ %>
    
    <li class="file-li">
        <a title="<%= localServerAttachments.fileInfoArray[i].filename %>" class="previewList file-local-server" href="#simple_form">
            <%= getShorterFilename(localServerAttachments.fileInfoArray[i].filename) %>
        </a>
        <b>
            <% if(localServerAttachments.fileInfoArray[i].error) { %>
            (<span class="red-text"><%= localServerAttachments.fileInfoArray[i].error %></span>)
            <% } else { %>
            100%
            <% } %>
        </b>
        &nbsp;
        <button class="small download" val="<%= localServerAttachments.fileInfoArray[i].id %>" data-path="<%= localServerAttachments.fileInfoArray[i].hotfolderFilepath %>" href="#" type="button"><?php echo __('Download'); ?></button>
        
        <!-- if not preview -->    
        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderFile)): ?>
        <% if(!localServerAttachments.preview) { %>
        <button class="small notice" id="<%= localServerAttachments.fileInfoArray[i].id %>" href="#" type="button"><?php echo __('Notice')?></button>
        <a href="#" class="del_item" id="item_<%= localServerAttachments.fileInfoArray[i].id %>">
            <img width="16" height="16" src="/images/icons/fugue/cross-circle.png" />
        </a>
        <% } %>
        <br/>
        <textarea class="notice-text" rel="<%= localServerAttachments.fileInfoArray[i].id %>"><%= localServerAttachments.fileInfoArray[i].notice %></textarea>
        <div class="notice-div"><%= localServerAttachments.fileInfoArray[i].notice %></div>
        <?php endif; ?>
    </li>

    <% }; %>
    <?php endif; ?>
    
</script>


<?php if($form->getObject()->getClient()): ?>
    <?php include_partial('clientNotice', array('form' => $form, 'clientId' => $form->getObject()->getClient()->getId())); ?>
<?php endif ?>

<!-- if notification template exists -->
<?php if(isset($calculationSendTemplate)): ?>
<?php include_partial('order/orderEmail/sendCalculation', 
        array(
            'form' => $calculationForm,
            'order' => $form->getObject(),
            'calculationSendTemplate' => $calculationSendTemplate)); ?>
<?php endif; ?>

<!-- sets slot -->
<?php extendSlot('actionJavascript'); ?>
<?php include_partial('global/alertModalConfirm'); ?>
<?php include_partial('global/autocomplete'); ?>
<?php include_partial('order/javascript/createJavascript', array('order' => $form->getObject())); ?>
<?php include_partial('order/javascript/filesJavascript', 
        array('order' => $form->getObject(),
            'wizardId' => $wizardId,
            'localFileServerEnabled' => $localFileServerEnabled)); ?>
<script type="text/javascript" charset="utf-8"> 
    jQuery(document).ready(function($) 
    {
        window.CalculationApp = new CalculationView();

    	window.CalculationApp.tax_amount = <?php echo $form->getObject()->getTaxFloat(); ?>;
    	window.CalculationApp.discount = <?php echo json_encode($discount) ?>;

        <?php if($sf_data->getRaw('orderCalculationJson')): ?>
    	calcItems.reset(<?php echo $sf_data->getRaw('orderCalculationJson'); ?>);
        <?php endif; ?>

        var trader = <?php echo $sf_data->getRaw('clientTraderJson'); ?>;
	    var others = <?php echo $sf_data->getRaw('otherCostJson'); ?>;
        
	    // creates main view
        window.app = new MainView(trader); 
        window.app.addExistingTraders(<?php echo $sf_data->getRaw('orderTraderJson'); ?>);

	    // creates cost main view
	    window.app2 = new MainCostView(others);
	    window.app2.addExistingCosts(<?php echo $sf_data->getRaw('orderCostJson'); ?>);

	    /**
         *  Other switch change event. 
         */
        $("#order_fields .mini-switch-replace").click(function()
        {            
            askCalculation();
        });

        /**
         * Select and input change event. Triggers calculation proccess.
         */
        $("#order_fields select, #order_fields input[type=number], #order_fields input[type=text], #order_pricelist_id").change(function()
        {
            askCalculation();
        });

        /**
         * Print report card on button click.
         */
        $("#reportBtn").click(function(e)
        {
            if($('#form_changed').val())
            {
            $('#save-message').show().delay(5000).fadeOut('slow');
            return;
            }
            e.preventDefault();
            window.open('<?php echo url_for('reportPrint', $form->getObject()); ?>');
        });

        // EDITING TITLE    
        $('#title').click(function() 
        {
            $(this).hide();
            $('#newTitleInput').val($.trim($(this).html()));
            $('#newTitle').show();
        });

        function setTitle()
        {              
            $('#newTitle').hide();
            $('#title').html($('#newTitleInput').val()).show();
            $('input#order_name').val($.trim($('#newTitleInput').val()));
            $("#title").addClass("title-changed");
            return false;
        }

        $("#newTitleInput").bind("keypress", function(e) 
        {
            if (e.keyCode == 13) return setTitle();
        });

        $('#acceptTitle').click(function() 
        {
            setTitle();
        });

        $('#newTitleInput').change(function() 
        {
            setTitle();
        });

        $('#cancelTitle').click(function() 
        {
            $('#newTitle').hide();
            $('#title').show();
        });

        $('input#order_name').val($.trim($('#title').html()));
        
        // customizable
        function checkSizeChoice()
        {
            var selectVal = $("#order_attributes_size").val();
            var customizableVal = $("#order_attributes_size option:containsmatch('^- (.)* -$')").val();

            var obj = $('.size_form');
            (selectVal == customizableVal) ? $(obj).show() : $(obj).hide();
        }
        checkSizeChoice();
        
        $("#order_attributes_size").change(function()
        {
            checkSizeChoice();
        });

    });    
</script>
<?php end_slot(); ?>

<div id="package-form-container">
    <?php if(isset($editPackageFormArr)): ?>
    <?php echo include_partial('package/form/editPackageForm', 
            array(
                'form' => $editPackageFormArr,
                'defaultPaymentId' => $defaultPaymentId)) ?>
    <?php endif ?>
</div>
    
<?php include_slot('wizardForm');  ?>
