<script type="text/javascript">
    /**
     * Switches modal footer loader.
     */
    function modalFooterLoader(switchValue) {
        if (switchValue) {
            $(".block-content .block-footer").prepend(
                '<?php echo image_tag('/images/loader.gif',
    array('width' => 30,
        'class' => 'modal-footer-loader')); ?>');
            $(".block-content .block-footer button:first").attr('disabled', 'disabled');
        }
        else {
            $(".block-content .block-footer img").remove();
            $(".block-content .block-footer button:first").removeAttr('disabled');
        }
    }
</script>