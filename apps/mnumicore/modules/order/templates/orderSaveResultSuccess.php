<?php 
$result = $sf_data->getRaw('result');

$result['message'] = __($result['message']);
if(!$form->isValid())
{
    $errors = array();
    foreach($form->getErrorSchema()->getErrors() as $formField)
    {
        $errors[] = __($formField->getMessage());
    }
    $result['errors'] = $errors;
}
echo json_encode($result);
