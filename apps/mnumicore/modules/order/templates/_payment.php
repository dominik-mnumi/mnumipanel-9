<?php use_helper('OrderPackage'); ?>

<b><?php echo __($row->getOrderPackage()->isPaid() ? 'paid' : 'not paid'); ?></b>
<br/>
<small><?php echo $row->getOrderPackage()->getPayment(); ?></small>
<br/>
<?php echo __('Package') . ': ' .link_to(order_package_name($row->getOrderPackage()), 'clientPackageDetails', $row->getOrderPackage()); ?>