<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <?php include_partial('global/filterForm', $tableOptions) ?>
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form" id="complex_form" >
            <h1>
                <?php echo __('Orders'); ?>
            </h1> 
            <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>


<script type="text/javascript">
    jQuery(function($) {
        $('form#simple_form #order_filters_range').change(function() {
            if($(this).val() == 'custom')
            {
                $(this).parent().hide();
                $('form #order_filters_date_range_from').parent().show();
                loadDataPicker($);
            }
            else
            {
                $('form #order_filters_date_range_from').parent().hide();
            }
        });
        $('form #order_filters_date_range_from').parent().hide();
    });
</script>