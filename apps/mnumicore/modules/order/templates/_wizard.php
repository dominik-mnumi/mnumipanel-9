<button type="button" id="addproject" class="add-wizard-button">
    <?php echo image_tag('icons/wizard_24.png',
            array('width' => 18, 'height' => 18)); ?>
    <?php echo __('mWizard'); ?>
</button>

<?php slot('wizardForm'); ?>
<form id="new-wizard-form" action="<?php echo WizardManager::getNewOrderUrl(); ?>" 
      method="post" 
      style="display: none;" 
      target="MnumiWizard" 
      onsubmit="javascript:popupWizardWindow();">
    <input type="hidden" id="parameter" name="parameter" />
    <input type="hidden" id="signature" name="signature" />
    <input type="hidden" id="display" name="display" value="manage" />
    
    <?php if(!empty($wizardId)): // optional wizard id ?>
    <input type="hidden" name="id" value="<?php echo $wizardId; ?>" />
    <?php endif; ?>
</form>

<form id="edit-wizard-form" action="<?php echo WizardManager::getEditUrl(); ?>" 
      method="post" 
      style="display: none;" 
      target="MnumiWizard" 
      onsubmit="javascript:popupWizardWindow();">
    <input type="hidden" id="edit-parameter" name="parameter" />
    <input type="hidden" id="edit-signature" name="signature" />   
    <input type="hidden" id="display" name="display" value="manage" />
    
    <?php if(!empty($wizardId)): // optional wizard id ?>
    <input type="hidden" name="id" value="<?php echo $wizardId; ?>" />
    <?php endif; ?>
</form>
<?php end_slot(); ?>