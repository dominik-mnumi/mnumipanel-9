<div class="grey-bg clearfix" id="control-bar" style="opacity: 1;">   
    <div id="order-form-actions" class="container_12">
        <div class="float-left">
            <?php echo link_to(image_tag('/images/icons/fugue/navigation-180.png'). ' ' .__('Back'), '@orderList', 'class=big-button title="'.__('Back').'"'); ?>
        </div>
     
        <div class="float-right">
            <div class="float-left">                
                <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderDuplicate) && (OrderTable::getInstance()->canCreate())): ?>
                <button class="grey" type="button" id="duplicate_order_btn">
                    <?php echo __('Duplicate order'); ?>
                </button>
                <?php endif; ?>
                <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderReminder)): ?>
                <button class="grey" type="button" id="add_reminder_btn">
                    <?php echo __('Add reminder'); ?>
                </button> 
                <?php endif; ?>               
            </div>    
            
            <div id="steps" class="float-left button-menu">
                <?php include_component('order', 'steps',
                        array('orderObj' => $orderObj)); ?>
            </div>       
        </div>  
        
        <div id="order-form-actions-loader" class="loader">
            <?php echo image_tag('/images/loader.gif', array('height' => 34)); ?>
        </div>
    </div>
</div>