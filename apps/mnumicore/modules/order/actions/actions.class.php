<?php

/**
 * order actions.
 *
 * @package    mnumicore
 * @subpackage order
 * @author     Marek Dąbrowski
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class orderActions extends complexTablesActions
{

    /**
     * Executes index action. Shows orders list
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        parent::orderList($request);

        // gets order filter ( needed for customize partial price)
        $orderFilterArr = $request->getParameter('order_filters');

        // sets parameter to partial
        $this->displayTableFields['Price'] = array('label' => 'Price',
            'partial' => 'order/price',
            'attributes' => array('traderPrice' => isset($orderFilterArr['trader'])
                ? $orderFilterArr['trader']
                : false));

        $onlyActiveQuery = OrderTable::getInstance()->getActiveOrdersQuery();
        $this->customQuery = OrderTable::getInstance()->getLatestQuery($onlyActiveQuery);

        $this->layoutOptions['showCheckboxes'] = true;
        $this->layoutOptions['manyActionsDefaultLabel'] = 'Choose action...';
        $this->tableOptions = $this->executeTable();

        if ($request->isXmlHttpRequest()) {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }

    /**
     * Executes delete action. Deletes order.
     *
     * @param sfRequest $request A request object
     */
    public function executeDelete(sfWebRequest $request)
    {
        $order = $this->getRoute()->getObject();
        $order->delete();

        $this->getUser()->setFlash('info_data',
                array(
            'message' => 'Deleted successfully.',
            'messageType' => 'msg_success',
        ));
        $this->redirect('@orderList');
    }

    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Order';
        parent::executeDeleteMany($request);
    }

    public function executeEdit(sfWebRequest $request)
    {
        // prepares objects
        $orderObj = $this->getRoute()->getObject();
        $this->forward404Unless($orderObj->getClientId(), 'No client id for order');

        //package form (if order have package)
        if ($orderObj->hasPackage()) {
            $this->editPackageFormArr = new EditPackageForm($orderObj->getOrderPackage());
        }

        // calculation email
        $calculationForm = new SendCalculationForm($orderObj);
        $calculationForm->setDefault('to', $orderObj->getSfGuardUser()->getEmailAddress());

        // view objects
        $this->product = $orderObj->getProduct();
        $this->client = $orderObj->getClient();
        $this->clientTraderJson = sfGuardUserTable::getInstance()->getClientTraderJson();
        $this->orderTraderJson = $orderObj->getTraderJson();
        $this->orderTraderCalculationJson = $orderObj->getTraderCalculationJson();
        $this->otherCostJson = $orderObj->getOthersJson();
        $this->orderCostJson = $orderObj->getCostJson();
        $this->order = $orderObj;
        $this->order_id = $orderObj->getId();
        $this->form = new OrderForm($orderObj);
        $this->calculationForm = $calculationForm;
        $this->wizardId = sfConfig::get('app_wizard_id');
        $this->localFileServerEnabled = $request->getCookie('local_file_server_enable');

        // twig template
        if(NotificationTable::getInstance()->getByTemplateNameAndTypeName(
                NotificationTemplateTable::$calculation,
                NotificationTypeTable::$email))
        {
            // prepares calculation notification object (shortcodes)
            $calculationNotificationObj = new CalculationNotification(
                $this->getContext(),
                $orderObj,
                array(
                    'calculation' => $this->getPartial('order/orderEmail/generatedCalculation')
                ));

            $this->calculationSendTemplate = TwigNotificationManager::getInstance($this->getUser())
                    ->getTwigTemplateObj(NotificationTemplateTable::$calculation,
                    NotificationTypeTable::$email)
                    ->render($calculationNotificationObj->getShortcodesAsArgArr());
        }

        // WIZARD section
        if ($orderObj->getProduct()->getProductWizards()->count()) {
            $this->newWizardForm = $this->getNewWizardForm($orderObj);
            $this->editWizardForm = new WizardForm();
            $this->wizardEnabled = true;
        } else {
            $this->wizardEnabled = false;
        }

        if ($this->getRequest()->isMethod('post')) {
            $this->proceedEditForm($this->form);
            $this->getUser()->setFlash('info_data',
                    array('messageType' => 'info_green',
                'message' => 'Saved successfully.'));

            $this->redirect('@orderListEdit?id='.$this->form->getObject()->getId());
        }

        // sets template
        $this->setTemplate('form');

        $this->defaultPaymentId = PaymentTable::getInstance()->getDefaultOfficePayment()->getId();

        if ($this->getRequest()->isMethod('post')) {
            $this->result = $this->orderSaveResult($this->form->getObject(), $this->form->isValid());
            $this->proceedEditForm($this->form);
            $this->setTemplate('orderSaveResult');
        }

        $this->priceBlockForm = new BlockPricesForm(
                null,
                array('checked' => $orderObj->getPriceBlock()));

        $this->orderCalculationJson = null;

        $calculation = $this->prepareCalculation($orderObj);

        if ($calculation) {
            if ($orderObj->getPriceBlock() == true) {
                $calculation = $orderObj->mergeCalculatedFriendlyFormatWithCustomPrice($calculation);
            }

            $this->orderCalculationJson = json_encode($calculation);
        } else {
            $this->getUser()->setFlash('info_data', array(
                    'messageType' => 'msg_error',
                    'message' => 'Missing custom sizes: width or height',
                    'messageParam' => array())
            );
        }

        //discount
        $this->discount = $orderObj->getDiscount();

        $formActionUrlKey = $orderObj->isUneditable() ? 'orderChangeStatus' : 'orderSave';
        $this->formActionUrl = $this->generateUrl($formActionUrlKey);

        $this->defaultDisableAdvancedAccountancy = sfConfig::get('app_default_disable_advanced_accountancy', 0);

    }

    /**
     * Services calculation email sending.
     *
     * @param  sfWebRequest $request
     * @return json
     */
    public function executeReqSendEmailCalculation(sfWebRequest $request)
    {
        $result = array();
        $result['message'] = array();
        if ($request->isMethod('post')) {
            $paramArr = $request->getParameter('send_calculation');

            /** @var Order $order */
            $order = OrderTable::getInstance()->find($paramArr['id']);
            if ($order->getOrderStatusName() == OrderStatusTable::$draft &&
                OrderWorkflowTable::getInstance()
                    ->getTransition(
                        $order->getProduct(),
                        OrderStatusTable::$draft,
                        OrderStatusTable::$calculation)
            ) {
                $order->setOrderStatusName(OrderStatusTable::$calculation);
                $order->save();
                $result['message'][] = $this->getContext()->getI18N()->__('Order is now in "calculation" status');

            }

            /** @var SendInvoiceForm $form */
            $form = new SendCalculationForm($order);

            /** @var jsonForm $jsonForm */
            $jsonForm = new jsonForm($form, $this->getContext()->getI18N());

            $form->bind($paramArr);
            if ($form->isValid()) {
                $email = new EmailDecorator($this->getMailer());
                $email->setSender($order->getShopName());

                try {
                    $email->sendEmail(
                        $form['to']->getValue(),
                        $form['subject']->getValue(),
                        $form['calculation']->getValue()
                    );
                    $form->save();

                    $jsonForm->setSuccess('Email has been sent.');
                } catch (Exception $e) {
                    $jsonForm->setError($e->getMessage());
                }
            }
        }

        return $this->renderText($jsonForm->getMessages());
    }

    public function executeReqNewPackage(sfWebRequest $request)
    {
        $orderObj = $this->getRoute()->getObject();

        $packageObj = new OrderPackage();
        $packageObj->createAndConnectWithOrder($orderObj);

        // triggers the event
        $this->dispatcher->notify(new sfEvent($this,
                        'package.create',
                        array('obj' => $packageObj)));

        // gets package info
        $packageInfo = $this->getPartial('packagingInformations',
                        array('orderObj' => $orderObj));

        // gets package form
        $packageForm = $this->getPartial('package/form/editPackageForm',
                array('form' => new EditPackageForm($packageObj),
                    'defaultPaymentId' => PaymentTable::getInstance()->getDefaultOfficePayment()->getId()));

        return $this->renderText(json_encode(array(
            'status' => 'success',
            'packageInfo' => $packageInfo,
            'packageForm' => $packageForm)));
    }

    public function executeReqPackageInfo(sfWebRequest $request)
    {
        $orderObj = $this->getRoute()->getObject();

        // gets package info
        $packageInfo = $this->getPartial('packagingInformations',
                        array('orderObj' => $orderObj));

        $responseArr = array(
            'status' => 'success',
            'packageInfo' => $packageInfo);

        if ($orderObj->hasPackage()) {
            $packageObj = $orderObj->getOrderPackage();

            // gets package form
            $packageForm = $this->getPartial('package/form/editPackageForm',
                    array(
                        'form' => new EditPackageForm($packageObj),
                        'defaultPaymentId' => PaymentTable::getInstance()->getDefaultOfficePayment()->getId()));

            $responseArr['packageForm'] = $packageForm;
        }

        return $this->renderText(json_encode($responseArr));
    }

    /**
     * Change order status
     *
     * @param sfWebRequest $request A request object
     */
    public function executeChangeStatus(sfWebRequest $request)
    {
        $requestOrder = $request->getParameter('order');

        /** @var Order $orderObj */
        $orderObj = OrderTable::getInstance()->find($requestOrder['id']);

        try {
            $this->prepareOrder($orderObj, $request);
            $success = true;
        } catch (Exception $e) {
            $success = false;
        }

        $this->result = $this->orderSaveResult($orderObj, $success);
        $this->setTemplate('orderChangeStatusResult');
    }

    /**
     * Change order status
     *
     * @param sfWebRequest $request A request object
     */
    public function executeSave(sfWebRequest $request)
    {
        $requestOrder = $request->getParameter('order');

        /** @var Order $orderObj */
        $orderObj = OrderTable::getInstance()->find($requestOrder['id']);

        $this->forward404Unless($orderObj instanceOf Order);

        $productFields = $orderObj->getProduct()->getOrderFields();

        $this->form = new OrderForm($orderObj, array('fields' => $productFields));
        $this->prepareOrder($orderObj, $request, $this->form);

        // view objects
        $this->result = $this->orderSaveResult($orderObj, $this->form->isValid());

        $this->setTemplate('orderSaveResult');
    }

    /**
     * Prepare order for save
     *
     * @param type $orderObj
     * @param type $request
     * @param type $form
     */
    public function prepareOrder($orderObj, $request, $form = null)
    {
        $currentStatus = $orderObj->getOrderStatusName();
        $nextStatus = false;

        if ($request->hasParameter('nextStatus')) {
            // gets new status change
            $nextStatus = $request->getParameter('nextStatus');

            // checks permission for changing this status
            if ($this->getUser()->hasCredential(sfGuardPermissionTable::getOrderStatusPermission($nextStatus))) {
                $nextStatus = $nextStatus;
            }
        }

        if ($nextStatus) {
            $orderObj->setOrderStatusName($nextStatus);
            $orderObj->save();
        }

        if ($form) {
            $this->proceedEditForm($form);
        }

        if ($nextStatus) {
            if(($nextStatus == OrderStatusTable::$new ||
                $currentStatus == OrderStatusTable::$draft ||
                $currentStatus == OrderStatusTable::$calculation) &&
                $nextStatus !== OrderStatusTable::$calculation)
            {
                // checks if waiting package exists then sets
                $orderObj->connectToWaitingPackageOrCreateNew();
            } elseif ($nextStatus == OrderStatusTable::$realization) {
                // triggers the event
                $this->dispatcher->notify(new sfEvent($this,
                        'order.status.new',
                        array('obj' => $orderObj, 'webapi_key' => $orderObj->getShopName())));
            }

            // update client balance
            $client = $orderObj->getClient();
            $client->calculateBalance();
            $client->save();
        }
    }

    public function executeCreate(sfWebRequest $request)
    {
        if (!OrderTable::getInstance()->canCreate()) {
            throw new Exception('Order creation disabled. Licence condition problem.');
        }

        $productObj = ProductTable::getInstance()->find($request->getParameter('productId'));
        $clientObj = ClientTable::getInstance()->find($request->getParameter('clientId'));
        $userObj = sfGuardUserTable::getInstance()->find($request->getParameter('userId'));

        $this->forward404Unless($productObj, "Product does not exist");
        $this->forward404Unless($clientObj, "Client does not exist");
        $this->forward404Unless($userObj, "User does not exist");

        //create new order with basic data
        $orderObj = new Order();
        $orderObj->setProduct($productObj);
        $orderObj->setClient($clientObj);
        $orderObj->setSfGuardUser($userObj);
        $orderObj->setName($productObj->getName());
        $orderObj->setOrderStatusName('draft');
        $orderObj->setEditor($this->getUser()->getGuardUser());

        $orderObj->save();

        // if user has not edit permission then redirect to client edit again
        if (!$this->getUser()->hasCredential(sfGuardPermissionTable::$orderEdit)) {
            $this->getUser()->setFlash('info_data',
                    array('messageType' => 'info_green',
                'message' => 'Order created successfully.'));

            $this->redirect('clientListEdit', $clientObj);
        }

        $this->redirect('orderListEdit', $orderObj);
    }

    /**
     * Load calculation (paper, material & others)
     * for request parameters
     *
     * (output: REST)
     *
     * @param  sfWebRequest $request
     * @return string       Rest result
     */
    public function executeCalculation(sfWebRequest $request)
    {
        $orderObj = OrderTable::getInstance()->find($request->getParameter('order_id'));

        $this->getResponse()->setContentType('application/json');

        $calculation = $this->prepareCalculation($orderObj);

        if ($calculation === false) {
            $response = $this->getContext()->getI18N()->__('Missing custom sizes: width or height');
            $this->getResponse()->setStatusCode('400');
        } else {
            $response = json_encode($calculation);
        }

        return $this->renderText($response);
    }

    /**
     *
     */
    protected function prepareCalculation(Order $orderObj)
    {
        if (!$orderObj->isUneditable()) {

            $fieldArr = $this->getRequest()->getParameter('fields', array());

            // prepares fields array
            $postArray = $this->getCalculationFieldsUsingRestRequest($fieldArr);

            // prepares pricelist id
            $pricelistId = $this->getPricelistIdFromPostFields($fieldArr);

            // gets calculation report
            try {
                $report = $orderObj->calculate($postArray, $pricelistId);

                $decorator = new CalculationDecorator($this->getContext()->getI18N());
                $calculation = $decorator->getView($report);
            } catch (\Mnumi\Bundle\CalculationBundle\Library\Exception\CalculationMissingCustomSizeException $e) {
                $calculation = false;
            }
        } else {
            $calculation = [];
            foreach ($orderObj->getPrices() as $price) {
                $calculation[] = $price->asArray();
            }
        }

        return $calculation;
    }

    /**
     * Puts order to shelf and redirects back.
     *
     * @param  sfWebRequest $request
     * @return string
     */
    public function executeOrderToShelf(sfWebRequest $request)
    {
        /** @var Order $orderObj */
        $orderObj = $this->getRoute()->getObject();

        $result = $orderObj->orderToShelf($this->getUser());

        $this->getUser()->setFlash('info_data', $result['message']);

        $orderPackageObj = $orderObj->getOrderPackage();

        // send notification
        if (isset($result['readyEvent']) && $result['readyEvent'] && $orderPackageObj->getOrderPackageStatusName() == OrderPackageStatusTable::$completed) {
            // triggers the event
            $this->dispatcher->notify(new sfEvent($this,
                        'package.ready',
                        array('obj' => $orderPackageObj, 'webapi_key' => $orderObj->getShopName())));
        }

        // if ajax request
        if ($request->isXmlHttpRequest()) {
            $this->setLayout(false);

            // if to book is set
            $url = $orderPackageObj->getToBook()
                ? $this->generateUrl('clientListEdit', $orderObj->getClient()).'#tab-orders'
                : $this->generateUrl('clientPackageDetails', $orderPackageObj);

            $this->resultArr = array(
                'result' => array(
                    'redirectUrl' => $url));

            return 'Json';
        } else {
            // redirects back to package
            $this->redirect('clientPackageDetails',
                    array(
                'id' => $orderObj->getOrderPackage()->getId(),
                'client_id' => $orderObj->getClientId()
            ));
        }
    }

    /**
     * Executes Changelog action. List of order changes.
     *
     * @param sfRequest $request A request object
     */
    public function executeChangelog(sfWebRequest $request)
    {
        /** @var Order $invoice */
        $order = $this->getRoute()->getObject();

        $this->differences = $order->getChanges();
    }

    /**
     * Executes ReqMoveOrderToPackage action. Moves order from one package to other.
     *
     * @param sfRequest $request A request object
     */
    public function executeReqMoveOrderToPackage(sfWebRequest $request)
    {
        if ($request->isXmlHttpRequest()) {
            // gets parameters
            $toPackageId = $request->getParameter('to');
            $orderId = $request->getParameter('id');

            if (!$toPackageId || !$orderId) {
                throw new InvalidArgumentException('To package id or order id is undefined.');
            }

            // gets toPackage object
            $toPackageObj = OrderPackageTable::getInstance()->find($toPackageId);

            // gets order object and saves it
            $orderObj = OrderTable::getInstance()->find($orderId);

            // gets order package because necessary to flash
            $fromPackageObj = $orderObj->getOrderPackage();

            // if all packages completed
            if ($fromPackageObj->isCompleted() && $toPackageObj->isCompleted()) {
                // sets message
                $this->getUser()->setFlash('info_data', array(
                    'messageType' => 'msg_error',
                    'message' => 'Order number %id% cannot be moved from package %from% to %to% bacause both packages are already completed',
                    'messageParam' => array(
                        '%from%' => $fromPackageObj->getId(),
                        '%to%' => $toPackageObj->getId(),
                        '%id%' => $orderObj->getId()
                    )
                ));
            }
            // if from package completed
            elseif ($fromPackageObj->isCompleted() || $fromPackageObj->isPaid()) {
                // sets message
                $this->getUser()->setFlash('info_data', array(
                    'messageType' => 'msg_error',
                    'message' => 'Order number %id% cannot be moved from package %from% to %to% bacause package %other% is already completed or is set as paid',
                    'messageParam' => array(
                        '%from%' => $fromPackageObj->getId(),
                        '%to%' => $toPackageObj->getId(),
                        '%other%' => $fromPackageObj->getId(),
                        '%id%' => $orderObj->getId()
                    )
                ));
            } elseif($fromPackageObj->hasUnmovableOrders()) {
                $this->getUser()->setFlash('info_data', array(
                    'messageType' => 'msg_error',
                    'message' => 'Order number %id% cannot be moved from package %from% to %to%',
                    'messageParam' => array(
                        '%from%' => $fromPackageObj->getId(),
                        '%to%' => $toPackageObj->getId(),
                        '%id%' => $orderObj->getId()
                    )
                ));
            } elseif ($toPackageObj->isCompleted() || $toPackageObj->isPaid()) {
                // sets message
                $this->getUser()->setFlash('info_data', array(
                    'messageType' => 'msg_error',
                    'message' => 'Order number %id% cannot be moved from package %from% to %to% bacause package %other% is already completed or is set as paid',
                    'messageParam' => array(
                        '%from%' => $fromPackageObj->getId(),
                        '%other%' => $toPackageObj->getId(),
                        '%to%' => $toPackageObj->getId(),
                        '%id%' => $orderObj->getId()
                    )
                ));
            } elseif ($toPackageObj->isBooked()) {
                // sets message
                $this->getUser()->setFlash('info_data', array(
                    'messageType' => 'msg_error',
                    'message' => 'Order number %id% cannot be moved from package %from% to %to% bacause package %other% is already booked',
                    'messageParam' => array('%from%' => $fromPackageObj->getId(),
                        '%other%' => $toPackageObj->getId(),
                        '%to%' => $toPackageObj->getId(),
                        '%id%' => $orderObj->getId()
                    )
                ));
            } else {
                $orderObj->setOrderPackageId($toPackageObj->getId());
                $orderObj->setShelfId(null);

                $conn = Doctrine_Manager::connection();
                try {
                    $conn->beginTransaction();

                    $orderObj->save();

                    if ($fromPackageObj->getOrdersNotOnShelfArray() === false) {
                        $fromPackageObj->setOrderPackageStatusName(OrderPackageStatusTable::$completed);
                        $fromPackageObj->save();
                    }

                    $conn->commit();
                } catch (Exception $e) {
                    $conn->rollback();
                    throw new Exception(get_class($this).' -> '.__METHOD__.' - '.$e->getMessage());
                }

                // sets message
                $this->getUser()->setFlash('info_data', array(
                    'messageType' => 'msg_success',
                    'message' => 'Order number %id% moved from package %from% to %to% successfully',
                    'messageParam' => array(
                        '%from%' => $fromPackageObj->getId(),
                        '%to%' => $toPackageObj->getId(),
                        '%id%' => $orderObj->getId()
                    )
                ));
            }
        }

        return $this->renderText(json_encode(
                array('redirectUrl' => $this->generateUrl('clientPackageDetails',
                $fromPackageObj->getOrders()->count()
                    ? $fromPackageObj
                    : $toPackageObj))));
    }

    /**
     * Export all order list as file.
     *
     * @param  sfWebRequest $request
     * @return string
     */
    public function executeExportOrderListAll(sfWebRequest $request)
    {
        $sfFormat = $request->getParameter('sf_format');
        $filterParamArr = $request->getParameter('order_filters');

        $filterForm = new OrderFormFilter();
        $filterForm->bind($filterParamArr);
        if ($filterForm->isValid()) {
            $query = $filterForm->getQuery();
        }

        $orderColl = $query->execute();
        $this->getExportOrderListResponse($orderColl, $sfFormat);

        return sfView::NONE;
    }

    /**
     * Export selected order list as file.
     *
     * @param  sfWebRequest $request
     * @return string
     */
    public function executeExportOrderList(sfWebRequest $request)
    {
        $sfFormat = $request->getParameter('sf_format');
        $orderIdArray = $request->getParameter('selected');

        if ($orderIdArray == '') {
            $this->getUser()->setFlash('info_data', array(
                'messageType' => 'msg_error',
                'message' => 'Please select some items.',
            ));
            $this->redirect('@orderList');
        }

        $orderColl = OrderTable::getInstance()->getOrderColl($orderIdArray);
        $this->getExportOrderListResponse($orderColl, $sfFormat);

        return sfView::NONE;
    }

    /**
     * Get order list response
     *
     * @param array $orderColl
     * @param string $sfFormat
     * @return sfResponse
     */
    private function getExportOrderListResponse($orderColl, $sfFormat)
    {
        $exportEncoding = sfConfig::get('app_default_export_encoding', 'windows-1250');
        $timeStart = microtime(true);
        $items = array();
        /** @var Order $order */
        foreach ($orderColl as $order) {
            $items[] = array(
                'id' => $order->getId(),
                'name' => $order->getName(),
                'client_id' => $order->getClient()->getId(),
                'client_name' => $order->getClient()->getFullname(),
                'client_street' => $order->getClient()->getStreet(),
                'client_postcode' => $order->getClient()->getPostcode(),
                'client_city' => $order->getClient()->getCity(),
                'client_tax_id' => $order->getClient()->getTaxId(),
                'client_country' => $order->getClient()->getCountry(),
                'user_id' => $order->getSfGuardUser()->getId(),
                'user_name' => $order->getSfGuardUser()->getFirstName().' '.$order->getSfGuardUser()->getLastName(),
                'user_email' => $order->getSfGuardUser()->getEmailAddress(),
                'user_phone' => ($order->getSfGuardUser()->getDefaultContactByType(NotificationTypeTable::$sms)) ? $order->getSfGuardUser()->getDefaultContactByType(NotificationTypeTable::$sms)->getValue() : '',
                'order_status' => $order->getOrderStatus()->getTitle(),
                'order_created_date' => $order->getCreatedAt(),
                'base_amount' => $order->getBaseAmount(),
                'discount_amount' => $order->getDiscountAmount(),
                'total_amount' => $order->getTotalAmount(),
                'total_amount_gross' => $order->getTotalAmountGross(),
                'product_name' => $order->getProduct()->getName(),
                'delivery_name' => $order->getOrderPackage()->getDeliveryName(),
                'delivery_street' => $order->getOrderPackage()->getDeliveryStreet(),
                'delivery_postcode' => $order->getOrderPackage()->getDeliveryPostcode(),
                'delivery_city' => $order->getOrderPackage()->getDeliveryCity(),
                'delivery_description' => $order->getOrderPackage()->getDescription(),
                'payment_id' => $order->getOrderPackage()->getPayment()->getName(),
                'invoice_number' => ($order->getInvoice())?$order->getInvoice()->getName():'',
                'invoice_steet' => $order->getOrderPackage()->getInvoiceStreet(),
                'invoice_postcode' => $order->getOrderPackage()->getInvoicePostcode(),
                'invoice_city' => $order->getOrderPackage()->getInvoiceCity(),
                'invoice_tax_id' => $order->getOrderPackage()->getInvoiceTaxId(),
                'invoice_info' => $order->getOrderPackage()->getInvoiceInfo()
            );
        }

        $columnsHeaders = array(
            'id' => 'Order number',
            'name' => 'Order name',
            'client_id' => 'Client number',
            'client_name' => 'Client name',
            'client_street' => 'Client street/no.',
            'client_postcode' => 'Client postcode',
            'client_city' => 'Client city',
            'client_tax_id' => 'Client tax id',
            'client_country' => 'Client country',
            'user_id' => 'User number',
            'user_name' => 'User firstname and lastname',
            'user_email' => 'User email',
            'user_phone' => 'User phone number',
            'order_status' => 'Order status',
            'order_created_date' => 'Order created',
            'base_amount' => 'Base amount',
            'discount_amount' => 'Discount amount',
            'price_net' => 'Order net amount',
            'price_gross' => 'Order gross amount',
            'product_name' => 'Product name',
            'delivery_name' => 'Delivery name',
            'delivery_street' => 'Delivery street/no.',
            'delivery_postcode' => 'Delivery postcode',
            'delivery_city' => 'Delivery city',
            'delivery_description' => 'Delivery description',
            'payment_id' => 'Payment name',
            'invoice_number' => 'Invoice number',
            'invoice_steet' => 'Invoice street/no.',
            'invoice_postcode' => 'Invoice postcode',
            'invoice_city' => 'Invoice city',
            'invoice_tax_id' => 'Invoice tax',
            'invoice_info' => 'Invoice information'
        );
        $timeEnd = microtime(true);
        $content = $this->getPartial('global/exportList',
            array('items' => $items,
                'buildTime' => $timeEnd - $timeStart,
                'columnsHeaders' => $columnsHeaders,
                'title' => 'Order list'
            )
        );

        $content = iconv('UTF-8', $exportEncoding.'//TRANSLIT', $content);
        $response = $this->getResponse();
        $response->setContentType('text/'.$sfFormat);
        $response->setHttpHeader('Content-Disposition',
                'attachment; filename="orderListTo'
                .ucfirst($sfFormat)
                .'_'.date("Y-m-d_G:i:s").'.'.$sfFormat.'"');

        $response->setContent($content);
    }

    /**
     * Add wizard.
     *
     * @param sfRequest $request A request object
     */
    public function executeAddWizardOrder(sfWebRequest $request)
    {
        // gets parameters
        $projectName = $request->getParameter('projectName');
        $productSlug = $request->getParameter('productSlug');
        $orderId = $request->getParameter('orderId');

        // gets order object
        $orderObj = OrderTable::getInstance()->find($orderId);

        // adds wizard to order
        $orderObj->addWizardOrder($projectName, $productSlug);

        return sfView::NONE;
    }

    /**
     * Deletes wizard from list.
     *
     * @param sfRequest $request A request object
     */
    public function executeDeleteWizardOrder(sfWebRequest $request)
    {
        $projectName = $request->getParameter('projectName');
        $orderId = $request->getParameter('orderId');

        // gets order object
        $orderObj = OrderTable::getInstance()->find($orderId);

        // deletes wizard from order
        $orderObj->deleteWizardOrder($projectName);

        return sfView::NONE;
    }

    /**
     * Execute BlockPrices action
     *
     * @param sfRequest $request A request object
     */
    public function executeBlockPrices(sfWebRequest $request)
    {
        $orderObj = $this->getRoute()->getObject();
        if (!$request->hasParameter('blockPrices')) {
            throw new Exception('Block prices value must be set');
        }

        $orderObj->setPriceBlock($request->getParameter('blockPrices'));
        $orderObj->save();

        return sfView::NONE;
    }

    /**
     * Execute setCustomPrice action
     *
     * @param sfRequest $request A request object
     */
    public function executeSetCustomPrice(sfWebRequest $request)
    {
        $orderId = $request->getParameter('order_id', '');
        $fieldsetId = $request->getParameter('fieldset_id', '');
        $fieldItemId = $request->getParameter('field_item_id', '');
        $price = $request->getParameter('price', 0);

        $this->forward404Unless($orderId, 'Incorrect order ID: '.$orderId);
        $this->forward404Unless($fieldsetId,
                'Incorrect fieldset ID: '.$fieldsetId);
        $this->forward404Unless(is_numeric($price), 'Wrong price : '.$price);

        $key = $orderId.';'.$fieldsetId.';'.$fieldItemId;

        if ($this->getUser()->hasAttribute('fixedPrice')) {
            $customPrices = $this->getUser()->getAttribute('fixedPrice');
            $customPrices[$key] = $price;
            $this->getUser()->setAttribute('fixedPrice', $customPrices);
        } else {
            $this->getUser()->setAttribute('fixedPrice', array($key => $price));
        }

        return sfView::NONE;
    }

    /**
     * Execute duplicate action
     *
     * @param sfRequest $request A request object
     */
    public function executeDuplicate(sfWebRequest $request)
    {
        /** @var Order $orderObj */
        $orderObj = $this->getRoute()->getObject();
        $duplicateOrderObj = $orderObj->duplicate();

        return $this->renderText(json_encode(array('result' => $duplicateOrderObj->toArray())));
    }

    /**
     * Executes action after "mWizard" button click (ajax).
     *
     * @param sfWebRequest $request
     */
    public function executeReqWizardEncodedParamAndSignature(sfWebRequest $request)
    {
        $productName = $request->getParameter('productName');
        $count = $request->getParameter('count');
        $countChange = $request->getParameter('countChange');
        $orderId = $request->getParameter('id');
        $barcode = $request->getParameter('barcode');

        // gets order object
        $orderObj = OrderTable::getInstance()->find($orderId);

        $resultArr = WizardManager::getInstance()
                ->generateNewWizardEncodedParamAndSignature($productName,
                        $countChange, $count, $orderId, $barcode,
                        $orderObj->getProduct()->getProductWizardsSeperatedByComma());

        return $this->renderText(json_encode(array(
            'status' => 'success',
            'result' => array('encodedParameter' => $resultArr['encodedParameter'],
            'signature' => $resultArr['signature']))));
    }

    /**
     * Proceed form save for edit order
     *
     * @param object $form
     */
    private function proceedEditForm(OrderForm $form)
    {
        if (!$this->getRequest()->isMethod('post')) {
            return;
        }

        $parameterArray = $this->getRequest()->getParameter($form->getName());

        // is not neccessary for save proccess (but can be in future)
        unset($parameterArray['system_price_net']);
        unset($parameterArray['price_net']);

        // if no pricelist credentials then unset this field
        if (!$this->getUser()->hasCredential(sfGuardPermissionTable::$orderPricelistChange)) {
            unset($form['pricelist_id']);
        }

        $form->bind($parameterArray);

        if ($form->isValid()) {
            if (!$form->getObject()->isNew()) {
                /** @var Order $order */
                $order = $form->getObject();

                $orderId = $form->getObject()->getId();

                // TRADERS SECTION
                // checks permissions for order trader
                if ($this->getUser()->hasCredential(sfGuardPermissionTable::$orderTrader)) {
                    // gets traders from parameters
                    $traderFormArr = isset($parameterArray['trader'])
                        ? $parameterArray['trader']
                        : array();

                    // gets traders from database
                    $traderColl = OrderTraderTable::getInstance()
                        ->findByOrderId($orderId);

                    // synchronizes with database
                    $this->synchronizeTradersOrCostsWithDb('OrderTraderForm', $traderColl, $traderFormArr, $orderId);
                }

                // COST SECTION
                // checks permissions for order cost
                if ($this->getUser()->hasCredential(sfGuardPermissionTable::$orderCost)) {
                    // gets traders from parameters
                    $costFormArr = isset($parameterArray['cost'])
                        ? $parameterArray['cost']
                        : array();

                    // gets costs from database
                    $costColl = OrderCostTable::getInstance()
                        ->findByOrderId($orderId);

                    // synchronizes with database
                    $this->synchronizeTradersOrCostsWithDb('OrderCostForm', $costColl, $costFormArr, $orderId);

                }

                // checks permissions for block price
                if ($this->getUser()->hasCredential(sfGuardPermissionTable::$orderBlockPrice)) {
                    // get prices from request
                    $prices = $this->getRequest()->getParameter('price', array());
                } else {
                    // calculate order price
                    try {
                        $calculation = $order->calculate();
                    } catch (Exception $e) {
                    }

                    $prices = $calculation['priceItems'];
                }

                $form->getObject()->setPrices($prices);
                $form->getObject()->updatePrices();
            }

            $form->save();
        }
    }

    private function doCheckNewOrderToArray($array, $order)
    {
        $resultArray['priceBlock'] = $order->getPriceBlock();
        $resultArray['orderId'] = $order->getId();

        $resultArray['summaryPriceNet'] = $array['summaryPriceNet'] + $array['trader']['tradersTotalPrice'];
        $resultArray['priceTax'] = $array['priceTax'];
        $resultArray['summaryPriceVat'] = $resultArray['summaryPriceNet'] + $resultArray['summaryPriceNet'] * $resultArray['priceTax'];
        $resultArray['factor'] = (float) $array['factor'];
        $resultArray['count'] = array('label' => $array['count']['label'],
            'value' => $array['count']['value']);
        $resultArray['quantity'] = array('label' => $array['quantity']['label'],
            'value' => $array['quantity']['value']);
        $resultArray['size'] = array('label' => $array['size']['label'],
            'fieldLabel' => $array['size']['fieldLabel']);
        $resultArray['sides'] = array('label' => $array['sides']['label'],
            'fieldLabel' => $array['sides']['fieldLabel']);
        $resultArray['printSizeWidth'] = (int) $array['printSizeWidth'];
        $resultArray['printSizeHeight'] = (int) $array['printSizeHeight'];
        $resultArray['printerWidth'] = (int) $array['printerWidth'];
        $resultArray['printerHeight'] = (int) $array['printerHeight'];
        $resultArray['printerName'] = $array['printerName'];
        $resultArray['measureType'] = $array['measureType'];
        $resultArray['fixedPrice'] = $array['fixedPrice'];
        $resultArray['pricelistId'] = $array['pricelistId'];
        $resultArray['printerPageNb'] = $array['printerPageNb'];
        $resultArray['square_metre'] = $resultArray['printSizeHeight'] / 1000
                * $resultArray['printSizeWidth'] / 1000
                * $resultArray['quantity']['value']
                * $resultArray['count']['value'];

        $resultArray['priceItems']['COUNT']['name'] = 'COUNT';
        $resultArray['priceItems']['COUNT']['label'] = $array['count']['label'];
        $resultArray['priceItems']['COUNT']['price'] = 0;
        $resultArray['priceItems']['COUNT']['fieldLabel'] = $array['count']['value'];

        $resultArray['priceItems']['QUANTITY']['name'] = 'COUNT';
        $resultArray['priceItems']['QUANTITY']['label'] = $array['quantity']['label'];
        $resultArray['priceItems']['QUANTITY']['price'] = 0;
        $resultArray['priceItems']['QUANTITY']['fieldLabel'] = $array['quantity']['value'];

        $resultArray['priceItems']['MATERIAL']['name'] = $array['priceItems']['MATERIAL']['name'];
        $resultArray['priceItems']['MATERIAL']['label'] = $array['priceItems']['MATERIAL']['label'];
        $resultArray['priceItems']['MATERIAL']['price'] = (float) $array['priceItems']['MATERIAL']['price'];
        $resultArray['priceItems']['MATERIAL']['fieldLabel'] = $array['priceItems']['MATERIAL']['fieldLabel'];
        $resultArray['priceItems']['MATERIAL']['fieldId'] = $array['priceItems']['MATERIAL']['fieldId'];

        $resultArray['priceItems']['PRINT']['name'] = $array['priceItems']['PRINT']['name'];
        $resultArray['priceItems']['PRINT']['label'] = $array['priceItems']['PRINT']['label'];
        $resultArray['priceItems']['PRINT']['price'] = (float) $array['priceItems']['PRINT']['price'];
        $resultArray['priceItems']['PRINT']['fieldLabel'] = $array['priceItems']['PRINT']['fieldLabel'];
        $resultArray['priceItems']['PRINT']['fieldId'] = $array['priceItems']['PRINT']['fieldId'];

        $resultArray['priceItems']['SIZE']['name'] = 'SIZE';
        $resultArray['priceItems']['SIZE']['label'] = $array['size']['label'];
        $resultArray['priceItems']['SIZE']['price'] = 0;
        $resultArray['priceItems']['SIZE']['fieldLabel'] = $array['size']['fieldLabel'];

        $resultArray['priceItems']['SIDES']['name'] = 'SIDES';
        $resultArray['priceItems']['SIDES']['label'] = $array['sides']['label'];
        $resultArray['priceItems']['SIDES']['price'] = 0;
        $resultArray['priceItems']['SIDES']['fieldLabel'] = $array['sides']['fieldLabel'];

        $resultArray['trader'] = $array['trader'];
        $i = 0;

        //if OTHER's exist
        if (@count($array['priceItems']['OTHER']) > 0) {
            foreach ($array['priceItems']['OTHER'] as $rec) {
                $resultArray['priceItems']['OTHER'][$i]['name'] = $rec['name'];
                $resultArray['priceItems']['OTHER'][$i]['label'] = $rec['label'];
                $resultArray['priceItems']['OTHER'][$i]['price'] = (float) $rec['price'];
                $resultArray['priceItems']['OTHER'][$i]['fieldLabel'] = $rec['fieldLabel'];
                $resultArray['priceItems']['OTHER'][$i]['fieldId'] = $rec['fieldId'];
                $resultArray['priceItems']['OTHER'][$i]['fieldItemId'] = $rec['fieldItemId'];
                $i++;
            }
        }

        return $resultArray;
    }

    /**
     * Converts post parameters to calculation-understand parameters.
     *
     * @param  array $fieldArr
     * @return array
     */
    private function getCalculationFieldsUsingRestRequest($fieldArr = array())
    {
        $postArray = array();
        $other_i = 0;
        foreach ($fieldArr as $options_key => $options) {
            if(!is_array($options))
                continue;
            foreach ($options as $option_key => $option) {
                if (is_array($option) && $option_key == 'other') {
                    if($option_key == '_csrf_token')
                        continue;
                    foreach ($option as $val) {
                        $postArray['OTHER'][$other_i] = array(
                            'name' => strtoupper($option_key),
                            'value' => $val
                        );
                        $other_i++;
                    }
                } elseif(preg_match('/^order\[attributes\]\[(\w+)\]\[?(\d+)?\]?/i',
                                $option, $result))
                {
                    $key = $result[1];
                    if('_' == substr($key, 0, 1))
                        continue;
                    if('informations' == $key)
                        continue;

                    if ('size_width' == $key) {
                        $postArray['SIZE']['width'] = array('name' => strtoupper($key),
                            'value' => $options['value']);
                        continue;
                    }
                    if ('size_height' == $key) {
                        $postArray['SIZE']['height'] = array('name' => strtoupper($key),
                            'value' => $options['value']);
                        continue;
                    }

                    $post_data_array = array(
                        'name' => strtoupper($key),
                        'value' => $options['value'],
                    );

                    //some validation for other field values
                    if ('other' == $key && !empty($post_data_array['value'])) {
                        $other_id = $result[2];

                        $postArray[strtoupper($key)][$other_id] = $post_data_array;
                        continue;
                    } elseif ('other' == $key) {
                        continue;
                    }

                    $postArray[strtoupper($key)] = $post_data_array;
                } elseif(preg_match('/^order\[trader\]\[(\d+)\]\[?(\w+)?\]?/i',
                                $option, $result))
                {
                    $value = $options['value'];
                    $postArray['TRADER'][$result[1]][$result[2]] = $value;
                } else {
                    if($option_key == '_csrf_token')
                        continue;

                    //sets custom size properly
                    if ('size_width' == $option_key) {
                        $postArray['SIZE']['width'] = $option;
                        continue;
                    }
                    if ('size_height' == $option_key) {
                        $postArray['SIZE']['height'] = $option;
                        continue;
                    }

                    $postArray[strtoupper($option_key)] = array(
                        'name' => strtoupper($option_key),
                        'value' => $option
                    );
                }
            }
        }

        return $postArray;
    }

    /**
     * Returns pricelsit id basing on post parameters.
     *
     * @param  array $fieldArr
     * @return mixed
     */
    private function getPricelistIdFromPostFields($fieldArr = array())
    {
        foreach ($fieldArr as $optionArr) {
            if(!is_array($optionArr))
                continue;
            if(preg_match('/^order\[pricelist_id\]$/i', $optionArr['name']))

            {
                return $optionArr['value'];
            }
        }

        return null;
    }

    private function exportTradersFromPostArray(array $array)
    {
        $total = 0;
        $traderArr = array();
        if (@count($array['TRADER']) > 0) {
            foreach ($array['TRADER'] as $key => $rec) {
                $traderArr[$key] = array(
                    'quantity' => $rec['quantity'],
                    'price' => $rec['price'],
                    'name' => OrderTraderTable::getInstance()->findOneByUserId($rec['user_id'])->getSfGuardUser()->__toString(),
                    'description' => $rec['description']
                );

                $total += $rec['quantity'] * $rec['price'];
            }
        }

        return array('tradersTotalPrice' => $total,
            'traders' => $traderArr);
    }

    /**
     * Returns new wizard form with prepared defaults.
     *
     * @param  Order      $orderObj
     * @return WizardForm
     */
    private function getNewWizardForm(Order $orderObj)
    {
        // gets prepared parameter
        $parameterDecoded = WizardManager::getPreparedArrayParameter(
                $orderObj->getProduct()->getSlug(),
                WizardManager::getNewBackUrl(),
                'initOrder',
                null,
                null,
                null,
                null,
                null,
                null,
                $orderObj->getId(),
                $orderObj->getBarcode());

        // creates signature manager and mandatory encoded parameters
        $signatureManager = new SignatureManager(WizardManager::getSecretKey());
        $encodedParameter = $signatureManager->encodeParameter($parameterDecoded);
        $signature = $signatureManager->generateSignature($parameterDecoded);

        // creates defaults
        $defaults = array(
            'parameter' => $encodedParameter,
            'signature' => $signature);

        return new WizardForm($defaults);
    }

     /**
     * Return result data
     *
     * @param  object $form
     * @return array
     */
    private function orderSaveResult($orderObj, $isValid)
    {
        // gets client and order traders
        $clientTraderJson = sfGuardUserTable::getInstance()->getClientTraderJson();
        $orderTraderJson = $orderObj->getTraderJson();
        $formActionUrlKey = $orderObj->isUneditable() ? 'orderChangeStatus' : 'orderSave';

        $result = array(
            'formActionUrl' => $this->generateUrl($formActionUrlKey),
            'type' => $isValid ? 'success' : 'error',
            'message' => $isValid ? "Saved successfully." : "Errors while saving form.",
            'buttons' => $this->getComponent(
                    'order',
                    'steps',
                    array('orderObj' => $orderObj)),
            'icon_url' => $orderObj->getOrderStatusIcon(),
            'currentStatus' => $orderObj->getOrderStatusName(),
            'isUneditable' => $orderObj->isUneditable(),
            'clientTraders' => $clientTraderJson,
            'orderTraders' => $orderTraderJson
        );

        return $result;
    }

    /**
     * Synchronizes traders or costs with db.
     *
     * @param $coll
     * Available collections:
     * - OrderTrader
     * - OrderCost
     *
     * @param string  $formClassName
     * @param array   $formArr
     * @param integer $orderId
     */
    private function synchronizeTradersOrCostsWithDb($formClassName, $coll, $formArr, $orderId)
    {
        // foreach rec from database
        foreach ($coll as $rec) {
            $deleted = true;

            // foreach rec from form
            foreach ($formArr as $rec2) {
                // sets values for form
                $valueArray = $rec2;
                $valueArray['order_id'] = $orderId;

                // if rec exists in database
                if ($rec->getId() == $rec2['id']) {
                    // edit order rec form
                    $form = new $formClassName($rec);
                    $form->bind($valueArray);

                    // if order rec form valid
                    if ($form->isValid()) {
                        // sets false for trader edit
                        $deleted = false;
                        $form->save();

                        break;
                    }
                }
            }

            // if no existed trader in form then delete
            if ($deleted) {
                $rec->delete();
            }
        }

        // foreach new rec - save form if valid
        foreach ($formArr as $rec2) {
            // if not numeric then means new trader (temp0)
            if (preg_match('/temp(\d+)/', $rec2['id'])) {
                $valueArray = $rec2;
                $valueArray['id'] = null;
                $valueArray['order_id'] = $orderId;

                unset($valueArray['id']);

                $form = new $formClassName();
                $form->bind($valueArray);

                // if order rec form valid
                if ($form->isValid()) {
                    $form->save();
                }
            }
        }
    }
}
