<?php

/**
 * order components.
 *
 * @package    mnumicore
 * @subpackage order
 */
class orderComponents extends sfComponents
{

    /**
     * Executes generateOrderFormSteps action
     *
     * @param sfWebRequest $request A request object
     */
    public function executeSteps(sfWebRequest $request)
    {
        // gets app.yml parameter
        $defaultDisableAdvancedAccountancy = sfConfig::get('app_default_disable_advanced_accountancy', 0);

        $statusArr = array();
        $nextStatusArr = array();
        $previousStatusArr = array();

        // gets next workflows
        $nextWorkflowColl = $this->orderObj->getNextWorkflows();

        // only save status
        if($nextWorkflowColl->count() == 0)
        {
            if($this->orderObj->isUneditable())
            {
                $statusArr['button'] = array(
                'imagePath' => 'icons/fugue/navigation-000-white.png',
                'title'     => 'Actions',
                'name'      => '',
                'id'        => '');
            }
            else
            {
                $statusArr['button'] = array(
                    'imagePath' => 'icons/fugue/tick-circle.png',
                    'title'     => 'Save',
                    'name'      => '',
                    'id'        => 'saveAndReturn');
            }
        }
        else
        {
            // first save status
            if(!$this->orderObj->isUneditable()) {
                $nextStatusArr[] = array(
                    'imagePath'   => 'icons/fugue/tick-circle.png',
                    'title'       => 'Save',
                    'name'        => '',
                    'id'          => 'saveAndReturn',
                    'iconClassLi' => 'icon_save');
            }

            // separator
            $nextStatusArr[] = array(
                'imagePath'   => '',
                'title'       => '',
                'name'        => '',
                'id'          => '',
                'iconClassLi' => 'sep');

            foreach($nextWorkflowColl as $key => $nextWorkflowObj)
            {
                // if next workflow exists and user has credentials
                if($this->getUser()
                                ->hasCredential(sfGuardPermissionTable::getOrderStatusPermission(
                                                $nextWorkflowObj->getOrderStatus()->getName())))
                {
                    // gets OrderStatus
                    $nextStatusObj = $nextWorkflowObj->getOrderStatus();

                    if($nextStatusObj->getName() == 'closed' && !$this->orderObj->isReady()){
                        continue;
                    }
                    
                    // for first next status generate button
                    if($key == 0)
                    {
                        $statusArr['button'] = array(
                            'imagePath'   => 'icons/orderStatus/16/'.$nextStatusObj->getIconNameBasedOnStatus(),
                            'title'       => $nextWorkflowObj->getTitle(),
                            'name'        => $nextWorkflowObj->getNextStatus(),
                            'id'          => '',
                            'iconClassLi' => 'icon_'.$nextStatusObj->getName());
                    }
                    else
                    {
                        $nextStatusArr[] = array(
                            'imagePath'   => 'icons/orderStatus/16/'.$nextStatusObj->getIconNameBasedOnStatus(),
                            'title'       => $nextWorkflowObj->getTitle(),
                            'name'        => $nextWorkflowObj->getNextStatus(),
                            'id'          => '',
                            'iconClassLi' => 'icon_'.$nextStatusObj->getName());
                    }
                }
            }
            if(!isset($statusArr['button']) || ($statusArr['button'] === null))
            {
                $statusArr['button'] = array(
                    'imagePath' => 'icons/fugue/navigation-000-white.png',
                    'title'     => 'Actions',
                    'name'      => '',
                    'id'        => '');
            }
        }

        // gets order invoice
        $orderInvoiceObj = $this->orderObj->getInvoice();

        // if advanced accountancy
        if(!$defaultDisableAdvancedAccountancy)
        {
            // if order invoice exists
            if($orderInvoiceObj)
            {
                $title = $this->getContext()->getI18N()->__('Open invoice %nr%',
                        array(
                            '%nr%' => $orderInvoiceObj->__toString()));

                $cssClassA = '';
                $disabled = '';
                $href = $this->generateUrl('invoiceListEdit', $orderInvoiceObj);
            }
            // otherwise checks if package exist
            else
            {
                $title = $this->orderObj->getOrderPackageId()
                        ? 'To invoicing'
                        : 'To invoicing (no package)';

                $cssClassA = 'to-invoicing';
                $disabled = !$this->orderObj->getOrderPackageId() ? TRUE : FALSE;
                $href = '';
            }

            // to invoice
            $nextStatusArr[] = array(
                'imagePath'   => '',
                'title'       => $title,
                'name'        => OrderStatusTable::$ready,
                'id'          => 'to-invoice',
                'iconClassLi' => 'icon_invoice',
                'cssClassA'   => $cssClassA,
                'disabled'    => $disabled,
                'href'        => $href);
        }

        $statusArr['button']['menu']['nextStatuses'] = $nextStatusArr;

        // gets previous statuses
        $previousWorkflowColl = $this->orderObj->getPreviousWorkflows();       
        if($previousWorkflowColl->count() > 0 && count($nextStatusArr) > 0)
        {
            // separator
            $previousStatusArr[] = array(
                'imagePath'   => '',
                'title'       => '',
                'name'        => '',
                'id'          => '',
                'iconClassLi' => 'sep');
        }

        foreach($previousWorkflowColl as $key => $previousWorkflowObj)
        {
            // if next workflow exists and user has credentials
            if($this->getUser()->hasCredential(sfGuardPermissionTable::getOrderStatusPermission(
                                            $previousWorkflowObj->getOrderStatus()->getName())))
            {
                // gets OrderStatus
                $previousStatusObj = $previousWorkflowObj->getOrderStatus();

                $previousStatusArr[] = array(
                    'imagePath'   => 'icons/orderStatus/16/'.$previousStatusObj->getIconNameBasedOnStatus(),
                    'title'       => $previousWorkflowObj->getTitle(),
                    'name'        => $previousWorkflowObj->getNextStatus(),
                    'id'          => '',
                    'iconClassLi' => 'icon_'.$previousStatusObj->getName());
            }
        }
        $statusArr['button']['menu']['previousStatuses'] = $previousStatusArr;

        // view objects
        $this->statusArr = $statusArr;
    }

}
