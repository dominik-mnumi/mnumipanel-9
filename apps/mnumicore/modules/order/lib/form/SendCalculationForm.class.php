<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SendCalculationForm
 *
 * @package    mnumishop
 * @subpackage form
 * @author     Adam Marchewicz
 */
class SendCalculationForm extends BaseOrderForm
{

    public function configure()
    {
        parent::configure();

        $this->setWidget('to', new sfWidgetFormInputText());
        $this->setWidget('subject', new sfWidgetFormInputText());
        $this->setWidget('calculation', new sfWidgetFormTextarea(array(), array('id' => 'email_calculation_textarea')));

        $this->widgetSchema->setLabels(array(
            'to' => 'E-mail',
            'subject' => 'Subject',
        ));

        $this->setValidator('to', new sfValidatorEmail(array('required' => true),
                                               array('required' => 'Email required', 
                                                     'invalid' => 'Email is invalid')));
        $this->setValidator('subject', new sfValidatorString(array('max_length' => 50, 
                                                     'required' => true), 
                                               array('required' => 'Subject required')));
        $this->setValidator('calculation', new sfValidatorString(array('required' => false)));

        $this->getWidgetSchema()->setNameFormat('send_calculation[%s]');
        
        $this->useFields(array('to', 'subject', 'calculation'));
    }
}
