<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * BlockPricesForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class BlockPricesForm extends BaseForm
{
    public function configure()
    {
        parent::configure();
        
        $checked = $this->getOption('checked');
        
        $this->setWidgets(array(
            'block_prices' => new sfWidgetFormInputCheckbox(),
        ));
        
        $this->widgetSchema->setLabels(array(
            'block_prices' => 'Block prices',
        ));
        
        $this->setValidators(
            array('block_prices' => new sfValidatorBoolean())
        );
        
        $this->setDefault('block_prices', $checked);
    }

}