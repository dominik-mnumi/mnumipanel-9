<?php
/*
 * This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * search components.
 *
 * @package    mnumicore
 * @subpackage search
 */
class searchComponents extends sfComponents
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeForm(sfWebRequest $request)
    {
        $this->form = new SearchForm();
    }
}
