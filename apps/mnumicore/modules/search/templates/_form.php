<?php echo include_partial('global/autocomplete') ?>
<form id="search-form" name="search-form" method="get" action="<?php echo url_for('searchStrict'); ?>">
    <?php echo $form['phrase']->render() ?>
    <div id="search-result" class="result-block" style="display: none; ">
        <div id="server-search">
            <ul class="small-files-list icon-html relative" id="search-list" style="overflow-x: hidden; overflow-y: hidden;"></ul>                        
        </div>
    </div>
</form>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() 
{
    autocompleteInit('<?php echo url_for('searchAutocomplete') ?>');
});
</script>