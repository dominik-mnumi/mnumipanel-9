<?php use_helper('objectLink'); ?>
<?php use_helper('OrderPackage'); ?>

<?php echo include_partial('global/alertModalConfirm'); ?>
<?php echo include_partial('dashboard/message'); ?>
<article id="package-index" class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content">
                <h1>
                    <?php echo __('Customer') ?>: <?php echo $client->getFullname(); ?>
                </h1>
                
                <?php include_partial('client/edit/tabList', 
                        array('clientObj' => $client, 
                            'currentTab' => 'tab-packages', 
                            'filtered' => $filtered,
                            'defaultDisableAdvancedAccountancy' => $defaultDisableAdvancedAccountancy)) ?>

                <?php if(isset($packageInDetails)): ?>
                <?php echo include_partial('extendedTask', 
                        array('package' => $packageInDetails, 
                            'formArray' => $formArray,
                            'defaultPaymentId' => $defaultPaymentId,
                            'defaultDisableAdvancedAccountancy' => $defaultDisableAdvancedAccountancy)); ?>
                <?php endif; ?>

                <?php if(count($packages)): ?>
                <?php foreach($packages as $package): ?>               
                <div class="task with-legend droppable ui-droppable package-link" packageid="<?php echo $package->getId(); ?>">
                    <div class="legend">
                        <?php echo link_to(order_package_name($package), url_for('clientPackageDetails', $package), "class=order-package-title"); ?>
                    </div>

                    <ul class="floating-tags">
                        <li class="tag-time"><?php echo order_package_name($package); ?></li>
                        <li class="tag-info"><?php echo __($package->getOrderPackageStatus()->getTitle()); ?></li>

                        <?php if($package->getInvoice()): ?>
                        <li class="tag-info"><?php echo link_to($package->getInvoice(), 
                                'clientInvoiceEdit', $package->getInvoice()); ?>
                        </li>
                        <?php endif; ?>
                        
                        <!-- payment status -->
                        <?php include_partial('package/bookAndPaymentStatus', 
                                array('packageObj' => $package)); ?>
                    </ul>  
                    
                    <div class="order-package-content">
                        <div class="package-block">
                            <?php echo __('Delivery address') ?>: 
                            <?php if($package->hasCarrier()) : ?>
                                <b><?php echo $package->getCarrier() ?></b>,
                            <?php endif ?>
                            <?php echo $package->getFullDeliveryAddress(); ?>
                        </div>

                        <?php if($package->getHangerNumber() != "") : ?>
                        <div class="package-block">
                            <?php echo __('Numbered hanger tag') ?>:
                             <b><?php echo $package->getHangerNumber() ?></b>
                        </div>
                        <?php endif ?>

                        <?php $orders = $package->Orders; ?>
                        <?php if(count($orders)): $counter = 1; ?>

                        <ul class="mini-blocks-list connectedSortable" id="sortable_<?php echo $package->getId(); ?>">
                            <?php foreach ($orders as $order): ?>
                            <li>
                                <a href="<?php echo url_for('orderListEdit', array('id' => $order->getId())); ?>" class="float-left">
                                    <img width="16" height="16" src="<?php echo $order->getImageShelfStatus(); ?>" /> <?php echo $order->getId(); ?>. <?php echo $order->getName(); ?>
                                </a>
                                <span style="float:right"><?php echo $order->getTotalAmount(); ?> <?php echo $sf_user->getCurrencySymbol() ?></span>
                            </li>                             
                            <?php $counter++; endforeach ?>
                        </ul>
                        <?php endif ?>                        
                    </div>
                </div>  
                
                <?php endforeach; ?>   
                <?php endif ?>
                
                <?php if(isset($packageInDetails) && count($packageInDetails->getCashDesks())): ?>
                <div class="task with-legend">
                    <div class="legend">
                        <?php echo __('Package payments') ?>
                    </div>
                    <div>
                        <ul class="mini-blocks-list">
                            <?php foreach($packageInDetails->getCashDesks() as $cashDesk): ?>
                            <li>
                                <a href="<?php echo url_for('cashDeskPrint', $cashDesk) ?>" class="float-left">
                                    <?php echo $cashDesk->getClientName() ?> <?php echo $cashDesk->getFormattedNumber() ?>
                                    <?php if($cashDesk->getPaymentId()): ?>
                                        (<?php echo $cashDesk->getPayment() ?>)
                                    <?php endif ?>
                                </a>
                                <span style="float:right"><?php echo $cashDesk->getValue() ?> <?php echo $sf_user->getCurrencySymbol() ?></span>
                            </li>
                            <?php endforeach ?>
                        </ul>                   
                    </div>
                </div>
                <?php endif ?>
            </div>                        
        </div>
    </section>
</article>

<script type="text/javascript">
$(document).ready(function() 
{ 
    $("#extender_order_list").tablesorter(); 
    
    $(".draggable").draggable({
        revert: true,
        containment: '.block-content',
        cursor: 'move',
        cursorAt: { top: 35, left: 45 },
        helper: function(event) 
        {
            var selector = $('<div id="drag-tr" orderid="' + $(this).attr('orderid') + '">' + $(this).children('.package-name').html() + "<br /><?php echo __('Move to proper package'); ?>" + '</div>');

            return selector;
        },
        drag: function(event, ui)
        {}
    });

    $(".droppable").droppable({
        drop: function(event, ui) 
        {      
            $.ajax({
                type: "GET",
                url: "<?php echo url_for('reqMoveOrderToPackage'); ?>" + "?to=" + $(this).attr('packageid') + "&id=" + $("#drag-tr").attr('orderid'),
                dataType: 'json',
                error: function(xhr, ajaxOptions, thrownError)
                {
                    alert('<?php echo __('There was error. Please try again later.'); ?>');
                },
                success: function(dataOutput)
                {
                    window.location.href = dataOutput.redirectUrl;                    
                }
            });

            $("#drag-tr").remove();          
        }
    });
    
    /**
     * Makes package box as link.
     */
    $(".package-link").click(function()
    {
        window.location.href = '<?php if(sfConfig::get('sf_environment') != 'prod'){ echo $sf_request->getScriptName(); } ?>/client/<?php echo $client->getId(); ?>/packages/' + $(this).attr('packageid');
    });
}); 
</script>
