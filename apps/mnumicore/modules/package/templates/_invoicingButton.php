<?php if($package->isCompleted()): ?>
<a <?php if(isset($ajax) && $ajax) echo 'id="toBookButton"' ?> href="<?php echo url_for('clientPackageToBook', $package); ?>" title="<?php echo __('Package to invoicing')?>" class="big-button red">        
    <?php echo __('Package to invoicing'); ?>
</a>           
<?php else: ?>
<button type="button" class="disabled"><?php echo __('Package to invoicing'); ?></button>
<?php endif; ?>
