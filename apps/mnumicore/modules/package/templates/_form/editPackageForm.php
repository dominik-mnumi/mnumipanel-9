<div id="dialog-edit-package">
    <p class="validateTips"></p>

    <form class="package-with-address-form"
          action="<?php echo url_for('reqEditPackageForm', array(
              'model' => 'OrderPackage',
              'type' => 'edit')); ?>"
          enctype="multipart/form-data"
          method="post" style="display:none;">
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <?php echo $form['client_id']->render(); ?>
        <div class="block-border" style="height: 465px;">
            <div class="block-content form no_border">
                <div class="colx3-left">
                    <p>
                        <?php echo $form['carrier_id']->renderLabel(); ?>
                        <?php echo $form['carrier_id']->render(); ?>
                    </p>
                    <div id="delivery_data">
                        <p class="required">
                            <?php echo $form['client_address_id']->renderLabel(); ?>
                            <?php echo $form['client_address_id']->render(); ?>
                        </p>
                        <div id="package-delivery-fullname-div" style="display: none;">
                            <p class="required">
                                <?php echo $form['delivery_name']->renderLabel(); ?>
                                <?php echo $form['delivery_name']->render(); ?>
                            <p>
                        </div>

                        <p class="required">
                            <?php echo $form['delivery_street']->renderLabel(); ?>
                            <?php echo $form['delivery_street']->render(); ?>
                        <p>
                        <p class="required">
                            <?php echo $form['postcodeAndCity']->renderLabel(); ?>
                            <?php echo $form['postcodeAndCity']->render(); ?>
                        <p>
                        <p class="required">
                            <?php echo $form['delivery_country']->renderLabel(); ?>
                            <?php echo $form['delivery_country']->render(); ?>
                        <p>
                    </div>
                </div>
                <div class="colx3-left">
                    <p>
                        <?php echo $form['payment_id']->renderLabel(); ?>
                        <?php echo $form['payment_id']->render(); ?>
                    </p>
                    <p>
                        <?php echo $form['description']->renderLabel(); ?>
                        <?php echo $form['description']->render(); ?>
                    </p>
                    <p>
                        <?php echo $form['delivery_at']->renderLabel(); ?>
                        <span class="input-type-text">
                            <?php echo $form['delivery_at']->render(); ?>
                        </span>
                    </p>
                </div>
                <div class="colx3-right">
                    <p>
                        <?php echo $form['want_invoice']->render(); ?>
                        <?php echo $form['want_invoice']->renderLabel(); ?>
                    </p>
                    <div id="invoice_data">
                        <p>
                            <?php echo $form['invoice_name']->renderLabel(); ?>
                            <?php echo $form['invoice_name']->render(); ?>
                        </p>
                        <p>
                            <?php echo $form['invoice_street']->renderLabel(); ?>
                            <?php echo $form['invoice_street']->render(); ?>
                        </p>
                        <p>
                            <?php echo $form['invoice_city']->renderLabel(); ?>
                            <?php echo $form['invoice_city']->render(); ?>
                        </p>
                        <p>
                            <?php echo $form['invoice_country']->renderLabel(); ?>
                            <?php echo $form['invoice_country']->render(); ?>
                        </p>
                        <p>
                            <?php echo $form['invoice_postcode']->renderLabel(); ?>
                            <?php echo $form['invoice_postcode']->render(); ?>
                        </p>
                        <p>
                            <?php echo $form['invoice_tax_id']->renderLabel(); ?>
                            <?php echo $form['invoice_tax_id']->render(); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
    
<?php include_partial('package/javascript/editPackageForm',
        array(
            'packageObj' => $form->getObject(),
            'defaultPaymentId' => $defaultPaymentId)); ?>
