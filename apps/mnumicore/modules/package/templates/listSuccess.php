<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <?php include_partial('global/filterForm', $tableOptions) ?>
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form" id="complex_form" >
            <h1>
                <?php echo __('Packages'); ?>
            </h1> 
            <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>

<script type="text/javascript">

$(function(){

     $('#order_package_filters_not_sent').change(function(){

        var hide = $(this).is(':checked');
        $('#order_package_filters_sent_from_date').val('').parent('p').toggle(!hide);
        $('#order_package_filters_sent_to_date').val('').parent('p').toggle(!hide);

     });
});

</script>