<script type="text/javascript">

function initializeSelects(target)
{
    $('.modal-content form.package-with-address-form').show();

    var selector1 = $(".modal-content .delivery-select");
    var selector2 = $(".modal-content .payment-select");
    
    if(!selector1.val())
    {
        selector2.val("");
        selector2.attr("disabled", "disabled");
    }
    else
    {
        selector2.removeAttr("disabled");
    }

    // if collection then collection form
    if($.inArray($(".modal-content .delivery-select").val(), <?php echo json_encode(CarrierTable::getInstance()->getCarrierIdsWithRequireShipmentData()); ?>) != -1)
    {
        $(".modal-content #delivery_data").show();
    }
    else
    {
        $(".modal-content #delivery_data").hide();
    }

    $(".modal-content #invoice_data").toggle(
        checkWantInvoiceState(target)
    );
}

$('input#EditPackageForm_want_invoice').change(function() {
    initializeSelects(this);
    $(".modal-content #invoice_data").toggle($(this).is(':checked'));
});

function checkWantInvoiceState(target){
    var checkedProp = $('input#EditPackageForm_want_invoice:checkbox').is(':checked');
    if(target && $(target).attr('id') == 'EditPackageForm_want_invoice'){
        if(checkedProp == true){
            return false;
        } else {
            return true;
        }
    } else {
        return checkedProp;
    }
}


/**
 * Renders errors list depending on json return array.
 */
function renderError(errorArray)
{
    var errorlist = '<ul class="message error no-margin">';
    for(i in errorArray)
    {
        errorlist += '<li>' + errorArray[i] + '</li>';
    }
    errorlist += '</ul>';
    
    return errorlist;
}

/**
 * Shows loader, disables buttons.
 */
function showLoaderAndDisableButtons()
{
    $(".block-footer button").attr("disabled", "disabled");
    $(".block-footer").prepend('<?php echo image_tag('loader.gif', array('class' => 'package-modal-loader-img')); ?>');
}

/**
 * Hides loader, enable buttons.
 */
function hideLoaderAndEnableButtons()
{
    $(".block-footer button").removeAttr("disabled");
    $(".block-footer").find("img").remove();
}

/**
 * Synchronizes form with address and without address.
 */
$(".delivery-select, .payment-select, .description-textarea, .want-invoice-checkbox").change(function()
{
    var classes = $(this).attr("class").split(/\s/);
    var className = classes[classes.length -1];
    var value = $(this).val();

    if($(this).is(":checkbox"))
    {
        if($(this).is(":checked"))
        {
            $("." + className).attr("checked", true);
        }
        else
        {
            $("." + className).removeAttr("checked", false);
        }
    }
    else
    {
       $("." + className).val(value);
    }
});

// client address
$("#EditPackageForm_client_address_id").change(
function()
{
    $.ajax({
        type: "GET",
        url: "<?php echo url_for('reqGetClientDeliveryDataJson'); ?>" + "?model=ClientAddress&id=" + $(this).val(),
        dataType: 'json',
        error: function(xhr, ajaxOptions, thrownError)
        {
            alert('<?php echo __('There was error. Please try again later.') ?>');
        },
        success: function(dataOutput)
        {
            // if null then show fullname for new address
            if(!$(".modal-content #EditPackageForm_client_address_id").val())
            {
                $(".modal-content #package-delivery-fullname-div").show();
            }
            // otherwise hide
            else
            {
                $(".modal-content #package-delivery-fullname-div").hide();

                $(".modal-content input#EditPackageForm_delivery_name").val(dataOutput.fullname);
                $(".modal-content select#EditPackageForm_delivery_country").val(dataOutput.country);
                $(".modal-content input#EditPackageForm_delivery_street").val(dataOutput.street);
                $(".modal-content input#EditPackageForm_postcodeAndCity").val($.trim(dataOutput.postcode + ' ' + dataOutput.city));              
            }                          
        }
    });
});

// carrier change
$(".delivery-select").change(function()
{
    $.ajax({
        type: "GET",
        url: "<?php echo url_for('reqGetPaymentsJson'); ?>" + "?model=Client&id=" + $("#EditPackageForm_client_id").val() + "&carrierId=" + $(this).val(),
        dataType: 'json',
        error: function(xhr, ajaxOptions, thrownError)
        {
            alert('<?php echo __('There was error. Please try again later.'); ?>');
        },
        success: function(dataOutput)
        {              
            var paymentId = $(".modal-content .payment-select").val();
            
            // if payment is not defined by select box then gets from default_payment
            if(!paymentId)
            {
                paymentId = <?php echo $defaultPaymentId; ?>;
            }

            var html = '<option value=""></option>\n';
            for(i in dataOutput)
            {
                var element = dataOutput[i];

                var selectValue = (paymentId == element.id) ? ' selected="selected"' : '';

                html = html + '<option' + selectValue + ' value="' + element.id + '">' 
                			+ element.label + '</option>\n';
            }

            var selector = $(".modal-content .payment-select");
            selector.html(html); 
            
            initializeSelects();
        }
    });
});

// if package can be sent then add js to service this action
<?php if($packageObj->canSend()): ?>
/**
 * Adds order notice. 
 */
$('#dialog-send-package-click').click(function() 
{
    $.modal({
        content: $("#send-package-form"),
        title: "<?php echo __('Send package'); ?>",
        maxWidth: 700,
        maxHeight: 350,
        resizable: false,
        onOpen: function()
        {
            var inputSelector = $(".modal-content #send-package-form input[type=text]");
        
            var interval = setInterval(
            function()
            {
                if(!inputSelector.first().is(":focus"))
                {
                    $(inputSelector).first().focus();
                    clearInterval(interval);
                }
            }
            , 
            700);
        },
        buttons: 
        {
            '<?php echo __('Send') ?>': function(win) 
            {
                var form = $(".modal-content #send-package-form");
                var dataInput = form.serialize();
                var dataOutput;
                var uri =  form.attr('action');
                var method = form.attr('method');

                $.ajax({
                    type: method,
                    url: uri,
                    data: dataInput,
                    dataType: 'json',
                    error: function(xhr, ajaxOptions, thrownError)
                    {
                        alert('<?php echo __('An error occurred'); ?>');
                    },
                    success: function(dataOutput)
                    {
                    	if('success' == dataOutput.result.status)
                        {         
                            if(0 != dataOutput.result.error.errorCount)
                            {
                                var errorlist = renderError(dataOutput.result.error.errorArray);

                                $(".modal-content #send-package-form .validateTips").html(errorlist);
                            }
                            else
                            {
                                win.closeModal();
                                window.location.reload();                                
                            }
                            
                        }
                    }
                });
            },
            '<?php echo __('Close') ?>': function(win) 
            {
                win.closeModal();
            }
        }
    }); 
});
<?php endif; ?>

/**
 * Delivery and payment form.
 */ 
$("#dialog-edit-package-click").click(function() 
{                       
    $.modal({
        content: $("#dialog-edit-package"),
        title: '<?php echo __('Package details'); ?>',
        width: 800,
        height: 515,
        resizable: false,
        onOpen: function()
        {            
            initializeSelects();
            $("#EditPackageForm_client_address_id").trigger('change');  
            $('#EditPackageForm_carrier_id').trigger('change');
        },
        onOpened: function()
        {
            $('#modal #EditPackageForm_delivery_at').attr('id', 'EditPackageForm_delivery_at_modal');
            loadDataPicker($, "#EditPackageForm_delivery_at_modal");
        },
        buttons: 
        {
            '<?php echo __('Save'); ?>': function(win) 
            {
                showLoaderAndDisableButtons();

                var form = $(".modal-content .package-with-address-form");

                var dataInput = form.serialize();
                var dataOutput;
                var uri =  form.attr('action');
                var method = form.attr('method');
                $.ajax({
                    type: method,
                    url: uri,
                    data: dataInput,
                    dataType: 'json',
                    error: function(xhr, ajaxOptions, thrownError)
                    {
                        alert('<?php echo __('There was error. Please try again later.') ?>');
                    },
                    success: function(dataOutput)
                    {
                        if('success' == dataOutput.result.status)
                        {         
                            if(0 != dataOutput.result.error.errorCount)
                            {
                                var errorlist = renderError(dataOutput.result.error.errorArray);

                                $(".modal-content #dialog-edit-package .validateTips").html(errorlist);

                                // hides loader
                                hideLoaderAndEnableButtons();
                            }
                            else
                            {
                            	// prevents page from viewing confirming popup
                            	$('#form_changed').val('');

                            	// saves order form
                            	if ($("#order_form").length > 0)
                            	{
                                    var form = $("#order_form");
                            	    var dataInput = form.serialize();
                            	    var dataOutput;
                            	    var uri =  form.attr('action');
                            	    var method = form.attr('method');
                            	    
                            		$.ajax({
                            	        type: method,
                            	        url: uri,
                            	        data: dataInput,
                            	        dataType: 'json',
                            	        error: function(xhr, ajaxOptions, thrownError)
                            	        {
                            	            alert('<?php echo __('There was error. Please try again later.') ?>');
                            	        },
                            	        success: function(dataOutput)
                            	        {
                            	            win.closeModal();
                            	            window.location.reload();
                            	        }
                            	    });
                            	}
                            	else
                            	{
                                    win.closeModal();
                            	    window.location.reload();
                            	}      
                            }
                            
                        }
                       
                    }
                });
            }
        }
    });      
});
</script>