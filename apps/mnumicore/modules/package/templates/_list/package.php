<?php use_helper('OrderPackage'); ?>

<?php echo link_to(order_package_name($row), url_for('client_package_details', $row)); ?><br />

<?php if($row->getOrders()->count()): ?>
<?php foreach($row->getOrders() as $rec): ?>
<?php $orderHrefArr[] = link_to($rec->getId(), 'orderListEdit', $rec); ?>
<?php endforeach; ?>
(<?php echo __('orders'); ?>: <?php echo implode(', ', $orderHrefArr); ?>)
<?php endif; ?>
