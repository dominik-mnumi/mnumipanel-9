<?php if($package->canPay()): ?>
<a href="<?php echo url_for('packagePay', $package) ?>">
    <button id="order-package-pay-button"
            type="button">
        <?php if($package->isPaid()): ?>
            <?php echo __('Unpay'); ?>
        <?php else: ?>
            <?php echo __('Pay'); ?>
        <?php endif; ?>
    </button>
</a>
<?php else: ?>
<button id="order-package-pay-button"
        type="button"
        class="disabled">
    <?php if($package->isPaid()): ?>
        <?php echo __('Unpay'); ?>
    <?php else: ?>
        <?php echo __('Pay'); ?>
    <?php endif; ?>
</button>
<?php endif; ?>