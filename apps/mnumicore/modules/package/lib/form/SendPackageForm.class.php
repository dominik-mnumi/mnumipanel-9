<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SendPackageForm.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class SendPackageForm extends BaseOrderPackageForm
{
    public function configure()
    {
        parent::configure();
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $this->setCssClasses('full-width');
        $this->useFields(array('transport_number'));
    }
    
    public function save($conn = null)
    {
        $this->getObject()->sendPackage($this->getValue('transport_number'));
        return $this->getObject();
    }
}

