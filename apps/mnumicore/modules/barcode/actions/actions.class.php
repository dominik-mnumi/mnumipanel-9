<?php

/**
 * barcode actions.
 *
 * @package    mnumicore
 * @subpackage barcode
 * @author     Adam Marchewicz <adam.marchewicz@itme.eu>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class barcodeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeRead(sfWebRequest $request)
  {
      $barcode = new BarcodeDecoder($request->getParameter('barcode'));
      
      return $this->renderText(json_encode(array('redirect' => $barcode->getObject()->getEditUrl())));
  }
}
