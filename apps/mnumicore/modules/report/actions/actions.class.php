<?php

/**
 * report actions.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
//require_once VENDORS_DIR .'/Image/Barcode.php';

class reportActions extends tablesActions
{
    /**
     * Renders barcode in browser, parameter :code required
     *
     * @param sfWebRequest $request 
     * @return binary data
     */
    public function executeBarcode(sfWebRequest $request)
    {
        $image_type = sfConfig::get('app_barcode_image_type', 'png');

        $barcodeType = sfConfig::get('app_barcode_type', 'code39');

        $barcodeParam = array(
            'text' => strtoupper($request->getParameter('code')),
            'barHeight' => sfConfig::get('app_barcode_height', 50)
        );

        $render = Zend_Barcode::render(
            $barcodeType, 'image', $barcodeParam, array('imageType' => $image_type)
        );

        $contentType = 'image/' . $image_type;
        $this->getResponse()->setHttpHeader('Content-Type', $contentType);

        return $this->renderText($render);
    }

    /**
     * Executes carrier action
     *
     * @param sfRequest $request A request object
     */
    public function executeCarrier(sfWebRequest $request)
    {
        $this->carrierReportTypeForm = new CarrierReportTypeListForm();
        
        $this->displayTableFields = array(
            'Lp.' => array('label' => 'Lp.', 'partial' => 'global/lp'),
            'DeliveryName' => array('label' => 'Addressee'),
            'DeliveryAddress' => array('label' => 'Address'),
            'Orders' => array('label' => 'Orders', 'partial' => 'orders'),
        );
        
        $this->modelObject = 'OrderPackage';
        $this->actions = array();
        if(!CarrierTable::getInstance()->findOneByName(CarrierTable::$pocztaPolska))
        {
            $this->setTemplate('pocztaPolskaRequired');
        }
        $this->customQuery = OrderPackageTable::getInstance()->getDespatchPackagesQuery();
        
        $this->tableOptions = $this->executeTable();
        
        if($request->isXmlHttpRequest())
        {
          return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }
    
    /**
     * Executes loadDespatches action
     *
     * @param sfRequest $request A request object
     */
    public function executeLoadDespatches(sfWebRequest $request)
    {
        $this->despatches = ReportDespatchTable::getInstance()->getOrderedDespatchesByReportName($request->getParameter('name'));
        
        $despatches = array();
        $previousDate = null;
        foreach ($this->despatches as $despatch)
        {
            $currentDate = date('Y-m', strtotime($despatch->getCreatedAt()));
            if(array_key_exists($currentDate, $despatches))
            {
                $despatches["$currentDate"] = array_merge($despatches["$currentDate"], array($despatch->toArray()));
            }
            else
            {
                $despatches["$currentDate"] = array($despatch->toArray());
            }
            
        }
        $this->despatches = $despatches;
    }
    
    /**
     * Executes carrierReportView action
     *
     * @param sfRequest $request A request object
     */
    public function executeCarrierReportView(sfWebRequest $request)
    {
        $this->reportObj = $this->getRoute()->getObject();
        $this->type = $this->reportObj->getCarrierReportTypeName();
    }
    
    /**
     * Executes closeBook action
     *
     * @param sfRequest $request A request object
     */
    public function executeCloseBook(sfWebRequest $request)
    {
        $type = $request->getParameter('CarrierReportTypeListForm');
        $reportDespatch = ReportDespatchTable::getInstance()->closeBook($type['type']);
        
        $this->getUser()->setFlash('info_data', array(
            'message' => 'Saved successfully.',
            'messageType' => 'msg_success',
        ));
        
        $this->redirect('@settingsReportCarrier');
    }


    /**
     * Executes report for accounting action.
     *
     * @param sfRequest $request A request object
     */
    public function executeAccountingReport(sfWebRequest $request)
    {
        $accountingReportForm = new AccountingReportForm();

        // view objects
        $this->accountingReportForm = $accountingReportForm;
    }

    /**
     * Executes report for accounting action.
     *
     * @param sfRequest $request A request object
     */
    public function executeAccountingReportExport(sfWebRequest $request)
    {
        // gets format
        $sfFormat = $request->getParameter('sf_format');

        $generatedContentArr = array();

        // gets parameters
        $paramArr = $request->getParameter('AccountingReportForm');

        // if range is not defined then redirects
        if(!isset($paramArr['range']))
        {
            $this->redirect('accountingReport');
        }

        foreach($paramArr['range'] as $rec)
        {
            $timeStart = microtime(true);

            // if sale exists
            if($rec == 'sale')
            {
                $title = 'Sale';
                $filename = 'sale.'.$sfFormat;
                $partial = 'report/accountingReportSaleExport';
                $coll = InvoiceTable::getInstance()
                    ->getInvoicesByStatus(
                        InvoiceStatusTable::$paid,
                        $paramArr['from_date'],
                        $paramArr['to_date']);
            }
            else
            {

                $title = PaymentTable::getInstance()->find($rec)->getLabel();
                /**
                 * https://bugs.php.net/bug.php?id=53948
                 */
                if(mb_check_encoding($title, 'UTF-8'))
                    $title = iconv("UTF-8", "ASCII//TRANSLIT", $title);
                
                $filename = Cast::prepareFilename($title).'.'.$sfFormat;
                $partial = 'report/accountingReportPaymentExport';
                $coll = CashReportTable::getInstance()->getToExport(
                    $rec,
                    $paramArr['from_date'],
                    $paramArr['to_date']);
            }

            $timeEnd = microtime(true);
            $content = $this->getPartial($partial,
                array('coll' => $coll,
                    'title' => $title,
                    'buildTime' => $timeEnd - $timeStart));

            // prepares array with contents
            $generatedContentArr[$filename] = $content;
        }

        // creates object
        $dir = sfConfig::get('sf_cache_dir').'/';
        if(!is_dir($dir))
        {
            mkdir($dir, 0777, true);
        }

        // creates unique temporary zip file
        $tmpFile = tempnam(sfConfig::get('sf_cache_dir').'/', "zip");

        $zipArchiveObj = new ZipArchive();
        $opened = $zipArchiveObj->open($tmpFile, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
        if( $opened !== true )
        {
            throw new Exception("Cannot open $tmpFile for writing.");
        }

        // gets i18N
        $i18NObj = $this->getContext()->getI18N();

        // foreach generated file
        foreach($generatedContentArr as $key => $rec)
        {
            $zipArchiveObj->addFromString($key, $rec);
        }

        $zipArchiveObj->close();

        // sets header
        $response = $this->getResponse();
        $response->clearHttpHeaders();
        $response->setContentType('application/zip');
        $response->setHttpHeader('Content-Disposition',
            'attachment; filename="'.$i18NObj->__('reports')
                .'_'.date("Y-m-d_G:i:s").'.zip"');
        $response->setHttpHeader('Content-Description', 'File Transfer');
        $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
        $response->setHttpHeader('Content-Length', filesize($tmpFile));
        $response->setHttpHeader('Cache-Control', 'public, must-revalidate');
        $response->setHttpHeader('Pragma', 'public');
        $response->sendHttpHeaders();

        ob_end_flush();

        return $this->renderText(readfile($tmpFile));
    }

    /**
     * Render sale report
     *
     * @param sfRequest $request A request object
     */
    public function executeSaleReport(sfWebRequest $request)
    {
        $this->form = new InvoiceSaleReportFormFilter();
    }
}
