<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CarrierReportTypeListForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class CarrierReportTypeListForm extends BaseForm
{
    public function configure()
    {
        $this->setWidgets(array(
            'type' => new sfWidgetFormDoctrineChoice(array('model' => 'CarrierReportType'), array('id' => 'reportType'))
        ));
        $this->widgetSchema->setNameFormat('CarrierReportTypeListForm[%s]');
     
        $this->setValidators(array(
            'type' => new sfValidatorDoctrineChoice(array('model' => 'CarrierReportType'))
        ));
    }
}