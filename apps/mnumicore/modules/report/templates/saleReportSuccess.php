<article class="container_12">
    <section class="grid_12">
        <div class="block-border search-div">
            <form method="post" 
                  class="block-content form search-filter-form"
                  action="<?php echo url_for('saleReportPdf'); ?>"
                  target="_blank">
                <h1>
                    <?php echo __('Sale report'); ?>             
                </h1>

                <?php echo $form; ?>

                <div class="text-right">
                    <button><?php echo __('Generate'); ?></button>
                </div>
            </form>
        </div>
    </section>
</article>
