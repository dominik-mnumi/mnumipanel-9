<?php echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<reports version="<?php echo sfConfig::get('app_version'); ?>" buildtime="<?php echo $buildTime; ?>" title="<?php echo __($title); ?>">
    <?php foreach($coll as $rec): ?>
    <report>
        <number><?php echo $rec->getNumber(); ?></number>
        <payment_name><?php echo __($rec->getPayment()->getLabel()); ?></payment_name>
        <created_at><?php echo $rec->getCreatedAt(); ?></created_at>
        <items>
            <?php foreach($rec->getCashDesks() as $rec2): ?>
            <?php $invoiceObj = $rec2->getInvoice(); ?>
            <?php $paymentObj = $rec2->getPayment(); ?>
            <?php $clientObj = $rec2->getClient(); ?>
            <item>
                <number><?php echo $rec2->getName(); ?></number>

                <?php if($rec2->getClientId()): ?>
                <client_name><?php echo $clientObj->getFullname(); ?></client_name>
                <client_address><?php echo $clientObj->getStreet(); ?></client_address>
                <client_postcode><?php echo $clientObj->getPostcode(); ?></client_postcode>
                <client_city><?php echo $clientObj->getCity(); ?></client_city>
                <client_tax_id><?php echo $clientObj->getTaxId(); ?></client_tax_id>
                <?php else: ?>
                <client_name></client_name>
                <client_address></client_address>
                <client_postcode></client_postcode>
                <client_city></client_city>
                <client_tax_id></client_tax_id>
                <?php endif; ?>

                <?php if($rec2->getInvoiceId()): ?>
                <invoice_number><?php echo $invoiceObj; ?></invoice_number>   
                <?php else: ?>
                <invoice_number></invoice_number>   
                <?php endif; ?>

                <?php if($rec2->getPaymentId()): ?>
                <payment_name><?php echo __($paymentObj->getLabel()); ?></payment_name>
                <?php else: ?>
                <payment_name></payment_name> 
                <?php endif; ?>

                <value><?php echo $rec2->getValue(); ?></value>
                <description><?php echo $rec2->getDescription(); ?></description>
                <created_at><?php echo $rec2->getCreatedAt(); ?></created_at>   
            </item> 
            <?php endforeach; ?>
        </items>
    </report>    
    <?php endforeach; ?>
</reports>

