<form method = "post" class="form" action="<?php echo url_for('configurationFiscalPrinter'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationFiscalPrinter')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationFiscalPrinter')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_fiscal">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Konfiguracja drukarki fiskalnej'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Configuration'); ?></legend>
                            <p>
                                <?php echo $form['fiscal_printer_com']->renderLabel(); ?>
                                <?php echo $form['fiscal_printer_com']->render(); ?>
                                <?php echo $form['fiscal_printer_com']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['fiscal_printer_type']->renderLabel(); ?>
                                <?php echo $form['fiscal_printer_type']->render(); ?>
                                <?php echo $form['fiscal_printer_type']->renderHelp(); ?>
                            </p>
                        </fieldset>
                        <div class="box">
                            <p class="mini-infos color-blue bold">
                                <?php echo __('To operate your fiscal printer, you have to install Mnumi Fiscal Printer software on your computer which is connected to fiscal printer. You can download the latest version by clicking on the %link%. After successful installation, you must download the file %properties% and save it in: C:\\Program Files\\Mnumi Fiscal Printer\\conf\\SpringBeans.xml.',
                                    array('%link%' => '<a class="red" href="http://security.mnumi.com/FiscalPrinter-latest.exe" title="Mnumi Fiscal Printer">FiscalPrinter-latest.exe</a>',
                                        '%properties%' => link_to('SpringBeans.xml',
                                            '@configurationFiscalPrinterFile',
                                            array('class' => 'red')))); ?>

                            </p>
                        </div>


                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

