<form method = "post" class="form" action="<?php echo url_for('configurationLocalFileServer'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationLocalFileServer')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationLocalFileServer')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_online_payments">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Local file server'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Server data'); ?></legend>
                            <p>
                                <?php echo $form['local_file_server_ip']->renderLabel(); ?>
                                <?php echo $form['local_file_server_ip']->render(); ?>
                                <?php echo $form['local_file_server_ip']->renderHelp(); ?>
                            </p>     
                            <p>
                                <?php echo $form['local_file_server_enable']->renderLabel(); ?>
                                <?php echo $form['local_file_server_enable']->render(); ?>
                                <?php echo $form['local_file_server_enable']->renderHelp(); ?>
                            </p> 
                        </fieldset>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

