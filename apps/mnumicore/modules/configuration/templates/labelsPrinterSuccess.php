<form method = "post" class="form" action="<?php echo url_for('configurationLabelsPrinter'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationLabelsPrinter')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationLabelsPrinter')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_label_printer">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Label printer'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Label printer'); ?></legend>
                            <p>
                                <?php echo $form['printlabel_name']->renderLabel(); ?>
                                <?php echo $form['printlabel_name']->render(); ?>
                                <?php echo $form['printlabel_name']->renderHelp(); ?>
                                <fieldset>
                                    <legend><?php echo __('Printer settings'); ?></legend>
                                    <p>
                                        <?php echo $form['printlabel_width']->renderLabel(); ?>
                                        <?php echo $form['printlabel_width']->render(); ?>
                                        <?php echo $form['printlabel_width']->renderHelp(); ?>
                                    </p>

                                    <p>
                                        <?php echo $form['printlabel_height']->renderLabel(); ?>
                                        <?php echo $form['printlabel_height']->render(); ?>
                                        <?php echo $form['printlabel_height']->renderHelp(); ?>
                                    </p>
                                    <p>
                                        <?php echo $form['printlabel_orientation']->renderLabel(); ?>
                                        <?php echo $form['printlabel_orientation']->render(); ?>
                                        <?php echo $form['printlabel_orientation']->renderHelp(); ?>
                                    </p>
                                </fieldset>
                            </p>

                            <p>
                                <?php echo $form['printlabel_large_name']->renderLabel(); ?>
                                <?php echo $form['printlabel_large_name']->render(); ?>
                                <?php echo $form['printlabel_large_name']->renderHelp(); ?>
                                <fieldset>
                                    <legend><?php echo __('Printer settings'); ?></legend>
                                    <p>
                                        <?php echo $form['printlabel_large_width']->renderLabel(); ?>
                                        <?php echo $form['printlabel_large_width']->render(); ?>
                                        <?php echo $form['printlabel_large_width']->renderHelp(); ?>
                                    </p>

                                    <p>
                                        <?php echo $form['printlabel_large_height']->renderLabel(); ?>
                                        <?php echo $form['printlabel_large_height']->render(); ?>
                                        <?php echo $form['printlabel_large_height']->renderHelp(); ?>
                                    </p>
                                    <p>
                                        <?php echo $form['printlabel_large_orientation']->renderLabel(); ?>
                                        <?php echo $form['printlabel_large_orientation']->render(); ?>
                                        <?php echo $form['printlabel_large_orientation']->renderHelp(); ?>
                                    </p>
                                </fieldset>
                            </p>


                            
                            <div class="box">
                                <p class="mini-infos color-blue bold">
                                    <?php echo __('To operate your printer\'s software, you have to install Mnumi Printlabel software on your computer which is connected to label printer. You can download the latest version by clicking on the %link%. After successful installation, you must download the file %properties% and save it in:<br /> C:\Program Files\Mnumi Printlabel\app\config\properties.ini.',
                                            array('%link%' => '<a class="red" href="http://security.mnumi.com/PrintlabelInstaller-latest.exe" title="Mnumi Printlabel">PrintlabelInstaller-latest.exe</a>',
                                                '%properties%' => link_to('properties.ini', 
                                                        '@configurationLabelsPrinterProperties',
                                                        array('class' => 'red')))); ?>
                                    
                                </p>
                            </div>                           
                        </fieldset>
                        
                        <h2 class="bigger margin-bottom-10px"><?php echo __('UPS data'); ?></h2>
                        <hr />
                        
                        <div class="box">
                            <p class="mini-infos color-blue bold">
                                <?php echo __('To generate UPS labels properly all fields must be filled.'); ?>                            
                            </p>
                        </div> 

                        <fieldset>
                            <legend><?php echo __('API data'); ?></legend>
                            <p>
                                <?php echo $form['ups_url']->renderLabel(); ?>
                                <?php echo $form['ups_url']->render(); ?>
                                <?php echo $form['ups_url']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_access']->renderLabel(); ?>
                                <?php echo $form['ups_access']->render(); ?>
                                <?php echo $form['ups_access']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_user_id']->renderLabel(); ?>
                                <?php echo $form['ups_user_id']->render(); ?>
                                <?php echo $form['ups_user_id']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_password']->renderLabel(); ?>
                                <?php echo $form['ups_password']->render(); ?>
                                <?php echo $form['ups_password']->renderHelp(); ?>
                            </p>
                        </fieldset>
                        
                        <fieldset>
                            <legend><?php echo __('Sender data'); ?></legend>

                            <p>
                                <?php echo $form['ups_shipper_shipper_number']->renderLabel(); ?>
                                <?php echo $form['ups_shipper_shipper_number']->render(); ?>
                                <?php echo $form['ups_shipper_shipper_number']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_shipper_name']->renderLabel(); ?>
                                <?php echo $form['ups_shipper_name']->render(); ?>
                                <?php echo $form['ups_shipper_name']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_shipper_attention_name']->renderLabel(); ?>
                                <?php echo $form['ups_shipper_attention_name']->render(); ?>
                                <?php echo $form['ups_shipper_attention_name']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_shipper_tax_identification_number']->renderLabel(); ?>
                                <?php echo $form['ups_shipper_tax_identification_number']->render(); ?>
                                <?php echo $form['ups_shipper_tax_identification_number']->renderHelp(); ?>
                            </p> 
                            <p>
                                <?php echo $form['ups_shipper_address_address_line']->renderLabel(); ?>
                                <?php echo $form['ups_shipper_address_address_line']->render(); ?>
                                <?php echo $form['ups_shipper_address_address_line']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_shipper_address_city']->renderLabel(); ?>
                                <?php echo $form['ups_shipper_address_city']->render(); ?>
                                <?php echo $form['ups_shipper_address_city']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_shipper_address_postal_code']->renderLabel(); ?>
                                <?php echo $form['ups_shipper_address_postal_code']->render(); ?>
                                <?php echo $form['ups_shipper_address_postal_code']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_shipper_address_country_code']->renderLabel(); ?>
                                <?php echo $form['ups_shipper_address_country_code']->render(); ?>
                                <?php echo $form['ups_shipper_address_country_code']->renderHelp(); ?>
                            </p>
                        </fieldset>
                        
                        <fieldset>
                            <legend><?php echo __('Settings'); ?></legend>
                            <p>
                                <?php echo $form['ups_service_code']->renderLabel(); ?>
                                <?php echo $form['ups_service_code']->render(); ?>
                                <?php echo $form['ups_service_code']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_package_packaging_code']->renderLabel(); ?>
                                <?php echo $form['ups_package_packaging_code']->render(); ?>
                                <?php echo $form['ups_package_packaging_code']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_package_package_weight_unit_of_measurement_code']->renderLabel(); ?>
                                <?php echo $form['ups_package_package_weight_unit_of_measurement_code']->render(); ?>
                                <?php echo $form['ups_package_package_weight_unit_of_measurement_code']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_package_package_weight_weight']->renderLabel(); ?>
                                <?php echo $form['ups_package_package_weight_weight']->render(); ?>
                                <?php echo $form['ups_package_package_weight_weight']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_package_dimensions_unit_of_measurement_code']->renderLabel(); ?>
                                <?php echo $form['ups_package_dimensions_unit_of_measurement_code']->render(); ?>
                                <?php echo $form['ups_package_dimensions_unit_of_measurement_code']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_package_dimensions_length']->renderLabel(); ?>
                                <?php echo $form['ups_package_dimensions_length']->render(); ?>
                                <?php echo $form['ups_package_dimensions_length']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_package_dimensions_width']->renderLabel(); ?>
                                <?php echo $form['ups_package_dimensions_width']->render(); ?>
                                <?php echo $form['ups_package_dimensions_width']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['ups_package_dimensions_height']->renderLabel(); ?>
                                <?php echo $form['ups_package_dimensions_height']->render(); ?>
                                <?php echo $form['ups_package_dimensions_height']->renderHelp(); ?>
                            </p>
                        </fieldset>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

