<form method = "post" class="form" action="<?php echo url_for('configurationNotifications'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationNotifications')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationNotifications')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_notifications">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Notifications'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Notifications'); ?></legend>
                            <p>
                                <?php echo $form['notification_sender']->renderLabel(); ?>
                                <?php echo $form['notification_sender']->render(); ?>
                                <?php echo $form['notification_sender']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['notification_sender_name']->renderLabel(); ?>
                                <?php echo $form['notification_sender_name']->render(); ?>
                                <?php echo $form['notification_sender_name']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['notification_default_enable_sms']->renderLabel(); ?>
                                <?php echo $form['notification_default_enable_sms']->render(); ?><br/>
                                <?php echo $form['notification_default_enable_sms']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['notification_default_enable_email']->renderLabel(); ?>
                                <?php echo $form['notification_default_enable_email']->render(); ?><br/>
                                <?php echo $form['notification_default_enable_email']->renderHelp(); ?>
                            </p>
                        </fieldset>

                        <fieldset>
                            <legend><?php echo __('PowiadomieniaSMS.pl'); ?></legend>
                            <p>
                                <?php echo $form['sms_server_user']->renderLabel(); ?>
                                <?php echo $form['sms_server_user']->render(); ?>
                                <?php echo $form['sms_server_user']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['sms_server_password']->renderLabel(); ?>
                                <?php echo $form['sms_server_password']->render(); ?>
                                <?php echo $form['sms_server_password']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['sms_server_id']->renderLabel(); ?>
                                <?php echo $form['sms_server_id']->render(); ?>
                                <?php echo $form['sms_server_id']->renderHelp(); ?>
                            </p>
                        </fieldset>
                    </div>
  
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

