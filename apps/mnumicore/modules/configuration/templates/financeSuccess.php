<form method = "post" class="form" action="<?php echo url_for('configurationFinance'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationFinance')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationFinance')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_finance">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Finance'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Default currency'); ?></legend>
                            <p>
                                <?php echo $form['default_currency']->renderLabel(); ?>
                                <?php echo $form['default_currency']->render(); ?>
                                <?php echo $form['default_currency']->renderHelp(); ?>
                            </p>
                        </fieldset>

                        <fieldset>
                            <legend><?php echo __('Tax'); ?></legend>
                            <p>
                                <?php echo $form['price_tax']->renderLabel(); ?>
                                <?php echo $form['price_tax']->render(); ?>
                                <?php echo $form['price_tax']->renderHelp(); ?>
                            </p>
                        </fieldset>

                        <fieldset>
                            <legend><?php echo __('Accountancy'); ?></legend>
                            <p>
                                <span class="half-width">
                                    <?php echo $form['default_disable_advanced_accountancy']->renderLabel(); ?>
                                    <?php echo $form['default_disable_advanced_accountancy']->render(); ?>
                                </span>
                                <?php echo $form['default_disable_advanced_accountancy']->renderHelp(); ?>
                            </p>
                            
                            <p>
                                <span class="half-width">
                                    <?php echo $form['invoice_allow_zeroed_prices']->renderLabel(); ?>
                                    <?php echo $form['invoice_allow_zeroed_prices']->render(); ?>
                                </span>
                                <?php echo $form['invoice_allow_zeroed_prices']->renderHelp(); ?>
                            </p>                            
                        </fieldset>

                        <fieldset>
                            <legend><?php echo __('Invoice'); ?></legend>
                            <p>
                                <?php echo $form['invoice_receipt_to_invoice_days']->renderLabel(); ?>
                                <?php echo $form['invoice_receipt_to_invoice_days']->render(); ?>
                                <?php echo $form['invoice_receipt_to_invoice_days']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['invoice_naming_pattern_invoice']->renderLabel(); ?>
                                <?php echo $form['invoice_naming_pattern_invoice']->render(); ?>
                                <?php echo $form['invoice_naming_pattern_invoice']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['invoice_naming_pattern_correction']->renderLabel(); ?>
                                <?php echo $form['invoice_naming_pattern_correction']->render(); ?>
                                <?php echo $form['invoice_naming_pattern_correction']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['invoice_naming_pattern_preliminary']->renderLabel(); ?>
                                <?php echo $form['invoice_naming_pattern_preliminary']->render(); ?>
                                <?php echo $form['invoice_naming_pattern_preliminary']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['invoice_naming_pattern_receipt']->renderLabel(); ?>
                                <?php echo $form['invoice_naming_pattern_receipt']->render(); ?>
                                <?php echo $form['invoice_naming_pattern_receipt']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['invoice_item_format_order']->renderLabel(); ?>
                                <?php echo $form['invoice_item_format_order']->render(); ?>
                                <?php echo $form['invoice_item_format_order']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['invoice_notice']->renderLabel(); ?>
                                <?php echo $form['invoice_notice']->render(); ?>
                                <?php echo $form['invoice_notice']->renderHelp(); ?>
                            </p>

                        </fieldset>

                        <fieldset>
                            <legend><?php echo __('Loyalty points'); ?></legend>
                            <p>
                                <?php echo $form['loyalty_points_scale']->renderLabel(); ?>
                                <?php echo $form['loyalty_points_scale']->render(); ?>
                                <?php echo $form['loyalty_points_scale']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['loyalty_points_reverted_scale']->renderLabel(); ?>
                                <?php echo $form['loyalty_points_reverted_scale']->render(); ?>
                                <?php echo $form['loyalty_points_reverted_scale']->renderHelp(); ?>
                            </p>
                        </fieldset>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

