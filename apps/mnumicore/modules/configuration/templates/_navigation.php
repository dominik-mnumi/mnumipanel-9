<ul id="conf-list" class="simple-list">
    <li class="<?php echo $route == 'configurationCompanyData' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Company data'), 'configurationCompanyData'); ?>
    </li>
    <li class="<?php echo $route == 'configurationNotifications' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Notifications'), 'configurationNotifications'); ?>
    </li>
    <li class="<?php echo $route == 'configurationLabelsPrinter' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Label printer'), 'configurationLabelsPrinter'); ?>
    </li>
    <li class="<?php echo $route == 'configurationFinance' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Finance'), 'configurationFinance'); ?>
    </li>
    <li class="<?php echo $route == 'configurationOnlinePayments' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Online payments'), 'configurationOnlinePayments'); ?>
    </li>
    <li class="<?php echo $route == 'configurationLocalFileServer' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Local file server'), 'configurationLocalFileServer'); ?>
    </li>
    <li class="<?php echo $route == 'configurationRegionalSettings' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Regional settings'), 'configurationRegionalSettings'); ?>
    </li>
    <li class="<?php echo $route == 'configurationDeliveryAndPayment' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Delivery and payment'), 'configurationDeliveryAndPayment'); ?>
    </li>
    <li class="<?php echo $route == 'configurationFiscalPrinter' ? 'simple-list-selected-li' : ''; ?>">
        <?php echo link_to(__('Fiscal printer'), 'configurationFiscalPrinter'); ?>
    </li>
</ul>