<?php

/**
 * configuration actions.
 *
 * @package    mnumicore
 * @subpackage configuration
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class configurationActions extends sfActions
{
    public function preExecute()
    {
        parent::preExecute();

        $this->appFilePath = sfConfig::get("sf_app_config_dir").'/app.yml';
        $this->settingsFilePath = sfConfig::get("sf_app_config_dir").'/settings.yml';
        $this->fileWritable = is_writable($this->appFilePath);
        $this->fileSettingsWritable = is_writable($this->settingsFilePath);
    }

    /**
     * Executes companyData action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeCompanyData(sfWebRequest $request)
    {
        $this->form = new ConfigurationCompanyDataForm();
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes notifications action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeNotifications(sfWebRequest $request)
    {
        $this->form = new ConfigurationNotificationsForm();
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes labelPrinter action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeLabelsPrinter(sfWebRequest $request)
    {
        $this->form = new ConfigurationLabelsPrinterForm();
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes finance action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeFinance(sfWebRequest $request)
    {
        $this->form = new ConfigurationFinanceForm();
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes onlinePayments action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeOnlinePayments(sfWebRequest $request)
    {
        $this->form = new ConfigurationOnlinePaymentsForm();
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes local file server action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeLocalFileServer(sfWebRequest $request)
    {
        $this->form = new ConfigurationLocalFileServerForm();
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes regionalSettings action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeRegionalSettings(sfWebRequest $request)
    {
        $this->form = new ConfigurationRegionalSettingsForm();
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes deliveryAndPayment action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeDeliveryAndPayment(sfWebRequest $request)
    {
        $this->form = new ConfigurationDeliveryAndPaymentForm();
        $this->proceedForm($this->form, $request);
    }

    /**
     * Executes fiscalPrinter action.
     *
     * @param sfWebRequest $request A request object
     */
    public function executeFiscalSettings(sfWebRequest $request)
    {
        $this->form = new ConfigurationFiscalPrinterForm();
        $this->proceedForm($this->form, $request);
    }
    
    /**
     * Executes getPrintLabelProperties action.
     *
     * @param sfWebRequest $request A request object
     * @return string
     */
    public function executeGetPrintLabelProperties(sfWebRequest $request)
    {
        $restServer = 'http://'.$request->getHost().'/index.php/api';
        $connectionKey = substr(sfConfig::get('sf_csrf_secret'), 0, 6);
        
        $fileContent = PrintlabelQueueTable::getInstance()->generateProperties(
                $restServer, 
                $connectionKey);
        
        // sets response
        $response = $this->getResponse();
        $response->setContentType('text/xml');
        $response->setHttpHeader('Content-Disposition', "attachment; filename=properties.ini");
        $response->setContent($fileContent);

        return sfView::NONE;
    }

    public function executeFiscalConfigFile(sfWebRequest $request)
    {
        $host = 'http://'.$request->getHost();
        $connectionKey = substr(sfConfig::get('sf_csrf_secret'), -6);
        $fiscalType = sfConfig::get('app_fiscal_printer_type', 'Thermal401');
        $fiscalCom = sfConfig::get('app_fiscal_printer_com', 'COM1');

        $content = '<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context-2.5.xsd">

    <context:annotation-config />

    <bean id="runtimeBean" class="com.mnumi.fps.runtime.FiscalPrinterRuntime">
        <property name="fiscalPrinterType" value="%s" />
        <property name="fiscalPrinterComPort" value="%s" />
    </bean>

    <bean id="FiscalPrinterThread" class="com.mnumi.fps.thread.FiscalPrinterThread">
        <constructor-arg type="String"><value>default</value></constructor-arg>
        <constructor-arg type="int"><value>7000</value></constructor-arg>
    </bean>

    <bean id="DAOBean" class="com.mnumi.fps.db.RestServer">
        <property name="host" value="%s" />
        <property name="secretKey" value="%s" />
    </bean>


</beans>';

        // sets response
        $response = $this->getResponse();
        $response->setContentType('text/xml');
        $response->setHttpHeader('Content-Disposition', "attachment; filename=SpringBeans.xml");
        $response->setContent(sprintf($content, $fiscalType, $fiscalCom, $host, $connectionKey));

        return sfView::NONE;
    }
    
    /**
     * Returns available payments details in json.
     * 
     * @param sfWebRequest $request
     * @return json
     */
    public function executeGetPayments(sfWebRequest $request)
    {
        $carrierId = $request->getParameter('carrierId', null);

        $paymentChoiceArr = Cast::getChoiceArr(
                PaymentTable::getInstance()->getFilteredPayment(
                        null, 
                        null, 
                        null, 
                        $carrierId),
                'label', 
                'id');

        foreach($paymentChoiceArr as &$rec)
        {
            $rec = $this->getContext()->getI18N()->__($rec);
        }
        
        return $this->renderText(json_encode($paymentChoiceArr));
    }
    
    /**
     * Proceeds form.
     * 
     * @param AppYamlEditForm $form
     * @param sfWebRequest $request
     * @return void
     */
    private function proceedForm(AppYamlEditForm $form, sfWebRequest $request)
    {
        if(!$this->getRequest()->isMethod('post'))
        {
            return;
        }
        
        $form->bind($this->getRequest()->getParameter($form->getName()),
                $this->getRequest()->getFiles($form->getName()));

        if($form->isValid())
        {
            $form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            $this->redirect(sfContext::getInstance()->getRouting()->getCurrentRouteName());
        }
    }
}
