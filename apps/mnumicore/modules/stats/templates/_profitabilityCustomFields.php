<p>
    <label for="shop_name"><?php echo __('Stats for:'); ?></label>
    <select id="stats_for" name="stats_for">
        <option <?php if($stats_for == 'categories'): ?>selected<?php endif; ?> value="categories"><?php echo __('Categories'); ?></option>
        <option <?php if($stats_for == 'products'): ?>selected<?php endif; ?> value="products"><?php echo __('Products'); ?></option>
    </select>
</p>