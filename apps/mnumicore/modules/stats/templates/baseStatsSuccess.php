<article class="container_12">
    <div class="block-border"><div class="block-content">
            <h1>
                <?php echo __('Report'); ?>:
                <?php echo __($title); ?>
            </h1>

            <form class="form" action="" method="post">
                
                <fieldset class="grey-bg">
                    <legend><a href="javascript:void(0)"><?php echo __('Options'); ?></a></legend>
                    <div class="formFilter">
                        <?php if(isset($customFieldsView))
                                {
                                    echo include_partial($customFieldsView, $customFieldsData);
                                }
                        ?>
                        <?php echo $filter; ?>
                    </div>
                </fieldset>
                <?php if(method_exists($filter, 'getDateRange')): ?>
                <h2>
                    <?php echo __('Report generated for the range') . ": " . __($filter->getDateRange()); ?>
                </h2>
                <?php endif; ?>
                <?php if(count($pager) > 0): ?>
                <div id="chart">
                    <object height="400" width="100%" type="application/x-shockwave-flash" data="/images/open-flash-chart.swf" id="my_chart" style="visibility: visible;">
                        <param name="flashvars" value="<?php echo http_build_query(array(
                             'data-file' => url_for("@stats".ucfirst($sf_params->get('action'))."Json?".$sf_data->getRaw('url').'&limit='. $pager->getMaxPerPage().'&page='. $sf_request->getGetParameter('page', 1) . (isset($customFieldsUrlParam) ? '&'.$customFieldsUrlParam : ''))
                        ), '', '&'); ?>">
                        <param name="wmode" value="transparent">
                        <param name="movie" value="/images/open-flash-chart.swf">
                    </object>
                </div>

                <div id="stat-table">
                    <table class="table full-width">
                        <thead>
                        <tr>
                            <?php foreach($tableHeader as $head): ?>
                                <th><?php echo __($head); ?></th>
                            <?php endforeach; ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($tableRows as $row): ?>
                            <tr>
                                <?php $i=0; foreach($row as $field): ?>
                                    <td<?php echo ($i==0) ? '': ' class="right"' ?>><?php echo $field; ?></td>
                                <?php $i++; endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                            
                            <?php if(isset($pageSummary) && $showPageSummary): ?>
                            <tr class="page-result-summary">
                                <td colspan="<?php echo $colspanSummary; ?>" class="right"><?php echo __('Page summary');?></td>
                                <?php $i=0; foreach($pageSummary as $field): ?>
                                    <td class="right title"><?php echo $field; ?></td>
                                <?php $i++; endforeach; ?>
                            </tr>
                            <?php endif; ?>
                            
                            <?php if(isset($tableSummary)): ?>
                            <tr class="table-result-summary">
                                <td colspan="<?php echo $colspanSummary; ?>" class="right title"><?php echo __('Total sum');?></td>
                                <?php $i=0; foreach($tableSummary as $field): ?>
                                    <td class="right"><?php echo $field; ?></td>
                                <?php $i++; endforeach; ?>
                            </tr>
                            <?php endif; ?>
                            
                    </table>
                    <div class="table-navigation">
                        <p class="limit">
                            <?php echo __('Show'); ?>
                            <select name="limit" onchange="this.form.submit()">
                                <option<?php if($pager->getMaxPerPage() == 10): ?> selected=""<?php endif; ?>>10</option>
                                <option<?php if($pager->getMaxPerPage() == 20): ?> selected=""<?php endif; ?>>20</option>
                                <option<?php if($pager->getMaxPerPage() == 30): ?> selected=""<?php endif; ?>>30</option>
                                <option<?php if($pager->getMaxPerPage() == 40): ?> selected=""<?php endif; ?>>40</option>
                                <option<?php if($pager->getMaxPerPage() == 50): ?> selected=""<?php endif; ?>>50</option>
                            </select>
                            <?php echo __('entries'); ?>
                        </p>
                        <p class="info">
                            <?php echo __('Showing'); ?>
                            <?php echo ' ' . $pager->getFirstIndice() . ' ' . __('to') . ' ' . $pager->getLastIndice(); ?>
                            <?php echo __('of'); ?>
                            <strong>
                                <?php echo format_number_choice(
                                    '[0]No entries|[1]One entry|(1,+Inf]%count% entries',
                                    array('%count%' => $pager->getNbResults()),
                                    $pager->getNbResults()
                                ); ?>
                            </strong>
                        </p>
                        <span class="navigation">
                            <?php if($pager->getPage() > 1): ?>
                                &#171; <?php echo link_to(__('Previous'), '@stats'.ucfirst($sf_params->get('action')).
                                        '?limit='. $pager->getMaxPerPage().
                                        '&page='. $pager->getPreviousPage().
                                        '&'.$sf_data->getRaw('url').
                                        (isset($customFieldsUrlParam) ? '&'.$customFieldsUrlParam : ''));?>
                                -
                            <?php endif; ?>
                            <?php if($pager->getPage() < $pager->getLastPage()): ?>
                                <?php echo link_to(__('Next'), '@stats'.ucfirst($sf_params->get('action')).
                                        '?limit='. $pager->getMaxPerPage().
                                        '&page=' . $pager->getNextPage().
                                        '&'.$sf_data->getRaw('url').
                                        (isset($customFieldsUrlParam) ? '&'.$customFieldsUrlParam : ''));?> &#187;
                            <?php endif; ?>
                        </span>
                    </div>
                </div>
                <?php else: ?>
                    <p><?php echo __('No results'); ?></p>
                <?php endif; ?>
            </form>

        </div></div>
</article>


<script type="text/javascript">
    jQuery(function($) {
        $('form.form :input').not('[name=limit]').change(function(){
            $('#sf_content').load('<?php echo url_for("@stats".ucfirst($sf_params->get('action'))); ?>',
                $('form.form').serializeArray(), function(response, status, xhr) {
                    if (status == "error")
                    {
                        alert('<?php echo __('An error occurred'); ?>');
                    }
                }
            );
        });

        $(".navigation a").on("click", function(){
            $("#sf_content").load($(this).attr("href"));
            return false;
        });

        if($('form #range').val() == 'custom')
        {
            $('form #range').parent().hide();
            $('form #date_range_from').parent().show();
            loadDataPicker($);
        }
        else
        {
            $('form #date_range_from').parent().hide();
        }
    });
</script>