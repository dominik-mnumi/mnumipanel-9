<article class="container_12">
    <div class="block-border"><div class="block-content">
            <h1>
                <?php echo __('Use of employees'); ?>             
            </h1>

            <div class="block-controls">
                <?php echo $dateForm['year']->renderLabel(); ?>:   
                <?php echo $dateForm['year']->render(); ?>  
                <?php echo $dateForm['month']->renderLabel(); ?>:  
                <?php echo $dateForm['month']->render(); ?>
	            <?php echo $dateForm['orderType']->renderLabel(); ?>:
	            <?php echo $dateForm['orderType']->render(); ?>
            </div>

            <ul class="planning no-margin stats-employee-container-ul">
                <li class="planning-header">
                    <span><b><?php echo __('Users'); ?></b></span>
                    <?php include_partial('scale', 
                            array('statsObj' => $employeeStatsObj)); ?>
                </li>

                <?php foreach($coll as $key => $rec): ?>  
                <?php $orderCount = $rec->getStatsOrdersCount($year, $month, $orderType); ?>
                <li>
                    <a href="<?php echo url_for('userListEdit', $rec); ?>">
                        <?php echo image_tag('icons/fugue/user-red.png', array('width' => 16, 'height' => 16)); ?> <?php echo $rec; ?>
                    </a>
                    <ul>
                        <?php if($orderCount): ?>
                        <li style="float: left; left: 0%; right: <?php echo $employeeStatsObj->getStatsCssRightPercentage($orderCount); ?>%;">
                            <a href="<?php echo url_for('userListEdit', $rec); ?>">
                                <span title="<?php echo __plural('1 order', '@count orders', array('@count' => $orderCount)); ?>" class="text-right event-<?php echo $key % 2 ? 'blue' : 'green'; ?>">
                                    <?php echo __plural('1 order', '@count orders', array('@count' => $orderCount)); ?>&nbsp
                                </span>
                            </a>
                        </li>      
                        <?php endif; ?>
                    </ul>
                </li>   
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</article>

<script type ="text/javascript">
jQuery(document).ready(function($)
{
    function loader()
    {
        $(".stats-employee-container-ul").html('<div style="text-align: center; margin: 50px 0;"><?php echo image_tag('loader.gif'); ?><div>');
    }
    
    function loadStats()
    {
        $.ajax({
            url: "<?php echo url_for('statsEmployee'); ?>",
            type: "GET",
            data: 
            { 
                year: $('#year').val(), 
                month: $('#month').val(),
	            orderType: $('#orderType').val()
            },
            success: function(html)
            {
                $("#sf_content").html(html);
            }
        });
    }
    
    $('#year, #month, #orderType').change(function()
    {
        loader();
    	loadStats();
    });
});
</script>