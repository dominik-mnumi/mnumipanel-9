<article class="container_12">
    <div class="block-border"><div class="block-content">
            <h1>
                <?php echo __('Sale'); ?>             
            </h1>

            <div class="block-controls">
                <div class="legend-div" title="<?php echo __('Legend'); ?>">
                    <div class="legend-color-div purple-background"></div>
                    <div class="legend-description-div"><?php echo __('Orders from shop'); ?></div>
                    <div class="legend-color-div green-background"></div>
                    <div class="legend-description-div"><?php echo __('Orders from panel'); ?></div>
                </div>
                <?php echo $dateForm['month']->renderLabel(); ?>:
                <?php echo $dateForm['month']->render(); ?>

                <?php echo $dateForm['shop']->renderLabel(); ?>:
                <?php echo $dateForm['shop']->render(); ?>
            </div>

            <ul class="planning no-margin stats-sale-container-ul">
                <li class="planning-header">
                    <span><b><?php echo __('Sale'); ?></b></span>
                    <?php include_partial('scale',
                            array('statsObj' => $saleStatsObj)); ?>
                </li>


                <?php foreach($results as $year => $statsArr): ?>
                <li>
                    <a href="#"><?php echo image_tag('icons/fugue/newspaper.png', array('width' => 16, 'height' => 16)); ?> <?php echo $year; ?></a>
                    <ul>
                        <?php if($statsArr[0]['shopOrderCount'] != 0): ?>
                        <li style="left: 0%; right: <?php echo $saleStatsObj->getStatsCssRightPercentage($statsArr[0]['shopIncome']); ?>%;">
                            <a href="#">
                                <span title="<?php echo __plural('1 order', '@count orders', array('@count' => $statsArr[0]['shopOrderCount'])); ?>; <?php echo $sf_user->formatCurrency($statsArr[0]['shopIncome']); ?> (<?php echo __('net'); ?>)" class="text-right event-purple">
                                    <?php echo __plural('1 order', '@count orders', array('@count' => $statsArr[0]['shopOrderCount'])); ?>; <?php echo $sf_user->formatCurrency($statsArr[0]['shopIncome']); ?> (<?php echo __('net'); ?>)&nbsp
                                </span>
                            </a>
                        </li>    
                        <?php endif; ?>
                        <?php if($statsArr[0]['panelOrderCount'] != 0): ?>
                        <li style="left: <?php echo $saleStatsObj->getStatsCssLeftPercentage($statsArr[0]['shopIncome']); ?>%; right: <?php echo $saleStatsObj->getStatsCssRightPercentage($statsArr[0]['shopIncome'] + $statsArr[0]['panelIncome']); ?>%;">
                            <a href="#">
                                <span title="<?php echo __plural('1 order', '@count orders', array('@count' => $statsArr[0]['panelOrderCount'])); ?>; <?php echo $sf_user->formatCurrency($statsArr[0]['panelIncome']); ?> (<?php echo __('net'); ?>)" class="text-right event-green">
                                    <?php echo __plural('1 order', '@count orders', array('@count' => $statsArr[0]['panelOrderCount'])); ?>; <?php echo $sf_user->formatCurrency($statsArr[0]['panelIncome']); ?> (<?php echo __('net'); ?>)&nbsp
                                </span>
                            </a>
                        </li> 
                        <?php endif; ?>
                    </ul>
                </li>  
                <li>         
                    <a href="#"></a>
                    <ul>
                        <?php if($statsArr[1]['shopOrderCount'] != 0): ?>
                        <li style="left: 0%; right: <?php echo $saleStatsObj->getStatsCssRightPercentage($statsArr[1]['shopIncome']); ?>%;">
                            <a href="#">
                                <span title="<?php echo __plural('1 order', '@count orders', array('@count' => $statsArr[1]['shopOrderCount'])); ?>; <?php echo $sf_user->formatCurrency($statsArr[1]['shopIncome']); ?> (<?php echo __('net'); ?>)" class="text-right event-purple">
                                    <?php echo __plural('1 order', '@count orders', array('@count' => $statsArr[1]['shopOrderCount'])); ?>; <?php echo $sf_user->formatCurrency($statsArr[1]['shopIncome']); ?> (<?php echo __('net'); ?>)&nbsp
                                </span>
                            </a>
                        </li>    
                        <?php endif; ?>
                        <?php if($statsArr[1]['panelOrderCount'] != 0): ?>
                        <li style="left: <?php echo $saleStatsObj->getStatsCssLeftPercentage($statsArr[1]['shopIncome']); ?>%; right: <?php echo $saleStatsObj->getStatsCssRightPercentage($statsArr[1]['shopIncome'] + $statsArr[1]['panelIncome']); ?>%;">
                            <a href="#">
                                <span title="<?php echo __plural('1 order', '@count orders', array('@count' => $statsArr[1]['panelOrderCount'])); ?>; <?php echo $sf_user->formatCurrency($statsArr[1]['panelIncome']); ?> (<?php echo __('net'); ?>)" class="text-right event-green">
                                    <?php echo __plural('1 order', '@count orders', array('@count' => $statsArr[1]['panelOrderCount'])); ?>; <?php echo $sf_user->formatCurrency($statsArr[1]['panelIncome']); ?> (<?php echo __('net'); ?>)&nbsp
                                </span>
                            </a>
                        </li> 
                        <?php endif; ?>
                    </ul>
                </li>  
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</article>

<script type ="text/javascript">
jQuery(document).ready(function($)
{
    function loader()
    {
        $(".stats-sale-container-ul").html('<div style="text-align: center; margin: 50px 0;"><?php echo image_tag('loader.gif'); ?><div>');
    }
    
    function loadStats()
    {
        $.ajax({
        url: "<?php echo url_for('statsSale'); ?>",
        type: "GET",
        data: 
        { 
            year: $('#year').val(), 
            month: $('#month').val(),
            shop:  $('#shop').val(),
        },
        success: function(html)
        {
            $("#sf_content").html(html);
        }
        });
    }
    $('#year, #month, #shop').change(function()
    {
        loader();
    	loadStats();
    });
});
</script>