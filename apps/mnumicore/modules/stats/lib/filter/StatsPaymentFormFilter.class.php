<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class StatsPaymentFormFilter extends OrderFormFilter
{
    public function configure()
    {
        parent::configure();

        // choices

        $statusChoices = array(
            'none' => 'all',
            'cc' => 'closed (paid)',
            'cnc' => 'closed (not paid)',
        );

        // widgets

        $this->setWidget('status', new sfWidgetFormChoice(array(
            'choices' => $statusChoices,
            'label' => 'Order payment status',
        )));

        // validators


        $this->setValidator('status', new sfValidatorChoice(array(
            'choices' => array_keys($statusChoices),
            'required' => true,
        )));

        $this->useFields(array(
            'range',
            'date_range',
            'status',
            'shop_name'
        ));

        $this->getWidgetSchema()->setNameFormat('%s');
        $this->disableCSRFProtection();
    }

    /**
     * Add custom query from_date
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @param string $value
     */
    public function addStatusColumnQuery(Doctrine_Query $query, $field, $value)
    {
        $query->andWhereNotIn('order_status_name', array(
            'deleted', 'draft', 'calculation'
        ));

        if($value == 'cc')
        {
            $query->addWhere('op.payment_status_name = ?', 'paid');
        }
        elseif($value == 'cnc')
        {
            $query->addWhere('op.payment_status_name != ?', 'paid');
        }

    }

}
