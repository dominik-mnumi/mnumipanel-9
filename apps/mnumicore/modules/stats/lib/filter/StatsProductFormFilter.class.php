<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class StatsProductFormFilter extends ProductFormFilter
{
    public function configure()
    {
        parent::configure();

        $orderStatusNameArray[''] = '';
        $orderStatusNameArray += OrderStatusTable::getInstance()->getStatusArr();

        $ranges = array(
            'all' => 'all days',
            'cm' => 'current month',
            'pm' => 'previous month',
            'cq' => 'current quarter',
            'pq' => 'previous quarter',
            'cy' => 'current year',
            'py' => 'previous year',
            'custom' => 'exact date range',
        );

        $this->setWidgets(array(
            'range' => new sfWidgetFormChoice(array(
                'choices' => $ranges,
                'label' => 'Date of placing the order',
            )),
            'date_range' => new sfWidgetFormDateRange(array(
                'from_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
                'to_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
                'label' => 'Date of placing the order',
            ), array('class' => 'datepicker')),
            'shop_name' => new sfWidgetFormDoctrineChoice(array(
                'model' => 'Shop',
                'add_empty' => 'all',
                'label' => 'Source',
                'query' => ShopTable::getInstance()->createQuery()
                    ->andWhere('type = ?', 'shop'),
            )),
            'order_status_name' => new sfWidgetFormChoice(array(
                'choices' => $orderStatusNameArray,
                'label' => 'Order payment status',
            )),
        ));

        $this->setValidators(array(
            'shop_name' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'Shop', 'column' => 'name')),
            'order_status_name' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($orderStatusNameArray))),

            'range' => new sfValidatorChoice(array('choices' => array_keys($ranges))),
            'date_range' => new sfValidatorDateRange(array(
                'from_date' => new sfValidatorDate(array('required' => false, 'datetime_output' => 'Y-m-d')),
                'to_date' => new sfValidatorDate(array('required' => false, 'datetime_output' => 'Y-m-d')),
            )),
        ));

        $this->useFields(array(
            'range',
            'date_range',
            'order_status_name',
            'shop_name'
        ));

        $this->getWidgetSchema()->setNameFormat('%s');
        $this->disableCSRFProtection();
    }

    protected function doBuildQuery(array $values)
    {
        switch ($values['range']) {
            case 'cm': // current month
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, date("m"), 1,   date("Y"))),
                    'to' => date('Y-m-d'),
                );
                break;
            case 'pm': // previous month
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, date("m")-1, 1,   date("Y"))),
                    'to' => date('Y-m-d', mktime(0, 0, 0, date("m"), 0,   date("Y"))),
                );
                break;
            case 'cq': // current quarter
                $startMonth = date('m') - ((date('m')-1) % 3 );
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, $startMonth, 1,   date("Y"))),
                    'to' => date('Y-m-d', mktime(0, 0, 0, $startMonth+3, 0,   date("Y"))),
                );
                break;
            case 'pq': // previous quarter
                $startMonth = date('m') - 3 - ((date('m')-1) % 3 );
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, $startMonth, 1,   date("Y"))),
                    'to' => date('Y-m-d', mktime(0, 0, 0, $startMonth+3, 0,   date("Y"))),
                );
                break;
            case 'cy': // current year
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1,   date("Y"))),
                    'to' => date('Y-m-d'),
                );
                $this->dateRange = date("Y");
                break;
            case 'py': // previous year
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1,   date("Y")-1)),
                    'to' => date('Y-m-d', mktime(0, 0, 0, 1, 0,   date("Y"))),
                );
                break;
            case 'all': // all days
                $youngerOrder = OrderTable::getInstance()->createQuery()->limit(1)->fetchOne();

                $values['date_range'] = array(
                    'from' => $youngerOrder ? $youngerOrder->getAcceptedAt('Y-m-d') : date('Y-m-d'),
                    'to' => date('Y-m-d'),
                );
                break;
            case 'custom': // custom date range
                break;
        }

        $this->dateRange =
            date('Y-m-d', strtotime($values['date_range']['from'])) .
                ' - ' .
                date('Y-m-d', strtotime($values['date_range']['to']));

        return parent::doBuildQuery($values);
    }

    /**
     * Add custom query from_date
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    public function addDateRangeColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if(array_key_exists('from', $value))
        {
            $query->andWhere('accepted_at >= ?', $value['from']);
        }

        if(array_key_exists('to', $value))
        {
            $query->andWhere('accepted_at <= ?', $value['to']);
        }
    }

    public function addOrderStatusNameColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value != '')
        {
            $query->andWhere('o.order_status_name = ?', $value);
        }
    }

    public function addShopNameColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value != '')
        {
            $query->andWhere('o.shop_name = ?', $value);
        }

    }

    public function getDateRange()
    {
        return $this->dateRange;
    }
}
