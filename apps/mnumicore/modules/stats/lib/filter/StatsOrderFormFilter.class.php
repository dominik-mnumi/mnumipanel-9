<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class StatsOrderFormFilter extends StatsPaymentFormFilter
{
    public function configure()
    {
        parent::configure();

        // choices

        $groupByChoices = array(
            'd' => 'days',
            'w' => 'weeks',
            'm' => 'months',
            'q' => 'quarters',
            'y' => 'years',
        );

        // widgets
        $this->setWidget('group_by', new sfWidgetFormChoice(array(
            'choices' => $groupByChoices,
            'label' => 'Show as',
        )));

        // validators
        $this->setValidator('group_by', new sfValidatorChoice(array(
            'choices' => array_keys($groupByChoices),
            'required' => true,
        )));

        $this->useFields(array(
            'range',
            'date_range',
            'group_by',
            'status',
            'shop_name'
        ));

        $this->getWidgetSchema()->setNameFormat('%s');
        $this->disableCSRFProtection();
    }

    /**
     * Add custom query from_date
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @param string $value
     */
    public function addGroupByColumnQuery(Doctrine_Query $query, $field, $value)
    {
        switch ($value) {
            case 'm': // month
                $query->addSelect('DATE_FORMAT( accepted_at, "%Y-%m" ) AS date')
                    ->addGroupBy('YEAR( accepted_at )')
                    ->addGroupBy('MONTH( accepted_at )')
                    ->addOrderBy('YEAR( accepted_at )')
                    ->addOrderBy('MONTH( accepted_at )');
                break;
            case 'w': // week
                $query->addSelect('DATE_FORMAT( accepted_at, "%Y, %v" ) AS date')
                    ->addGroupBy('WEEK( accepted_at, 1 )')
                    ->addGroupBy('MONTH( accepted_at )')
                    ->addOrderBy('WEEK( accepted_at, 1 )')
                    ->addOrderBy('MONTH( accepted_at )');
                break;
            case 'd': // day
                $query->addSelect('DATE_FORMAT( accepted_at, "%Y-%m-%d" ) AS date')
                    ->addGroupBy('YEAR( accepted_at )')
                    ->addGroupBy('MONTH( accepted_at )')
                    ->addGroupBy('DAY( accepted_at )')
                    ->addOrderBy('YEAR( accepted_at )')
                    ->addOrderBy('MONTH( accepted_at )')
                    ->addOrderBy('DAY( accepted_at )');
                break;
            case 'q': // quarter
                $query->addSelect('concat(DATE_FORMAT( accepted_at, "%Y" ), ", Q", (QUARTER(accepted_at))) AS date')
                    ->addGroupBy('YEAR( accepted_at )')
                    ->addGroupBy('QUARTER( accepted_at )')
                    ->addOrderBy('YEAR( accepted_at )')
                    ->addOrderBy('QUARTER( accepted_at )');
                break;
            case 'y'; // year
            default:
                $query->addSelect('DATE_FORMAT( accepted_at, "%Y" ) AS date')
                    ->addGroupBy('YEAR( accepted_at )')
                    ->addOrderBy('YEAR( accepted_at )');
                break;
        }

    }

}
