<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class StatsOrderCategoryFormFilter extends StatsOrderFormFilter
{
    public function configure()
    {
        parent::configure();

        $this->useFields(array(
            'range',
            'date_range',            
            'status',
            'shop_name'
        ));

        $this->getWidgetSchema()->setNameFormat('%s');
        $this->disableCSRFProtection();
    }
}
