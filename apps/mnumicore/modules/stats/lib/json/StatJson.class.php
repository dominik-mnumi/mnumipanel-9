<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */ 
class StatJson 
{
    protected $model = null;
    protected $options = array();
    protected $query;
    protected $result = array();

    protected $maxAxisY = 0;
    protected $maxAxisYRight = 0;

    public function __construct($options = array())
    {
        $this->options = $options;

        $this->result = array(
            'title' => array('text' => ''),
            'elements' => array(),
            'bg_colour' => '#FFFFFF',
        );
    }

    public function setQuery(Doctrine_Query $query)
    {
        $this->query = $query;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getModel()
    {
        $model = ucfirst($this->model) . 'Table';

        return $model::getInstance();
    }

    public function getMethod()
    {
        $methodName = $this->options['method_name'];

        if (!method_exists($this->getModel(), $methodName)) {
            throw new Exception('Method ' . $methodName . ' does not exist for class ' . get_class($this->getModel()));
        }

        return $this->getModel()->$methodName();
    }

    public function asArray()
    {        
        $rows = $this->getQuery()->execute();

        $params = $this->getMethod();

        $this->result = array(
            'elements' => array(),
            'bg_colour' => '#FFFFFF',
            'title' => array('text' => ''),
        );

        // add axis
        if(array_key_exists('x_axis', $params))
        {
            $this->result['x_axis'] = $this->renderAxis($params['x_axis'], $rows);
        }

        if(array_key_exists('y_axis', $params))
        {
            $this->result['y_axis'] = $this->renderAxis($params['y_axis'], $rows);
        }

        if(array_key_exists('y_axis_right', $params))
        {
            $this->result['y_axis_right'] = $this->renderAxis($params['y_axis_right'], $rows);
        }

        $i = 0;
        foreach ($params['elements'] as $param) {
            $type = $param['type'];

            $element = array();

            if ($type == 'pie') {
                $element = $this->renderElementPie($param, $rows);
            } elseif ($type == 'hbar') {
                $element = $this->renderElementHbar($param, $rows);

            } elseif ($type == 'bar_stack') {
                $element = $this->renderElementBarStack($param, $rows);
            } elseif ($type == 'bar') {
                $element = $this->renderElementBar($param, $rows);
            } elseif ($type == 'area') {
                $element = $this->renderElementArea($param, $rows);
            } elseif ($type == 'line') {
                $element = $this->renderElementLine($param, $rows);
            } else {
                throw new Exception('Wrong type: ' . $type);
            }

            if (array_key_exists('axis', $param) && $param['axis'] == 'right') {
                $element['axis'] = $param['axis'];
            }

            $this->result['elements'][] = $element;

        }

        return $this->result;
    }

    protected function getI18n()
    {
        return $this->options['i18n'];
    }

    protected function renderAxis($param, $rows)
    {
        $displayRange = array_key_exists('displayRange', $param)
            ? $param['displayRange']
            : false;

        $labels = array();
        $maxValue = 0;
        $minValue = 0;

        if(!$displayRange)
        {
            $labelName = $param['label'];

            foreach ($rows as $row) {
                $labels[] = $this->getI18n()->__($row->$labelName);
            }

            return array(
                'offset' => 1,
                'colour' => '#DADADA',
                'grid-colour' => '#DADADA',
                'steps' => 1,
                'labels' => array(
                    'labels' => $labels,
                    'steps' => 1,
                ),
            );
        }

        // IF display ranges

        $values = $param['value'];

        if(!is_array($values))
        {
            $values = array($values);
        }

        foreach ($rows as $row) {
            $val = 0;
            foreach($values as $value)
            {
                $valueName = 'get' . ucfirst($value);
                $val += (float)$row->$valueName();

            }
            $maxValue = $val > $maxValue ? $val : $maxValue;
            $minValue = $val < $minValue ? $val : $minValue;
        }

        $maxValue += ($maxValue * 0.1);
        $minValue = $minValue * 1.1;

        return array(
            'colour' => '#DADADA',
            'offset' => false,
            'min' => $minValue,
            'max' => $maxValue,
            'steps' => floor($maxValue / 10),
            'labels' => array(
                'text' => '#val#',
            ),
        );

    }

    protected function renderElementPie($param, $rows) {
        $valueName = 'get' . ucfirst($param['value']);
        $labelName = $param['label'];

        $values = array();

        foreach ($rows as $row) {
            $values[] = array(
                'value' => (float)$row->$valueName(),
                'label' => $this->getI18n()->__($row->$labelName),
                'label-colour' => '#333333',
            );
        }

        return array(
            'type' => 'pie',
            'values' => $values,
            'colours' => $param['colours'],
            'tip' => $param['tip'],
        );
    }

    protected function renderElementHbar($param, $rows)
    {
        $valueName = 'get' . ucfirst($param['value']);

        $values = array();

        foreach ($rows as $row) {
            $left = 0;

            $val = array(
                'left' => $left,
                'right' => (float)$row->$valueName(),
            );

            $values[] = $val;
        }

        return array(
            'type' => 'hbar',
            'values' => $values,
            'text' => $param['label'],
            'font-size' => 10,
            'colour' => $param['colour'],
            'text' => $param['text'],
            'tip' => $this->getI18n()->__($param['tip']),
            'font-size' => (array_key_exists('font-size', $param)) ? $param['font-size'] : 12,
        );
    }


    protected function renderElementBarStack($param, $rows) {
        $labelName = $param['label'];
        $tipFormat = $this->getI18n()->__($param['tip']);

        foreach ($rows as $row) {
            $val = array();
            foreach ($param['value'] as $el) {
                $valueName = 'get' . ucfirst($el);
                $val[] = (float)$row->$valueName();
            }
            $values[] = $val;
        }

        return array(
            'type' => 'bar_stack',
            'values' => $values,
            'colours' => $param['colours'],
            'text' => $labelName,
            'font-size' => 10,
            'tip' => $tipFormat,
        );
    }


    protected function renderElementBar($param, $rows) {
        $tipFormat = $this->getI18n()->__($param['tip']);
        $valueName = 'get' . ucfirst($param['value']);

        // prepare tip
        $extraTip = array_key_exists('extra_tip', $param) ? $param['extra_tip'] : false;
        $extraTipMethod = ($extraTip) ? 'get' . $extraTip : false;

        foreach ($rows as $row) {
            $extraTip = ($extraTipMethod) ? $row->$extraTipMethod() : '';

            $values[] = array(
                'top' => (float)$row->$valueName(),
                'tip' => ($extraTip != '') ? str_replace('##extra_tip##', $extraTip, $tipFormat) : $tipFormat,
            );
        }

        return array(
            'type' => 'bar',
            'values' => $values,
            'colour' => $param['colour'],
            'dot-style' => array(
                'type' => 'solid-dot',
                'fill-alpha' => 0.35,
                'values' => array(),
                'colour' => $param['colour'],
                'dot-size' => 4,
            )
        );
    }

    protected function renderElementArea($param, $rows) {
        $tipFormat = $this->getI18n()->__($param['tip']);
        $valueName = 'get' . ucfirst($param['value']);

        // prepare tip
        $extraTip = array_key_exists('extra_tip', $param) ? $param['extra_tip'] : false;
        $extraTipMethod = ($extraTip) ? 'get' . $extraTip : false;


        foreach ($rows as $row) {
            $extraTip = ($extraTipMethod) ? $row->$extraTipMethod() : '';

            $values[] = array(
                'value' => (float)$row->$valueName(),
                'tip' => ($extraTip != '') ? str_replace('##extra_tip##', $extraTip, $tipFormat) : $tipFormat,
            );
        }

        return array(
            'type' => 'area',
            'values' => $values,
            'colour' => $param['colour'],
            'dot-size' => 2,
            'dot-style' => array(
                'type' => 'solid-dot',
                'fill-alpha' => 0.35,
                'values' => array(),
                'colour' => $param['colour'],
                'dot-size' => 4,
            ),
        );
    }

    protected function renderElementLine($param, $rows) {
        $tipFormat = $this->getI18n()->__($param['tip']);
        $valueName = 'get' . ucfirst($param['value']);

        // prepare tip
        $extraTip = array_key_exists('extra_tip', $param) ? $param['extra_tip'] : false;
        $extraTipMethod = ($extraTip) ? 'get' . $extraTip : false;


        foreach ($rows as $row) {
            $extraTip = ($extraTipMethod) ? $row->$extraTipMethod() : '';

            $values[] = array(
                'value' => round((float)$row->$valueName(), 2),
                'tip' => ($extraTip != '') ? str_replace('##extra_tip##', $extraTip, $tipFormat) : $tipFormat,
            );
        }

        return array(
            'type' => 'line',
            'values' => $values,
            'colour' => $param['colour'],
            'dot-size' => 2,
            'dot-style' => array(
                'tip' => '',
            ),
        );
    }



}
