<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Stats date form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class StatsDateForm extends BaseForm
{
    public static $yearArr = array(2012, 2011, 2010, 2009, 2008, 2007, 2006,
        2005, 2003, 2002, 2001, 2000, 1999);
    
    public static $monthArr = array(1 => 'January', 2 => 'February', 3 => 'March', 
            4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 
            9 => 'September', 10 => 'October', 11 => 'November', 
            12 => 'December');

	public static $orderTypeArr = array(
		'all' => 'All',
		'toBook' => 'To book',
		'paid' => 'Paid');

    public function configure()
    {
        $this->declareYearArray();
        
        //$yearArrTmp[''] = 'All years';
        $yearArr = array_combine(self::$yearArr, self::$yearArr);
        //$yearArr = $yearArrTmp + $yearArr;

        //$monthArrTmp[''] = 'All months';    
        $monthArr = self::$monthArr;
        //$monthArr = $monthArrTmp + $monthArr;
        
        $this->setWidget('year', new sfWidgetFormChoice(array(
            'label' => 'Year',
            'choices' => $yearArr
        )));
        
        $this->setWidget('month', new sfWidgetFormChoice(array(
            'label' => 'Month',
            'choices' => $monthArr
        )));

	    $this->setWidget('orderType', new sfWidgetFormChoice(array(
	                                                          'label' => 'Order payment status',
	                                                          'choices' => self::$orderTypeArr
	                                                     )));

        $this->setWidget('shop', new sfWidgetFormDoctrineChoice(array(
            'model' => 'Shop',
            'add_empty' => 'All',
            'label' => 'Shop'
        )));
    }
    
    private function declareYearArray()
    {
        self::$yearArr = array();
        
        if(sfConfig::get('app_demo_mode', false))
        {
            $firstYear = 1999;
        }
        else
        {
            $firstOrder = OrderTable::getInstance()->createQuery()->orderBy('id')->limit(1)->fetchOne();
        
            if(!$firstOrder)
            {
                $firstYear = date('Y');
            }
            else
            {
                $firstYear = date('Y', strtotime($firstOrder->getCreatedAt()));
            }
        }
        
        for($i=date('Y'); $i>=$firstYear; $i--)
        {
            self::$yearArr[] = $i;
        }
    }
}