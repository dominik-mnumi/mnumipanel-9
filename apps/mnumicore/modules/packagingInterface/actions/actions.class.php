<?php

/**
 * packagingInterface actions.
 *
 * @package    mnumicore
 * @subpackage packagingInterface
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class packagingInterfaceActions extends sfActions
{
   /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    {
    }
    
    /**
  * Executes barcode action
  *
  * @param sfRequest $request A request object
  */
    public function executeBarcode(sfWebRequest $request)
    {
        $barcodePackaging = new BarcodePackaging($request->getParameter('barcode'));
        $type = $barcodePackaging->getTableNameFromType();
      
        //if type is user request is for authorize
        if($type == 'sfGuardUserTable')
        {
            $this->result = array('auth' => $this->getUser()
                    ->authorizeUsingPasswordSaltAndId($barcodePackaging->getId(), 
                            $barcodePackaging->getAdditionalNumber()));
            
            return sfView::SUCCESS;
        }
        // save new worklog
        elseif($type == 'OrderTable')
        {
            if(!$this->getUser()->isAuthorizedUsingPasswordSaltAndId())
            {
                throw new Exception('You have to be authorized to create new worklog.');
            }
            $this->result = $barcodePackaging->orderToShelf();

            /** @var OrderPackage $orderPackage */
            $orderPackage = $barcodePackaging->getObject()->getOrderPackage();

            // send notification
            if(isset($this->result['readyEvent']) && $this->result['readyEvent'] && $orderPackage->getOrderPackageStatusName() == OrderPackageStatusTable::$completed)
            {
                $webapiKey = null;

                if($barcodePackaging->getObject() instanceof Order) {
                    $webapiKey = $barcodePackaging->getObject()->getShopName();
                }

                // triggers the event
                $this->dispatcher->notify(new sfEvent($this,
                        'package.ready',
                        array('obj' => $orderPackage, 'webapi_key' => $webapiKey)));
            }
            
            $this->object = $barcodePackaging->getObject();
            return sfView::SUCCESS;
        }
      
        throw new Exception('This type of barcode is not supported in printerInterface.');
    }
    
    /**
     * Executes printlabels action
     *
     * @param sfRequest $request A request object
     */
    public function executePrintlabels(sfWebRequest $request)
    {
        $allPrintlabels = PrintlabelQueueTable::getInstance()
                ->loadQueue('PENDING')
                ->toArray();
        
        return $this->renderText(json_encode($allPrintlabels));
    }
    
    /**
     * Executes printlabels action
     *
     * @param sfRequest $request A request object
     */
    public function executePrintAgain(sfWebRequest $request)
    {
        $orderPackage = OrderPackageTable::getInstance()->find($request->getParameter('id'));
        
        if(!$orderPackage)
        {
            $this->forward404();
        }
        
        PrintlabelQueueTable::getInstance()
                ->saveDefaultPrintlabelQueueWithAttributes($orderPackage);
        
        return sfView::NONE;
    }
}
