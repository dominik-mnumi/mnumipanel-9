<form id="new_field_category_form" method = "post" class="form"
      action="<?php echo $form->isNew() ? url_for('settingsPaymentAdd') : url_for('settingsPaymentEdit', $form->getObject())?>">
    <?php echo $form->renderHiddenFields() ?>

    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'settingsPayment')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo $form->isNew() ? __('Create payment') : __('Edit payment')  ?></h1>
                    <p>
                        <?php echo $form['name']->renderError(); ?>
                        <?php echo $form['name']->renderLabel(); ?>
                        <?php echo $form['name']->render(); ?>
                    </p>
                    <p>
                        <?php echo $form['label']->renderError(); ?>
                        <?php echo $form['label']->renderLabel(); ?>
                        <?php echo $form['label']->render(); ?>
                    </p>

                    <p>
                        <?php echo $form['description']->renderError(); ?>
                        <?php echo $form['description']->renderLabel(); ?>
                        <?php echo $form['description']->render(); ?>
                    </p>
                    
                    <p>
                        <?php echo $form['active']->renderError(); ?>
                        <?php echo $form['active']->render(); ?>
                        <?php echo $form['active']->renderLabel(); ?>                     
                    </p>
                    
                    <p>
                        <?php echo $form['for_office']->renderError(); ?>
                        <?php echo $form['for_office']->render(); ?>
                        <?php echo $form['for_office']->renderLabel(); ?>                       
                    </p>
                    
                    <p>
                        <?php echo $form['required_credit_limit']->renderError(); ?>                       
                        <?php echo $form['required_credit_limit']->render(); ?>
                        <?php echo $form['required_credit_limit']->renderLabel(); ?>
                    </p>
                    
                    <p>
                        <?php echo $form['cost']->renderError(); ?>    
                        <?php echo $form['cost']->renderLabel(); ?>                   
                        <?php echo $form['cost']->render(); ?>
                        
                    </p>
                    
                    <p>
                        <?php echo $form['free_payment_amount']->renderError(); ?>    
                        <?php echo $form['free_payment_amount']->renderLabel(); ?>                   
                        <?php echo $form['free_payment_amount']->render(); ?>                        
                    </p>
                    
                    <p>
                        <?php echo $form['income_pattern']->renderError(); ?>    
                        <?php echo $form['income_pattern']->renderLabel(); ?>                   
                        <?php echo $form['income_pattern']->render(); ?>                       
                    </p>
                    
                    <p>
                        <?php echo $form['outcome_pattern']->renderError(); ?>    
                        <?php echo $form['outcome_pattern']->renderLabel(); ?>                   
                        <?php echo $form['outcome_pattern']->render(); ?>                       
                    </p>
                    
                    <p>
                        <?php echo $form['deposit_enabled']->renderError(); ?>
                        <?php echo $form['deposit_enabled']->render(); ?>
                        <?php echo $form['deposit_enabled']->renderLabel(); ?>                     
                    </p>
                    
                    <div id="carriers" style="margin: 0px;">
                        <?php echo $form['custom_carriers']->renderError(); ?>
                        <?php echo $form['custom_carriers']->renderLabel(); ?>    
                        <?php echo $form['custom_carriers']->render(); ?>
                                           
                    </div>
                    <br/>               
                    <div id="pricelists" style="margin: 0px;">
                        <?php echo $form['custom_pricelists']->renderError(); ?>
                        <?php echo $form['custom_pricelists']->renderLabel(); ?>
                        <?php echo $form['custom_pricelists']->render(); ?>
                    </div>               
                </div>
            </div>
        </section>
    </article>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        // pricelists
        function pricelistSelect()
        {
            if(!$('#payment_custom_pricelists___ALL__').is(':checked'))
            {
                $('#pricelists .checkbox_list li:not(:first)').show();
            }
            else
            {
                $('#pricelists .checkbox_list li:not(:first)').hide();
            }
        }
        
        $('#payment_custom_pricelists___ALL__').click(function() {
            pricelistSelect();
        });
        pricelistSelect();   

        //carriers
        function carrierSelect()
        {
            if(!$('#payment_custom_carriers___ALL__').is(':checked'))
            {
                $('#carriers .checkbox_list li:not(:first)').show();
            }
            else
            {
                $('#carriers .checkbox_list li:not(:first)').hide();
            }
        }

        $('#payment_custom_carriers___ALL__').click(function() {
            carrierSelect();
        });
        carrierSelect();
        
    });
</script>