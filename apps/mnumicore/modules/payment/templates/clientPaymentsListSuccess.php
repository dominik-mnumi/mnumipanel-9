<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_8">
        <div class="block-border">
            <div class="block-content form" name="table_form" id="table_form">
                <h1>
                    <?php echo __('Client payment list'); ?>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>
