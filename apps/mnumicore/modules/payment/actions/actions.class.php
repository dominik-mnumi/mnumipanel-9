<?php

/**
 * payment actions.
 *
 * @package    mnumicore
 * @subpackage payment
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class paymentActions extends tablesActions
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->displayTableFields = array('Label' => array('translate' => true));
        $this->displayGridFields = array(
            'Label'  => array('destination' => 'name', 'translate' => true),
        );
        $this->sortableFields = array('label');
        $this->modelObject = 'Payment';

        // hides many actions
        $this->manyActionAvailable = false;

        // hides checkboxes
        $this->layoutOptions['showCheckboxes'] = false;

        $this->tableOptions = $this->executeTable();
      
        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->form = new PaymentForm();
        $this->proceedForm($this->form);
        $this->setTemplate('edit');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $this->form = new PaymentForm($this->getRoute()->getObject());
        $this->proceedForm($this->form);
    }

    public function executeDelete(sfWebRequest $request)
    {
        $object = $this->getRoute()->getObject();
        
        if($object->canDelete())
        {
            $object->delete();
            $flashMessage = array(
                'message' => 'Deleted successfully.',
                'messageType' => 'msg_success',
            );
        }
        else
        {
            $flashMessage = array(
                'message' => 'Deleted failed.',
                'messageType' => 'msg_error',
            );
        }
        
        $this->getUser()->setFlash('info_data', $flashMessage);
        $this->redirect('@settingsPayment');
    }

    private function proceedForm($form)
    {
        if(!$this->getRequest()->isMethod('post'))
        {
            return;
        }

        $this->form->bind($this->getRequest()->getParameter($this->form->getName()));

        if($this->form->isValid())
        {
            $this->form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            $this->redirect('@settingsPaymentEdit?id=' . $this->form->getObject()->id);
        }
    }
    
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Payment';
        parent::executeDeleteMany($request);
    }
    
    /**
     * Get client payments list
     *
     * @param sfRequest $request A request object
     */    
    public function executeClientPaymentsList(sfWebRequest $request)
    {
        $this->displayTableFields = array(
            'Name' => array('label' => 'Name'),
            'Description' => array('label' => 'Description'),
            'Cost' => array('label' => 'Cost')
        );
        $this->displayGridFields = array(
            'Name' => array('destination' => 'name', 'label' => 'Name'),
            'Description' => array('label' => 'Description'),
            'Cost' => array('label' => 'Cost')
        );
        $this->sortableFields = array('name', 'cost');
        $this->modelObject = 'Payment';
        $this->actions = array();
        
        $this->customRoutingParameters = array(
            'id' => $this->getRoute()->getObject()->id,
        );
        
        $this->customQuery = PaymentTable::getInstance()->createQuery('p')
                            ->leftJoin('p.PaymentPricelists pp')
                            ->leftJoin('pp.Pricelist pl')
                            ->leftJoin('pl.Clients c ON c.pricelist_id = pl.id')
                            ->where('c.id = ?', $request->getParameter('id'));

        $this->tableOptions = $this->executeTable();

        if ($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }        
    }
    
    /**
     * Handle PayU response
     *
     * @param sfRequest $request A request object
     */
    public function executeHandlePayUResponse(sfWebRequest $request)
    {
        $this->setLayout(false);
        
        if(!$this->getRequest()->isMethod('post'))
        {
            $this->getLogger()->err('[PayU] Data must be send by POST method');
            return $this->renderText('OK');
        }

        $posId = $request->getParameter('pos_id');
        $sessionId = $request->getParameter('session_id');
        $ts = $request->getParameter('ts');
        $sig = $request->getParameter('sig');
        
        if(!$posId || !$sessionId || !$ts || !$sig)
        {
            $missingParam = !$posId ? 'posId ' : '' . !$sessionId ? 'sessionId ' : '' . !$ts ? 'ts ' : '' . !$sig ? 'sig ' : '';
            
            $this->getLogger()->err('[PayU] Missing some important parameters : '. $missingParam);
            return $this->renderText('OK');
        }
        
        if ($posId != sfConfig::get('app_platnoscipl_pos_id', false))
        {
            $this->getLogger()->err('[PayU] Invalid pos_id: '.$posId);
            return $this->renderText('OK');
        }
        
        $sig2 = md5($posId . $sessionId . $ts . sfConfig::get('app_platnoscipl_key_2', false));
        
        //check if sig send by payU match with system data
        if($sig2 != $sig)
        {
            $this->getLogger()->err('[PayU] Invalid sig: '.$sig);
            return $this->renderText('OK');
        }
        
        // for tests
        if($request->getParameter('test'))
        {
            $host = $this->getController()->genUrl('payUTestResult', true);
        }
        //for normal use
        else
        {
            $host = "https://" . sfConfig::get('app_platnoscipl_server', 'www.platnosci.pl').
            sfConfig::get('app_platnoscipl_confirm_script', '/paygw/ISO/Payment/get');
        }
        
        $ts = time();
        $sig = md5($posId . $sessionId . $ts . sfConfig::get('app_platnoscipl_key_1', false));
        $parameters = "pos_id=" . $posId . "&session_id=" . $sessionId . "&ts=" . $ts . "&sig=" . $sig;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $payUResponse = curl_exec($ch);
        curl_close($ch);
        
        $xml = simplexml_load_string($payUResponse);
        if($xml->status->__toString() == "OK" && $xml->trans->status->__toString() == 99)
        {
            $xml->trans->desc->__toString();
            preg_match('/P-(.*)-P C-(.*)-C/', $xml->trans->desc->__toString(), $matches);
            
            $package = OrderPackageTable::getInstance()->find($matches[1]);
            // check package
            if(!$package)
            {
                $this->getLogger()->err('[PayU] Wrong package id: '.$matches[1]);
                return $this->renderText('OK');
            }
            // check client
            if($package->getClientId() != $matches[2])
            {
                $this->getLogger()->err('[PayU] Client does not match: '. $package->getClientId().' and '.$matches[2]);
                return $this->renderText('OK');
            }
            
            // check if package is already payed
            if($package->getPaymentStatus()->getName() == PaymentStatusTable::$paid)
            {
                $this->getLogger()->err("[PayU] Package: {$package->getId()} already paid");
                return $this->renderText('OK');
            }
            
            // check if payment is PayU
            if(!PaymentTable::getInstance()->checkPayUPayment($package->getPaymentId()))
            {
                $this->getLogger()->err("[PayU] Payment {$package->getPaymentId()} is not PayU payment");
                return $this->renderText('OK');
            }

            // payU return amount in grosze
            $amount = ($xml->trans->amount->__toString() / 100);

            $description = $this->getContext()->getI18N()->__('PayU automatic payment');
            
            // pay package - set status and generate cash desk
            $package->pay($amount, sfGuardUserTable::getInstance()->getPayuUser(), $description);
        }
        else
        {
            $this->getLogger()->err('[PayU] Status is: '.$xml->status->__toString().', payment status is: '.$xml->trans->status->__toString());
            return $this->renderText('OK');
        }
        
        return $this->renderText('OK');
    }
    
    /**
     * For test purpose
     *
     * @param sfRequest $request A request object
     */
    public function executePayUTestResult()
    {
       echo $content= "
        <response>
        <status>OK</status>
        <trans>
        <id>7</id>
        <pos_id>1</pos_id>
        <session_id>417419</session_id>
        <order_id></order_id>
        <amount>100</amount>
        <status>99</status>
        <pay_type>t</pay_type>
        <pay_gw_name>pt</pay_gw_name>
        <desc>P-4-P C-1-C</desc>
        <desc2></desc2>
        <create>2004-08-2310:39:52</create>
        <init>2004-08-3113:42:43</init>
        <sent>2004-08-3113:48:13</sent>
        <recv></recv>
        <cancel></cancel>
        <auth_fraud>0</auth_fraud>
        <ts>1094205828574</ts>
        <sig>a95dc2145079b16a3668175279c35736</sig>
        </trans>
        </response>
        ";
        return sfView::NONE;
    }
}
