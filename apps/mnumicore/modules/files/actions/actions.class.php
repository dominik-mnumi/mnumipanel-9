<?php

/**
 * files actions.
 *
 * @package    orderForms
 * @subpackage files
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class filesActions extends sfActions
{
    /**
     * Executes index action.
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    { 
        $orderObj = $this->getRoute()->getObject();
        $sessionObj = $this->getUser();

        $fileInfoArray = $this->getOrderFileInfoArray($orderObj, $sessionObj);
        
        return $this->renderText(json_encode(array('result' => $fileInfoArray)));
    }

    /**
     * Executes printLabelFile action.
     *
     * @param sfRequest $request A request object
     */
    public function executePrintLabelFile(sfWebRequest $request)
    { 
        $fullFilename = PrintlabelQueueAttributeTable::getInstance()
                ->getPrintLabelImageDir().$request->getParameter('filename').
                '.'.$request->getParameter('sf_format');

        $this->forward404If(!file_exists($fullFilename));

        $mime = '';

        if($request->getParameter('sf_format') == 'bmp')
        {
            $mime = 'image/bmp';
        }

        // prepares response
        $img = new sfImage($fullFilename, $mime, 'ImageMagick');
        $img->rotate(90);
        $response = $this->getResponse();
        $response->setContentType($img->getMIMEType());
        $response->setContent($img);

        return sfView::NONE;
    }
    
    /**
     * Method to get title from filename
     *
     * @param string $filename
     * @return string
     */
    private function getTitleFromFilename($filename)
    {
        $filename = trim($filename);
        $info = pathinfo($filename);
        if(!array_key_exists('extension', $info))
        {
            return false;
        }

        $filename = basename($filename, '.' . $info['extension']);

        if(strlen($filename) > 3 && preg_match('/[a-zA-Z]+/', $filename))
        {
            $filename = str_replace(array('.', '-', '_'), ' ', $filename);
            $filename = preg_replace('/[^a-zA-Z0-9\s]/', '', $filename);
            $filename = ucfirst($filename);

            return $filename;
        }
        else
        {
            return false;
        }
    }

    public function executeDelete(sfWebRequest $request)
    {
        $itemNo = $request->getParameter('itemNo', -1);
        $this->setLayout(false);

        if($itemNo == -1)
        {
            $this->forward404();
        }
   
        $order = $this->getRoute()->getObject();
        $order->deleteFile($itemNo);
        
        return sfView::NONE;
    }

    public function executePreview(sfWebRequest $request)
    {
        $this->setLayout(false);
        
        $order = $this->getRoute()->getObject();
        $files = $order->getFiles()->toArray();
        
        foreach($files as $key => $file)
        {
            if($file['id'] == $request->getParameter('itemNo'))
            {
                $currentFile = $file;
            }
        }
        
        if(!isset($currentFile))
        {
            throw new Exception('File for preview does not exist');
        }

        $filepath = $order->getFilePath().$currentFile['filename'];
              
        // if filepath not exists then 500 error
        if(!file_exists($filepath))
        {
            $i18n = $this->getContext()->getI18N();
            throw new Exception($i18n->__('File not found').': '.$filepath);
        }
        
        $subPreview = $request->getParameter('itemSubNo', 0);
        
        $md5_filepath = md5($filepath); 
        $cache_filename = dirname($filepath).'/'. $md5_filepath.'_'.$subPreview.'.jpg';

        if(!file_exists($cache_filename))
        {
            $newWidth = 400;
            $newHeight = 400;
            try
            {
                // check for pdf files
                if('application/pdf' == mime_content_type($filepath))
                {
                    // nr of pages
                    $nbOfPages = exec(sprintf(
                        "pdfinfo %s | grep -e '^Pages:' | awk '{print $2}'",
                        $filepath
                    ));

                    //generates image page
                    if ($subPreview >= 0 && $subPreview < $nbOfPages)
                    {
                        // guess best resolution (no more than 600px width)
                        $pageWidth = exec(sprintf(
                                "pdfinfo -f %d %s | grep -e '^Page size:' | awk '{print $3}'",
                                ($subPreview + 1),
                                $filepath
                        ));

                        $resolution = 150;

                        $finalPageWidth = $pageWidth * $resolution / 72;

                        if ($finalPageWidth > 600) {
                            $nbTrials = 0;
                            while($nbTrials < 10 && $finalPageWidth > 600) {
                                $resolution = floor($resolution / 1.5);
                                $finalPageWidth = floor($pageWidth * $resolution / 72);
                                $nbTrials++;
                            }
                        }

                        $imageFile = dirname($filepath) . '/' . $md5_filepath . '_' . $subPreview;

                        exec(sprintf(
                                'pdftocairo -cropbox -singlefile -jpeg -f %d -r %d %s %s',
                                ($subPreview + 1),
                                $resolution,
                                $filepath,
                                $imageFile
                            )
                        );

                        $img = new myImage($imageFile .'.jpg');
                        if($img->getWidth() > $img->getHeight())
                        {
                            $img->resize($newWidth, ($newWidth * $img->getHeight() / $img->getWidth()));
                        }
                        else
                        {
                            $img->resize(($newHeight * $img->getWidth() / $img->getHeight()), $newHeight);
                        }
                        
                        $img->saveAs($imageFile .'.jpg', 'image/jpeg');
                    }
                                                    
                }               
                else
                {        
                    $img = new myImage($filepath);
             
                    // check for psd files
                    if(strstr($img->getMIMEType(), 'psd') || strstr($img->getMIMEType(), 'photoshop'))
                    {
                        exec(sprintf('convert -layers merge %s %s', $filepath, $cache_filename));
                        
                        //gets image
                        $img = new myImage($cache_filename);
                    }
                    
                    //sets size
                    if($img->getWidth() > $img->getHeight())
                    {
                        $img->resize($newWidth, ($newWidth * $img->getHeight() / $img->getWidth()));
                    }
                    else
                    {
                        $img->resize(($newHeight * $img->getWidth() / $img->getHeight()), $newHeight);
                    }
                    
                    $img->saveAs($cache_filename, 'image/jpeg');
                }             
            }
            //if failed set no_preview.jpg
            catch(sfImageTransformException $e)
            {
                if(!file_exists($cache_filename))
                {
                    $filepath = sfConfig::get('sf_web_dir').'/images/no-preview.jpg';
                    $cache_filename = sfConfig::get('sf_web_dir').'/images/no-preview.jpg';
                }
            }
            
        }

        //if large preview
        if(1 == $request->getParameter('large') && 'application/pdf' != mime_content_type($filepath))
        {
            $cache_filename = $filepath;
        }
        //preview for pdf
        elseif(1 == $request->getParameter('large') && 'application/pdf' == mime_content_type($filepath))
        {
            $response = $this->getResponse();
            $response->clearHttpHeaders();
            $response->setStatusCode(200);
            $response->setContentType('application/pdf');
            $response->setHttpHeader('Pragma', 'public'); 
            $response->setHttpHeader('Expires', 0); 
            $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
            $response->setHttpHeader('Content-Disposition', "attachment; filename=".basename($filepath));
            $response->setHttpHeader('Content-Length', filesize($filepath));
            $response->setContent(file_get_contents($filepath));
            
            return sfView::NONE;
        }    
        
        //normal preview for image 
        $img = new sfImage($cache_filename);

        $response = $this->getResponse();
        $response->setContentType($img->getMIMEType());
        $response->setContent($img);
        
        return sfView::NONE;
    }

    public function executeUpload(sfWebRequest $request)
    {
        // HTTP headers for no cache etc
        
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $this->setLayout(false);
        
        $chunk = $request->getParameter('chunk');
        $chunks = $request->getParameter('chunks');
        
        $order = $this->getRoute()->getObject();
        
        $files = FileManager::uploadFiles($request->getFiles(), $order);
        
        if($chunk == ($chunks-1) || !$chunks)
        {
            foreach($files as $file)
            {
                $order->addFile($file['filename']);
            }
        }
    }

    /**
     * Downloads file.
     * 
     * @param sfWebRequest $request
     * @return type 
     */
    public function executeDownload(sfWebRequest $request)
    {     
        // gets file object (cannot use doctrine route bacause of javascript address generation)
        $fileObj = FileTable::getInstance()->find($request->getParameter('id'));
        $orderAttribute = OrderAttributeTable::getInstance()->getOrderQuantityQuery($fileObj->getOrderId());

        if(file_exists($fileObj->getFullFilePath()))
        {
            // gets mime type
            $open = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($open, $fileObj->getFullFilePath());
            $file = file_get_contents($fileObj->getFullFilePath());

            // sets response
            $response = $this->getResponse();
            $response->setContentType($mime);
            $response->setHttpHeader('Content-Disposition', "attachment; filename=".$orderAttribute->limit(1)->fetchOne()->getValue().'_'.
                    $fileObj->getOrderId().'_'.$fileObj->getFilename());
            $response->setContent($file);
        }
        else
        {
            throw new Exception('File does not exist: '.$fileObj->getFullFilePath());
        }
      
        // sets no layout
        $this->setLayout(false);
        
        return sfView::NONE;
    }

    /**
     * Executes getMainThumb action
     *
     * @param \sfWebRequest $request A request object
     * @return string
     */
    public function executeGetMainThumb(sfWebRequest $request)
    {
        /** @var Order $order */
        $order = $this->getRoute()->getObject();
        
        // gets order object
        $fileObj = $order->getMainFileObj();
    
        //if order has file
        if($fileObj)
        {
            try {
                // creates thumb
                $fullThumbFilePath = $fileObj->createThumb(90, 90);
            } catch (CommandException $e) {
                $this->logMessage($e->getMessage(), 'err');
                $fullThumbFilePath = false;
            }

            if($fullThumbFilePath === false) {
                $fullThumbFilePath = sfConfig::get('sf_web_dir').'/images/no-preview-small.jpg';
            }

            // prepares response
            $img = new sfImage($fullThumbFilePath);

            $response = $this->getResponse();
            $response->setContentType($img->getMIMEType());
            $response->setContent($img);
            
            return sfView::NONE;
        }
        
        //else check if there is wizard
        $wizard = $order->getMainWizardProject();
        
        if($wizard)
        {
            $response = $this->getResponse();
            $response->setContentType('image/jpeg; charset=binary');
            
            $wizardPath = WizardManager::getInstance()->generatePreviewUrl($wizard->getValue());
            
            $response->setContent(file_get_contents($wizardPath));
            
            return sfView::NONE;
        }

        // else there is not project
        return sfView::NONE;
    }
 
    /**
     * Executes setNotice action - set notice in file table
     *
     * @param sfRequest $request A request object
     */
    public function executeSetNotice(sfWebRequest $request)
    {
        $file = FileTable::getInstance()->find($request->getParameter('fileId'));
        if(!$file)
        {
            throw new Exception('File does not exist');
        }
        $file->setNotice($request->getParameter('text'));
        $file->save();
        
        return sfView::NONE;
    }
    
    /**
     * Executes setNotice action - set notice in file table
     *
     * @param sfRequest $request A request object
     */
    public function executeZip(sfWebRequest $request)
    {   
        $order = $this->getRoute()->getObject();
        $files = $order->getFiles()->toArray();
        
        // prepares destination
        $destination = sfConfig::get('sf_cache_dir').'/files'.$this->getUser()->getGuardUser()->getId().'.zip';
        
        // creates zip object
        $zipObj = new ZipArchive();
        $opened = $zipObj->open($destination, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
        if($opened !== true)
        {
            throw new Exception("Cannot open $destination for writing.");
        }
        
        // add each file
        foreach($files as $file)
        {       
            $zipObj->addFile($order->getFilePath().$file['filename'], 
                    $order->getId().'_'.$file['filename']);
        }
        $zipObj->close(); 
        
        $response = $this->getResponse();
        $response->setContentType('application/zip');
        $response->setContent(file_get_contents($destination));
        
        // removes zip file
        unlink($destination);
        
        header('Content-Disposition: attachment; filename="'.$order->getId().'.zip"');
        
        return sfView::NONE;
    }
    
    /**
     * Execute Render order files/wizards
     * 
     * @param sfRequest $request A request object
     */
    public function executeAttachmentList(sfWebRequest $request)
    {
        $orderObj   = $this->getRoute()->getObject();
        $sessionObj = $this->getUser();

        $fileInfoArray   = $this->getOrderFileInfoArray($orderObj, $sessionObj);
        $wizardInfoArray = $orderObj->getWizardFileInfoArray();

        $type = $request->getParameter('type', 'preview');

        return $this->renderPartial('order/'.$type.'List', 
                array('fileInfoArray' => $fileInfoArray,
                    'wizardInfoArray' => $wizardInfoArray,
                    'preview'         => $request->getParameter('preview')));
    }

    /**
     *  Gets wizards files from session
     *  @return array
     */
    private function getWizards($orderObj)
    {
        $wizard = new WizardSession($orderObj->getProduct()->getSlug(), $this->getUser());
        $wizards = $wizard->fetchAll();
        $files = array();

        foreach($wizards as $key => $wiz)
        {
            $wiz['files']['wizard'] = 1;
            $wiz['files']['id'] = $key;
            $files[] = $wiz['files'];
        }
        return $files;
    }
    
    private function getOrderFileInfoArray($orderObj, $sessionObj)
    {
        $fileColl = $orderObj->getFilesDesc();   
                 
        $return = array();
        $i = 0;
        $imageInfo = '';
        
        foreach($fileColl as $file)
        {
            // reset error flag for each file
            $error = '';

            // sets one page at start
            $nrOfPages = 1;
            
            // cleans sub images at start
            $previewNav = array();
            
            $i18n = $this->getContext()->getI18N();

            //filename
            $filename = '<a target="_blank" href="'.$this->getContext()->getRouting()->generate('filePreview', array('id' => $orderObj->getId(), 'itemNo' => $file->getId(), 'itemSubNo' => 0)).'?rand='.substr((float)(1/rand()), 0, 7).'&large=1">'.$file->getFilename().'</a>';
            
            // format
            $format = $file->getMetadataAttribute('format');
    
            // color space
            $colorspace = $file->getMetadataAttribute('colorspace');
            if($colorspace == 'RGB')
            {
                $colorspace .= '<span class="cmyk-span"> - '.$i18n->__('CMYK IS RECOMMENDED') . '</span>';
            }      
       
            $imageInfo = $filename;
            $imageInfo .= '<br/>' . $i18n->__('File format').': ' . $format;
                
            // current size in pixels
            // width
            $widthPx = $file->getMetadataAttribute('widthPx');
         
            // height
            $heightPx = $file->getMetadataAttribute('heightPx');
           
            if(file_exists($orderObj->getFilePath().$file->getFilename()))
            {
                //if pdf
                if('application/pdf' == mime_content_type($orderObj->getFilePath().$file->getFilename()))
                {
                    // print size - convert inches to milimeters
                    $imageWidthMm = round($widthPx / 72 * 25.4);
                    $imageHeightMm = round($heightPx / 72 * 25.4);

                    // nr of pages
                    $nrOfPages = $file->getMetadataAttribute('nrOfPages');                

                    $imageInfo .= '<br/>' . $imageWidthMm . 'x' . $imageHeightMm . ' mm' . '<br/>' . $i18n->__('Number of pages') . ': ' . $nrOfPages;                      
                }
                // if image
                else
                {
                    // first divide by 300 means one inch with 300dpi
                    // next multiply by 25,4 - means one inch in milimeters
                    $imageWidthMm = round($widthPx / 300 * 25.4);
                    $imageHeightMm = round($heightPx / 300 * 25.4);

                    $imageInfo .= '<br/>' . $colorspace . '<br/>' .$imageWidthMm . 'x' . $imageHeightMm . ' mm';                          
                }   
            }
            else
            {
                $error = $i18n->__('File not found');
            }
            
            if(1 < $nrOfPages)
            {
                for($j = 0; $j < $nrOfPages; $j++)
                {
                    $previewNav[] = $this->getContext()->getRouting()->generate('filePreview', 
                            array('id' => $orderObj->getId(), 'itemNo' => $file->getId(), 'itemSubNo' => $j));
                }                                         
            }
            else
            {
                $previewNav[] = $this->getContext()->getRouting()->generate('filePreview', 
                        array('id' => $orderObj->getId(), 'itemNo' => $file->getId(), 'itemSubNo' => 0));
            }
    
            $return[] = array(
                    'filename' => $file->getFilename(),
                    'preview' => $previewNav, 
                    'info' => $imageInfo,
                    'id' => $file->getId() ?: false,
                    'notice' => $file->getNotice() ?: '',
                    'error' => $error,
                    //'filepath' => $orderObj->getFilePath().$file->getFilename(),
                    'hotfolderFilepath' => strstr($file->getHotfolderFilename(), 'data/hotfolder/'),
                    'displayable' => $file->isDisplayable()
                ); 
            $i++;
        }

        return $return;
    }
}
