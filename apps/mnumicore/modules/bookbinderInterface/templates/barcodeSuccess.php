<?php $result = $sf_data->getRaw('result') ?>
<?php 
// translate messages
if(isset($result['message']) && $result['message'] == 'wrong_status')
{
    $result['message'] = array('type' => 'error', 'text' => __('Could not do this operation. Wrong order status: ') . __($object->getOrderStatusName()));
}
if(isset($result['message']) && $result['message'] == 'success')
{
    $result['message'] = array('type' => 'success', 'text' => __('Saved successfully.'));
}
  
?>
<?php echo json_encode($result) ?>