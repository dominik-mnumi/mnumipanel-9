<?php use_helper('openModal') ?>

<script type="text/javascript">
    var barcodePrefix = '<?php echo sfConfig::get('app_barcode_prefix', '^') ?>';
    var barcodeSufix = '<?php echo sfConfig::get('app_barcode_sufix', '#') ?>';
    var barcodeSize = <?php echo sfConfig::get('app_barcode_size', '8') ?>;
    var barcodeRead = '<?php echo url_for('@barcodeBookbinder') ?>';
    var activeTime = <?php echo sfConfig::get('app_barcode_active_time', '60') ?>;
    var removeAuthorization = '<?php echo url_for('@barcodeRemoveAuthorization') ?>';
</script>

<div class="printer-interface">
    <?php include_partial('global/pageLocked') ?>
    
    <div id="page-authorized">
        <div class="wizard-bg">
            <section>
            <div class="block-border">
            <div class="block-content form">
                <ol class="wizard-steps no-margin">
    				<li style="float: right;"><span id="countdown"><?php echo sfConfig::get('app_barcode_active_time', '60') ?></span> <?php echo __('sec.')?></li>
    			</ol>
            	<h1><?php echo __('Point of Print Workflow')?></h1>
            	<ul id="global-message" class="message no-margin" style="display:none;"></ul>
            	<div id="worklog-wait" class="big-blue"><?php echo __('Please read order barcode to register order as ready.') ?></div>
             </div>
             </div>
            </section>
        </div>
    </div>
</div>
<div id="progressAndWait" style="display: none;"></div>