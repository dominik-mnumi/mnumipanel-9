<?php

/**
 * notificationMessage actions.
 *
 * @package    mnumicore
 * @subpackage notificationMessage
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class notificationMessageActions extends complexTablesActions
{
 /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        parent::notificationMessageList($request);
        
        $this->tableOptions = $this->executeTable();
        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }
}
