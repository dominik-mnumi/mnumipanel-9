<?php

class mnumicoreConfiguration extends sfApplicationConfiguration
{
    public function configure()
    {
    }

    /**
     * Get default configured country (ex. 'pl')
     * @return string Lower case country name
     * @throws Exception
     */
    public static function getCountry()
    {
        if(sfConfig::has('app_default_country'))
        {
            return strtolower(sfConfig::get('app_default_country'));
        }

        $culture = sfConfig::get('sf_default_culture', false);
        if($culture)
        {
            if($pos = strpos($culture, '_'))
            {
                return strtolower(substr($culture, 0, $pos));
            }

            return strtolower($culture);
        }

        return 'pl';
    }

    /**
     * Get precision number for setting prices
     *
     * @example sfValidatorPrices class
     * @return int Precision number (default "2")
     */
    public static function getSettingPricePrecision()
    {
        return sfConfig::get('app_price_precision', 2);
    }

    /**
     * Get twitter news cache file path
     *
     * @return string The file
     */
    public static function getTwitterFile()
    {
        $cacheDir = sfConfig::get('sf_config_cache_dir');

        return $cacheDir.'/twitter.json';
    }
    /**
     * Returns currencies array with symbol and name based on app_currencies_codes configuration
     *
     * @return array
     */
    public static function getCurrencies()
    {
        $currencies = sfConfig::get('app_currencies_codes', array(sfConfig::get('app_currency', 'PLN')));
        return sfCultureInfo::getInstance(sfConfig::get('sf_default_culture'))
                ->getCurrencies($currencies, true);
    }

    /**
     * Returns array with symbol and name of currency by given currency code
     *
     * @param string $code
     * @return array
     * @throws Exception
     */
    public static function getCurrency($code)
    {
        $cs = self::getCurrencies();

        if(!isset($cs[$code])) {
            throw new Exception('Currency code:' . $code . ' does not exist');
        }

        return isset($cs[$code]) ? $cs[$code] : array();
    }
}
