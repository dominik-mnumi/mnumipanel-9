<?php

/**
 * tables actions.
 *
 * @package    mnumicore
 * @author     Adam Marchewicz
 */
class tablesActions extends myAction
{
    // search
    private $search = false;
    // list query
    private $query;
    //records per page
    private $recPerPage;
    //per page form
    private $perPageForm;
    //table fields for display
    public $displayTableFields = array();
    //grid fields for display
    public $displayGridFields = array();
    // fields that are available to sort
    public $sortableFields = array();
    // available views
    public $availableViews = array('grid', 'table');
    // filtering form
    public $filterForm;
    //default view
    public $tableView = 'table';
    // model object class
    public $modelObject;
    // custom query for special cases
    public $customQuery = false;
    // custom route for special cases
    public $customRoutingParameters = array();
    // custom route for special cases
    public $customRouting = '';
    // actions available
    public $actions = array('edit' => array(), 'delete' => array());
    // default icons 
    public $defaultIcons = array('edit' => '/images/icons/fugue/pencil.png',
        'delete' => '/images/icons/fugue/cross-circle.png',
        'default' => '/images/icons/fugue/plus-circle.png',
        'exportToXml' => '/images/icons/fugue/plus-circle.png');
    // global actions (select all etc)
    public $globalActions = true;   
    // includes customize options for display
    public $layoutOptions = array('manyActionsDefaultLabel' => 'Action for selected...');

    public $manyActionAvailable = null;

    //store settings in instance rather than method
    protected $settings = array();

    /**
     * Executes Table
     *
     * @param string $model - model name 
     */
    public function executeTable()
    {
        if(!$this->modelObject)
        {
            throw new Exception('Model must be specified.');
        }

        $request = $this->getRequest();

        //row per page
        $this->handlePerPage($request);

        $modelTable = $this->modelObject.'Table';

        if($this->customQuery)
        {
            $this->query = $this->customQuery;
        }
        else
        {
            $this->query = $modelTable::getInstance()->createQuery('c');
        }

        // search
        $this->handleSearch($request);

        // filter
        $this->handleFilter($request);
        
        // sorting
        $this->handleSorting($request);

        // table view
        $this->handleTableView($request);

        // actions
        $this->handleActions($request);

        $query = clone $this->query;

        //pager
        $pager = new sfDoctrinePager($this->modelObject, $this->recPerPage);
        $pager->setQuery($query);
        $pager->setPage($request->getParameter('page'), 1);
        $pager->init();

        //settings
        $route = $this->getContext()->getRouting()->getCurrentRouteName();

        if($this->customRouting)
        {
            $route = $this->customRouting;
        }
        
        $this->settings['route'] = $route;
        $this->settings['tableView'] = $this->tableView;
        $this->settings['search'] = $this->search;
        $this->settings['searchWord'] = $this->getUser()->getSearchWord();
        $this->settings['actions'] = $this->actions;
        $this->settings['globalActions'] = $this->globalActions;
        $this->settings['class_name'] = $this->getTableClassName();
      
        //$params = strstr($this->getContext()->getRouting()->getCurrentInternalUri(true), '?');
        $this->settings['routeParams'] = $this->customRoutingParameters;

        // type fields to view
        if($this->tableView == 'grid')
        {
            $fields = $this->displayGridFields;
        }
        else
        {
            $fields = $this->displayTableFields;
        }

        if(method_exists($this->modelObject, 'getPhotoWithPath'))
        {
            $this->settings['photo'] = true;
        }

        return array(
            'pager' =>          $pager,
            'fields' =>         $fields,
            'perPageForm' =>    $this->perPageForm,
            'settings' =>       $this->settings,
            'sortableFields' => $this->sortableFields,
            'filterForm' =>     $this->filterForm,
            'availableViews' => $this->availableViews,
            'layoutOptions' =>  $this->layoutOptions,
            'uid' =>            uniqid()
        );
    }
    
    private function getTableClassName()
    {
    	if(isset($this->className))
    	{
            return $this->className;
    	}
    	
    	return $this->modelObject;
    }

    /**
     * handlePerPage function. Handling rows per page.
     *
     * @param sfRequest $request A request object
     */
    private function handlePerPage($request)
    {
        $this->perPageForm = new PerPageForm();

        if($request->getParameter('perPage'))
        {
            $this->perPageForm->bind($request->getParameter('perPage'));
            if($this->perPageForm->isValid())
            {
                $parameters = $request->getParameter('perPage');
                $this->getUser()->setRecPerPage($parameters['options']);
            }
        }

        $this->recPerPage = $this->getUser()->getRecPerPage();
        $this->perPageForm->setDefault('options', $this->recPerPage);
    }

    /**
     * handleSorting function. Handling rows sorting.
     *
     * @param sfRequest $request A request object
     */
    private function handleSorting($request)
    {
        if($request->getParameter('sortBy') && $request->getParameter('type'))
        {
            $this->query->orderBy($request->getParameter('sortBy').' '.$request->getParameter('type'));
            $this->getUser()->setSorting(
                    array('sortBy' => $request->getParameter('sortBy'), 
                        'type' => $request->getParameter('type')));
        }
        elseif($this->getUser()->getSorting())
        {
            $sorting = $this->getUser()->getSorting();
            $this->query->orderBy($sorting['sortBy'].' '.$sorting['type']);
        }
    }

    /**
     * handleTableView function. Handling table view.
     *
     * @param sfRequest $request A request object
     */
    private function handleTableView($request)
    {
        if($request->getParameter('tableView'))
        {
            $this->getUser()->setTableView($request->getParameter('tableView'));
        }

        if($this->getUser()->getTableView())
        {
            $this->tableView = $this->getUser()->getTableView();
        }
    }

    /**
     * handleSearch function. Handling search method.
     *
     * @param sfRequest $request A request object
     */
    private function handleSearch($request)
    {
        $modelTable = $this->modelObject.'Table';

        if(method_exists($modelTable, 'search'))
        {
            $this->search = true;
            $searchQuery = $this->query;
            if($request->hasParameter('search'))
            {
                $searchQuery = $modelTable::getInstance()->search($this->query, $request->getParameter('search'));
                $this->getUser()->setSearchWord($request->getParameter('search'));
            }
            elseif($this->getUser()->getSearchWord())
            {
                $searchQuery = $modelTable::getInstance()->search($this->query, $this->getUser()->getSearchWord());
            }

            $this->query = $searchQuery;
        }
    }

    /**
     * handleFilter function. Handling filtering method.
     *
     * @param sfRequest $request A request object
     */
    private function handleFilter($request)
    {
        if($this->filterForm)
        {
            $formName = $this->filterForm->getName();
            $formValues = $request->getParameter($formName);
            if($formValues)
            {
                $this->filterForm->bind($formValues);
                if($this->filterForm->isValid())
                {   
                    $this->query = $this->filterForm->getQuery();
                }
            }
        }
    }

    public function executeDeleteMany(sfWebRequest $request)
    {
        $messageType = 'msg_error';   
        if($request->hasParameter('selected'))
        {
            $paramArr = $request->getParameter('selected');
            $currentUserObj = $this->getUser()->getGuardUser();
  
            $modelTable = $this->modelObject.'Table';
            if($modelTable::getInstance()->deleteMany($paramArr))
            {           
                // if current user was deleted
                if($this->modelObject == 'sfGuardUser' && in_array($currentUserObj->getId(), $paramArr))
                {
                    $this->redirect('sf_guard_signout');
                }
         
                $message = "Deleted successfully.";
                $messageType = 'msg_success';
            }
            else
            {
                $message = "Couldn't delete some of elements.";
            }
        }
        else
        {
            $message = "No items to delete.";   
        }

        $this->getUser()->setFlash('info_data', array(
                    'message' => $message,
                    'messageType' => $messageType));

        $params = '';
        if($this->customRoutingParameters)
        {
            $params = '?'.http_build_query($this->customRoutingParameters);
        }
        $this->redirect(stristr($this->getContext()->getRouting()->getCurrentInternalUri(true), 'deleteMany', true).$params);
    }

    /**
     * Unsets action from actions array.
     * 
     * @param string $actionName 
     */
    public function removeAction($actionName)
    {
        // if action is array (eg. array('edit' => array("manyRoute" => "someRoute"), 'delete' => array("manyRoute" => "someOtherRoute")))
        if(is_array($this->actions["$actionName"]))
        {
            unset($this->actions["$actionName"]);
        }
        
        //if action is string (eg. array('edit', 'delete'))
        foreach($this->actions as $key => $rec)
        {
            if($rec == $actionName)
            {
                unset($this->actions[$key]);
            }
        }
    }
    
    /**
     * handleActions function. Handling actions.
     *
     * @param sfRequest $request A request object
     */
    private function handleActions($request)
    {
        $route = $this->getContext()->getRouting()->getCurrentRouteName();
        
        if($this->customRouting)
        {
            $route = $this->customRouting;
        }

        // set flag to false
        $this->settings['manyActionAvailable'] = false;

        $oldActions = $this->actions;

        // cleanup actions - build list in new way
        $this->actions = array();
        
        $hasDeleteMany = false;

        foreach($oldActions as $actionName => $actionParams)
        {
            if(is_string($actionParams))
            {
                $actionName = $actionParams;
                $actionParams = array();
            }

            if(isset($actionParams['route']) && isset($actionParams['manyRoute']))
            {
                throw new Exception('You could not declare action with two parameters at once: "route" and "manyRoute" - action: ' . $actionName);
            }
            
            $actionUcName = ucfirst($actionName);
            $actionParams['name'] = $actionName;
            $actionParams['route'] = isset($actionParams['route']) ? $actionParams['route'] : str_replace('@', '', $route).$actionUcName;
            $actionParams['label'] = isset($actionParams['label']) ? $actionParams['label'] : $actionUcName;
            $actionParams['icon'] = isset($actionParams['icon']) ? $actionParams['icon'] : @$this->defaultIcons[$actionName];

            if($actionName == 'delete')
            {
                $actionParams['attributes'] = isset($actionParams['attributes']) ?
                        $actionParams['attributes'] :
                        array('class' => 'without-tip',
                    'title' => 'delete',
                    'onclick' => isset($actionParams['attributes']['onclick']) ? $actionParams['attributes']['onclick'] : 'alertModalConfirm(\''.$this->getContext()->getI18N()->__('Delete item').'\',\'<h3>'.$this->getContext()->getI18N()->__('Do you really want to delete this item?').
                    '</h3>\', 500, this.href); return false;');
                //
                $hasDeleteMany = true;
            }
            elseif(!isset($actionParams['attributes']))
            {
                $actionParams['attributes'] = array('class' => 'without-tip',
                    'title' => $actionName);
            }

            // if there are any "many route" param set flag to true
            if(isset($actionParams['manyRoute']) && $actionParams['manyRoute'] != false)
            {
                $this->settings['manyActionAvailable'] = true;  
            };

            // save changes
            $this->actions[$actionName] = $actionParams;
        }

        if($hasDeleteMany)
        {
            $actionParams = array(
                'name' => 'deleteSelected',
                'label' => 'Delete selected',
            );
            $actionParams['attributes'] = isset($actionParams['attributes']) 
                    ? $actionParams['attributes'] 
                    : array('class' => 'without-tip',
                        'title' => 'delete',
                        'onclick' => isset($actionParams['attributes']['onclick']) ? $actionParams['attributes']['onclick'] : 'alertModalConfirm(\''.$this->getContext()->getI18N()->__('Delete item').'\',\'<h3>'.$this->getContext()->getI18N()->__('Do you really want to delete this item?').
                        '</h3>\', 500, this.href); return false;'
                    );
            
            $actionParams['manyRoute'] = isset($actionParams['manyRoute']) ? $actionParams['manyRoute'] : str_replace('@', '', $route).'DeleteMany';
            
           $this->actions['deleteSelected'] = $actionParams;
           $this->settings['manyActionAvailable'] = true;
        }

        // if showCheckboxes option is not defined then set automatically 
        if(!isset($this->layoutOptions['showCheckboxes']))
        {
            // if many actions and show checkboxes are not defined
            $this->layoutOptions['showCheckboxes'] = $this->settings['manyActionAvailable'];
        }

        // if defined manually by user
        if($this->manyActionAvailable !== null)
        {
            $this->settings['manyActionAvailable'] = $this->manyActionAvailable;
        }
    }

    /**
     * @return Doctrine_Query
     */
    protected function getQuery()
    {
        return $this->query;
    }

}
