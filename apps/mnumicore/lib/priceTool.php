<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * priceTool help to calculate price for order
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>, Marek Balicki <marek.balicki@mnumi.com>
 */
abstract class priceTool
{
    public static $licenseType = array(
        'yearly-subs' => 'Yearly subscription', 
        'evaluation' => 'Evaluation');
    
    public static $liteAllowedPermissionArr = array(
        'client_viewList',
        'client_edit',
        'client_generalInformationEdit',
        'client_creditEdit',
        'client_deliveryEdit',
        'client_tagEdit',
        'order_viewList',
        'order_edit',
        'order_blockPrice',
        'order_file',
        'order_duplicate',
        'order_pricelistChange',
        'order_status',
        'order_statusNew',
        'order_statusReady',
        'order_statusDeleted',
        'panel_access',
        'system_configuration',
        'license'
    );

    /**
     * Method to validate licence key
     * 
     * @param string $key
     * @return boolean|string
     */
    public static function checkKey($key = null)
    {
        if($key == null)
        {
            $key = self::getLicenceKey();
        }
        
        if($key == null)
        {
            return false;
        }
        
        $method = 'aes256';
        $pass = '8das7das7dhad7asdgasd6asfdg6asgdas';
        $iv = '7dgasdasdgas7dga';
    
        $decrypt = openssl_decrypt($key, $method, $pass, false, $iv);
        if($decrypt === false)
        {
            return false;
        }
    
        $exploded = explode("|", $decrypt);
        if($exploded === false)
        {
            
            return false;
        }
            
        list($genDate, $information) = $exploded;
    
        $information = json_decode($information, 1);
        if($information === false)
        {
            return false;
        }

        $requiredKeys = array('name', 'ip', 'type', 'expire_at');
    
        foreach($requiredKeys as $key)
        {
            if (!array_key_exists($key, $information))
            {
                return false;
            }
        }
    
        if(!is_numeric($genDate) || $genDate < 1352816854)
        {
            return false;
        }
    
        return $information;
    }
    
    /** 
     * Path to licence key file
     *  
     * @return string
     */
    public static function getLicenceKeyPath()
    {
        return sfConfig::get('sf_data_dir').'/files/license';
    }
    
    /**
     * Defined licence key
     * 
     * @return string|FALSE
     */
    public static function getLicenceKey()
    {
        $filename = self::getLicenceKeyPath();
        
        if(!file_exists($filename))
        {
            return false;
        }
        
        $data = file_get_contents($filename);
        
        if($data === false)
        {
            return false;
        }
        
        return $data;
    }
    
    /**
     * Get allowed order workflow
     */
    public static function getOrderWorkflow()
    {
        $license = priceTool::checkKey();
        
        if($license === false)
        {
            return 'Simple Order Workflow';
        }
        
        $product = (string)$license['product'];

        if(!in_array($product, array('MnumiBasic', 'MnumiBusiness')))
        {
            return 'Default Order Workflow';
        }
        
        return 'Simple Order Workflow';
    }
    
}

