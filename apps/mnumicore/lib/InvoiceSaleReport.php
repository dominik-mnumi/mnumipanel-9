<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * InvoiceSaleReport class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

class InvoiceSaleReport
{
    const BASE_FILTER_NAME_FORMAT = 'invoice_filters';

    /** @var array  */
    private $filterParameters = array();

    /** @var array */
    private $invoices = array();

    /** @var array  */
    private $invoicesTaxesReportData = array();

    public function __construct(array $filterParameters) {

        $filter = new InvoiceSaleReportFormFilter();

        $filter->bind($filterParameters);
        $filterQuery = $filter->getQuery()
                ->andWhere('r.invoice_type_name != ?', InvoiceTypeTable::$PRELIMINARY_INVOICE)
                ->andWhere('r.invoice_status_name != ?', InvoiceStatusTable::$canceled);

        $this->invoices = $filterQuery->execute();
        $this->taxes = TaxTable::getInstance()->findAll();
    }

    /**
     * Returns array of filtered invoices
     *
     * @return array
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * Prepares report prices sum
     *
     * @return array
     */
    public function getSum()
    {

        $summedInvoicesIds = array();
        $sum = array(
            'priceNet' => 0,
            'priceTax' => 0,
            'priceGross' => 0,
        );

        foreach($this->taxes as $tax) {

            $sum['taxes'][$tax->getValue()]['priceNet'] = 0;
            $sum['taxes'][$tax->getValue()]['taxAmount'] = 0;

            foreach($this->invoices as $invoice) {
                $sum['taxes'][$tax->getValue()]['priceNet'] += $this->getInvoicePriceNetByTax($invoice, $tax->getValue());
                $sum['taxes'][$tax->getValue()]['taxAmount'] += $this->getInvoiceTaxAmountByTax($invoice, $tax->getValue());

                if(!in_array($invoice->getId(), $summedInvoicesIds)) {
                    $sum['priceNet'] += $invoice->getPriceNet();
                    $sum['priceTax'] += $invoice->getPriceTax();
                    $sum['priceGross'] += $invoice->getPriceGross();
                    $summedInvoicesIds[] = $invoice->getId();
                }
            }
        }

        return $sum;
    }

    /**
     * Returns invoice's price for given tax value
     *
     * @param Invoice $invoice
     * @param int $tax
     * @return float
     * @throws Exception
     */
    public function getInvoicePriceNetByTax(Invoice $invoice, $tax)
    {
        $taxesReport = $this->getInvoiceTaxesForReport($invoice);

        if(!isset($taxesReport[$tax])) {
            throw new Exception(sprintf('Report for tax [%s] does not exist', $tax));
        }

        return $taxesReport[$tax]['priceNet'];
    }

    /**
     * Returns invoice's tax amount for given tax value
     *
     * @param Invoice $invoice
     * @param int $tax
     * @return float
     * @throws Exception
     */
    public function getInvoiceTaxAmountByTax(Invoice $invoice, $tax)
    {
        $taxesReport = $this->getInvoiceTaxesForReport($invoice);

        if(!isset($taxesReport[$tax])) {
            throw new Exception(sprintf('Report for tax [%s] does not exist', $tax));
        }

        return $taxesReport[$tax]['taxAmount'];
    }

    /**
     * Return full report as array
     *
     * @return array
     */
    public function toArray()
    {
        foreach($this->invoices as $invoice) {
            $this->getInvoiceTaxesForReport($invoice);
        }

        return $this->invoicesTaxesReportData;
    }

    /**
     * Returns tax grouped invoice's price and tax amount as array
     *
     * @param Invoice $invoice
     * @return array
     */
    protected function getInvoiceTaxesForReport(Invoice $invoice)
    {
        $invoiceId = $invoice->getId();

        if(isset($this->invoicesTaxesReportData[$invoiceId])) {
            return $this->invoicesTaxesReportData[$invoiceId];
        }

        $items = $invoice->getInvoiceItems();
        $taxes = TaxTable::getInstance()->findAll();

        $reportData = array();

        foreach($taxes as $tax) {

            $reportData[$tax->getValue()] = array(
                'priceNet' => 0,
                'taxAmount' => 0
            );
        }

        foreach($items as $item) {
            $reportData[(int)$item->getTaxValue()]['priceNet'] += $item->getPriceNetTotalAfterDiscount();
            $reportData[(int)$item->getTaxValue()]['taxAmount'] += $item->getTaxAmount();
        }
        $this->invoicesTaxesReportData[$invoiceId] = $reportData;

        return $this->invoicesTaxesReportData[$invoiceId];
    }
}