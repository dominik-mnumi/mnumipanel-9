<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class services "from" clause.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class MnumiQLClauseFrom extends MnumiQLClause
{   
    public static $availableModelArr = array(
        'Carrier',
        'CashDesk',
        'CashReport',
        'Category',
        'Client',    
        'ClientAddress',
        'ClientAttachment',
        'ClientNotice',
        'ClientTag',
        'ClientUser',
        'Coupon',
        'CustomPrice',
        'CustomSize',
        'Fieldset',
        'FieldItem',
        'File',
        'Invoice',
        'InvoiceCost',
        'InvoiceItem',
        'InvoiceStatus',
        'InvoiceType',
        'LoyaltyPoints',
        'Order',
        'OrderAttribute',
        'OrderPackage',
        'OrderPackageStatus',
        'OrderStatus',
        'OrderTrader',
        'Payment',
        'Pricelist',
        'Printsize',
        'Product',
        'ProductField',
        'ProductFieldItem',
        'ProductFixprice',
        'ProductWizard',
        'SfGuardGroup',
        'SfGuardUser',
        'Shop',
        'Tax',
        'UserContact');
    
    protected $clause = 'FROM';
    protected $stopArr = array('WHERE', 'GROUP BY', 'ORDER BY', 'LIMIT');
    
    public function parse()
    {     
        $string = parent::parse();
        
        // if no 'from' clause
        if(empty($string))
        {
            throw new MnumiQLException('Clause "From" and "ModelName" are required');    
        }
        
        return $string;
    }
    
    public function getQuery(Doctrine_Query $doctrineQuery = null)
    {
        $doctrineQuery = parent::getQuery($doctrineQuery);       
        $string = $this->parse();

        $doctrineQuery->addFrom($string);

        return $doctrineQuery;
    }
    
    /**
     * Returns names of classes used in query.
     * 
     * @return string 
     */
    public function getModelsName()
    {
        $string = $this->parse(); 
        $string = str_replace(array('INNER JOIN', 'LEFT JOIN'), '', $string);        
        $string = $this->removeMoreThenOneWhitespaces($string);

        $modelArr = explode(' ', $string);

        return $modelArr;
    }
}