<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class services "where" clause.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class MnumiQLClauseWhere extends MnumiQLClause
{   
    // allowed operators
    public static $operatorArr = array('=', '<>', '<', '<=', '>', '>=',
        'LIKE', 'CLIKE', 'SLIKE', 'NOT',
        'IS', 'IN', 'BETWEEN');   
    
    protected $clause = 'WHERE';
    protected $stopArr = array('GROUP BY', 'ORDER BY', 'LIMIT');
    protected $pattern;
    
    public function __construct($input)
    {
        parent::__construct($input);
        
        // concatenates array operator entries in one string
        $operatorStr = implode('|', self::$operatorArr);

        $this->pattern = '/([\w\(\)_%-]+)[\s]*('.$operatorStr.')[\s]*\'?([\w\s%"_,.:-]+)\'?/';
    }
    
    public function parse()
    {     
        return parent::parse();
    }
    
    public function getQuery(Doctrine_Query $doctrineQuery = null)
    {
        $doctrineQuery = parent::getQuery($doctrineQuery);     
        
        // gets where expression
        $string = $this->parse();   

        // gets doctrine where dql
        $dqlWhereString = $this->prepareWhereDoctrineClauseString($string);

        // gets array of values
        $valArr = $this->getValueArr($string);
 
        if(!empty($dqlWhereString) && !empty($valArr))
        {   
            $doctrineQuery->addWhere($dqlWhereString, $valArr);
        }
        return $doctrineQuery;
    }

    protected function prepareWhereDoctrineClauseString($string)
    {         
        $replacement = '$1 $2 ?';

        // prepares where string (replaces values to ?)
        $dqlString = preg_replace($this->pattern, $replacement, $string);

        return $dqlString;
    }
    
    protected function getValueArr($string)
    {
        preg_match_all($this->pattern, $string, $matchArr);

        $valueArr = array();
        if(isset($matchArr[3]))
        {
            foreach($matchArr[3] as $rec)
            {
                $valueArr[] = $rec;
            }
        }

        return $valueArr;
    }

}