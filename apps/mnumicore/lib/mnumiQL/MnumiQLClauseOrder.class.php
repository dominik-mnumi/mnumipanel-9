<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class services "order" clause.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class MnumiQLClauseOrder extends MnumiQLClause
{   
    protected $clause = 'ORDER BY';
    protected $stopArr = array('LIMIT');
    
    public function parse()
    {     
        return parent::parse();
    }
    
    public function getQuery(Doctrine_Query $doctrineQuery = null)
    {
        $doctrineQuery = parent::getQuery($doctrineQuery);     
        
        // gets where expression
        $string = $this->parse();   

        if(!empty($string))
        {
            $doctrineQuery->addOrderBy($string);
        }

        return $doctrineQuery;
    }

}