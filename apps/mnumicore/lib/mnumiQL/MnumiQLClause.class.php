<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Abstract class for clause.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
abstract class MnumiQLClause
{
    // allowed query section
    public static $querySectionArr = array('SELECT', 'FROM', 'WHERE', 'GROUP BY',
        'LIMIT', 'ORDER');   
    protected $query;
    protected $clause = '';
    protected $stopArr = array();

    /**
     * Creates instance of extending classes. 
     * 
     * @param string $input
     */
    public function __construct($input)
    {  
        // removes more then one whitespaces
        $input = $this->removeMoreThenOneWhitespaces($input);
        
        $this->query = trim($input); 
    }
    
    /**
     * Returns slice of main token array with current "clause".
     * 
     * @return array slice of main array 
     */
    public function parse()
    {   
        // prepares string
        $string = '';
       
        // prepares temporary array - adds empty at the end
        $tempArr = $this->stopArr + array('' => ''); 
        foreach($tempArr as $rec)
        {   
            // if empty then gets all to end of string
            if(empty($rec))
            {
                $regEx = '/('.$this->clause.'){1}[\s](.+)/i';
            }
            else
            {
                $regEx = '/('.$this->clause.'){1}[\s](.+)([\s]('.$rec.'))/i';
            }
            
            preg_match($regEx, $this->query, $matchArr);
            
            if(!empty($matchArr[2]))
            {
                $string = $matchArr[2];
                break;
            }                 
        }

        return $string;
    }
    
    /**
     * This method adds query to existent Doctrine_Query.
     * 
     * @param Doctrine_Query $doctrineQuery
     * @return Doctrine_Query 
     */
    public function getQuery(Doctrine_Query $doctrineQuery = null)
    {
        $doctrineQuery = ($doctrineQuery) ?: Doctrine_Query::create();
        
        return $doctrineQuery;
    }
    
    /**
     * Removes more then one whitespaces.
     * 
     * @param string $string
     * @return string 
     */
    public function removeMoreThenOneWhitespaces($string)
    {
        // removes more then one whitespaces
        $string = preg_replace('/(\s\s+|\t|\n)/', ' ', $string);
        
        return $string;
    }
}