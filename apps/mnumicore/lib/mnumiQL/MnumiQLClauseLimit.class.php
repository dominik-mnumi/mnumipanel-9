<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class services "limit" clause.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class MnumiQLClauseLimit extends MnumiQLClause
{   
    protected $clause = 'LIMIT';
    protected $stopArr = array();
    
    public function parse()
    {     
        return parent::parse();
    }
    
    public function getQuery(Doctrine_Query $doctrineQuery = null)
    {
        $doctrineQuery = parent::getQuery($doctrineQuery);     
        
        // gets where expression
        $string = $this->parse();   

        if(!empty($string))
        {
            $doctrineQuery->limit($string);
        }

        return $doctrineQuery;
    }

}