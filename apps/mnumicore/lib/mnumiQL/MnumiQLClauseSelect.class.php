<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class services "select" clause.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class MnumiQLClauseSelect extends MnumiQLClause
{   
    protected $clause = 'SELECT';
    protected $stopArr = array('FROM');
    protected $pattern;
    protected $columnToDisplayArr = array();


    public function __construct($input, $columnToDisplayArr)
    {
        $this->columnToDisplayArr = $columnToDisplayArr;
        
        parent::__construct($input);
    }
    
    public function parse()
    {     
        return parent::parse();
    }
    
    public function getQuery(Doctrine_Query $doctrineQuery = null)
    {
        $doctrineQuery = parent::getQuery($doctrineQuery);     

        // gets SELECT expression
        $string = $this->parse(); 
        
        $customColumnArr = array();
        
        $parsedCustomColumnArr = array_map('trim', explode(",", $string));
        if($parsedCustomColumnArr[0] != '')
        {
            $customColumnArr = $parsedCustomColumnArr;
        }
        
        // prepares column to display array
        $columnToDisplayArr = array();
        
        // foreach allowed phrase
        foreach($customColumnArr as $rec)
        {
            if(array_key_exists($rec, $this->columnToDisplayArr))
            {
                $columnToDisplayArr[$rec] = $this->columnToDisplayArr[$rec];
            }
            else
            {
                $columnToDisplayArr[$rec] = $rec;
            }
        }

        // after conditions checks if columns are defined properly
        if(!empty($columnToDisplayArr))
        {
            $this->columnToDisplayArr = $columnToDisplayArr;
        }

        // adds select clause
        foreach($this->columnToDisplayArr as $key => $rec)
        { 
            $doctrineQuery->addSelect($key.' as '.$key);
        }

        return $doctrineQuery;
    }
    
    /**
     * Returns prepares array with columns to select.
     * 
     * @return array
     */
    public function getColumns()
    {
        return $this->columnToDisplayArr;
    }
}