<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class MnumiQLException extends Exception class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class MnumiQLException extends Exception
{
    public function __construct($message, $i18NObj = null, $argArr = array())
    {
        if($i18NObj && !empty($argArr))
        {
            $message = $i18NObj->__($message, $argArr);
        }
        
        parent::__construct($message);
    }
}