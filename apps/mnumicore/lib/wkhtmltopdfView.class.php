<?php

class wkhtmltopdfView extends sfPHPView
{
    /**
     * Renders the presentation.
     *
     * @return string A string representing the rendered presentation
     */
    public function render()
    {
        $htmlTempFile = tempnam(sys_get_temp_dir(), "core3html") . '.html';
        $pdfTempFile = tempnam(sys_get_temp_dir(), "core3pdf") . '.pdf';

        $content = parent::render();

        file_put_contents($htmlTempFile, $content);

        $exec = sprintf('/usr/bin/xvfb-run /usr/bin/wkhtmltopdf -q %s %s', $htmlTempFile, $pdfTempFile );

//        die($exec);
        shell_exec($exec);

        $pdfContent = file_get_contents($pdfTempFile);

        unset($htmlTempFile, $pdfTempFile);

        return $pdfContent;
    }
}