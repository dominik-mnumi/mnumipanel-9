<?php

/*
 * This file is part of the MnumiCore3 package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * jsonForm prepare form messages for ajax
 *
 * @package    mnumi
 * @author     Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
class jsonForm {

    /**
     * @var array
     */
    protected $messages = array();

    /**
     * @var sfForm
     */
    protected $form;

    /**
     * @var sfI18N
     */
    protected $i18n;

    /**
     * @param sfForm $form
     * @param sfI18N $i18n
     */
    public function __construct(sfForm $form, sfI18N $i18n)
    {
        $this->form = $form;
        $this->i18n = $i18n;
    }

    /**
     * Get all form messages
     *
     * @return string
     */
    public function getMessages()
    {
        foreach ($this->form->getErrorSchema()->getErrors() as $key => $error) {
            $this->messages['message'][] = $this->i18n->__($error->getMessage());
        }

        return json_encode(array('result' => $this->messages));
    }

    /**
     * Add success message
     *
     * @param string $message
     * @return jsonForm $this
     */
    public function setSuccess($message)
    {
        $this->messages['success'] = 1;
        $this->messages['message'][] = $this->i18n->__($message);

        return $this;
    }

    /**
     * Add error message
     *
     * @param string $message
     * @return jsonForm $this
     */
    public function setError($message)
    {
        $this->messages['message'][] = $this->i18n->__($message);

        return $this;
    }
} 