<?php
/**
 * Created by IntelliJ IDEA.
 * User: dominik
 * Date: 03.03.15
 * Time: 12:48
 */

class MnumiI18n {

    /**
     * Get culture from request, session or default
     * 
     * @return mixed
     */
    public static function getCulture()
    {
        $request = sfContext::getInstance()->getRequest();
        $culture = sfContext::getInstance()->getUser()->getAttribute('default_culture', null);
        if($culture === null)
        {
            $culture = $request->getParameter('lang', sfConfig::get('sf_default_culture'));
        }
        return $culture;
    }

    /**
     * Set culture to session
     *
     * @param $culture
     */
    public static function setCulture($culture)
    {
        sfContext::getInstance()->getUser()->setAttribute('default_culture', $culture);
    }
} 