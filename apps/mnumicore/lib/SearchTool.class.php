<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Search handling class
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class SearchTool
{
    // phrase for searach
    private $phrase;
    
    // array of s earch results
    private $results = array();
    
    // results limit
    private $limit = null;
    
    /**
     * Perform search and return results
     * 
     * @return array
     */
    public function search($phrase)
    {
        //clear results in case of multiple search on this same object
        $this->results = array();
        
        $this->phrase = $phrase;
        
        if(is_numeric($this->phrase))
        {
            $this->searchInOrder();
        }
        else
        {
            $this->searchInUser();
        }
        
        $this->searchInClient();
        $this->searchInInvoice();
        
        return $this->results;
    }
    
    /**
     * Returns searched object if exists.
     * 
     * @param string $phrase
     * @return mixed object|boolean
     */
    public function searchStrict($phrase)
    {
        $recObj = false;
        
        // if numeric checks in order
        if(is_numeric($phrase))
        {
            $recObj = OrderTable::getInstance()->searchOrder($this->phrase)
                    ->getFirst();
        }
        
        if($recObj)
        {
            return $recObj;
        }
        
        $recObj = sfGuardUserTable::getInstance()->searchUser($phrase)
                ->getFirst();
        
        if($recObj)
        {
            return $recObj;
        }
        
        $recObj = ClientTable::getInstance()->searchClient($phrase)
                ->getFirst();
        if($recObj)
        {
            return $recObj;
        }

        $recObj = InvoiceTable::getInstance()->searchInvoice($phrase)
            ->getFirst();
        if($recObj)
        {
            return $recObj;
        }

        return false;
    }
    
    /**
     * Search in Client model
     */
    private function searchInClient()
    {
        // check if limit is set and was reached
        if($this->limit > 0 && $this->limit <= count($this->results))
        {
            return;
        }
        
        $clients = ClientTable::getInstance()->searchClient($this->phrase);
        
        foreach($clients as $client)
        {
            $this->addResult($client);
        }
    }
    
    /**
     * Search in Order model
     */
    private function searchInOrder()
    {
        // check if limit is set and was reached
        if($this->limit > 0 && $this->limit <= count($this->results))
        {
            return;
        }
        $orders = OrderTable::getInstance()->searchOrder($this->phrase);
        foreach($orders as $order)
        {
            $this->addResult($order);
        }
    }
    
    /**
     * Search in Invoice model
     */
    private function searchInInvoice()
    {
        // check if limit is set and was reached
        if($this->limit > 0 && $this->limit <= count($this->results))
        {
            return;
        }
        
        //we have to get all invoice and filter them by formatted number
        $invoices = InvoiceTable::getInstance()->searchInvoice($this->phrase);
        foreach($invoices as $invoice)
        {
            $this->addResult($invoice);
        }
    }
    
    /**
     * Search in sfGuardUser model
     */
    private function searchInUser()
    {
        // check if limit is set and was reached
        if($this->limit > 0 && $this->limit <= count($this->results))
        {
            return;
        }
        
        $users = sfGuardUserTable::getInstance()->searchUser($this->phrase);
        foreach($users as $user)
        {
            $this->addResult($user);
            
            // adds user clients
            foreach($user->getClientUsers() as $clientUserObj)
            {
                $this->addResult($clientUserObj->getClient());
            }
        }
    }
    
    /**
     * Set limit for number of results
     * 
     * @param int $limit
     */
    public function setLimit($limit)
    {
        if(!is_numeric($limit) || $limit < 0)
        {
            throw new Exception('Limit must be valid number greater than 0');
        }
        $this->limit = $limit;
    }
    
    /**
     * Adds result to result array
     * 
     * @param Doctrine_record $object
     */
    private function addResult($object)
    {
        // check if limit is set and was reached
        if($this->limit > 0 && $this->limit <= count($this->results))
        {
            return;
        }

        // gets id
        $id = $object->getId();

        // gets name
        $name = $object->__toString();
        
        // gets class
        $type = get_class($object);
        
        // for distinct records
        foreach($this->results as $rec)
        {
            if($rec['id'] == $id
                    && $rec['type'] == $type)
            {
                return;
            }
        }
        
        $this->results[] = array(
            'id'   => $id,
            'name' => $name,
            'type' => $type, 
            'url' => $object->getEditUrl());
    }
    
    
}