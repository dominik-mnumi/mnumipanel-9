<?php

/*
 * This file is part of the MnumiCore package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * BarcodePrinter class for handling barcode printers(typographers) actions
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class BarcodePrinter extends BarcodeDecoder
{
    /**
     * Save order worklog and change order status
     *
     * @param $userId user id (sfGuardUser)
     *
     */
    public function orderWorklog($userId)
    {
        $order = $this->getObject();
        $i18n = sfContext::getInstance()->getI18n();
        
        // if status is new or ready we cannot perform this action
        if($order->getOrderStatusName() == 'new' || $order->getOrderStatusName() == 'ready')
        {
            return array('message' => 'wrong_status');
        }
        
        $fields = $order->getCalculationFields();
        
        $report = $order->calculate(null);
        if(isset($report['measureOfUnit']) && $report['measureOfUnit'])
        {
            if($report['measureOfUnit'] == 'sheet')
            {
                $value = (int)$report['factor'];
            }
            else
            {
                $value = (int)$report['squareMetre'];
            }
        }
        else
        {
            $value = 1;
        }
        
        $notice = $i18n->__($report['priceItems']['MATERIAL']['fieldLabel']);
        
        $orderWorklog = $order->saveWorklogForOrder($userId, "PRINT", $value, $notice, 'bindery');
        
        //prepare previous worklogs array
        $previousWorklogs = array();
        foreach($order->getSortedOrderWorklogs() as $worklog)
        {
            $previousWorklogs[] = array(
                'username'  => $worklog->getSfGuardUser()->__toString(),
                'value'     => $worklog->getValue(),
                'notice'    => $worklog->getNotice(),
                'createdAt' => $worklog->getCreatedAt()
               
            );
        }
        
        return array('worklog' => array(
            'id'               => $orderWorklog->getId(),
            'material'         => $this->getObject()->getAttribute('MATERIAL')->getValue(),
            'value'            => $value,
            'previousWorklogs' => $previousWorklogs
        ));
    }
    
}
