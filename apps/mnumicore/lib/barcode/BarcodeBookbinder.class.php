<?php

/*
 * This file is part of the MnumiCore package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * BarcodeBookbinder class for handling barcode bookbinders actions
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class BarcodeBookbinder extends BarcodeDecoder
{
    /**
     * Set order status to ready
     *
     * @return array
     */
    public function setAsReady()
    {
        $order = $this->getObject();
        
        // if status is new or ready we cannot perform this action
        if($order->getOrderStatusName() == OrderStatusTable::$ready)
        {
            return array('message' => 'wrong_status');
        }
        
        $order->setOrderStatusName(OrderStatusTable::$ready);
        $order->save();
        
        return array('message' => 'success');
    }
    
}
