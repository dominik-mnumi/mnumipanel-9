<?php

/*
* This file is part of the MnumiCore package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
* BarcodePackaging class for handling barcode packaging actions
*
* @author Adam Marchewicz <adam.marchewicz@itme.eu>
*/
class BarcodePackaging extends BarcodeDecoder
{
    /**
    * Set order status to ready
    *
    * @return array
    */
    public function orderToShelf()
    {
        $order = $this->getObject();
        
        $types = array('info_green' => 'success', 'info_red' => 'error');

        $result = $order->orderToShelf();

        return array('message' => 
            array('type' => isset($types[$result['message']['messageType']]) ? $types[$result['message']['messageType']] : 'error', 
                  'text' => !empty($result['message']['message']) ? $result['message']['message'] : 'Invalid parameter', 
                  'param' => !empty($result['message']['messageParam'])? $result['message']['messageParam']: null),
                  'allOrders' => $order->getOrderPackage()->getOrders()->toArray(),
                  'orderPackageId' => $order->getOrderPackage()->getId(),
                  'allPrintlabels' => PrintlabelQueueTable::getInstance()->loadQueue('PENDING')->toArray(),
                  'readyEvent' => (isset($result['readyEvent']) && $result['readyEvent']) ? true : false );
    }
    
}