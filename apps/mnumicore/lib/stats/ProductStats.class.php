<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Employee class extends Stats class.
 *
 * @package    mnumicore
 * @subpackage stats
 * @author     Marek Balicki
 */
class ProductStats extends Stats
{  
    /**
     * Sets maximal item count and step. 
     */
    public function setMaxItemScaleAndStep()
    {    
        $this->maxItemCount = ProductTable::getInstance()
                ->getMaximalOrdersIncome($this->year, $this->month);

        parent::setMaxItemScaleAndStep();
    }
}