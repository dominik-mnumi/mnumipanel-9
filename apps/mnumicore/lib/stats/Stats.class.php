<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Abstract class for stats.
 *
 * @package    mnumicore
 * @subpackage stats
 * @author     Marek Balicki
 */
abstract class Stats implements StatsInterface
{   
    // year
    protected $year;
    
    // month
    protected $month;
    
    // maximal item count for current stats (e.g. maximal orders from users)
    protected $maxItemCount;
    
    // maximal item scale for current stats (scale)
    protected $maxItemScale;
    
    // size of one step
    protected $stepSize;
    
    // full size scale array
    protected static $measureArr = array(1, 15, 20, 25, 50, 100, 150, 200, 250, 300, 
        500, 1000, 1500, 2000, 5000, 10000, 25000, 50000, 10000, 200000, 500000, 
        1000000, 1500000, 2000000, 5000000, 10000000);
    
    // one step size array
    protected static $stepSizeArr = array(1, 2, 5, 10, 15, 20, 25, 50, 100, 150, 200, 250, 
        300, 500, 1000, 2000, 5000, 10000, 25000, 50000, 10000, 200000, 500000, 
        1000000, 1500000, 2000000, 5000000, 10000000);

    public function __construct($year = null, $month = null)
    {    
        $this->year = $year;
        $this->month = $month;
        
        // refers to extended method
        $this->setMaxItemScaleAndStep();      
    }
    
    /**
     * Sets maximal item count based on measure array. This is some kind of 
     * automatic scale adjustment.
     */
    public function setMaxItemScaleAndStep()
    {
        foreach(self::$measureArr as $rec)
        {
            if($this->maxItemCount < $rec)
            {
                $this->maxItemScale = $rec;
                break;
            }
        }
  
        foreach(self::$stepSizeArr as $rec)
        {
            if($this->maxItemScale % $rec == 0 
                    && $this->maxItemScale / $rec <= 30)
            {
                $this->stepSize = $rec;
                break;
            }
        }   
    }
    
    /**
     * Returns maximal items for stats (needed for measure).
     * 
     * @return integer 
     */
    public function getMaxItemScale()
    {
        return $this->maxItemScale;
    }
    
    /**
     * Returns array with measure range params.
     * array('percentageValue' => 'measureNumber')
     * 
     * @return array 
     */
    public function getMeasureRange()
    {
        $stepSize = $this->getStepSize();
        $percentage = $stepSize / $this->getMaxItemScale() * 100;
        $stepCount = $this->getStepCount();
        
        $measureDisplayArr = array();
        $start = 0;
        for($i = 0; $i < $stepCount + 1; $i++)
        {
            if($i == 0 || $i == $stepCount)
            {
                $start = '';
            }     
            
            $measureDisplayArr[] = array('percentage' => round($percentage * $i, 2),
                                         'scale' => $start);
            $start += $stepSize;
        }

        return $measureDisplayArr;
    }
    
    /**
     * Returns step size.
     * 
     * @return integer 
     */
    public function getStepSize()
    {
        return $this->stepSize;
    }

    /**
     * Returns number of steps for measure.
     * 
     * @return integer 
     */
    public function getStepCount()
    {
        return $this->getMaxItemScale() / $this->getStepSize();
    }
    
    /**
     * Returns percentage of one measure number.
     * 
     * @return float 
     */
    public function getOneNumberPercentage()
    {
        return 100 / $this->getMaxItemScale();
    }
    
    /**
     * Returns var needed for css to display stats (left attribute for stripe).
     * 
     * @param float $count
     * @return float $count  
     */
    public function getStatsCssLeftPercentage($count = 0)
    {
        return round($this->getOneNumberPercentage() * $count, 2);
    }
    
    /**
     * Returns var needed for css to display stats (right attribute for stripe).
     * 
     * @param float $count
     * @return float $count  
     */
    public function getStatsCssRightPercentage($count)
    {
        return round(100 - $this->getOneNumberPercentage() * $count, 2);
    }
}