<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Sale class extends Stats class.
 *
 * @package    mnumicore
 * @subpackage stats
 * @author     Marek Balicki
 */
class SaleStats extends Stats
{  
    // array with years
    protected $yearArr;
    
    public function __construct($yearArr = array(), $month = null, $shop = false)
    {    
        $this->yearArr = $yearArr;
        $this->shop = $shop;
  
        parent::__construct(null, $month);
    }
    
    /**
     * Sets maximal item count and step.
     */
    public function setMaxItemScaleAndStep()
    {    
        $this->maxItemCount = OrderTable::getInstance()
                ->getMaximalAllOrdersIncome($this->yearArr, $this->month, $this->shop);

        parent::setMaxItemScaleAndStep();
    }
}