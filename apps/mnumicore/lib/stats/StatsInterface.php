<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Stats interface.
 *
 * @package    mnumicore
 * @subpackage stats
 * @author     Marek Balicki
 */

interface StatsInterface
{    
    /**
     * Sets maximal item count based on measure array. This is some kind of 
     * automatic scale adjustment.
     */
    public function setMaxItemScaleAndStep();
    
    /**
     * Returns maximal items for stats (needed for measure).
     */
    public function getMaxItemScale();
    
    /**
     * Returns array with measure range params.
     * array('percentageValue' => 'measureNumber')
     */
    public function getMeasureRange();
    
    /**
     * Returns step size.
     */
    public function getStepSize();
    
    /**
     * Returns number of steps for measure.
     */
    public function getStepCount();
    
    /**
     * Returns percentage of one measure number.
     */
    public function getOneNumberPercentage();
    
    /**
     * Returns var needed for css to display stats (left attribute for stripe). 
     */
    public function getStatsCssLeftPercentage($count = 0);
    
    /**
     * Returns var needed for css to display stats (right attribute for stripe).
     */
    public function getStatsCssRightPercentage($count);
}