<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Employee class extends Stats class.
 *
 * @package    mnumicore
 * @subpackage stats
 * @author     Marek Balicki
 */
class EmployeeStats extends Stats
{
    // permission groups of users
    protected $groupPermissionArr;

    protected $orderType;

    public function __construct($groupPermissionArr = array(), $year = null, $month = null, $orderType = null)
    {
        $this->groupPermissionArr = $groupPermissionArr;
        $this->orderType = $orderType;

        parent::__construct($year, $month);
    }

    /**
     * Sets maximal item count and step.
     */
    public function setMaxItemScaleAndStep()
    {
        $coll = sfGuardUserTable::getInstance()
            ->getUsersByGroupPermission($this->groupPermissionArr);
        
        $orderCount = array();
        foreach ($coll as $rec)
        {
            $orderCount[] = $rec->getStatsOrdersCount($this->year, $this->month, $this->orderType);
        }

        $this->maxItemCount = max($orderCount);
        parent::setMaxItemScaleAndStep();
    }
}