<?php

/**
 * Email decorator
 *
 * Class EmailDecorator
 */
class EmailDecorator {

    /**
     * @var sfMailer
     */
    protected $mailer;

    /**
     * @var bool|Swift_Attachment
     */
    protected $attahment = false;

    /**
     * @var array
     */
    protected $sender;

    /**
     * @param sfMailer $mailer
     */
    public function __construct(sfMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $body
     * @param string $sender
     * @param bool $attachment
     * @return bool
     * @throws Exception
     */
    public function sendEmail($to, $subject, $body, $contentType = "text/html")
    {
        if (count($this->getSender()) == 0) {
            throw new Exception('Missing e-mail sender address');
        }

        try {
            $message = $this->mailer->compose(
                $this->getSender(),
                $to,
                $subject
            );

            $message->setBody($body, $contentType);

            $attachment = $this->getAttahment();
            if($attachment !== false) {
                $message->attach($attachment);
            }

            $this->mailer->send($message);

            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @return bool|Swift_Attachment
     */
    public function getAttahment()
    {
        return $this->attahment;
    }

    /**
     * @param Swift_Attachment $attahment
     */
    public function setAttahment($attahment)
    {
        $this->attahment = $attahment;

        return $this;
    }

    /**
     * @return array
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     * @return reqEmail $this
     */
    public function setSender($sender)
    {
        $this->sender = NotificationEmail::getSender($sender);

        return $this;
    }


} 