<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Menu interface.
 *
 * @package    mnumicore
 * @author     Marek Balicki
 */
interface MenuInterface
{
    /**
     * Returns menu route. 
     */
    public function getRoute();
    
    /**
     * Returns menu class. 
     */
    public function getClass();
    
    /**
     * Returns menu title. 
     */
    public function getTitle();
    
    /**
     * Returns true if menu has submenu. 
     */
    public function hasSubmenu();
    
    /**
     * Returns submenu collection. 
     */
    public function getSubmenu();

    /**
     * Returns level.
     */
    public function getLevel();
    
    /**
     * Returns manually defined actions. 
     */
    public function getActions();
    
    /**
     * Returns true if menu is current.
     */
    public function isCurrent();
    
    /**
     * Returns true if user has credential to menu, otherwise false.
     */
    public function hasCredential();

    /**
     * Returns true if user has at least one submenu with credential..
     */
    public function hasSubmenuWithCredential();
}