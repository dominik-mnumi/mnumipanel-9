<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Navigator interface.
 *
 * @package    mnumicore
 * @author     Marek Balicki
 */
interface NavigatorInterface
{
    /**
     * Returns menu collection objects. 
     */
    public function getMenu();
    
    /**
     * Checks if menu object is current.
     */
    public function isCurrent(Menu $menuObj);
    
    /**
     * Checks if menu object has cdredential. 
     */
    public function hasCredential(Menu $menuObj);
}