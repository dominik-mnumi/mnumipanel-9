<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Navigator class manages routing, menus and permissions.
 *
 * @package    mnumicore
 * @author     Marek Balicki
 */
class Navigator implements NavigatorInterface
{
    protected $config;
    protected $sfUser;
    protected $module;
    protected $action;
    protected $routes;
    protected $manuallyDefinedActionArr;
    protected $credentialGetterObj;
    
    /**
     * Creates instance of navigator object.
     * 
     * @param myUser $sfUser
     * @param string $module
     * @param string $action
     * @param array $routes
     * @param sfActionCredentialsGetter $credentialGetterObj
     * @param sfController $controller
     * @throws Exception 
     */
    public function __construct(myUser $sfUser, $module, $action, 
            array $routes, sfActionCredentialsGetter $credentialGetterObj,
            sfController $controller)
    {
        if(!$controller->actionExists($module, $action))
        {
            throw new Exception('Current action name does not exist.');
        }
        
        $this->config = sfConfig::get('mod_dashboard_menu');
        $this->sfUser = $sfUser;
        $this->module = $module;
        $this->action = $action;
        $this->routes = $routes;
        $this->credentialGetterObj = $credentialGetterObj;
        
        // foreach menu
        foreach($this->getMenu() as $rec)
        {
            $this->getManuallyDefinedActions($rec);
        }
    }
   
    /**
     * Returns collection of menu objects.
     * 
     * @return \Menu 
     */
    public function getMenu()
    {     
        $menuColl = array();
        foreach($this->config as $rec)
        {
            $menuColl[] = new Menu($rec, $this);
        }    
        
        return $menuColl;
    }

    /**
     * Checks if menu is current.
     *  
     * @param Menu $menuObj
     * @return boolean 
     */
    public function isCurrent(Menu $menuObj)
    {
        if($this->isCurrentMenu($menuObj))
        {
            return TRUE;
        }

        if($menuObj->hasSubmenu())
        {
            foreach($menuObj->getSubmenu() as $rec)
            {
                if($this->isCurrentMenu($rec))
                {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }
    
    /**
     * Returns true if user has credential to menu, otherwise false.
     * 
     * @param Menu $menuObj
     * @return boolean 
     */
    public function hasCredential(Menu $menuObj)
    {
        // gets route parameters
        $paramArr = $this->getRouteParams($menuObj);

        return $this->credentialGetterObj->isUserAllowedToExecuteAction($this->sfUser,
                $paramArr['module'], $paramArr['action']);

    }
    
    /**
     * Helper method for isCurrent method.
     * 
     * @param Menu $menuObj
     * @return boolean 
     */
    protected function isCurrentMenu(Menu $menuObj)
    {
        // if route is # then module and action cannot be recognized
        if($menuObj->getRoute() == '#')
        {
            return FALSE;
        }

            // gets module name and action name
        $paramArr = $this->getRouteParams($menuObj);

        // gets manually defined route
        $manuallyDefinedRoute = $this
        ->getManuallyDefinedRouteByModuleAndAction($this->module, $this->action); 

        // 1. if action is manually defined (not null)
        if($manuallyDefinedRoute)
        {
            // 2. if action is manually defined by current route
            if($manuallyDefinedRoute == $menuObj->getRoute())
            {
                return TRUE;
            }
            // 3. if action is manually defined by other route
            else
            {
                return FALSE;
            }
        }

        // 4. if menu does not have defined actions
        // (means that menu with "~" in actions is default)
        //  and modules are equal 
        if(!$this->getManuallyDefinedRouteByModuleAndAction($paramArr['module'], $paramArr['action'])
                && $paramArr['module'] == $this->module)
        {
            return TRUE;
        }
        
        return FALSE;
    }

    /**
     * Returns route params (module name and action name).
     * 
     * @param Menu $menuObj
     * @throws Exception
     * @return array 
     */
    protected function getRouteParams(Menu $menuObj)
    {
        $route = substr($menuObj->getRoute(), 1);
        if(!isset($this->routes[$route]))
        {
            throw new Exception('Route defined in module.yml 
                is not defined in routing.yml.');
        }

        // gets module name and action name
        return $this->routes[$route]->getDefaults();
    }
    
    /**
     * Returns all manually defined actions (out of schema).
     *
     * @param Menu $menuObj
     * @return array 
     */
    protected function getManuallyDefinedActions(Menu $menuObj)
    {
        // if # route then continue
        if($menuObj->getRoute() != '#')
        {
            // adds defined orders to 
            // array['route'] = array('moduleName' => ..., 'actionName' => ...);
            $actionArr = $menuObj->getActions();
            if(!empty($actionArr))
            {
                $this->manuallyDefinedActionArr[$menuObj->getRoute()] = $actionArr;
            }
        }

        foreach($menuObj->getSubmenu() as $rec)
        {
            $this->getManuallyDefinedActions($rec);               
        }  

        return $this->manuallyDefinedActionArr;
    }
    
    /**
     * Returns manually defined route or null if does not exist.
     * 
     * @param string $module
     * @param string $action
     * @return mixed 
     */
    protected function getManuallyDefinedRouteByModuleAndAction($module, $action)
    {
        foreach($this->manuallyDefinedActionArr as $key => $rec)
        {
            foreach($rec as $rec2)
            {
                if($rec2['module'] == $module
                      && $rec2['action'] == $action)
                {
                    return $key;
                }   
            }
        }
        
        return NULL;
    }
  
}