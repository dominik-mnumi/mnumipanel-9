<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductCalculationProductData class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ProductCalculationProductData 
{
    /**
     * @var sfCache
     */
    private $cache;
    /**
     * @var Product
     */
    private $product;


    public function __construct(Product $product)
    {
        $this->cache = new sfFileCache(array(
            'cache_dir' => sfConfig::get('sf_cache_dir') . '/productData'
        ));
        $this->product = $product;
    }

    public function getCalculationProductDataObject()
    {
        // try to load cache
        $cacheProductData = new CacheProductCalculationProductData($this->product, $this->cache);

        $data = $cacheProductData->getCalculationProductDataObject();

        if (!$data) {
            $databaseProductData = new DoctrineProductCalculationProductData($this->product);

            $data = $databaseProductData->getCalculationProductDataObject();
            $cacheProductData->cacheProductData($data);
        }

        return $data;
    }

    public function clearCache()
    {
        $cacheProductData = new CacheProductCalculationProductData($this->product, $this->cache);
        $cacheProductData->clearCache();
    }
}