<?php

class myUser extends myUserTerminalAuthentication
{
    public static $attributeStatsNamespace = 'stats';
 
    private $moduleName;
    private $actionName;
    
    /**
     * Extends - Signs in the user on the application.
     *
     * @param sfGuardUser $user The sfGuardUser id
     * @param boolean $remember Whether or not to remember the user
     * @param Doctrine_Connection $con A Doctrine_Connection object
     */
    public function signIn($user, $remember = false, $con = null)
    {
        // if current user has any office permission group
        if(!$user->getGroups()->count())
        {
            return false;
        }
        
        $this->getAttributeHolder()->clear();
        parent::signIn($user, $remember, $con);
        
        return true;
    }
    
    /**
     * Returns session id. If session was created before, then update it with 
     * user id.
     * 
     * @param sfGuardUser $userObj
     * @param string $sessionHandle
     * @return string 
     */
    public function signInRest($sessionHandle, $userObj)
    {
        /** @var RestSession $restSessionObj */
        $restSessionObj = RestSessionTable::getInstance()->find($sessionHandle);

        // if rest session does exist
        if(!$restSessionObj)
        {
            $restSessionObj = RestSessionTable::getInstance()->registerRestLogin($sessionHandle, $userObj);
        }

        $restSessionObj->updateRestData($userObj);

        // sets user id
        $restSessionObj->setSfGuardUser($userObj);
        $restSessionObj->save();
        
        $sessionHandle = $restSessionObj->getId();
        
        return $sessionHandle;
    }

    /**
     * Extends signs out the user.
     *
     */
    public function signOut($sessionHandle = null)
    {
        $this->getAttributeHolder()->clear();
        $this->setCulture(null);
        
        if($sessionHandle)
        {
            $this->signOutFromRest($sessionHandle);
        }
        
        parent::signOut();
    }
    
    /**
     * Extends signs out the user from rest.
     * 
     * @return boolean
     */
    public function signOutFromRest($sessionHandle)
    {
        $restSessionObj = RestSessionTable::getInstance()->find($sessionHandle);
        if($restSessionObj)
        {
            $restSessionObj->delete();
            return true;
        }
        
        return false;
    }

    /**
     * Sets page
     *
     * @param integer $page
     * @return boolean
     */
    public function setPage($page, $ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->setAttribute('page', $page, $this->moduleName . '.' . $ns);
    }

    /**
     * Returns current page
     *
     * @return integer
     */
    public function getPage($ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->getAttribute('page', 1, $this->moduleName . '.' . $ns);
    }

    /**
     * Sets records per page
     *
     * @return boolean
     */
    public function setRecPerPage($nr, $ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->setAttribute('recPerPage', $nr, $this->moduleName . '.' . $ns);
    }

    /**
     * Returns number of records per page
     *
     * @return integer
     */
    public function getRecPerPage($ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->getAttribute('recPerPage', 10, $this->moduleName . '.' . $ns);
    }

    /**
     * Sets filters
     *
     * @param array $filters
     * @return boolean
     */
    public function setFilters(array $filterArray, $ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->setAttribute('filters', $filterArray, $this->moduleName . '.' . $ns);
    }

    /**
     * Gets filters
     *
     * @return boolean
     */
    public function getFilters($ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->getAttribute('filters', array(null, null), $this->moduleName . '.' . $ns);
    }

    /**
     * Sets sort
     *
     * @param array $sort
     * @return boolean
     */
    public function setSort(array $sortArray, $ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        if(null !== $sortArray[0] && null === $sortArray[1])
        {
            $sortArray[1] = 'asc';
        }

        return $this->setAttribute('sort', $sortArray, $this->moduleName . '.' . $ns);
    }

    /**
     * Returns sort array
     *
     * @return array
     */
    public function getSort($ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        $sort = $this->getAttribute('sort', array(null, null), $this->moduleName . '.' . $ns);

        return $sort;
    }

    /**
     * Sets tree navigation id to session
     *
     * @return boolean
     */
    public function setTreeNavigationId($id = null, $ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->setAttribute('navigationId', $id, $this->moduleName . '.' . $ns);
    }

    /**
     * Gets tree navigation from session
     *
     * @return boolen
     */
    public function getTreeNavigationId($ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->getAttribute('navigationId', 
                CategoryTable::getInstance()
                ->findOneBySlug(CategoryTable::$main)->getId(), 
                $this->moduleName. '.'.$ns);
    }

    private function getNecessaryData()
    {
        $this->moduleName = sfContext::getInstance()->getRequest()->getParameter('module');
        $this->actionName = sfContext::getInstance()->getRequest()->getParameter('action');
    }
    
    /**
     * Sets sorting
     *
     * @return boolean
     */
    public function setSorting($nr, $ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->setAttribute('sorting', $nr, $this->moduleName . '.'. $this->actionName . '.' . $ns);
    }

    /**
     * Returns sort type
     *
     * @return array
     */
    public function getSorting($ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->getAttribute('sorting', null, $this->moduleName.'.'. $this->actionName .'.' . $ns);
    }
    
    /**
     * Sets table view
     *
     * @return boolean
     */
    public function setTableView($nr, $ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->setAttribute('tableView', $nr, $this->moduleName.'.'.$this->actionName.'.'.$ns);
    }

    /**
     * Returns table view
     *
     * @return string
     */
    public function getTableView($ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->getAttribute('tableView', '', $this->moduleName.'.'.$this->actionName.'.'.$ns);
    }
    
    /**
     * Sets search word
     *
     * @return boolean
     */
    public function setSearchWord($nr, $ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->setAttribute('searchWord', $nr, $this->moduleName.'.'.$this->actionName.'.'.$ns);
    }

    /**
     * Returns search word
     *
     * @return string
     */
    public function getSearchWord($ns = 'default')
    {
        //gets data and put it to vars
        $this->getNecessaryData();

        return $this->getAttribute('searchWord', '', $this->moduleName.'.'.$this->actionName.'.'.$ns);
    }   
    
    /**
     * Send notification to User (email, sms)
     *
     * @return boolean
     */
    public function send($type, $message, $subject = null)
    {
        $type = strtolower($type);
        $class = sfConfig::get('app_notification_type_'.$type);
        
        if(!$class)
        {
            throw new Exception('Missing notification class in config file.');
        }
        
        $notificationMessage = new $class();
        $notificationMessage->composeMessage($this->getGuardUser(), $message, $subject);
        
    }
    
    /**
     * Returns true if user has the given permission/credential.
     *
     * @param string $name The permission name
     * @param boolean $useAnd 
     * @return boolean
     */
    public function hasCredential($permission, $useAnd = true)
    {              
        // gets flattened credential array
        $credentialArr = Cast::multiToOneDimension($permission);  

        $license = priceTool::checkKey();
        if($license === false)
        {
            return false;
        }

        // for lite license checks permissions from priceTool
        $product = (string)$license['product'];
        if(in_array($product, array('MnumiBasic', 'MnumiBusiness')))
        {
            return count(array_intersect($credentialArr, priceTool::$liteAllowedPermissionArr)) 
                    == count($credentialArr);
        }

        // foreach permission checks if user has superUser priviledges
        foreach($credentialArr as $rec)
        {   
            // if user has permission like %_superUser then allow
            $prefix = substr($rec, 0, strpos($rec, '_'));
            $superUserCredential = $prefix.'_superUser';
            if(parent::hasCredential($superUserCredential, false)) 
            {
                return true;
            }
            
            // if user has permission like order_status then bypass all
            // order_status% 
            if(preg_match('/order_status(.*)/', $rec))
            { 
                if(parent::hasCredential('order_status', false)) 
                {
                    return true;
                }
            }         
        }

        return parent::hasCredential($permission, $useAnd);
    }
    
    /**
     * Returns amount in word depending on user.
     * 
     * @param float $amount
     * @param string $currency
     * @return string
     */
    public function getAmountInWords($amount, $currency = null)
    {
        $amount = abs($amount);
        
        $say = new Say();
        return $say->getPL($amount, $this->getCurrencySymbol($currency));
    }
    
    /**
     * Return currency from configuration
     * @return string
     */
    public function getCurrency()
    {
      return sfConfig::get('app_default_currency', 'USD');
    }
    
    /**
     * Get currency symbol (example polish zloty: "zł")
     * 
     * @param string $currency
     * @return string
     */
    public function getCurrencySymbol($currency = null)
    {
    	if($currency == null)
    	{
            $currency = $this->getCurrency();
    	}
    	
    	return sfNumberFormatInfo::getInvariantInfo()->getCurrencySymbol($currency);
    }
    
    /**
     * Return formatted currency
     *
     * @param float $amount
     * @param string $currency
     */
    public function formatCurrency($amount, $currency = null)
    {
        if($currency == null || (is_object($currency) && $currency->getCode() == null))
        {
            $currency = $this->getCurrency();
        }
      
        $nf = new MyNumberFormat($this->getCulture());
        return $nf->format($amount, 'c', $currency);
    }
    
    /**
     * Returns country code of current user.
     * 
     * @return string
     */
    public function getCountryCode()
    {
        // gets country from app.yml
        $countryCode = sfConfig::get('app_default_country');
        
        // if not defined then gets from culture
        if(!$countryCode)
        {
            $countryCode = explode('_', $this->getCulture());
            $countryCode = strtoupper(end($countryCode));
            
            // country - language mapping
            switch($countryCode)
            {
                case 'EN':
                    $countryCode = 'GB';
                    break;
                case 'PL':
                    $countryCode = 'PL';
                    break;
            }
        }
        
        return strtoupper($countryCode);
    }
    
    /**
     * Returns country name.
     * 
     * @param string $countryCode
     * @return string
     */
    public function getCountry($countryCode = null)
    {
        if(!$countryCode)
        {
            $countryCode = $this->getCountryCode();
        }
        
        $c = new sfCultureInfo($this->getCulture());
        $countryArr = $c->getCountries();

        return $countryArr[$countryCode];
    }
}
