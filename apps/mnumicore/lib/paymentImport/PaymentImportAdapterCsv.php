<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Payment import adapter from csv file.
 *
 * @package    mnumicore
 * @subpackage cashDesk
 * @author     Marek Balicki
 */
class PaymentImportAdapterCsv extends PaymentImportAdapter
{    
    protected $delimiter = ';';
    protected $keyArr = array(
        'date' => 0,
        'title' => 2,
        'amount' => 4);

    /**
     * Creates instance of PaymentImportAdapterCsv class.
     * 
     * @param string $filepath 
     * @param string $inputEncoding 
     * @param string $delimiter 
     */
    public function __construct($filepath, $inputEncoding = null, $delimiter = ';')
    {
        parent::__construct($filepath, $inputEncoding);

        // sets delimiter
        $this->delimiter = $delimiter;
 
        // prepared array with data
        $this->contentArr = $this->_csvToArray($this->filename, $delimiter);
    }
    
    /**
     * Returns prepared array parsed from csv file.
     */
    private function _csvToArray()
    {
        $data = array();
        if(($handle = fopen($this->filename, 'r')) !== FALSE)
        {           
            while(($row = fgetcsv($handle, 1000, $this->delimiter)) !== FALSE)
            {
                array_walk($row, array($this, '_toEncoding'));
              
                $data[] = $row;
            }
            fclose($handle);
        }
        return $data;
    }
    
    /**
     * Converts value to defined encoding.
     * 
     * @param array $rec 
     */
    private function  _toEncoding(&$rec) 
    {
        if($this->inputEncoding != $this->outputEncoding)
        {
            $rec = iconv($this->inputEncoding, $this->outputEncoding, $rec);
        }
    }
}