<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Payment import parser.
 *
 * @package    mnumicore
 * @subpackage cashDesk
 * @author     Marek Balicki
 */
class PaymentImportParser
{    
    public $parsedInvoiceArr = array();
    public $parsedPackageArr = array();
    public $parsedOrderArr = array();
    
    protected $sfContext;
    protected $input;
    
    protected $packageWordArr = array('pack');
    protected $orderWordArr = array('ord');

    /**
     *  Returns array with numbers from prepared string.
     * 
     * @param string $input
     * @return array
     */
    public static function getNumbersFromString($input)
    {
        $inputArr = preg_split('/[\s,]+/', trim($input));
        
        $resultArr = array();
        foreach($inputArr as $rec)
        {
            if(!empty($rec))
            {
                $resultArr[] = (int)trim($rec);
            }
        }
        
        return $resultArr;
    }
    
    /**
     * creates instance of parser object.
     * 
     * @param type $input
     */
    public function __construct($input)
    {
        // sets context instance
        $this->sfContext = sfContext::getInstance();
        
        // sets 
        $this->input = $input;
        
        $i18NObj = $this->sfContext->getI18N();

        // package word translate
        foreach($this->packageWordArr as &$rec)
        {
            $rec = $i18NObj->__($rec);
        }
        
        // order word translate
        foreach($this->orderWordArr as &$rec)
        {
            $rec = $i18NObj->__($rec);
        }
    }
    
    /**
     * Returns Doctrin_Collection from string. Method recognizes invoice basing
     * on orders, packages or invoices.
     * 
     * @param Doctrine_Collection $invoicesToPayColl
     * @return \Doctrine_Collection
     */
    public function findInvoices(Doctrine_Collection $invoicesToPayColl)
    {
        // parse invoices
        $uniqueInvoiceIdArr = $this->parsedInvoiceArr = $this->parseInvoices($invoicesToPayColl);

        // parse packages        
        $this->parsedPackageArr = $this->parsePackages();
        if(!empty($this->parsedPackageArr))
        {
            // gets package collection
            $orderPackageColl = OrderPackageTable::getInstance()
                    ->getPackagesByIds($this->parsedPackageArr);
            foreach($orderPackageColl as $rec)
            {
                $invoiceObj = $rec->getInvoice();
                if($invoiceObj)
                {
                    // gets invoice id
                    $invoiceId = $invoiceObj->getId();

                    // if this invoice id is not added to invoice array
                    if(!in_array($invoiceId, $uniqueInvoiceIdArr))
                    {
                        $uniqueInvoiceIdArr[] = $invoiceId;
                    }
                }
            }  
        }
        
        // parse orders   
        $this->parsedOrderArr = $this->parseOrders();
        if(!empty($this->parsedOrderArr))
        {
            // gets package collection
            $orderColl = OrderTable::getInstance()
                    ->getOrderColl($this->parsedOrderArr);
            foreach($orderColl as $rec)
            {
                $invoiceObj = $rec->getInvoice();
                if($invoiceObj)
                {
                    $invoiceId = $invoiceObj->getId();

                    // if this invoice id is not added to invoice array
                    if(!in_array($invoiceId, $uniqueInvoiceIdArr))
                    {
                        $uniqueInvoiceIdArr[] = $invoiceId;
                    }
                }
            }      
        }
        
        // gets matched invoices
        $invoiceColl = InvoiceTable::getInstance()
                ->getInvoiceByIds($uniqueInvoiceIdArr);

        // prepares doctrine collection
        $coll = new Doctrine_Collection('Invoice');
        
        // foreach matched invoice which is not paid
        foreach($invoiceColl as $rec)
        {
            $coll->add($rec);            
        }

        return $coll;
    }

    /**
     * Returns array of parsed invoices.
     * 
     * @param Doctrine_Collection $invoicesToPayColl
     * @return array 
     */
    protected function parseInvoices(Doctrine_Collection $invoicesToPayColl)
    {
        $uniqueInvoiceIdArr = array();
        foreach($invoicesToPayColl as $rec)
        {
            if(preg_match_all(
                    '|'.$rec->getName().'|i',
                    $this->input,
                    $matchArr))
            {
                foreach($matchArr[0] as $rec2)
                {
                    // if this invoice id is not added to invoice array
                    if(!in_array($rec->getId(), $uniqueInvoiceIdArr))
                    {
                        $uniqueInvoiceIdArr[] = $rec->getId();
                    }
                }
            }
        }

        return $uniqueInvoiceIdArr;
    }
    
    /**
     * Returns array of parsed packages.
     * 
     * @return array 
     */
    protected function parsePackages()
    {        
        $invoiceArr = array();
        foreach($this->packageWordArr as $rec)
        {
            if(preg_match_all('/'.$rec.'[a-ząęółńśćź\s:]+([\d,\s]+)/i', 
                    $this->input, $matchArr))
            {
                if(!empty($matchArr[1][0]))
                {
                    // gets numbers
                    return $this->getNumbersFromString($matchArr[1][0]);      
                }
            }
        }

        return $invoiceArr;
    }
    
    /**
     * Returns array of parsed orders.
     * 
     * @return array 
     */
    protected function parseOrders()
    {        
        $orderArr = array();
        foreach($this->orderWordArr as $rec)
        {
            if(preg_match_all('/'.$rec.'[a-ząęółńśćź\s:]+([\d,\s]+)/i', 
                    $this->input, $matchArr))
            {
                if(!empty($matchArr[1][0]))
                {
                    // gets numbers
                    return $this->getNumbersFromString($matchArr[1][0]);      
                }
            }
        }

        return $orderArr;
    }
 
}