<?php

class myAction extends sfActions
{
    /**
     * Form object after execute executeRequestAjaxProcessForm()
     *
     * @var sfForm
     */
    protected $ajaxForm = null;

    /**
     * Executes an application defined process prior to execution of this sfAction object.
     * 
     * @return void
     */
    public function preExecute()
    {
        
    }

    /**
     * Execute an application defined process immediately after execution of this sfAction object.
     * 
     * @return void
     */
    public function postExecute()
    {
        
    }

    /**
     * Executes process form action via ajax.
     *
     * This method has $request parameter.
     * Mandatory params (in GET):
     * - module - from which module action is referred,
     * - model - what kind of object should create (model class),
     * - type - object is new or edit,
     * - forcePost (optional) - if from some reasons request cannot be in xmlRequest.
     * 
     * Moreover $request should have POST request.
     * 
     * @param sfWebRequest $request A request object
     * @param bool $asArray
     * @return string
     */
    protected function executeRequestAjaxProcessForm(sfWebRequest $request, $asArray = false)
    {
        $returnArray = array();

        if($request->isXmlHttpRequest() || $request->getParameter('forcePost'))
        {
            if($request->isMethod('post'))
            {
                $id = null;

                //prepares array
                $errorArray = array();

                $ajaxForm = new AjaxForm();
                $ajaxForm->setRequest($request);

                //creates form
                $form = $ajaxForm->createForm();

                $paramArray = $ajaxForm->getParams();
                $fileArray = $ajaxForm->getFiles();

                // if we have user_id field add current user id
                $fields = $form->getWidgetSchema()->getFields();
                if(array_key_exists('user_id', $fields) 
                        && empty($paramArray['user_id'])
                        && !in_array($form->getName(), array('EditClientNewUserForm')))
                {
                    $paramArray['user_id'] = $this->getUser()->getGuardUser()->getId();
                }

                $this->ajaxForm = $this->processAjaxForm($form, $paramArray, $fileArray);

                $id = $this->ajaxForm->getObject()->getId();

                if(!$this->ajaxForm->isValid())
                {
                    foreach($form as $field)
                    {
                        if($field->hasError())
                        {
                            $errorArray[] = $this->getContext()->getI18N()->__($field->getError()->__toString(), array('%label%' => $this->getContext()->getI18N()->__($field->renderLabelName())));
                        }
                    }
                }
            }
        }

        // when normal POST - no XMLRequest
        if($request->hasParameter('forcePost'))
        {
            $this->disableWebDebug();
        }

        //prepares returnArray
        $returnArray['result']['status'] = 'success';
        $returnArray['result']['id'] = $id;
        $returnArray['result']['error']['errorArray'] = $errorArray;
        $returnArray['result']['error']['errorCount'] = count($errorArray);

        if($asArray == true) {
            return $returnArray;
        }
        return $this->renderText(json_encode($returnArray));
    }

    protected function processAjaxForm(sfForm $form, $requestParams, $requestFiles)
    {
        if($form->isMultipart())
        {
            $form->bind($requestParams, $requestFiles);
        }
        else
        {
            $form->bind($requestParams);
        }

        if($form->isValid())
        {
            $obj = $form->save();

            // if form model is defined as sfGuardUser and returned
            // object is SfGuardUserProfile then sets $obj as sfGuardUser
            if($obj instanceof SfGuardUserProfile && $form->getModelName() == 'sfGuardUser')
            {
                $obj = $obj->getUser();
            }

            $id = $obj->getId();

            $this->getUser()->setFlash('info_data', array(
                'messageType' => 'msg_success',
                'message' => 'Saved successfully.',
                'messageParam' => array()));
        }

        return $form;
    }

    public function disableWebDebug()
    {
        sfConfig::set('sf_web_debug', false);
    } 
}
