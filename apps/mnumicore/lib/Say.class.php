<?php
/*
 * This file is part of the MnumiCore package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class Say
{
    private function sayPL($amount, $part)
    {
        if ($amount == 0) return '';
        if ($amount <= 1.00 && $amount >= 1.99) return __('one').' '.$part;
        if ($amount > 999) return __('Wrong number (> 999)');
        $segment = array();

        $hundred = intval(($amount / 100));
        $ten     = intval(($amount - 100 * $hundred) / 10);
        $unit    = intval(($amount - 100 * $hundred - 10 * $ten));
        switch ($hundred) {
            case 9: $segment[] =__('nine hundred');
                break;
            case 8 : $segment[]=__('eight hundred');
                break;
            case 7 : $segment[]=__('seven hundred');
                break;
            case 6 : $segment[]=__('six hundred');
                break;
            case 5 : $segment[]=__('five hundred');
                break;
            case 4 : $segment[]=__('four hundred');
                break;
            case 3 : $segment[]=__('three hundred');
                break;
            case 2 : $segment[]=__('two hundred');
                break;
            case 1 : $segment[]=__('hundred');
                break;
        }

        switch ($ten) {
            case 9 : $segment[]=__('ninety');
                break;
            case 8 : $segment[]=__('eighty');
                break;
            case 7 : $segment[]=__('seventy');
                break;
            case 6 : $segment[]=__('sixty');
                break;
            case 5 : $segment[]=__('fifty');
                break;
            case 4 : $segment[]=__('forty');
                break;
            case 3 : $segment[]=__('thirty');
                break;
            case 2 : $segment[]=__('twenty');
                break;
            case 1 :
                switch ($unit) {
                    case 9 : $segment[]=__('nineteen');
                        break;
                    case 8 : $segment[]=__('eighteen');
                        break;
                    case 7 : $segment[]=__('seventeen');
                        break;
                    case 6 : $segment[]=__('sixteen');
                        break;
                    case 5 : $segment[]=__('fifteen');
                        break;
                    case 4 : $segment[]=__('fourteen');
                        break;
                    case 3 : $segment[]=__('thirteen');
                        break;
                    case 2 : $segment[]=__('twelve');
                        break;
                    case 1 : $segment[]=__('eleven');
                        break;
                    case 0 : $segment[]=__('ten');
                        break;
                }
        }

        if ($ten != 1) {
            switch ($unit) {
                case 9 : $segment[]=__('nine');
                    break;
                case 8 : $segment[]=__('eight');
                    break;
                case 7 : $segment[]=__('seven');
                    break;
                case 6 : $segment[]=__('six');
                    break;
                case 5 : $segment[]=__('five');
                    break;
                case 4 : $segment[]=__('four');
                    break;
                case 3 : $segment[]=__('three');
                    break;
                case 2 : $segment[]=__('two');
                    break;
                case 1 : $segment[]=__('one');
                    break;
            }
        }
        return (implode(" ", $segment).' '.$part.' ');
    }

    /**
     * Returns amount in words.
     * 
     * @param float $amount
     * @param string $currencySymbol
     * @return string 
     */
    public function getPL($amount, $currencySymbol)
    {
        $mln  = intval(($amount / 1000000));
        $thou = intval((($amount - 1000000 * $mln) / 1000));
        $unit = intval(($amount - 1000000 * $mln - 1000 * $thou));
        $gr   = round(100 * ($amount - 1000000 * $mln - 1000 * $thou - $unit));

        $wordAmount = $this->sayPL($mln, __('mil.'))
            .$this->sayPL($thou, __('thous.'))
            .$this->sayPL($unit, $currencySymbol)
            .$gr.'/100'
        ;

        return $wordAmount;
    }
}