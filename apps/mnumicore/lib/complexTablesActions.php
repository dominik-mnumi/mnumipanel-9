<?php

/**
 * Complex tables actions.
 *
 * @package    mnumicore
 * @subpackage tables
 * @author     Adam Marchewicz <adam.marchewicz@itme.eu>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class complexTablesActions extends tablesActions
{
  /**
   * Default notification list
   *
   * @param sfWebRequest $request
   */
    public function notificationMessageList($request)
    {
        $this->displayTableFields = array(
            'SenderName' => array('label' => 'Sender'),
            'RecipientName' => array('label' => 'Recipient'),
            'Title' => array('label' => 'Title'),
            'MessagePlain' => array('label' => 'Message'),
            'SendAt' => array('label' => 'Send on', 'partial' => 'notificationMessage/sendAt'),
        );
        
        $this->actions = array();
        $this->availableViews = array('table');
        $this->sortableFields = array('title', 'send_at', 'sender_name', 'recipient_name');
        $this->modelObject = 'NotificationMessage';
    }
    
    /**
     * Default order list
     *
     * @param sfWebRequest $request
     */
    public function orderList($request)
    {
        $this->displayTableFields = array(
            'id' => array('label' => 'Number'),
            'PhotoWithPath' => array('label' => 'Photo', 'class' => 'order-align-center'),
            'Name' => array('label' => 'Name', 'partial' => 'order/name'),
            'StatusTitle' => array('label' => 'Status', 'partial' => 'order/status'),
            'Payment' => array('label' => 'Payment', 'partial' => 'order/payment'),
            'AcceptedAt' => array('label' => 'Order / delivery date', 'partial' => 'order/accepted_at'),
            'Delivery' => array('label' => 'Client', 'partial' => 'order/delivery'),
            'Price' => array('label' => 'Price', 'partial' => 'order/price')
        );
        $this->displayGridFields = array(
            'Name' => array('destination' => 'name', 'label' => 'Name'),
            'AcceptedAt' => array('destination' => 'keywords', 'label' => 'Order date')
        );
        $this->sortableFields = array('name', 'created_at', 'updated_at');
        $this->filterForm = new OrderFormFilter();
        $this->modelObject = 'Order';
        $this->actions['delete']['manyRoute'] = false;
        $this->actions['invoice']             = array('label' => 'Invoice',
            'partial' => 'order/invoice');
        $this->actions['exportToXml']         = array('manyRoute' => 'orderListExportXml',
            'label' => 'Export to XML');
        $this->actions['exportToCsv']         = array('manyRoute' => 'orderListExportCsv',
            'label' => 'Export to CSV (selected)');
        $this->actions['exportToCsvAll']      = array('manyRoute' => 'orderListExportCsvAll',
            'label' => 'Export to CSV (all)');

        $this->customQuery = OrderTable::getInstance()->getActiveOrdersQuery();
    }
    
    /**
     * Default invoice list
     * 
     * @param sfWebRequest $request
     */
    public function invoiceList($request)
    {
        $this->displayTableFields = array(
            'FormattedNumber' => array('label' => 'Number', 'partial' => 'invoice/formattedNumber'),
            'ClientName' => array('label' => 'Client name'),
            'CreatedAt' => array('label' => 'Created at', 'partial' => 'invoice/createdAt'),
            'Payment' => array('label' => 'Payment', 'partial' => 'invoice/payment'),
            'Price' => array('label' => 'Price', 'partial' => 'invoice/price'),
            
        );
        $this->displayGridFields = array(
            'Number' => array('destination' => 'number', 'label' => 'Number'),
            'ClientName' => array('destination' => 'client_name', 'label' => 'Client name'),
            'CreatedAt' => array('destination' => 'keywords', 'label' => 'Created at')
        );
        $this->sortableFields = array('number', 'created_at', 'updated_at');
        $this->modelObject = 'Invoice';
        $this->filterForm = new InvoiceFormFilter();

        unset($this->actions['delete']);
        $this->actions['exportToCsv'] = array(
            'manyRoute' => 'invoiceListExportCsv',
            'label' => 'Export to CSV'
        );
        $this->actions['print'] = array(
            'manyRoute' => 'invoicePrintMultiple',
            'label' => 'Print it'
        );
        $this->layoutOptions['showCheckboxes'] = false;
        $this->layoutOptions['manyActionsDefaultLabel'] = 'Choose action...';
    }
    
    /**
     * Default cashDesk list
     * 
     * @param sfWebRequest $request
     */
    public function cashDeskList($request)
    {
        $this->displayTableFields = array('FormattedNumber' => array('label' => 'Number', 'partial' => 'number'),                                       
                                          'ClientName' => array('label' => 'Client'),
                                          'Value' => array('partial' => 'value'),
                                          'Balance' => array(),
                                          'Description' => array('partial' => 'description'),
                                          'sfGuardUser' => array('label' => 'Added by'),
                                          'CreatedAt' => array('label' => 'Created at'));
                                          
        $this->displayGridFields = array(
            'FormattedNumber'  => array('destination' => 'name', 'label' => 'Number'),
            'Balance'  => array('destination' => 'keywords', 'label' => 'Balance'));
        $this->sortableFields = array('type', 'value', 'created_at');
        $this->modelObject = 'CashDesk';
        $this->customQuery = CashDeskTable::getInstance()->createQuery('c')
                ->orderBy('c.created_at desc');
        $this->availableViews = array('table');
        $this->globalActions = false;
        
        // custom action for 
        $this->actions['exportToXml']['manyRoute'] = 'cashDeskExportToXmlMany';
        $this->actions['exportToXml']['label'] = 'Export to XML';
        
        // remove edit action
        $this->removeAction('edit');
        
        $this->filterForm = new CashDeskFormFilter();
    }

    /**
     * Default FieldItem list
     *
     * @param sfWebRequest $request
     */
    public function settingsList($request)
    {
        $this->modelObject = 'FieldItem';
        $this->availableViews = array('table');

        $this->displayTableFields = array(
            'Category' => array(
                'label'     => 'Category',
                'partial'   => 'settings/fieldItem/category'
            ),
            'Name' => array(
                'label'     => 'Name',
                'partial'   => 'settings/fieldItem/name'
            ),
            'Availability' => array(
                'label'     => 'Quantity in stock',
                'partial'   => 'settings/fieldItem/availability'
            ),
            'AvailabilityAlert' => array(
                'label'     => 'Stock warning',
                'partial'   => 'settings/fieldItem/availabilityAlert'
            ),
        );

        $query = FieldsetTable::getFieldItemsByRootIdsQuery(
            (array) $this->fieldset->getFieldsetIds()
        );

        $this->customQuery = FieldItemTable::getInstance()->getDepositoryFieldItemsQuery($query);

        $this->sortableFields = array('name', 'availability', 'availability_alert');

        $this->filterForm = new FieldItemFormFilter();
    }
}