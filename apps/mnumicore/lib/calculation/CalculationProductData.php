<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * calculationProduct class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationProductData
{
    /** @var array Fields labels */
    private $fieldsetLabel = array();

    /** @var PriceListFixPriceCollection $pricelistFixPrice */
    private $PriceListFixPriceCollection;

    /** @var CalculationField[] $fields */
    public $fields;

    /** @var int */
    private $defaultPriceListId;

    /** @var int */
    private $emptyFieldId;

    /** @var string */
    private $otherFinishLabel;

    /** @var array */
    private $defaultFields;

    /**
     * Constructor
     *
     * @param array $defaultFields
     * @param int $defaultPriceListId Id of default Price List
     * @param int $emptyFieldId
     * @param string $otherFinishLabel
     */
    public function __construct($defaultFields, $defaultPriceListId, $emptyFieldId, $otherFinishLabel = 'Finishing')
    {
        $this->defaultFields = $defaultFields;
        $this->defaultPriceListId = $defaultPriceListId;
        $this->emptyFieldId = $emptyFieldId;
        $this->otherFinishLabel = $otherFinishLabel;
    }

    /**
     * Set fix price for specific pricelist
     *
     * @param PriceListFixPriceCollection $collection
     */
    public function setPriceListFixPriceCollection(PriceListFixPriceCollection $collection)
    {
        $this->PriceListFixPriceCollection = $collection;
    }


    /**
     * Add calculation field
     *
     * @param CalculationField $calculationField
     */
    public function addField(CalculationField $calculationField)
    {
        $primaryKey = $calculationField->getType();

        if($primaryKey == 'OTHER') {
            $this->fields[$primaryKey][] = $calculationField;
        }
        $this->fields[$primaryKey] = $calculationField;
    }

    /**
     * Get field
     *
     * @param $primaryKey
     * @return CalculationField|null
     */
    public function getField($primaryKey)
    {
        if(!array_key_exists($primaryKey, $this->fields)) {
            return null;
        }

        return $this->fields[$primaryKey];
    }

    /**
     * @return CalculationField[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Check if product has fixed price
     *
     * @return bool
     */
    public function hasFixedPrice()
    {
        return (bool) (count($this->getPriceListFixPriceCollection()) > 0);
    }

    /**
     * @return PriceListFixPriceCollection
     */
    public function getPriceListFixPriceCollection()
    {
        return $this->PriceListFixPriceCollection;
    }

    /**
     * Get Default Price List ID
     *
     * @todo Consider to set this method as private
     *
     * @return int
     */
    public function getDefaultPriceListId()
    {
        return $this->defaultPriceListId;
    }

    /**
     * @return int
     */
    public function getEmptyFieldId()
    {
        return $this->emptyFieldId;
    }

    /**
     * @return string
     */
    public function getOtherFinishLabel()
    {
        return $this->otherFinishLabel;
    }

    /**
     * @return array
     */
    public function getDefaultFields()
    {
        return $this->defaultFields;
    }

    public function getPriceMeasureUnit()
    {
        if ($this->hasFixedPrice()) {
            return MeasureUnitTable::$formalName['Per copy'];
        }

        return $this->getMeasureUnit();
    }

    public function getMeasureUnit()
    {
        $iterator = $this->getField('MATERIAL')->getIterator();

        if ($iterator->valid()) {
            /** @var CalculationFieldItem $calculationFieldItem */
            $calculationFieldItem = $iterator->current();

            return MeasureUnitTable::$formalName[$calculationFieldItem->getMeasureUnit()];
        }

        return MeasureUnitTable::$formalName['Per copy'];
    }
}