<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Universal calculation decorator class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationDecorator 
{
    /**
     * @var sfI18N
     */
    private $i18n;

    public function __construct($i18n)
    {
        $this->i18n = $i18n;
    }

    /**
     * Get formatted report: with size, quantity and count only
     *
     * @param array $report
     * @return array
     */
    public function getOrderDetailsView($report)
    {
        $view = $this->getView($report);

        $result = array();

        foreach($view as $item)
        {
            if(in_array($item['id'], array('[SIZE]', '[QUANTITY]')))
            {
                $result[] = $item;
            }
        }

        return $result;
    }

    public function getMeasureSize($report)
    {
        $i18n = $this->i18n;

        // if price per count or quantity
        if($report['measureOfUnit'] == 'sheet')
        {
            return $i18n->__plural('1 sheet', '@count sheets', array('@count' => $report['factor']));
        }
        else
        {
            return number_format($report['squareMetre'], 2).' '.$report['measureOfUnit'];
        }
    }

    /**
     * Generate output formatted report
     *
     * @param array $report
     * @return array
     */
    public function getView($report)
    {
        $i18n = $this->i18n;

        $resultArray = array();

        // size
        $label = $i18n->__('Size').': ';
        if($report['size']['fieldLabel'] == '- Customizable -')
        {
            $label .= $report['printSizeWidth'].' x '.$report['printSizeHeight'].' '.$report['sizeMetric'];
        }
        else
        {
            $label .= $i18n->__($report['size']['fieldLabel']);
        }
        $label .= ', '.$i18n->__($report['sides']['fieldLabel']);

        $resultArray[] = array(
            'id' => '[SIZE]',
            'label' => $label,
            'price' => 0,
            'cost' => 0
        );

        // issue
        $label = $i18n->__('Issue').': ' . $this->getMeasureSize($report);

        $resultArray[] = array(
            'id' => '[ISSUE]',
            'label' => $label,
            'price' => 0,
            'cost' => 0
        );

        // quantity
        $label = $i18n->__('Item quantity').': '.$report['count']['value'].' * '.$report['quantity']['value'].' '.$i18n->__('pc.');
        if(!in_array($report['measureType'],
            array('price_linear_metre', 'price_square_metre')))
        {
            $translated = $i18n->__(
                '[0]No cards|[1]One card|(1,+Inf]%count% cards',
                array('%count%' => $report['count']['value'] * $report['quantity']['value'])
            );

            $choice = new sfChoiceFormat();

            $label .= ' (' . $choice->format(
                    $translated,
                    $report['count']['value'] * $report['quantity']['value'])
                    .')'
            ;
        }

        $resultArray[] = array(
            'id' => '[QUANTITY]',
            'label' => $label,
            'price' => 0,
            'cost' => 0
        );

        $additionalCost = 0;

        // if fixed price
        if(!$report['fixedPrice'])
        {
            // material
            $resultArray[] = array(
                'id' => '[MATERIAL]',
                'label' => $i18n->__($report['priceItems']['MATERIAL']['label']).': '.$i18n->__($report['priceItems']['MATERIAL']['fieldLabel']),
                'price' => $report['priceItems']['MATERIAL']['price'],
                'cost' => $report['priceItems']['MATERIAL']['cost']
            );
        }
        else
        {
            $additionalCost += $report['priceItems']['MATERIAL']['cost'];
        }


        // print
        $resultArray[] = array(
            'id' => '[PRINT]',
            'label' => $i18n->__($report['priceItems']['PRINT']['label']).': '.$i18n->__($report['priceItems']['PRINT']['fieldLabel']),
            'price' => $report['priceItems']['PRINT']['price'],
            'cost' => $report['priceItems']['PRINT']['cost'] + $additionalCost,
        );

        // if not others exists
        if(is_array($report['priceItems']) && array_key_exists('OTHER',
                $report['priceItems']))
        {
            foreach($report['priceItems']['OTHER'] as $id => $value)
            {
                $label = ($value['fieldLabel'] != '') ? $i18n->__($value['label']).': '.$i18n->__($value['fieldLabel']) : $i18n->__($value['label']);
                $resultArray[] = array(
                    'id' => '[OTHER]['.$id.']',
                    'label' => $label,
                    'fieldLabel' => $label,
                    'price' => $value['price'],
                    'cost' => $value['cost'],
                    'dbId' => $value['id']
                );
            }
        }

        return $resultArray;
    }
}
