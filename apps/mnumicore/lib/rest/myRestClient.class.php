<?php

/**
 * REST client library
 * 
 * @author Piotr Plenik <piotr.plenik@teamlab.pl>
 * @copyright Mnumi Sp. z o.o.
 * @package Extremouh
 * */
class myRestClient extends baseRestClient
{

    /**
     * Get data about user:
     *  - user data (if logged in)
     *  - basket
     * 
     * @return Zend_Rest_Client_Result
     */
    public function userData($session_handle, $myFullData = false)
    {
        $logger = new RESTLogger(get_class($this), __FUNCTION__);
        $logger->preRest();

        $returnREST = $this->getClient()
                ->doGetMyData($this->getApiKey(), $session_handle, $myFullData)
                ->post();

        $logger->postRest($returnREST);

        return $returnREST;
    }

}
