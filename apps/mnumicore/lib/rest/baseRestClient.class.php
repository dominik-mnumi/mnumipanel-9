<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of baseRestClient
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
abstract class baseRestClient
{
    /**
     * Connection to REST client 
     * (initalized in __construct())
     * @var Zend_Rest_Client
     */
    protected $client = null;
    /**
     * API key for frontend
     * (initalized in __construct())
     * @var string
     */
    protected $api_key = null;
    /**
     * Rest API id - identification 
     * of api connection
     * 
     * @var string
     */
    protected $api_id = null;

    /**
     * Initalize the myRestClient class.
     * @return void
     */
    public function __construct($rest_api_id = 'rest')
    {
        $this->api_id = $rest_api_id;
        $this->client = new Zend_Rest_Client($this->getRestUrl());
        $httpClient = $this->client->getHttpClient();
        $httpClient->setConfig(array('timeout' => sfConfig::get('app_rest_timeout', 20)));
        $this->client->setHttpClient($httpClient);
    }
    
    /*
     * Get initalized REST client
     *
     * @return Zend_Rest_Client
     */

    protected function getClient()
    {
        return $this->client;
    }

    /**
     * Get URL path to REST server
     * (from configuration: app.yml flie)
     *
     * @return string
     */
    protected function getRestUrl()
    {
        return $this->getConfig('url');
    }

    /**
     * Get api key from configuration file
     * (app.yml)
     *
     * @return string
     */
    protected function getApiKey()
    {
        return $this->getConfig('api_key');
    }

    /**
     * Get server session handle
     * (this attribute is initalized in productAction.class.php
     * in executeSetSession()
     *
     * @return string
     */
    protected function getSessionHandle()
    {
        return sfContext::getInstance()->getUser()->getAttribute('webapi_server_session', '');
    }

    /**
     * Get API id
     *
     * @return string
     */
    protected function getRestId()
    {
        return $this->api_id;
    }

    /**
     * Get value from app config
     *
     * @param string $key
     * @return string
     */
    protected function getConfig($key)
    {
        return sfConfig::get(sprintf('app_%s_%s', $this->getRestId(), $key));
    }
}
