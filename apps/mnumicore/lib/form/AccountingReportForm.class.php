<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Accounting report form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class AccountingReportForm extends BaseForm
{    
    public function configure()
    {      
        // gets range options
        $rangeOptionArr = $this->getRangeOptions();
        
        // sets widgets
        $this->setWidget('from_date', 
                new sfWidgetFormInput(array(), array('class' => 'datepicker')));
        $this->setWidget('to_date', 
                new sfWidgetFormInput(array(), array('class' => 'datepicker')));
        
        $this->setWidget('range',
                new sfWidgetFormChoice(array(
                    'label' => 'Report range',
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $rangeOptionArr,
                    'default' => array_keys($rangeOptionArr)
                )));
        
        // sets validators
        $this->setValidator('from_date', 
                new sfValidatorDateTime(
                        array('required' => false, 
                            'datetime_output' => 'Y-m-d 00:00:00')));
        $this->setValidator('to_date', 
                new sfValidatorDateTime(
                        array('required' => false, 
                            'datetime_output' => 'Y-m-d 00:00:00')));
        $this->setValidator('range', new sfValidatorPass());
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
    }
    
    /**
     * Returns prepared array.
     * 
     * @return array 
     */
    protected function getRangeOptions()
    {
        $paymentArr = PaymentTable::getInstance()->findAll()->toArray();
        
        $optionArr = array('sale' => 'Sale');
        $optionArr += Cast::getChoiceArr($paymentArr, 'label', 'id');
        
        return $optionArr;
    }
}