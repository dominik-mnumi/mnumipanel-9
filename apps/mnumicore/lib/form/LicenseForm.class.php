<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Licence Form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class LicenseForm extends BaseForm
{
    public function configure()
    {
        $this->setWidgets(array(
            'license' => new sfWidgetFormTextarea(array(), array('class' => 'full-width', 'rows' => 10)),
        ));
        
        $this->setValidators(array(
            'license' => new sfValidatorLicense(array('max_length' => 1000)),
        ));
        
        $this->getWidgetSchema()->setNameFormat('key[%s]');
    }
    
}

