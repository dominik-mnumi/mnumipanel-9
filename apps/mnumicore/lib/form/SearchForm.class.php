<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended OrderPackageForm.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class SearchForm extends BaseForm
{
    public function configure()
    {
        $this->setWidgets(array(
            'phrase' => new sfWidgetFormInputText(array(), array('autocomplete' => 'off')),
        ));
        
        $this->setValidators(array(
            'phrase' => new sfValidatorString(array('max_length' => 255)),
        ));
        
        $this->widgetSchema->setNameFormat('search_%s');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
    }
    
}

