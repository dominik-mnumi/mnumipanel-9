<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationRegionalSettingsForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationRegionalSettingsForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();
        
        $this->useFields(array(
            'default_currency',
            'price_tax',
            'default_culture',
            'default_country',
            'default_export_encoding'
        ));
    }

    /**
     * Saves data into app.yml.
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['default_currency'] = $values['default_currency'];
        $this->AppArray['all']['price_tax'] = $values['price_tax'];
        $this->AppArray['all']['default_export_encoding'] = $values['default_export_encoding'];
        $this->AppArray['all']['default_country'] = $values['default_country'];

        // sets default culture
        $this->SettingsArray = sfYAML::Load(sfConfig::get('sf_app_config_dir').'/settings.yml');
        $this->SettingsArray['all']['.settings']['default_culture'] = $values['default_culture'];

        // save settings.yml file
        $settingsYamlResult = file_put_contents(sfConfig::get('sf_app_config_dir').'/settings.yml', sfYAML::Dump($this->SettingsArray));
        if(!$settingsYamlResult)
        {
            throw new Exception('No permissions to settings.yml file.');
        }
        
        // sets null as current culture (to get from default_culture)
        sfContext::getInstance()->getUser()->setCulture(null);


        // save app.yml file & clear cache
        parent::save();
    }
}