<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationLabelsPrinterForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationLabelsPrinterForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();



        
        $this->useFields(array(
            'printlabel_name',
            'printlabel_width',
            'printlabel_height',
            'printlabel_orientation',
            'printlabel_large_name',
            'printlabel_large_width',
            'printlabel_large_height',
            'printlabel_large_orientation',
            'ups_access',
            'ups_user_id',
            'ups_password',
            'ups_url',
            'ups_shipper_name',
            'ups_shipper_attention_name',
            'ups_shipper_tax_identification_number',
            'ups_shipper_shipper_number',
            'ups_shipper_address_address_line',
            'ups_shipper_address_city',
            'ups_shipper_address_postal_code',
            'ups_shipper_address_country_code',
            'ups_service_code',
            'ups_package_packaging_code',
            'ups_package_package_weight_unit_of_measurement_code',
            'ups_package_package_weight_weight',
            'ups_package_dimensions_unit_of_measurement_code',
            'ups_package_dimensions_length',
            'ups_package_dimensions_width',
            'ups_package_dimensions_height'));

        // validators
        $taxValidator = new sfValidatorSchemaTaxId(
            'ups_shipper_tax_identification_number',
            array('countryField' => 'ups_shipper_address_country_code'),
            array('invalid' => 'Field "Tax ID" is not valid')
        );

        // sets post validators
        $this->mergePostValidator(new sfValidatorAnd(
                array($taxValidator))
        );
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['printlabel']['name'] = $values['printlabel_name'];
        $this->AppArray['all']['printlabel']['width'] = (empty($values['printlabel_width'])) ? 89 : $values['printlabel_width'];
        $this->AppArray['all']['printlabel']['height'] = (empty($values['printlabel_height']))?36:$values['printlabel_height'];
        $this->AppArray['all']['printlabel']['orientation'] = $values['printlabel_orientation'];
        $this->AppArray['all']['printlabel']['large_name'] = $values['printlabel_large_name'];
        $this->AppArray['all']['printlabel']['large_width'] = (empty($values['printlabel_large_width']))?152.4:$values['printlabel_large_width'];
        $this->AppArray['all']['printlabel']['large_height'] = (empty($values['printlabel_large_height']))?101.6:$values['printlabel_large_height'];
        $this->AppArray['all']['printlabel']['large_orientation'] = $values['printlabel_large_orientation'];
        $this->AppArray['all']['ups']['access'] = $values['ups_access'];
        $this->AppArray['all']['ups']['user_id'] = $values['ups_user_id'];
        $this->AppArray['all']['ups']['password'] = $values['ups_password'] == '0000000000' ? $this->defaults['ups_password'] : $values['ups_password'];
        $this->AppArray['all']['ups']['url'] = $values['ups_url'];
        $this->AppArray['all']['ups']['shipper']['name'] = $values['ups_shipper_name'];
        $this->AppArray['all']['ups']['shipper']['attention_name'] = $values['ups_shipper_attention_name'];
        $this->AppArray['all']['ups']['shipper']['tax_identification_number'] = $values['ups_shipper_tax_identification_number'];
        $this->AppArray['all']['ups']['shipper']['shipper_number'] = $values['ups_shipper_shipper_number'];
        $this->AppArray['all']['ups']['shipper']['address']['address_line'] = $values['ups_shipper_address_address_line'];
        $this->AppArray['all']['ups']['shipper']['address']['city'] = $values['ups_shipper_address_city'];
        $this->AppArray['all']['ups']['shipper']['address']['postal_code'] = $values['ups_shipper_address_postal_code'];
        $this->AppArray['all']['ups']['shipper']['address']['country_code'] = $values['ups_shipper_address_country_code'];
        $this->AppArray['all']['ups']['service']['code'] = $values['ups_service_code'];
        $this->AppArray['all']['ups']['package']['packaging']['code'] = $values['ups_package_packaging_code'];
        $this->AppArray['all']['ups']['package']['package_weight']['unit_of_measurement']['code'] = $values['ups_package_package_weight_unit_of_measurement_code'];
        $this->AppArray['all']['ups']['package']['package_weight']['weight'] = $values['ups_package_package_weight_weight'];
        $this->AppArray['all']['ups']['package']['dimensions']['unit_of_measurement']['code'] = $values['ups_package_dimensions_unit_of_measurement_code'];
        $this->AppArray['all']['ups']['package']['dimensions']['length'] = $values['ups_package_dimensions_length'];
        $this->AppArray['all']['ups']['package']['dimensions']['width'] = $values['ups_package_dimensions_width'];
        $this->AppArray['all']['ups']['package']['dimensions']['height'] = $values['ups_package_dimensions_height'];

        // save app.yml file & clear cache
        parent::save();
    }
}
