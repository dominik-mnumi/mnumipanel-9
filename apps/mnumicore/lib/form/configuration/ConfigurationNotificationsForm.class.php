<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationNotificationsForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationNotificationsForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();
        
        $this->useFields(array(
            'sms_server_user', 
            'sms_server_password',
            'sms_server_id', 
            'notification_sender', 
            'notification_sender_name',
            'notification_default_enable_sms',
            'notification_default_enable_email'));
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['sms_server']['user'] = $values['sms_server_user'];
        $this->AppArray['all']['sms_server']['password'] = $values['sms_server_password'];
        $this->AppArray['all']['sms_server']['id'] = $values['sms_server_id'];
        $this->AppArray['all']['notification']['sender'] = $values['notification_sender'];
        $this->AppArray['all']['notification']['sender_name'] = $values['notification_sender_name'];
        $this->AppArray['all']['notification']['default_disable_sms'] = !$values['notification_default_enable_sms'];
        $this->AppArray['all']['notification']['default_disable_email'] = !$values['notification_default_enable_email'];

        // save app.yml file & clear cache
        parent::save();
    }
}