<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationDeliveryAndPaymentForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationDeliveryAndPaymentForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();
        
        $this->validatorSchema->setPostValidator(new sfValidatorCallback(array(
                'callback' => array($this, 'validPayment'))));
        
        $this->useFields(array(
            'default_delivery',
            'default_payment',
            'default_delivery_days'));
    }

    /**
     * Saves data into app.yml.
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['default_delivery'] = $values['default_delivery'];
        $this->AppArray['all']['default_payment'] = $values['default_payment'];
        $this->AppArray['all']['default_delivery_days'] = $values['default_delivery_days'];

        // saves app.yml file & clear cache
        parent::save();
    }
    
    /**
     * Checks if current payment is valid depending on current carrier.
     * 
     * @param type $validator
     * @param type $values
     * @return type 
     */
    public function validPayment($validator, $values)
    {
        if($values['default_delivery'] && $values['default_payment'])
        {
            $paymentIdArray = array_keys(Cast::getChoiceArr(
                PaymentTable::getInstance()->getFilteredPayment(
                        null, 
                        null, 
                        null, 
                        $values['default_delivery']),
                'label', 
                'id'));

            if(!in_array($values['default_payment'], $paymentIdArray))
            {
                $error = new sfValidatorError($validator, 'Wrong default payment');
                throw new sfValidatorErrorSchema($validator, array('default_payment' => $error));
            }
        }
        return $values;
    }
}