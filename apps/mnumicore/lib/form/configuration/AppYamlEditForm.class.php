<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * AppYamlEditForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class AppYamlEditForm extends BaseForm
{
    protected static $currencies = array('CHF', 'CZK', 'EUR', 'GBP', 'PLN', 'RUB', 'USD');
    protected static $cultures = array(
        'en' => 'English', 
        'pl' => 'Polish');
    protected static $loyaltyPointsConvert = 100;
    protected static $serviceTypes = array(
        '01' => 'Next Day Air',
        '02' => '2nd Day Air',
        '03' => 'Ground',
        '07' => 'Express',
        '08' => 'Expedited',
        '11' => 'UPS Standard',
        '12' => '3 Day Select',
        '13' => 'Next Day Air Saver',
        '14' => 'Next Day Air Early AM',
        '54' => 'Express Plus',
        '59' => '2nd Day Air A.M.',
        '65' => 'UPS Saver',
        '82' => 'UPS Today Standard',
        '83' => 'UPS Today Dedicated Courier',
        '84' => 'UPS Today Intercity',
        '85' => 'UPS Today Express',
        '86' => 'UPS Today Express Saver',
        '96' => 'UPS Worldwide Express Freight');
    protected static $packageTypes = array(
        '01' => 'UPS Letter',
        '02' => 'Customer Supplied Package',
        '03' => 'Tube',
        '04' => 'PAK',
        '21' => 'UPS Express Box',
        '24' => 'UPS 25KG Box',
        '25' => 'UPS 10KG Box',
        '30' => 'Pallet',
        '2a' => 'Small Express Box',
        '2b' => 'Medium Express Box',
        '2c' => 'Large Express Box');
    protected static $unitOfWeightMeasurement = array(
        'LBS' => 'pounds',
        'KGS' => 'kilograms');
    protected static $unitOfLengthMeasurement = array(
        'IN' => 'inches',
        'CM' => 'centimeters',
        '00' => 'Metric Units Of Measurement',
        '01' => 'English Units of Measurement');

    protected static $encodingArr = array(
        'windows-1250',
        'UTF-8',
        'UTF-16',
        'UTF-32'
    );

    protected static $fiscalPrinterType = array(
        'Thermal401' => 'Posnet Thermal 4.01',
        'ElzabMera' => 'Elzab Mera',
        'ElzabOmega2' => 'Elzab Omega 2',
        'InnovaProfit451' => 'Innova Profit 4.51',
        'OptimusVivo' => 'Optimus VIVO',
        'DFEmul' => 'Emulator',
        'BogusFiscalPrinter' => 'Konsola Test',
    );

    public function configure()
    {
        // gets context object
        $contextObj = sfContext::getInstance();
        
        $this->AppArray = sfYAML::Load(sfConfig::get('sf_app_config_dir').'/app.yml');
        $this->AppArray['all']['local_file_server']['enable'] = $contextObj->getRequest()->getCookie('local_file_server_enable');
        $this->AppArray['all']['default_culture'] = substr(sfConfig::get('sf_default_culture'), 0, 2);


        // get country from configuration or default country
        if(
            !array_key_exists('seller_country', $this->AppArray['all']['company_data']) ||
            $this->AppArray['all']['company_data']['seller_country'] == ''
        )
        {
            $defaultCountry = mnumicoreConfiguration::getCountry();
        }
        else
        {
            $defaultCountry = $this->AppArray['all']['company_data']['seller_country'];
        }

        $this->AppArray['all']['company_data']['seller_country'] = strtoupper($defaultCountry);

        $this->prepareDefaults($this->AppArray['all']);

        $edit_mode = isset($this->defaults['company_data_seller_logo_path'])
            && $this->defaults['company_data_seller_logo_path']
            && isset($this->defaults['company_data_seller_logo_image'])
            && $this->defaults['company_data_seller_logo_image'];

        // gets carrier choice array
        $carrierChoiceArr = Cast::getChoiceArr(
                CarrierTable::getInstance()->getActiveCarriers(), 
                'label', 
                'id');

        // gets payment choice array
        $paymentChoiceArr = Cast::getChoiceArr(
                PaymentTable::getInstance()->getFilteredPayment(
                        null, 
                        null, 
                        null, 
                        null),
                'label', 
                'id');
        $orientations = array('landscape' => 'landscape', 'portrait' => 'portrait', 'reverse_landscape' => 'reverse_landscape');

        $this->setWidgets(array(
            'printlabel_name' => new sfWidgetFormInput(array()),
            'printlabel_width' => new sfWidgetFormInput(array()),
            'printlabel_height' => new sfWidgetFormInput(array()),
            'printlabel_orientation' => new sfWidgetFormSelect(array('choices' => $orientations)),
            'printlabel_large_name' => new sfWidgetFormInput(array()),
            'printlabel_large_width' => new sfWidgetFormInput(array()),
            'printlabel_large_height' => new sfWidgetFormInput(array()),
            'printlabel_large_orientation' => new sfWidgetFormSelect(array('choices' => $orientations)),
            'company_data_seller_name' => new sfWidgetFormInput(array()),
            'company_data_seller_address' => new sfWidgetFormInput(array()),
            'company_data_seller_postcode' => new sfWidgetFormInput(array()),
            'company_data_seller_city' => new sfWidgetFormInput(array()),
            'company_data_seller_tax_id' => new sfWidgetFormInput(array()),
            'company_data_seller_bank_name' => new sfWidgetFormInput(array()),
            'company_data_seller_bank_account' => new sfWidgetFormInput(array()),
            'company_data_seller_logo_image' => new sfWidgetFormInputFileEditable(
                        array('file_src' => sfContext::getInstance()->getRouting()->generate('imageHandlePreview',
                                array('type' => 'logo',
                                    'width' => 100,
                                    'fileName' => array_key_exists('company_data_seller_logo_image', $this->defaults)
                                        ? $this->defaults['company_data_seller_logo_image'] : null
                                )),
                            'is_image' => true,
                            'edit_mode' => $edit_mode,
                            'delete_label' => 'Delete image')),
            'company_data_seller_country' => new sfWidgetFormI18nChoiceCountry(
                array('culture' => sfContext::getInstance()->getUser()->getCulture(), 'label' => 'Country')
            ),
            'default_currency' => new sfWidgetFormChoice(array('choices' => array_combine(self::$currencies, self::$currencies))),
            'sms_server_user' => new sfWidgetFormInput(array()),
            'sms_server_password' => new sfWidgetFormInput(array()),
            'sms_server_id' => new sfWidgetFormInput(array()),
            'notification_sender' => new sfWidgetFormInput(array()),
            'notification_sender_name' => new sfWidgetFormInput(array()),
            'price_tax' => new sfWidgetFormInput(array()),
            'invoice_receipt_to_invoice_days' => new sfWidgetFormInput(array()),
            'invoice_naming_pattern_invoice' => new sfWidgetFormInput(array()),
            'invoice_naming_pattern_correction' => new sfWidgetFormInput(array()),
            'invoice_naming_pattern_preliminary' => new sfWidgetFormInput(array()),
            'invoice_naming_pattern_receipt' => new sfWidgetFormInput(array()),
            'invoice_item_format_order' => new sfWidgetFormInput(array('default' => '{id}: {order}')),
            'invoice_notice' => new sfWidgetFormTextarea(array()),
            'invoice_allow_zeroed_prices' => new sfWidgetFormInputCheckbox(array()),
            'loyalty_points_scale' => new sfWidgetFormInput(array()),
            'loyalty_points_reverted_scale' => new sfWidgetFormInput(array()),
            'notification_default_enable_sms' => new sfWidgetFormInputCheckbox(array()),
            'notification_default_enable_email' => new sfWidgetFormInputCheckbox(array()),
            'platnoscipl_pos_id' => new sfWidgetFormInput(array()),
            'platnoscipl_pos_auth' => new sfWidgetFormInputPassword(array(), array('value' => empty($this->defaults['platnoscipl_pos_auth']) ? '' : '0000000000')),
            'platnoscipl_key_1' => new sfWidgetFormInputPassword(array(), array('value' => empty($this->defaults['platnoscipl_key_1']) ? '' : '00000000000000000000000000000000')),
            'platnoscipl_key_2' => new sfWidgetFormInputPassword(array(), array('value' => empty($this->defaults['platnoscipl_key_2']) ? '' : '00000000000000000000000000000000')),
            'local_file_server_ip' => new sfWidgetFormInput(array()),
            'local_file_server_enable' => new sfWidgetFormInputCheckbox(array()),
            'default_culture' => new sfWidgetFormChoice(
                        array('choices' => self::$cultures)),
            'default_country' => new sfWidgetFormI18nChoiceCountry(
                        array('add_empty' => '-- same as language --',
                            'culture' => $contextObj->getUser()->getCulture()),
                        array('class' => 'full-width')),
            'ups_access' => new sfWidgetFormInput(array()),
            'ups_user_id' => new sfWidgetFormInput(array()),
            'ups_password' => new sfWidgetFormInputPassword(
                    array(), 
                    array('value' => empty($this->defaults['ups_password']) ? '' : '0000000000')),
            'ups_url' => new sfWidgetFormInput(array()),
            'ups_shipper_name' => new sfWidgetFormInput(array()),
            'ups_shipper_attention_name' => new sfWidgetFormInput(array()),
            'ups_shipper_tax_identification_number' => new sfWidgetFormInput(array()),
            'ups_shipper_shipper_number' => new sfWidgetFormInput(array()),
            'ups_shipper_address_address_line' => new sfWidgetFormInput(array()),
            'ups_shipper_address_city' => new sfWidgetFormInput(array()),
            'ups_shipper_address_postal_code' => new sfWidgetFormInput(array()),
            'ups_shipper_address_country_code' => new sfWidgetFormI18nChoiceCountry(
                        array('default' => $contextObj->getUser()->getCountryCode(),
                            'culture' => $contextObj->getUser()->getCulture()),
                        array('class' => 'full-width')),
            'ups_service_code' => new sfWidgetFormChoice(
                    array('default' => '11',
                        'choices' => self::$serviceTypes)),
            'ups_package_packaging_code' => new sfWidgetFormChoice(
                    array('default' => '02',
                        'choices' => self::$packageTypes)),
            'ups_package_package_weight_unit_of_measurement_code' => new sfWidgetFormChoice(
                    array('default' => 'KGS',
                        'choices' => self::$unitOfWeightMeasurement)),
            'ups_package_package_weight_weight' => new sfWidgetFormInput(array(
                'default' => 1
            )),
            'ups_package_dimensions_unit_of_measurement_code' => new sfWidgetFormChoice(
                    array('default' => 'CM',
                        'choices' => self::$unitOfLengthMeasurement)),
            'ups_package_dimensions_length' => new sfWidgetFormInput(array(
                'default' => 50
            )),
            'ups_package_dimensions_width' => new sfWidgetFormInput(array(
                'default' => 50
            )),
            'ups_package_dimensions_height' => new sfWidgetFormInput(array(
                'default' => 10
            )),
            'fiscal_printer_com' => new sfWidgetFormInput(array(
                'default' => 'COM1',
            )),
            'fiscal_printer_type' => new sfWidgetFormChoice(array(
                'choices' => self::$fiscalPrinterType,
            )),
            'default_delivery' => new sfWidgetFormChoice(array(
                'choices' => $carrierChoiceArr)),
            'default_payment' => new sfWidgetFormChoice(array(
                'choices' => $paymentChoiceArr)),
            'default_delivery_days' => new sfWidgetFormInput(array(
                'default' => 1)),
            'default_disable_advanced_accountancy' => new sfWidgetFormInputCheckbox(array()),
            'default_export_encoding' => new sfWidgetFormChoice(array(
                'choices' => array_combine(
                    self::$encodingArr,
                    self::$encodingArr
                )))
        ));

        $i18NObj = sfContext::getInstance()->getI18N();
        $this->widgetSchema->setLabels(array(
            'printlabel_name' => 'Printer',
            'printlabel_width' => 'Paper width',
            'printlabel_height' => 'Paper height',
            'printlabel_orientation' => 'Paper orientation',
            'printlabel_large_name' => 'Large printer',
            'printlabel_large_width' => 'Paper width',
            'printlabel_large_height' => 'Paper height',
            'printlabel_large_orientation' => 'Paper orientation',
            'company_data_seller_name' => 'Company full name',
            'company_data_seller_address' => 'Street',
            'company_data_seller_postcode' => 'Postcode',
            'company_data_seller_city' => 'City',
            'company_data_seller_tax_id' => 'Taxpayer Identification Number (TIN)',
            'company_data_seller_bank_name' => 'Bank name',
            'company_data_seller_bank_account' => 'Bank account',
            'company_data_seller_logo_image' => 'Logo',
            'default_currency' => 'Currency',
            'sms_server_user' => 'Public key',
            'sms_server_password' => 'Private key',
            'sms_server_id' => 'Connection id',
            'notification_sender' => 'Sender email',
            'notification_sender_name' => 'Sender name',
            'price_tax' => 'Tax',
            'invoice_receipt_to_invoice_days' => 'Number of days that can change receipt to invoice',
            'invoice_naming_pattern_invoice' => 'Pattern of invoice name',
            'invoice_naming_pattern_correction' => 'Pattern of correction invoice name',
            'invoice_naming_pattern_preliminary' => 'Pattern of preliminary invoice name',
            'invoice_naming_pattern_receipt' => 'Pattern of receipt name',
            'invoice_item_format_order' => 'Format of accounting document item',
            'invoice_notice' => 'Default information on the invoice',
            'invoice_allow_zeroed_prices' => 'Allow zeroed prices on accounting document',
            'loyalty_points_scale' => $i18NObj->__('Points value of 100 spent currency unit (%currency%)', 
                    array('%currency%' => sfContext::getInstance()->getUser()->getCurrency())),
            'loyalty_points_reverted_scale' => 'Cash value of 100 loyalty point',
            'notification_default_enable_sms' => 'Enable SMS notification by default',
            'notification_default_enable_email' => 'Enable email notification by default',
            'platnoscipl_pos_id' => 'Payment point id',
            'platnoscipl_pos_auth' => 'Payment authorization key',
            'platnoscipl_key_1' => 'Key 1',
            'platnoscipl_key_2' => 'Key 2',
            'local_file_server_ip' => 'IP address',
            'local_file_server_enable' => 'Enable local file server on THIS computer',
            'default_culture' => 'Language',
            'default_country' => 'Country',
            'ups_access' => 'Access key',
            'ups_user_id' => 'User ID',
            'ups_password' => 'Password',
            'ups_url' => 'API url',
            'ups_shipper_name' => 'Sender name',
            'ups_shipper_attention_name' => 'Sender attention name',
            'ups_shipper_tax_identification_number' => 'Taxpayer Identification Number (TIN)',
            'ups_shipper_shipper_number' => 'Sender number',
            'ups_shipper_address_address_line' => 'Sender address',
            'ups_shipper_address_city' => 'Sender city',
            'ups_shipper_address_postal_code' => 'Sender postcode',
            'ups_shipper_address_country_code' => 'Sender country',
            'ups_service_code' => 'Service type',
            'ups_package_packaging_code' => 'Package type',
            'ups_package_package_weight_unit_of_measurement_code' => 'Unit of weight measurement',
            'ups_package_package_weight_weight' => 'Default package weight (in unit above)',
            'ups_package_dimensions_unit_of_measurement_code' => 'Unit of length measurement',
            'ups_package_dimensions_length' => 'Default package length (in unit above)',
            'ups_package_dimensions_width' => 'Default package width (in unit above)',
            'ups_package_dimensions_height' => 'Default package height (in unit above)',
            'default_delivery' => 'Default delivery',
            'default_payment' => 'Default payment',
            'default_delivery_days' => 'Default delivery time (days)',
            'default_disable_advanced_accountancy' => 'Disable advanced accountancy',
            'default_export_encoding' => 'Export file encoding',
            'fiscal_printer_com' => 'COM port',
            'fiscal_printer_type' => 'Printer type',
        ));
        
        $this->setValidators(array(
            'printlabel_name' => new sfValidatorString(
                    array('required' => true, 
                        'max_length' => 100)),
            'printlabel_width' => new sfValidatorNumber(
                    array('required' => false)),
            'printlabel_height' => new sfValidatorNumber(
                    array('required' => false)),
            'printlabel_orientation' => new sfValidatorString(
                    array('required' => false)),
            'printlabel_large_name' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 100)),
            'printlabel_large_width' => new sfValidatorNumber(
                    array('required' => false)),
            'printlabel_large_height' => new sfValidatorNumber(
                    array('required' => false)),
            'printlabel_large_orientation' => new sfValidatorString(
                    array('required' => false)),
            'company_data_seller_name' => new sfValidatorString(
                    array('required' => true, 
                        'max_length' => 100)),
            'company_data_seller_address' => new sfValidatorString(
                    array('required' => true, 
                        'max_length' => 100)),
            'company_data_seller_postcode' => new sfValidatorString(
                    array('required' => true, 
                        'max_length' => 20)),
            'company_data_seller_city' => new sfValidatorString(
                    array('required' => true, 
                        'max_length' => 50)),
            'company_data_seller_tax_id' => new sfValidatorString( // tax id validation is in postValidation()
                    array('required' => true)),
            'company_data_seller_bank_name' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 50)),
            'company_data_seller_bank_account' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 50)),
            'company_data_seller_logo_image' => new sfValidatorFile(
                    array('required' => false,
                        'mime_types' => 'web_images')),
            'company_data_seller_logo_image_delete' => new sfValidatorBoolean(),
            'company_data_seller_country' => new sfValidatorI18nChoiceCountry(),
            'default_currency' => new sfValidatorChoice(
                    array('choices' => self::$currencies)),
            'sms_server_user' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 50)),
            'sms_server_password' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 50)),
            'sms_server_id' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 100)),
            'notification_sender' => new sfValidatorEmail(
                    array('required' => true)),
            'notification_sender_name' => new sfValidatorString(
                    array('required' => true, 
                        'max_length' => 255)),            
            'price_tax' => new sfValidatorNumber(
                    array('required' => true)),
            'invoice_receipt_to_invoice_days' => new sfValidatorNumber(
                    array('required' => false)),
            'invoice_naming_pattern_invoice' => new sfValidatorString(
                    array('required' => true)),
            'invoice_naming_pattern_correction' => new sfValidatorString(
                    array('required' => true)),
            'invoice_naming_pattern_preliminary' => new sfValidatorString(
                    array('required' => true)),
            'invoice_naming_pattern_receipt' => new sfValidatorString(
                    array('required' => true)),
            'invoice_item_format_order' => new sfValidatorString(
                    array('required' => true)),
            'invoice_notice' => new sfValidatorString(
                    array('required' => false)),
            'invoice_allow_zeroed_prices' => new sfValidatorBoolean(),
            'loyalty_points_scale' => new sfValidatorNumber(
                    array('required' => true)),
            'loyalty_points_reverted_scale' => new sfValidatorNumber(
                    array('required' => true)),
            'notification_default_enable_sms' => new sfValidatorBoolean(),
            'notification_default_enable_email' => new sfValidatorBoolean(),
            'platnoscipl_pos_id' => new sfValidatorRegex(
                    array('required' => true,
                        'pattern' => '/^[0-9]{1,100}$/i'),
                    array('invalid' => 'Field "Payment point id" must be an integer')),
            'platnoscipl_pos_auth' => new sfValidatorRegex(
                    array('required' => true,
                        'pattern' => '/^[a-z0-9]{1,10}$/i'),
                    array('invalid' => 'Field "Payment authorization key" is too long (10 characters max)')),
            'platnoscipl_key_1' => new sfValidatorRegex(
                    array('required' => true,
                        'pattern' => '/^[a-z0-9]{32}$/i',),
                    array('invalid' => 'Field "Key 1" can only have letters (a-z) or numbers (0-9) and 32 chars long')),
            'platnoscipl_key_2' => new sfValidatorRegex(
                    array('required' => true,
                        'pattern' => '/^[a-z0-9]{32}$/i'),
                    array('invalid' => 'Field "Key 2" can only have letters (a-z) or numbers (0-9) and 32 chars long')),
            'local_file_server_ip' => new sfValidatorIp(
                    array('required' => false)),
            'local_file_server_enable' => new sfValidatorBoolean(),
            'default_culture' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$cultures))),
            'default_country' => new sfValidatorI18nChoiceCountry(
                    array('required' => false)),
            'ups_access' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 25)),
            'ups_user_id' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 25)),
            'ups_password' => new sfValidatorPass(
                    array('required' => false)),
            'ups_url' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 255)),
            'ups_shipper_name' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 255)),
            'ups_shipper_attention_name' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 255)),
            'ups_shipper_tax_identification_number' =>  new sfValidatorString( // tax id validation is in postValidation()
                array('required' => false)),
            'ups_shipper_shipper_number' => new sfValidatorRegex(
                    array('required' => false,
                        'pattern' => '/^[a-z0-9]{1,6}$/i'),
                    array('invalid' => 'Field "Sender number" is invalid (6 characters max)')),
            'ups_shipper_address_address_line' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 255)),
            'ups_shipper_address_city' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 255)),
            'ups_shipper_address_postal_code' => new sfValidatorString(
                    array('required' => false, 
                        'max_length' => 20)),
            'ups_shipper_address_country_code' => new sfValidatorI18nChoiceCountry(
                    array('required' => false)),
            'ups_service_code' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$serviceTypes))),
            'ups_package_packaging_code' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$packageTypes))),
            'ups_package_package_weight_unit_of_measurement_code' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$unitOfWeightMeasurement))),
            'ups_package_package_weight_weight' =>new sfValidatorNumber(
                    array('required' => false),
                    array('invalid' => 'Field "%label%" is invalid')),
            'ups_package_dimensions_unit_of_measurement_code' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$unitOfLengthMeasurement))),
            'ups_package_dimensions_length' => new sfValidatorNumber(
                    array('required' => false),
                    array('invalid' => 'Field "%label%" is invalid')),
            'ups_package_dimensions_width' => new sfValidatorNumber(
                    array('required' => false),
                    array('invalid' => 'Field "%label%" is invalid')),
            'ups_package_dimensions_height' => new sfValidatorNumber(
                    array('required' => false),
                    array('invalid' => 'Field "%label%" is invalid')),
            'default_delivery' => new sfValidatorChoice(
                    array(
                'choices' => array_keys($carrierChoiceArr)),
                    array(
                        'invalid' => 'Wrong default delivery'
                    )),
            'default_payment' => new sfValidatorChoice(
                    array(
                'choices' => array_keys($paymentChoiceArr)),
                    array(
                        'invalid' => 'Wrong default payment'
                    )),
            'default_delivery_days' => new sfValidatorNumber(
                    array('required' => true),
                    array('invalid' => 'Field "%label%" is invalid')),
            'default_disable_advanced_accountancy' => new sfValidatorBoolean(),
            'default_export_encoding' => new sfValidatorChoice(
                array('choices' => self::$encodingArr)),
            'fiscal_printer_com' => new sfValidatorString(
                array(
                    'required' => false,
                    'max_length' => 5,
                )),
            'fiscal_printer_type' => new sfValidatorChoice(
                array('choices' => array_keys(self::$fiscalPrinterType))),

        ));
        
        $this->setMessages();
 
        $this->widgetSchema->setHelps(array(
            'printlabel_name' => 'Default printer',
            'printlabel_width' => 'Width expressed in millimeters (eg. 101.6)',
            'printlabel_height' => 'Height expressed in millimeters (eg. 152.4)',
            'printlabel_large_name' => 'For UPS labels',
            'printlabel_large_width' => 'Width expressed in millimeters (eg. 101.6)',
            'printlabel_large_height' => 'Height expressed in millimeters (eg. 152.4)',
            'company_data_seller_name' => 'Information displayed among others in invoices',
            'company_data_seller_tax_id' => 'Number without "-" (for example 1234567819)',
            'default_currency' => 'Default currency',
            'sms_server_user' => 'Public key (PubKey) in connection settings',
            'sms_server_password' => 'Private key (PrivKey) in connection settings',
            'sms_server_id' => 'Connection id (IDconn) with service',
            'price_tax' => 'Tax in format 1/100 (for example 0.23)',
            'loyalty_points_scale' => 'Number of loyalty points (for example 0.5 or 300)',
            'loyalty_points_reverted_scale' => 'Cash value (for example 0.5 or 300)',
            'notification_default_enable_sms' => 'For example in registration',
            'notification_default_enable_email' => 'For example in registration',
            'notification_sender' => 'All notifications to your customers will be sent from this email address',
            'notification_sender_name' => 'This name will appear as the sender name of any notifications to your customers',
            'invoice_naming_pattern_invoice' => 'For NEW invoice name like \'FW/12/2013/1000\', you should type \'FW/mm/yy/{number}\'',
            'invoice_naming_pattern_correction' => 'For CORRECTION invoice name like \'CORR/12/2013/1000\', you should type \'CORR/mm/yy/{number}\'',
            'invoice_naming_pattern_preliminary' => 'For PRELIMINARY invoice name like \'PREL/12/2013/1000\', you should type \'PREL/mm/yy/{number}\'',
            'invoice_naming_pattern_receipt' => 'For RECEIPT name like \'REC/12/2013/1000\', you should type \'REC/mm/yy/{number}\'',
            'invoice_item_format_order' => 'Available formats: {id} - order number, {order} - order name, {product} - product name, {quantity} - quantity from order',
            'default_country' => 'Default country',
            'ups_url' => 'API url, that will be used to generate UPS labels',
            'ups_shipper_name' => 'e.g. Sample Company',
            'ups_shipper_attention_name' => 'Additional sender name e.g. John Whittle',
            'ups_shipper_shipper_number' => 'Field must be 6 alphanumeric characters long',
            'ups_shipper_address_address_line' => 'Sender street and number'
        ));

        // unsets 
        unset($this->AppArray['all']['local_file_server']['enable'],
                $this->AppArray['all']['default_culture']);
        
        $this->setCssClasses('full-width');
        $this->getWidget('company_data_seller_logo_image')->setAttribute('class', '');
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
        
        $this->setDefaults($this->defaults);    
    }

    /**
     * Prepare default data
     * 
     */
    protected function prepareDefaults($value, $key = '')
    {
        if(is_array($value))
        {
            foreach($value as $k => $v)
            {  
                $this->prepareDefaults($v, $key ? $key.'_'.$k : $k);
            }
        }
        else
        {
            // conversion must be set for points 
            if(in_array($key, array('loyalty_points_scale', 'loyalty_points_reverted_scale')))
            {
                $value = $value * self::$loyaltyPointsConvert;
            }
            // negation for notification default disabled
            elseif(in_array($key, array('notification_default_disable_sms', 'notification_default_disable_email')))
            {
                $value = !$value;
                
                $key = str_replace('disable', 'enable', $key);
            }
            
            $this->defaults["$key"] = $value;
        }
    }
    
    /**
     * Saves data into app.yml.
     * Load $this->AppArray from child class
     *
     */
    public function save()
    {
        // save app.yml file
        file_put_contents(sfConfig::get('sf_app_config_dir').'/app.yml', sfYAML::Dump($this->AppArray, 4));
        
        // clear cache
        $cache = new sfFileCache(array('cache_dir' => sfConfig::get('sf_cache_dir').'/'));
        $cache->clean();
    }
}
