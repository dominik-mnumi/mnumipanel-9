<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationFinanceForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationFinanceForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();
        
        $this->useFields(array(
            'default_currency', 
            'price_tax',
            'invoice_receipt_to_invoice_days', 
            'invoice_naming_pattern_invoice', 
            'invoice_naming_pattern_correction',
            'invoice_naming_pattern_preliminary',
            'invoice_naming_pattern_receipt',
            'invoice_item_format_order',
            'invoice_notice',
            'invoice_allow_zeroed_prices',
            'loyalty_points_scale',
            'loyalty_points_reverted_scale',
            'default_disable_advanced_accountancy'));
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['invoice']['receipt_to_invoice_days'] = $values['invoice_receipt_to_invoice_days'];
        $this->AppArray['all']['invoice']['naming_pattern']['invoice'] = $values['invoice_naming_pattern_invoice'];
        $this->AppArray['all']['invoice']['naming_pattern']['correction'] = $values['invoice_naming_pattern_correction'];
        $this->AppArray['all']['invoice']['naming_pattern']['preliminary'] = $values['invoice_naming_pattern_preliminary'];
        $this->AppArray['all']['invoice']['naming_pattern']['receipt'] = $values['invoice_naming_pattern_receipt'];
        $this->AppArray['all']['invoice']['item_format']['order'] = $values['invoice_item_format_order'];
        $this->AppArray['all']['invoice']['notice'] = $values['invoice_notice'];
        $this->AppArray['all']['invoice']['allow_zeroed_prices'] = $values['invoice_allow_zeroed_prices'];
        $this->AppArray['all']['loyalty_points']['scale'] = $values['loyalty_points_scale'] / self::$loyaltyPointsConvert;
        $this->AppArray['all']['loyalty_points']['reverted_scale'] = $values['loyalty_points_reverted_scale'] / self::$loyaltyPointsConvert;
        $this->AppArray['all']['default_disable_advanced_accountancy'] = $values['default_disable_advanced_accountancy'];
        $this->AppArray['all']['price_tax'] = $values['price_tax'];

        // save app.yml file & clear cache
        parent::save();
    }
}
