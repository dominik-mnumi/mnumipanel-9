<?php

/**
 * Invoice sale report filter form.
 *
 * @package    mnumicore
 * @author     Bartosz Dembek<bartosz.dembek@mnumi.com>
 */
class InvoiceSaleReportFormFilter extends BaseInvoiceFormFilter
{

    public function configure()
    {
        $this->setWidgets(array(
            'from_date' => new sfWidgetFormInput(array('label' => ''), array('class' => 'datepicker')),
            'to_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
            'currency_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Currency'), 'add_empty' => false)),
        ));

        $this->setValidators(array(
            'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')),
            'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')),
            'currency_id' => new sfValidatorDoctrineChoice(array('required' => true, 'model' => $this->getRelatedModelName('Currency'), 'column' => 'id')),
        ));

        $this->widgetSchema->setNameFormat(InvoiceSaleReport::BASE_FILTER_NAME_FORMAT . '[%s]');
    }

    /**
     * Below methods are used by symfony filter mechanisms which automatically 
     * adds query sections to final filter query (based on sfFormFilterDoctrine.class.php).
     */

    /**
     * Add custom query from_date.
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addFromDateColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('r.created_at > ?', $value);
        }
    }

    /**
     * Add custom query to_date.
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addToDateColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('r.created_at < ?', $value);
        }
    }
}
