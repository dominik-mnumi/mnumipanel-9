<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ClientForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditClientGeneralForm extends ClientGeneralDataForm
{
    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $this->getWidget('fullname')->setAttribute('class', 'full-width');

        $this->getWidgetSchema()->setLabel('postcodeAndCity', 'Postcode and city');

        $this->useFields(array(
                'fullname',
                'street',
                'postcodeAndCity',
                $this->taxIdFormName,
                'account',
                $this->countryFormName,
                'wnt_ue',
            )
        );
    }
}

