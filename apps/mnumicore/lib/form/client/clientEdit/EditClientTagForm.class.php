<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended sfForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditClientTagForm extends BaseClientForm
{
    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
        
        $clientId = sfContext::getInstance()->getUser()->getAttribute('id', null, 'client');
        $clientObj = ClientTable::getInstance()->find($clientId);
        
        //gets string tag
        $tagString = $clientObj->getClientTagString();
   
        $this->setWidget('tagContainer', new sfWidgetFormInputHidden(
                array('default' => $tagString)));
        
        $this->getWidget('tagContainer')->setLabel('Please enter tags (format: tag1, tag2, ...)');
        
        $this->setValidator('tagContainer', new sfValidatorTagString(
                array('required' => false)));
    
        $this->useFields(array('tagContainer'));
    }
    
    public function save($con = null) 
    {
        $clientId = sfContext::getInstance()->getUser()->getAttribute('id', null, 'client');
        
        if(!$clientId)
        {
            throw new Exception('Invalid client id. Client id should be on session in client namespace.');
        }
        
        //gets clientObj
        $clientObj = ClientTable::getInstance()->find($clientId);
        
        $tagContainer = $this->getValue('tagContainer');
        
        //saves tags to database
        $clientObj->saveTagString($tagContainer);
        
        return $clientObj;
    }
    
   
 
}

