<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ClientSfGuardUserProfileForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */

class EditClientNewUserForm extends ClientUserForm
{

    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $this->getWidget('client_user_permission_id')->setAttribute('class', 'full-width');
        $this->getWidget('client_user_permission_id')->setOption('label', 'Permission');

        $this->useFields(array('user', 'client_user_permission_id',
            'user_id', 'client_id'));
    }

}

