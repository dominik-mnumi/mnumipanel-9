<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ClientGeneralDataForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class ClientGeneralDataForm extends ClientForm
{
    protected $taxIdFormName = 'tax_id';
    protected $countryFormName = 'country';

    public function configure()
    {
        parent::configure();

        $postcodeAndCityValidator = new sfValidatorCallback(
            array('callback' => array($this, 'postcodeAndCity'))
        );

        $taxValidator = new sfValidatorSchemaTaxId(
            $this->taxIdFormName,
            array('countryField' => $this->countryFormName),
            array('invalid' => 'Field "Tax ID" is not valid')
        );
//        new sfValidatorCallback(
//            array('callback' => array($this, 'taxId'))
//        );

        // sets post validators
        $this->mergePostValidator(new sfValidatorAnd(
            array($postcodeAndCityValidator, $taxValidator))
        );
    }

    /**
     * Validates postcode and city depending on country.
     *
     * @param sfValidator $validator
     * @param array $values
     * @return array
     * @throws sfValidatorErrorSchema
     */
    public function postcodeAndCity($validator, $values)
    {
        // gets error schema
        $errorSchema = new sfValidatorErrorSchema($validator);

        // prepares validator
        $postcodeAndValidatorClass = 'sfValidatorPostcodeAndCity'.$values[$this->countryFormName];

        // checks if class exists (if not then sets default class)
        if(!class_exists($postcodeAndValidatorClass))
        {
            $postcodeAndValidatorClass = 'sfValidatorPostcodeAndCityGB';
        }

        // sets validator
        $this->setValidator('postcodeAndCity', new $postcodeAndValidatorClass(
            array('max_length' => 150,
                'required' => true)));

        // do validator clean
        try
        {
            $values['postcodeAndCity'] =  $this->validatorSchema['postcodeAndCity']
                ->clean($values['postcodeAndCity']);
        }
        catch(sfValidatorError $e)
        {
            $errorSchema->addError($e, 'postcodeAndCity');
        }

        // if some errors
        if($errorSchema->count())
        {
            throw $errorSchema;
        }

        return $values;
    }
}
