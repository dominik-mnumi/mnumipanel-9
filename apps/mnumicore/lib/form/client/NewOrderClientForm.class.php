<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * New order form from client interface.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class NewOrderClientForm extends BaseOrderForm
{
    public function configure()
    {        
        $clientObj = $this->getOption('clientObj');
        $userId = $this->getOption('userId');
        
        $this->setWidget('user_id', new sfWidgetFormChoice(
                array('choices' => $clientObj->getClientUserChoiceArray(),
                    'default' => $userId),
                array('class' => 'full-width')));

        $this->useFields(array('user_id'));
    } 
}

