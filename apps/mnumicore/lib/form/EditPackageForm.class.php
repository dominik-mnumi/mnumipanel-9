<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended OrderPackageForm.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditPackageForm extends OrderPackageForm
{
    public function configure()
    {        
        parent::configure();

        $this->getWidgetSchema()->setNameFormat('EditPackageForm[%s]');

        // widgets
        $this->setWidget('client_id', new sfWidgetFormInputHidden());
        $this->setWidget('delivery_name', new sfWidgetFormInput(
                array('default' => $this->getObject()->getClientAddress()->getFullname()),
                array('class' => 'full-width'))); 

        $this->getWidget('want_invoice')->setLabel('I want invoice');
        $this->getWidget('want_invoice')->setAttribute('class', 'want-invoice-checkbox');
        
        $this->setMessages('required');

        // this validators is checked in post validator
        $this->setValidator('delivery_street', new sfValidatorPass());
        $this->setValidator('postcodeAndCity', new sfValidatorPass());


        // post validators
        $validatorPayment = new sfValidatorCallback(array(
                'callback' => array($this, 'validPayment'))
        );
        $validatorDelivery = new sfValidatorCallback(array(
                'callback' => array($this, 'validDelivery'))
        );

        $this->validatorSchema->setPostValidator(
            new sfValidatorAnd(array($validatorPayment, $validatorDelivery))
        );
        
        $this->useFields(array('payment_id', 'client_address_id', 'carrier_id',
            'delivery_name', 'delivery_street', 'postcodeAndCity', 'description',
            'want_invoice', 'delivery_country', 'invoice_name', 'invoice_street',
            'invoice_city', 'invoice_country', 'invoice_postcode', 'invoice_tax_id',
            'delivery_at'
        ));
    }
    
    public function save($conn = null)
    {
        $packageObj = $this->updateObject();

        // if existent client address
        $clientAddressObj = $this->getValue('client_address_id')       
            ? ClientAddressTable::getInstance()->find($this->getValue('client_address_id'))
            : new ClientAddress();

        // client address section
        $clientAddressObj->setClient($packageObj->getClient());
        
        // if new get from delivery_name input
        if($clientAddressObj->isNew())
        {
            $clientAddressObj->setFullname($this->getValue('delivery_name'));
        }
        
        $clientAddressObj->setCountry($this->getValue('delivery_country'));
        $clientAddressObj->setPostcodeAndCity($this->getValue('postcodeAndCity'));
        $clientAddressObj->setStreet($this->getValue('delivery_street'));
        
        // package section
        $packageObj->setDeliveryPostcodeAndCity($this->getValue('postcodeAndCity'));
        $packageObj->setClientAddress($clientAddressObj);
        
        parent::save($conn);
        
        return $packageObj;
    }

    /**
     * Checks if current payment is valid depending on current carrier.
     *
     * @param sfValidatorBase $validator
     * @param array $values
     * @throws sfValidatorErrorSchema
     * @return array
     */
    public function validPayment(sfValidatorBase $validator, $values)
    {
        $paymentColl = $this->getObject()->getClient()->getAvailablePayments($values['carrier_id']);

        $paymentIdArray = array();
        foreach($paymentColl as $rec)
        {
            $paymentIdArray[] = $rec->getId();
        }

        if(!in_array($values['payment_id'], $paymentIdArray))
        {
            $error = new sfValidatorError($validator, 'Wrong payment method');
            throw new sfValidatorErrorSchema($validator, array('payment_id' => $error));
        }

        return $values;
    }

    /**
     * Check if carrier require delivery, all delivery field is valid
     *
     * @param sfValidatorBase $validator
     * @param array $values
     * @throws sfValidatorErrorSchema
     * @return array
     */
    public function validDelivery(sfValidatorBase $validator, $values)
    {
        /** @var Carrier $carrier */
        $carrier = CarrierTable::getInstance()->find($values['carrier_id']);

        if(!$carrier)
        {
            $error = new sfValidatorError($validator, 'Missing carrier');
            throw new sfValidatorErrorSchema($validator, array('carrier_id' => $error));
        }


        if($carrier->getRequireShipmentData() == false)
        {
            $values['delivery_name'] = null;
            $values['delivery_street'] = null;
            $values['postcodeAndCity'] = null;

            return $values;
        }

        // delivery_name validator
        try
        {
            $validator = new sfValidatorString(array( 'required' => true));
            $values['delivery_name'] = $validator->clean($values['delivery_name']);
        }
        catch(sfValidatorError $e)
        {
            throw new sfValidatorErrorSchema($validator, array('delivery_name' => $e));
        }

        // delivery_street validator
        try
        {
            $validator = new sfValidatorString(array( 'required' => true));
            $values['delivery_street'] = $validator->clean($values['delivery_street']);
        }
        catch(sfValidatorError $e)
        {
            throw new sfValidatorErrorSchema($validator, array('delivery_street' => $e));
        }

        // postcodeAndCity validator
        try
        {
            $validator = new sfValidatorPostcodeAndCity(array( 'max_length' => 150, 'required' => true));
            $values['postcodeAndCity'] = $validator->clean($values['postcodeAndCity']);
        }
        catch(sfValidatorError $e)
        {
            throw new sfValidatorErrorSchema($validator, array('postcodeAndCity' => $e));
        }

        return $values;
    }
    
}

