<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * DoctrineProductCalculationProductData class
 * Prepare CalculationProductData based on Doctrine 1.x Product entity
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class DoctrineProductCalculationProductData 
{
    /** @var \Product Product */
    private $product;

    /**
     * Consctructor
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return CalculationProductData
     */
    public function getCalculationProductDataObject()
    {
        $defaultPriceListId = PricelistTable::getInstance()->getDefaultPricelist()->getId();
        $emptyFieldId = FieldItemTable::getInstance()->getEmptyField()->getId();
        $otherFinishLabel = FieldsetTable::getInstance()->findOneByName('OTHER')->getLabel();

        $defaultFields = $this->getProduct()->getProductFieldsetsForPriceDefaultArray();
        $calculationProductData = new \Mnumi\Bundle\CalculationBundle\Library\CalculationProductData(
            $defaultFields,
            $defaultPriceListId,
            $emptyFieldId,
            $otherFinishLabel,
            (float) $this->getProduct()->getTax() / 100
        );

        $calculationProductData = $this->prepareFields($calculationProductData);
        $calculationProductData = $this->prepareFixedPrice($calculationProductData);

        return $calculationProductData;
    }

    private function prepareFields($calculationProductData)
    {
//        $otherId = 0;

        /** @var ProductField $field */
        foreach ($this->getProduct()->getProductFields() as $field) {
            $type = $field->getFieldset()->getName();

            if(in_array($type, array('GENERAL', 'FIXEDPRICE', 'WIZARD'))) {
                continue;
            }

            $label = $field->getFieldsetLabel();

            $fieldType = ($type == 'OTHER')
                ? 'OTHER_' . $field->getId()
                : $type;

            $calculationField = new \Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationField($fieldType, $label, !$field->getVisible());
            $calculationProductData->addField($calculationField);

            if (in_array($type, array('COUNT', 'QUANTITY'))) {
                continue;
            }

            if($type == 'OTHER') {
                $productFieldItems = $this->getProduct()->getProductFieldById($field->getId())->getProductFieldItems();
//                $otherId++;
            } else {
                $productFieldItems = $this->getProduct()->getProductFieldByFieldsetName($type)->getProductFieldItems();
            }

            /** @var ProductFieldItem $productItem */
            foreach($productFieldItems as $productItem) {
                $fieldItem = $productItem->getFieldItem();

                $calculationFieldItem = new \Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationFieldItem(
                    $fieldItem->getId(),
                    $fieldItem->getName(),
                    $fieldItem->getLabel(),
                    $fieldItem->getCost(),
                    $fieldItem->getMeasureUnit()->getName()
                );

                $calculationField->add($calculationFieldItem);

                if ($type == 'SIZE') {
                    /** @var FieldItemSize $fieldItemSize */
                    $fieldItemSize = $fieldItem->getFieldItemSize()->getFirst();

                    if(!$fieldItemSize) {
                        $fieldItemSize = $fieldItem->getFieldItemSize()->getLast();
                    }

                    if ($fieldItemSize) {
                        $calculationFieldItemSize = new \Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationFieldItemSize(
                            $fieldItemSize->getWidth(),
                            $fieldItemSize->getHeight()
                        );

                        $calculationFieldItem->setItemSize($calculationFieldItemSize);
                    }
                }

                if ($type == 'MATERIAL') {
                    /** @var FieldItemMaterial $fieldItemMaterial */
                    $fieldItemMaterial = $fieldItem->getFieldItemMaterial()->getFirst();

                    if ($fieldItemMaterial) {
                        $printSize = $fieldItemMaterial->getPrintsize();

                        if(!$printSize instanceof Printsize)
                        {
                            throw new Exception('Print Size obj object does not exist');
                        }

                        $calculationPrintSize = new \Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationPrintSize(
                            $printSize->getWidth(),
                            $printSize->getHeight(),
                            $printSize->getId()
                        );

                        $calculationFieldItem->setPrintSize($calculationPrintSize);
                    }
                }

                /** @var FieldItemPrice $fieldItemPrice */
                foreach($fieldItem->getFieldItemPrices() as $fieldItemPrice) {
                    $calculationFieldCollection = new \Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationFieldItemPriceCollection(
                        $fieldItemPrice->getPricelistId(),
                        $fieldItemPrice->getMinimalPrice(),
                        $fieldItemPrice->getMaximalPrice(),
                        $fieldItemPrice->getPlusPrice(),
                        $fieldItemPrice->getPlusPriceDouble(),
                        $fieldItemPrice->getCost()
                    );
                    $calculationFieldItem->add($calculationFieldCollection);

                    /** @var FieldItemPriceQuantity $quantity */
                    foreach($fieldItemPrice->getFieldItemPriceQuantities() as $quantity) {
                        $calculationFieldCollection->addPrice(new \Mnumi\Bundle\CalculationBundle\Library\FieldItem\CalculationFieldItemPrice(
                                $quantity->getFieldItemPriceTypeName(),
                                $quantity->getQuantity(),
                                $quantity->getPrice(),
                                $quantity->getCost(),
                                $quantity->getType()
                            )
                        );
                    }
                }
            }
        }

        return $calculationProductData;
    }

    private function prepareFixedPrice($calculationProductData)
    {
        $priceListCollection = new \Mnumi\Bundle\CalculationBundle\Library\PriceListFixPriceCollection();
        $calculationProductData->setPriceListFixPriceCollection($priceListCollection);

        /** @var ProductFixprice $price */
        foreach ($this->getProduct()->getProductFixprices() as $price) {
            $priceListId = $price->getPricelistId();

            if ($priceListCollection->contains($priceListId)) {
                $priceListFixPrice = $priceListCollection->get($priceListId);
            } else {
                $priceListFixPrice = new \Mnumi\Bundle\CalculationBundle\Library\PriceListFixPrice($priceListId);
                $priceListCollection->add($priceListFixPrice);
            }

            $priceListFixPrice->add(
                new \Mnumi\Bundle\CalculationBundle\Library\FixPrice($price->getQuantity(), $price->getPrice(), $price->getCost())
            );
        }

        return $calculationProductData;
    }

    /**
     * @return \Product
     */
    protected function getProduct()
    {
        return $this->product;
    }

}