<?php

/**
 * Ajax form.
 *
 * @package    mnumicore
 * @author     Marek Balicki
 */
class AjaxForm
{
    public $request;
    
    // GET params
    // module from reffered
    public $module;
    
    // model class name
    public $model;
    
    // type - new or edit
    public $type;
    
    // optional - if there are reasons that cannot be xmlRequest
    public $forcePost;
    
    // Form name depending on POST array name
    public $formName;
    
    public $paramArray = array();
    
    /**
     * Main method. Sets request object in current class.
     * Mandatory. 
     * 
     * This method has $request parameter.
     * Mandatory params (in GET):
     * - module - from which module action is referred,
     * - model - what kind of object should create (model class),
     * - type - object is new or edit,
     * - forcePost (optional) - if from some reasons request cannot be in xmlRequest.
     * 
     * Moreover $request should have POST request.
     * 
     * @param sfWebRequest $request 
     */
    public function setRequest(sfWebRequest $request)
    {
        $this->request = $request;
        
        $this->module = $this->request->getParameter('module', null);
        $this->model = $this->request->getParameter('model', null);
        $this->type = $this->request->getParameter('type', null);
        $this->forcePost = $this->request->getParameter('forcePost', null); 
      
        if(!$this->module)
        {
            throw new Exception('Module parameter undefined.');    
        }
        
        if(!$this->model)
        {
            throw new Exception('Model parameter undefined.');    
        }
        
        if(!$this->type)
        {
            throw new Exception('Type parameter undefined.');    
        }
        
        $this->formName = $this->getFormName();
        $this->paramArray = $this->getParams();
    }
    
    /**
     * Returns array of files depending on $request and form name.
     *  
     * @return array $fileArray
     */
    public function getFiles()
    {
        if(empty($this->request))
        {
            throw Exception('Request is undefined.'); 
        }

        $fileArray = $this->request->getFiles($this->formName);
        
        return $fileArray;
    }
    
    /**
     * Returns form which is created on existing $request.
     * 
     * @return sfFormDoctrine $form 
     */
    public function createForm()
    {
        //creates form
        if ('new' == $this->type)
        {
            $form = new $this->formName();
        }
        //edit mode
        else
        {
            //gets model object
            $modelTable = $this->model.'Table';
            $modelObj = $modelTable::getInstance()->find($this->paramArray['id']);

            //creates loaded form object
            $form = new $this->formName($modelObj);
        }
        
        return $form;
    }
    
    /**
     * Returns form class name depending on $request.
     * 
     * @return string $formName
     */
    public function getFormName()
    {
        if(empty($this->request))
        {
            throw Exception('Request is undefined.'); 
        }
        
        $paramArray = $this->request->getPostParameters();

        //gets form name in array
        $tmp = array_keys($paramArray);

        //gets form name
        $formName = array_pop($tmp);
        
        return $formName;
    }
   
    /**
     * Returns array of parameters depending on $request and form name.
     * 
     * @return array $paramArray 
     */
    public function getParams()
    {
        if(empty($this->request))
        {
            throw Exception('Request is undefined.'); 
        }
        
        $paramArray = $this->request->getPostParameters();

        $paramArray = $paramArray[$this->formName];
        
        return $paramArray;
    }
}
