<script type="text/javascript" charset="utf-8">
var delay = (function()
{
    var timer = 0;
    return function(callback, ms)
    {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();    
    
(function($) 
{
    hideAutocompleteList = function()
    {
        $('#search-result').fadeOut('slow', function()
        {
            $('#search-list').html('');   
        });
    }
    
    autocompleteInit = function(url)
    {
        $('#search_phrase').keyup(function(e)
        {
            if(e.keyCode == 27) 
            { 
                hideAutocompleteList();              
                return false;
            }   
            
            delay(function()
            {           
                if ($('#search_phrase').val() != '')
                {
                    $.ajax(
                    {
                        type: 'POST',
                        url: url,
                        data: $('#search-form').serialize(),
                        dataType: 'json',
                        success: function(dataOutput)
                        {
                            $('#search-list').html('');
                            if(dataOutput)
                            {
                                $('#search-result').css('display', 'block');
                                for (var item in dataOutput)
                                {
                                    var name = dataOutput[item]['name'] + ' <small>[' + dataOutput[item]['id'] + ']</small>';
                                    var anchor = $('<a/>')
                                            .attr('href', dataOutput[item]['url'])
                                            .html(name);
                                    var li = $('<li/>')
                                            .addClass(dataOutput[item]['type'])
                                            .append(anchor);

                                    $('#search-list').append(li);
                                }
                            }
                            else
                            {
                                $('#search-result').fadeOut('slow', function ()
                                {
                                    $('#search-list').html('');   
                                });
                            }

                        }
                    });
                }
                else
                {
                    $('#search-result').fadeOut('slow', function ()
                    {
                        $('#search-list').html('');   
                    });
                }
            }, 300);
        });
        
        $('#search_phrase').focusout(function()
        {
            hideAutocompleteList();
        });
    }
})(jQuery);

</script>
