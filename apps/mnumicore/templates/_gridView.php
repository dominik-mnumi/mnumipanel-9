<div class="with-head no-margin">
    <div class="head">
        <div class="black-cell with-gap"><span class="success"></span><span class="loading" style="display: none;"></span></div>
        <div class="black-cell"><?php echo __('Sort by'); ?></div>
        
        <?php foreach($fields as  $field => $option) : ?>
        <div>
            <?php $sort = '' ?>
            <?php foreach($sortableFields as $sortField): ?>
            <?php if(strtolower($field) == str_replace('_', '',$sortField)) $sort = $sortField ?>
            <?php endforeach ?>
            <?php if($sort): ?>
            <span class="column-sort">
                <a href="#" title="<?php echo __('Sort in descending order') ?>" class="sort sort-up" rel="<?php echo $sort ?>" type="ASC"></a>
                <a href="#" title="<?php echo __('Sort in ascending order') ?>" class="sort sort-down" rel="<?php echo $sort ?>" type="DESC"></a>
            </span>
            <?php if(isset($option['label'])) : ?>
            <?php echo __($option['label']) ?>
            <?php else : ?>
            <?php echo __(ucfirst(str_replace('_', ' ', $field))) ?>
            <?php endif ?>
            <?php endif ?>
        </div>
        <?php endforeach ?>
        
    </div>
    <ul class="grid dark-grey-gradient">
        <?php foreach($pager as $row): ?>
        <li>
            <p class="grid-name">
                <?php if(@$settings['photo']): ?>
                <img alt="" src="<?php echo $row->getPhotoWithPath(); ?>" />
                <?php endif ?>
                <?php foreach($fields as $field => $option): ?>               
                <?php if(@$option['destination'] == 'name') : ?>
                <?php if(!empty($option['partial'])): ?>
                    <?php include_partial($option['partial'], array('row' => $row, 'fields' => $fields, 'settings' => $settings)) ?>
                <?php else: ?>
                    <?php $name = 'get'.$field ?>
                    <?php if(isset($option['clickable'])) : ?>
                    <a href="<?php echo url_for($settings['editRoute'],$row) ?>"><?php echo $row->$name(); ?></a>
                    <?php else : ?>
                    <?php echo $row->$name(); ?>
                    <?php endif ?>
                    <?php endif ?>
                <?php endif ?>
                <?php endforeach ?>
            </p>
            <ul class="keywords">
                <?php foreach($fields as $field => $option) : ?>
                <?php if(@$option['destination'] == 'keywords') : ?>
                <?php $keywords = 'get'.$field ?>
                <li class="purple-keyword">
                    <?php echo $row->$keywords() ?>
                </li>
                <?php endif ?>
                <?php endforeach ?>
            </ul>
            <p class="grid-details"></p>
            
            <ul class="grid-actions">
                <?php if(!empty($settings['actions'])): ?>
                <?php foreach ($settings['actions'] as $key => $action): ?>
                <?php if(isset($action['manyRoute']) && ($action['manyRoute'] != '')) continue; // ignore, when it's global action ?>
                <?php if(isset($action['partial']) && $action['partial']): ?>                
                <li><?php include_partial($action['partial'], array('row' => $row)) ?></li>
                <?php elseif($key == 'delete' && method_exists($settings['class_name'], 'canDelete') && !$row->canDelete($sf_user)): continue; ?>  
                <?php else: ?>
                <li>
                    <?php $icon_tag = image_tag($action['icon'], array('width' => 16, 'height' => 16)); ?>
                    <?php $attributes = isset($action['attributes']) ? $action['attributes']->getRawValue() : array(); ?>
                    <?php isset($attributes['class']) ?  $attributes['class'] .= ' ' .$key : $attributes['class'] = $key; ?>
                    <?php 
                    if(array_key_exists('title', $attributes))
                    {
                        $attributes['title'] = __($attributes['title']);
                    }
                    ?>
                    <?php echo link_to($icon_tag, $action['route'], $row, $attributes); ?>
                </li>
                <?php endif ?>
                <?php endforeach; ?>
                <?php endif; ?>
                <li><input type="checkbox" name="selected[]" id="grid-selected-1" value="<?php echo $row->getPrimaryKey() ?>"></li>
            </ul>
        </li>
        <?php endforeach ?>
    </ul>
</div>
