<?php
/** @var ChangeGroup $diff */
foreach($differences as $diff):
?>
    <h2>
        <?php echo (string) $diff->getSfGuardUser(); ?>,
        <?php echo __('at'); ?> <?php echo $diff->getUpdatedAt('Y-m-d H:i:s'); ?>
    </h2>
    <table class="table full-width">
        <thead>
        <tr>
            <th><?php echo __('Field'); ?></th>
            <th><?php echo ucfirst(__('of')); ?></th>
            <th><?php echo ucfirst(__('for')); ?></th>
        </tr>
        </thead>
        <?php
        /** @var ChangeItem $change */
        foreach($diff->getChangeItems() as $change):
            ?>
            <tr>
                <td><b><?php echo __($change->getField()); ?></b></td>
                <td><?php echo __($change->getOldValue()); ?></td>
                <td><?php echo __($change->getNewValue()); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

<?php endforeach; ?>
