<?php echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<items version="<?php echo sfConfig::get('app_version'); ?>" buildtime="<?php echo $buildTime; ?>" title="<?php echo __($title); ?>">
    <?php foreach($items as $item): ?>
    <item>
        <?php foreach($item as $key => $value): ?>
            <<?php echo $key?>><?php echo $value ?></<?php echo $key?>>
        <?php endforeach ?>
    </item>
    <?php endforeach; ?>
</items>
