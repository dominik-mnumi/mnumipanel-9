<?php $sortableFields = $sortableFields->getRawValue(); ?>
<?php $layoutOptions = $layoutOptions->getRawValue(); ?>

<table id="<?php echo $settings['class_name']; ?>TableView" class="table sortable no-margin" cellspacing="0" width="100%">
    <thead>
        <tr>
            <?php if($layoutOptions['showCheckboxes']): ?>
            <th class="black-cell sorting_disabled" style="width: 16px; ">
                <span class="success"></span>
                <span class="loading" style="display: none;"></span>
            </th>
            <?php endif; ?>
            <?php foreach($fields as  $field => $option): ?>          
            <th scope="col" class="sorting <?php if(isset($option['class'])){ echo $option['class']; } ?>">                
                <?php $sort ='' ?>
                <?php foreach($sortableFields as $sortField): ?>
                <?php if(strtolower($field) == str_replace('_', '', $sortField)) $sort = $sortField ?>
                <?php endforeach ?>
                <?php if($sort): ?>
                <span class="column-sort">
                    <a href="#" title="<?php echo __('Sort in descending order') ?>" class="sort sort-up" rel="<?php echo $sort ?>" type="ASC"></a>
                    <a href="#" title="<?php echo __('Sort in ascending order') ?>" class="sort sort-down" rel="<?php echo $sort ?>" type="DESC"></a>
                </span>
                <?php endif ?>
                <?php if(isset($option['label'])): ?>
                <?php echo __($option['label']); ?>
                <?php else: ?>
                <?php echo __(ucfirst(str_replace('_', ' ', $field))); ?>
                <?php endif; ?>
            </th>
            <?php endforeach; ?>
            <?php if(count($settings['actions']) > 0): ?>
            <th scope="col" class="table-actions sorting_disabled" style="width: 94px; ">
                <?php echo __('Actions'); ?>
            </th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0 ?>

        <?php foreach($pager as $rowKey => $row) : ?>
        <?php $i++;?>
        <tr class="<?php echo ($i%2) ? 'odd' : 'even' ?>">
            <?php if($layoutOptions['showCheckboxes']): ?>
            <td class="th table-check-cell sorting_1">
                <input type="checkbox" name="selected[]" id="table-selected-<?php echo $i ?>" value="<?php echo $row->getPrimaryKey() ?>">
            </td>
            <?php endif; ?>
            <?php foreach($fields as  $field => $option) : ?>
            <?php $rowTmp = $row; ?>

            <?php if($field == 'PhotoWithPath'): ?>   
            <td class="<?php echo $field; ?>">
                <img alt="" src="<?php echo $row->getPhotoWithPath(); ?>" />
            </td>       
            <?php elseif(!isset($option['partial'])): ?>
            <?php if(isset($option['relation'])): ?>
            <?php $string = 'get'.$option['relation']; ?>
            <?php $rowTmp = $row->$string(); ?>
            <?php endif; ?>
            <?php $get = 'get'.$field ?>
            <?php if(isset($option['clickable'])) : ?>
            <td class="<?php echo $field; ?>">
                <a href="<?php echo url_for($settings['actions']['edit']['route'], $rowTmp); ?>">
                    <?php if(isset($option['translate'])): ?>
                    <?php echo __($rowTmp->$get()); ?>
                    <?php else: ?>
                    <?php echo $rowTmp->$get(); ?>
                    <?php endif; ?>
                </a>
            </td>
            <?php else: ?>
            <td class="<?php echo $field; ?>">
                <?php if(isset($option['translate'])): ?>
                   <?php echo __($rowTmp->$get()) ?>
                <?php else: ?>
                    <?php echo $rowTmp->$get() ?>
                <?php endif ?>
            </td>
            <?php endif; ?>      
            <?php else: ?>
            <td class="<?php echo $field; ?>">
                <?php include_partial($option['partial'], array('row' => $row, 'key' => $rowKey, 'attributes' => isset($option['attributes']) ? $option['attributes'] : array())); ?>
            </td>
            <?php endif; ?>
            <?php endforeach ?>
            <?php if(count($settings['actions']) > 0): ?>
            <td class="table-actions">
                <?php foreach($settings['actions'] as $key => $action): ?>
                <?php if(isset($action['manyRoute']) && ($action['manyRoute'] != '')) continue; // ignore, when it's global action ?>
                <?php if(isset($action['partial']) && $action['partial']): ?>
                <?php include_partial($action['partial'], array('row' => $row)); ?>
                <?php else: ?>
                <?php if(($key == 'delete') && (method_exists($settings['class_name'], 'canDelete') && ($row->canDelete($sf_user) == false))) continue; ?>
                <?php if(($key == 'edit') && (method_exists($settings['class_name'], 'canEdit') && !$row->canEdit($sf_user))) continue; ?>
                <?php 
                    $icon_tag = image_tag($action['icon'], array('width' => 16, 'height' => 16));
                    $attributes = isset($action['attributes']) ? $action['attributes']->getRawValue() : array(); 
                    if(array_key_exists('title', $attributes))
                    {
                        $attributes['title'] = __($attributes['title']);
                    }                   
                    isset($attributes['class']) ?  $attributes['class'] .= ' ' .$key : $attributes['class'] = $key;
                    echo link_to($icon_tag, $action['route'], $row, $attributes); ?>
                <?php endif; ?>
                <?php endforeach; ?>
            </td>
            <?php endif; ?>
        </tr>
        <?php endforeach ?>
    </tbody>      
</table>
