<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <?php include_title() ?>
        <link rel="shortcut icon" type="image/x-icon"  href="/images/icons/favicon.ico" />
        <?php include_stylesheets() ?>
        <?php include_javascripts() ?>

        <?php if(has_slot('actionJavascript')): ?>
        <?php include_slot('actionJavascript') ?>
        <?php endif; ?>
    </head>
    <body>
    <?php if($sf_user->isAuthenticated()): ?>
    <script type="text/javascript">
        var barcodePrefix = '<?php echo sfConfig::get('app_barcode_prefix', '^') ?>';
        var barcodeSufix = '<?php echo sfConfig::get('app_barcode_sufix', '#') ?>';
        var barcodeSize = <?php echo sfConfig::get('app_barcode_size', '8') ?>;
        var barcodeRead = '<?php echo url_for('@barcodeRead') ?>';

        var devenv =  <?php echo (sfConfig::get('sf_environment') == 'dev') ? 'true' : 'false'; ?>;

        /**
         * Pings to server.
         */
        function ping()
        {
            $.ajax(
            {
                type: 'GET',
                url: '<?php echo url_for('ping'); ?>',
                error: function(xhr, ajaxOptions, thrownError)
                {
                    alert('<?php echo __('Failed attempt to connect to the server. Please check your internet connection.'); ?>');
                }
            });
        }

        setInterval(ping, 5 * 60000);
    </script>
    <script type="text/javascript" src="/js/barcode-listener.js"></script>

    <?php if(substr($sf_user->getCulture(), 0, 2) != 'en'): ?>
        <script type="text/javascript" src="/js/jquery-datepicker-<?php echo substr($sf_user->getCulture(), 0, 2) ?>.js"></script>
    <?php endif; ?>
    
    <?php endif ?>
        <!--[if lt IE 9]><div class="ie"><![endif]-->
        <!--[if lt IE 8]><div class="ie7"><![endif]-->

        <!-- Server status -->
        <header>
            <div class="container_12">
                <p id="skin-name"><small>Mnumisystem<br /> Admin PANEL</small> <strong title="<?php echo ProjectConfiguration::getRepositoryVersion(); ?>"><?php echo ProjectConfiguration::getVersion(); ?></strong></p>
            </div>
        </header>
        <!-- End server status -->
        <!-- Main nav -->
        <nav id="main-nav">
            <?php echo include_component('dashboard', 'mainNavi'); ?>
        </nav>
        <!-- End main nav -->

        <!-- Sub nav -->
        <div id="sub-nav">
            <div class="container_12">
                &nbsp;
                <?php echo include_component('search', 'form'); ?>
            </div>
        </div>
        <!-- End sub nav -->

        <?php echo include_component('dashboard', 'statusBar'); ?>

        <div id="sf_content">
            <?php echo $sf_content ?>
        </div>

        <!--[if lt IE 8]></div><![endif]-->
        <!--[if lt IE 9]></div><![endif]-->
        <footer>

        <div class="float-right">
            <a class="button" href="#top">
                <?php echo image_tag('icons/fugue/navigation-090.png', array('style' => 'width: 16px; height: 16px;')) ?> <?php echo __('Page top') ?>
            </a>
        </div>
        
</footer>

    <?php if($sf_user->isAuthenticated()): ?>
            <script>
            UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/SnKAUeINVfUHsvpCoEyXRw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

            // Set colors
            UserVoice.push(['set', {
                accent_color: '#448dd6',
                trigger_color: 'white',
                trigger_background_color: '#448dd6'
            }]);

            // Identify the user and pass traits
            // To enable, replace sample data with actual user traits and uncomment the line
            UserVoice.push(['identify', {
                email:      '<?php echo $sf_user->getGuardUser()->getEmailAddress(); ?>', // User’s email address
                name:       '<?php echo $sf_user->getGuardUser()->__toString(); ?>', // User’s real name
                //created_at: 1364406966, // Unix timestamp for the date the user signed up
                id:         <?php echo $sf_user->getGuardUser()->getId(); ?>, // Optional: Unique id of the user (if set, this should not change)
                type:       'Customer', // Optional: segment your users by type
                account: {
                  id:           '<?php echo sfConfig::get('app_company_data_seller_tax_id'); ?>',
                  name:         '<?php echo sfConfig::get('app_company_data_seller_name'); ?>'
                }
            }]);

            UserVoice.push(['addTrigger', { mode: 'smartvote', trigger_position: 'bottom-left' }]);
            UserVoice.push(['autoprompt', {}]);
        </script>
    <?php endif; ?>
    </body>
</html>
