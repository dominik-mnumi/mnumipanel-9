<?php $settings = $settings->getRawValue() ?>

<script type ="text/javascript">
$(document).ready(function()
{
    /**
     * Services classes for favourite orders.
     */
    function serviceFavouriteOrderStar(selector)
    {
        if($(selector).hasClass('favourite-order-inactive'))
        {
            $(selector).removeClass('favourite-order-inactive');
            $(selector).addClass('favourite-order-active');
        }
        else
        {
            $(selector).removeClass('favourite-order-active');
            $(selector).addClass('favourite-order-inactive');
        }
    }
    
    /**
     * Shows loader for order favourite.
     */
    function showFavouriteOrderLoader(selector)
    {
        $(selector).addClass('favourite-order-loader');
    }
    
    /**
     * Hides loader for order favourite.
     */
    function hideFavouriteOrderLoader(selector)
    {
        $(selector).removeClass('favourite-order-loader');
    }
    
    function showLoading()
    {
        $('#table<?php echo $uid ?> .success').hide();
        $('#table<?php echo $uid ?> .loading').show();
    }

    $('#table<?php echo $uid ?>').on('keypress','#searchWord', function(e)
    {
        if(e.which == 13)
        {
                showLoading();
            $.ajax({
                url: "<?php echo url_for($settings['route'], $settings['routeParams']) ?>",
                type: "GET",
                data: 'search='+$(this).val(),
                success: function(html){
                    $("#table<?php echo $uid ?>").html(html);
                }
            });
            return false;
        }
    });
		
    // filter form click
    $('#filterBtn').unbind('click');
    $('.search-div .form').on('click','#filterBtn', function()
    {
        $.ajax({
            url: "<?php echo url_for($settings['route'], $settings['routeParams']) ?>",
            type: "GET",
            data: $('form[name*="filters"]').serialize(),
            success: function(html){
                $("#table<?php echo $uid ?>").html(html);
            }
        });
    }); 	
	// change records per page
	$('#table<?php echo $uid ?>').on('change', '#table-display', function() {
		showLoading();
		$.ajax({
            url: "<?php echo url_for($settings['route'], $settings['routeParams']) ?>",
            type: "GET",
            data: 'tableView='+$("#table<?php echo $uid ?> #table-display").val(),
            success: function(html){
                $("#table<?php echo $uid ?>").html(html);
            }
        });
        return false;
	});

	// change sorting
    $('#table<?php echo $uid ?>').on('click', '.sort', function() {
        showLoading();
        $.ajax({
            url: "<?php echo url_for($settings['route'], $settings['routeParams']) ?>",
            type: "GET",
            data: 'sortBy='+$(this).attr('rel')+'&type='+$(this).attr('type'),
            success: function(html){
                $("#table<?php echo $uid ?>").html(html);
            }
        });
        return false;
    });

    // select all
    $('#table<?php echo $uid ?>').on('click', '#selectAll', function() 
    {
        $("input[name=\"selected[]\"]").each(function()
        {
            this.checked = 1;
        });
        
        $(this).hide();
        $("#deselectAll").show();
        return false;
    });
    
    // unselect all
    $('#table<?php echo $uid ?>').on('click','#deselectAll', function() 
    {
        $("input[name=\"selected[]\"]").each(function()
        {
            this.checked = 0;
        });
        
        $(this).hide();
        $("#selectAll").show();
        return false;
    });
    
	// delete many rows
    <?php if($settings['manyActionAvailable']): ?>
    $('#table<?php echo $uid ?>').on('click', '#submitItems', function() 
    {
        var tableActionSelect = $('#table<?php echo $uid; ?> #table-action');
        var manyItemsForm = $('#table<?php echo $uid; ?> #manyItems');

        /* Ignore button click if no choice has been set */
        if(tableActionSelect.val() == '')
        {
            return false;
        }

        <?php foreach($settings['routeParams'] as $k => $v): ?>
            manyItemsForm.prepend('<input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>" />');
        <?php endforeach; ?>

        // first remove filters from previous request
        manyItemsForm.find('.table .table-filters-hidden').remove();

        // prepares filter paramters
        var filterArr = $("#simple_form").serializeArray();
        $.each(filterArr, function(i, field)
        {
            manyItemsForm.prepend('<input class="table-filters-hidden" type="hidden" name="' + field.name + '" value="' + field.value + '" />');
        });

        <?php foreach($settings['actions'] as $actionName => $action): ?>
            <?php if (isset($action['manyRoute'])): ?>
            if(tableActionSelect.val() == '<?php echo $actionName; ?>')
            {
                manyItemsForm.attr('action', '<?php echo url_for($action['manyRoute']); ?>');
            }
            <?php endif; ?>
        <?php endforeach; ?>
            
        manyItemsForm.submit();
         
        return false;
    });
    <?php endif; ?>
    
    //reloading
    $(document).ready(function(){
        function loadItems(pageNb)
        {
            if(pageNb)
            {
                var page = pageNb;
            }
            else
            {
                var page = $('#table<?php echo $uid ?> .controls-buttons .current').attr('rel');
            }
            $.ajax({
                url: "<?php echo url_for($settings['route'], $settings['routeParams']) ?>",
                type: "GET",
                data: "page="+page+'&'+$('form[name*="filters"]').serialize(),
                success: function(html){
                    $("#table<?php echo $uid ?>").html(html);
                }
            });
        }
        
        $('#table<?php echo $uid ?>').on('click', '.page', function() {
            showLoading();
            loadItems($(this).attr('rel'));
            return false;
        });
        $('#table<?php echo $uid ?>').on('click', '#reload', function() {
            showLoading();
            loadItems();
            return false;
        });
    });

    // change records per page
    $('#table<?php echo $uid ?>').on('change', '#perPage', function() {
        showLoading();
        $.ajax({
            url: "<?php echo url_for($settings['route'], $settings['routeParams']) ?>",
            type: "GET",
            data: $("#table<?php echo $uid ?> #perPageForm, form[name*=\"filters\"]").serialize(),
            success: function(html){
                $("#table<?php echo $uid ?>").html(html);
            }
        });
    });
	
    /**
     * Favourite orders.
     */
    $(".favourite-order-span").on('click', 
    function()
    {
        // gets clicked selector
        var clickedSel = this;
        
        if($(clickedSel).hasClass('favourite-order-loader'))
        {
            return false;
        }
        
        // shows loader
        showFavouriteOrderLoader(clickedSel);

         $.ajax(
         {
            url: $(clickedSel).attr('data-favourite-url'),
            type: "POST",
            dataType: 'json',
            data: { 'orderId': $(clickedSel).attr('data-order-id') },
            success: function(response)
            {
                if(response.result == 'success')
                {
                    hideFavouriteOrderLoader(clickedSel);
                    serviceFavouriteOrderStar(clickedSel);
                }
            }
        });

    });
});
</script>