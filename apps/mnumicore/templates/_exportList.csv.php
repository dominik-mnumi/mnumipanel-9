<?php 
$elements = count($columnsHeaders); $i = 1;
foreach($columnsHeaders as $header)
{
    echo __($header);
    if($i >= $elements)
    {
      echo '
';
    }
    else
    {
      echo ';';
    }
    $i++;
}
?>
<?php 
foreach($items as $item)
{
    $elements = count($item); $i = 1;
    foreach($item as $column)
    {
        echo htmlspecialchars_decode($column);
        if($i >= $elements)
        {
            echo '
';
        }
        else
        {
            echo ';';
        }
        $i++;
    }
}



