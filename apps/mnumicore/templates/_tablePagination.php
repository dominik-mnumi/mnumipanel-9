<?php if(isset($pager)) : ?>
    <?php if ($pager->haveToPaginate()): ?>
    <?php if($pager->getPage() > 1): ?>
        <li><a title="<?php echo __('Previous') ?>" href="#" class = "page" rel="<?php echo $pager->getPreviousPage() ?>"><img width="16" height="16" src="/images/icons/fugue/navigation-180.png"> <?php echo __('Prev') ?></a></li>
    <?php endif; ?>
    <?php $links = $pager->getLinks(); ?>
    <?php foreach ($links as $page): ?>
        <li><a class = "page <?php if($page == $pager->getPage()) echo ' current' ?>" title="<?php echo __('Page') ?> <?php echo $page; ?>" href="#" rel="<?php echo $page; ?>"><b><?php echo $page; ?></b></a></li>
    <?php endforeach ?> 
    <?php if($pager->getPage() < $pager->getLastPage()): ?>
        <li><a title="<?php echo __('Next') ?>" href="#" rel="<?php echo $pager->getNextPage() ?>" class = "page"> <?php echo __('Next') ?> <img width="16" height="16" src="/images/icons/fugue/navigation.png"></a></li>
    <?php endif; ?>
    <li class="sep"></li>
    <?php endif; ?>
    <li><a href="#" id="reload">&nbsp;<img width="16" height="16" src="/images/icons/fugue/arrow-circle.png">&nbsp;</a></li>
<?php endif; ?>