<?php $settings = $settings->getRawValue() ?>
<div class="no-margin last-child">
    <div class="block-controls">
        <div class="controls-buttons">
            <ul class="controls-buttons">
                <?php 
                include_partial('global/tablePagination', array(                     
                    'pager' => $pager 
                    ));
                ?>
            </ul>
        </div>
    </div>

    <form method="post" action="" id="manyItems" class="no-margin">
        <?php if($settings['tableView'] == 'table') : ?>
        <?php include_partial('global/tableView', array(
            'fields' => $fields, 
            'pager' => $pager, 
            'sortableFields' => $sortableFields, 
            'settings' => $settings,
            'layoutOptions' => $layoutOptions)) ?>
        <?php elseif($settings['tableView'] == 'grid') : ?>
        <?php include_partial('global/gridView', array(
            'fields' => $fields, 
            'pager' => $pager, 
            'sortableFields' => $sortableFields, 
            'settings' => $settings,
            'layoutOptions' => $layoutOptions)) ?>
        <?php else: ?>
        <?php echo __('View is not implemented yet') ?>
        <?php endif ?>
    </form>

    <div class="message no-margin">
        <?php echo format_number_choice(
            '[0]No entries|[1]One entry|(1,+Inf]%count% entries',
            array('%count%' => $pager->count()),
            $pager->count()
        ); ?>
    </div>

    <div class="block-footer clearfix">
        <?php if($settings['manyActionAvailable']): ?>
        <div class="float-left">
            <?php if($layoutOptions['showCheckboxes']): ?>
            <img width="16" height="16" class="picto" src="/images/icons/fugue/arrow-curve-000-left.png"> 
            <a class="button" href="#" id="selectAll"><?php echo __('Select All') ?></a> 
            <a class="button" href="#" id="deselectAll"><?php echo __('Unselect All') ?></a>
            <span class="sep"></span>
            <?php endif; ?>
            <select id="table-action" name="table-action">
                <option value=""><?php echo __($layoutOptions['manyActionsDefaultLabel']); ?></option>
                <?php foreach($settings['actions'] as $actionName => $action): ?>
                <?php if(!empty($action['manyRoute']) && empty($action['disabledForMany'])): ?>
                <option value="<?php echo $actionName; ?>"><?php echo __($action['label']) ?></option>
                <?php endif;?>    
                <?php endforeach; ?>
            </select>
            <button class="small" type="submit" id="submitItems"><?php echo __('Ok') ?></button>
        </div>
        <span class="sep float-left"></span>
        <?php endif; ?>

        <div class="float-left">
            <form action="<?php echo url_for($settings['route'], $settings['routeParams']) ?>" method="post" id="perPageForm">
                <div class="float-left">
                    <?php echo __('Show')?>    
                    <?php echo $perPageForm->renderHiddenFields(); ?>
                    <?php echo $perPageForm['options']->render(array("id" => "perPage")) ?>  
                    <?php echo __('entries')?>
                </div>
            </form>
        </div>
        <div class="float-right" style="margin-left: 10px;">
            <?php if(count($availableViews->getRawValue()) > 1): ?>
            <?php echo __('Display mode')?>: 
            <select id="table-display" name="table-display">
                <?php foreach($availableViews->getRawValue() as $option) : ?>
                <option value="<?php echo $option ?>" <?php if($option == $settings['tableView']) echo 'selected="selected"' ?>><?php echo __($option) ?></option>
                <?php endforeach ?>
            </select>
            <?php endif ?>
        </div>
        <?php if($settings['search']): ?>
        <div class="float-right"><?php echo __('Search')?>: <input type="text" id="searchWord" value="<?php echo $settings['searchWord'] ?>">
            <div id="search-result" class="result-block" style="display: none; "></div>
        </div>
        <?php endif ?>
    </div>
    
    <div class="block-controls bottom-pagination">
        <div class="controls-buttons">
            <ul class="controls-buttons">
                <?php 
                include_partial('global/tablePagination', array(                     
                    'pager' => $pager 
                    ));
                ?>
            </ul>
        </div>
    </div>
    
</div>