<script type="text/javascript">
confirmResults = false;

function modalConfirm(title, content, MaxWidth, confirm, click)
{
    if(!MaxWidth)
    {
        MaxWidth = 500;
    }

    $.modal({
        content: content,
        title: title,
        maxWidth: MaxWidth,
        buttons: {
            '<?php echo __('Confirm'); ?>': function(win) 
            {
                confirmResults = true;
                
                if(click)
                {
                        $("#"+confirm).trigger('click');
                }
                else
                {
                        window.location = confirm;
                }
                
                win.closeModal(); 
                return true;
            },
            '<?php echo __('Close'); ?>': function(win) 
            {
                confirmResults = false;
                win.closeModal();
            }
        }
    });
}

function infoModal(title, content, maxWidth)
{
    $.modal({
        content: content,
        title: title,
        maxWidth: maxWidth,
        buttons: 
        {
            '<?php echo __('OK'); ?>': function(win) 
            {
                win.closeModal();           
            }
        }
    });
}

function alertModalConfirm(title, content, MaxWidth, confirm, click)
{
    if(!confirmResults)
    {
        modalConfirm(title, content, MaxWidth, confirm, click);
        return false;
    }

	confirmResults = false;

    return true;
}


</script>