<div id="page-locked">
    <div class="block">
    	<div id="head">
    		<h1><?php echo __('Page locked') ?></h1>
    	</div>
    	<div id="body" class="big-blue">
    		<?php echo __('Please read your personal barcode to unlock page.') ?>
    	</div>
    </div>
</div>