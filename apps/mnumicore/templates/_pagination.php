<?php if(isset($pager)) : ?>
    <?php if ($pager->haveToPaginate()): ?>
        <ul class="controls-buttons">
          <?php if($pager->getPage() > 1): ?>
              <?php if(isset($showFirstLast)) : ?>
              <li>
                  <a title="<?php echo __('First') ?>" href="<?php echo url_for($url).'?page='.$pager->getFirstPage() ?>">
                      <img width="16" height="16" src="/images/icons/fugue/navigation-180.png"> <?php echo __('First') ?>
                  </a>
              </li>
              <?php endif ?>
              <li>
                  <a title="<?php echo __('Previous') ?>" href="<?php echo url_for($url).'?page='.$pager->getPreviousPage() ?>">
                      <img width="16" height="16" src="/images/icons/fugue/navigation-180.png"> <?php echo __('Prev') ?>
                  </a>
              </li>
          <?php endif ?>
          <?php $links = $pager->getLinks(); ?>
          <?php foreach ($links as $page): ?>
              <li>
                  <a class = "<?php if($page == $pager->getPage()) echo ' current' ?>" title="<?php echo __('Page') ?> <?php echo $page ?>" 
                     href="<?php echo url_for($url).'?page='.$page ?>">
                      <b><?php echo $page ?></b>
                  </a>
              </li>
          <?php endforeach ?>
          <?php if($pager->getPage() < $pager->getLastPage()): ?>
              <li>
                  <a title="<?php echo __('Next') ?>" href="<?php echo url_for($url).'?page='.$pager->getNextPage() ?>"> <?php echo __('Next') ?> 
                      <img width="16" height="16" src="/images/icons/fugue/navigation.png">
                  </a>
              </li>
              <?php if(isset($showFirstLast)) : ?>
              <li>
                  <a title="<?php echo __('Last') ?>" href="<?php echo url_for($url).'?page='.$pager->getLastPage() ?>"> <?php echo __('Last') ?> 
                      <img width="16" height="16" src="/images/icons/fugue/navigation.png">
                  </a>
              </li>
              <?php endif ?>
          <?php endif ?>
        </ul>
    <?php else :?>
        <br/>
    <?php endif ?>
<?php endif ?>