<?php if ($filterForm): ?>

<section class="grid_12">
    <div class="block-border search-div">
        <form method="post" action="" id="simple_form" class="block-content form search-filter-form" name="filters" >
            <h1><?php echo __('Search'); ?></h1>
            <?php echo $filterForm; ?>
            <button type="button" id="filterBtn"><?php echo __('Search'); ?></button>
        </form>
    </div>
</section>

<?php endif; ?> 
