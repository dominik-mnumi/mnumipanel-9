<?php use_helper('I18N') ?>

<script type="text/javascript">

	$(document).ready(function()
	{
		// We'll catch form submission to do it in AJAX, but this works also with JS disabled
		$('#login-form').submit(function(event)
		{
			// Stop full page load
			event.preventDefault();
			
			// Check fields
			var login = $('#signin_username').val();
			var pass = $('#signin_password').val();
			var csrf_token = $('#signin__csrf_token').val();
			
			if (!login || login.length == 0)
			{
				$('#login-block').removeBlockMessages().blockMessage('<?php echo __('Please enter your user name'); ?>', {type: 'warning'});
			}
			else if (!pass || pass.length == 0)
			{
				$('#login-block').removeBlockMessages().blockMessage('<?php echo __('Please enter your password'); ?>', {type: 'warning'});
			}
			else
			{
				var submitBt = $(this).find('button[type=submit]');
				submitBt.disableBt();
				
				// Target url
				var target = $(this).attr('action');
				if (!target || target == '')
				{
					// Page url without hash
					target = document.location.href.match(/^([^#]+)/)[1];
				}
				
				// Request
				var data = {
					signin: { username: login, password: pass, _csrf_token: csrf_token }
				};
				var redirect = $('#redirect');
				if (redirect.length > 0)
				{
					data.redirect = redirect.val();
				}
				
				// Start timer
				var sendTimer = new Date().getTime();
				
				// Send
				$.ajax({
					url: target,
					dataType: 'json',
					type: 'POST',
					data: data,
					success: function(data, textStatus, XMLHttpRequest)
					{
						if (data.valid)
						{
							// Small timer to allow the 'cheking login' message to show when server is too fast
							var receiveTimer = new Date().getTime();
							if (receiveTimer-sendTimer < 500)
							{
								setTimeout(function()
								{
									document.location.href = data.redirect;
									
								}, 500-(receiveTimer-sendTimer));
							}
							else
							{
								document.location.href = data.redirect;
							}
						}
						else
						{
							// Message
							$('#login-block').removeBlockMessages().blockMessage(data.error || 'An unexpected error occured, please try again', {type: 'error'});
							
							submitBt.enableBt();
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown)
					{
						// Message
						$('#login-block').removeBlockMessages().blockMessage('Error while contacting server, please try again', {type: 'error'});
						
						submitBt.enableBt();
					}
				});
				
				// Message
				$('#login-block').removeBlockMessages().blockMessage('Please wait, cheking login...', {type: 'loading'});
			}
		});
	});

</script>

<div class="login-bg">

	<section id="login-block" class="medium-margin">
		<div class="block-border"><form class="form block-content" name="login-form" id="login-form" method="post" action="">
			
			<div class="block-header"><?php echo __('Please log in'); ?></div>
			
			<?php echo get_partial('sfGuardAuth/signin_form', array('form' => $form)) ?>
		</form></div>
	</section>
</div>

