<?php

/**
 * report actions.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class reportActions extends sfActions
{
 /**
  * Executes general action
  *
  * @param sfRequest $request A request object
  */
  public function executeGeneral(sfWebRequest $request)
  {
  }
  
  /**
   * Executes product action
   *
   * @param sfRequest $request A request object
   */
  public function executeProduct(sfWebRequest $request)
  {
  	$this->products = ProductTable::getInstance()->createQuery()->orderBy('name')->execute();
  }
}
