	<header>
		<h1>Reports</h1>
	</header>
	
	<div id="menu">
		<?php echo link_to('Logout', '@sf_guard_signout'); ?>
	</div>
	
	<a href="<?php echo url_for('homepage');?>" id="back">Back</a>
	<div id="header-shadow"></div>
	
	<article>
		
		<section id="login-block">
			<div class="block-border"><div class="block-content no-padding">
				
				<h1>Products overview</h1>
				
				<div class="block-controls">
					
					<ul class="controls-tabs js-tabs">
						<li><a href="#tab-today" title="Today"><img src="/images/icons/calendar/calendar-day.png" width="24" height="24"></a></li>
						<li><a href="#tab-week" title="This week"><img src="/images/icons/calendar/calendar-week.png" width="24" height="24"></a></li>
						<li><a href="#tab-month" title="This month"><img src="/images/icons/calendar/calendar-month.png" width="24" height="24"></a></li>
						<li><a href="#tab-year" title="This year"><img src="/images/icons/calendar/calendar-year.png" width="24" height="24"></a></li>
					</ul>
					
				</div>
				
				<div id="tab-today">
					
					<ul class="message success no-margin">
						<li><?php echo date('F j, Y'); ?></li>
					</ul>
					
					<table class="table" width="100%">
					<thead>
						<tr>
							<th>Product name</th>
							<th width="10%">Orders no.</th>
							<th width="10%">Income</th>
						</tr>
					</thead>
					<?php $income = 12847; $orders = 840; ?>
					<?php foreach($products as $row): ?>
						<tr>
							<td><?php echo $row->getName(); ?> <?php if(!$row->active): ?> <strong>(disabled)</strong><?php endif; ?></td>
							<td><?php echo round(rand($income - ($income * 10 / 100), $income + ($income * 10 / 100)))?></td>
							<td><?php echo round(rand($orders - ($orders * 10 / 100), $orders + ($orders * 10 / 100)))?></td>
						</tr>
					<?php endforeach; ?>
					</table>
					
					
				</div>
				
				<div id="tab-week">
					
					<ul class="message success no-margin">
						<li>
							from <?php echo date('F j, Y', mktime('0', 0, 0, date("m")  , date("d")-7, date("Y"))); ?> to <?php echo date('F j, Y'); ?>
						</li>
					</ul>
					
					<table class="table" width="100%">
					<thead>
						<tr>
							<th>Product name</th>
							<th width="10%">Orders no.</th>
							<th width="10%">Income</th>
						</tr>
					</thead>
					<?php $income = 66200; $orders = 5100; ?>
					<?php foreach($products as $row): ?>
						<tr>
							<td><?php echo $row->getName(); ?> <?php if(!$row->active): ?> <strong>(disabled)</strong><?php endif; ?></td>
							<td><?php echo round(rand($income - ($income * 10 / 100), $income + ($income * 10 / 100)))?></td>
							<td><?php echo round(rand($orders - ($orders * 10 / 100), $orders + ($orders * 10 / 100)))?></td>
						</tr>
					<?php endforeach; ?>
					</table>
				</div>
				
				<div id="tab-month">
					
					<ul class="message success no-margin">
						<li>
							from <?php echo date('F j, Y', mktime('0', 0, 0, date("m"), date("d"), date("Y")-1)); ?> to <?php echo date('F j, Y'); ?>
						</li>
					</ul>
					
					<table class="table" width="100%">
					<thead>
						<tr>
							<th>Product name</th>
							<th width="10%">Orders no.</th>
							<th width="10%">Income</th>
						</tr>
					</thead>
					<?php $income = 257829; $orders = 19991; ?>
					<?php foreach($products as $row): ?>
						<tr>
							<td><?php echo $row->getName(); ?> <?php if(!$row->active): ?> <strong>(disabled)</strong><?php endif; ?></td>
							<td><?php echo round(rand($income - ($income * 10 / 100), $income + ($income * 10 / 100)))?></td>
							<td><?php echo round(rand($orders - ($orders * 10 / 100), $orders + ($orders * 10 / 100)))?></td>
						</tr>
					<?php endforeach; ?>
					</table>
					
				</div>
				
				
				<div id="tab-year">
					
					<ul class="message success no-margin">
						<li>
							from <?php echo date('F j, Y', mktime('0', 0, 0, date("m"), date("d"), date("Y")-1)); ?> to <?php echo date('F j, Y'); ?>
						</li>
					</ul>
					
					<table class="table" width="100%">
					<thead>
						<tr>
							<th>Product name</th>
							<th width="10%">Orders no.</th>
							<th width="10%">Income</th>
						</tr>
					</thead>
					<?php $income = 3097170; $orders = 203182; ?>
					<?php foreach($products as $row): ?>
						<tr>
							<td><?php echo $row->getName(); ?> <?php if(!$row->active): ?> <strong>(disabled)</strong><?php endif; ?></td>
							<td><?php echo round(rand($income - ($income * 10 / 100), $income + ($income * 10 / 100)))?></td>
							<td><?php echo round(rand($orders - ($orders * 10 / 100), $orders + ($orders * 10 / 100)))?></td>
						</tr>
					<?php endforeach; ?>
					</table>
					
				</div>
				
				
			</div></div>
		</section>
		
	</article>
