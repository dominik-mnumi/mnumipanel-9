<header>
	<h1>Dashboard</h1>
</header>
	
<div id="menu">
	<?php echo link_to('Logout', '@sf_guard_signout'); ?>
</div>
	
<div id="status-bar">
	Logged as: <strong>Admin</strong>
</div>
<div id="header-shadow"></div>

<article>
	<ul class="shortcuts-list">
		<li><a href="<?php echo url_for('@generalReport'); ?>">
			<img src="../images/icons/web-app/48/stats.png" width="48" height="48"> General
		</a></li>
	 	<li><a href="<?php echo url_for('@productReport'); ?>">
			<img src="../images/icons/web-app/48/Pie-Chart.png" width="48" height="48"> Products
		</a></li>
	</ul>
	<p class="message info" style="margin-top:100px;">Want to see more? <a href="http://www.mnumi.com/">Click here</a> to take the Mnumi tour</p>
</article>
