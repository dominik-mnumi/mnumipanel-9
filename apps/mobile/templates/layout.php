<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" class="no-js dark">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon"  href="/images/icons/favicon.ico" />
    <link rel="icon" type="image/png" href="/images/icons/favicon-large.png">
	<link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png"/>
    
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body class="dark" style="min-height:100%;">
    <?php echo $sf_content ?>
  </body>
</html>
