<?php

echo "Rebuild model classess from exist schema...\n";
exec('./symfony doctrine:build --all-classes');


// show errors
error_reporting(E_ALL);

// lets stop the time
$start = microtime(true);


// enable autoloading of classes
require_once('./lib/vendor/mysql-workbench-schema-exporter/lib/MwbExporter/Core/SplClassLoader.php');
$classLoader = new SplClassLoader();
$classLoader->setIncludePath('./lib/vendor/mysql-workbench-schema-exporter/lib');
$classLoader->register();

// configure your output
$setup = array(
'extendTableNameWithSchemaName' => false
);

// create a formatter
$formatter = new \MwbExporter\Formatter\Doctrine1\Yaml\Loader($setup);

// parse the mwb file
$mwb = new \MwbExporter\Core\Workbench\Document('docs/db-diagram.mwb', $formatter);

file_put_contents('config/doctrine/schema.yml', $mwb->display());

// show some information about used memory
echo (memory_get_peak_usage(true) / 1024 / 1024) . " MB used\n";

exec('unzip docs/db-diagram.mwb document.mwb.xml; mv document.mwb.xml docs/;');

// show the time needed to parse the mwb file
$end = microtime(true);
echo  sprintf('%0.3f', $end-$start) . " sec needed\n";

exec('./symfony cc');

echo "Generate migration classes by producing a diff between old and new schema...\n";
exec("sed -i 's/generateFiles: true/generateFiles: false/g' config/doctrine/schema.yml");
exec('./symfony doctrine:generate-migrations-diff --env=workbench'); // remeber, before generate migration diff, you must set generateFiles to FALSE
exec("sed -i 's/generateFiles: false/generateFiles: true/g' config/doctrine/schema.yml"); // this will allow generate version files

exec('./symfony cc');

echo "Cleanup old model files...\n";
exec('./symfony doctrine:clean-model-files --no-confirmation');

echo "Generate version model files...\n";
exec('./symfony doctrine:build --all-classes');
exec("sed -i 's/generateFiles: true/generateFiles: false/g' config/doctrine/schema.yml");

exec('./symfony cc');

echo "Original english database loading...\n";
exec('./symfony mnumi:data-load --no-confirmation --env=workbench --language=en_US');

echo "Migrating...\n";
exec('./symfony mnumi:migrate --env=workbench');

echo "English database dump...\n";
exec('./symfony mnumi:data-dump --no-confirmation --env=workbench --language=en_US');

echo "Polish database dump...\n";
exec('./symfony mnumi:data-dump --language=pl_PL --no-confirmation --env=workbench');
