#!/bin/bash

## Download required libraries
./vendors.sh

TYPE="$1"

if [ "$TYPE" == "" ]; then
  TYPE="phpunit"
fi

if [ "$TYPE" != "phpunit" ] && [ "$TYPE" != "sahi" ]; then
  PROGNAME=$(basename $0)
  echo "Incorrect type. "
  echo "Format: $PROGNAME [sahi/phpunit]"
  exit 1
fi

echo "##########################"
echo "# Prepare for test: $TYPE "
echo "##########################"

if [ ! -d cache ];
then
    echo " > Creating 'cache' directory."
    mkdir cache
    chmod 777 cache
fi

if [ ! -d log ];
then
    echo " > Creating 'log' directory.'"
    mkdir log
    chmod 777 log
fi

if [ ! -d data/hotfolder ];
then
    echo " > Creating 'hotfolder' directory."
    mkdir data/hotfolder
    chmod 777 data/hotfolder
fi
 
## if NOT exist:
if [ ! -f config/databases.yml ];
then
    echo " > Setting databases.yml file"
    cp config/databases.yml.sample config/databases.yml
    sed -e 's/##PROD_HOST##/localhost/' -i config/databases.yml
    sed -e 's/##PROD_DATABASE##/mnumicore3/' -i config/databases.yml
    sed -e 's/##PROD_USERNAME##/tests/' -i config/databases.yml
    sed -e 's/##PROD_PASSWORD##//' -i config/databases.yml
    
    sed -e 's/##TEST_HOST##/localhost/' -i config/databases.yml
    sed -e 's/##TEST_DATABASE##/mnumicore3_test/' -i config/databases.yml
    sed -e 's/##TEST_USERNAME##/tests/' -i config/databases.yml
    sed -e 's/##TEST_PASSWORD##//' -i config/databases.yml
fi

echo " > Generating app.yml file (always override)"
cp -f apps/mnumicore/config/app.yml.sample apps/mnumicore/config/app.yml

if [ ! -f config/phpunit.yml ];
then
    echo " > Generating PHPUnit file (phpunit.xml)"
    cp config/phpunit.yml.sample config/phpunit.yml
fi

## Clear cache (and remove old log.xml)
echo " > Clear cache for symfony project"
./symfony cc > /dev/null

## Set correct permissions to files and directories
echo " > Set permissions for files"
./symfony project:permissions > /dev/null

if [ ! -d test/phpunit/fixtures/snapshots/cache ];
then
    mkdir -p test/phpunit/fixtures/snapshots/cache
fi

## Load database data (for test database only). You should have two environments (test, test2).
echo " > Load database data (test enviroment only)"
if [ `./symfony doctrine:build --all --env=test --no-confirmation 2>&1 | grep "created tables successfully" | wc -l` -gt 1 ];
then
    echo -e "\e[00;31mERROR\e[00m: Could not create tables"
    echo $?
    exit 1
fi

if [ `./symfony doctrine:build --all --env=test --application=mnumicore --no-confirmation 2>&1 | grep "created tables successfully" | wc -l` -gt 1 ];
then
    echo -e "\e[00;31mERROR\e[00m: Could not create tables (test)"
    echo $?
    exit 1
fi

if [ `./symfony doctrine:build --all --env=test2 --application=mnumicore --no-confirmation 2>&1 | grep "created tables successfully" | wc -l` -gt 1 ];
then
    echo -e "\e[00;31mERROR\e[00m: Could not create tables (test2)"
    echo $?
    exit 1
fi

./symfony doctrine:clean-model-files --no-confirmation 2>&1 > /dev/null

if [ `./symfony doctrine:data-load --env=test --application=mnumicore --no-confirmation 2>&1 | grep "Data was successfully loaded"  | wc -l` -gt 1 ];
then
    echo -e "\e[00;31mERROR\e[00m: Could not load test fixtures data (test)"
    echo $?
    exit 1
fi

if [ `./symfony doctrine:data-load --env=test2 --application=mnumicore --no-confirmation 2>&1 | grep "Data was successfully loaded"  | wc -l` -gt 1 ];
then
    echo -e "\e[00;31mERROR\e[00m: Could not load test fixtures data (test2)"
    echo $?
    exit 1
fi

if [ "$TYPE" == "phpunit" ]; 
then
    echo " > Create PHPUnit snapshot"

    if [ `./symfony phpunit:do-snapshot --env=test --application=mnumicore 2>&1 | wc -l` -gt 0 ];
    then
        echo -e "\e[00;31mERROR\e[00m: Could not create PHPUnit snapshot"
        echo $?
        exit 1
    fi

    if [ `./symfony phpunit:do-snapshot --env=test2 --application=mnumicore 2>&1 | wc -l` -gt 0 ];
    then
        echo -e "\e[00;31mERROR\e[00m: Could not create PHPUnit snapshot"
        echo $?
        exit 1
    fi
else
    echo " > Create SAHI snapshot..."
    if [ `./symfony sahi:do-snapshot 2>&1 | wc -l` -gt 0 ];
    then
        echo -e "\e[00;31mERROR\e[00m: Could not create SAHI snapshot"
        exit 1
    fi
fi
echo \
"************************************************
* Finished. It's time to run all tests 
**************************************************
"

echo "
APP.YML preview:
----------------------------------
"
cat apps/mnumicore/config/app.yml

echo "
----------------------------------

DATABASES.YML preview:
----------------------------------
"
cat config/databases.yml
echo "
----------------------------------
"