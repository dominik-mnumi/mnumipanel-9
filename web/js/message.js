/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * show message from json result
 *
 * @param content object
 * @param type - success, error, message, info
 * @param messageResults - json result
 */
function displayAlert(content, type, messageResults)
{
    if(content && messageResults)
    {
        var html = '<ul class="message ' + type + ' grid_12">';
        $.each(messageResults, function(key, message) {
            html += '<li>' + message + '</li>';
        })
        html += '</ul>';
        content.html(html);
    }
}