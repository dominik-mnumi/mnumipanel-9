$(document).ready(function(){
    $(function() {
        $("a.parent").toggle(
            function()
            {
                $("a.parent").removeClass('current');
                $(this).addClass('current')
	
                //root id
                var val = $(this).attr('val');
                $(".root_id_hidden").val(val);
            },
            function()
            {
                $("a.parent").removeClass('current');
                $(".root_id_hidden").val("");
            }
	        
            );
    });
});


