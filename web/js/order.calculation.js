jQuery(document).ready(function($)
{
    window.localStorage['calculationPrice'] = '{}';

    window.askCalculation = function(callback)
    {
        var script = (devenv) ? '/mnumicore_dev.php' : '';

        var fields = $("#order_form").serializeArray();

        // disables cost select
        $('select.cost-name').attr('disabled', 'disabled');
        $.post(script + '/order/calculation',
        {
            fields: fields,
            product_id: $('#order_product_id').val(),
            client_id: $('#order_client_id').val(),
            order_id: $('#order_id').val()
        },
        function(data) 
        {
            $('table.calculation-table').show();
            $('#calculation_error').hide();

            window.CalculationApp.updateCalculation(data);

            // enables cost select
            $('select.cost-name').removeAttr('disabled');

            // callback section
            if(callback)
            {
                callback();
            }

            return;
        }, 'json').fail(function(data) {
                if (data.status == 400) {
                    $('table.calculation-table').hide();
                    $('#calculation_error').show().html(data.responseText);
                }
        });

    }

    window.CalculationItem = Backbone.Model.extend(
    {
        defaults: function()
        {
            return {
                'price': 0,
                'systemPrice': 0,
                'type': ''
            };
        },

        idAttribute: "_id"
    });

    window.CalculationItemList = Backbone.Collection.extend(
    {
        model: CalculationItem,
        localStorage: new Store("calculationPrice"),
        getSummaryItems: function()
        {
            return _.reduce(this.models, function(memo, item) 
            {
                return (item.get('type') != 'trader')
                    ? memo + parseFloat(item.get('price') )
                    : memo;
            }, 0);
        },
        getTradersItems: function()
        {
            return _.reduce(this.models, function(memo, item)
            {
                return (item.get('type') == 'trader')
                    ? memo + parseFloat(item.get('price') )
                    : memo;
            }, 0);
        },
        // calculates discount
        calculateDiscount: function(price)
        {
            price = parseFloat(price).toFixed(2);

            discount = 0;
            if(window.CalculationApp && window.CalculationApp.discount)
            {
                if(window.CalculationApp.discount.type == 'percent')
                {
                    discount = price * (window.CalculationApp.discount.value/100);

                    // if discount is with a problematic decimal like 44.215
                    // it is rounded down in second number after point (* 100 and / 100)
                    if((discount * 100) % 1 == 0.5)
                    {
                        discount = Math.floor(discount * 100) / 100;
                    }

                    discount = parseFloat(discount).toFixed(2);
                }
                else if(window.CalculationApp.discount.type == 'amount')
            	{
                    discount = parseFloat(window.CalculationApp.discount.value).toFixed(2);
            	}
            }
            return discount;
            
        },
        // gets discount
        getDiscount: function()
        {
            return parseFloat(this.calculateDiscount(this.getSummaryItems())).toFixed(2);
        },
        // gets system discount
        getSystemDiscount: function()
        {
            return parseFloat(this.calculateDiscount(this.getSystemSummaryItems())).toFixed(2);
        },
        // gets price net basing on user inputs (or calculated by system if block
        // not enabled)
        getSummary: function()
        {
            return parseFloat(this.getSummaryItems() + this.getTradersItems() - this.getDiscount()).toFixed(2);
        },
        // gets price gross basing on user inputs (or calculated by system if block
        // not enabled)
        getSummaryGross: function()
        {
            var summary = parseFloat(this.getSummary());
            return parseFloat(summary + summary * window.CalculationApp.tax_amount).toFixed(2);
        },
        getSystemSummaryItems: function()
        {
            return _.reduce(this.models, function(memo, item)
            {
                return (item.get('type') != 'trader')
                    ? memo + parseFloat(item.get('systemPrice') )
                    : memo;
            }, 0);
        },
        getTradersSystemSummaryItems: function()
        {
            return _.reduce(this.models, function(memo, item)
            {
                return (item.get('type') == 'trader')
                    ? memo + parseFloat(item.get('systemPrice') )
                    : memo;
            }, 0);
        },
        // gets only system price net based on system data
        getSystemSummary: function()
        {
            return parseFloat(this.getSystemSummaryItems() + this.getTradersSystemSummaryItems() - this.getSystemDiscount()).toFixed(2);
        },
        // gets only system price gross based on system data
        getSystemSummaryGross: function()
        {
            return parseFloat(this.getSystemSummary() + this.getSystemSummary() * CalculationApp.tax_amount).toFixed(2);
        },
        getCostSummaryItems: function()
        {
            return _.reduce(this.models, function(memo, item)
            {
                return memo + parseFloat(item.get('cost'));
            }, 0);
        },
        // gets cost net basing on user inputs (or calculated by system if block
        // not enabled)
        getCostSummary: function()
        {
            return this.getCostSummaryItems();
        },
        getSizeInformation: function()
        {
            return $('input[name|="price[SIZE][label]"]').val();
        },
        getIssueInformation: function()
        {
            return $('input[name|="price[ISSUE][label]"]').val();
        },
        getQuantityInformation: function()
        {
            return $('input[name|="price[QUANTITY][label]"]').val();
        },
        getProductName: function()
        {
            return $('span#product-name').text();
        },
        getOrderName: function()
        {
            return $('h1 span.name').text();
        }
    });

    window.calcItems = new CalculationItemList();

    window.CalculationItemView = Backbone.View.extend(
    {
        tagName: "tr",

        template: _.template($('#calculation-item').html()),

        events: 
        {
            "change .price": "updateRow"
        },

        initialize: function() 
        {
            this.template = _.template($('#calculation-item').html());
        },
        render: function() 
        {
            var localModelJSON = this.model.toJSON();

            // if price is empty then do not show
            if(localModelJSON.price == 0 || localModelJSON.price == 0.00
            || localModelJSON.price == null)
            {
                localModelJSON.price = '';
            }

            // if price is empty then do not show
            if(localModelJSON.cost == 0 || localModelJSON.cost == 0.00
                || localModelJSON.cost == null)
            {
                localModelJSON.cost = '';
            }

            $(this.el).html(this.template(localModelJSON));
            
            // sets label selector
            this.label = this.$('.price_label');
            
            // if element exists (for issue and size does not exist)
            var price = 0;
            if(this.$('.price_amount').length != 0)
            {
                // sets user price selector (or system price if price is not blocked)
                price = this.$('.price_amount');
            }
            
            this.price = price;
            
            // sets system price
            this.systemPrice = this.$('.price_system_amount');

            // costs
            var cost = 0;
            if(this.$('.price_cost').length != 0)
            {
                // sets user cost selector (or system price if price is not blocked)
                cost = this.$('.price_cost');
            }

            this.cost = cost;

            return this;
        },
        updateRow: function()
        {           
            this.model.save({
                'label': this.label.val(), 
                'price': this.price.val(),
                'systemPrice': this.systemPrice.val(),
                'cost': (typeof this.cost.val === 'function') ? this.cost.val() : 0
            });
            return this;
        }
    });

    window.CalculationView = Backbone.View.extend(
    {
        el: '#js-calculation',
        iteration: 0,
        tax_amount: 0,

        summaryTemplate: _.template($('#calculation-summary').html()),

        initialize: function() 
        {
            _.bindAll(this, 'addOne');

            calcItems.bind('add',   this.addOne, this);
            calcItems.bind('reset', this.addAll, this);
            calcItems.bind('all',   this.render, this);

//            calcItems.fetch();
        },

        addOne: function(item) 
        {
            var _id = item.get('id');
            if(item.get('id') == '[MATERIAL]')
            {
                id = '1';
            }
            if(item.get('id') == '[PRINT]')
            {
                id = '2';
            }

            item.set(
            {
                '_id': _id
            }).save();

            var view = new CalculationItemView(
            {
                model: item, 
                className: (this.iteration %2) ? 'grey ': ''
            });
            $(this.el).append(view.render().el);
            this.iteration++;
        },

        addAll: function() 
        {
            $(this.el).html('');
            this.iteration = 0;
            calcItems.each(this.addOne);
        },

        /**
        * Updates calculation form basing on response from server.
        */
        updateCalculation: function(restCalculation) 
        {
            // 1. clear all costs and prices
            calcItems.each(function(item) 
            {
                // do not set 0 for trader price and additional costs - these elements are calculated
                // from diff source
                if(['trader', 'cost'].indexOf(item.get('type')) == -1)
                {
                    // if price block is checked then reset only systemPrice
                    // otherwise price and systemPrice
                    if($('input[name="order[price_block]"]').is(':checked'))
                    {
                        item.set(
                        {
                            'systemPrice': 0
                        }).save();
                    }
                    else
                    {
                        item.set(
                        {
                            'cost': 0,
                            'price': 0,
                            'systemPrice': 0
                        }).save();
                    }    
                }
            });

            // 2. load all new values
            var selectedOtherStr = '';
            $.each(restCalculation, function() 
            {
                var item = calcItems.get(this.id);

                if(item)
                {
                    // if price block is checked then sets only systemPrice
                    // otherwise price, systemPrice and cost
                    if($('input[name="order[price_block]"]').is(':checked'))
                    {
                        item.set(
                        {
                            'label': this.label, 
                            'systemPrice': parseFloat(this.price).toFixed(2),
                        }).save();   
                    }
                    else
                    {
                        item.set(
                        {
                            'label': this.label, 
                            'price': parseFloat(this.price).toFixed(2),
                            'systemPrice': parseFloat(this.price).toFixed(2),
                            'cost': parseFloat(this.cost).toFixed(2)
                        }).save();
                    }    
                }
                // if does not exist then add
                else
                {
                    calcItems.add(new CalculationItem({
                        'id': this.id, 
                        '_id': this.id,
                        'label': this.label, 
                        'price': parseFloat(this.price).toFixed(2),
                        'systemPrice': parseFloat(this.price).toFixed(2),
                        'cost': parseFloat(this.cost).toFixed(2)
                    }));
                }

                // if other then add option to select
                if(this.id.match(/OTHER/))
                {
                    selectedOtherStr += '<option value="' + this.dbId + '">' + this.label + '</option>';
                }
            });

            calcItems.fetch();

            // removes all data excluding first one ('Select finish')
            $('select.cost-name option:not(:first)').remove();
            $('select.cost-name').append(selectedOtherStr);
        },

        render: function() 
        {      
            var summaryBasicPrice = calcItems.getSummaryItems();
            var systemSummaryBasicPrice = calcItems.getSystemSummaryItems();
            var summary = parseFloat(calcItems.getSummary()).toFixed(2);
            var summaryGross = parseFloat(calcItems.getSummaryGross()).toFixed(2);
            var systemSummary = parseFloat(calcItems.getSystemSummary()).toFixed(2);
            var discount = calcItems.getDiscount();      
            var taxSystem = parseFloat(systemSummary * parseFloat(this.tax_amount)).toFixed(2);
            var summaryCost = parseFloat(calcItems.getCostSummary()).toFixed(2);
            var taxCost = parseFloat(summaryCost * parseFloat(this.tax_amount)).toFixed(2);

            var sum = this.summaryTemplate(
            {
                summary_price: summary,
                summary_gross: summaryGross,
                summary_system_price: parseFloat(systemSummary).toFixed(2),
                symmary_system_gross: parseFloat(parseFloat(systemSummary) + parseFloat(taxSystem)).toFixed(2),
                summary_system_discount: parseFloat(discount).toFixed(2),
                summary_basic_price: summaryBasicPrice,
                summary_system_basic_price: systemSummaryBasicPrice,
                summary_cost: summaryCost,
                symmary_cost_gross: parseFloat(parseFloat(summaryCost) + parseFloat(taxCost)).toFixed(2)
            });
            $('#calculation-sum').html(sum);
        }
    });

    // TRADERS SECTION
    
    window.traderColl = new Array();
    
    /**
     * New thing
     */
    window.TradersView = Backbone.View.extend(
    {
        el: '#trader_app',
        traderNo: 0,
        events: 
        {
            "click .add": "newRow"
        },
        initialize: function() 
        {
            this.template = _.template($('#client-traders-form').html());
        },
        render: function() 
        {
            rendered_content = this.template(
            {
                collection: this.collection
            });
            $(this.el).html(rendered_content);
            return this;
        },

        newRow: function(e) 
        {
            e.preventDefault();
            if(this.$("select[name='_trader']  option:selected").val() == "") 
            {
                this.$(".#trader_error").show();
                return false;
            }
            r = new TraderRowView(
            {
                id: 'temp'+this.traderNo,
                user_id: this.$("select[name='_trader']").val(),
                name: this.$("select[name='_trader']  option:selected").text(),
                quantity: this.$("input[name='_quantity']").val(),
                price: this.$("input[name='_price']").val(),
                description: this.$("input[name='_description']").val()    
            });

            this.traderNo++;

            this.$('#trader_list').append(r.render().el);
            this.$(".#trader_error").hide();
      
        }

    });

    window.TraderRowView = Backbone.View.extend(
    {
        events: 
        {
            "click .delete": "removeRow"
        },
        initialize: function() 
        {
            this.template = _.template($('#client-row').html());
            this.id = this.options['id'];
            this.user_id = this.options['user_id'];
            this.name = this.options['name'];
            this.price = this.options['price'];
            this.quantity = this.options['quantity'];
            this.description = this.options['description'];              
        },
        render: function() 
        {
            rendered_content = this.template(
            {
                name: this.name,
                quantity: this.quantity,
                price: this.price,
                description: this.description
            })  
            $(this.el).append(rendered_content + this.hiddenInputs());
            calcItems.add(new CalculationItem({
                'id': '[TRADER][' + this.id + ']', 
                '_id': '[TRADER][' + this.id + ']', 
                'label': this.description, 
                'price': parseFloat(this.price * this.quantity).toFixed(2), 
                'systemPrice': parseFloat(this.price * this.quantity).toFixed(2),
                'type': 'trader',
                'cost': 0
            }));
            return this;
        },
        removeRow: function(e) 
        {
            e.preventDefault();
            calcItems.get('[TRADER][' + this.id + ']').destroy();
            askCalculation();
            $(this.el).fadeOut('fast', function() 
            {
                $(this).remove();
                askCalculation();
            });
        },
        hiddenInputs: function() 
        {
            var d = new Date();
            t = d.getTime();
            str = '<input type="hidden" name="order[trader][' + this.id + '][id]" value="' + this.id + '"/>';
            str += '<input type="hidden" name="order[trader][' + this.id + '][user_id]" value="' + this.user_id + '"/>';
            str += '<input type="hidden" name="order[trader][' + this.id + '][quantity]" value="' + this.quantity + '"/>';
            str += '<input type="hidden" name="order[trader][' + this.id + '][price]" value="' + this.price + '"/>';
            str += '<input type="hidden" name="order[trader][' + this.id + '][description]" value="' + this.description + '"/>';

            return str;      
        }
    });

    window.MainView = Backbone.View.extend(
    {  
        initialize: function(traders) 
        {
            this.traders = traders;

            // render traders
            this.traders_view = new TradersView(
            {
                collection: traders
            });
            this.traders_view.render();
        },
        addExistingTraders: function(rows) 
        {
            for(var i = 0; i < rows.length; i++)
            {
                r = new TraderRowView(
                {
                    id: rows[i].id,
                    user_id: rows[i].user_id,
                    name: rows[i].name,
                    quantity: rows[i].quantity,
                    price: rows[i].price,
                    description: rows[i].description   
                });

                $('#trader_list').append(r.render().el);
            };
            window.TradersView.traderNo = rows.length;
        }
    });
    // END TRADERS SECTION

    // COSTS SECTION
    window.costColl = new Array();

    /**
     * New thing
     */
    window.CostsView = Backbone.View.extend(
    {
        el: '#cost_app',
        costNo: 0,
        events:
        {
            "click .add": "newRow"
        },
        initialize: function()
        {
            this.template = _.template($('#cost-form').html());
        },
        render: function()
        {
            rendered_content = this.template(
            {
                collection: this.collection
            });
            $(this.el).html(rendered_content);
            return this;
        },

        newRow: function(e)
        {
            e.preventDefault();
            if(this.$("select[name='_cost'] option:selected").val() == "")
            {
                this.$(".#cost-name-error").show();
                return false;
            }
            r = new CostRowView({
                id: 'temp' + this.costNo,
                field_item_id: this.$("select[name='_cost']").val(),
                name: this.$("select[name='_cost'] option:selected").text(),
                quantity: this.$("input[name='_quantity']").val(),
                cost: this.$("input[name='_price']").val(),
                description: this.$("input[name='_description']").val()
            });

            this.costNo++;

            this.$('#cost-list').append(r.render().el);
            this.$("#cost-name-error").hide();

        }

    });

    window.CostRowView = Backbone.View.extend(
    {
        events:
        {
            "click .delete": "removeRow"
        },
        initialize: function()
        {
            this.template = _.template($('#cost-row').html());
            this.id = this.options['id'];
            this.field_item_id = this.options['field_item_id'];
            this.name = this.options['name'];
            this.cost = this.options['cost'];
            this.quantity = this.options['quantity'];
            this.description = this.options['description'];
        },
        render: function()
        {
            rendered_content = this.template({
                name: this.name,
                quantity: this.quantity,
                cost: this.cost,
                description: this.description
            })
            $(this.el).append(rendered_content + this.hiddenInputs());
            calcItems.add(new CalculationItem({
                'id': '[COST][' + this.id + ']',
                '_id': '[COST][' + this.id + ']',
                'label': this.description,
                'cost': parseFloat(this.cost * this.quantity).toFixed(2),
                'price': 0,
                'systemPrice': 0,
                'type': 'cost'
            }));
            return this;
        },
        removeRow: function(e)
        {
            e.preventDefault();
            calcItems.get('[COST][' + this.id + ']').destroy();
            askCalculation();
            $(this.el).fadeOut('fast', function()
            {
                $(this).remove();
                askCalculation();
            });
        },
        hiddenInputs: function()
        {
            var d = new Date();
            t = d.getTime();
            str = '<input type="hidden" name="order[cost][' + this.id + '][id]" value="' + this.id + '"/>';
            str += '<input type="hidden" name="order[cost][' + this.id + '][field_item_id]" value="' + this.field_item_id + '"/>';
            str += '<input type="hidden" name="order[cost][' + this.id + '][quantity]" value="' + this.quantity + '"/>';
            str += '<input type="hidden" name="order[cost][' + this.id + '][cost]" value="' + this.cost + '"/>';
            str += '<input type="hidden" name="order[cost][' + this.id + '][description]" value="' + this.description + '"/>';

            return str;
        }
    });

    window.MainCostView = Backbone.View.extend(
    {
        initialize: function(costs)
        {
            this.costs = costs;

            // render traders
            this.costs_view = new CostsView(
            {
                collection: costs
            });
            this.costs_view.render();
        },
        addExistingCosts: function(rows)
        {
            for(var i = 0; i < rows.length; i++)
            {
                r = new CostRowView({
                    id: rows[i].id,
                    field_item_id: rows[i].field_item_id,
                    name: rows[i].name,
                    quantity: rows[i].quantity,
                    cost: rows[i].cost,
                    description: rows[i].description
                });

                $('#cost-list').append(r.render().el);
            };
            window.CostsView.costNo = rows.length;
        }
    });

});


