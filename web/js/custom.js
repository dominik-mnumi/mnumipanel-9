jQuery(document).ready(function($)
{
    $('select.change-language').change(function() {
        $('body').prepend("<div style='width: 100%; height: 100%; position: fixed; z-index: 9999999; background: none repeat scroll 0px 0px rgba(255, 255, 255, 0.8);'>" +
            "<div style='width: 100%; height: 100%; background: url(/images/loader.gif) no-repeat scroll center center transparent;'></div>" +
        "</div>");
        $.post( $('select.change-language').attr('data-url'), { lang: $(this).val() } )
            .done(function( data ) {
                location.reload();
        });
    });
});
/**
 *
 * @param string content
 * @param string title
 * @param string tinyMceElement
 * @param update function
 * @param callback function
 * @param string closeButton
 * @param string sendButton
 */
function modalEmailForm(content, title, tinyMceElement, update, callback, closeButton, sendButton)
{
    var buttons ={};
    buttons[closeButton] = function(win)
    {
        tinyMCE.triggerSave()
        modalFooterLoader(true);
        callback();
    };
    buttons[sendButton] = function(win)
    {
        win.closeModal();
    };

    $.modal(
        {
            content: content,
            title: title,
            maxWidth: 500,
            maxHeight: 450,
            resizable: false,
            onOpened: function() {

                update();

                $('#modal').css({
                    position: 'absolute',
                    zIndex: 100,
                    top: $(window).scrollTop()
                });

                $('body, html').css({
                    overflowY: 'hidden'
                });

                tinyMCE.init({
                    mode : "exact",
                    theme : "advanced",
                    elements : tinyMceElement,
                    plugins : "insertdatetime,preview,print,contextmenu,paste,directionality,fullscreen,visualchars",

                    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
                    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                    theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap,|,print,|,ltr,rtl,|,fullscreen",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "left",
                    theme_advanced_statusbar_location : "bottom"
                });

            },
            onClose: function() {
                $('body, html').css({
                    overflowY: 'auto'
                });
                tinyMCE.editors[0].remove();
            },
            buttons: buttons
        });
}

/**
 * Opens popup wizard window.
 */
function popupWizardWindow()
{
    // sets defaults (for older browsers)
    winWidth = 1024; 
    winheight = 768; 

    if(screen)
    { 
        winWidth = screen.width;
        winHeight = screen.height;
    }

    var newWizardPopupParam;
    newWizardPopupParam = 'toolbar=no, menubar=no, resizable=no, scrollbars=yes';   
    newWizardPopupParam += ', width=' + winWidth;
    newWizardPopupParam += ', height=' + winHeight;  
    newWizardPopupParam += ', top=0, left=0';

    newWindow = window.open('about:blank',
        'MnumiWizard',
        newWizardPopupParam);

    newWindow.focus();
}

function initializeAutocomplete(selector, url)
{
    // autocomplete
    selector.autocomplete(url, 
    {
        width: selector.width,
        //mustMatch: true,
        multiple: false,
        selectFirst: false
    });

    // if method exist
    if($.isFunction($.fn.result))
    {
        selector.result(
            function(event, data, formatted)
            {
                if(data)
                {
                    selector.next("input").val(data[1]);
                }
            });
    }

    selector.keyup(
        function(e) 
        {
        	var code = (e.keyCode ? e.keyCode : e.which);
        	if(code == 13) 
        	{
	       	    selector.next("input").val(data[1]);
	       	}
        	else
        	{
	            var val = selector.next("input").val();
	            if(val)
	            {
	                selector.val("");
	                selector.next("input").val("");
	            }
        	}
            
        });
}

/**
 * Renders errors list depending on json return array.
 */
function renderError(errorArray)
{
    var errorlist = '<ul class="message error no-margin">';
    for(i in errorArray)
    {
        errorlist += '<li>' + errorArray[i] + '</li>';
    }
    errorlist += '</ul>';
        
    return errorlist;
}

(function($) 
{
     var match = function (a, i, m) {
         return RegExp(m[3]).test(a.textContent || a.innerText || $(a).text() || "");
     };

     var match_ignore_case = function (a, i, m) {
         return RegExp(m[3], 'i').test(a.textContent || a.innerText || $(a).text() || "");
     };

     $.extend($.expr[':'], {
                  containsmatch: match,
                  containsmatchi: match_ignore_case
              });
 })(jQuery);

/**
 * Helper for tabs switching.
 * 
 * @param linkSel link selector
 */
function hideAllShowCurrent(linkSel)
{    
    // hides all tabs
    $("#tab-edit").css("display", "none");
    $("#tab-orders").css("display", "none");
    $("#tab-invoices").css("display", "none");
    $("#tab-notification").css("display", "none");

    $(".controls-tabs li").removeClass('current');
    linkSelArr = $(".controls-tabs li a");       
    var tab = '';

    // if agrument does exist gets param tab
    if(linkSel)
    {
        var href = $(linkSel).attr("href").toString();
        var param = href.split("#");
        
        if(param.length == 2)
        {
            tab = param[1].replace(/&/g, '');
        }
    }
    else
    {
        // gets all link selectors
        var linkSelArr = $(".controls-tabs li a");
            
        // gets param tab
        var href = window.location.toString();
        var param = href.split("#");              

        if(param.length == 2)
        {
            tab = param[1].replace(/&/g, '');
            
            // if none or undefined set to first tab
            if(tab == 'none' || tab == 'undefined')
            {
                tab = 'tab-edit';
            }
            else
            {
                if(tab == 'tab-edit')
                {
                    linkSel = linkSelArr[0];
                }
                else if(tab == 'tab-packages')
                {
                    linkSel = linkSelArr[1];
                }
                else if(tab == 'tab-orders')
                {
                    linkSel = linkSelArr[2];
                }
                else if(tab == 'tab-invoices')
                {
                    linkSel = linkSelArr[3];
                }
            }
        }
        // sets to first tab 
        else
        {
            tab = 'tab-edit';
                
            // gets link selector           
            linkSel = linkSelArr[0];
        }     
    }    

    // services links       
    $(linkSel).parents("li").addClass('current');

    //load ajax if attribute is set
	if($("#" + tab).attr('ajax'))
	{ 
		//show loader
		$("#" + tab).html('<div style="text-align:center;"><img src="/images/loader.gif" id="tab-loader" /></div>');
		//get data from url
		$.get($("#" + tab).attr('ajax'), function(data) {
			$('#tab-loader').remove();
			$("#" + tab).html(data);
		});
	}
    
    $("#" + tab).css("display", "");
}

/**
 * Checks if value is valid integer.
 */
function isInt(value)
{
    if((parseFloat(value) == parseInt(value)) && !isNaN(value))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function insertAtCaret(areaId, text) 
{
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
            "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") { 
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart ('character', -txtarea.value.length);
            strPos = range.text.length;
    }
    else if (br == "ff") strPos = txtarea.selectionStart;

    var front = (txtarea.value).substring(0,strPos);  
    var back = (txtarea.value).substring(strPos,txtarea.value.length); 
    txtarea.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") { 
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart ('character', -txtarea.value.length);
            range.moveStart ('character', strPos);
            range.moveEnd ('character', 0);
            range.select();
    }
    else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}

/**
 * Returns shorter filename.
 */
function getShorterFilename(value)
{
    var first, second;
    
    if(value.length > 30)
    {
        first = value.substr(0, 10);
        second = value.substr(value.length -15, value.length);
        
        value = first + '...' + second;
    }
    
    return value;
}

/**
 * Disables enter key.
 * 
 * @param Event e
 * @returns boolean
 */
function disableEnterKey(e)
{
    var key;
    if(window.event)
    {
        key = window.event.keyCode;     // IE
    }
    else
    {
         key = e.which;     // firefox
    }
    
    if(key == 13)
    {
        return false;
    }
    else
    {
        return true;
    }
}