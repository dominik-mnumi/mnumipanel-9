function loadDataPicker($, selector) {
    selector = selector || '.datepicker';
    $(selector).datepicker({
        alignment: 'bottom',
        dateFormat: 'yy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true,
        showOn: "button",
        buttonImage: "/images/icons/fugue/calendar-month.png",
        buttonImageOnly: true,
        renderer: {
            picker: '<div class="datepick block-border clearfix form"><div class="mini-calendar clearfix">' +
                '{months}</div></div>',
            monthRow: '{months}',
            month: '<div class="calendar-controls" style="white-space: nowrap">' +
                '{monthHeader:M yyyy}' +
                '</div>' +
                '<table cellspacing="0">' +
                '<thead>{weekHeader}</thead>' +
                '<tbody>{weeks}</tbody></table>',
            weekHeader: '<tr>{days}</tr>',
            dayHeader: '<th>{day}</th>',
            week: '<tr>{days}</tr>',
            day: '<td>{day}</td>',
            monthSelector: '.month',
            daySelector: 'td',
            rtlClass: 'rtl',
            multiClass: 'multi',
            defaultClass: 'default',
            selectedClass: 'selected',
            highlightedClass: 'highlight',
            todayClass: 'today',
            otherMonthClass: 'other-month',
            weekendClass: 'week-end',
            commandClass: 'calendar',
            commandLinkClass: 'button',
            disabledClass: 'unavailable'
        }
    });
}


jQuery(document).ready(function($)
{
    loadDataPicker($);
});