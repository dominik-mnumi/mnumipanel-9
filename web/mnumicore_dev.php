<?php

// this check prevents access to debug front controllers that are deployed by accident to production servers.
if(!(in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1', '178.33.52.12'))))
{
    // Redirect to home page
    $host  = $_SERVER['HTTP_HOST'];
    header("Location: http://$host/");
    exit;
}

$rootDir = dirname(dirname($_SERVER["SCRIPT_FILENAME"]));
require_once($rootDir.'/config/ProjectConfiguration.class.php');

$configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'dev', true, $rootDir);
sfContext::createInstance($configuration)->dispatch();
