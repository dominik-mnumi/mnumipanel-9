<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/settings')->

  with('response')->begin()->
    isStatusCode(401)-> // Status 401: authorization request
    checkElement('body', '/Please log in/')->
  end()->

  with('request')->begin()->
    isParameter('module', 'settings')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()->

  click('Login', array('signin' => array(
    'username' => 'admin',
    'password' => 'admin',
  )))->

  with('response')->begin()->
    isRedirected()->
    isStatusCode(302)->
  end()->

  get('/settings/api')->

  with('request')->begin()->
    isParameter('module', 'shop')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Group: API keys/')->
  end()
;

mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();

$browser->get('settings/api/add')->
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/Create API key/')->
    end()->
    //
    info('#try save empty form')->
    //
    click(' Save')->
    //
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/required/')->
    end()->
    //
    info('#go to new shop form again')->
    //
    click(' Save', array('shop' => array('description' => 'description',
        'host' => 'host')))->
    //
    followRedirect()->
    //
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/Saved successfully/')->
    end()
;