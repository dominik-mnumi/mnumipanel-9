<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = mnumiTestFunctional::getLoggedInstance();

$browser->
  get('/settings/report')->

  with('request')->begin()->
    isParameter('module', 'report')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Group: Status report/')->
  end()
;