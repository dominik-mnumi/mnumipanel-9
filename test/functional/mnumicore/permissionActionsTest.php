<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();

$browser->
  get('/permission')->

  with('request')->begin()->
    isParameter('module', 'permission')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
  end()
;

$browser->
    get('permission')->
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Permissions/')->
    end();//->

$browser->get('/permission');

$c = new sfDomCssSelector($browser->getResponseDom());
$selectorArray = $c->matchAll('a.delete');

$nodeArray = $selectorArray->getNodes();
