<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');
//include(dirname(__FILE__).'/../../../../bootstrap/Doctrine.php');
mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();

// check index action
$browser->
  get('/invoiceCost')->

  with('request')->begin()->
    isParameter('module', 'invoiceCost')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('.block-content', '/Invoice cost/')->
  end()
;

// check edit action
$browser->
  get('/invoiceCost/edit/1')->

  with('request')->begin()->
    isParameter('module', 'invoiceCost')->
    isParameter('action', 'edit')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('.block-content', '/Invoice cost/')->
  end()
  
;

$browser->
    post('/invoiceCost/paySelected',
        array('invoices' => array('1')))->
    with('response')->
        begin()->
        isStatusCode(200)->
    end()   
;

$invoiceCost = Doctrine_Query::create()
                ->from('InvoiceCost ic')
                ->fetchOne();

$balance = $invoiceCost->getAmount() - $invoiceCost->getPayedAmount();

$browser->
    post('/invoiceCost/paySelected/process',
        array('invoicesId' => array($invoiceCost->getId()),
              'payValue' => $balance))->
    with('response')->
        begin()->
        isRedirected()->
        isStatusCode(302)->
    end();

$browser->
    post('/invoiceCost/paySelected/process',
        array('invoicesId' => array(),
              'payValue' => $balance))->
    with('response')->
        begin()->
        isRedirected()->
        isStatusCode(302)->
    end();
