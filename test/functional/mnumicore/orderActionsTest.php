<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();

#testing edit page and new pack creation
$orders = OrderTable::getInstance()->findAll();
$order = $orders[0];

$package_count = OrderPackageTable::getInstance()->count();

$browser->
        get('/order/edit/'.$order->getId())->
        info('#checking edit order page')->
        with('request')->begin()->
        isParameter('module', 'order')->
        isParameter('action', 'edit')->
        end()->
        with('response')->begin()->
        isStatusCode(200)->
        checkElement('h1', '/'.$order->getName().'/')->
        end()
;

#order with no package
$order->setOrderPackage(null);
$order->save();

$browser->
        get('/order/edit/'.$order->getId())->
        info('#checking edit order page')->
        with('request')->begin()->
        isParameter('module', 'order')->
        isParameter('action', 'edit')->
        end()->
        with('response')->begin()->
        isStatusCode(200)->
        checkElement('h1', '/'.$order->getName().'/')->
        checkElement('body', '/Move to new Package/')->
        end()

;

# index 
$browser->
        get('/order')->
        with('request')->begin()->
        isParameter('module', 'order')->
        isParameter('action', 'index')->
        end()->
        with('response')->begin()->
        isStatusCode(200)->
        checkElement('body', '/Orders/')->
        checkElement('.table tr')->
        end()
;

$browser->
        get('/order/edit/'.$order->getId())->
        info('#checking edit order page')->
        with('request')->begin()->
        isParameter('module', 'order')->
        isParameter('action', 'edit')->
        end()->
        with('response')->begin()->
        isStatusCode(200)->
        checkElement('body', '!/new name/')->
        end()->
        info('order save')->
        click(' Save',
                array('order' => array(
                "id" => 1,
                "name" => "new name",
                "informations" => "This are some additional info",
                "attributes" => array(
                    "count" => 1,
                    "quantity" => 1,
                    "size" => FieldItemTable::getInstance()->findOneByName('A4')->getId(),
                    "material" => FieldItemTable::getInstance()->findOneByName('Paper X')->getId(),
                    "print" => FieldItemTable::getInstance()->findOneByName('Print X')->getId(),
                    "sides" => FieldItemTable::getInstance()->findOneByName('Single')->getId(),
                    "other" => array(ProductFieldTable::getInstance()->findOneByLabel('test label')->getId() => FieldItemTable::getInstance()->findOneByName('Bindery')->getId())
                ),
                'informations' => '',
                'price_net' => 100.00),
                'price' => array('MATERIAL' => array('label' => 'Podłoże: Paper X',
                                                           'price' => 0),
                                       'OTHER' => array('9' => array('label' => 'Others: test label',
                                                                   'price' => '3.1')),
                                       'PRINT' => array('label' => 'Typ zadruku: Print X',
                                                        'price' => 7))), 
                array('_with_csrf' => true)
        )->
        info('#check if order was proper save in ajax (another get request is needed)')->
        get('/order/edit/'.$order->getId())->
            info('#checking edit order page')->
            with('request')->begin()->
            isParameter('module', 'order')->
            isParameter('action', 'edit')->
            end()->
            with('response')->begin()->
            isStatusCode(200)->
            checkElement('body', '/new name/')->
            end()
;

$product = ProductTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
$client = ClientTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
$user = Doctrine_Core::getTable('sfGuardUser')->createQuery()->fetchOne();
if(!$user instanceof sfGuardUser)
{
    throw new Exception('No User object found.');
}

$browser->
        get('/order/add?productId='.$product->getId().'&clientId='.$client->getId().'&userId='.$user->getId())->
        info('#creating new order')->
        with('request')->begin()->
        isParameter('module', 'order')->
        isParameter('action', 'create')->
        end()->
        info('#redirect to edit form')->
        followRedirect()->
        with('request')->begin()->
        isParameter('module', 'order')->
        isParameter('action', 'edit')->
        end()
;

$browser->
        get('/order/changelog/'.$order->getId())->
        info('#checking changelog')->
        with('request')->begin()->
        isParameter('module', 'order')->
        isParameter('action', 'changelog')->
        end()->
        with('response')->begin()->
        isStatusCode(200)->
        checkElement('body', '/Changed by/')->
        checkElement('body', '/new name/')->
        end();