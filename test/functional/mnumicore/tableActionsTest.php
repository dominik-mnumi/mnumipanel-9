<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/settings/pricelist')->
    
  // login username  
  click('Login', array('signin' => array(
    'username' => 'admin',
    'password' => 'admin',
  )))->
        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
        
  get('/settings/pricelist')->

  with('request')->begin()->
    isParameter('module', 'pricelist')->
    isParameter('action', 'index')->
  end()->
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('#manyItems table tr', '%Default%', array('position' => 1))->
    checkElement('#manyItems table tr', '%Very important%', array('position' => 2))->
  end()->
  
  // sort cheking
  get('/settings/pricelist?sortBy=name&type=DESC')->

  with('request')->begin()->
    isParameter('module', 'pricelist')->
    isParameter('action', 'index')->
  end()->
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('#manyItems table tr', '%Very important%', array('position' => 1))->
    checkElement('#manyItems table tr', '%Default%', array('position' => 2))->
    
  end()->
  
  // table view
  get('/settings/pricelist?tableView=grid')->

  with('request')->begin()->
    isParameter('module', 'pricelist')->
    isParameter('action', 'index')->
  end()->
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('.grid', '%Default%')->
    checkElement('.grid', '%Very important%')->
    
  end();
