<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

//mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();
$browser->
  get('/settings/carrier')->

  with('request')->begin()->
    isParameter('module', 'carrier')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Group: Carrier/')->
  end()
;

$edit_item = $browser->getResponseDomCssSelector()->matchSingle('table .table-actions a.edit');
if(!$edit_item)
{
    throw new Exception('Cound not get first item in Carrier table.');
}
$delete_item = $browser->getResponseDomCssSelector()->matchSingle('table .table-actions a.delete');

$browser->
  click($edit_item->getNode(), array(), array('method' => 'get'))->
  with('request')->begin()->
    isParameter('module', 'carrier')->
    isParameter('action', 'edit')->
  end()->
  with('response')->begin()->
    isStatusCode()->
    checkElement('body', '/Delivery time/')->
    checkElement('body', '/Pricelist/')->
    checkElement('body', '!/is required/')->
  end()->
  
  click(' Save', array('carrier' => array(
      'name' => 'test name',
      'delivery_time' => '',
      'active' => '1',
      'tax' => '1'
  )),
  array('_with_csrf' => true)
  )->
    
    with('response')->begin()->
    checkElement('body', '/Delivery time/')->
    checkElement('body', '/is required/')->
    isStatusCode()->
  end()->
  
  click(' Save', array('carrier' => array(
      'name' => 'test name',
      'delivery_time' => 'fdsfds',
      'active' => '1',
      'tax' => '1',
      'cost' => '2,22',
      'price' => '2.22',
      'free_shipping_amount' => '2,22'
  )),
  array('_with_csrf' => true)
  )->
  
  followRedirect()->
  with('response')->begin()->
    checkElement('body', '/Delivery time/')->
    checkElement('body', '!/is required/')->
    checkElement('body', '!/2.22/')->
    isStatusCode()->
  end()->

  info('Try delete exist carrier')->
  get('/settings/carrier')->
  click($delete_item->getNode(), array(), array('method' => 'post'))->
        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
  
  followRedirect()->
    with('response')->begin()->
    checkElement('article ul[class="message success grid_12"]', true)->
    isStatusCode()->
  end();
  
