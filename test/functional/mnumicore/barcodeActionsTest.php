<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = new sfTestFunctional(new sfBrowser());

$client = Doctrine_Core::getTable('Client')->createQuery()->fetchOne();
if(!$client instanceof Client)
{
    throw new Exception('No Client object found.');
}

$order = Doctrine_Core::getTable('Order')->createQuery()->fetchOne();
if(!$order instanceof Order)
{
    throw new Exception('No Order object found.');
}

$user = Doctrine_Core::getTable('sfGuardUser')->createQuery()->fetchOne();
if(!$user instanceof sfGuardUser)
{
    throw new Exception('No User object found.');
}

$browser->
  get('/')->
    
  // login username  
  click('Login', array('signin' => array(
    'username' => 'admin',
    'password' => 'admin',
  )))->
        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
  
  // litle trick - instead of 8 digits id we put three 0 and rest is id
  info('Client test')->
  get('/barcode/read?barcode=^'.$client->getBarcode())->

  with('request')->begin()->
    isParameter('module', 'barcode')->
    isParameter('action', 'read')->
  end()->
  with('response')->begin()->
    isStatusCode(200)->
  end()->
  
  // litle trick - instead of 8 digits id we put three 0 and rest is id
  info('Order test')->
  get('/barcode/read?barcode=^'.$order->getBarcode())->

  with('request')->begin()->
    isParameter('module', 'barcode')->
    isParameter('action', 'read')->
  end()->
  with('response')->begin()->
    isStatusCode(200)->
  end()->
  
  // litle trick - instead of 8 digits id we put three 0 and rest is id
  info('User profile test')->
  get('/barcode/read?barcode=^'.$user->getBarcode())->

  with('request')->begin()->
    isParameter('module', 'barcode')->
    isParameter('action', 'read')->
  end()->
  with('response')->begin()->
    isStatusCode(200)->
  end();
  
  // @todo - test for package - when its ready
