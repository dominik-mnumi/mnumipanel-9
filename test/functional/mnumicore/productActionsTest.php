<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();

$browser->
    get('/product')->
    with('request')->begin()->
        isParameter('module', 'product')->
        isParameter('action', 'index')->
    end()->
    //
    with('response')->begin()->
        isStatusCode(200)->
        checkElement('body', '/Category list/')->
    end();


$boolkets_link = $browser->getResponseDomCssSelector()
    ->matchSingle('.tree-list a[title="Boolkets"]');
if(!$boolkets_link)
{
    throw new Exception('Cound not get first link from list.');
}

    //
$browser->
    click($boolkets_link->getNode())->
    with('request')->begin()->
        isParameter('module', 'product')->
        isParameter('action', 'save')->
    end()->
    with('response')->begin()->
        isRedirected()-> 
    end()->
    followRedirect()->
    with('response')->begin()->
        isStatusCode(200)->
        checkElement('body', '/Bulk/')->
    end()
;

$form = new CategoryAddForm();
$token = $form->getCSRFToken();
$token_name = $form->getCSRFFieldName();

$browser->
    info('#try add category - send parameters')->
    post('/product/categoryAdd',
        array('category' => array(
                'name' => 'test',
                'parentId' => CategoryTable::getInstance()->findOneByName('Calendars')->getId(),
                $token_name => $token)))->
    //
    with('request')->
    begin()->
    isParameter('module', 'product')->
    isParameter('action', 'categoryAdd')->
    end()->
    //
    followRedirect()->
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Saved successfully/')->
    info('#category created successfully')->
    end()
;

$c = new sfDomCssSelector($browser->getResponseDom());
$browser->
    info('#try delete category')->
    //
    click($c->matchSingle('a.delete')->getNode(), array(), array('method' => 'post'))->
    //
    with('response')->begin()->
    isRedirected()->
    isStatusCode(302)->
    end()->
    //
    followRedirect()->
    with('response')->
    begin()->
    checkElement('body', '/Deleted successfully/')->
    isStatusCode(200)->
    end()->
    info('#category deleted successfully')
;

$c = new sfDomCssSelector($browser->getResponseDom());
$browser->
    info('#go to new product form')->
    //
    click($c->matchSingle('#table_form h1 a')->getNode())->
    //
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Add product/')->
    end()->
    //
    info('#try save empty form')->
    //
    click(' Save')->
    //
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Product name is required/')->
    checkElement('body', '/Size - "Available values" are required/')->
    checkElement('body', '/Material type - "Available values" are required/')->
    checkElement('body', '/Print type - "Available values" are required/')->
    checkElement('body', '/Sides - "Available values" are required/')->
    
    checkElement('body', '/Product category is required/')->
    end()->
    //
    info('#go to new product form again')->
    //
    click(' Save',
        array(
            'product_0' =>
            array(
                'name' => 'test product',
                'category_id' => CategoryTable::getInstance()->findOneByName('Bussiness Cards')->getId()),
            'product_field_1' => array(),
            'product_field_2' =>
            array(
                'default_value' => 1),           
            'product_field_3' =>
            array(
                'default_value' => 1),
            'product_field_4' =>
            array(
                'product_field_item' => array(0 => FieldItemTable::getInstance()->findOneByName('A4')->getId()),
                'default_value' => FieldItemTable::getInstance()->findOneByName('A4')->getId()),
            'product_field_5' =>
            array(
                'product_field_item' => array(0 => FieldItemTable::getInstance()->findOneByName('Paper X')->getId()),
                'default_value' => FieldItemTable::getInstance()->findOneByName('Paper X')->getId()),
            'product_field_6' =>
            array(
                'product_field_item' => array(0 => FieldItemTable::getInstance()->findOneByName('Print X')->getId()),
                'default_value' => FieldItemTable::getInstance()->findOneByName('Print X')->getId()),
            'product_field_7' =>
            array(
                'product_field_item' => array(0 => FieldItemTable::getInstance()->findOneByName('Single')->getId()),
                'default_value' => FieldItemTable::getInstance()->findOneByName('Single')->getId()),
            'product_field_8' =>
            array(
                'fixpriceList' =>
                array(1 =>
                    array(0 =>
                        array('pricelist_id' => PricelistTable::getInstance()->getDefaultPricelist()->getId(),
                            'quantity' => 1,
                            'price' => '2,22'),
                        array('pricelist_id' => PricelistTable::getInstance()->getDefaultPricelist()->getId(),
                            'quantity' => 2,
                            'price' => '2.22')
                        )))
    ))->
    //
    info('#product created')->
    //
    with('response')->
    begin()->
    isRedirected()->
    isStatusCode(302)->
    end()->
    //
    followRedirect()->
    //
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Saved successfully/')->
    end()
;
    
$browser->get('/product')
    ->info('#try to delete product');

$c = new sfDomCssSelector($browser->getResponseDom());
$selectorArray = $c->matchAll('.grid > li');

// in grid actions for product 1
$c = new sfDomCssSelector($selectorArray->nodes[3]);
$selector = $c->matchSingle('a.delete');

$productDeleteLink = $selector->getNode();

$browser->
    click($productDeleteLink)->
    with('response')->
    begin()->
    isRedirected()->
    isStatusCode(302)->
    end()->
    //
    followRedirect()->
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Deleted successfully/')->
    end()->
    //
    info('#product deleted')
;

$c = new sfDomCssSelector($browser->getResponseDom());
$selectorArray = $c->matchAll('.grid > li > ul.grid.actions');

//in grid actions for product 1
$c = new sfDomCssSelector($selectorArray->nodes[0]);
$selectorArray = $c->matchSingle('li');

//edit node
$productEditLink = $browser->getResponseDomCssSelector()
    ->matchSingle('.grid a.edit')->getNode();


$browser->
    info('#try to edit product')->
    //
    click($productEditLink)->
    //
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Product: product /')->
    end()->
    //
    click(' Save',
            array('product_field_9' =>
            array('valid' => '')
        ))-> 
    //
    //
    info('#product saved')->
    //
    with('response')->
    begin()->
    isRedirected()->
    isStatusCode(302)->
    end()->
    //
    followRedirect()->
    //
    with('response')->
    begin()->
    isStatusCode(200)->
    checkElement('body', '/Saved successfully/')->
    end();     
