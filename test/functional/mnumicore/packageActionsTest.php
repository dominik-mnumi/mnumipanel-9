<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();

$client = ClientTable::getInstance()->findOneByFullname('client1');

if(!$client instanceof Client)
{
    throw new Exception('Could not find client "client1"');
}

#index for client

$browser->
        get('/client/'.$client->getId().'/packages')->
        with('request')->begin()->
        isParameter('module', 'package')->
        isParameter('action', 'index')->
        end()->
        with('response')->begin()->
        isStatusCode(200)->
        checkElement('body', '/Package/')->
        checkElement('h1', '/Client: '.$client->getFullname().'/')->
        end()
;

$package = $client->getOrderPackages();
$package = $package[0];

#single package view

$browser->
        get('/client/'.$client->getId().'/packages/'.$package->getId())->
        with('request')->begin()->
        isParameter('module', 'package')->
        isParameter('action', 'show')->
        end()->
        with('response')->begin()->
        isStatusCode(200)->
        checkElement('body', '/'.(String) $package.'/')->
        end();
        //
        $c = new sfDomCssSelector($browser->getResponseDom());
        $selectorArray = $c->matchAll('.table-actions');
        $c = new sfDomCssSelector($selectorArray->nodes[1]);
        $selectorArray = $c->matchAll('a');

;
