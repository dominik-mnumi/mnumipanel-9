<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/settings/pricelist')->
    
  // login username  
  click('Login', array('signin' => array(
    'username' => 'admin',
    'password' => 'admin',
  )))->
        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
        
  get('/settings/pricelist')->

  with('request')->begin()->
    isParameter('module', 'pricelist')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Group: Pricelist/')->
  end()->
    
  click('Add pricelist')->
    
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Create pricelist/')->
  end()->
    
  click(' Save', 
      array('pricelist' => array('name' => 'first test printsize'))
  )->

  followRedirect()->
    
  with('request')->begin()->
    isParameter('module', 'pricelist')->
    isParameter('action', 'edit')->
  end()->
    
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Saved successfully./')->
  end()->
    
  click(' Back')->
    
  with('request')->begin()->
    isParameter('module', 'pricelist')->
    isParameter('action', 'index')->
  end()->
    
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/first test printsize/')->
  end();

$c = new sfDomCssSelector($browser->getResponseDom());

$browser->
  click($c->matchSingle('.table-actions a.edit')->getNode())->
    
  with('request')->begin()->
    isParameter('module', 'pricelist')->
    isParameter('action', 'edit')->
  end()->
    
  click(' Save', 
      array('pricelist' => array('name' => 'Very important Person'))
  )->

  followRedirect()->
    
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Saved successfully./')->
  end()->
    
  click(' Back')->
    
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Very important Person/')->
  end()->
    
  click($c->matchSingle('.table-actions a.delete')->getNode(), array(), array('method' => 'post'))->
    
  followRedirect()->
    
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Deleted successfully./')->
  end()
  
;
