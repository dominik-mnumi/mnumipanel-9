<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

$browser = mnumiTestFunctional::getLoggedInstance();

$browser->
  get('/settings/payment')->

  with('request')->begin()->
    isParameter('module', 'payment')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Group: Payment/')->
  end()
;

$browser->
    get('settings/payment/add')->
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/Create payment/')->
    end()->
    //
    info('#try save empty form')->
    //
    click(' Save')->
    //
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/Field "Name" is required/')->
    end()->
    //
    info('#go to new payment form again')->
    //
    click(' Save', array('payment' =>
        array('name' => 'test name',
            'label' => 'test label')))->
    //
    followRedirect()->
    //
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/Saved successfully/')->
    end();

$browser->get('settings/payment?page=2');

$c = new sfDomCssSelector($browser->getResponseDom());
$browser->
    //
    info('Try edit payment')->
    //
    click($c->matchSingle('.table-actions a.edit')->getNode(), array(), array('method' => 'get'))->
    //
    with('request')->
    begin()->
    isParameter('module', 'payment')->
    isParameter('action', 'edit')->
    end()->
    //
    info('#go to edit form')->
    //
    click(' Save', array('payment' =>
        array('name' => 'test name 2',
            'label' => 'test label 2')))->
    //
    followRedirect()->
    //
    with('response')->
    begin()->
        isStatusCode(200)->
        checkElement('body', '/Saved successfully/')->
    end()
;

$browser->get('settings/payment?page=2');

    $c = new sfDomCssSelector($browser->getResponseDom());
$browser-> 
    //
    info('Try delete exist payment')->
    //
    click($c->matchSingle('.table-actions a.delete')->getNode(), array(), array('method' => 'post'))->
    //
    with('response')->begin()->
    isRedirected()->
    isStatusCode(302)->
    end()->
    followRedirect()->
    with('response')->begin()->
    checkElement('article ul[class="message success grid_12"]', true)->
    isStatusCode()->
    end()
;

    