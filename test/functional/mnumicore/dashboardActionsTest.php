<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = mnumiTestFunctional::getLoggedInstance();
$browser->
  get('/')->
  
  with('response')->begin()->
    isStatusCode(200)->
  end()
;
