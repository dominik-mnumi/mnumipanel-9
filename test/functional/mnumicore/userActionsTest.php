<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new sfTestFunctional(new sfBrowser());

$user = sfGuardUserTable::getInstance()->findOneByUsername('admin');

$browser->

  get('/user')->
        
  with('response')->begin()->
    isStatusCode(401)-> // Status 401: authorization request
    checkElement('body', '/Please log in/')->
  end()->

  with('request')->begin()->
    isParameter('module', 'user')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()->
        
  click('Login', array('signin' => array(
    'username' => 'admin',
    'password' => 'admin',
  )))->
        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
  
  get('/user')->

  with('request')->begin()->
    isParameter('module', 'user')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '!/This is a temporary page/')->
  end()->
  
  
  # LOYALTY POINTS TESTS
  
  get('/user/loyaltyPoints/'.$user->getId())->
  
  with('request')->begin()->
    isParameter('module', 'user')->
    isParameter('action', 'loyaltyPoints')->
  end()->

  with('response')->begin()->
    checkElement('body', '/Points/')->
    checkElement('body', '/100 points/')->
    isStatusCode()->
  end() ->
  
  get('/user/loyaltyPoints/'.$user->getId().'?type=new')->
  
  with('request')->begin()->
    isParameter('module', 'user')->
    isParameter('action', 'loyaltyPoints')->
  end()->

  with('response')->begin()->
    checkElement('body', '/Points/')->
    checkElement('body', '/300 points/')->
    checkElement('body', '!/100 points/')->
    isStatusCode()->
  end()->
  
  get('/user/loyaltyPoints/'.$user->getId().'?type=accepted')->
  
  with('request')->begin()->
    isParameter('module', 'user')->
    isParameter('action', 'loyaltyPoints')->
  end()->

  with('response')->begin()->
    checkElement('body', '/Points/')->
    checkElement('body', '!/300 points/')->
    checkElement('body', '/200 points/')->
    isStatusCode()->
  end()->
  
  get('/user/loyaltyPoints/'.$user->getId().'?type=used')->
  
  with('request')->begin()->
    isParameter('module', 'user')->
    isParameter('action', 'loyaltyPoints')->
  end()->

  with('response')->begin()->
    checkElement('body', '/Points/')->
    checkElement('body', '/100 points/')->
    checkElement('body', '!/200 points/')->
    isStatusCode()->
  end()->
  
  get('/user/loyaltyPoints/'.$user->getId().'?type=lost')->
  
  with('request')->begin()->
    isParameter('module', 'user')->
    isParameter('action', 'loyaltyPoints')->
  end()->

  with('response')->begin()->
    checkElement('body', '/Points/')->
    checkElement('body', '!/300 points/')->
    checkElement('body', '/250 points/')->
    isStatusCode()->
  end();
