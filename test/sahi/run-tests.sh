#!/bin/bash

if [ "$(id -u)" == "0" ]; then
  echo "This script must be run normal user (not ROOT)" 1>&2
  exit 1
fi

DIR=`php -r "echo dirname(realpath('$0'));"`

export SAHI_HOME="$DIR/../../lib/vendor/sahi/"
export SAHI_CLASS_PATH=$SAHI_HOME/lib/sahi.jar:$SAHI_HOME/extlib/rhino/js.jar:$SAHI_HOME/extlib/apc/commons-codec-1.3.jar
export SAHI_EXT_CLASS_PATH=

echo " > Try to load xvfb service..."
pkill "Xvfb" > /dev/null
export DISPLAY=:99.0 && Xvfb :1 -screen 0 1024x768x16 > /dev/null 2>&1 & 

echo " > Run SAHI service..."
pkill "/lib/sahi.jar" -f > /dev/null
sleep 3
java -classpath $SAHI_EXT_CLASS_PATH:$SAHI_CLASS_PATH net.sf.sahi.Proxy "$SAHI_HOME" "$DIR/userdata" &

sleep 3

if [ `pgrep "/lib/sahi.jar" -f | wc -l` != 1 ]; 
then
   echo "Could not run SAHI. Exiting!"
   exit 1
fi

cd $DIR/userdata

for D in `find scripts -name "*.sah"`
do
   # load database
   echo " > Test $D - load snapshot..."
   phpunit ../cleanupDbTest.php > /dev/null
   
   # run test
   echo " > Test $D - run test..."
   java -cp $SAHI_HOME/lib/ant-sahi.jar net.sf.sahi.test.TestRunner \
        -test $D -browserType chrome -baseURL "http://localhost:8099/" -threads 5 \
        -junitLog true -junitLogDir "$DIR/../../cache/"
done

echo "Exported JUNIT logs to: ./cache."

echo " > Shutdown SAHI service..."
pkill "/lib/sahi.jar" -f > /dev/null

echo " > Shutdown Xvfb service..."
pkill "Xvfb" > /dev/null

echo "Done."
