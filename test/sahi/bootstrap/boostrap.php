<?php

require_once __DIR__ . '/../../../config/ProjectConfiguration.class.php';
mnumiTestSahiFunctional::getConfiguration();

require_once __DIR__ . '/../BaseSahiTestCase.php';

class mnumiTestSahiFunctional
{
    static $configuration = false;
    
    public static function getConfiguration()
    {
        if(self::$configuration == false)
        {
            self::$configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'test', true);
            sfContext::createInstance(self::$configuration);
        }
        
        return self::$configuration;
    }
}
