<?php

require_once __DIR__.'/../BaseSahiTestSuite.php';
require_once __DIR__.'/../SahiSnapshotFactory.class.php';

/**
 *
 * @author Maksim Kotlyar <mkotlar@ukr.net>
 *
 */
class SahiDoSnapshotContainer extends sfPhpunitBaseDoSnapshotContainer
{
	/**
	 *
	 * @var sfPhpunitFixture
	 */
	protected $_fixture;

	public function doSnapshots($options = array())
	{
		// initalize db connection
		$configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'test', true);
		new sfDatabaseManager($configuration);
		
		// initalize fixutes class
		$suite = new BaseSahiTestSuite();
		$this->_fixture = sfPhpunitFixture::build($suite, $options);
		
		$this->_fixture->clean()
			// clean up tables used for storing snapshot data.
			->cleanSnapshots();
		
		// prepare "common" snapshot
		$this->loadCommonSnapshot();
	}
	
	protected function loadCommonSnapshot()
	{
		SahiSnapshotFactory::prepareCommonFixtures($this->fixture())
			->doSnapshot('common');
		
		return;
	}
	
	protected function fixture()
	{
		return $this->_fixture;
	}
}