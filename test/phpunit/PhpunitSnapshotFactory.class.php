<?php

class PhpunitSnapshotFactory
{
	public static function prepareCommonFixtures($fixtures)
	{
		return $fixtures->clean()
			->loadCommon('dictionaries')
			->loadCommon('notification')
			->loadCommon('settings_and_product')
			->loadCommon('coupon')
			->loadCommon('carrier')
			->loadCommon('payment')
			->loadCommon('sfGuard')
			->loadCommon('shop')
			->loadCommon('invoiceStatus')
			->loadCommon('order_package')
			->loadCommon('order')
			->loadCommon('loyalty_points')
			->loadCommon('invoice')
			->loadCommon('cashDesk');
	}
}