<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');


class StudioAuthAlgorithmReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
    }

    public function testCalculate()
    {
    	$this->assertEquals(studioAuthAlgorithm::calculate('qwerty'), 'b24f8e94be38c3d7a5ea');
    }
    
}