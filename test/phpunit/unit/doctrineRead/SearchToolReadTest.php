<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');


class SearchToolReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    public function testSearch()
    {
        $searchTool = new SearchTool();
        $searchTool->setLimit(10);
        $results = $searchTool->search('client');
        
        $this->assertEquals($results[0]['name'], 'client1');
        $this->assertEquals($results[0]['type'], 'Client');
        $this->assertEquals($results[1]['name'], 'client2');
        $this->assertEquals($results[1]['type'], 'Client');
        
        $results = $searchTool->search('john.doe@gmail.com');
        $this->assertEquals($results[0]['name'], 'John Doe');
        $this->assertEquals($results[0]['type'], 'sfGuardUser');
        
        $results = $searchTool->search('INVOICE');
        $this->assertEquals($results[0]['type'], 'Invoice');
        $this->assertContains('INVOICE/', $results[0]['name']);
        
        $results = $searchTool->search('INVOICE');
        $this->assertEquals($results[0]['type'], 'Invoice');
        $this->assertContains('INVOICE/', $results[0]['name']);
        
        $results = $searchTool->search(1);
        $this->assertEquals($results[0]['type'], 'Order');
        $this->assertContains('Card 100 pieces', $results[0]['name']);
    }
    
}