<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');


class NotificationsToolReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    public function testSMSsending()
    {
        $status = 1;
        /*
        TEST TURNED OFF. PLEASE FILL APPS WITH REAL SMS SENDING DATA AND UNCOMMENT CODE BELOW TO GET IT WORK
        
        $psms = new SmsNotificationAdapter();
        // test email sending (last parameter determines that sms will not be sent)
        $status = $psms->sendSms('123123123', 'test message',0,0,10);
        */
        
        $this->assertEquals(1 , $status);
    }

}