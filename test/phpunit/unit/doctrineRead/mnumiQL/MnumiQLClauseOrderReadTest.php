<?php

require_once(__DIR__ . '/../../../bootstrap/boostrap2.php');


class MnumiQLClauseOrderReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
       
    }

    /**
     * Tests parse() method;
     */
    public function testParse()
    {
        // 1.
        $query = 'FROM Client WHERE id > 1 ORDER BY id DESC';
        $fromObj = new MnumiQLClauseOrder($query);
        $this->assertEquals('id DESC', $fromObj->parse());

        // 2.
        $query = 'FROM Client WHERE id > 1 ORDER BY id, created_at DESC';
        $fromObj = new MnumiQLClauseOrder($query);
        $this->assertEquals('id, created_at DESC', $fromObj->parse());
        
        // 3.
        $query = 'FROM Client WHERE id > 1 OR id <> 1            ORDER BY id, created_at           DESC';
        $fromObj = new MnumiQLClauseOrder($query);
        $this->assertEquals('id, created_at DESC', $fromObj->parse());
    } 

    /**
     * Tests getQuery() method;
     */
    public function testGetQuery()
    {
        // 1. Checks if arguments were properly passed to Doctrine_Query
        $query = 'FROM Client WHERE id > 1 ORDER BY id DESC';
        $whereObj = new MnumiQLClauseOrder($query);
        $this->assertEquals('ORDER BY id DESC', trim($whereObj->getQuery()->getDql()));
        
        // 2. 
        $query = 'FROM Client WHERE id > 1 AND id <> 2 ORDER BY id, created_at DESC LIMIT 10';
        $whereObj = new MnumiQLClauseOrder($query);
        $this->assertEquals('ORDER BY id, created_at DESC', trim($whereObj->getQuery()->getDql()));
    } 
}