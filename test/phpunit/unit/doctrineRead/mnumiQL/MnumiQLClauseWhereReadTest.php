<?php

require_once(__DIR__ . '/../../../bootstrap/boostrap2.php');


class MnumiQLClauseWhereReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
       
    }

    /**
     * Tests parse() method;
     */
    public function testParse()
    {
        // 1.
        $query = 'FROM Client WHERE';
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('', $fromObj->parse());

        // 2.
        $query = 'FROM Client WherE id = 1 OR id=2';
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('id = 1 OR id=2', $fromObj->parse());
        
        // 3.
        $query = 'FROM Client WHERE id <> 1  ';
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('id <> 1', $fromObj->parse());
        
        // 4.
        $query = '      FROM    Client   WHERE    id>1 ';
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('id>1', $fromObj->parse());
        
        // 5.
        $query = 'FROM Client INNER JOIN Client.ClientAddresses WHERE id = 1 AND id <> 2';
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('id = 1 AND id <> 2', $fromObj->parse());
 
        // 6.
        $query = 'FROM Client WHERE id= 1 ORDER BY created_at DESC';
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('id= 1', $fromObj->parse());
       
        // 7.
        $query = 'FROM Client WHERE id = 1 LIMIT 10';
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('id = 1', $fromObj->parse());  
        
        // 8.
        $query = 'FROM Client WHERE id = 1 AND id <> 2 OR (id <> 3 AND id <> 4) order by created_at desc limit 10';
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('id = 1 AND id <> 2 OR (id <> 3 AND id <> 4)', $fromObj->parse());  
        
        // 9.
        $query = "SELECT Invoice.number, Invoice.invoice_type_name, Invoice.price_net, Invoice.price_gross, Invoice.invoice_status_name, Payment.label
                    FROM Invoice INNER JOIN Invoice.Payment Payment
                    WHERE DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') < '2014-10-10'";
        $fromObj = new MnumiQLClauseWhere($query);
        $this->assertEquals("DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') < '2014-10-10'", $fromObj->parse());  
    } 

    /**
     * Tests getQuery() method;
     */
    public function testGetQuery()
    {
        // 1. Checks if arguments were properly passed to Doctrine_Query
        $query = 'FROM Client WHERE id > 1';
        $whereObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('WHERE id > ?', trim($whereObj->getQuery()->getDql()));
        $paramArr = $whereObj->getQuery()->getParams();
        $this->assertEquals(array(1), $paramArr['where']);
        
        // 2.
        $query = 'FROM Client WHERE id > \'2\' OR (id <> \'1\' AND id <> \'2\')';
        $whereObj = new MnumiQLClauseWhere($query);
        $this->assertEquals('WHERE id > ? OR (id <> ? AND id <> ?)', trim($whereObj->getQuery()->getDql()));
        $paramArr = $whereObj->getQuery()->getParams();
        $this->assertEquals(array(2, 1, 2), $paramArr['where']);      
    }
}