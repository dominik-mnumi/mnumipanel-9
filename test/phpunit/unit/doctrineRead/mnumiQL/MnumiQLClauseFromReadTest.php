<?php

require_once(__DIR__ . '/../../../bootstrap/boostrap2.php');


class MnumiQLClauseFromReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
       
    }

    /**
     * Tests parse() method;
     */
    public function testParse()
    {
        // 1.
        $query = 'FROM Client';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());

        // 2.
        $query = 'FroM Client';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());
        
        // 3.
        $query = 'FROM Client ';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());
        
        // 4.
        $query = '      FROM    Client        ';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());
        
        // 5.
        $query = '      from    Client        ';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());
        
        // 6.
        $query = 'FROM Client INNER JOIN Client.ClientAddresses';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client INNER JOIN Client.ClientAddresses', $fromObj->parse());
        
        // 7.
        $query = 'FROM Client INNER JOIN Client.ClientAddresses ';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client INNER JOIN Client.ClientAddresses', $fromObj->parse());
        
        // 8.
        $query = 'FROM Client WHERE ';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());
        
        // 9.
        $query = 'FROM Client WHERE id = 1 or id = 2';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());
        
        // 10.
        $query = 'FROM Client ORDER BY created_at DESC';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());
        
        // 11.
        $query = 'FROM Client LIMIT 10';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Client', $fromObj->parse());  
        
        // 12.
        $query = "SELECT Invoice.number, Invoice.invoice_type_name, Invoice.price_net, Invoice.price_gross, Invoice.invoice_status_name, Payment.label
                    FROM Invoice INNER JOIN Invoice.Payment Payment
                    WHERE DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Invoice.created_at, '%Y-%m-%d') < '2014-10-10'";
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('Invoice INNER JOIN Invoice.Payment Payment', $fromObj->parse());     
    } 
    
    /**
     * Tests getQuery() method;
     */
    public function testGetQuery()
    {
        // 1. Checks if arguments were passed properly to Doctrine_Query
        $query = 'FROM Client';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('FROM Client', trim($fromObj->getQuery()->getDql()));
        
        // 2.
        $query = 'FROM ClientAddress';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('FROM ClientAddress', trim($fromObj->getQuery()->getDql()));  
        
        // 3.
        $query = 'FROM Client INNER JOIN Client.ClientAddresses';
        $fromObj = new MnumiQLClauseFrom($query);
        $this->assertEquals('FROM Client INNER JOIN Client.ClientAddresses', trim($fromObj->getQuery()->getDql()));  
    }
}