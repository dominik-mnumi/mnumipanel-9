<?php

require_once(__DIR__ . '/../../../bootstrap/boostrap2.php');


class MnumiQLClauseLimitReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
       
    }

    /**
     * Tests parse() method;
     */
    public function testParse()
    {
        // 1.
        $query = 'FROM Client WHERE id > 1 order by id desc LIMIT 10';
        $fromObj = new MnumiQLClauseLimit($query);
        $this->assertEquals('10', $fromObj->parse());
        
        // 2.
        $query = 'FROM Client WHERE id > 1 order by id desc LIMIT 5, 10';
        $fromObj = new MnumiQLClauseLimit($query);
        $this->assertEquals('5, 10', $fromObj->parse());
    } 

    /**
     * Tests getQuery() method;
     */
    public function testGetQuery()
    {
        // 1. Checks if arguments were properly passed to Doctrine_Query
        $query = 'FROM Client where id > 1 order by id desc LIMIT 10';
        $whereObj = new MnumiQLClauseLimit($query);
        $this->assertEquals('LIMIT 10', trim($whereObj->getQuery()->getDql()));
        
        // 2. 
        $query = 'FROM Client where id > 1 AND id <> 2 order by id, created_at desc LIMIT 5, 10';
        $whereObj = new MnumiQLClauseLimit($query);
        $this->assertEquals('LIMIT 5, 10', trim($whereObj->getQuery()->getDql()));
    } 
}