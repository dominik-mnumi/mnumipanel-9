<?php

require_once(__DIR__ . '/../../../bootstrap/boostrap2.php');


class MnumiQLClauseGroupReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->query1 = "SELECT User.username, UserContact.value 
                FROM UserContact INNER JOIN UserContact.User User LEFT JOIN User.Orders Orders
                WHERE DATE_FORMAT(Orders.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Orders.created_at, '%Y-%m-%d') < '2014-10-10' 
                GROUP BY User.id, UserContact.id HAVING COUNT(Orders.id) > 5 AND SUM(Orders.price_net) > 5000";
        
        $this->query2 = "FROM SfGuardUser INNER JOIN SfGuardUser.Orders Orders
                WHERE DATE_FORMAT(Orders.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Orders.created_at, '%Y-%m-%d') < '2014-10-10' 
                GROUP BY SfGuardUser.id HAVING COUNT(Orders.id) > 5 AND SUM(Orders.price_net) > 50";
    }

    /**
     * Tests parse() method;
     */
    public function testParse()
    {
        // 1.
        $groupObj = new MnumiQLClauseGroup($this->query1);
        $this->assertEquals('User.id, UserContact.id HAVING COUNT(Orders.id) > 5 AND SUM(Orders.price_net) > 5000', $groupObj->parse());

        // 2.
        $groupObj = new MnumiQLClauseGroup($this->query2);
        $this->assertEquals('SfGuardUser.id HAVING COUNT(Orders.id) > 5 AND SUM(Orders.price_net) > 50', $groupObj->parse());

    } 

    /**
     * Tests getQuery() method;
     */
    public function testGetQuery()
    {
        // 1. Checks if arguments were properly passed to Doctrine_Query
        $groupObj = new MnumiQLClauseGroup($this->query1);
        $this->assertEquals('GROUP BY User.id, UserContact.id HAVING COUNT(Orders.id) > 5 AND SUM(Orders.price_net) > 5000', trim($groupObj->getQuery()->getDql()));
   
        // 2. Checks if arguments were properly passed to Doctrine_Query        
        $groupObj = new MnumiQLClauseGroup($this->query2);
        $this->assertEquals('GROUP BY SfGuardUser.id HAVING COUNT(Orders.id) > 5 AND SUM(Orders.price_net) > 50', trim($groupObj->getQuery()->getDql()));
   
    } 
}