<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');

class PaymentImportRowReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $filename = __DIR__.'/files/transactions.csv';
        $paymentImportAdapterCsvObj = new PaymentImportAdapterCsv($filename);

        $coll = $paymentImportAdapterCsvObj->getRowColl();
        
        // view objects
        $this->rowObj = $coll[0];
    }
    
    /**
     * Checks row object.
     */
    public function testRowObj()
    {  
        $this->assertInstanceOf('PaymentImportRow', $this->rowObj);
    }
    
    /**
     * Checks date.
     */
    public function testGetDate()
    {  
        $this->assertEquals('2012-05-28', $this->rowObj->getDate());        
    }
    
    /**
     * Checks date.
     */
    public function testGetTitle()
    {  
        $this->assertEquals('Invoices VAT INVOICE/11/2012/4 (external transfer)', $this->rowObj->getTitle());        
    }
    
    /**
     * Checks date.
     */
    public function testGetAmount()
    {  
        $this->assertEquals('232.47', $this->rowObj->getAmount());        
    }
    
}