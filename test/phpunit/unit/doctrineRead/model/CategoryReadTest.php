<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class CategoryReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests canDelete method.
     */
    public function testCanDelete()
    {
    	$category = CategoryTable::getInstance()->findOneByName('Main');
    	$this->assertInstanceOf('Category', $category);
    	$this->assertFalse($category->canDelete(), 'Have subcategories and products');
    	
    	$category = CategoryTable::getInstance()->findOneByName('Boolkets');
    	$this->assertInstanceOf('Category', $category);
    	$this->assertFalse($category->canDelete(), 'Have subcategories without products');
    	
    	$category = CategoryTable::getInstance()->findOneByName('Bussiness cards');
    	$this->assertInstanceOf('Category', $category);
    	$this->assertFalse($category->canDelete(), 'Have products without subcategories');
    	
    	$category = CategoryTable::getInstance()->findOneByName('Catalogs');
    	$this->assertInstanceOf('Category', $category);
    	$this->assertTrue($category->canDelete(), 'Don\'t have any products and categories');
    }
    
    
    /**
     * Tests isActiveWithParents method.
     */
    public function testIsActiveWithParents()
    {
        $category = CategoryTable::getInstance()->findOneByName('Boolkets');
        $this->assertTrue($category->isActiveWithParents());
        
        $category = CategoryTable::getInstance()->findOneByName('Inactive parent');
        $this->assertFalse($category->isActiveWithParents());
        
        //parent is inactive so child is also inactive in shop
        $category = CategoryTable::getInstance()->findOneByName('Active child');
        $this->assertFalse($category->isActiveWithParents());
    }

}