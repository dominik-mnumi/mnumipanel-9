<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class ClientReadTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->client = ClientTable::getInstance()->findOneByFullname('client1');
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('invoiceCost'));
    }

    public function testRetrievingOrderPackages()
    {
        $this->assertTrue(count($this->client->getOrderPackages()) > 0);
        return $this->client->getOrderPackages();
    }

    /**
     * @depends testRetrievingOrderPackages
     */
    public function testIfAllRetrievedOrderPackagesAreInstancesOfOrderPackageAndIfClientAddressMatchesCurrentClient(Doctrine_Collection $packages)
    {
        foreach($packages as $package)
        {
            $this->assertInstanceOf('OrderPackage', $package);
            $this->assertEquals($package->ClientAddress->getClientId(), $this->client->getId());
        }
    }

    /**
     * @depends testRetrievingOrderPackages
     */
    public function testRetrievingOrderPackageForClientByIdWithCorrectId(Doctrine_Collection $packages)
    {
        $correct_id = $packages[0]->getId();
        $result = $this->client->findOrderPackage($correct_id);
        $this->assertInstanceOf('OrderPackage', $result);
        return $result;
    }

    /**
     * @depends testRetrievingOrderPackageForClientByIdWithCorrectId
     */
    public function testIfRetrievedOrderPackageByIdForSureIsPackageForCurrentClient(OrderPackage $package)
    {
        $this->assertEquals($package->ClientAddress->getClientId(), $this->client->getId());
    }

    /**
     * @depends testRetrievingOrderPackages
     */
    public function testRetrievingOrderPackageForClientByIdWithIncorrectId(Doctrine_Collection $packages)
    {
        $result = $this->client->findOrderPackage(10001);
        $this->assertFalse($result);
    }

    /**
     * Tests getAvailableCarriers method
     */
    public function testGetAvailableCarriers()
    {
        // gets all available without payment relation
        $count = $this->client->getAvailableCarriers()->count();
        $this->assertEquals(4, $count);
        
        // gets all with not existent payment relation
        $count = $this->client->getAvailableCarriers(999)->count();
        $this->assertEquals(0, $count);
        
        // gets all with existent payment relation 
        $count = $this->client
                ->getAvailableCarriers(PaymentTable::getInstance()
                        ->findOneByName(PaymentTable::$transfer21Days)->getId())
                ->count();
        $this->assertEquals(1, $count);
    }
    
    /**
     * Tests getAvailablePayments method
     */
    public function testGetAvailablePayments()
    {   
        // gets all with not existent payment relation
        $count = $this->client->getAvailablePayments(999)->count();
        $this->assertEquals(0, $count);
        
        // gets all with existent payment relation
        $carrier = CarrierTable::getInstance()->findOneByName('Courier');
        if(!$carrier instanceof Carrier)
        {
            throw new Exception('Could not get "Courier".');
        }
        
        $paymentObj = $this->client->getAvailablePayments($carrier->getId())
                ->getFirst();
        $this->assertEquals('test 1', $paymentObj->getName());
        
        // gets all with for office
        $count1 = PaymentTable::getInstance()->createQuery()
                ->where('(apply_on_all_pricelist = ? OR for_office = ?) AND active = ?', 
                        array(true, true, true))               
                ->count();
        
        $count2 = $this->client
                ->getAvailablePayments(null, true)
                ->count();
        $this->assertEquals($count1, $count2);
    }
    
    public function testSetPostcodeAndCity()
    {
        $this->client->setPostcodeAndCity('21-435 New york city');
      
        $this->assertEquals('21-435', $this->client->getPostcode());
        $this->assertEquals('New york city', $this->client->getCity());
      
        $this->client->setPostcodeAndCity('sss33xdpw-ds Warsaw');
      
        $this->assertEquals('sss33xdpw-ds', $this->client->getPostcode());
        $this->assertEquals('Warsaw', $this->client->getCity());
      
        $this->client->setPostcodeAndCity('xxxxxx Some other long named city');
      
        $this->assertEquals('xxxxxx', $this->client->getPostcode());
        $this->assertEquals('Some other long named city', $this->client->getCity());
    }
    
    /**
     * Tests isAffectedByCoupon method
     */
    public function testIsAffectedByCoupon()
    {
        $coupon = CouponTable::getInstance()->find('BonusCoupon');
        $coupon2 = CouponTable::getInstance()->find('BonusCoupon2');
        $coupon3 = CouponTable::getInstance()->find('CouponInactive');
      
        // afected by table coupon_pricelist
        $this->assertTrue($this->client->isAffectedByCoupon($coupon));
      
        // affected by apply for all pricelist
        $this->assertTrue($this->client->isAffectedByCoupon($coupon2));
      
        // not affected (pricelist)
        $this->assertFalse($this->client->isAffectedByCoupon($coupon3));
    }

}