<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class ClientAddressReadTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->client = ClientTable::getInstance()->findOneByFullname('client1');
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * test setPostcodeAndCity
     */
    public function testSetPostcodeAndCity()
    {
        $address = new ClientAddress();
        $address->setClient($this->client);
        $address->setPostcodeAndCity('21-435 New york city');
        
        $this->assertEquals('21-435', $address->getPostcode());
        $this->assertEquals('New york city', $address->getCity());
        
        $address->setPostcodeAndCity('sss33xdpw-ds Warsaw');
        
        $this->assertEquals('sss33xdpw-ds', $address->getPostcode());
        $this->assertEquals('Warsaw', $address->getCity());
        
        $address->setPostcodeAndCity('xxxxxx Some other long named city');
        
        $this->assertEquals('xxxxxx', $address->getPostcode());
        $this->assertEquals('Some other long named city', $address->getCity());
    }

}