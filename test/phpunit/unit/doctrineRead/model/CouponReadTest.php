<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');

class CouponObjReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('invoiceCost'));
    }

    /**
     * Test getAffectedProductsArray() method.
     */
    public function testGetAffectedProductsArray()
    {
        $coupon = CouponTable::getInstance()->find('BonusCoupon');
        $products = $coupon->getAffectedProductsArray();
        $this->assertEquals(1, count($products));
        
        $coupon = CouponTable::getInstance()->find('BonusCoupon2');
        $products = $coupon->getAffectedProductsArray();
        $this->assertEquals(0, count($products));
    }
    
    /**
     * Test getAffectedPricelistsArray() method.
     */
    public function testGetAffectedPricelistsArray()
    {
        $coupon = CouponTable::getInstance()->find('BonusCoupon');
        $pricelist = $coupon->getAffectedPricelistsArray();
        $this->assertEquals(1, count($pricelist));
      
        $coupon = CouponTable::getInstance()->find('BonusCoupon2');
        $pricelist = $coupon->getAffectedPricelistsArray();
        $this->assertEquals(0, count($pricelist));
    }
    
    /**
     * Test generateUniqueName()
     */
    public function testGenerateUniqueName()
    {
        $this->assertEquals(strlen(CouponTable::getInstance()->generateUniqueName()), 5);
        $this->assertEquals(strlen(CouponTable::getInstance()->generateUniqueName(10)), 10);
    }
}