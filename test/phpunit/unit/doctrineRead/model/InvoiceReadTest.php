<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class InvoiceReadTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{
    
    protected function _start()
    {
        // gets invoice object
        $this->invoice = InvoiceTable::getInstance()
        	->findOneByNumber(4);

        // preliminary invoice
        $this->preliminaryInvoiceObj = InvoiceTable::getInstance()
            ->findOneByNumberAndInvoiceTypeName(1, InvoiceTypeTable::$PRELIMINARY_INVOICE);
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->loadSnapshot('common'));

        $this->preliminaryInvoiceObj->updatePrice();
        $this->preliminaryInvoiceObj->save();
        $this->preliminaryInvoiceObj->refresh();
    }

    /**
     * Test instanceof 
     */
    public function testInstanceOf()
    {
    	$this->assertInstanceOf('Invoice', $this->invoice);
    }
    
    public function testCopyWithItems()
    {
    	$correction = $this->invoice->copyWithItems(InvoiceTypeTable::$CORRECTION_INVOICE);
    	$this->assertInstanceOf('Invoice', $correction);
    	
    	$this->assertEquals($correction->getParentId(), $this->invoice->getId());
    	$this->assertTrue($this->invoice->hasChildren());
    	$this->assertFalse($correction->hasChildren());
    	$this->assertEquals($this->invoice->getCorrectionId(), $correction->getId());
    	$this->assertFalse($correction->hasChildren());
    }

    /**
     * Tests getCalculatedPriceNet() method. 
     */
    public function testGetCalculatedPriceNet()
    {       
        $price = $this->invoice->getCalculatedPriceNet();
        
        $priceNet = (10*440)+(10*500)+(10*80-20);
        
        $this->assertEquals($priceNet, $price);
    }
    
    /**
     * Tests getCalculatedPriceGross() method. 
     */
    public function testGetCalculatedPriceGross()
    {       
        $price = $this->invoice->getCalculatedPriceGross();
        
        $this->assertEquals(12490.20, $price);
    }
    
    /**
     * Tests getCalculatedTaxAmount() method. 
     */
    public function testGetCalculatedTaxAmount()
    {       
        $price = $this->invoice->getCalculatedTaxAmount();
        
        $this->assertEquals(2310.20, $price);
    }
    
    /**
     * Tests getTotalCurrentTaxGroupedByTax() method. 
     */
    public function testGetTotalCurrentTaxGroupedByTax()
    {       
        // 23% VAT
        $taxObj = TaxTable::getInstance()->find(23);
        $price = $this->invoice->getTotalCurrentTaxGroupedByTax($taxObj);
        
        $this->assertEquals(2162.00, $price);
        
        // 19% VAT
        $taxObj = TaxTable::getInstance()->find(19);
        $price = $this->invoice->getTotalCurrentTaxGroupedByTax($taxObj);
        
        $this->assertEquals(148.20, $price);
    }
    
    /**
     * Tests getTotalCurrentPriceAfterDiscountGroupedByTax() method. 
     */
    public function testGetTotalCurrentPriceAfterDiscountGroupedByTax()
    {       
        // 23% VAT
        $taxObj = TaxTable::getInstance()->find(23);
        $price = $this->invoice->getTotalCurrentPriceAfterDiscountGroupedByTax($taxObj);
        
        $this->assertEquals(9400.00, $price);
        
        // 19% VAT
        $taxObj = TaxTable::getInstance()->find(19);
        $price = $this->invoice->getTotalCurrentPriceAfterDiscountGroupedByTax($taxObj);
        
        $this->assertEquals(780.00, $price);
    }
    
    /**
     * Tests getTotalCurrentGrossPriceGroupedByTax() method.
     */
    public function testGetTotalCurrentGrossPriceGroupedByTax()
    {       
        // 23% VAT
        $taxObj = TaxTable::getInstance()->find(23);
        $price = $this->invoice->getTotalCurrentGrossPriceGroupedByTax($taxObj);
        
        $this->assertEquals(11562.00, $price);
        
        // 19% VAT
        $taxObj = TaxTable::getInstance()->find(19);
        $price = $this->invoice->getTotalCurrentGrossPriceGroupedByTax($taxObj);
        
        $this->assertEquals(928.20, $price);
    }
    
    /**
     * Check formated number for invoice
     */
    public function testGetFormattedNumber()
    {
        $this->assertEquals($this->invoice->getFormattedNumber('INVOICE/mm/yy/{id}'), 'INVOICE/'.date('m', strtotime($this->invoice->getCreatedAt())).'/'
            .date('Y', strtotime($this->invoice->getCreatedAt())).'/'.$this->invoice->getId());
    }

}