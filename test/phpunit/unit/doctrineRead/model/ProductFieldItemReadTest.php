<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class ProductFieldItemReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->pricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests delete process of ProductFieldItem.
     * Product and ProductField should be untouched.
     */
    public function testProductFieldItemCascadeDelete()
    {
        $productObj = ProductTable::getInstance()->findOneByName('product 1');
        $productId = $productObj->getId();
        $productFieldColl = ProductFieldTable::getInstance()->findByProductId($productId);
        $productFieldCount = ProductFieldTable::getInstance()->findByProductId($productId)->count();
        $productFieldArray = array();

        foreach($productFieldColl as $rec)
        {
            $productFieldArray[] = $rec->getId();
        }

        $productFieldItemColl = ProductFieldItemTable::getInstance()->createQuery()
                ->whereIn('product_field_id', $productFieldArray)
                ->execute();

        $productFieldItemCount = $productFieldItemColl->count();

        //asserts before deletion
        $this->assertEquals(12, $productFieldCount);
        $this->assertEquals(17, $productFieldItemCount);

        //deletes all product field items of product 1
        $productFieldItemColl->delete();
        
        $productFieldItemColl = ProductFieldItemTable::getInstance()->createQuery()
                ->whereIn('product_field_id', $productFieldArray)
                ->execute();

        //asserts after deletion
        $this->assertEquals(12, $productFieldColl->count());
        $this->assertEquals(0, $productFieldItemColl->count());
        
        //checks if reference does not deleted product 1
        $productObj = ProductTable::getInstance()->findOneByName('product 1');
        
        $this->assertEquals('product 1', $productObj->getName());
        
        //check fieldsets if left untouched
        $this->assertEquals(1, FieldsetTable::getInstance()->findByName('GENERAL')->count());
    }  

}
