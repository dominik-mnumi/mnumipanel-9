<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class FieldItemPriceReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->pricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadOwn('fieldItemPrice'));
    }

    /**
     * Tests getPriceBeetween() method, test maximal, minimal and double price.
     */
    public function testGetPriceBeetween()
    {
        // gets field item - paper_x
        $fieldItemObj = FieldItemTable::getInstance()->findOneByName('Paper X');
        $priceObj = $fieldItemObj->getFieldItemPrice($this->pricelist);

        $this->assertEquals(3, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_page, 1));
        $this->assertEquals(3, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_page, 2));
        $this->assertEquals(2, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_page, 10));
        $this->assertEquals(2, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_page, 20));

        //gets field item - paper_y - square metre
        $fieldItemObj = FieldItemTable::getInstance()->findOneByName('Paper Y');
        $priceObj = $fieldItemObj->getFieldItemPrice($this->pricelist);

        $this->assertEquals(10, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 1));
        $this->assertEquals(8, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 10));
        
        // another tests with more price range
        // gets field item - paper_z
        $fieldItemObj = FieldItemTable::getInstance()->findOneByName('product 3 material');
        $priceObj = $fieldItemObj->getFieldItemPrice($this->pricelist);

        $this->assertEquals(46, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 0.99));
        $this->assertEquals(46, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 1));
        $this->assertEquals(46, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 1.01));
        $this->assertEquals(46, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 4.99));
        $this->assertEquals(24, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 5));
        $this->assertEquals(24, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 5.01));
        $this->assertEquals(24, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 19.99));
        $this->assertEquals(8, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 20));
        $this->assertEquals(8, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 20.01));
        $this->assertEquals(8, $priceObj->getPriceBeetween(FieldItemPriceTypeTable::$price_square_metre, 100));
    }

    /**
     * Tests percent getPercentPriceValue.
     */
    public function testGetPercentPriceValue()
    {
        $fieldItemPriceQuantityObj = FieldItemPriceQuantityTable::getInstance()->findOneByPriceAndType(50, 'percent');

        $fieldItemPriceObj = $fieldItemPriceQuantityObj->getFieldItemPrice();
        $price = $fieldItemPriceObj->getPercentPriceValue('price_page', 1, 50);

        $this->assertEquals(1.50, $price);
    }

}