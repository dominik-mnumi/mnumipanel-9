<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');

class ProductReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->object =  ProductTable::getInstance()->findOneBySlug('product-1');
	    $this->object2 =  ProductTable::getInstance()->findOneBySlug('product-2');
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests Product general elements.
     */
    public function testGeneral()
    {
        $this->assertInstanceOf('Product', $this->object);
    }
    
    /**
     * Tests findActiveBySlug() method.
     */
    public function testFindActiveBySlug()
    {
        // check if find "product-1"
        $active = ProductTable::getInstance()->findActiveBySlug('product-1');
        $this->assertInstanceOf('Product', $active);

        // find if "inactive" product exist
        $inactive = ProductTable::getInstance()->findOneBySlug('inactive');
        $this->assertInstanceOf('Product', $active);
        
        // check if "inactive" product is not choose
        $inactive = ProductTable::getInstance()->findActiveBySlug('inactive');
        $this->assertFalse($inactive);
    }
    
    /**
     * Tests findActiveByCategoryId() method.
     */
    public function testFindActiveByCategoryId()
    {
        $cat = CategoryTable::getInstance()->findOneBySlug('bussiness-cards');
        $products = ProductTable::getInstance()->findActiveByCategoryId($cat->getId());
        
        $this->assertInstanceOf('Doctrine_Collection', $products);
        $this->assertGreaterThan(0, count($products));
    }
    

    /**
     * Tests getProductFieldsetsForPriceDefaultArray() method. 
     */
    public function testGetProductFieldsetsForPriceDefaultArray()
    {
        //gets default fieldsets array
        $defaultFields = $this->object->getProductFieldsetsForPriceDefaultArray();
        foreach($defaultFields as $rec)
        {
            $this->assertTrue(is_array($rec));
        }
    }

    /**
     * Tests getProductFieldsetsForPrice() method. 
     */
    public function testGetProductFieldsetsForPrice()
    {
        //gets fieldsets for price
        $productFieldsetColl = $this->object->getProductFieldsetsForPrice();

        $this->assertInstanceOf('Doctrine_Collection', $productFieldsetColl);
    }
    
    /**
     * Tests getProductFixpriceByPriceListId method.
     */
    public function testGetProductFixpriceByPriceListId()
    {
        $this->assertInstanceOf('Doctrine_Collection', $this->object->getProductFixpriceByPriceListId());
    }
    
    /**
     * Tests isActiveInCategory method.
     */
    public function testIsActiveInCategory()
    {
        $this->assertTrue($this->object->isActiveInCategory());
        
        $product = ProductTable::getInstance()->findOneByName('product in inactive category');
        $this->assertFalse($product->isActiveInCategory());
        
        $product = ProductTable::getInstance()->findOneByName('product in active category but inactive parent');
        $this->assertFalse($product->isActiveInCategory());
    }

	/**
	 * Tests getProductFixPriceBeetween method.
	 *
	 * Tests two products. One has fixed prices and order package option is enabled. Second has defined fixed prices
	 * but order package option is not enabled.
	 */
	public function testGetProductFixPriceBeetween()
	{
		// tests for product with package order enabled
		$quantity = 1000;
		$pricelistId = PricelistTable::getInstance()->getDefaultPricelist()->getId();

		// tests for object
		$this->assertInstanceOf('ProductFixprice', $this->object->getProductFixPriceBeetween($quantity, $pricelistId));

		// tests for object2 (fixprice is defined for 10000, but order package is disabled)
		$quantity = 10000;

		$this->assertFalse($this->object2->getProductFixPriceBeetween($quantity, $pricelistId));
	}


}


?>
