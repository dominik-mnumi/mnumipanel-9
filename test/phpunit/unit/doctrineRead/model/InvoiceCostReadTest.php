<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');
require_once(__DIR__.'/../../../PhpunitSnapshotFactory.class.php');



class InvoiceCostReadTest extends BasePhpunitTestCase
implements sfPhpunitFixtureDoctrineAggregator
{
	/***
	 * @class InvoiceCost
	 */
	protected $invoice;
	
    protected function _start()
    {
    	// gets invoice object
    	$this->invoice = Doctrine_Query::create()
	    	->from('InvoiceCost ic')
	    	->where('ic.invoice_number = ?', '14/12/2011')
	    	->fetchOne();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->loadSnapshot('invoiceCost'));
    }

    public function testInstanceOf()
    {
    	$this->assertInstanceOf('InvoiceCost', $this->invoice);
    }

}