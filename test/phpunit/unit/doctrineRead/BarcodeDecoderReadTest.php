<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');

class BarcodeDecoderReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    public function testGetObject()
    {
        $barcode = new BarcodeDecoder('^1000000001');
        $barcode->getObject();
        
        $this->assertEquals(get_class($barcode->getObject()) , 'sfGuardUser');
        $this->assertContains('/user/edit/'.$barcode->getObject()->getId(), $barcode->getObject()->getEditUrl());
        
        $barcode = new BarcodeDecoder('^1100000001');
        $barcode->getObject();
        
        $this->assertEquals(get_class($barcode->getObject()) , 'Client');
        $this->assertContains('/client/edit/'.$barcode->getObject()->getId(), $barcode->getObject()->getEditUrl());
        
        $barcode = new BarcodeDecoder('^1200000001');
        $barcode->getObject();
        
        $this->assertEquals(get_class($barcode->getObject()) , 'Order');
        $this->assertContains('/order/edit/'.$barcode->getObject()->getId(), $barcode->getObject()->getEditUrl());
        
    }
    
}