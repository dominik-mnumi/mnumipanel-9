<?php

require_once(__DIR__.'/../../bootstrap/boostrap2.php');


class CalculationToolReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    protected $slug, $slug2;
    protected $fields = array();
    protected $pricelistId = 1;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
    	$this->pricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();

        $this->slug = 'product-1';
        $this->slug2 = 'product-2';
        $this->fields['SIZE']['name'] = 'SIZE';
        $this->fields['SIZE']['value'] = FieldItemTable::getInstance()->findOneByName('A3')->getId();
        $this->fields['QUANTITY']['name'] = 'QUANTITY';
        $this->fields['QUANTITY']['value'] = 1;
        $this->fields['COUNT']['name'] = 'COUNT';
        $this->fields['COUNT']['value'] = 1;
        $this->fields['MATERIAL']['name'] = 'MATERIAL';
        $this->fields['MATERIAL']['value'] = FieldItemTable::getInstance()->findOneByName('Paper X')->getId();
        $this->fields['PRINT']['name'] = 'PRINT';
        $this->fields['PRINT']['value'] = FieldItemTable::getInstance()->findOneByName('Print X')->getId();
        $this->fields['SIDES']['name'] = 'SIDES';
        $this->fields['SIDES']['value'] = FieldItemTable::getInstance()->findOneByName('Single')->getId();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Create new Calculation tool instance
     *
     * @param $slug
     * @param $fields
     * @param int|null $pricelistId
     * @return calculationTool
     */
    private function initializeCalculationTool($slug, $fields, $pricelistId = null)
    {
        if ($pricelistId == null) {
            $pricelistId = $this->pricelistId;
        }


        $product = ProductTable::getInstance()->find($slug);

        $calculationProductData = new ProductCalculationProductData($product);

        $calculationTool = new calculationTool($calculationProductData->getCalculationProductDataObject());
        $calculationTool->initialize($fields, $pricelistId);

        return $calculationTool;
    }
    
    /**
     * Tests getFactor() method. calculationTool.class.php.
     */
    public function testGetFactor()
    {
        // printer is: 320,00 x 450,00
        $session_handle = null;

        $slug = $this->slug;
        $fields = $this->fields;

        // 1. simple 1 to 1
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        $factor = $report['factor'];

        $this->assertEquals(1, $factor);

        // 2. SIZE 2 times smaller
        $fields['SIZE']['value'] = FieldItemTable::getInstance()->findOneByName('A4')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        $factor = $report['factor'];
        
        $this->assertEquals(1, $factor);

        // 3. SIZE much bigger
        $fields['SIZE']['value'] = FieldItemTable::getInstance()->findOneByName('A0')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        $factor = $report['factor'];
        
        $this->assertEquals(8, $factor);
    }

    /**
     * Tests getMeasureType() method for MATERIAL.
     */
    public function testGetMeasureType()
    {
        //prepares input array.
        $slug = $this->slug2;
        $fields = $this->fields;

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        $measureType = $report['measureType'];

        // 1. price_page
        $this->assertEquals('price_page', $measureType);
        
        // 2. price_square_metre
        $fields['MATERIAL']['name'] = 'MATERIAL';
        $fields['MATERIAL']['value'] = FieldItemTable::getInstance()->findOneByName('Paper Y')->getId();
        $fields['PRINT']['name'] = 'PRINT';
        $fields['PRINT']['value'] = FieldItemTable::getInstance()->findOneByName('Print Y')->getId();
 
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        $measureType = $report['measureType'];
        $this->assertEquals('price_square_metre', $measureType);
        
        // for order package when price i fixed
        $slug = $this->slug;
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        $measureType = $report['measureType'];

        // 1. price_copy - despite th fact that material is price_page 
        // for product-1
        $this->assertEquals('price_copy', $measureType);
    }

    /**
     * Tests getComponentPrice().
     */
    public function test1GetComponentPrice()
    {
        $slug = $this->slug2;
        $fields = $this->fields;

        // 0.
        // - size: A3,
        // - count: 2,
        // - quantity: 1,
        // - sides: single,
        // - paper x,
        // - print x;
        //

        //COUNT = 2
        $fields['COUNT']['value'] = 2;
        $fields['QUANTITY']['value'] = 1;

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();

        $this->assertEquals(3 * 2, $report['priceItems']['MATERIAL']['price']);
        $this->assertEquals(2 * 2, $report['priceItems']['PRINT']['price']);

        //COUNT = 100
        $fields['COUNT']['value'] = 49;
        $fields['QUANTITY']['value'] = 1;
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(98, $report['priceItems']['MATERIAL']['price']);
        $this->assertEquals(98, $report['priceItems']['PRINT']['price']);

        // 1.
        // - size: 90x50,
        // - count: 1,
        // - quantity: 40,
        // - sides: single,
        // - paper x,
        // - print x;
        //

        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 40;

        // sets 90x50
        $fields['SIZE']['value'] = FieldItemTable::getInstance()->findOneByName('Business card - 9x5')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(6, $report['priceItems']['MATERIAL']['price']);
        $this->assertEquals(4, $report['priceItems']['PRINT']['price']);

        // 2.
        // - size: A4,
        // - count: 1,
        // - quantity: 10,
        // - sides: simplex,
        // - paper y,
        // - print y;
        //

        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 10;

        // sets A4
        $fields['SIZE']['value'] = FieldItemTable::getInstance()->findOneByName('A4')->getId();

        // sides simplex
        $fields['SIDES']['value'] = FieldItemTable::getInstance()->findOneByName('Single')->getId();

        // paper y
        $fields['MATERIAL']['value'] = FieldItemTable::getInstance()->findOneByName('Paper Y')->getId();

        // print y
        $fields['PRINT']['value'] = FieldItemTable::getInstance()->findOneByName('Print Y')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();

        $this->assertEquals(6.24, round($report['priceItems']['MATERIAL']['price'], 2));
        $this->assertEquals(12.47, round($report['priceItems']['PRINT']['price'], 2));
    }
    
     /**
     * Tests getComponentPrice() for others.
     */
    public function test2GetComponentPrice()
    {
        $slug = $this->slug;
        $fields = $this->fields;
 
        // 0.
        // - size: A3,
        // - count: 1,
        // - quantity: 1,
        // - sides: single,
        // - paper x,
        // - print x;
        //

        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 1;
        
        // OTHER 1
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Bindery')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();

        $this->assertEquals(3.10, round($report['priceItems']['OTHER'][0]['price'], 2));
        
        // OTHER 2
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Cutting')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(17.83, round($report['priceItems']['OTHER'][0]['price'], 2));
                
        // tests per_page for OTHER
        //for COUNT = 1
        // SIZE = Business card - 10x16
        $fields = $this->fields;
        $fields['COUNT']['value'] = 1;
        $fields['SIZE']['name'] = 'SIZE';
        $fields['SIZE']['value'] = FieldItemTable::getInstance()->findOneByName('Business card - 10x16')->getId();       
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Bindery')->getId();
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(3.10, round($report['priceItems']['OTHER'][0]['price'], 2));
        
        //for COUNT = 31
        $fields['COUNT']['value'] = 31;
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(3.40, round($report['priceItems']['OTHER'][0]['price'], 2));
               
        //for per item (COUNT = 1)
        //reset fields
        $fields = $this->fields;
        $fields['COUNT']['value'] = 1;
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Something per item')->getId();
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(10, round($report['priceItems']['OTHER'][0]['price'], 2));
        
        //(COUNT = 8)
        $fields['COUNT']['value'] = 8;
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(80, round($report['priceItems']['OTHER'][0]['price'], 2));
    }

    /**
     * Tests max and min component price.
     */
    public function test2MaxAndMinComponentPrice()
    {
        $slug = $this->slug;
        $fields = $this->fields;

        // 1.
        // - size: A4,
        // - count: 5,
        // - quantity: 100,
        // - sides: duplex,
        // - paper x,
        // - print x;
        //

        $fields['COUNT']['value'] = 5;
        $fields['QUANTITY']['value'] = 100;

        // sets A4
        $fields['SIZE']['value'] = FieldItemTable::getInstance()->findOneByName('A4')->getId();

        // sides duplex
        $fields['SIDES']['value'] = FieldItemTable::getInstance()->findOneByName('Double')->getId();

        $priceObj = $this->initializeCalculationTool($this->slug2, $fields);
        $report = $priceObj->fetchReport();
        
        //max is 100 for MATERIAL AND PRINT
        $this->assertEquals(100, round($report['priceItems']['MATERIAL']['price'], 2));
        $this->assertEquals(100, round($report['priceItems']['PRINT']['price'], 2));
        
        //count = 1
        //quantity = 1
        //min is 3 for MATERIAL AND PRINT
        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 1;

        $priceObj = $this->initializeCalculationTool($this->slug2, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(3, round($report['priceItems']['MATERIAL']['price'], 2));
        $this->assertEquals(3, round($report['priceItems']['PRINT']['price'], 2));
    }
    
    /**
     * Tests getSummaryPriceNet() with other fieldsets.
     */
    public function testGetSummaryPriceNet()
    {
        $slug = $this->slug;
        $fields = $this->fields;

        // 0.
        // - size: A3,
        // - count: 1,
        // - quantity: 1,
        // - sides: single,
        // - paper x,
        // - print x;
        //

        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 1;
        
        // OTHER (one fieldset)
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Bindery')->getId();

        $priceObj = $this->initializeCalculationTool($this->slug, $fields);
        $report = $priceObj->fetchReport();
        $this->assertEquals(10.10, round($report['summaryPriceNet'], 2));
        
        
        // OTHER (two different fieldsets)
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Bindery')->getId();
        
        $fields['OTHER'][1]['name'] = 'OTHER';
        $fields['OTHER'][1]['value'] = FieldItemTable::getInstance()->findOneByName('Cutting')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(27.93, round($report['summaryPriceNet'], 2));
      
        // OTHER (two the same other fieldsets) - known bug in edit order. 
        // Calculation counts good but default values in form are bad.
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Cutting')->getId();
        
        $fields['OTHER'][1]['name'] = 'OTHER';
        $fields['OTHER'][1]['value'] = FieldItemTable::getInstance()->findOneByName('Cutting')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(42.66, round($report['summaryPriceNet'], 2));    
        
        // OTHER (three different fieldsets with per item price)
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Bindery')->getId();
        
        $fields['OTHER'][1]['name'] = 'OTHER';
        $fields['OTHER'][1]['value'] = FieldItemTable::getInstance()->findOneByName('Cutting')->getId();
        
        $fields['OTHER'][2]['name'] = 'OTHER';
        $fields['OTHER'][2]['value'] = FieldItemTable::getInstance()->findOneByName('Something per item')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(37.93, round($report['summaryPriceNet'], 2));
       
    }
    
    /**
     * Tests value = 0 of other fieldsets.
     */
    public function testOtherZeroValue()
    {
        $slug = $this->slug;
        $fields = $this->fields;

        // 0.
        // - size: A3,
        // - count: 1,
        // - quantity: 1,
        // - sides: single,
        // - paper x,
        // - print x;
        //

        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 1;
        
        // OTHER (normal value)
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Bindery')->getId();
        
        // (value = 0)
        $fields['OTHER'][1]['name'] = 'OTHER';
        $fields['OTHER'][1]['value'] = 0;

        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        // price for product with only one other fieldset value
        $this->assertEquals(10.1, round($report['summaryPriceNet'], 2));
    }
    
    /**
     * Tests fixed price.
     */
    public function testFixedPrice()
    {
        $slug = $this->slug;
        $fields = $this->fields;
        
        // 0.
        // - size: A3,
        // - count: 1,
        // - quantity: 1,
        // - sides: single,
        // - paper x,
        // - print x;
        //
        
        //for QUANTITY 1000
        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 1000;
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(10000, round($report['summaryPriceNet'], 2)); 
        
        //for QUANTITY 2000
        $fields['QUANTITY']['value'] = 2000;
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(16000, round($report['summaryPriceNet'], 2));   
    }
    
    /**
     * Tests metric size.
     */
    public function testMetricSize()
    {
        $slug = $this->slug;
        $fields = $this->fields;
        
        // 0.
        // - size: Customizable,
        // - count: 1,
        // - quantity: 1,
        // - sides: single,
        // - paper x,
        // - print x;
        //
        
        //for QUANTITY 1000
        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 1;
        $fields['SIZE']['value'] = FieldItemTable::getInstance()->findOneByName('- Customizable -')->getId();
        $fields['SIZE']['width']['value'] = 500;
        $fields['SIZE']['height']['value'] = 500;
        
        // ### for mm ###
        $fields['SIZE']['metric']['value'] = 'mm';
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        // factor
        $this->assertEquals(4, round($report['factor'], 2)); 
        
        // ### for cm ###
        $fields['SIZE']['metric']['value'] = 'cm';
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        
        // factor
	$expected = (16*11);  // row:16(5000/320), col:11(5000/488)
        $this->assertEquals(176, $report['factor']); 
    }
    
    /**
     * Tests fixed price for default and vip pricelist.
     */
    public function testVipAndDefaultFixedPrice()
    {
        $slug = $this->slug;
        $fields = $this->fields;
        $fields['QUANTITY']['value'] = 10000;
        // - count: 1,
        // - quantity: 10000,
        // - sides: single,
        // - paper x,
        // - print x;
        //
     
        // ### for mm ###
        $fields['SIZE']['metric'] = 'mm';
   
        // gets default pricelist
        $pricelistId = $this->pricelist;
        
        $priceObj = $this->initializeCalculationTool($slug, $fields, $pricelistId);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(100000000, round($report['summaryPriceNet'], 2));   
        
        // gets VIP pricelist
        $pricelistId = PricelistTable::getInstance()->findOneByName('Very important Person')->getId();
        
        $priceObj = $this->initializeCalculationTool($slug, $fields, $pricelistId);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(50000000, round($report['summaryPriceNet'], 2));   
        
        // VIP pricelist without fixed price entries -> should take fixed price from default pricelist
        $priceObj = $this->initializeCalculationTool('product-2', $fields, $pricelistId);
        $report = $priceObj->fetchReport();
        
        $this->assertEquals(40000000, round($report['summaryPriceNet'], 2));   
        
    }
    
     /**
     * Tests empty field for others.
     */
    public function testEmptyField()
    {
        $slug = $this->slug;
        $fields = $this->fields;
 
        // 0.
        // - size: A3,
        // - count: 1,
        // - quantity: 1,
        // - sides: single,
        // - paper x,
        // - print x;
        //

        //for QUANTITY 1000
        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 1000;
        
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        $price = $report['summaryPriceNet'];
        $price1 = round($price, 2); 
              
        // OTHER 1
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->getEmptyField()->getId();
   
        $priceObj = $this->initializeCalculationTool($slug, $fields);
        $report = $priceObj->fetchReport();
        $price = $report['summaryPriceNet'];
        $price2 = round($price, 2); 
        
        // no changes with empty field
        $this->assertEquals($price1, $price2);
        
    }

    /**
     * Tests percent pricelist.
     */
    public function test1PercentComponentPrice()
    {
        // gets VIP pricelist
        $vipPricelistId = PricelistTable::getInstance()->findOneByName('Very important Person')->getId();

        $slug = $this->slug;
        $fields = $this->fields;

        // 0.
        // - size: A3,
        // - count: 2,
        // - quantity: 1,
        // - sides: single,
        // - paper x,
        // - print x;
        //

        // COUNT = 1
        $fields['COUNT']['value'] = 1;
        $fields['QUANTITY']['value'] = 1;

        /*
        other_bindery_1_1:
          FieldItemPrice: other_1_price
          FieldItemPriceType: PricePage
          quantity: '1.00'
          price: '0.10'
        other_bindery_1_2:
          FieldItemPrice: other_1_price
          FieldItemPriceType: PriceCopy
          quantity: '1.00'
          price: '3.00'

        Price from default: 3.10


        other_bindery_1_3:
          FieldItemPrice: other_1_price_vip
          FieldItemPriceType: PricePage
          quantity: '1.00'
          price: '50'
          type: percent
        other_bindery_1_4:
          FieldItemPrice: other_1_price_vip
          FieldItemPriceType: PriceCopy
          quantity: '1.00'
          price: '50'
          type: percent

          Price from vip: 1.55
        */

        // OTHER 1
        $fields['OTHER'][0]['name'] = 'OTHER';
        $fields['OTHER'][0]['value'] = FieldItemTable::getInstance()->findOneByName('Bindery')->getId();

        $priceObj = $this->initializeCalculationTool($slug, $fields, $vipPricelistId);
        $report = $priceObj->fetchReport();

        $this->assertEquals(1.55, $report['priceItems']['OTHER'][0]['price']);
    }
}
