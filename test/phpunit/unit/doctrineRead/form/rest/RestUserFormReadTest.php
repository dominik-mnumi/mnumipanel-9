<?php

require_once(__DIR__.'/../../../../bootstrap/boostrap2.php');

class RestUserFormReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->userObj = sfGuardUserTable::getInstance()->findOneByEmailAddress('john.doe@gmail.com');
        $this->userFormData = array(
            'first_and_last_name' => 'Bobby Max Duck',
            'email_address' => 'bobbymax123@gmail.com');
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Test RestUserWrongEmail
     */
    public function testRestUserWrongEmail()
    {
        $this->userFormData['email_address'] = 'wrong_address';
        $form = new RestUserForm($this->userObj, array('userFormData' => $this->userFormData));
        
        $this->isValidForm($form, $this->userFormData, false, array('email_address'));    
    }

    /**
     * Method to check if form is valid
     */
    private function isValidForm($form, $formData, $isValid, $errorFields = array())
    {
        $form->bind($formData);
        $this->assertEquals($form->isValid(), $isValid);
      
        foreach($form->getErrorSchema()->getErrors() as $field => $message)
        {
            $this->assertEquals(in_array($field, $errorFields), true);
        }
    }
}