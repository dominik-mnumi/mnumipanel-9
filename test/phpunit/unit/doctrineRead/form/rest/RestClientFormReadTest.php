<?php

require_once(__DIR__.'/../../../../bootstrap/boostrap2.php');

class RestClientFormReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->clientObj = ClientTable::getInstance()->findOneByFullname('client1');
        $this->clientAddressObj = ClientAddressTable::getInstance()->findOneByFullname('address1');

        if($this->clientObj && $this->clientAddressObj)
        {
            $this->clientFormData = array(
                'fullname' => 'New client name',
                'city' => 'New York',
                'street' => 'Washington street',
                'postcode' => '11-222',
                'tax_id' => '123-45-67-819'
            );

            $this->deliveryData = array(
                'fullname' => 'New client address name',
                'city' => 'New York',
                'street' => 'Washington street',
                'postcode' => '11-222',
                'client_id' => $this->clientAddressObj->getClient()->getId()
            );

            $this->addressData = array(
                'fullname' => 'New client address name',
                'postcodeAndCity' => '11-333 Chicago',
                'street' => 'Washington street',
                'client_id' => $this->clientAddressObj->getClient()->getId()
            );
        }
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Test RestClientNoFullname
     */
    public function testRestClientNoFullname()
    {
        $clientFormData = $this->clientFormData;

        $clientFormData['fullname'] = '';
        
        $form = new RestClientForm($this->clientObj, array('clientFormData' => $clientFormData));
        
        $this->isValidForm($form, $clientFormData, false, array('fullname'));
    }
    
    /**
     * Test RestClientWrongTaxId
     */
    public function testRestClientWrongTaxId()
    {
        $clientFormData = $this->clientFormData;

        $clientFormData['tax_id'] = 'abc-45-67-819';
        
        $form = new RestClientForm($this->clientObj, array('clientFormData' => $clientFormData));
        
        $this->isValidForm($form, $clientFormData, false, array('tax_id'));
    }

    /**
     * Test RestClientDeliveryNoFullname
     */
    public function testRestClientDeliveryNoFullname()
    {
        $deliveryData = $this->deliveryData;

        // no fullname
        $deliveryData['fullname'] = '';
        
        $form = new RestClientDeliveryForm($this->clientAddressObj);
        
        $this->isValidForm($form, $deliveryData, false, array('fullname'));
    }
    
    /**
     * Test RestClientDeliveryNoStreet
     */
    public function testRestClientDeliveryNoStreet()
    {
        $deliveryData = $this->deliveryData;

        $deliveryData['street'] = '';
        
        $form = new RestClientDeliveryForm($this->clientAddressObj);
        
        $this->isValidForm($form, $deliveryData, false, array('street'));
    }
    
    /**
     * Test RestClientDeliveryNoCity
     */
    public function testRestClientDeliveryNoCity()
    {
        $deliveryData = $this->deliveryData;

        $deliveryData['city'] = '';
        
        $form = new RestClientDeliveryForm($this->clientAddressObj);

        $this->isValidForm($form, $deliveryData, false, array('city'));
    }
    
    /**
     * Test ClientDeliveryNoPostcode
     */
    public function testRestClientDeliveryNoPostcode()
    {
        $deliveryData = $this->deliveryData;

        $deliveryData['postcode'] = '';
        
        $form = new RestClientDeliveryForm($this->clientAddressObj);
        
        $this->isValidForm($form, $deliveryData, false, array('postcode'));
    }

    /**
     * Test RestClientAddressNoFullname
     */
    public function testRestClientAddressNoFullname()
    {
        $addressData = $this->addressData;

        // no fullname
        $addressData['fullname'] = '';
      
        $form = new RestClientAddressForm($this->clientAddressObj);
        
        $this->isValidForm($form, $addressData, false, array('fullname'));
    }
    
    /**
     * Test RestClientAddressNoStreet
     */
    public function testRestClientAddressNoStreet()
    {
        $addressData = $this->addressData;

        $addressData['street'] = '';
      
        $form = new RestClientAddressForm($this->clientAddressObj);
        
        $this->isValidForm($form, $addressData, false, array('street'));
    }
    
    /**
     * Test RestClientAddressNoPostcodeAndCityForm
     */
    public function testRestClientAddressNoPostcodeAndCityForm()
    {
        $addressData = $this->addressData;

        $addressData['postcodeAndCity'] = '';
      
        $form = new RestClientAddressForm($this->clientAddressObj);
        
        $this->isValidForm($form, $addressData, false, array('postcodeAndCity'));
    }
    
    /**
     * Method to check if form is valid
     */
    private function isValidForm($form, $formData, $isValid, $errorFields = array())
    {
        $form->bind($formData);
        $this->assertEquals($form->isValid(), $isValid);
        
        foreach($form->getErrorSchema()->getErrors() as $field => $message)
        {
            $this->assertEquals(in_array($field, $errorFields), true);
        }
    }
    
}