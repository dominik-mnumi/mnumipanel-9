<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');

class MnumiHotfolderUpdateTasReadkTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->orderObj = OrderTable::getInstance()->find(1);
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }
    
    /**
     * Tests hotfolder update task.
     */
    public function testHotfolderUpdate()
    {
        $this->assertEquals($this->orderObj->getFiles()->count(), 1);
        
        $task = new MnumiHotfolderUpdateTask(Mnumi::getInstance()->getEventDispatcher(), new sfFormatter());
        $task->run();
        
        $this->orderObj->refresh(true);
        $this->assertEquals($this->orderObj->getFiles()->count(), 3);
    }
    
}