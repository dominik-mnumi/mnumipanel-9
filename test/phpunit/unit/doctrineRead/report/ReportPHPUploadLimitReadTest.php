<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class ReportPHPUploadLimitReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->report = new ReportPHPUploadLimit();
    }

    /**
     * Test getValue();
     */
    public function testGetValue()
    {
        $this->assertNotEmpty($this->report->getValue());
    }

    /**
     * Test checkStatus();
     */
    public function testCheckStatus()
    {
        $this->assertTrue(in_array($this->report->checkStatus(), Report::$statusArray));

        $this->assertEquals('green', $this->report->checkStatus('350MB'));
        $this->assertEquals('red', $this->report->checkStatus('250MB'));
    }

    
    
}