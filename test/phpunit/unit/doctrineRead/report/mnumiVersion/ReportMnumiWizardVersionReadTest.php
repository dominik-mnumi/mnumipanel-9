<?php

require_once(__DIR__.'/../../../../bootstrap/boostrap2.php');


class ReportMnumiWizardVersionReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->verArr = ReportMnumiVersion::getPreparedVersionSourceArr(ReportMnumiVersion::$versionSource);
        
        // production
        $this->report1 = new ReportMnumiWizardVersion($this->verArr['mnumicore3']);
        
        
        // nightly
        $this->date = date('Ymd');
        $this->report2 = new ReportMnumiWizardVersion($this->date);
        
        // beta
        $this->report3 = new ReportMnumiWizardVersion('1.2.3+1.3-b1');        
    }

    /**
     * Test getValue();
     */
    public function testGetValue()
    {
        $this->assertNotEmpty($this->report1->getValue());
        $this->assertNotEmpty($this->report2->getValue());
        $this->assertNotEmpty($this->report3->getValue());
    }

    /**
     * Test checkStatus();
     */
    public function testCheckStatus()
    {
        $this->assertTrue(in_array($this->report1->checkStatus(), Report::$statusArray));
        $this->assertTrue(in_array($this->report2->checkStatus(), Report::$statusArray));
        $this->assertTrue(in_array($this->report3->checkStatus(), Report::$statusArray));

        // production
        $this->assertEquals(Report::$statusGreen, $this->report1->checkStatus('9.9.9'));
        $this->assertEquals(Report::$statusRed, $this->report1->checkStatus('1.0.0'));
        
        // nightly   
        $this->assertEquals(Report::$statusGreen, $this->report2->checkStatus($this->date));

        $yesterdayDate = date('Ymd', strtotime('-1 day', strtotime($this->date))); 
        $this->assertEquals(Report::$statusYellow, $this->report2->checkStatus($yesterdayDate));
        
        $this->assertEquals(Report::$statusRed, $this->report2->checkStatus('20000101'));
        
        // beta 
        $this->assertEquals(Report::$statusYellow, $this->report3->checkStatus('1.2.3+1.3-b1'));
    }
  
}