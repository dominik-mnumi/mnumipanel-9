<?php

require_once(__DIR__ . '/../../bootstrap/boostrap.php');


class PricelistFixPriceTest extends BasePhpunitTestCase
{
    public function testConstructor()
    {
        $pricelistId = 9;

        $fixPrice = new PricelistFixPrice($pricelistId);

        $this->assertEquals($fixPrice->getPriceListId(), $pricelistId);
    }

    public function testAddMethod()
    {
        $pricelistId = 9;

        $fixPrice = new PricelistFixPrice($pricelistId);

        $this->assertEquals(0, count($fixPrice));
        $fixPrice->add($this->prepareFixPriceObject(1));
        $this->assertEquals(1, count($fixPrice));

        $fixPrice->add($this->prepareFixPriceObject(2));
        $this->assertEquals(2, count($fixPrice));

        $fixPrice->add($this->prepareFixPriceObject(1));
        $this->assertEquals(2, count($fixPrice));
    }

    public function testIterator()
    {
        $pricelistId = 9;

        $fixPrice = new PricelistFixPrice($pricelistId);

        $quantity = 88;
        $fixPrice->add($this->prepareFixPriceObject($quantity));

        $iterator = $fixPrice->getIterator();


        $this->assertTrue($iterator->valid());
        $this->assertEquals($quantity, $iterator->current()->getQuantity());
    }

    public function testGet()
    {
        $pricelistId = 9;

        $priceListFixedPrice = new PricelistFixPrice($pricelistId);

        /* Generate fixed price for quantity range: 0.5 to 9.5 */
        for ($i=0; $i<10;$i++) {
            $quantity = $i + 0.5;
            $priceListFixedPrice->add($this->prepareFixPriceObject($quantity));
        }

        // check exactly price
        $fixedPrice = $priceListFixedPrice->get(5.5);
        $this->assertInstanceOf('FixPrice', $fixedPrice);
        $this->assertEquals(5.5, $fixedPrice->getQuantity());

        // check little higher price
        $fixedPrice = $priceListFixedPrice->get(3.6);
        $this->assertInstanceOf('FixPrice', $fixedPrice);
        $this->assertEquals(3.5, $fixedPrice->getQuantity());

        // check little lower price
        $fixedPrice = $priceListFixedPrice->get(7.4);
        $this->assertInstanceOf('FixPrice', $fixedPrice);
        $this->assertEquals(6.5, $fixedPrice->getQuantity());

        // check lowest price
        $fixedPrice = $priceListFixedPrice->get(0);
        $this->assertInstanceOf('FixPrice', $fixedPrice);
        $this->assertEquals(0.5, $fixedPrice->getQuantity());

        // check highest price
        $fixedPrice = $priceListFixedPrice->get(1000);
        $this->assertInstanceOf('FixPrice', $fixedPrice);
        $this->assertEquals(9.5, $fixedPrice->getQuantity());
    }

    public function testSerialize()
    {
        $pricelistId = 9;

        $fixPrice = new PricelistFixPrice($pricelistId);
        $unserialized = unserialize(serialize($fixPrice));

        $this->assertEquals($fixPrice, $unserialized);
    }

    private function prepareFixPriceObject($quantity = 1)
    {
        return new FixPrice($quantity, 23, 4);
    }

}