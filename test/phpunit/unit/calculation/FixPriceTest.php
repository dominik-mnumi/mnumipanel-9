<?php

require_once(__DIR__ . '/../../bootstrap/boostrap.php');


class FixPriceTest extends BasePhpunitTestCase
{
    public function testConstructor()
    {
        $quantity = 12.5;
        $price = 999.21;
        $cost = 82.99;

        $fixPrice = new FixPrice($quantity, $price, $cost);

        $this->assertEquals($fixPrice->getQuantity(), $quantity);
        $this->assertEquals($fixPrice->getPrice(), $price);
        $this->assertEquals($fixPrice->getCost(), $cost);
    }
}