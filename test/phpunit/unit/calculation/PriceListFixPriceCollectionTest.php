<?php

require_once(__DIR__ . '/../../bootstrap/boostrap.php');


class PricelistFixPriceCollectionTest extends BasePhpunitTestCase
{
    public function testAddMethod()
    {
        $collection = new PriceListFixPriceCollection();

        $this->assertEquals(0, count($collection));
        $collection->add($this->preparePricelistFixPriceObject(1));
        $this->assertEquals(1, count($collection));

        $collection->add($this->preparePricelistFixPriceObject(2));
        $this->assertEquals(2, count($collection));
    }

    /**
     * @expectedException \OutOfBoundsException
     * @expectedExceptionMessage Pricelist 1 already exist
     */
    public function testExceptionAddMethod()
    {
        $collection = new PriceListFixPriceCollection();

        $collection->add($this->preparePricelistFixPriceObject(1));
        $collection->add($this->preparePricelistFixPriceObject(1));
    }

    public function testContainsMethod()
    {
        $collection = new PriceListFixPriceCollection();

        $this->assertFalse($collection->contains(3));

        $collection->add($this->preparePricelistFixPriceObject(3));
        $this->assertTrue($collection->contains(3));
    }

    public function testIterator()
    {
        $collection = new PriceListFixPriceCollection();
        $record = $this->preparePricelistFixPriceObject(9);

        $collection->add($record);

        $iterator = $collection->getIterator();

        $this->assertTrue($iterator->valid());
        $this->assertEquals($record, $iterator->current());
    }

    public function testGet()
    {
        $collection = new PriceListFixPriceCollection();

        $this->assertNull($collection->get(1));

        $priceListId = 87;
        $record = $this->preparePricelistFixPriceObject($priceListId);
        $collection->add($record);
        $this->assertEquals($record, $collection->get($priceListId));
    }

    public function testAssertion()
    {
        $priceListId = 12;

        $priceListCollection = new PriceListFixPriceCollection();
        $priceListFixPrice = new PriceListFixPrice($priceListId);
        $priceListCollection->add($priceListFixPrice);
        $this->assertEquals(1, count($priceListCollection));

        $priceListFixPrice->add(new FixPrice(1, 2, 3));
        $priceListFixPrice->add(new FixPrice(2, 2, 3));
        $this->assertEquals(2, count($priceListCollection->get($priceListId)));
    }

    public function testSerialize()
    {
        $pricelistId = 9;

        $fixPrice = new PricelistFixPrice($pricelistId);
        $unserialized = unserialize(serialize($fixPrice));

        $this->assertEquals($fixPrice, $unserialized);
    }

    private function preparePricelistFixPriceObject($pricelistId = 1)
    {
        return new PriceListFixPrice($pricelistId);
    }

}