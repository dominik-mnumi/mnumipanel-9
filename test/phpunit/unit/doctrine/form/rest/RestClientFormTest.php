<?php

require_once(__DIR__.'/../../../../bootstrap/boostrap.php');

class RestClientFormTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common');

        $this->clientFormData = array(
            'fullname' => 'New client name',
            'city' => 'New York',
            'street' => 'Washington street',
            'postcode' => '11-222',
            'tax_id' => '123-45-67-819'
        );

        $this->clientObj = ClientTable::getInstance()->findOneByFullname('client1');

        $this->clientAddressObj = ClientAddressTable::getInstance()->findOneByFullname('address1');

        $this->deliveryData = array(
            'fullname' => 'New client address name',
            'city' => 'New York',
            'street' => 'Washington street',
            'postcode' => '11-222',
            'client_id' => $this->clientAddressObj->getClient()->getId()
        );

        $this->addressData = array(
            'fullname' => 'New client address name',
            'postcodeAndCity' => '11-333 Chicago',
            'street' => 'Washington street',
            'client_id' => $this->clientAddressObj->getClient()->getId()
        );
    }
    
    /**
     * Test RestClientValid
     */
    public function testRestClientValid()
    {
        $form = new RestClientForm($this->clientObj, array('clientFormData' => $this->clientFormData));
        
        $this->isValidForm($form, $this->clientFormData, true);
        
        $form->save();
        
        $this->clientObj = ClientTable::getInstance()->findOneByFullname('New client name');
        
        $this->assertInstanceOf('Client', $this->clientObj);
        $this->assertEquals($this->clientObj->getCity(), 'New York');
        $this->assertEquals($this->clientObj->getStreet(), 'Washington street');
        $this->assertEquals($this->clientObj->getPostcode(), '11-222');
        // proper formated tax id after cleanup
        $this->assertEquals($this->clientObj->getTaxId(), '1234567819');
    }

    /**
     * Test RestClientDelivery
     */
    public function testRestClientDelivery()
    { 
        $form = new RestClientDeliveryForm($this->clientAddressObj);
        
        $this->isValidForm($form, $this->deliveryData, true);
        
        $form->save();
        
        $this->clientAddressObj = ClientAddressTable::getInstance()->findOneByFullname('New client address name');
        
        $this->assertInstanceOf('ClientAddress', $this->clientAddressObj);
        $this->assertEquals($this->clientAddressObj->getCity(), 'New York');
        $this->assertEquals($this->clientAddressObj->getStreet(), 'Washington street');
        $this->assertEquals($this->clientAddressObj->getPostcode(), '11-222');
    }

    /**
     * Test RestClientAddress
     */
    public function testRestClientAddress()
    {
        $form = new RestClientAddressForm($this->clientAddressObj);
        
        $this->isValidForm($form, $this->addressData, true);
      
        $form->save();
      
        $this->clientAddressObj = ClientAddressTable::getInstance()->findOneByFullname('New client address name');
      
        $this->assertInstanceOf('ClientAddress', $this->clientAddressObj);
        $this->assertEquals($this->clientAddressObj->getCity(), 'Chicago');
        $this->assertEquals($this->clientAddressObj->getStreet(), 'Washington street');
        $this->assertEquals($this->clientAddressObj->getPostcode(), '11-333');
    }

    /**
     * Method to check if form is valid
     */
    private function isValidForm($form, $formData, $isValid, $errorFields = array())
    {
        $form->bind($formData);
        $this->assertEquals($form->isValid(), $isValid);
        
        foreach($form->getErrorSchema()->getErrors() as $field => $message)
        {
            $this->assertEquals(in_array($field, $errorFields), true);
        }
    }
    
}