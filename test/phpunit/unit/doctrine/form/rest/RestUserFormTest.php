<?php

require_once(__DIR__.'/../../../../bootstrap/boostrap.php');

class RestUserFormTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->fixture()->clean()->loadSnapshot('common');
        
        $this->userObj = sfGuardUserTable::getInstance()->findOneByEmailAddress('john.doe@gmail.com');
        $this->userFormData = array('first_and_last_name' => 'Bobby Max Duck', 'email_address' => 'bobbymax123@gmail.com');
    }
    
    /**
     * Test RestUserForm
     */
    public function testRestUserForm()
    {
        $form = new RestUserForm($this->userObj, array('userFormData' => $this->userFormData));
        
        $this->isValidForm($form, $this->userFormData, true);
        
        $form->save();
        
        $this->assertEquals($form->getObject()->getFirstName(), 'Bobby');
        $this->assertEquals($form->getObject()->getLastName(), 'Max Duck');
        $this->assertEquals($form->getObject()->getEmailAddress(), 'bobbymax123@gmail.com');
    }
    
    /**
     * Test RestUserEmailUpdate
     */
    public function testRestUserEmailUpdate()
    {
        $this->userFormData['email_address'] = 'new_bobbymax123@gmail.com';
        $form = new RestUserForm($this->userObj, array('userFormData' => $this->userFormData));
        
        $this->isValidForm($form, $this->userFormData, true);
        
        $form->save();
        
        // email changes
        $this->assertEquals($form->getObject()->getEmailAddress(), 'new_bobbymax123@gmail.com');
        // name stay the same
        $this->assertEquals($form->getObject()->getFirstName(), 'Bobby');   
    }

    /**
     * Test RestSmsSet
     */
    public function testRestSmsSet()
    {
        $smsContact = $this->userObj->getDefaultContactByType(NotificationTypeTable::$sms);
        
        $this->assertInstanceOf('UserContact', $smsContact);
        $this->assertTrue($smsContact->getSendNotification());
        
        $userFormData = array('sms_notification' => false);
        $form = new RestUserForm($this->userObj, array('userFormData' => $userFormData));
        
        $this->isValidForm($form, $userFormData, true);
        
        $form->save();
        
        // check if form changed "send_notification" field
        $smsContact = $this->userObj->getDefaultContactByType(NotificationTypeTable::$sms);
        
        $this->assertFalse($smsContact->getSendNotification());
    }
    
    /**
     * Test RestEmailSet
     */
    public function testRestEmailSet()
    {    
        $smsContact = $this->userObj->getDefaultContactByType(NotificationTypeTable::$email);
        
        $this->assertInstanceOf('UserContact', $smsContact);
        $this->assertTrue($smsContact->getSendNotification());
        
        $userFormData = array('email_notification' => false);
        $form = new RestUserForm($this->userObj, array('userFormData' => $userFormData));
        
        $this->isValidForm($form, $userFormData, true);
        
        $form->save();
        
        $smsContact = $this->userObj->getDefaultContactByType(NotificationTypeTable::$email);
        
        $this->assertFalse($smsContact->getSendNotification());
        
        $smsContact = $this->userObj->getDefaultContactByType(NotificationTypeTable::$sms);
        
        // one more time to check if it will be changed to true
        $userFormData = array('email_notification' => true);
        $form = new RestUserForm($this->userObj, array('userFormData' => $userFormData));
        
        $this->isValidForm($form, $userFormData, true);
        
        $form->save();
        
        $smsContact = $this->userObj->getDefaultContactByType(NotificationTypeTable::$email);
        
        $this->assertTrue($smsContact->getSendNotification());
    }
    
    /**
     * Test RestUserContactMobilePhoneForm
     */
    public function testRestUserContactMobilePhoneForm()
    {
        $mobileObj = $this->userObj->getDefaultContactOrCreateByType(NotificationTypeTable::$sms);
        
        $form = new RestUserContactMobilePhoneForm($mobileObj);
        
        $this->isValidForm($form, array('value' => '+4811122233334444'), true);
        
        $form->save();
        
        $this->assertEquals($form->getObject()->getValue(), '+4811122233334444');
    }
    
    /**
     * Test RestChangePasswordForm
     */
    public function testRestChangePasswordForm()
    {
        $this->assertTrue($this->userObj->checkPassword('admin'));
      
        $form = new RestChangePasswordForm($this->userObj);
        $passwordData = array('password' => 'new-password', 'password_current' => 'wrong_current_password');
      
        $this->isValidForm($form, $passwordData, false, array('password_old'));
        
        $passwordData = array('password' => 'new-password', 'password_current' => 'admin');
        
        $this->isValidForm($form, $passwordData, true);
      
        $form->save();
        
        $this->assertTrue($this->userObj->checkPassword('new-password'));
    }
    
    /**
     * Method to check if form is valid
     */
    private function isValidForm($form, $formData, $isValid, $errorFields = array())
    {
        $form->bind($formData);
        $this->assertEquals($form->isValid(), $isValid);
      
        foreach($form->getErrorSchema()->getErrors() as $field => $message)
        {
            $this->assertEquals(in_array($field, $errorFields), true);
        }
    }
}