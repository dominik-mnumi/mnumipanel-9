<?php

require_once(__DIR__ . '/../../bootstrap/boostrap.php');


class NotificationsToolTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->fixture()->clean()->loadSnapshot('common');
    }
    
    public function testEmailClass()
    {
        $notification = new NotificationEmail();
        $user = sfGuardUserTable::getInstance()->find(1);
        $status = $notification->composeMessage($user, 'test', 'title');
        
        $this->assertEquals(true, $status);
        
        $message = NotificationMessageTable::getInstance()->find(1);
        
        $notification = new NotificationEmail($message);
        $status = $notification->send(sfContext::getInstance()->getMailer());
        $this->assertEquals(true, $status);
    }
    
    public function testSmsClass()
    {
        $notification = new NotificationSMS();
        $user = sfGuardUserTable::getInstance()->find(1);
        $status = $notification->composeMessage($user, 'test_2', 'title_2');
        
        $this->assertEquals(true , $status);
        
        //we do not test real sms sending. Test sms is send by testSMSsending() method
    }

}