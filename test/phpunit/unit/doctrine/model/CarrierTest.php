<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class CarrierTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->fixture()->clean()->loadSnapshot('common');
    }

    public function testDeleteMany()
    {
        $carrier1 = new Carrier();
        $carrier1->setName('some name');
        $carrier1->setDeliveryTime('3 days');
        $carrier1->setRequireShipmentData(1);
        $carrier1->save();
        
        $carrier2 = new Carrier();
        $carrier2->setName('some name2');
        $carrier2->setDeliveryTime('4 days');
        $carrier2->setRequireShipmentData(1);
        $carrier2->save();
        
        $carrier3 = new Carrier();
        $carrier3->setName('some name3');
        $carrier3->setDeliveryTime('5 days');
        $carrier3->setRequireShipmentData(1);
        $carrier3->save();
        
        $status = CarrierTable::getInstance()->deleteMany(array($carrier1->getId(), $carrier2->getId(), $carrier3->getId()));
        
        $this->assertEquals(3 , $status);
    }

}