<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class InvoiceTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{
    
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common');

        // gets invoices objects
        $this->invoice = InvoiceTable::getInstance()
        	->findOneByNumberAndInvoiceTypeName(4, InvoiceTypeTable::$INVOICE);

        $this->invoiceUnpaid = InvoiceTable::getInstance()
            ->findOneByNumberAndInvoiceTypeName(5, InvoiceTypeTable::$INVOICE);
        $this->invoiceUnpaid->updatePrice();
        $this->invoiceUnpaid->save();
        $this->invoiceUnpaid->refresh(TRUE);

        $this->invoiceParentOfCorrection = InvoiceTable::getInstance()
            ->findOneByNumberAndInvoiceTypeName(6, InvoiceTypeTable::$INVOICE);
        $this->invoiceParentOfCorrection->updatePrice();
        $this->invoiceParentOfCorrection->save();
        $this->invoiceParentOfCorrection->refresh(TRUE);

        $this->invoiceCorrection = InvoiceTable::getInstance()
            ->findOneByNumberAndInvoiceTypeName(1, InvoiceTypeTable::$CORRECTION_INVOICE);
        $this->invoiceCorrection->updatePrice();
        $this->invoiceCorrection->save();
        $this->invoiceCorrection->refresh(TRUE);

        // preliminary invoice
        $this->preliminaryInvoiceObj = InvoiceTable::getInstance()
            ->findOneByNumberAndInvoiceTypeName(1, InvoiceTypeTable::$PRELIMINARY_INVOICE);
        $this->preliminaryInvoiceObj->updatePrice();
        $this->preliminaryInvoiceObj->save();
        $this->preliminaryInvoiceObj->refresh(TRUE);
    }

    /**
     * Tests preUpdate method.
     */
    public function testPreUpdate()
    {
    	// set invoice as payed
    	$this->invoice->setInvoiceStatusName(InvoiceStatusTable::$paid);
    	$this->invoice->save();

    	$cashDesk = CashDeskTable::getInstance()->createQuery()
    		->andWhere('invoice_id = ?', $this->invoice->getId())
    		->orderBy('id DESC')
    		->execute();
    	
    	$cashDesk = $cashDesk[0];
    	
    	$this->assertEquals('auto generated', $cashDesk->getDescription());
    	
    	// get balance
    	$prevBalance = $cashDesk->getBalance();
    	
    	// check loyality points
    	$points = Doctrine_Query::create()
	    	->from('LoyaltyPoint l')
	    	->leftJoin('l.InvoiceItem ii')
	    	->andWhere('ii.invoice_id = ?', $this->invoice->getId())
	    	->execute();

    	$pointsNb = array();
    	foreach($points as $p)
    	{
    		$pointsNb[] = (integer) $p->getPoints();
    	}
    	// 440 x 10 items = 4400
    	// 80 x 10 items - 20 = 780 -> method getPriceNetTotalAfterDiscount in InvoiceItem
    	$this->assertEquals(array(4400, 5000, 780), $pointsNb);
    	
    	$pointsCountBefore = Doctrine_Query::create()->from('LoyaltyPoint l')->count();
    	
    	// revert paid invoice
    	$this->invoice->setInvoiceStatusName(InvoiceStatusTable::$unpaid);
    	$this->invoice->save();

    	$cashDesk = CashDeskTable::getInstance()->createQuery()
	    	->andWhere('invoice_id = ?', $this->invoice->getId())
	    	->orderBy('id DESC')
	    	->execute();
	    	 
    	$cashDesk = $cashDesk[0];
    	
    	$newBalance = $cashDesk->getBalance();
    	
    	$this->assertNotEquals($prevBalance, $newBalance);
    	
    	// and pay once again
    	$this->invoice->setInvoiceStatusName(InvoiceStatusTable::$paid);
    	$this->invoice->save();
    	
    	$pointsCountAfter = Doctrine_Query::create()->from('LoyaltyPoint l')->count();
    	
    	$cashDesk = CashDeskTable::getInstance()->createQuery()
	    	->andWhere('invoice_id = ?', $this->invoice->getId())
	    	->orderBy('id DESC')
	    	->execute();
    	 
    	$cashDesk = $cashDesk[0];
    	
    	$newestBalance = $cashDesk->getBalance();
    	 
    	$this->assertEquals($prevBalance, $newestBalance);
    	$this->assertEquals($pointsCountBefore, $pointsCountAfter);
    }

    /**
     * Tests preUpdate for preliminary invoice.
     */
    public function testPreUpdatePreliminary()
    {
        $this->checkPaidUnpaidCashDeskAndLoyaltyPointsGeneration($this->preliminaryInvoiceObj);
    }

    /**
     * Tests preUpdate for normal invoice.
     */
    public function testPreUpdateInvoice()
    {
        $this->checkPaidUnpaidCashDeskAndLoyaltyPointsGeneration($this->invoiceUnpaid);
    }

    /**
     * Tests preUpdate for correction invoice.
     */
    public function testPreUpdateCorrection()
    {
        $this->checkPaidUnpaidCashDeskAndLoyaltyPointsGeneration($this->invoiceCorrection);
    }

    /**
     * Tests postSave for correction invoice. Pay correction invoice (with positive balance).
     */
    public function testPostSaveCorrection()
    {
        // origin invoice paid
        $this->invoiceParentOfCorrection->setInvoiceStatusName(InvoiceStatusTable::$paid);
        $this->invoiceParentOfCorrection->save();
        $this->invoiceParentOfCorrection->refresh(TRUE);

        // correction pay (with positive balance)
        $this->invoiceCorrection->setInvoiceStatusName(InvoiceStatusTable::$paid);
        $this->invoiceCorrection->setPriceNet(1000);
        $this->invoiceCorrection->setPriceGross(1230);
        $this->invoiceCorrection->save();
        $this->invoiceCorrection->refresh(TRUE);

        // next gets number of cash desk documents for invoice (2 documents from origin and correction)
        $countCashDeskAfter = $this->invoiceCorrection->getCashDesks()->count() + $this->invoiceParentOfCorrection->getCashDesks()->count();
        $countLoyaltyPointsAfter = $this->invoiceCorrection->getLoyaltyPoints()->count();

        // one document from origin invoice and one from correction
        $this->assertEquals(2, $countCashDeskAfter);
        $this->assertEquals(2, $countLoyaltyPointsAfter);
    }

    /**
     * Tests postSave for correction invoice. Pay correction invoice (with 0 balance).
     */
    public function testPostSaveCorrection2()
    {
        // origin invoice is not paid
        // correction pay (with 0 balance)
        $this->invoiceCorrection->setInvoiceStatusName(InvoiceStatusTable::$paid);
        $this->invoiceCorrection->save();
        $this->invoiceCorrection->refresh(TRUE);

        // next gets number of cash desk documents for invoice (only one document should be generated - automatic pay from origin
        // and origin invoice sets status to paid)
        $countCashDeskAfter = $this->invoiceCorrection->getCashDesks()->count() + $this->invoiceParentOfCorrection->getCashDesks()->count();
        $countLoyaltyPointsAfter = $this->invoiceCorrection->getLoyaltyPoints()->count();

        // one document from origin invoice and one from correction
        $this->assertEquals(1, $countCashDeskAfter);
        $this->assertEquals(2, $countLoyaltyPointsAfter);
    }

    public function testUpdatePrice()
    {
        // 1. checks prices and tax for preliminary invoice
        $this->preliminaryInvoiceObj->updatePrice();
        $this->preliminaryInvoiceObj->save();

        $this->assertEquals($this->preliminaryInvoiceObj->getPriceNet(), 1380.00);
        $this->assertEquals($this->preliminaryInvoiceObj->getPriceTax(), 317.40);
        $this->assertEquals($this->preliminaryInvoiceObj->getPriceGross(), 1697.40);

        // 2.
    }

    /**
     * Tests preInsert method.
     */
    public function testPreInsert()
    {
        $obj = new Invoice();
        $obj->setClient(ClientTable::getInstance()->findOneByFullname('client1'));
        $obj->setInvoiceType(InvoiceTypeTable::getInstance()->findOneByName(InvoiceTypeTable::$PRELIMINARY_INVOICE));
        $obj->setClientName('Ted Stuart');
        $obj->setClientAddress('Panton Street 12/3');
        $obj->setClientPostcode('00-000');
        $obj->setClientCity('London');
        $obj->setClientTaxId('1234567819');
        $obj->setPaymentDateAt('2020-12-12');
        $obj->setPayment(PaymentTable::getInstance()->findOneByName(PaymentTable::$cashPayment));
        $obj->save();
        $obj->refresh();

        // should be unpaid
        $this->assertFalse($obj->isPaid());

        // seller data should be saved
        $this->assertTrue((bool)$obj->getSellerName());
        $this->assertTrue((bool)$obj->getSellerAddress());
        $this->assertTrue((bool)$obj->getSellerPostcode());
        $this->assertTrue((bool)$obj->getSellerCity());
        $this->assertTrue((bool)$obj->getSellerTaxId());
        $this->assertTrue((bool)$obj->getSellerBankName());
        $this->assertTrue((bool)$obj->getSellerBankAccount());

        // sell at date should be saved
        $this->assertEquals($obj->getSellAt(), date('Y-m-d'));
    }

    /**
     * Checks CashDesk and LoyaltyPoints document generation. Checks documents quantity.
     *
     * @param $invoiceObj
     */
    protected function checkPaidUnpaidCashDeskAndLoyaltyPointsGeneration($invoiceObj)
    {
        // 1. changes payment status to paid (checks cash desk and loyalty points - checks if not duplicated)

        // first gets number of cash desk documents for preliminary invoice
        $countCashDeskBefore = $invoiceObj->getCashDesks()->count();
        $countLoyaltyPointsBefore = $invoiceObj->getLoyaltyPoints()->count();

        $invoiceObj->setInvoiceStatusName(InvoiceStatusTable::$paid);
        $invoiceObj->save();
        $invoiceObj->refresh(TRUE);

        // next gets number of cash desk documents for invoice
        $countCashDeskAfter = $invoiceObj->getCashDesks()->count();
        $countLoyaltyPointsAfter = $invoiceObj->getLoyaltyPoints()->count();

        $expectedCashDeskCount = $invoiceObj->isCorrection() && $invoiceObj->getPriceNet() == 0
            ? $expectedCashDeskCount = 0
            : $countCashDeskBefore + 1;

        $expectedLoyaltyPointCount = $countLoyaltyPointsBefore + 2;
        $this->assertEquals($expectedCashDeskCount, $countCashDeskAfter);
        $this->assertEquals($expectedLoyaltyPointCount, $countLoyaltyPointsAfter); // two invoice items with orders

        // 2. changes status to unpaid
        $countCashDeskBefore = $invoiceObj->getCashDesks()->count();
        $invoiceObj->setInvoiceStatusName(InvoiceStatusTable::$unpaid);
        $invoiceObj->save();
        $invoiceObj->refresh(TRUE);

        $countCashDeskAfter = $invoiceObj->getCashDesks()->count();
        $countLoyaltyPointsAfter = $invoiceObj->getLoyaltyPoints()->count();

        $expectedCashDeskCount = $invoiceObj->isCorrection() && $invoiceObj->getPriceNet() == 0
            ? 0
            : $countCashDeskBefore + 1;

        $this->assertEquals($expectedCashDeskCount, $countCashDeskAfter);
        $this->assertEquals(0, $countLoyaltyPointsAfter);
    }
}