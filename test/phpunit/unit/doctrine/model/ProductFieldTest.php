<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class ProductFieldTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common');
        
        $this->pricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();
    }

    /**
     * Tests delete process of ProductField.
     * Should be cascade deletion: product_field->product_field_item
     * Product should be untouched.
     */
    public function testProductFieldCascadeDelete()
    {
        $productObj = ProductTable::getInstance()->findOneByName('product 1');
        $productId = $productObj->getId();
        $productFieldColl = ProductFieldTable::getInstance()->findByProductId($productId);
        $productFieldCount = ProductFieldTable::getInstance()->findByProductId($productId)->count();
        $productFieldArray = array();

        foreach($productFieldColl as $rec)
        {
            $productFieldArray[] = $rec->getId();
        }

        $productFieldItemColl = ProductFieldItemTable::getInstance()->createQuery()
                ->whereIn('product_field_id', $productFieldArray)
                ->execute();

        $productFieldItemCount = $productFieldItemColl->count();

        //asserts before deletion
        $this->assertEquals(12, $productFieldCount);
        $this->assertEquals(17, $productFieldItemCount);

        //deletes all field of product 1
        $productFieldColl->delete();
        
        $productFieldItemColl = ProductFieldItemTable::getInstance()->createQuery()
                ->whereIn('product_field_id', $productFieldArray)
                ->execute();

        //asserts after deletion
        $this->assertEquals(0, $productFieldColl->count());
        $this->assertEquals(0, $productFieldItemColl->count());
        
        //checks if reference does not deleted product 1
        $productObj = ProductTable::getInstance()->findOneByName('product 1');
        
        $this->assertEquals('product 1', $productObj->getName());
        
        //check fieldsets if left untouched
        $this->assertEquals(1, FieldsetTable::getInstance()->findByName('GENERAL')->count());
    }
    
}
