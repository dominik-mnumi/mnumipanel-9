<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class OrderPackageTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{
    
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common');

        $this->package = OrderPackageTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
    }


    public function testCorrectValueReturnedByToStringAfterSavingObject()
    {
        $this->package->save();
        $this->assertEquals((String)$this->package, date('Y-m-d G:i:s', strtotime($this->package->getCreatedAt())));
    }
    
    /**
     * Tests OrderPackage changeShelfId method. 
     */
    public function testChangeShelfId()
    {       
        $orders = $this->package->getOrders();
        
        foreach($orders as $rec)
        {
            $rec->moveToShelf();
        }
        
        // sets shelf package and shelf related orders to null
        $this->package->changeShelfId();
        
        // no orders on shelf because taken down
        $this->assertTrue(!$this->package->isOrderOnShelf());
    }
    
    /**
     * Tests OrderPackage takeDownFromShelf method. 
     */
    public function testTakeDownFromShelf()
    {             
        $this->package->setOrderPackageStatusName('completed');
        $this->package->save();
        
        // checks when package can be taken down
        $this->assertTrue($this->package->takeDownFromShelf());
    }
    
    /**
     * Tests OrderPackage sendPackage method.
     */
    public function testSendPackage()
    {
        $this->package->setOrderPackageStatusName(OrderPackageStatusTable::$completed);
        $this->package->save();
        $this->package->sendPackage('111-22-33-44');
        
        $this->assertEquals($this->package->getTransportNumber(), '111-22-33-44');
        $this->assertNotEquals($this->package->getSendAt(), null);    
    }
    
    /**
     * Tests OrderPackage linkWithCoupon method.
     */
    public function testLinkWithCoupon()
    {
        $coupon = CouponTable::getInstance()->find('BonusCoupon');
        
        $this->assertTrue($coupon instanceof Coupon);
        
        $this->assertEquals($this->package->getCouponName(), '');
        $this->assertFalse($coupon->getUsed());
        
        $this->package->linkWithCoupon($coupon);
      
        $this->assertEquals($this->package->getCouponName(), 'BonusCoupon');
        $this->assertTrue($coupon->getUsed());
        
        $this->package->unlinkCoupon();
        
        $this->assertEquals($this->package->getCouponName(), '');
        $this->assertFalse($coupon->getUsed());
    }

    /**
     * Tests OrderPackage payWithLoyaltyPoints method.
     */
    public function testPayWithLoyaltyPoints()
    {
        $package = OrderPackageTable::getInstance()->findOneByDeliveryCity('London');
        $this->assertTrue($package->payWithLoyaltyPoints() instanceof LoyaltyPoint);
    }
    
    /**
     * Tests OrderPackage payWithLoyaltyPoints method.
     */
    public function testIsPaidWithLoyaltyPoints()
    {
        $this->assertTrue($this->package->isPaidWithLoyaltyPoints());
        $this->package->unpayLoyaltyPoints();
        $this->assertFalse($this->package->isPaidWithLoyaltyPoints());
    }
   
}