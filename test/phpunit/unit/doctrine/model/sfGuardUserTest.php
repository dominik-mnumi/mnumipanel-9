<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class sfGuardUserTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * @var sfGuardUser
     */
    private $object;
    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common');
        $this->object = Doctrine_Core::getTable('sfGuardUser')->findOneByUsername('admin');
    }

    /**
     * Test isUsernameGenerated
     */
    public function testIsUsernameGenerated()
    {
        $user = new sfGuardUser();
        $user->setFirstName('John');
        $user->setLastName('Johny');
        $user->setEmailAddress('johny@gmail.com');
        $user->setPassword('test');
        $user->setIsActive(1);
        $user->save();
        
        $this->assertEquals(true, $user->isUsernameGenerated());
        
        $user2 = new sfGuardUser();
        $user2->setFirstName('Bob');
        $user2->setLastName('Bobynsky');
        $user2->setEmailAddress('bober@gmail.com');
        $user2->setPassword('test3');
        $user2->setIsActive(1);
        $user2->setUsername('Bobby');
        $user2->save();
        
        $this->assertEquals(false, $user2->isUsernameGenerated());
    }
}