<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class RestSessionTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{

    
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
    	$this->fixture()->clean()->loadSnapshot('common');
    }
    
    /**
     * Test registerRestLogin
     */
    public function testCheckSession()
    {
        $restSession = RestSessionTable::getInstance()->checkSession('test_handle');
        //no such a session

        $this->assertEquals(false, $restSession);
        
        $session = new RestSession();
        $session->setId('test_handle');
        $session->setUserId(1);
        $session->save();
        
        $restSession = RestSessionTable::getInstance()->checkSession('test_handle');
        //session exists
        $this->assertEquals(true, (boolean)$restSession);
    }
    
    /**
     * Test registerRestLogin
     */
    public function testRegisterRestLogin()
    {
        $user = sfGuardUserTable::getInstance()->retrieveByUsernameOrEmailAddress('admin');
        RestSessionTable::getInstance()->registerRestLogin('some_secret', $user);
      
        $restSession = RestSessionTable::getInstance()->createQuery('')
            ->where('user_id = ?', $user->getId())
            ->execute();
      
        $date = $restSession->getFirst()->getUpdatedAt();
        $this->assertEquals(count($restSession), 1);
      
        //pause between next login request
        sleep(1);
      
        RestSessionTable::getInstance()->registerRestLogin('some_secret', $user);
      
        $restSession = RestSessionTable::getInstance()->createQuery('')
            ->where('user_id = ?', $user->getId())
            ->execute();
      
        $newDate = $restSession->getFirst()->getUpdatedAt();
        $this->assertEquals(count($restSession), 1);
      
        // new updated at for new request
        $this->assertNotEquals($date, $newDate);
    }
}