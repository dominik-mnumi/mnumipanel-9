<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class FieldItemTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common');
        $this->pricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();
    }

    /**
     * Tests delete process of FieldItem.
     * Fieldset should be untouched.
     */
    public function testFieldItemCascadeDelete()
    {
        $this->assertEquals(16, FieldsetTable::getInstance()->findAll()->count());
        
        FieldItemTable::getInstance()->findAll()->delete();
        
        $this->assertEquals(16, FieldsetTable::getInstance()->findAll()->count());
    }
  
    
}