<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class ClientTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common')->loadSnapshot('invoiceCost');
        
        $this->client = ClientTable::getInstance()->findOneByFullname('client1');
    }
    
    public function testUpdateInvoiceCostPayment()
    {
    	$invoiceCost = Doctrine_Query::create()
	    	->from('InvoiceCost ic')
	    	->where('ic.invoice_number = ?', '14/12/2011')
	    	->fetchOne();
    	
    	$this->assertInstanceOf('InvoiceCost', $invoiceCost);
    	
    	$data = Doctrine_Query::create()
	    	->select('SUM(ic.amount) AS amount, SUM(ic.payed_amount) AS payed_amount')
	    	->from('InvoiceCost ic')
	    	->where('ic.client_id = ?', (int)$invoiceCost->getClientId())
	    	->fetchOne();
    	
    	$countedBalance = ($data->getAmount() - $data->getPayedAmount());
    	$balance = $invoiceCost->getClient()->updateInvoiceCostPayment();
    	
    	$this->assertEquals($balance, $countedBalance, 'Balance ok');
    	 
    }

}