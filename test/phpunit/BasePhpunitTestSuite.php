<?php

class BasePhpunitTestSuite extends sfBasePhpunitTestSuite 
    implements sfPhpunitFixtureDoctrineAggregator
{

    /**
     * Dev hook for custom "setUp" stuff
     */
    protected function _start()
    {
        $this->_initFilters();
        
        $configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'test', true);
        new sfDatabaseManager($configuration);
        sfContext::createInstance($configuration);

        // to initalize snapshot, run: './symfony phpunit:do-snapshot'
    }

    /**
     * Dev hook for custom "tearDown" stuff
     */
    protected function _end()
    {
        
    }

    protected function _initFilters()
    {
        $filters = sfConfig::get('sf_phpunit_filter', array());
        
        // unrecognized problem with PHP_CodeCoverage
        if('3.6.2' != PHPUnit_Runner_Version::id())
        {
            foreach ($filters as $filter)
            {
                if (version_compare(PHPUnit_Runner_Version::id(), '3.5') >= 0)
                {
                    $f = new PHP_CodeCoverage_Filter();
                    $f->addDirectoryToBlacklist($filter['path']);
                }
                else
                {
                    PHPUnit_Util_Filter::addDirectoryToFilter($filter['path'], $filter['ext']);
                }
            }
        }
    }

    public function getApplication()
    {
        return 'mnumicore';
    }

}