<?php

require_once(__DIR__.'/../../bootstrap/boostrap.php');

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class FieldsetTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        $this->fixture()->clean()->loadCommon('default_selenium');
    }

    /**
     *  Test add fieldset
     */
    public function testAdd()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("mnumicore_test_selenium.php/settings");
        $this->click("id=add_field_category");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("xpath=(//form[@id='new_field_category_form'])[2]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->type("xpath=(//input[@id='fieldset_name'])[2]", "test fieldset");
        $this->type("xpath=(//input[@id='fieldset_label'])[2]",
                "test fieldset label");
        $this->click("xpath=(//form[@id='new_field_category_form']/table/tbody/tr[3]/td/div/div/div/div/div/div/ul/li/a/span)[6]");
        $this->click("css=button[type=\"button\"]");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Settings group created successfully.");
    }

    /**
     *  Test delete fieldset
     */
    public function testDelete()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("mnumicore_test_selenium.php/settings");
        $this->verifyTextPresent("can_delete");
        $this->click("//div[@id='sf_content']/article/section/div/div/ul/li[5]/span/a[3]/img");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if("Delete item" == $this->getText("css=div.modal-window.block-border > div.block-content > h1"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("css=button[type=\"button\"]");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertFalse($this->isTextPresent("can_delete"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

}

?>