<?php

require_once(__DIR__.'/../../bootstrap/boostrap.php');

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

/**
 * @TODO when permission section will be completed then finish this class.
 */



class ConfigurationTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        $this->fixture()->clean()->loadCommon('default_selenium');
    }

    /**
     *  Test index action
     */
    public function testIndex()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/configuration");
        try
        {
            $this->assertTrue($this->isTextPresent("Configuration"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        $this->type("id=AppYamlEditForm_company_data_seller_name", "Some other company name");
        $this->click("id=saveAndReturn");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertTrue($this->isTextPresent("Some other company name"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        try
        {
            $this->assertTrue($this->isTextPresent("Saved succesfully"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }
}

?>