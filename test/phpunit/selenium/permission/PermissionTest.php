<?php

require_once(__DIR__.'/../../bootstrap/boostrap.php');

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

/**
 * @TODO when permission section will be completed then finish this class.
 */



class PermissionTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        $this->fixture()->clean()->loadCommon('default_selenium');
    }

    /**
     *  Test index action
     */
    public function testIndex()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("mnumicore_test_selenium.php/permission");
        $this->verifyTextPresent("admin");
        $this->verifyTextPresent("Administrator group");
        $this->verifyTextPresent("Permissions");
    }

    /**
     *  Test add action (ajax)
     */
    public function testAdd()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("mnumicore_test_selenium.php/permission");
        $this->click("css=img[alt=\"Add permission\"]");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("css=#modal div.block-footer.align-right > button[type=\"button\"]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->type("xpath=(//input[@id='sfGuardGroupForm_name'])[2]",
                "new_permission");
        $this->type("xpath=(//textarea[@id='sfGuardGroupForm_description'])[2]",
                "Some new permission");
        $this->click("css=#modal button");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isTextPresent("Saved successfully."))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }
    }

    /**
     *  Test edit and update action
     */
    public function testEditAndUpdate()
    {
        $permissionObj = sfGuardGroupTable::getInstance()->findOneByName('other');

        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/permission/edit/".$permissionObj->getId());
        $this->verifyTextPresent("Edit permission");
        $this->verifyTextPresent("Name");
        $this->verifyTextPresent("Description");
        $this->verifyTextPresent("Permissions list");
        $this->click("id=sfGuardGroupForm_permissions_list_4");
        $this->type("id=sfGuardGroupForm_description", "Admin super user");
        $this->click("id=sfGuardGroupForm_permissions_list_2");
        $this->click("id=saveAndReturn");
        $this->waitForPageToLoad("30000");
        $this->verifyTextPresent("Saved successfully.");
        $this->verifyTextPresent("Admin super user");
    }

    /**
     *  Test delete action
     */
    public function testDelete()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("/mnumicore_test_selenium.php/permission");
        $this->verifyTextPresent("Some other group");
        $this->click("css=tr.even > td.table-actions > a.without-tip.delete > img");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isElementPresent("css=#modal div.block-footer.align-right > button[type=\"button\"]"))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        $this->click("css=#modal button");
        $this->waitForPageToLoad("30000");
        for($second = 0;; $second++)
        {
            if($second >= 60)
                $this->fail("timeout");
            try
            {
                if($this->isTextPresent("Deleted successfully."))
                    break;
            }
            catch(Exception $e)
            {
                
            }
            sleep(1);
        }

        try
        {
            $this->assertFalse($this->isTextPresent("Some other group"));
        }
        catch(PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

}

?>