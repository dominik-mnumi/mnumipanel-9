<?php

require_once __DIR__.'/../BasePhpunitTestSuite.php';
require_once __DIR__.'/../PhpunitSnapshotFactory.class.php';

/**
 *
 * @author Maksim Kotlyar <mkotlar@ukr.net>
 *
 */
class sfPhpunitDoSnapshotContainer extends sfPhpunitBaseDoSnapshotContainer
{
	/**
	 *
	 * @var sfPhpunitFixture
	 */
	protected $_fixture;

	public function doSnapshots($options = array())
	{
		// initalize db connection
		$configuration = ProjectConfiguration::getApplicationConfiguration($options['application'], $options['env'], true);
		new sfDatabaseManager($configuration);
		
		// initalize fixutes class
		$suite = new BasePhpunitTestSuite();
		$this->_fixture = sfPhpunitFixture::build($suite, $options);
		
		$this->_fixture->clean()
			// clean up tables used for storing snapshot data.
			->cleanSnapshots();
		
		// prepare "common" snapshot
		$this->loadCommonSnapshot();
		
		// prepare "invoiceCost" snapshot
		$this->loadInvoiceCostSnapshot();

	}
	
	protected function loadCommonSnapshot()
	{
		PhpunitSnapshotFactory::prepareCommonFixtures($this->fixture())
			->doSnapshot('common');
		
		return;
	}
	
	protected function loadInvoiceCostSnapshot()
	{
		PhpunitSnapshotFactory::prepareCommonFixtures($this->fixture())
			->loadCommon('invoiceCost')
			->doSnapshot('invoiceCost');
	
		return;
	}

	protected function fixture()
	{
		return $this->_fixture;
	}
}