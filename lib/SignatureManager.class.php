<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SignatureManager helps with generating and validating request.
 *
 * @author jupeter
 */
class SignatureManager
{
    protected $secretKey;

    /**
     *
     * @param string $secretKey 
     */
    public function __construct($secretKey)
    {
        if(empty($secretKey))
        {
            throw new Exception('Secret key is not defined.');
        }
        
        $this->secretKey = $secretKey;
    }

    /**
     * Validate if passed signature is correct.
     * 
     * @param string $parameter
     * @param string $signature 
     * @return bool
     */
    public function validateSignature($parameter, $signature)
    {
        if($signature == '')
        {
            return false;
        }
        if(count($parameter) == 0)
        {
            return false;
        }
        $parameter - $this->decodeParameter($parameter);
        
        return ($this->generateSignature($parameter) == $signature);
    }

    /**
     * Generate signature.
     * 
     * @param array $parameter
     * @return bool
     */
    public function generateSignature($parameter)
    {
        $secret = $this->getSecretKey();
        return base64_encode(hash_hmac('sha1', $this->encodeParameter($parameter), $secret, true));
    }

    /**
     * Encode request parameter.
     * 
     * @return string
     */
    public function encodeParameter($parameter)
    {
        return base64_encode(json_encode($parameter));
    }

    /**
     * Returns decoded paramter.
     * 
     * @param string $parameter
     * @return array
     */
    public function decodeParameter($parameter)
    {
        return json_decode(base64_decode($parameter), true);
    }
    
    /**
     * Get secret key from configuration.
     * 
     * @return string
     */
    private function getSecretKey()
    {
        return $this->secretKey;
    }
}
