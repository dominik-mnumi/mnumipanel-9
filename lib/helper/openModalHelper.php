<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
*  modal window
*/

echo '<script type="text/javascript" src="/js/jquery.modal.js"></script><script type="text/javascript" src="/js/openModal.js"></script>';