<?php

/**
 * returns formatted url for package
 *
 * @param OrderPackage $package 
 * @return string
 */
function package_details_url($package)
{
  return url_for("@client_package_details?id={$package->getClientId()}&package_id={$package->getId()}");
}