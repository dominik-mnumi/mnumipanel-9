<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @author Marek Balicki
 */
function extendSlot($name)
{
    $content = '';
    if(has_slot($name))
    {
        $content = get_slot($name);    
    }
    slot($name);
    echo $content;
}