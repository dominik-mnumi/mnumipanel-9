<?php
function getChangeLanguage($languages, $default){
    $html = '<div class="block-border margin-top10">
    <div class="block-content form">
        <fieldset>
            <legend>'.__('Regional settings').'</legend>

            <table class="field_item_table">
                <tbody>
                <tr>
                    <td><label for="lang">'.__('Language').':</label></td>
                    <td>
                        <select class="change-language" name="lang" data-url="'.url_for('@setI18nLang', true).'">';
    foreach($languages as $id => $language) {
        $selected = ($default === $id) ? 'selected' : '';
        $html .= '<option value="' . $id . '" ' . $selected . '>' . $language . '</option>';
    }
    $html .= '</select>
                    </td>
                </tr>
                </tbody>
            </table>
        </fieldset>
    </div>
</div>';
    
    echo $html;
    
}
