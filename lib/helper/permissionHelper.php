<?php

function has_permission($permission_name)
{
    return sfContext::getInstance()->getUser()->hasPermission($permission_name);
}
