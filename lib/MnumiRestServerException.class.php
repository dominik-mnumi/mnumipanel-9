<?php

class MnumiRestServerException extends Exception
{
    /**
     * Success. 
     */
    const E_MSG_SUCCESS = 0;
    
    /**
     * Unknown error. 
     */
    const E_MSG_UNKNOWN_ERROR = 1;

    /**
     * General error. 
     */
    const E_MSG_ERROR = 'E_MSG_ERROR';
    
    /**
     * Message: succes response
     */
    const ERR_MSG_SUCCESS = 'E_MSG_SUCCESS';

    /**
     * Method: doRegister()
     * Error: Given email address already exists in database.
     */
    const E_REG_LOGIN_EXISTS = 'E_REG_LOGIN_EXISTS';
    /**
     * Method: doRegister()
     * Error: Password field missing: password_repeat.
     */
    const E_REG_PSWD_MISS = 'E_REG_PSWD_MISS';
    /**
     * Method: doRegister()
     * Error: Password fields are not the same.
     */
    const E_REG_PSWD_NOT_SAME = 'E_REG_PSWD_NOT_SAME';
    /**
     * Method: doRegister()
     * Error: New user registry process failed.
     */
    const E_REG_FAILED = 'E_REG_FAILED';

    /**
     * Method: doRegister()
     * Error: NIP already exists.
     */
    const E_REG_NIP_EXISTS = 'E_REG_NIP_EXISTS';

    /**
     * Method: doUpdateMyData() 
     */
    const E_CHANGE_DATA_CURRENT_PASS = 'E_CHANGE_DATA_CURRENT_PASS';
   
    /**
     * Method: doSetLostPassword() 
     */
    const E_LOST_PASS_LOGIN = 'E_LOST_PASS_LOGIN';

    const E_USER_DOES_NOT_EXIST = 'E_USER_DOES_NOT_EXIST';
    
    /**
     * Method: doAddCoupon()
     */
    const COUPON_DOES_NOT_EXIST = 'COUPON_DOES_NOT_EXIST';
    
    /**
     * Method: doAddCoupon()
     */
    const COUPON_DOES_NOT_AFFECT_BASKET = 'COUPON_DOES_NOT_AFFECT_BASKET';
    
    /**
     * Method: doAddCoupon()
     */
    const COUPON_IS_ALREADY_USED = 'COUPON_IS_ALREADY_USED';
    
    /**
     * Method: doAddCoupon()
     */
    const COUPON_IS_OUT_OF_DATE = 'COUPON_IS_OUT_OF_DATE';
    
    /**
     * Method: doUpdateLostPassword()
     */
    const LOST_PASSWORD_UPDATE_FAILED = 'LOST_PASSWORD_UPDATE_FAILED';
    
    /**
     * Method: doUpdateMyData(), doChangeDefaultClient()
     */
    const E_USER_NOT_LOGGED_IN = 'E_USER_NOT_LOGGED_IN';
    
    /**
     * Method: doUpdateMyData()
     */
    const E_USER_FORM_INVALID = 'E_USER_FORM_INVALID';
    
    /**
     * Method: doUpdateMyData()
     */
    const E_MOBILE_FORM_INVALID = 'E_MOBILE_FORM_INVALID';
    
    /**
     * Method: doUpdateClientData()
     */
    const E_CLIENT_FORM_INVALID = 'E_CLIENT_FORM_INVALID';
    
    /**
     * Method: doUpdateClientData()
     */
    const E_CLIENT_ADDRESS_FORM_INVALID = 'E_CLIENT_ADDRESS_FORM_INVALID';
    
    /**
     * Method: doGetBasketRealPrice()
     */
    const E_BASKET_DOES_NOT_EXISTS = 'E_BASKET_DOES_NOT_EXISTS';
    
    /**
     * Error messages array
     * @var array
     */
    static $err_desc = array(
        'E_MSG_SUCCESS' => 'This is desc',
        'E_REG_PSWD_NOT_SAME' => 'Password fields are not the same.',
        'E_REG_PSWD_MISS' => 'Password field missing: password_repeat',
        'E_REG_FAILED' => 'New user registry process failed.',
        'E_REG_LOGIN_EXISTS' => 'Given email address already exists in database.',
        'E_REG_NIP_EXISTS' => 'Given NIP already exists in database'
    );

    public function parseRestMessage()
    {
        header('Content-Type: text/xml;');

        echo
        '<?xml version="1.0" encoding="UTF-8"?> 
     <exception>
      <status>failed</status> 
      <code>'.$this->getMessage().'</code>
      <message>'.$this->getDescription().'</message>
     </exception>';
        exit;
    }

    protected function getDescription()
    {
        if(!array_key_exists($this->getMessage(), $this->err_desc))
        {
            return '';
        }

        return $this->err_desc[$this->getMessage()];
    }

}