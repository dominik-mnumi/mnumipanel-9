<?php

class UPSShip
{
    // configuration
    protected
        $wsdl,   
        $outputFilename,  
        $outputFilenameReport,
        $access,
        $userid,
        $passwd,        
        $endpointurl,
           
        // for production
        //$endpointurl    = 'https://onlinetools.ups.com/webservices/Ship',
        $orderPackageObj;
            
    /**
     * Creates instance of UPSShip class.
     */
    public function __construct(OrderPackage $orderPackageObj)
    {
        $this->access = sfConfig::get('app_ups_access');
        $this->userid = sfConfig::get('app_ups_user_id');
        $this->passwd = sfConfig::get('app_ups_password');      
        $this->endpointurl = sfConfig::get('app_ups_url');   

        $this->orderPackageObj = $orderPackageObj;
        
        // prepares wsdl path
        $this->wsdl = sfConfig::get('sf_lib_dir') .'/UPS/wsdl/Ship.wsdl';
        
        // gets image filename
        $filename = $this->getImageFilename();

        $this->outputFilenameReport = sfConfig::get('sf_cache_dir').'/'.$filename.'.xml';


        // prepares output filename
        $printlabelDir = PrintlabelQueueAttributeTable::getInstance()->getPrintLabelImageDir();
        if(!file_exists($printlabelDir))
        {
            mkdir($printlabelDir, 0777, true);
        }
        
        $this->outputFilename = $printlabelDir.$filename.'.bmp';

        $fw = fopen($this->outputFilenameReport, 'a');
        fwrite($fw, "Begin connection at: ".date('r')."\n");

        try
        {
            // use soap 1.1 client
            $mode = array(
                'soap_version' => 'SOAP_1_1', 
                'trace'        => 1);

            // initializes soap client
            $client = new SoapClient($this->wsdl, $mode);

            fwrite($fw, "Set location: ".$this->endpointurl."\n");

            // sets endpoint url
            $client->__setLocation($this->endpointurl);

            // creates soap header
            $usernameToken['Username']                   = $this->userid;
            $usernameToken['Password']                   = $this->passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $this->access;
            $upss['UsernameToken']                       = $usernameToken;
            $upss['ServiceAccessToken']                  = $serviceAccessLicense;

            fwrite($fw, "Authentication: ".implode(', ', $usernameToken).', using: ' .$this->access."\n");

            $header = new SoapHeader(
                    'http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0', 
                    'UPSSecurity', 
                    $upss);
            
            $client->__setSoapHeaders($header);

            fwrite($fw, "Finished authentication.\n");


            fwrite($fw, "Begin process shippment.\n");

            $processShipment = $this->processShipConfirm();

            fwrite($fw, json_encode($processShipment)."\n");

            /* **** 1. Process Shipment **** */

            // gets response
            $resp = $client->__soapCall(
                    'ProcessShipConfirm', 
                    array($processShipment));
   
            // gets status
            //echo "Response confirm status: ".$resp->Response->ResponseStatus->Description."\n";

            $responseXML = $client->__getLastResponse();

            preg_match(
                    '/<ship:ShipmentDigest>(.*)<\/ship:ShipmentDigest>/i', 
                    $responseXML, 
                    $matchArr);
            
            $shipmentDigest = $matchArr[1];

            //echo $shipmentDigest;

            // saves soap request and response of ProcessShipConfirm to file
            fwrite($fw, "Request for ProcessShipConfirm: \n".$client->__getLastRequest()."\n");
            fwrite($fw, "Response for ProcessShipConfirm: \n".$client->__getLastResponse()."\n");


            fwrite($fw, "Begin accept shipment.\n");

            /* **** 2. Accept Shipment **** */
            // gets response
            $resp = $client->__soapCall(
                    'ProcessShipAccept', 
                    array($this->processShipAccept($shipmentDigest)));

            // gets status
            //echo "Response accept status: ".$resp->Response->ResponseStatus->Description."\n";
   
            // prepares files
            $cacheFilename = sfConfig::get('sf_cache_dir').'/'.$filename;
            file_put_contents($cacheFilename, 
                     base64_decode($resp->ShipmentResults
                             ->PackageResults
                             ->ShippingLabel
                             ->GraphicImage));

            $imObj = new Imagick($cacheFilename);
            $imObj->setImageFormat('bmp');
            $imObj->writeimage($this->outputFilename);
//
            fclose($fw);

//            // deletes cache file
//            unlink($cacheFilename);
            
            // saves soap request and response of ProcessShipAccept to file
            $fw = fopen($this->outputFilenameReport, 'a');
            fwrite($fw, "Request for ProcessShipAccept: \n".$client->__getLastRequest()."\n");
            fwrite($fw, "Response for ProcessShipAccept: \n".$client->__getLastResponse()."\n");
            fclose($fw);
        }
        catch(Exception $e)
        {
            throw new Exception('Error: ' . $e->getMessage() . '. Please check log file: ' . $this->outputFilenameReport);
        }
    }
    
    /**
     * Returns url filename.
     * 
     * @return string
     */
    public function getFileUrl()
    {
        $filenameArr = explode('.', basename($this->outputFilename));
        
        return sfContext::getInstance()->getRouting()
                        ->generate('printLabelImage', 
                                array('filename' => $filenameArr[0],
                                    'sf_format' => 'bmp'),
                                true);
    }

    /**
     * Convert Polish chars (ąćźż) to universal chars (aczz)
     * @param string $value
     * @return string
     */
    protected function convertCharacters($value)
    {
        $value = iconv('utf-8', 'ascii//translit', $value);
        $value = trim($value);

        return $value;
    }

    /**
     * Cutting text every 35 characters while maintaining whole words.
     * And return 3 array items
     *
     * @param string $value
     * @return array
     */
    protected function convertAddressLine($value)
    {
        $value = $this->convertCharacters($value);
        $arrayWords = explode(' ', $value);

        // Max size of each line
        $maxLineLength = 35;
        // Auxiliar counters, foreach will use them
        $currentLength = 0;
        $index = 0;

        foreach($arrayWords as $word)
        {
            // +1 because the word will receive back the space in the end that it loses in explode()
            $wordLength = strlen($word) + 1;
            if( ( $currentLength + $wordLength ) <= $maxLineLength )
            {
                $arrayOutput[$index][] = $word;
                $currentLength += $wordLength;
            }
            else
            {
                $index += 1;
                $currentLength = $wordLength;
                $arrayOutput[$index][] = $word;
            }
        }

        foreach ($arrayOutput as &$line)
        {
            $line = implode(" ", $line);
        }
        return array_reverse(array_slice($arrayOutput, 0, 3));
    }
    
    /**
     * Prepares shipment request array.
     * 
     * @return array
     */
    protected function processShipment()
    {
        // create soap request
        $request['Request'] = array(
            'RequestOption' => 'validate');
        
        //$shipment['Description']            = $this->orderPackageObj->getDescription();
        $shipperConfigurationArr = sfConfig::get('app_ups_shipper');
        
        $shipper = array(
            'Name' => $this->convertCharacters($shipperConfigurationArr['name']),
            'AttentionName' => $this->convertCharacters($shipperConfigurationArr['attention_name']),
            'TaxIdentificationNumber' => $shipperConfigurationArr['tax_identification_number'],
            'ShipperNumber' => $shipperConfigurationArr['shipper_number'],
            'Address' => array(
                'AddressLine' => $this->convertAddressLine($shipperConfigurationArr['address']['address_line']),
                'City' => $this->convertCharacters($shipperConfigurationArr['address']['city']),
                'PostalCode' => $shipperConfigurationArr['address']['postal_code'],
                'CountryCode' => (!empty($shipperConfigurationArr['address']['country_code']))
                    ? $shipperConfigurationArr['address']['country_code']
                    : sfConfig::get('app_default_country'),
                //'Phone' => array(
                //    'Number' => '',
                //    'Extension' => ''
                //)
            )
        );
        $shipment['Shipper'] = $shipper;

        $shipfrom = array(
            'Name' => $this->convertCharacters($shipperConfigurationArr['name']),
            'AttentionName' => $this->convertCharacters($shipperConfigurationArr['attention_name']),
            'Address' => array(
                'AddressLine' => $this->convertAddressLine($shipperConfigurationArr['address']['address_line']),
                'City' => $this->convertCharacters($shipperConfigurationArr['address']['city']),
                'PostalCode' => $shipperConfigurationArr['address']['postal_code'],
                'CountryCode' => (!empty($shipperConfigurationArr['address']['country_code']))
                    ? $shipperConfigurationArr['address']['country_code']
                    : sfConfig::get('app_default_country'),
                //'Phone' => array(
                //    'Number' => ''),
            ));
        $shipment['ShipFrom'] = $shipfrom;

        $deliveryCountry = $this->orderPackageObj->getDeliveryCountry();

        $deliveryCountry = (!empty( $deliveryCountry) )
            ? $this->orderPackageObj->getDeliveryCountry()
            : sfConfig::get('app_default_country');
        
        $shipto = array(
            'Name' => $this->convertCharacters($this->orderPackageObj->getClient()->getShortenedFullName(35)),
            'AttentionName' => $this->convertCharacters($this->orderPackageObj->getDeliveryName()),
            'Address' => array(
                'AddressLine' => $this->convertAddressLine($this->orderPackageObj->getDeliveryStreet()),
                'City' => $this->convertCharacters($this->orderPackageObj->getDeliveryCity()),
                'PostalCode' => $this->orderPackageObj->getDeliveryPostcode(),
                'CountryCode' => $deliveryCountry,
            ));

        $userPhoneNumber = $this->orderPackageObj->getSfGuardUser()->getPhoneNumer();
        if($userPhoneNumber)
        {
            $shipto['Address']['Phone'] = array(
                'Number' => $userPhoneNumber
            );
        }
        $shipment['ShipTo'] = $shipto;

        
        $paymentinformation = array(
            'ShipmentCharge' => array(
                'Type' => '01', // 02 does not work
                'BillShipper' => array(
                    'AccountNumber' => $shipperConfigurationArr['shipper_number'],                 
                )
            )
        );
        $shipment['PaymentInformation'] = $paymentinformation;
        
        $serviceConfigurationArr = sfConfig::get('app_ups_service');
        $shipment['Service'] = array(
            'Code' => $serviceConfigurationArr['code']);

        $packageConfigurationArr = sfConfig::get('app_ups_package');
        $package = array(
            'Description' => $this->orderPackageObj->getDescription(),
            'Packaging' => array(
                'Code' => $packageConfigurationArr['packaging']['code']
            ),
            'PackageWeight' => array(
                'UnitOfMeasurement' => array(
                    'Code' => $packageConfigurationArr['package_weight']['unit_of_measurement']['code']
                ),
                'Weight' => $packageConfigurationArr['package_weight']['weight']),
                'Dimenstions' => array(
                'UnitOfMeasurement' => array(
                    'Code' => $packageConfigurationArr['dimensions']['unit_of_measurement']['code']
                ),
                'Length' => $packageConfigurationArr['dimensions']['length'],
                'Width' => $packageConfigurationArr['dimensions']['width'],
                'Height' => $packageConfigurationArr['dimensions']['height']
            )     
        );
        $shipment['Package'] = $package;


        $labelspecification = array(
            'LabelImageFormat' => array(
                'Code' => 'GIF',
                'Description' => 'GIF'
            ),
            'HTTPUserAgent' => 'Mozilla/4.5'
        );

        $shipment['LabelSpecification'] = $labelspecification;
        $request['Shipment'] = $shipment;

        return $request;
    }

    /**
     * Prepares request for ProcessShipConfirm.
     * 
     * @return array
     */
    protected function processShipConfirm()
    {
        // creates soap request
        return $this->processShipment();
    }

    /**
     * Prepares request for ProcessShipAccept.
     * 
     * @param string $shipmentDigest
     * @return array
     */
    protected function processShipAccept($shipmentDigest)
    {
        $request = $this->processShipment();
        $request['ShipmentDigest'] = $shipmentDigest;
        
        // creates soap request
        return $request;
    }

    /**
     * Returns image filename.
     * 
     * @return string
     */
    protected function getImageFilename()
    {
        return $this->orderPackageObj->getId().'_UPS_'.date('Y_m_d_G_i_s').'_'.md5(rand(0, 99999));
    }
}

?>