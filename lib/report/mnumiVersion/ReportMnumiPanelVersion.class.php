<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PHP version extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportMnumiPanelVersion extends ReportMnumiVersion
{
    // sets name of mnumi application (to compare)
    protected $mnumiApplication = 'mnumicore3';
    
    public function checkStatus($value = null)
    {
        return parent::checkStatus($value);
    }

    /**
     * Returns value for MnumiPanel.
     * 
     * @return string 
     */
    public function getValue()
    {
        return $this->versionType.' '.$this->version;
    }
}