<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PHP upload limit extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportPHPUploadLimit extends Report
{
    public function checkStatus($value = null)
    {
        $status = 'red';

        ($value) ? $uploadMaxFilesize = $value
                 : $uploadMaxFilesize = ini_get("upload_max_filesize");

        $fileUploads = ini_get("file_uploads");
        
        if(strnatcmp('300M', $uploadMaxFilesize) <= 0 && $fileUploads)
        {
            $status = 'green';
        }

        return $status;
    }

    public function getValue()
    {
        $uploadMaxFilesize = ini_get("upload_max_filesize");

        return $uploadMaxFilesize;
    }
}