<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Database System Version extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportDbSystemVer extends Report
{

    public function checkStatus($value = null)
    {
        if(!$value)
        {
            $result = Doctrine_Manager::getInstance()
                    ->connection()
                    ->execute('select version() as version')
                    ->fetchObject()
                ->version;
        }
        else
        {
            $result = $value;
        }

        if(strnatcmp('5.2', $result) <= 0)
        {
            return 'green';
        }
        else if(strnatcmp('5.1', $result) <= 0)
        {
            return 'yellow';
        }
        else
        {
            return 'red';
        }
    }

    public function getValue()
    {
        return $result = Doctrine_Manager::getInstance()
            ->connection()
            ->execute('select version() as version')
            ->fetchObject()
        ->version;
    }
}