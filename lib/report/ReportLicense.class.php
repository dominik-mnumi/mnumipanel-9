<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Check if license is correct.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportLicense extends Report
{
    private $expires;
    
    public function checkStatus($value = null)
    {
        $information = priceTool::checkKey();
        
        $this->expires = $information['expire_at'];

        if(time() > $this->expires)
        {
            return 'red';
        }
        
        if(strtotime('+90 day') > $this->expires)
        {
            return 'yellow';
        }

        return 'green';
    }

    public function getValue()
    {
        return format_date(date($this->expires), 'D');
    }
}