<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Image Magick extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportImageMagick extends Report
{
    public function checkStatus($value = null)
    {
        if(!$value)
        {
            $result = shell_exec('convert --version');
            if(!empty($result))
            {
                preg_match('/Version: ImageMagick ([0-9]*\.[0-9]*\.[0-9]*)/', $result, $arr_return);

                $version =  $arr_return[1];

                if(strnatcmp('6.6', substr($version, 0, 3)) == 0)
                {
                    return 'green';
                }
                else
                {
                    return 'yellow';
                }
            }
        }
        else
        {
            $version = $value;

            if(strnatcmp('6.6', $version) == 0)
            {
                return 'green';
            }
            else
            {
                return 'yellow';
            }
        }
        return 'red';
    }

    public function getValue()
    {
        $result = shell_exec('convert --version');
        if(!empty($result))
        {
            preg_match('/Version: ImageMagick ([0-9]*\.[0-9]*\.[0-9]*)/', $result, $arr_return);

            $version =  $arr_return[1];

            return $version;
        }

        return null;
    }
}