<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PHP version extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportPHPVersion extends Report
{
    public function checkStatus($value = null)
    {
        ($value) ? $version = $value
                 : $version = phpversion();
        
        if(strnatcmp('5.2', $version) <= 0)
        {
            return 'green';
        }
        else
        {
            return 'red';
        }
    }

    public function getValue()
    {
        $version = phpversion();

        return $version;
    }
}