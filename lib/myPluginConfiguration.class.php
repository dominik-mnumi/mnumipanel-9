<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of myPluginConfiguration
 *
 * @author jupeter
 */
class myPluginConfiguration extends sfPluginConfiguration {
    
  /**
   * Initializes autoloading for the plugin.
   * 
   * This method is called when a plugin is initialized in a project
   * configuration. Otherwise, autoload is handled in
   * {@link sfApplicationConfiguration} using {@link sfAutoload}.
   * 
   * @see sfSimpleAutoload
   */
  public function initializeAutoload()
  {
    $autoload = mySimpleAutoload::getInstance(sfConfig::get('sf_cache_dir').'/project_autoload.cache');

    if (is_readable($file = $this->rootDir.'/config/autoload.yml'))
    {
      $this->configuration->getEventDispatcher()->connect('autoload.filter_config', array($this, 'filterAutoloadConfig'));
      $autoload->loadConfiguration(array($file));
      $this->configuration->getEventDispatcher()->disconnect('autoload.filter_config', array($this, 'filterAutoloadConfig'));
    }
    else
    {
      $autoload->addDirectory($this->rootDir.'/lib');
    }
    $autoload->register();
  }

}

?>
