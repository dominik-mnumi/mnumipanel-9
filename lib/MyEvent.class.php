<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Events class.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class MyEvent
{
    /**
     * Lisener of notification.
     * 
     * @param sfEvent $event
     * @return bool
     * @throws Exception
     */
    public static function notificationToUserListener(sfEvent $event)
    {
        // gets event name
        $eventName = $event->getName();

        // gets object
        $obj = $event['obj'];
        if(!$obj)
        {
            throw new Exception('Object for notification is required.');
        }

        $webapiKey = isset($event['webapi_key']) ? $event['webapi_key'] : null;

        // gets class name
        $className = self::getObjectClassName($eventName);
        
        /** 
         * According to PHP documentation, it could be "object or string variable".
         * @link http://php.net/manual/en/language.operators.type.php
         */
        if(!($obj instanceof $className))
        {
            throw new Exception('Given object should be instance of '.$className.'.');
        }

        // gets context object
        $contextObj = sfContext::getInstance();

        $notificationClassName = self::getNotificationClassByEvent($eventName);

        /** @var Notifications $notificationTemplateObj */
        $notificationTemplateObj = new $notificationClassName(
            $contextObj,
            $obj,
            isset($event['externalArr']) ? $event['externalArr'] : null,
            $webapiKey
        );

        return $notificationTemplateObj->prepareAndSendNotification();

    }

    public static function createPrintLabelListener(sfEvent $event)
    {
        // gets object
        $orderPackage = $event['obj'];

        PrintlabelQueueTable::getInstance()->saveDefaultPrintlabelQueueWithAttributes($orderPackage);
    }
    
    /**
     * Gets class name based on event name.
     * 
     * @param string $eventName
     * @return string 
     */
    private static function getObjectClassName($eventName)
    {
        // mapping event name to model object
        $mappingObjectInstance = array(
            'user' => 'sfGuardUser',
            'package' => 'OrderPackage',
            'order' => 'Order',
            'client' => 'sfGuardUser'
        );
 
        // gets object name
        $objectName = explode('.', $eventName);
        $objectName = $objectName[0];
        
        return isset($mappingObjectInstance[$objectName]) 
            ? $mappingObjectInstance[$objectName]
            : null;
    }
    
    /**
     * Event name to notification template mapping.
     * 
     * @param string $eventName
     * @return string 
     */
    private static function getNotificationTemplate($eventName)
    {
        // mapping event name to notification template
        $mappingNotificationTemplate = array(
            'user.create' => NotificationTemplateTable::$register,
            'user.password.lost' => NotificationTemplateTable::$lostPasswordPanel,
            'user.password.lost.shop' => NotificationTemplateTable::$lostPassword,
            'package.ready' => NotificationTemplateTable::$packageReady,
            'package.buy' => NotificationTemplateTable::$buy,
            'package.posted' => NotificationTemplateTable::$packagePosted,
            'order.status.new' => NotificationTemplateTable::$orderNew,
            'order.status.calculation' => NotificationTemplateTable::$calculation,
            'order.status.calculationNotLogged' => NotificationTemplateTable::$calculationWithoutLogging,
            'client.create' =>  NotificationTemplateTable::$register,
            'client.invitation' =>  NotificationTemplateTable::$clientInvitation
        );
        
        return $mappingNotificationTemplate[$eventName];
    }

    /**
     * Event name to notification template class.
     *
     * @param string $eventName
     * @return string
     */
    private static function getNotificationClassByEvent($eventName)
    {
        return Cast::camelize(self::getNotificationTemplate($eventName)).'Notification';
    }
}
