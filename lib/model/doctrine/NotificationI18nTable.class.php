<?php

/**
 * NotificationI18nTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class NotificationI18nTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return NotificationI18nTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('NotificationI18n');
    }
}
