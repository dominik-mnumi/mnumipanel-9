<?php

/**
 * ReportDespatchTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ReportDespatchTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object ReportDespatchTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ReportDespatch');
    }
    
    /**
     * Returns ordered despatches query by report name
     *
     * @param  $reportName string
     * @return Doctrine_Query
     */
    public function getOrderedDespatchesByReportNameQuery($reportName)
    {
        return ReportDespatchTable::getInstance()->createQuery('c')
            ->where('c.carrier_report_type_name = ?', $reportName)
            ->orderBy('c.created_at DESC');
    }
    
    /**
     * Returns ordered despatches by report name
     *
     * @param  $reportName string
     * @return Doctrine_Collection
     */
    public function getOrderedDespatchesByReportName($reportName)
    {
        return $this->getOrderedDespatchesByReportNameQuery($reportName)->execute();
    }
    
    /**
     * Returns new report despatch
     *
     * @param $type string
     * @return Doctrine_Record
     */
    public function closeBook($type)
    {
        $conn = Doctrine_Manager::connection();
        try
        {
            $conn->beginTransaction();
          
            $reportDespatch = new ReportDespatch();
            $reportDespatch->setNumber($this->getNextNumber($type));
            $reportDespatch->setCarrierReportTypeName($type);
            $reportDespatch->save();
            
            $orderPackages = OrderPackageTable::getInstance()->getDespatchPackages();
            
            foreach($orderPackages as $orderPackage)
            {
                $orderPackage->setReportDespatch($reportDespatch);
                $orderPackage->save();
            }
          
            $conn->commit();
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new Exception($e->getMessage());
        }
        
        return $reportDespatch;
    }
    
    /**
     * Returns next invoice number based on type.
     *
     * @param string $type
     * @return integer
     */
    public function getNextNumber($type)
    {
        $obj = self::getInstance()->createQuery()
            ->addWhere('carrier_report_type_name = ?', $type)
            ->orderBy('number DESC')
            ->limit(1)
            ->fetchOne();
      
        if(!$obj)
        {
            return 1;
        }
        return $obj->getNumber() + 1;
    }
}
