<?php

/**
 * InvoiceItemTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class InvoiceItemTable extends Doctrine_Table
{
    public static $displayTableFields = array('description' => 'Description',
        'quantity' => 'Quantity', 'price_net' => 'Price net',
        'price_discount' => 'Price discount', 'unit_of_measure' => 'Unit of measure');
    
    /**
     * Returns an instance of this class.
     *
     * @return InvoiceItemTable InvoiceItemTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('InvoiceItem');
    }
    
    /**
     * Returns array with column aliases or methods to use if defined.
     * 
     * @return array
     */
    public function getDisplayTableFields()
    {
        $fieldArr = array();
        foreach(self::$displayTableFields as $key => $rec)
        {
            $fieldArr[$this->getClassnameToReturn().'.'.$key] = $rec;
        }

        return $fieldArr;
    }

    public function getNetPriceQuery($invoiceId, $query = null)
    {
        if($query == null)
        {
            $query = $this->createQuery('ii');
        }

        return $query->select('sum(round(((ii.price_net * ii.quantity) - ii.price_discount), 2)) as sum')
            ->where('ii.invoice_id = ?', $invoiceId);
    }

    public function getGrossPriceQuery($invoiceId, $query = null)
    {
        if($query == null)
        {
            $query = $this->createQuery('ii');
        }

        return $query->select('sum(round(((ii.price_net * ii.quantity) - ii.price_discount) * (1 + (ii.tax_value/100)), 2)) as sum')
            ->where('ii.invoice_id = ?', $invoiceId);
    }


    public function getTaxPriceQuery($invoiceId, $query = null)
    {
        if($query == null)
        {
            $query = $this->createQuery('ii');
        }

        return $query->select('sum(round(((ii.price_net * ii.quantity) - ii.price_discount) * ((ii.tax_value/100)), 2)) as sum')
            ->where('ii.invoice_id = ?', $invoiceId);
    }

    /**
     * Returns Doctrine_Collection of invoice items based on order id array.
     *
     * @param array $orderIdArray
     * @param array $packageObjs
     *
     * @return Doctrine_Collection $invoiceItemColl
     */
    public function getInvoiceItemCollBasedOnOrderIdArray($orderIdArray, $packageObjs = array())
    {
        // prepares Doctrine_Collection for invoice items
        $invoiceItemColl = new Doctrine_Collection('InvoiceItem');
        
        // prepares Doctrine_Collection for order trader
        $orderTraderColl = new Doctrine_Collection('OrderTrader');

        // gets collection of orders
        $coll = OrderTable::getInstance()->getOrderColl($orderIdArray);

        // get loyalty points discount value
        $discountValue = 0;
        foreach($packageObjs as $packageObj)
        {
            $discountValue += $packageObj->getUsedLoyaltyPointsValue();
        }

        // creates collection of invoice items (for orders)
        foreach($coll as $rec)
        {
            // for all orders which price > 0
            if($rec->getPriceNet() > 0)
            {
                $invoiceItemObj = new InvoiceItem();
                $invoiceItemObj->loadDataFromOrder($rec);

                if ($discountValue > 0) {
                    $invoiceItemObj->setPriceDiscount(
                        $rec->getPriceNet() >= $discountValue ? $discountValue : $rec->getPriceNet()
                    );
                    $discountValue -= $rec->getPriceNet();
                }

                // adds invoice item to invoice
                $invoiceItemColl->add($invoiceItemObj);
                
                // gets traders (if exist)
                foreach($rec->getOrderTraders() as $rec2)
                {
                    $orderTraderColl->add($rec2);
                }
            }
        }
        
        // creates collection of invoice items (for traders)
        foreach($orderTraderColl as $rec)
        {
            // for all traders which price > 0
            if($rec->getPrice() > 0)
            {
                $invoiceItemObj = new InvoiceItem();
                $invoiceItemObj->loadDataFromTrader($rec);
    
                // adds invoice item to invoice
                $invoiceItemColl->add($invoiceItemObj);
            }
        }

        // invoice items for carrier and payment
        if($coll->count() > 0)
        {
            /** @var OrderPackage $orderPackage */
            $orderPackage = $coll->getFirst()->getOrderPackage();

            $q = $orderPackage->getOrdersDefaultQuery();
            $orderPackage->calculatePrice($q);

            // delivery section
            if($orderPackage->getDeliveryPrice() > 0) {
                $invoiceItemObj = new InvoiceItem();
                $invoiceItemObj->loadCarrierData($orderPackage);

                // adds invoice item to invoice
                $invoiceItemColl->add($invoiceItemObj);
            }
            // end delivery section
            
            // payment section
            if($orderPackage->getPaymentPrice() > 0) {
                $invoiceItemObj = new InvoiceItem();
                $invoiceItemObj->loadDataFromPayment($orderPackage->getPayment());

                // adds invoice item to invoice
                $invoiceItemColl->add($invoiceItemObj);
            }
            // end payment section
        }
//
//        // add loyalty points discount
//        if ($discountValue > 0) {
//            $invoiceItemObj = new InvoiceItem();
//            $invoiceItemObj->loadDataFromPackageLoytalyPaymentPoints($packageObj, $discountValue);
//
//            // adds invoice item to invoice
//            $invoiceItemColl->add($invoiceItemObj);
//        }
        
        return $invoiceItemColl;
    }
}
