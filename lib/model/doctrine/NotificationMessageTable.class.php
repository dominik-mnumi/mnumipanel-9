<?php

/**
 * NotificationMessageTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class NotificationMessageTable extends Doctrine_Table
{
    public static $displayTableFields = array('sender' => 'Sender',
        'sender_name' => 'Sender name', 'recipient' => 'Recipient',
        'recipient_name' => 'Recipient name', 'send_at' => 'Send on',
        'message_plain' => 'Message plain', 'messge_html' => 'Message html',
        'title' => 'Title', 'error_message' => 'Error message');
    
    /**
     * Returns an instance of this class.
     *
     * @return object NotificationMessageTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('NotificationMessage');
    }
    
    /**
     * Returns array with column aliases or methods to use if defined.
     * 
     * @return array
     */
    public function getDisplayTableFields()
    {
        $fieldArr = array();
        foreach(self::$displayTableFields as $key => $rec)
        {
            $fieldArr[$this->getClassnameToReturn().'.'.$key] = $rec;
        }
        
        return $fieldArr;
    }
    
    /**
     * Returns Doctrine_Query field items
     *
     * @return Doctrine_Query
     */
    public function getNotificationsToSend($limit = 10)
    {
       $query =  Doctrine_Core::getTable('NotificationMessage')
            ->createQuery('n')
            ->andWhere('n.send_at is null')
            ->andWhere('n.created_at >= DATE_SUB(CURDATE(),INTERVAL 2 DAY)')
            ->orderBy('n.created_at ASC')
            ->leftJoin('n.NotificationType t')
            ->limit($limit);
            
        return $query->execute();
    }
}