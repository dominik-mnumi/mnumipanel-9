<?php

/**
 * RestSession
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    mnumicore
 * @subpackage model
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class RestSession extends BaseRestSession
{ 
    /**
     *   Check if user in this session is logged user
     *
     *   @return boolean
     */
    public function isUserLoggedIn()
    {
        return $this->getUserId() ? true : false;
    }

    /**
     * Try to guess pricelist for default client
     *
     * @return Pricelist
     */
    public function getDefaultClientPricelist()
    {
        if ($this->isUserLoggedIn()) {
            $defaultClient = $this->getSfGuardUser()->getDefaultClient();
            if (!$defaultClient) {
                return PricelistTable::getInstance()->getDefaultPricelist();
            }

            return $defaultClient->getPricelist();
        }

        return PricelistTable::getInstance()->getDefaultPricelist();
    }
    
    /**
     * Returns Doctrine_Record of OrderPackage.
     * 
     * @return OrderPackage
     */
    public function getBasketPackage()
    {
        if($this->isUserLoggedIn())
        {
            $basketPackageObj = $this->getSfGuardUser()->getBasketPackage();
        }
        else
        {
            $basketPackageObj = $this->getBasketPackageFromSession();
        }        
        
        return $basketPackageObj;
    }
    
    /**
     * Returns OrderPackage (basket) from session.
     * 
     * @return OrderPackage 
     */
    public function getBasketPackageFromSession()
    {
        return OrderPackageTable::getInstance()
                ->findOneByRestSessionIdAndOrderPackageStatusName($this->getId(), OrderPackageStatusTable::$basket);
    }
    
    /**
     * Updates rest data with connected objects (sets user_id).
     * 
     * @param sfGuardUser $userObj
     * @throws MnumiRestServerException 
     */
    public function updateRestData($userObj)
    {
        // gets client
        $clientObj = $userObj->getDefaultClient();

        // gets basket package when user is unlogged and sets all references
        $basketPackageObj = $this->getBasketPackageFromSession();

        // if anonymous user does not have any order - basket does not exist
        if(!$basketPackageObj || !$clientObj)
        {
            return;
        }

        $basketPackageObj->setSfGuardUser($userObj);
        $basketPackageObj->setClient($clientObj);
        // sets user id
        $this->setSfGuardUser($userObj);

        // save section
        $conn = Doctrine_Manager::connection();
        try
        {
            $conn->beginTransaction();
            // get client price list id
            $clientPriceListId = $clientObj->getPricelistId();

            // foreach order in new basket
            /** @var Order $rec */
            foreach($basketPackageObj->getOrders() as $rec)
            {
                $rec->setClient($clientObj);
                $rec->setSfGuardUser($userObj);
                $rec->setEditor($userObj);
                 // calculate order price for new client
                $priceCalculate = $rec->calculate(null, $clientPriceListId);
                // update price value
                $rec->setPrices( $priceCalculate['priceItems'] );


                // update tax if client is
                if ($rec->getClient()->getWntUe() === true) {
                    $rec->setTaxValue(0);
                }

                $rec->updatePrices();

                $rec->save();

                foreach($rec->getOrderAttributes() as $rec2)
                {
                    $rec2->setEditor($userObj);
                    $rec2->save();
                }
            }

            // if old basket does exist transfer orders to old and delete
            $oldBasketPackageObj = $userObj->getBasketPackage();
            if($oldBasketPackageObj)
            {
                // transfers orders to existent basket
                $basketPackageObj->moveOrdersTo($oldBasketPackageObj);
                $basketPackageObj->delete();
            }
            else
            {
                $basketPackageObj->save();
            }

            // saves rest and basket
            $this->save();

            $conn->commit();       
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new MnumiRestServerException($e->getMessage());
        }
    }
}
