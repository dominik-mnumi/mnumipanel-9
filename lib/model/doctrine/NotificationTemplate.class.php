<?php

/**
 * NotificationTemplate
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    mnumicore
 * @subpackage model
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class NotificationTemplate extends BaseNotificationTemplate
{
    /**
     * Returns Notification class name, e.g. NewOrderNotification.
     *
     * @return string
     */
    public function getNotificationClassName()
    {
        return Cast::camelize($this->getName()).'Notification';
    }

    public function getNotification()
    {
        $className = $this->getNotificationClassName();

        if(!class_exists($className))
        {
            throw new Exception('Notification class does not exists: ' . $className);
        }

        return new $className(sfContext::getInstance());
    }
}
