<?php

/**
 * ProductI18nTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ProductI18nTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return ProductI18nTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ProductI18n');
    }
}
