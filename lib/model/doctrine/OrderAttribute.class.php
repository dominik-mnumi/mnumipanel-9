<?php

/**
 * OrderAttribute
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    mnumicore
 * @subpackage model
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class OrderAttribute extends BaseOrderAttribute
{

    /**
     * Event invoked before saving
     */
    public function preSave($event)
    {
        $invoker = $event->getInvoker();
        /*
         * Every time the attribute is changed release resources and acquire them
         * back. When atribute is created it is identified as "modified" too.
         */
        if ($this->isChanged() && $this->getOrder()->hasAcquiredStock()) {
            $conn = Doctrine_Manager::connection();
            try {
                $conn->beginTransaction();

                //Release aquired before field items, implicitly it is not used on creation
                $invoker->releaseToStock();
                // Acquire changed now field items
                $invoker->acquireFromStock();
                $conn->commit();
            }
            catch(Exception $e) {
                $conn->rollback();
                throw $e;
            }

        }

        if(!$invoker->isNew())
        {
            $this->recordChanges();
        }

        $orderObj = $invoker->getOrder();
        
        if(sfContext::hasInstance())
        {
            $instance = sfContext::getInstance();
        }
        else
        {
            $instance = false;
        }
        
        if(!$instance || $instance->getConfiguration()->getEnvironment() == 'test')
        {
            // for test enviroment
            $userObj = sfGuardUserTable::getInstance()->findOneByUsername('admin');
            
            if(!$userObj)
            {
                throw new \Exception('There is no "admin" username to run tests. Please check your fixtures data. ');
            }
            
            $userId = $userObj->getId();
        }
        elseif($instance->getUser()->isAuthenticated())
        {
            // for normal use (we are logged in panel)
            $userObj = $instance->getUser()->getGuardUser();
            
            if(!$userObj)
            {
                throw new \Exception('Could not get sfGuardUser object. ');
            }
            
            $userId = $userObj->getId();            
        }
        elseif($orderObj->getUserId() != null)
        {
            // if still no user then get user object directly from order
            $userId = $orderObj->getUserId();
        }
        else
        {
            // if still no user - there is anonymouse user (using MnumiShop)
            $userId = false;
        }
        
        if($userId)
        {
            $invoker->setEditorId($userId);
        }
        
    }

    /**
     *
     */
    public function preDelete($event)
    {
        $this->releaseToStock();
    }

    /**
     * Returns label for fieldset (field item)
     * 
     * @return string
     */
    public function getValueLabel()
    {
        return $this->getProductField()->getFieldsetLabel();
    }
    
    /**
     * Returns hotfolder filename.
     * 
     * @param string $orderStatus
     * @return string 
     */
    public function getHotfolderFilename($orderStatus = null)
    {
        $orderObj = $this->getOrder();
        return $orderObj->getHotfolderPath($orderStatus).$orderObj->getId().'.'
                .$this->getValue().'.pdf';
    }

    /**
     * Record all changes
     */
    protected function recordChanges()
    {
        $order = $this->getOrder();

        $changes = $this->getModified(true);

        if (count($changes) > 0 && $this->getEditorId() != null) {
            $changeGroup = ChangeGroupTable::getInstance()->addOrder($order);

            $productField = $this->getProductField();

            foreach ($changes as $field => $oldValue) {
                $methodName = 'get' . ucfirst($field);
                $newValue = $this->$methodName();

                /** @var ProductFieldItem $oldValueObject */
                $oldValueObject = $productField->getProductFieldItemById($oldValue);

                if ($oldValueObject) {
                    $oldValue = $oldValueObject->getFieldItem()->getName();
                }

                /** @var ProductFieldItem $oldValueObject */
                $newValueObject = $productField->getProductFieldItemById($newValue);

                if ($newValueObject) {
                    $newValue = $newValueObject->getFieldItem()->getName();
                }

                if ($newValue == $oldValue) {
                    continue;
                }

                $fieldValue = $this->getValueLabel();
                $changeGroup->addItem($fieldValue, $newValue, $oldValue);
            }

            $changeGroup->save();
        }
    }

    /**
     * Load value, replacing page count, when Product
     * has enabled "calculate_as_card" parameter
     *
     * @return string
     */
    public function getCalculatedValue()
    {
        $value = $this->getValue();

        if ($this->getProductField()->isCalculateAsCardEnabled()) {
            $sideOrderAttribute = $this->getOrder()->getAttribute('SIDES');

            if($sideOrderAttribute !== false) {
                $sideId = (int) $sideOrderAttribute->getValue();

                if($sideId > 0) {
                    /** @var FieldItem $side */
                    $side = FieldItemTable::getInstance()->find($sideId);

                    if ($side->getName() === 'Double') {
                        $value = (int) $value * 2;
                    }
                }
            }
        }

        return $value;
    }

    /**
     * Fix if exists cloned attribute, if not, we set first exists value.
     */
    public function fixNotExistAttribute()
    {
        $require = array();
        foreach ($this->getProductField()->getProductFieldItems() as $item)
        {
            $require[] = $item->getFieldItemId();
        }
        if(!in_array($this->getValue(), $require) && $require)
        {
            $this->setValue($require[0]);
        }
    }

    /**
     * Helper method for getting FieldItem used in Stock, by the given value
     * of OrderAttribute.
     *
     * @param integer            value of an OrderAttribute to get FieldItem
     * @return FieldItem|boolean FieldItem if has been found, false otherwise.
     */
    private function getStockFieldItem($value)
    {
        $fieldItems = FieldItemTable::getInstance()->findByIds(array($value));
        // We always pass only one value, so it is safe to get first element of
        // an array
        $fieldItem = $fieldItems[0];

        if ($fieldItem->isUsedInStock()) {
            return $fieldItem;
        }

        return false;
    }

    /**
     * Acquires related FieldItem from stock.
     * Checks if value has been changed, and if so every FieldItem with
     * which is used in stock and which is not set as an emptyField will be
     * acquired.
     *
     * @param boolean $force Force acquiring from Stock?
     * @throws NegativeStockException
     */
    private function acquireFromStock($force = false)
    {
        $value = false;

        // Get only changed NEW values
        $modified = $this->getModified();

        if (count($modified) > 0) {
            $value = $modified['value'];
        }
        else if ($force) {
            $value = $this->getValue();
        }

        if ($value !== false) {
            $fieldItem = $this->getStockFieldItem($value);
            if ($fieldItem !== false) {
                $current = $fieldItem->getAvailability();
                $fieldItem->setAvailability(--$current);
                $fieldItem->save();
            }
        }
    }

    /**
     * Releases acquired FieldItem back to stock.
     * Checks if value has been changed, and if so, every FieldItem with
     * which is used in stock and which is not set as an emptyField will be
     * released.
     *
     * @param boolean $force Force releasing to Stock?
     * @throws NegativeStockException
     */
    private function releaseToStock($force = false)
    {
        $value = false;

        // Get only changed OLD values
        $modified = $this->getModified(true);

        if (count($modified) > 0) {
            $value = $modified['value'];
        }
        else if ($force) {
            $value = $this->getValue();
        }

        if ($value !== false) {
            $fieldItem = $this->getStockFieldItem($value);
            if ($fieldItem !== false) {
                $current = $fieldItem->getAvailability();
                $fieldItem->setAvailability(++$current);
                $fieldItem->save();
            }
        }
    }

    /**
     * Changes stock availability based on the orderWorkflow in which change occured.
     * If we are moving backward in OrderWorkflow we release to Stock, in other
     * case we acquire from it.
     *
     * @param OrderWorkflow $orderWorkflow  OrderWorkflow when change occured.
     * @param boolean       $force          Force change?
     * @throws NegativeStockException
     */
    public function changeStockAvailability($orderWorkflow, $force = false) {
        if ($orderWorkflow->getStockChange()) {
            $acquireFromStock = !$orderWorkflow->isBackward();
            if ($acquireFromStock) {
                $this->acquireFromStock($force);
            }
            else {
                $this->releaseToStock($force);
            }
        }
    }

    /**
     * Helper method for checking if entry has been modified and its value changed
     *
     * @param boolean $deep     whether to process also the relations for changes
     * @return boolean
     */
    public function isChanged($deep = false) {
        return $this->isModified($deep) &&
            (count(array_diff($this->getModified(true), $this->getModified())) > 0);
    }
}
