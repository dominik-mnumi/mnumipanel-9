<?php

/**
 * InvoiceCostTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class InvoiceCostTable extends Doctrine_Table
{
    public static $displayTableFields = array('invoice_number' => 'Invoice number',
        'invoice_date' => 'Invoice date', 'payment_date' => 'Payment date',
        'payment_at' => 'Payment at', 'notice' => 'Notice',
        'amount' => 'Amount', 'payed_amount' => 'Paid amount');
    
    /**
     * Returns an instance of this class.
     *
     * @return object InvoiceCostTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('InvoiceCost');
    }
    
    /**
     * Returns array with column aliases or methods to use if defined.
     * 
     * @return array
     */
    public function getDisplayTableFields()
    {
        $fieldArr = array();
        foreach(self::$displayTableFields as $key => $rec)
        {
            $fieldArr[$this->getClassnameToReturn().'.'.$key] = $rec;
        }
        
        return $fieldArr;
    }
    
    /**
     * Returns invoices by ids.
     * 
     * @param type $input
     * @return type 
     */
    public function getInvoicesById($input)
    {
        if(is_array($input))
        {
            $invoicesId = implode(',', $input);
        }

        return Doctrine_Query::create()
                    ->from('InvoiceCost ic')
                    ->where('ic.id IN ('.$invoicesId.')')
                    ->execute();
    }
    
    /**
     * Deletes many rows from table.
     *
     * @param $ids array
     * @return boolean
     */
    public function deleteMany($ids)
    {
        if(!is_array($ids))
        {
            throw new Exception('Items to delete must be specified.');
        }
        
        // gets all selected products
        $coll = $this->createQuery('c')
                ->whereIn('c.id', $ids)
                ->execute();

        $return = true;
        
        // foreach product which can delete
        foreach($coll as $rec)
        {
            if($rec->canDelete())
            {
                $rec->delete();
            }
            else
            {
                $return = false;
            }
        }
        
        return $return;
    }
}