<?php

/**
 * ClientTagTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ClientTagTable extends Doctrine_Table
{
    public static $displayTableFields = array('name' => 'Name');
    
    /**
     * Returns an instance of this class.
     *
     * @return object ClientTagTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ClientTag');
    }
    
    /**
     * Returns array with column aliases or methods to use if defined.
     * 
     * @return array
     */
    public function getDisplayTableFields()
    {
        $fieldArr = array();
        foreach(self::$displayTableFields as $key => $rec)
        {
            $fieldArr[$this->getClassnameToReturn().'.'.$key] = $rec;
        }
        
        return $fieldArr;
    }
    
    public function getJsonTagArray()
    {
        $tagArray = array();
        
        $clientTagColl = $this->createQuery()
                ->select('DISTINCT (name) as name')
                ->execute();
        
        foreach($clientTagColl as $rec)
        {
            $tagArray[] = $rec->getName();      
        }
        $tagJson = json_encode($tagArray); 
        
        return $tagJson;
    }
}