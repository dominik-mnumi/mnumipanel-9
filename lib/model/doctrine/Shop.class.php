<?php

/**
 * Shop
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    mnumicore
 * @subpackage model
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Shop extends BaseShop
{
    public function setValidDate($date)
    {
        if(empty($date))
        {
            return $this->_set('valid_date', NULL);
        }
        else
        {
            return $this->_set('valid_date', $date);
        }
    }
    
    public function canDelete()
    {
        return true;
    }
    
    /**
     * Returns token url for lost password (shop).
     * 
     * @param string $token
     * @return string
     */
    public function getLostPasswordTokenUrl($token)
    {
        return $this->getHost().'/user/lostPassword/'.$token;
    }

    public function __toString()
    {
        return ($this->getDescription() != '') ? $this->getDescription() : '----';
    }

    /**
     * Return language for shop
     * 
     * @return string
     */
    public function getShopLang()
    {
        return $this->getOption("lang", MnumiI18n::getCulture());
    }

    /**
     * Get option by name
     *
     * @param $name
     * @param null $default
     * @return null|string
     */
    private function getOption($name, $default = null)
    {
        /** @var ShopOption $option */
        foreach ($this->getShopOptions() as $option)
        {
            if($option->getName() == $name)
            {
                return $option->getValue();
            }
        }
        return $default;
    }
}