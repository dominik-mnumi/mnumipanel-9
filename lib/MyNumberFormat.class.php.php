<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extends sfNumberFormat class.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class MyNumberFormat extends sfNumberFormat
{
    protected function fixFloat($string)
    {
        $string = str_replace(',', '.', $string);
        return parent::fixFloat($string);
    }

}
