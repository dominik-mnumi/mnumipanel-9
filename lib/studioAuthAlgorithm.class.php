<?php

class studioAuthAlgorithm
{
    /**
     * Calculate password hash, using old StudioUH algorithm
     * @param string $str
     * @return string 
     */
    public static function calculate($str)
    {
        $str = trim($str); // remove space (it's come from salt)
        $str .= '33'; // add static number "33" at the end of string
        
        $str = md5($str); // create md5 hash
        return substr($str, 0, 20); // return only first 20 chars
    }
}