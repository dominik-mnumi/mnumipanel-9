<?php

/**
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class sfWidgetFormSelectRadioSingleable extends sfWidgetFormSelectRadio
{
    public function formatChoices($name, $value, $choices, $attributes)
    {
        $onlyChoice = !empty($attributes['only_choice']) ? $attributes['only_choice'] : false;
        unset($attributes['only_choice']);

        if ($onlyChoice)
        {
            $option = $choices[$onlyChoice];
            return parent::formatChoices($name, $value, array($onlyChoice => $option), $attributes);
        }

        return parent::formatChoices($name, $value, $choices, $attributes);
    }

    public function generateId($name, $value = null)
    {
        if($value == 0)
        {
            $value = time().rand(1, 50);
        }

        return parent::generateId($name, $value);
    }
}