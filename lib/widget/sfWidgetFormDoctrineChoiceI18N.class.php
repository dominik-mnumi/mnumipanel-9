<?php

/**
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfWidgetFormChoiceBase::getChoices() supports translation of choices
 * but sfWidgetFormDoctrineChoice::getChoices() returns choices without translation
 * This class adds translation to choices from sfWidgetFormDoctrineChoice::getChoices()
 */
class sfWidgetFormDoctrineChoiceI18N extends sfWidgetFormDoctrineChoice
{
    /**
     * Gets choices from sfWidgetFormDoctrineChoice::getChoices() and translates
     * @return array
     */
    public function getChoices()
    {
        $choices = parent::getChoices();
        if (empty($choices)) {
            return array();
        }
        $results = array();
        foreach ($choices as $key => $choice) {
            if (is_array($choice)) {
                $results[$this->translate($key)] = $this->translateAll($choice);
            } else {
                $results[$key] = $this->translate($choice);
            }
        }
        return $results;
    }
}