<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Import layout from external site.
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class configureWizardHostTask extends sfBaseTask
{
    /**
     * @see sfTask
     */
    protected function configure()
    {
        $this->namespace = 'mnumi';
        $this->name = 'configure-wizard-host';
        $this->briefDescription = 'Configure HOST for MnumiWizard application';

        $this->detailedDescription = '
            The [mnumi:configure-wizard-host|INFO] configure MnumiWizard with MnumiShop application.
            
            Usage example: 
              [./symfony mnumi:configure-wizard-host application hostname secretKey secretId|INFO]
        ';
        
        $this->addArguments(array(
            new sfCommandArgument('hostname', sfCommandArgument::REQUIRED, 'The application host name'),
            new sfCommandArgument('secretKey', sfCommandArgument::REQUIRED, 'The MnumiWizard secret key'),
            new sfCommandArgument('secretId', sfCommandArgument::OPTIONAL, 'The MnumiWizard secret key id'),
        ));
        
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore')
        ));

    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {
        $hostname = $arguments['hostname'];

        if(substr($hostname, 0, 7) != 'http://' && substr($hostname, 0, 8) != 'https://')
        {
            $this->logBlock('Hostname must have "http(s)://" at the begin.', 'ERROR');
            exit(1);
        }
        
        if(substr($hostname, -1) != '/')
        {
            $this->logBlock('Hostname must have slash "/" at the end.', 'ERROR');
            exit(1);
        }
        $appConfiguration = sfConfig::get('sf_app_config_dir') .'/app.yml';

        $secretKey = $arguments['secretKey'];
        $secretId = $arguments['secretId'];

        $config = sfYaml::load($appConfiguration);
        $config['all']['wizard']['host'] = $hostname;
        $config['all']['wizard']['secret_key'] = $secretKey;

        unset($config['all']['wizard']['id']);
        if($secretId) {
            $config['all']['wizard']['id'] = (int) $secretId;
        }

        $result = file_put_contents($appConfiguration, sfYaml::dump($config, 4)); 
        if($result === false)
        {
            $this->logBlock(sprintf('Could not save (%s) file.', $appConfiguration), 'ERROR');
            exit(1);
        }
        $this->clearCache();
        $this->logBlock('Saved new API configuration (shop host).', 'INFO');
        exit(0);
    }
    
    private function clearCache()
    {
        foreach(array('prod', 'dev') as $env)
        {
            $cacheDir = sfConfig::get('sf_cache_dir') . '/' . sfConfig::get('sf_app') . '/'.$env.'/';
            $cache = new sfFileCache(array('cache_dir' => $cacheDir));
            $cache->clean();
        }
        
    }

}

