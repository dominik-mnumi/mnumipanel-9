<?php

class MnumiFixOrderPriceTask extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            new sfCommandOption('linuxuser', null, sfCommandOption::PARAMETER_OPTIONAL, 'The linux user name'),
            new sfCommandOption('linuxgroup', null, sfCommandOption::PARAMETER_OPTIONAL, 'The linux group name')
        ));
        
        $this->namespace = 'mnumi';
        $this->name = 'fix-order-price';
        $this->briefDescription = 'Update order price.';
        $this->detailedDescription = <<<EOF
The [mnumi:fix-order-price|INFO] task does things.
Call it with:
  [php symfony mnumi:fix-order-price|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->log('Begin');

        /** @var Order $order */
        foreach(OrderTable::getInstance()->findAll() as $order)
        {
            try {
                $order->setBaseAmount($order->prepareBaseAmount());
                $order->updatePrices();
                $order->save();
            }
            catch(Exception $e) {
                $this->logBlock('Problem with order: ' . $order->getId() . ' - error: ' . $e->getMessage(), 'ERROR');
            }
        }

        $this->log('Finished.');
    }

}
