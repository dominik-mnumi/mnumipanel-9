<?php

class sahiDoSnapshotsTask extends sfBaseTask
{
  protected $fixture;

  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'test'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name'),
      // add your own options here
    ));

    $this->namespace        = 'sahi';
    $this->name             = 'do-snapshots';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [sahi:do-snapshots|INFO] task does things.
Call it with:

  [php symfony sahi:do-snapshots|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    require_once sfConfig::get('sf_test_dir').'/sahi/fixtures/SahiDoSnapshotContainer.php';

    $container = new SahiDoSnapshotContainer($this->dispatcher, $this->formatter);
    $container->doSnapshots();
  }
}