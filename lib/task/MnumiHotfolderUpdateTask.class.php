<?php

class MnumiHotfolderUpdateTask extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            new sfCommandOption('linuxuser', null, sfCommandOption::PARAMETER_OPTIONAL, 'The linux user name'),
            new sfCommandOption('linuxgroup', null, sfCommandOption::PARAMETER_OPTIONAL, 'The linux group name')
        ));
        
        $this->namespace = 'mnumi';
        $this->name = 'hotfolderUpdate';
        $this->briefDescription = 'Updates hotfolder of all orders images.';
        $this->detailedDescription = <<<EOF
The [mnumi:hotfolderUpdate|INFO] task does things.
Call it with:
  [php symfony mnumi:hotfolderUpdate|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logBlock('Start updating hotfolder', 'INFO');

        // COPY SECTION
        // gets empty order packages
        $date = new DateTime();
        $date->modify('-90 days');

        $orderColl = OrderTable::getInstance()
            ->createQuery()
            ->andWhere('updated_at > ?', $date->format('Y-m-d H:i:s'))
            ->orderBy('updated_at DESC')
            ->execute();

        $this->logBlock(' - copy order files to hotfolder', 'INFO');
        
        $success = 0;
        $failed = 0;
        $statuses = OrderStatusTable::getHotfolderStatuses();

        $this->logBlock('   accept statuses: ' . implode(', ', $statuses), 'INFO');


        foreach($orderColl as $order)
        {
            // if status is properly for hotfolder
            if(!in_array($order->getOrderStatusName(), $statuses))
            {
                continue;
            }

            $this->logBlock('  * add order id: ' . $order->getId() . ' status: ' . $order->getOrderStatusName(), 'INFO');

            try
            {
                $order->moveFilesToHotfolder();
                $success++;
            }
            catch(FileManagerException $e)
            {
                $this->logBlock($e->getMessage(), 'ERROR');
                $failed++;
            }
            catch(Exception $e)
            {
                $failed++;
            }           
        }
  
        // DELETE SECTION
        // gets product collection
        $productColl = ProductTable::getInstance()->getActive();

        $this->logBlock(' - delete old files from hotfolder', 'INFO');

        foreach($productColl as $rec)
        {
            // gets all files from database
            $filenameFromDbArr = $rec->getDbFileHotfolderArr(); 

            // gets current files from hotfolder
            $currentHotfolderFileArr = $this->dirScan($rec->getHotfolderDir().'*');

            // prepare files to delete (diff current to database)
            $toDeleteArr = array_diff($currentHotfolderFileArr, $filenameFromDbArr);           
            foreach($toDeleteArr as $rec2)
            {
                if(is_dir($rec2))
                {
                    continue;
                }

                if(!is_writable($rec2))
                {
                    $this->logBlock('Insufficient permissions to delete file: ' . $rec2, 'ERROR');
                    continue;
                }

                unlink($rec2);
            }
        }

        // UPLOAD SECTION
        $this->logBlock(' - upload files from /hotfolder/upload', 'INFO');
        $uploadReportArr = $this->importFilesFromUploadHotfolder();

        // sets user and group owner
        // if linuxuser is defined
        if(!empty($options['linuxuser']))
        {
            // if linuxuser is defined and linuxgroup is not defined
            if(empty($options['linuxgroup']))
            {
                $options['linuxgroup'] = $options['linuxuser'];
            }
            exec('chown '.$options['linuxuser'].':'.$options['linuxgroup'].' data/hotfolder/* -R');
        }
        
        // report
        $this->log('Summary report:');
        $this->log(' - Orders: success '.$success.', failed '.$failed);

        if($uploadReportArr)
        {
            $this->log(' - Import: success '.$uploadReportArr['success'].', failed '.$uploadReportArr['failed']);
        }
        
        $this->log('End Hotfolder updating.');
    }
    
    /**
     * 
     * @param string $folder
     * @return array
     */
    private function dirScan($folder)
    {
        $fileArr = glob($folder);
        foreach($fileArr as $key => $f) 
        {
            if(is_dir($f)) 
            {
                //unset($fileArr[$key]);            
                $fileArr = array_merge($fileArr, $this->dirScan($f.'/*')); // scan subfolder               
            }
        }
        return $fileArr;
    }

    /**
     * Imports files from hotfolder/uploads directory.
     * 
     * @return array
     */
    private function importFilesFromUploadHotfolder()
    {
        if((sfContext::hasInstance()) && (sfContext::getInstance()->getConfiguration()->getEnvironment() == 'test'))
        {
            $uploadDir = sfConfig::get('sf_data_dir').'/../test/phpunit/data/uploads/';
        }
        else
        {
            $uploadDir = sfConfig::get('sf_data_dir').'/hotfolder/uploads/';
        }

        if(!is_dir($uploadDir))
        {
            $basedir = dirname($uploadDir);

            if(!is_writable($basedir))
            {
                $this->logBlock('Insufficient permissions to create directory: ' . $uploadDir, 'ERROR');
                return false;
            }

            mkdir($uploadDir);
        }

        // gets all files from upload dir
        $uploadFileArr = $this->dirScan($uploadDir.'*');
        $successNr = 0;
        $failedNr = 0;
        foreach($uploadFileArr as $rec)
        {
            if(!is_dir($rec))
            {
                preg_match('/\/([0-9]+)\.([\w\.\(\)-_\s]+)$/i', $rec, $matchedArr);
                
                $orderId = $matchedArr[1];
                $fileBasename = $matchedArr[2];

                // gets order object
                $orderObj = OrderTable::getInstance()->find($orderId);
                if($orderObj)
                {      
                    try
                    {
                        $orderObj->importFile($rec, $fileBasename);
                        $orderObj->save();
                        
                        // for test do NOT remove upload files
                        if(!sfContext::hasInstance() 
                                ||(sfContext::hasInstance() 
                                && (sfContext::getInstance()->getConfiguration()->getEnvironment() != 'test')))
                        {
                            unlink($rec);
                        }
                            
                        $successNr++;
                    }
                    catch(Exception $e)
                    {
                        $this->logBlock('Could not upload file: ' . $rec . '. Message: ' . $e->getMessage(), 'ERROR');
                        $failedNr++;
                    }       
                }
                else
                {
                    $failedNr++;
                }    
            }
        }
    
        return array(
            'success' => $successNr,
            'failed' => $failedNr);
    }
}
