<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of mnumiCreateWebapi
 *
 * @author jupeter
 */
class mnumiCreateWebapiTask extends sfDoctrineBaseTask
{

    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            new sfCommandOption('type', null, sfCommandOption::PARAMETER_OPTIONAL, 'Api key type', 'other')
                // add your own options here
        ));

        $this->addArguments(array(
            new sfCommandArgument('expireDate', sfCommandArgument::REQUIRED, 'Expire date'),
            new sfCommandArgument('host', sfCommandArgument::REQUIRED, 'Hostname'),
            new sfCommandArgument('description', sfCommandArgument::OPTIONAL, 'Hostname'),
        ));

        $this->namespace           = 'generate';
        $this->name                = 'webapi-key';
        $this->briefDescription    = '';
        $this->detailedDescription = <<<EOF
The [generate:webapi-key|INFO] task does generate new webapi key.
Call it with:

  [php symfony generate:webapi-key "2012-12-12" host.com "Place for description"|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);


        // check for Shop class
        if(!class_exists('Shop'))
        {
            include_once(dirname(__FILE__).'/../model/doctrine/Shop.class.php');
            include_once(dirname(__FILE__).'/../model/doctrine/ShopTable.class.php');
        }

        $shop = new Shop();
        $shop->setName(ShopTable::getInstance()->generateName());
        $shop->setHost($arguments['host']);
        $shop->setValidDate($arguments['expireDate']);
        $shop->setDescription($arguments['description']);
        $shop->setType($options['type']);

        if($shop->save() === false)
        {
            $this->log('Could not create WebApi key.');
            exit(1);
        }

        echo $shop->name;
        exit(0);
    }

}

?>
