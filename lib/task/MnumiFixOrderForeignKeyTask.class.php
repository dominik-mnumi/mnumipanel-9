<?php

class MnumiFixOrderForeignKeyTask extends sfBaseTask
{
  protected function configure()
  {    
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'mnumi';
    $this->name             = 'fix-order-foreign-key';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [mnumi:fix-order-foreign-key|INFO] task fixes relation in orders table (foreign key: orders_client_id_orders_client_id).
Call it with:

  [php symfony mnumi:fix-order-foreign-key|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    try {

        $connection
                ->prepare("ALTER TABLE `orders` DROP FOREIGN KEY `orders_client_id_orders_client_id`")
                ->execute();

        $this->logSection('Fixed', 'foreign key: orders_client_id_orders_client_id) dropped');

    }
    catch(Exception $e)
    {
        $this->logSection('No action', 'Probably key was fixed and does not exist');
    }
    
  }
}
