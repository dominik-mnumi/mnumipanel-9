<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once('apps/mnumicore/lib/barcode/BarcodeTool.class.php');

class migrateStudioTask extends sfBaseTask
{
    protected $mysqli;
    
    protected function configure()
    {
        // // add your own arguments here
        $this->addArguments(array(
            new sfCommandArgument('dbhost', sfCommandArgument::REQUIRED, 'Database host'),
            new sfCommandArgument('dbusername', sfCommandArgument::REQUIRED, 'Database username'),
            new sfCommandArgument('dbpassword', sfCommandArgument::REQUIRED, 'Database password'),
            new sfCommandArgument('dbname', sfCommandArgument::REQUIRED, 'Database name'),
            new sfCommandArgument('studio_path', sfCommandArgument::REQUIRED, 'Studio path (http or relative) - for images')
        ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
                // add your own options here
        ));

        $this->namespace           = 'mnumi';
        $this->name                = 'migrateStudio';
        $this->briefDescription    = '';
        $this->detailedDescription = <<<EOF
The [migrateStudio|INFO] task does things.
Call it with:

  [php symfony migateStudio|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('migrate', 'Migrating from database: "'.$arguments['dbname'].'" using username: "'.
                $arguments['dbusername'].'" and password: "'.$arguments['dbpassword'].'"');

        $this->mysqli = mysqli_connect($arguments['dbhost'] ? $arguments['dbhost'] : 'localhost', $arguments['dbusername'], $arguments['dbpassword']);
        mysqli_select_db($this->mysqli, $arguments['dbname']) or die();
        if(mysqli_connect_errno())
        {
            $this->logSection('migrate', 'Error during connect', null, 'ERROR');
            return;
        }
        $this->mysqli->query("SET NAMES 'utf8'");

        // GENERATE TEST USERS !! ONLY FOR TESTING PURPOSE!!
        //$this->generateTestsUsers();
        // END OF GENERATING TESTS USERS
        
        // imports users
        $clientUsersIds = $this->importUsers();
        
        // imports orders
        $orders = $this->importOrders($clientUsersIds);

        // imports files
        $this->importFiles($orders, $arguments['studio_path']);
        
        // imports orders
        $this->importInvoices($clientUsersIds);    
    }

    /**
     * Generates admin user.
     */
    protected function generateAdminUser()
    {      
        $adminObj = new sfGuardUser();
        $adminObj->setEmailAddress('admin@email.com');
        $adminObj->setUsername('admin');         
        $adminObj->setPassword('admin098');
        $adminObj->setIsActive(1);
        $adminObj->setIsSuperAdmin(1);
        $adminObj->save();

        $adminGroupObj = sfGuardGroupTable::getInstance()
                ->findOneByName(sfGuardGroupTable::$admin);

        $adminUser = new sfGuardUserGroup();
        $adminUser->setUserId($adminObj->getId());
        $adminUser->setGroupId($adminGroupObj->getId());
        $adminUser->save();    
    }

    /**
     * Returns sfGuardGroup object.
     * 
     * @param array $userRow
     * @return sfGuardGroup
     */
    protected function getGroupPermissionObjBasingOnUserFromStudio($userRow)
    {
        if(strstr($userRow['Perms'], 'admin'))
        {
            return sfGuardGroupTable::getInstance()
                    ->findOneByName(sfGuardGroupTable::$admin);
        }
        elseif(strstr($userRow['Perms'], 'office'))
        {
            return sfGuardGroupTable::getInstance()
                    ->findOneByName(sfGuardGroupTable::$office);
        }
        elseif(strstr($userRow['Perms'], 'finance'))
        {
            return sfGuardGroupTable::getInstance()
                    ->findOneByName(sfGuardGroupTable::$finance);
        }
        elseif(strstr($userRow['Perms'], 'graphic'))
        {
            return sfGuardGroupTable::getInstance()
                    ->findOneByName(sfGuardGroupTable::$graphic);
        }
    }
    
    /**
     * Imports users from oldcore.
     * 
     * @return array $clientUsersIds
     */
    protected function importUsers()
    {
        $this->logSection('migrate', 'migrating users and clients...');
        
        $query = "select * from didb_users where Service = 'STUD'";

        $result = $this->mysqli->query($query);

        $admin = sfGuardGroupTable::getInstance()->findOneByName(sfGuardGroupTable::$admin);
        
        //!! ADMIN CLIENT PERMISSIONS FOR ALL CLIENTS!! - PLEASE CHECK IT
        $clientUserPermission = ClientUserPermissionTable::getInstance()
                ->findOneByName(ClientUserPermissionTable::$admin);

        $sms = NotificationTypeTable::getInstance()->findOneByName(NotificationTypeTable::$sms);
        
        $pricelists = array();
        foreach(PricelistTable::getInstance()->findAll() as $pricelist)
        {
            $pricelists[] = $pricelist->getId();
        }

        $clientUsersIds = array();
        $uniqueUsernameArr = array();
        $uniqueEmailArr = array();
        while($row = $result->fetch_array())
        {  
            // sfGuardUser
            $user = new sfGuardUser();

            $name = explode(' ', $row['OsobaKontaktowa']);
            if(isset($name[0]))
            {
                $user->setFirstName($name[0]);
            }
            if(isset($name[1]))
            {
                $user->setLastName($name[1]);
            }
            
            // sets unique username
            $username = $row['OsobaKontaktowa'];
            if($username == 'Admin')
            {
                $username = strtolower($username);
            }
            if(sfGuardUserTable::getInstance()->findOneByUsername($username))
            {
                $username  = '';
            }

            // sets unique email
            $email = $row['Email'];
            if(sfGuardUserTable::getInstance()->findOneByEmailAddress($email))
            {
                $email  = time();
            }

            $user->setEmailAddress($email);
            $user->setUsername($username);
            $user->setAlgorithm('studioAuthAlgorithm::calculate');           
            $user->setPasswordHash($row['Haslo']);
            $user->setSalt('');
            $user->setIsActive(1);
            $user->setLastLogin($row['Time_login']);
            $user->setCreatedAt($row['Time_added']);
            $user->setUpdatedAt($row['Time_modified']);
            
            if($username == 'admin')
            {
                $user->setIsSuperAdmin(1);
            }
            
            $user->save();

            if($row['Admin'])
            {
                $adminUser = new sfGuardUserGroup();
                $adminUser->setUserId($user->getId());
                $adminUser->setGroupId($admin->getId());
                $adminUser->save();
            }

            $convertedNIP = '';
            if($row['NIP'])
            {
                $convertedNIP = str_replace(array(' ', '-'), array('', ''), $row['NIP']);
            }
            
            // client
            $client = new Client();
            $client->setFullname($row['OsobaKontaktowa']);
            $client->setCreditLimitAmount($row['LimitKredytowy']);
            $client->setCity($row['F_miasto']);
            $client->setPostcode($row['F_kod']);
            $client->setStreet($row['F_adres']);
            $client->setCountry($row['F_cc']);
            $client->setTaxId($convertedNIP);
            $client->setCreatedAt($row['Time_added']);
            $client->setUpdatedAt($row['Time_modified']);
            if(in_array($row['IDCennik'], $pricelists))
            {
                $client->setPricelistId($row['IDCennik']);
            }
            $client->save();

            $clientAddress = new ClientAddress();
            $clientAddress->setClientId($client->getId());
            $clientAddress->setCity($row['F_miasto']);
            $clientAddress->setPostcode($row['F_kod']);
            $clientAddress->setStreet($row['F_adres']);
            $clientAddress->setCountry($row['F_cc']);
            $clientAddress->setCreatedAt($row['Time_added']);
            $clientAddress->setUpdatedAt($row['Time_modified']);
            $clientAddress->save();

            if($row['Telefon'])
            {
                $phone = new UserContact();
                $phone->setUserId($user->getId());
                $phone->setNotificationTypeId($sms->getId());
                $phone->setSendNotification(0);
                $phone->setValue($row['Telefon']);
                $phone->save();
            }

            if($row['Telkom'])
            {
                $phone = new UserContact();
                $phone->setUserId($user->getId());
                $phone->setNotificationTypeId($sms->getId());
                $phone->setSendNotification($row['Sms'] ? 1 : 0 );
                $phone->setValue($row['Telkom']);
                $phone->save();
            }

            // client user permissions
            $clientUser = new ClientUser();
            $clientUser->setClientId($client->getId());
            $clientUser->setUserId($user->getId());
            $clientUser->setClientUserPermissionId($clientUserPermission->getId());
            $clientUser->save();

            // user group permissions
            $groupPermissionObj = $this->getGroupPermissionObjBasingOnUserFromStudio($row);           
            if($groupPermissionObj)
            {
                $userGroupObj = new sfGuardUserGroup();
                $userGroupObj->setUser($user);
                $userGroupObj->setGroup($groupPermissionObj);
                $userGroupObj->save();
            }
            
            // needed later for mapping order user and client
            $clientUsersIds[$row['IDklienta']] = array(
                'clientId' => $client->getId(), 
                'userId'   => $user->getId());  
        }
        
        // if admin user does not exist
        $adminObj = sfGuardUserTable::getInstance()->findOneByUsername('admin');
        if(!$adminObj)
        {
            // generates user
            $this->generateAdminUser(); 
        }
        
        return $clientUsersIds;
    }
    
    /**
     * Imports orders from oldcore.
     * 
     * @param $clientUsersIds
     */
    protected function importOrders($clientUsersIds)
    {
        $this->logSection('migrate', 'migrating orders...');

        // get all Products
        $products = ProductTable::getInstance()->findAll();
        $prods    = array();
        foreach($products as $product)
        {
            $prods["{$product->getSlug()}"] = $product;
        }

        // 3 - paid
        // 4 - booked
        // 7 - ready
        $query = "select * from praca 
            where product_name is not null and product_name <> '' and (Status = 7 or Status = 4) and 
            IDDeliverBy is not null and IDDeliverBy <> '' order by IDpaczka, IDpracy";

        $result    = $this->mysqli->query($query);
        $packageId = null;

        while($row = $result->fetch_array())
        {
            if(array_key_exists($row['product_name'], $prods)
                    && array_key_exists($row['IDklienta'], $clientUsersIds))
            {
                $clientUser = $clientUsersIds[$row['IDklienta']];

                if($row['IDpaczka'] != $packageId)
                {
                    $orderPackage = new OrderPackage();
                    $orderPackage->setCarrierId($row['IDDeliverBy']);
                    $orderPackage->setDeliveryName($row['Klient_Nazwa']);
                    $orderPackage->setDeliveryStreet($row['Klient_Adres']);
                    $orderPackage->setDeliveryCity($row['Klient_Miasto']);
                    $orderPackage->setDeliveryPostcode($row['Klient_Kod']);
                    $orderPackage->setDeliveryCountry('PL');
                    $orderPackage->setInvoiceName($row['Fakt_Nazwa']);
                    $orderPackage->setInvoiceStreet($row['Fakt_Adres']);
                    $orderPackage->setInvoiceCity($row['Fakt_Miasto']);
                    $orderPackage->setInvoicePostcode($row['Fakt_Kod']);
                    $orderPackage->setInvoiceTaxId($row['Klient_NIP']);
                    $orderPackage->setInvoiceInfo('');
                    $orderPackage->setPaymentId($row['Payment']);
                    
                    // PAYMENT STATUS !! IF Zaplacona_kwota == BodyPlusPrice THEN -> paid
                    if($row['BodyPlusPrice'] == $row['Zaplacona_kwota'])
                    {
                        $orderPackage->setPaymentStatusName(PaymentStatusTable::$paid);
                    }
                    else
                    {
                        $orderPackage->setPaymentStatusName(PaymentStatusTable::$waiting);
                    }
                    
                    $orderPackage->setClientId($clientUser['clientId']);
                    $orderPackage->setOrderPackageStatusName('completed');
                    $orderPackage->setUserId($clientUser['userId']);
                    
                    /**
                    if($row['Status'] == 4)
                    {
                        $orderPackage->setToBook(1);
                    }
                    else
                    {
                        $orderPackage->setToBook(0);
                    }
                    **/
                    $orderPackage->setToBook(1); 

                    $orderPackage->setSendAt($row['ShippingDate']);
                    $orderPackage->setCreatedAt($row['DataZamowienia']);
                    $orderPackage->setUpdatedAt($row['dotkniety']);
                    $orderPackage->save();

                    $packageId = $orderPackage->getId();
                }

                $product = $prods["{$row['product_name']}"];
                /*
                  $pfs = ProductFieldTable::getInstance()->findAll();
                  $productFields = array();

                  foreach($pfs as $pf)
                  {
                  $i = $pf->getDefaultValue().';'.$pf->getProductId();
                  $productFields["$i"] = $pf->getId();
                  }
               */
                $attributes = unserialize($row['product_data']);

                $order = new Order();
                $order->setName($row['nazwa']);
                $order->setPriceNet($row['BodyPlusPrice']);
                
                // check if tax is const is STUDIO !!
                $order->setTaxValue(23);
                $order->setClientId($clientUser['clientId']);
                $order->setUserId($clientUser['userId']);
                $order->setOrderStatusName('ready');
                $order->setOrderPackageId($orderPackage->getId());
                $order->setProduct($product);
                if(isset($attributes['fields']['INFORMATIONS']))
                {
                    $order->setInformations($attributes['fields']['INFORMATIONS']);
                }
                $order->setCreatedAt($row['DataZamowienia']);
                $order->setUpdatedAt($row['dotkniety']);
                $order->save();

                $orders[$row['IDpracy']] = $order->getId();


                if(isset($attributes['fields']['SIZE_WIDTH']) && isset($attributes['fields']['SIZE_HEIGHT']))
                {
                    $customSize = new CustomSize();
                    $customSize->setOrder($order);
                    $customSize->setWidth($attributes['fields']['SIZE_WIDTH']);
                    $customSize->setHeight($attributes['fields']['SIZE_HEIGHT']);
                    $customSize->save();
                }

                foreach($attributes['fields'] as $key => $attribute)
                {

                    if($key == 'SIZE_WIDTH' || $key == 'SIZE_HEIGHT' || $key == 'SIZE_METRIC' || $key == 'INFORMATIONS' || $key == 'ORDER_ID')
                    {
                        continue;
                    }

                    // for normal attributes
                    if(strpos($key, 'OTHER') === false
                            && strpos($key, 'WIZARD') === false)
                    {
                        $attr = new OrderAttribute();
                        $attr->setValue($attribute);
                        $attr->setOrder($order);
                        $attr->setProductField($product->getProductFieldByFieldsetName($key));
                        $attr->save();
                    }
                    // for others or wizards
                    else
                    {
                        if(is_array($attribute))
                        {
                            foreach($attribute as $att)
                            {
                                //$j = $attr.';'.$product->getId();
                                //$productFieldId = $productFields["$j"];
                                $pf = ProductFieldTable::getInstance()
                                        ->createQuery('pf')
                                        ->leftJoin('pf.ProductFieldItems pfi ON pf.id = pfi.product_field_id')
                                        ->where('pfi.field_item_id = ?', $att)
                                        //->andWhere('pf.product_id = ?', $product->getId())
                                        ->fetchOne();

                                if($att > 0)
                                {
                                    $attr = new OrderAttribute();
                                    $attr->setValue($att);
                                    $attr->setOrder($order);
                                    $attr->setProductField($pf);
                                    $attr->save();
                                }
                            }
                        }
                        else
                        {
                            //$j = $attribute.';'.$product->getId();
                            //$productFieldId = $productFields["$j"];

                            $pf = ProductFieldTable::getInstance()
                                    ->createQuery('pf')
                                    ->leftJoin('pf.ProductFieldItems pfi ON pf.id = pfi.product_field_id')
                                    ->where('pfi.field_item_id = ?', $attribute)
                                    //->andWhere('pf.product_id = ?', $product->getId())
                                    ->fetchOne();

                            if($attribute > 0)
                            {
                                $attr = new OrderAttribute();
                                $attr->setValue($attribute);
                                $attr->setOrder($order);
                                $attr->setProductField($pf);
                                $attr->save();
                            }
                        }
                    }
                }
            }
        }
        
        return $orders;
    }
    
    /**
     * Imports invioces from oldcore.
     * 
     * @return array $clientUsersIds
     */
    protected function importInvoices($clientUsersIds)
    {
        $this->logSection('migrate', 'migrating invoices...');

        $payments = PaymentTable::getInstance()->findAll();
        $pays     = array();
        foreach($payments as $payment)
        {
            $pays[] = $payment->getId();
        }

        $query  = "select * from pozycjafaktury";
        $result = $this->mysqli->query($query);

        $invoiceItems = array();
        while($row = $result->fetch_array())
        {
            $invoiceItems[$row['IDFaktura']] = array_merge(array($row), 
                    isset($invoiceItems[$row['IDFaktura']]) 
                        ? $invoiceItems[$row['IDFaktura']] 
                        : array());
        }

        $query  = "select * from faktura order by DataWystawienia";
        $result = $this->mysqli->query($query);

        $measureOfUnit = array(1 => 'pc', 2 => 'A4', 3 => 'godz.', 4 => 'm2', 5 => 'kpl.');

        while($row = $result->fetch_array())
        {
            if(!$row['SposObPLatnosci'])
            {
                $this->logSection('migrate', 'Invoice doesnt have payment_id. Invoice number: '.$row['IDFaktura'], null, 'ERROR');
                continue;
            }
            if(!in_array($row['SposObPLatnosci'], $pays))
            {
                $this->logSection('migrate', 'Payment doesnt exist in core. Id: '.$row['SposObPLatnosci'], null, 'ERROR');
                continue;
            }
            if(isset($clientUsersIds[$row['IDKlient']]))
            {
                $clientUser = $clientUsersIds[$row['IDKlient']];

                $addressArr = explode("\n", $row['AdresKlienta']);
                $postcodeAndCityArr = isset($addressArr[1]) ? explode(' ', $addressArr[1]) : array('', '');

                $invoiceType = $row['FakturaVAT'] 
                        ? InvoiceTypeTable::$INVOICE
                        : InvoiceTypeTable::$RECEIPT;
                
                $convertedNIP = '';
                if($row['NIP'])
                {
                    $convertedNIP = str_replace(array(' ', '-'), array('', ''), $row['NIP']);
                }
                
                $invoice = new Invoice();
                $invoice->setClientId($clientUser['clientId']);
                $invoice->setInvoiceTypeName($invoiceType);               
                $invoice->setClientName($row['NazwaKlienta']);
                $invoice->setClientAddress($addressArr[0]);
                $invoice->setClientPostcode($postcodeAndCityArr[0]);
                $invoice->setClientCity($postcodeAndCityArr[1]);
                $invoice->setClientTaxId($convertedNIP);
                $invoice->setPaymentId($row['SposObPLatnosci']);
                $invoice->setPaymentDateAt($row['DataPlatnosci']);
                if($row['DataZaplaty'] && $row['DataZaplaty'] != '0000-00-00 00:00:00')
                {
                    $invoice->setPayedAt($row['DataZaplaty']);
                }
                $invoice->setNotice($row['Uwagi']);
                $invoice->setSellAt($row['DataWystawienia']);
                $invoice->setUpdatedAt($row['DataWystawienia']);
                $invoice->setCreatedAt($row['DataWystawienia']);                
                $invoice->save();

                // little hack to set invoice payed without trigering preUpdate (generating cash desk etc)

                if($row['DataZaplaty'])
                {
                    Doctrine_Query::create()
                            ->update('Invoice i')
                            ->set('i.invoice_status_name', '?', InvoiceStatusTable::$paid)
                            ->where('i.id = ?', $invoice->getId())
                            ->execute();
                }

                if(isset($invoiceItems[$row['IDFaktura']]))
                {
                    foreach($invoiceItems[$row['IDFaktura']] as $item)
                    {
                        $invItem = new InvoiceItem();
                        $invItem->setInvoice($invoice);
                        $invItem->setTaxValue(23);
                        if($item['IDPracy'] && isset($orders[$item['IDPracy']]))
                        {
                            $invItem->setOrderId($orders[$row['IDPracy']]);
                        }
                        $invItem->setDescription(strip_tags($item['Opis']));
                        $invItem->setQuantity($item['Ilosc']);
                        $invItem->setPriceNet($item['Cena']);
                        $invItem->setPriceDiscount($item['Rabat']);
                        $invItem->setUnitOfMeasure($measureOfUnit[$item['JednMiary']]);
                        $invItem->setCreatedAt($item['OstatnieUzycie']);
                        $invItem->setUpdatedAt($item['OstatnieUzycie']);
                        $invItem->save();
                    }
                }
                
                $invoice->refresh(true);
                
                // updates price basing on invoice items
                $invoice->updatePrice();
                
                $invoice->setSellAt($row['DataWystawienia']);
                $invoice->setUpdatedAt($row['DataWystawienia']);
                $invoice->save();
            }
            else
            {
                $this->logSection('migrate', 'Error: no client with id: '.$row['IDKlient'], 
                        null, 
                        'ERROR');
            }
        }


        // =====================================  cost invoices  ========================================

        $this->logSection('migrate', 'migrating cost invoices...');

        $clients = array();

        $query  = "select * from koszty_firmy";
        $result = $this->mysqli->query($query);


        while($row = $result->fetch_array())
        {
            $client   = new Client();
            $client->setFullname($row['nazwa']);
            $client->setCreditLimitAmount(0);
            $cityCode = explode(' ', $row['miasto'], 2);
            $client->setCity(@$cityCode[1]);
            $client->setPostcode(@$cityCode[0]);
            $client->setStreet($row['ulica']);
            $client->setBalance($row['bilans']);
            $client->setAccount($row['konto']);
            $client->save();

            $clients[$row['kf_id']] = $client->getId();
        }

        $query  = "select * from koszty_faktury";
        $result = $this->mysqli->query($query);


        while($row = $result->fetch_array())
        {
            $invoiceCost = new InvoiceCost();
            $invoiceCost->setInvoiceNumber($row['zaco']);
            $invoiceCost->setInvoiceDate($row['wystawiona']);
            $invoiceCost->setPaymentDate($row['termin']);
            $invoiceCost->setPaymentAt($row['oplacono']);
            // what about user ?
            //$invoiceCost->setUserId($row['']);
            $invoiceCost->setNotice($row['uwagi']);
            $invoiceCost->setClientId($clients[$row['firma']]);
            $invoiceCost->setAmount($row['kwota']);
            $invoiceCost->setPayedAmount($row['zaplacono']);
            $invoiceCost->save();
        }

        // !!! Unable to link koszty_platnosci with koszty_faktury (but in new core there is relation between invoice_cost and invoice_cost_pamyent
        // =====================================  kasy  ========================================

        $this->logSection('migrate', 'migrating cash desk...');

        $payment = PaymentTable::getInstance()->findOneByName(PaymentTable::$cashPayment);

        if(!$payment)
        {
            $payment   = PaymentTable::getInstance()->findOneByName('płatność gotówką');
            // maybe we should do this for all payments??
            $payment->setName(PaymentTable::$cashPayment);
            $payment->save();
        }
        $paymentId = $payment->getId();

        $query  = "select * from kasa_raport";
        $result = $this->mysqli->query($query);

        $cashReports = array();

        while($row = $result->fetch_array())
        {
            $cashReport = CashReportTable::getInstance()->createCurrentDailyReportOfPaymentType($paymentId);
            $cashReport->setCreatedAt($row['data']);
            $cashReport->setUpdatedAt($row['data']);
            $cashReport->save();

            $cashReports[$row['uid']] = $cashReport->getId();
        }

        $types = array('wplata'   => CashDeskTable::$IN, 'zaliczka' => CashDeskTable::$IN, 'wyplata'  => CashDeskTable::$OUT, 'blokada'  => CashDeskTable::$BLOCK);

        $query  = "select * from kasa_transakcja";
        $result = $this->mysqli->query($query);


        while($row = $result->fetch_array())
        {
            $cashDesk = new CashDesk();
            $cashDesk->setNumber(CashDeskTable::getInstance()->getNextNumber($types["{$row['typ']}"], $paymentId));
            $cashDesk->setPaymentId($paymentId);
            $cashDesk->setType($types["{$row['typ']}"]);
            if($clientUsersIds[$row['IDpracownik']])
            {
                $clientUser = $clientUsersIds[$row['IDpracownik']];
                $cashDesk->setClientId($clientUser['clientId']);
                $cashDesk->setUserId($clientUser['userId']);
            }

            if($types["{$row['typ']}"] == 'IN')
            {
                $balance = CashDeskTable::getInstance()->getLastBalance($paymentId) + abs($row['wartosc']);
                $value   = abs($row['wartosc']);
            }
            elseif($types["{$row['typ']}"] == 'OUT' || $types["{$row['typ']}"] == 'BLOCK')
            {
                $balance = CashDeskTable::getInstance()->getLastBalance($paymentId) - abs($row['wartosc']);
                $value   = abs($row['wartosc']) * (-1);
            }
            else
            {
                throw new Exception('Unknown payment type for cash desk');
            }

            $cashDesk->setDescription($row['opis']);
            $cashDesk->setValue($value);
            $cashDesk->setBalance($balance);

            if($row['IDraport'])
            {
                $cashDesk->setCashReportId($cashReports[$row['IDraport']]);
            }
            $cashDesk->save();
        }
    }
    
    /**
     * Imports files from oldcore.
     * 
     * @param array $orders
     * @param string $studioPath
     */
    protected function importFiles($orders, $studioPath)
    {
        $this->logSection('migrate', 'migrating files...');

        // get all files
        $query = "select * from Plik";

        $result = $this->mysqli->query($query);
        $files  = array();

        while($row = $result->fetch_array())
        {
            $files[$row['IDPraca']][] = $row['filename'];
        }

        // old IDpracy => current order id
        foreach($orders as $key => $currentOrderId)
        {
            // if order have files
            if(isset($files[$key]))
            {
                foreach($files[$key] as $oldFilename)
                {
                    $basename = basename($oldFilename);
                    $oldFullFilepath = $studioPath.$oldFilename;

                    // if old core file exist
                    if(file_exists($oldFullFilepath))
                    {
                        // old file content
                        $file = file_get_contents($oldFullFilepath);
                        if($file)
                        {
                            // gets order object
                            $orderObj = OrderTable::getInstance()->find($currentOrderId);
                   
                            $target = $orderObj->getFilePath().$basename;
                            
                            // if file exists
                            if(file_exists($target))
                            {
                                $this->logSection('migrate', 'file: '.$target.' exists. Only database insert.');
                                $orderObj->importFile($target);        
                            }
                            else
                            {    
                                // copy to cache dir
                                $target2 = sfConfig::get('sf_cache_dir').'/'.$basename;
                                
                                $this->logSection('migrate', 'file: '.$target.' does not exist. Database insert and file coping to: '.$target2);
                                
                                file_put_contents($target2, $file);
                                $orderObj->importFile($target2);
                                unlink($target2); 
                            }
                                     
                        }
                    }
                }
            }
        }       
    }
}
