<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once('plugins/sfDoctrinePlugin/lib/task/sfDoctrineBaseTask.class.php');

/**
 * Installs data.
 */
class MnumiDataDumpTask extends sfDoctrineBaseTask
{

    /**
     * @see sfTask
     * 
     * example cli use: ./symfony mnumi:data-dump [--application="mnumicore"] [--env="dev"] [--no-confirmation] [--language="en"] dbHost dbUser dbPass dbName 
     * language default: en
     */
    protected function configure()
    {
        $this->addArguments(array(
            new sfCommandArgument('dbHost', sfCommandArgument::OPTIONAL, 'Database host'),            
            new sfCommandArgument('dbUser', sfCommandArgument::OPTIONAL, 'Database user'),
            new sfCommandArgument('dbPass', sfCommandArgument::OPTIONAL, 'Database password'),
            new sfCommandArgument('dbName', sfCommandArgument::OPTIONAL, 'Database name')
        ));

        $this->addOptions(array(
            new sfCommandOption('language', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application language', 'en_US'),
            new sfCommandOption('no-confirmation', null, sfCommandOption::PARAMETER_NONE, 'Whether to force proceed'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'mnumicore'),         
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev')
        ));

        $this->namespace        = 'mnumi';
        $this->name             = 'data-dump';
        $this->briefDescription = 'Dumps data to sql file';

        $this->detailedDescription = <<<EOF
The [mnumi:dump-data|INFO] task creates data sql file:
EOF;
    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {
        if(!$options['no-confirmation']
           && !$this->askConfirmation(array_merge(
                array('This command will create sql data file.'), 
                array(), 
                array('', 'Are you sure you want to proceed? (y/N)')), 
                'QUESTION_LARGE', 
                false))
        {
            $this->logSection('mnumi', 'task aborted');

            return 1;
        }

        $this->logSection('mnumi', 'data dump...');
        
        // sets language
        sfContext::createInstance($this->configuration)
                ->getUser()
                ->setCulture($options['language']);

        $i18NObj = sfContext::getInstance()->getInstance()->getI18N();

        // gets tables with translatable fields
        $tableFieldArr = sfYaml::load(sfConfig::get("sf_lib_dir").'/MnumiData/translatableFields.yml');

        // if parameters not defined then gets default connection
        if(!$arguments['dbHost'] && !$arguments['dbUser'] && !$arguments['dbPass']
                && !$arguments['dbName'])
        {
            $databaseManager = new sfDatabaseManager($this->configuration);
            $databaseConnection = $databaseManager->getDatabase('doctrine');
            
            $username = $databaseConnection->getParameter('username');
            $password = $databaseConnection->getParameter('password');
            $dsnInfo = $this->parseDsn( $databaseConnection->getParameter('dsn'));
            
            $arguments['dbHost'] = $dsnInfo['host'];
            $arguments['dbUser'] = $username;
            $arguments['dbPass'] = $password;
            $arguments['dbName'] = $dsnInfo['dbname'];
        }
        
        // creates MnumiDataDump object
        $mnumiDataDumpObj = new MnumiDataDump(
                $arguments['dbHost'],                
                $arguments['dbUser'],
                $arguments['dbPass'], 
                $arguments['dbName'], 
                $i18NObj, 
                $tableFieldArr['all']);

        $mnumiDataDumpObj->generateSqlFile();
        
        // loads sample data
        $this->logSection('mnumi', 'dump finished successfully...');

        
    }

    /**
     * Clears cache.
     */
    protected function clearCache()
    {
        foreach(array('prod', 'dev') as $env)
        {
            $cacheDir = sfConfig::get('sf_cache_dir').'/'.sfConfig::get('sf_app').'/'.$env.'/';
            $cache    = new sfFileCache(array('cache_dir' => $cacheDir));
            $cache->clean();
        }
    }
    
    /**
     * Parses string and returns array with host and dbname parameters.
     * 
     * @param string $dsn
     * @return array
     */
    private function parseDsn($dsn)
    {
        $dsnArray = array();
        $dsnArray['phptype'] = substr($dsn, 0, strpos($dsn, ':'));
        preg_match('/dbname=(\w+)/', $dsn, $dbname);
        $dsnArray['dbname'] = $dbname[1];
        preg_match('/host=(\w+)/', $dsn, $host);
        $dsnArray['host'] = $host[1];

        return $dsnArray;
    }

}