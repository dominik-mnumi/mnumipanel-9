<?php

class cronjobArchivePackagesTask extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
        ));

        $this->namespace = 'cronjob';
        $this->name = 'archivePackages';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [cronjob:archivePackages|INFO] task does things.
Call it with:
  [php symfony cronjob:archivePackages|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->log('Start archiving packages');

        // gets empty order packages
        $orderPackagesColl = OrderPackageTable::getInstance()->getEmptyPackages();
        
        $success = 0;
        $failed = 0;
        foreach($orderPackagesColl as $rec)
        {
            try
            {
                $rec->setOrderPackageStatusName(OrderPackageStatusTable::$archive);
                $rec->save();
                
                $success++;
            }
            catch(Exception $e)
            {
                $failed++;
            }           
        }
        
        $this->log('Success: '.$success.'  Failed: '.$failed);
        $this->log('Package archiving end');
    }

}
