<?php

class cronjobSendNotificationsTask extends sfBaseTask
{

    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
                // add your own options here
        ));

        $this->namespace = 'cronjob';
        $this->name = 'sendNotifications';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [cronjob:sendNotifications|INFO] task does things.
Call it with:

  [php symfony cronjob:sendNotifications|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
        $context = sfContext::hasInstance()
            ? sfContext::getInstance()
            : sfContext::createInstance($this->configuration);

        $this->log('Start notifications sending');

        $notificationsObj = new Notifications($context);
        $notifications = $notificationsObj->getNotificationsToSend(100);

        $success = $failed = 0;

        foreach($notifications as $n)
        {
            $type = strtolower($n->getNotificationType()->getName());
            $results = false;

            if($type == 'e-mail' || $type == 'email' || $type == 'mail')
            {
                try
                {
                    $notification = new NotificationEmail($n);
                    $results = $notification->send($this->getMailer());
                }
                catch(Exception $e)
                {
                    $this->logBlock('Could not send e-mail for NotificationMessage id: ' . $n->getId(), 'ERROR');
                }
            }
            elseif($type == 'sms')
            {
                try
                {
                    $notification = new NotificationSMS($n);
                    $results = $notification->send();
                }
                catch(Exception $e)
                {
                    $this->logBlock('Could not send SMS for NotificationMessage id: ' . $n->getId(), 'ERROR');
                }
            }
            else
            {
                $this->logBlock('Wrong notification type ' . $type . ' for NotificationMessage id: ' . $n->getId(), 'ERROR');
            }

            if($results == true)
            {
                $success++;
            }
            else
            {
                $failed++;
            }
        }
        $this->log('Success: '.$success.'  Failed: '.$failed);
        $this->log('Sending end');
    }

}
