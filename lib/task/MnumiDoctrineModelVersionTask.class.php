<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Installs data.
 */
class MnumiDoctrineModelVersionTask extends sfDoctrineBaseTask
{

    /**
     * @see sfTask
     * 
     * example cli use: ./symfony mnumi:data-dump [--application="mnumicore"] [--env="dev"] [--no-confirmation] [--language="en"] dbHost dbUser dbPass dbName 
     * language default: en
     */
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev')
        ));

        $this->namespace        = 'doctrine';
        $this->name             = 'build-model-version';
        $this->briefDescription = 'Creates version classes for the current model';

        $this->detailedDescription = <<<EOF
The [mnumi:dump-data|INFO] task creates version classes for model:
EOF;
    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $environment     = $this->configuration instanceof sfApplicationConfiguration ? $this->configuration->getEnvironment() : 'all';

        $builderOptions = $this->configuration->getPluginConfiguration('sfDoctrinePlugin')->getModelBuilderOptions();

        $config = $this->getCliConfig();

        $schema = $this->prepareSchemaFile($config['yaml_schema_path']);

        $import = new Doctrine_Import_Schema();
        $import->setOptions($builderOptions);
        $import->importSchema($schema, 'yml', $config['models_path']);

        // markup base classes with magic methods
        foreach (sfYaml::load($schema) as $model => $definition)
        {
            $actAs = (array_key_exists('actAs', $definition) ? $definition['actAs'] : array());

            if(array_key_exists('Versionable', $actAs)) {
                $className = array_key_exists('className', $definition)
                    ? $definition['className']
                    : $model;

                $this->logSection('mnumi', '> generate ' . $className . ' class.');
                new $className();
            }
        }

        // loads sample data
        $this->logSection('mnumi', 'Done.');
    }

    public function getCustomBuilderOptions()
    {
        return array(
            'className' => 'OrderVersion',
            'tableName' => 'order_version',
            'deleteVersions' => false,
            'auditLog' => true,
            'generateFiles' => true,
        );
    }

    /**
     * Clears cache.
     */
    protected function clearCache()
    {
        foreach(array('prod', 'dev') as $env)
        {
            $cacheDir = sfConfig::get('sf_cache_dir').'/'.sfConfig::get('sf_app').'/'.$env.'/';
            $cache    = new sfFileCache(array('cache_dir' => $cacheDir));
            $cache->clean();
        }
    }
    

}