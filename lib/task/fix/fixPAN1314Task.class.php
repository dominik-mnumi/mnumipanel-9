<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 * @author rafal
 */
class fixPAN1314Task extends sfDoctrineBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
        new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
        new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
        new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
        // add your own options here
        ));

        $this->namespace        = 'fix';
        $this->name             = 'PAN-1314';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [fix:PAN-1314|INFO] Fix permissions for hotfolder directory
Call it with:

[php symfony fix:PAN-1314 |INFO]
EOF;
    }
    
    protected function execute($arguments = array(), $options = array())
    {
        $configDir = '/etc/mnumi/mnumicore3';
        $hotfolderDir = '/usr/share/mnumi/mnumicore3/data/hotfolder';
        $siteId = $this->getSiteId();

        //dev environment
        if (is_array($siteId) && (count($siteId) == 0)) {
            $this->logBlock("Ignoring task execution because of DEV environment.", 'INFO');
            return;
        }
        else if ($siteId !== false) {
            $configDir = '/etc/mnumi/mnumicore3-mu/' . $siteId;
            $hotfolderDir = '/usr/share/mnumi/mnumicore3-mu/'. $siteId . '/data/hotfolder';
        }

        if (!file_exists($configDir . '/PAN-1314')) {
            if (file_exists($hotfolderDir)) {
                $this->logBlock('Fixing hotfolder directory permissions. This method will be run once.', 'INFO');

                exec('chown -R www-data:www-data ' . $hotfolderDir . '/*');
                exec('touch ' . $configDir . '/PAN-1314');
            }
            else {
                $this->logBlock(sprintf('Hotfolder directory (%s) does not exists', $hotfolder), 'NOTICE');
            }
        }
    }

    /**
     * @return false if application is not in multisite installation. 
     *      In other case integer meaning which site installation it is.
     */
    private function getSiteId() {
        if(strpos(__FILE__, '/usr/share/mnumi/mnumicore3/') !== false)
        {
            return false;
        }

        // multisite debian package installation
        preg_match('/^\/usr\/share\/mnumi\/mnumicore3-mu\/(\d+)\//', __FILE__, $site);
        return $site;
    }
}

?>
