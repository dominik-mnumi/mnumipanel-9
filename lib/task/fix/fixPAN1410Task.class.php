<?php

class fixPAN1410Task extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
        ));
        
        $this->namespace        = 'fix';
        $this->name             = 'PAN-1410';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [fix:PAN-1410|INFO] Migrates costs from FieldItem to FieldItemPrice
Call it with:

[php symfony fix:PAN-1410 |INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        $configDir = '/etc/mnumi/mnumicore3';
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->log('Starting FieldItem::cost migration');
        try {

            $connection
                ->prepare("UPDATE `field_item_price` fip INNER JOIN `field_item` fi ON fi.id = fip.field_item_id SET fip.cost = fi.cost;")
                ->execute();
            exec('touch ' . $configDir . '/PAN-1410');
        }
        catch(Exception $e)
        {
            $this->logSection('No action', 'There was a problem when trying to migrate costs from FieldItem to FieldItemPrice');
        }

        $this->log('Migration finished.');
    }

}
