<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once('plugins/sfDoctrinePlugin/lib/task/sfDoctrineBaseTask.class.php');
require_once('apps/mnumicore/lib/priceTool.php');

class migrateCore2Task extends sfDoctrineBaseTask
{
    protected $mysqli;

    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('no-confirmation', null, sfCommandOption::PARAMETER_NONE, 'Whether to force dropping of the database')
        ));
        
        // add your own arguments here
        $this->addArguments(array(
            new sfCommandArgument('dbhost', sfCommandArgument::REQUIRED, 'Database host'),          
            new sfCommandArgument('dbusername', sfCommandArgument::REQUIRED, 'Database username'),
            new sfCommandArgument('dbpassword', sfCommandArgument::REQUIRED, 'Database password'),
            new sfCommandArgument('dbname', sfCommandArgument::REQUIRED, 'Database name')
        ));

        $this->namespace           = 'mnumi';
        $this->name                = 'migrateCore2';
        $this->briefDescription    = '';
        $this->detailedDescription = <<<EOF
The [migrateCore2|INFO] migrates data from core2 to core3.
Call it with:

  [php symfony migrateCore2|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // creates instance and i18N object (easier data preparation)
        $contextObj = sfContext::createInstance($this->configuration);
        $contextObj->getUser()->setCulture('pl');
        
        $this->i18NObj = $contextObj->getI18N();

        // loads sample data
        $this->logSection('mnumi', 'preparing tables...');
        
        // prepares array with options and unsets some unnecessary 
        $withoutSomeOptionArr = $options;

        // drops and creates database for specific environment
        if($this->runTask('doctrine:drop-db', array(), $withoutSomeOptionArr))
        {
            $this->logSection('mnumi', 'task aborted');
            return 1;
        }

        unset($withoutSomeOptionArr['no-confirmation']);
        $this->runTask('doctrine:create-db', array(), $withoutSomeOptionArr);
        $this->runTask('doctrine:build-sql', array(), $withoutSomeOptionArr);
        $this->runTask('doctrine:insert-sql', array(), $withoutSomeOptionArr);
        
        // clears cache
        $this->logSection('mnumi', 'cache cleaning...');
        $this->clearCache();
        
        // MIGRATE SECTION
        $this->logSection('migrate', 'Migrating from database: "'.$arguments['dbname'].'" using username: "'.
                $arguments['dbusername'].'" and password: "'.$arguments['dbpassword'].'"');

        $this->mysqli = mysqli_connect($arguments['dbhost'] ? $arguments['dbhost'] : 'localhost', $arguments['dbusername'], $arguments['dbpassword']);
        mysqli_select_db($this->mysqli, $arguments['dbname']) or die();
        if(mysqli_connect_errno())
        {
            $this->logSection('migrate', 'Error during connect', null, 'ERROR');
            return;
        }
        $this->mysqli->query("SET NAMES 'utf8'");

        
        // =====================================  migration version  ========================================
        $this->logSection('migrate', 'setting migration version...');
        
        // MigrationVersion
        $fileMigration = new Doctrine_Migration(sfConfig::get('sf_root_dir').'/lib/migration/doctrine/');
        $obj = new MigrationVersion();
        $obj->setVersion($fileMigration->getLatestVersion());
        $obj->save();
        
        
        // =====================================  printsize  ========================================
        $this->logSection('migrate', 'migrating printsizes...');
        
        
        $query = "select * from printer";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $printer = new Printsize();
            $printer->setId($row['id']);
            $printer->setWidth($row['printsize_width']);
            $printer->setHeight($row['printsize_height']);
            $printer->save();
        }

        // =====================================  pricelist  ========================================
        $this->logSection('migrate', 'migrating pricelists...');

        $query = "select * from pricelist";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $pricelist = new Pricelist();
            $pricelist->setId($row['id']);
            $pricelist->setName($row['name']);
            $pricelist->setChangeable($row['changable']);
            $pricelist->save();
        }

        // =====================================  payment  ========================================
        $this->logSection('migrate', 'migrating payments...');

        $query = "select * from payment";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $name = $row['name'];
            
            if($name == 'przelew')
            {
                $name = PaymentTable::$transfer21Days;
            }
            elseif($name == 'karta (płatność kartą - terminal)')
            {
                $name = PaymentTable::$cardPayment;
            }
            elseif($name == 'płatność gotówką')
            {
                $name = PaymentTable::$cashPayment;
            }
            elseif($name == 'barter')
            {
                $name = PaymentTable::$barter;
            }
            elseif($name == 'kompensata')
            {
                $name = PaymentTable::$compensation;
            }
            elseif($name == 'pobranie')
            {
                $name = PaymentTable::$cashOnDelivery;
            }
            elseif($name == 'platnosc.pl (Płatności on-line)')
            {
                $name = PaymentTable::$payUPayment;
            }
            elseif($name == 'przedpłata na konto')
            {
                $name = PaymentTable::$paymentOnAccount;
            }
            elseif($name == 'gratis')
            {
                $name = PaymentTable::$gratis;
            }
            elseif($name == 'płatność punktami')
            {
                $name = PaymentTable::$pointsPayment;
            }           
            
            $payment = new Payment();
            $payment->setId($row['id']);
            $payment->setName($name);
            $payment->setLabel($row['label']);
            $payment->setDescription($row['description']);
            $payment->setActive($row['active']);
            $payment->setForOffice($row['for_office']);
            $payment->setApplyOnAllPricelist($row['apply_on_all_pricelist']);
            $payment->setChangeable(1);
            $payment->setApplyOnAllCarriers($row['apply_on_all_carriers']);
            $payment->setRequiredCreditLimit($row['required_credit_limit']);
            $payment->setCost($row['cost']);
            $payment->setFreePaymentAmount($row['free_payment_amount']);
            $payment->save();
        }
        
        $query = "select * from payment_pricelist";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $paymentStatus = new PaymentPricelist();
            $paymentStatus->setId($row['id']);
            $paymentStatus->setPaymentId($row['payment_id']);
            $paymentStatus->setPricelistId($row['pricelist_id']);
            $paymentStatus->save();
        }


        // =====================================  carrier  ========================================
        $query = "select * from carrier";

        $result = $this->mysqli->query($query);

        $this->logSection('migrate', 'migrating carriers...');

        $mappingV2ToV3CarrierNameArr = array(
            'Poczta Polska - Priorytet' => CarrierTable::$pocztaPolska,
            'Odbiorę osobiscie' => CarrierTable::$collection,
            'Wysyłka do odbiorcy' => CarrierTable::$shippingToCustomers,
            'Kurier cała Polska' => CarrierTable::$courier
        );
        
        while($row = $result->fetch_array())
        {
            $carrierName = array_key_exists($row['name'], $mappingV2ToV3CarrierNameArr) 
                    ? $mappingV2ToV3CarrierNameArr[$row['name']] 
                    : $row['name'];
            
            $carrier = new Carrier();
            $carrier->setId($row['id']);
            $carrier->setName($carrierName);
            $carrier->setLabel($row['name']);
            $carrier->setDeliveryTime($row['delivery_time']);
            $carrier->setApplyOnAllPricelist($row['apply_on_all_pricelist']);
            $carrier->setTax($row['tax']);
            $carrier->setRestrictForCities($row['restrict_for_cities']);
            $carrier->setRequireShipmentData($row['require_shipment_data']);
            $carrier->setCost($row['cost']);
            $carrier->setPrice($row['price']);
            $carrier->setFreeShippingAmount($row['free_shipping_amount']);
            $carrier->setActive(1);
            $carrier->save();
        }

        $query = "select * from carrier_payment";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $carrierPamyent = new CarrierPayment();
            $carrierPamyent->setId($row['id']);
            $carrierPamyent->setCarrierId($row['carrier_id']);
            $carrierPamyent->setPaymentId($row['payment_id']);
            $carrierPamyent->save();
        }

        $query = "select * from carrier_pricelist";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $carrierPricelist = new CarrierPricelist();
            $carrierPricelist->setId($row['id']);
            $carrierPricelist->setCarrierId($row['carrier_id']);
            $carrierPricelist->setPricelistId($row['pricelist_id']);
            $carrierPricelist->save();
        }

        // =====================================  category  ========================================

        $this->logSection('migrate', 'migrating categories...');

        $query = "select * from category";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $category = new Category();
            $category->setId($row['id']);
            $category->setName($row['name']);
            $category->setSortOrder($row['sort_order']);
            $category->setActive($row['active']);
            $category->setPhoto($row['photo']);
            $category->setCreatedAt($row['created_at']);
            $category->setUpdatedAt($row['updated_at']);
            $category->setSlug($row['slug']);
            $category->setLft($row['lft']);
            $category->setRgt($row['rgt']);
            $category->setLevel($row['level']);
            $category->save();
        }


        $mainCat = CategoryTable::getInstance()->findOneByName('All');
        if($mainCat)
        {
            $this->logSection('migrate', 'renaming main category from All to Main');
            $mainCat->setName('Main');
            $mainCat->setSlug('main');
            $mainCat->save();
        }



        // =====================================  settings  ========================================

        $this->logSection('migrate', 'migrating settings...');

        $query = "select * from measure_unit";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $measureUnit = new MeasureUnit();
            $measureUnit->setId($row['id']);
            $measureUnit->setName($row['name']);
            $measureUnit->save();
        }

        $query = "select * from fieldset";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $fieldset = new Fieldset();
            $fieldset->setId($row['id']);
            $fieldset->setName($row['name']);
            $fieldset->setLabel($row['label']);
            $fieldset->setChangeable($row['changable']);
            $fieldset->setHidden($row['hidden']);
            $fieldset->setRootId($row['root_id']);
            $fieldset->setLft($row['lft']);
            $fieldset->setRgt($row['rgt']);
            $fieldset->setLevel($row['level']);
            $fieldset->save();
        }

        $query = "select * from field_item";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $fieldItem = new FieldItem();
            $fieldItem->setId($row['id']);
            $fieldItem->setName($row['name']);
            $fieldItem->setCost($row['cost']);
            $fieldItem->setMeasureUnitId($row['measure_unit_id']);
            $fieldItem->setHidden($row['hidden']);
            $fieldItem->setFieldId($row['field_id']);
            $fieldItem->setLabel($row['label']);
            $fieldItem->save();
        }

        $query = "select * from field_item_price";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $fieldItemPrice = new FieldItemPrice();
            $fieldItemPrice->setId($row['id']);
            $fieldItemPrice->setFieldItemId($row['field_item_id']);
            $fieldItemPrice->setPricelistId($row['pricelist_id']);
            $fieldItemPrice->setMinimalPrice($row['minimal_price']);
            $fieldItemPrice->setMaximalPrice($row['maximal_price']);
            $fieldItemPrice->save();
        }


        $query = "select * from field_item_price_type";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $fieldItemPriceType = new FieldItemPriceType();
            $fieldItemPriceType->setName($row['name']);
            $fieldItemPriceType->save();
        }


        $query = "select * from field_item_price_quantity";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $fieldItemPriceQuantity = new FieldItemPriceQuantity();
            $fieldItemPriceQuantity->setId($row['id']);
            $fieldItemPriceQuantity->setFieldItemPriceId($row['field_item_price_id']);
            $fieldItemPriceQuantity->setFieldItemPriceTypeName($row['field_item_price_type_name']);
            $fieldItemPriceQuantity->setQuantity($row['quantity']);
            $fieldItemPriceQuantity->setPrice($row['price']);
            $fieldItemPriceQuantity->save();
        }


        $query = "select * from field_item_price_range";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $fieldItemPriceRange = new FieldItemPriceRange();
            $fieldItemPriceRange->setId($row['id']);
            $fieldItemPriceRange->setFieldItemPriceId($row['field_item_price_id']);
            $fieldItemPriceRange->setQuantity($row['quantity']);
            $fieldItemPriceRange->setPriceSimplex($row['price_simplex']);
            $fieldItemPriceRange->setPriceDuplex($row['price_duplex']);
            $fieldItemPriceRange->setPriceItem($row['price_item']);
            $fieldItemPriceRange->setPricePage($row['price_page']);
            $fieldItemPriceRange->setPriceCopy($row['price_copy']);
            $fieldItemPriceRange->setPriceLinearMetre($row['price_linear_metre']);
            $fieldItemPriceRange->setPriceSquareMetre($row['price_square_metre']);
            $fieldItemPriceRange->save();
        }

        // gets all field_item_material 
        $result = $this->getFieldItemMaterial();
        while($row = $result->fetch_array())
        {
            $fieldItemMaterial = new FieldItemMaterial();
            $fieldItemMaterial->setFieldItemId($row['id']);
            $fieldItemMaterial->setPrintsize($printer);
            $fieldItemMaterial->save();
        }
        
        // gets all field_item_material 
        $result = $this->getFieldItemSize();
        while($row = $result->fetch_array())
        {
            $fieldItemSize = new FieldItemSize();
            $fieldItemSize->setFieldItemId($row['field_item_id']);
            $fieldItemSize->setWidth($row['width']);
            $fieldItemSize->setHeight($row['height']);
            $fieldItemSize->save();
        }

        // =====================================  payment statuses  ========================================
        $this->logSection('migrate', 'adding payment statuses...');
        // PaymentStatus
        // paid
        $obj = new PaymentStatus();
        $obj->setName(PaymentStatusTable::$paid);
        $obj->save();
        $this->paymentStatus[0] = $obj;
        
        // waiting
        $obj = new PaymentStatus();
        $obj->setName(PaymentStatusTable::$waiting);
        $obj->save();
        $this->paymentStatus[10] = $obj;
        
        // =====================================  carrier report type  ========================================
        $this->logSection('migrate', 'adding carrier report types...');
        // CarrierReportType
        // paid
        $obj = new CarrierReportType();
        $obj->setName(CarrierReportTypeTable::$plPocztaPolska);
        $obj->setDescription('Poczta Polska');
        $obj->save();
        $this->carrierReportType[0] = $obj;
        
        
        // =====================================  workflow  ========================================
        $this->logSection('migrate', 'adding workflow...');
        
        // Workflow
        // default order workflow
        $obj = new Workflow();
        $obj->setName(WorkflowTable::$defaultOrderWorkflowName);
        $obj->setType(WorkflowTable::$orderType);
        $obj->save();
        $this->workflow[0] = $obj;
        
        // default package workflow
        $obj = new Workflow();
        $obj->setName(WorkflowTable::$defaultOrderPackageWorkflowName);
        $obj->setType(WorkflowTable::$orderPackageType);
        $obj->save();
        $this->workflow[10] = $obj;
        
        // simple order workflow
        $obj = new Workflow();
        $obj->setName(WorkflowTable::$simpleOrderWorkflowName);
        $obj->setType(WorkflowTable::$orderType);
        $obj->save();
        $this->workflow[20] = $obj;
        
        
        // =====================================  order statuses  ========================================
        $this->logSection('migrate', 'adding order statuses...');

        // OrderStatus
        // calculation
        $obj = new OrderStatus();
        $obj->setName(OrderStatusTable::$calculation);
        $obj->setTitle($this->i18NObj->__('Calculation'));
        $obj->setIcon('calculation_32.png');
        $obj->save();
        $this->orderStatus[0] = $obj;
        
        // draft
        $obj = new OrderStatus();
        $obj->setName(OrderStatusTable::$draft);
        $obj->setTitle($this->i18NObj->__('Draft'));
        $obj->setIcon('draft_32.png');
        $obj->save();
        $this->orderStatus[10] = $obj;
        
        // deleted
        $obj = new OrderStatus();
        $obj->setName(OrderStatusTable::$deleted);
        $obj->setTitle($this->i18NObj->__('Deleted'));
        $obj->setIcon('deleted_32.png');
        $obj->save();
        $this->orderStatus[20] = $obj;
        
        // new
        $obj = new OrderStatus();
        $obj->setName(OrderStatusTable::$new);
        $obj->setTitle($this->i18NObj->__('New order'));
        $obj->setIcon('new_32.png');
        $obj->save();
        $this->orderStatus[30] = $obj;
        
        // realization
        $obj = new OrderStatus();
        $obj->setName(OrderStatusTable::$realization);
        $obj->setTitle($this->i18NObj->__('Being processed'));
        $obj->setIcon('inproces_32.png');
        $obj->save();
        $this->orderStatus[40] = $obj;
        
        // bindery
        $obj = new OrderStatus();
        $obj->setName(OrderStatusTable::$bindery);
        $obj->setTitle($this->i18NObj->__('Bindery'));
        $obj->setIcon('bindery_32.png');
        $obj->save();
        $this->orderStatus[50] = $obj;
        
        // ready
        $obj = new OrderStatus();
        $obj->setName(OrderStatusTable::$ready);
        $obj->setTitle($this->i18NObj->__('Ready'));
        $obj->setIcon('ready_32.png');
        $obj->save();
        $this->orderStatus[60] = $obj;
        
        
        // =====================================  order workflow  ========================================
        $this->logSection('migrate', 'adding order workflow...');
        
        // OrderWorkflow

        // default order workflow
        // restore (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[20]); // deleted
        $obj->setNextStatus($this->orderStatus[30]); // new
        $obj->setTitle($this->i18NObj->__('Restore'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[0] = $obj;
      
        // to realization (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[30]); // new
        $obj->setNextStatus($this->orderStatus[40]); // realization
        $obj->setTitle($this->i18NObj->__('To realization'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[10] = $obj;
         
        // delete (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[30]); // new
        $obj->setNextStatus($this->orderStatus[20]); // deleted
        $obj->setTitle($this->i18NObj->__('Delete'));
        $obj->setBackward(1);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[20] = $obj;
        
        // back to new order (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[40]); // realization
        $obj->setNextStatus($this->orderStatus[30]); // new
        $obj->setTitle($this->i18NObj->__('Back to new order'));
        $obj->setBackward(1);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[30] = $obj;
        
        // to bindery (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[40]); // realization
        $obj->setNextStatus($this->orderStatus[50]); // bindery
        $obj->setTitle($this->i18NObj->__('To bindery'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[40] = $obj;
       
        // ready (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[50]); // bindery
        $obj->setNextStatus($this->orderStatus[60]); // ready
        $obj->setTitle($this->i18NObj->__('Ready'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[50] = $obj;
         
        // back to print (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[50]); // bindery
        $obj->setNextStatus($this->orderStatus[40]); // realization
        $obj->setTitle($this->i18NObj->__('Back to print'));
        $obj->setBackward(1);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[60] = $obj;
       
        // back to bindery (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[60]);
        $obj->setNextStatus($this->orderStatus[50]);
        $obj->setTitle($this->i18NObj->__('Back to bindery'));
        $obj->setBackward(1);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[70] = $obj;
         
        // create new order (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[10]); // draft
        $obj->setNextStatus($this->orderStatus[30]); // new
        $obj->setTitle($this->i18NObj->__('Create new order'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[80] = $obj;
  
        // create new order - from calculation (default order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[0]); // calculation
        $obj->setNextStatus($this->orderStatus[30]); // new
        $obj->setTitle($this->i18NObj->__('Create new order'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[0]);
        $obj->save();
        $this->orderWorkflow[90] = $obj;
     
         
        // simple order workflow
        // restore (simple order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[20]); // deleted
        $obj->setNextStatus($this->orderStatus[30]); // new
        $obj->setTitle($this->i18NObj->__('Restore'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[20]);
        $obj->save();
        $this->orderWorkflow[100] = $obj;
   
        // delete (simple order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[30]); // new
        $obj->setNextStatus($this->orderStatus[20]); // deleted
        $obj->setTitle($this->i18NObj->__('Delete'));
        $obj->setBackward(1);
        $obj->setWorkflow($this->workflow[20]);
        $obj->save();
        $this->orderWorkflow[110] = $obj;

        // ready (simple order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[30]); // new
        $obj->setNextStatus($this->orderStatus[60]); // ready
        $obj->setTitle($this->i18NObj->__('Ready'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[20]);
        $obj->save();
        $this->orderWorkflow[120] = $obj;
        
        // reopen order (simple order workflow)
        $obj = new OrderWorkflow();
        $obj->setCurrentStatus($this->orderStatus[60]); // ready
        $obj->setNextStatus($this->orderStatus[30]); // new
        $obj->setTitle($this->i18NObj->__('Reopen order'));
        $obj->setBackward(1);
        $obj->setWorkflow($this->workflow[20]);
        $obj->save();
        $this->orderWorkflow[130] = $obj;

        
        // =====================================  order package statuses  ========================================
        $this->logSection('migrate', 'adding order package statuses...');
        
        // OrderPackageStatus
        // waiting
        $obj = new OrderPackageStatus();
        $obj->setName(OrderPackageStatusTable::$waiting);
        $obj->setTitle($this->i18NObj->__('in progress'));
        $obj->save();
        $this->orderPackageStatus[0] = $obj;
        
        // completed
        $obj = new OrderPackageStatus();
        $obj->setName(OrderPackageStatusTable::$completed);
        $obj->setTitle($this->i18NObj->__('ready'));
        $obj->setIcon('status-dofakt.gif');
        $obj->save();
        $this->orderPackageStatus[10] = $obj;
        
        // basket
        $obj = new OrderPackageStatus();
        $obj->setName(OrderPackageStatusTable::$basket);
        $obj->setTitle($this->i18NObj->__('basket'));
        $obj->save();
        $this->orderPackageStatus[20] = $obj;
        
        // archive
        $obj = new OrderPackageStatus();
        $obj->setName(OrderPackageStatusTable::$archive);
        $obj->setTitle($this->i18NObj->__('archive'));
        $obj->save();
        $this->orderPackageStatus[30] = $obj;
        
        
        // =====================================  order package workflow  ========================================
        $this->logSection('migrate', 'adding order package workflow...');
        
        // OrderPackageWorkflow

        // default_package_workflow
        // to waiting room (default order workflow)
        $obj = new OrderPackageWorkflow();
        $obj->setCurrentStatus($this->orderPackageStatus[20]); // basket
        $obj->setNextStatus($this->orderPackageStatus[0]); // waiting
        $obj->setTitle($this->i18NObj->__('To waiting room'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[10]);
        $obj->save();
        $this->orderPackageWorkflow[0] = $obj;
        
        // complete (default order workflow)
        $obj = new OrderPackageWorkflow();
        $obj->setCurrentStatus($this->orderPackageStatus[20]); // waiting
        $obj->setNextStatus($this->orderPackageStatus[0]); // completed
        $obj->setTitle($this->i18NObj->__('Completed'));
        $obj->setBackward(0);
        $obj->setWorkflow($this->workflow[10]);
        $obj->save();
        $this->orderPackageWorkflow[10] = $obj;
        
        
        // =====================================  invoice statuses  ========================================
        $this->logSection('migrate', 'adding invoice statuses...');
        
         // InvoiceStatus
        // unpaid
        $obj = new InvoiceStatus();
        $obj->setName(InvoiceStatusTable::$unpaid);
        $obj->setTitle($this->i18NObj->__('Unpay invoice'));
        $obj->save();
        $this->invoiceStatus[0] = $obj;
        
        // paid
        $obj = new InvoiceStatus();
        $obj->setName(InvoiceStatusTable::$paid);
        $obj->setTitle($this->i18NObj->__('Pay invoice'));
        $obj->save();
        $this->invoiceStatus[10] = $obj;
        
        // canceled
        $obj = new InvoiceStatus();
        $obj->setName(InvoiceStatusTable::$canceled);
        $obj->setTitle($this->i18NObj->__('Cancel invoice'));
        $obj->save();
        $this->invoiceStatus[20] = $obj;
        
        // paid from receipt
        $obj = new InvoiceStatus();
        $obj->setName(InvoiceStatusTable::$paidFromReceipt);
        $obj->setTitle($this->i18NObj->__('Paid from receipt'));
        $obj->save();
        $this->invoiceStatus[30] = $obj;
        
        // =====================================  invoice types  ========================================
        $this->logSection('migrate', 'adding invoice types...');
        
        // InvoiceType
        
        // invoice
        $obj = new InvoiceType();
        $obj->setName(InvoiceTypeTable::$INVOICE);
        $obj->save();
        $this->invoiceType[0] = $obj;
        
        // preliminary invoice
        $obj = new InvoiceType();
        $obj->setName(InvoiceTypeTable::$PRELIMINARY_INVOICE);
        $obj->save();
        $this->invoiceType[10] = $obj;
        
        // correction invoice
        $obj = new InvoiceType();
        $obj->setName(InvoiceTypeTable::$CORRECTION_INVOICE);
        $obj->save();
        $this->invoiceType[20] = $obj;
        
        // receipt
        $obj = new InvoiceType();
        $obj->setName(InvoiceTypeTable::$RECEIPT);
        $obj->save();
        $this->invoiceType[30] = $obj;
        
        
        // =====================================  tax options  ========================================
        $this->logSection('migrate', 'adding tax options...');
        
        // Tax
        
        // 0
        $obj = new Tax();
        $obj->setValue(TaxTable::$t0);
        $obj->save();
        $this->tax[0] = $obj;
        
        // 7
        $obj = new Tax();
        $obj->setValue(TaxTable::$t7);
        $obj->save();
        $this->tax[10] = $obj;
        
        // 19
        $obj = new Tax();
        $obj->setValue(TaxTable::$t19);
        $obj->save();
        $this->tax[20] = $obj;
        
        // 23
        $obj = new Tax();
        $obj->setValue(TaxTable::$t23);
        $obj->save();
        $this->tax[30] = $obj;
        
        
        // =====================================  product  ========================================

        $this->logSection('migrate', 'migrating products...');

        $query = "select * from product";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $product = new Product();
            $product->setId($row['id']);
            $product->setName($row['name']);
            $product->setDescription($row['description']);
            $product->setPhoto($row['photo']);
            $product->setCategoryId($row['category_id']);
            $product->setActive($row['active']);
            $product->setExternalLink($row['external_link']);
            $product->setUploaderAvailable(1);
            $product->setCreatedAt($row['created_at']);
            $product->setUpdatedAt($row['updated_at']);
            $product->setSlug($row['slug']);
            $product->setWorkflow($this->workflow[0]);
            $product->save();
        }

        $query = "select * from product_field";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $productField = new ProductField();
            $productField->setId($row['id']);
            $productField->setFieldsetId($row['fieldset_id']);
            $productField->setProductId($row['product_id']);
            $productField->setLabel($row['label']);
            $productField->setVisible($row['visible']);
            $productField->setDefaultValue($row['default_value']);
            $productField->setRequired($row['required']);
            $productField->save();
        }

        $query = "select * from product_field_item";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $productFieldItem = new ProductFieldItem();
            $productFieldItem->setId($row['id']);
            $productFieldItem->setProductFieldId($row['product_field_id']);
            $productFieldItem->setFieldItemId($row['field_item_id']);
            $productFieldItem->setLabel($row['label']);
            $productFieldItem->save();
        }

        $query = "select * from product_fixprice";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $productFixprice = new ProductFixprice();
            $productFixprice->setId($row['id']);
            $productFixprice->setProductId($row['product_id']);
            $productFixprice->setPricelistId($row['pricelist_id']);
            $productFixprice->setQuantity($row['quantity']);
            $productFixprice->setPrice($row['price']);
            $productFixprice->setCost($row['cost']);
            $productFixprice->save();
        }

        $query = "select * from product_wizard";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $productWizard = new ProductWizard();
            $productWizard->setId($row['id']);
            $productWizard->setProductId($row['product_id']);
            $productWizard->setWizardName($row['wizard_name']);
            $productWizard->setWidth($row['width']);
            $productWizard->setHeight($row['height']);
            $productWizard->setSortOrder($row['sort_order']);
            $productWizard->setActive(1);
            $productWizard->save();
        }
 
        // =====================================  shop  ========================================

        $this->logSection('migrate', 'migrating shop...');

        $query = "select * from shop";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_array())
        {
            $shop = new Shop();
            $shop->setName($row['name']);
            $shop->setDescription($row['description']);
            $shop->setValidDate($row['valid_date']);
            $shop->setHost($row['host']);
            $shop->setCreatedAt($row['created_at']);
            $shop->setUpdatedAt($row['updated_at']);
            $shop->save();
        }

        // =====================================  notifications  ========================================
        
        $this->logSection('migrate', 'adding notifications...');
       
        // email
        $obj = new NotificationType();
        $obj->setName(NotificationTypeTable::$email);
        $obj->save();
        $this->notificationType[0] = $obj;
        
        // sms
        $obj = new NotificationType();
        $obj->setName(NotificationTypeTable::$sms);
        $obj->save();
        $this->notificationType[10] = $obj;
        
        
        // NotificationTemplate
        // calculation
        $obj = new NotificationTemplate();
        $obj->setName(NotificationTemplateTable::$calculation);
        $obj->save();
        $this->notificationTemplate[0] = $obj;
        
        // calculation without logging
        $obj = new NotificationTemplate();
        $obj->setName(NotificationTemplateTable::$calculationWithoutLogging);
        $obj->save();
        $this->notificationTemplate[10] = $obj;
        
        // register
        $obj = new NotificationTemplate();
        $obj->setName(NotificationTemplateTable::$register);
        $obj->save();
        $this->notificationTemplate[20] = $obj;
        
        // buy
        $obj = new NotificationTemplate();
        $obj->setName(NotificationTemplateTable::$buy);
        $obj->save();
        $this->notificationTemplate[30] = $obj;
        
        // package ready
        $obj = new NotificationTemplate();
        $obj->setName(NotificationTemplateTable::$packageReady);
        $obj->save();
        $this->notificationTemplate[50] = $obj;
        
        // new order
        $obj = new NotificationTemplate();
        $obj->setName(NotificationTemplateTable::$orderNew);
        $obj->save();
        $this->notificationTemplate[60] = $obj;
        
        // lost password
        $obj = new NotificationTemplate();
        $obj->setName(NotificationTemplateTable::$lostPassword);
        $obj->save();
        $this->notificationTemplate[70] = $obj;
        
        // client invitation
        $obj = new NotificationTemplate();
        $obj->setName(NotificationTemplateTable::$clientInvitation);
        $obj->save();
        $this->notificationTemplate[80] = $obj;

        
        // Notification
        // email, calculation
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[0]); // email
        $obj->setNotificationTemplate($this->notificationTemplate[0]); // calculation
        $obj->setTitle($this->i18NObj->__('Calculation'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans "We present calculation for the product" %} "{{productName}}" {% trans "that you have requested"%} :<br />{{ calculation }}<br /><br />{% trans "You can place your order on our website" %}:<br />{{ shopHost }}{{ shopUrlOrderPreview }}<br /><br />{% trans "Calculation date" %}: {{ date }}<br /><br />{% trans "Best regards" %},<br />{{ userName }}');
        $obj->save();
        $this->notification[0] = $obj;
        
        // email, calculation without logging
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[0]); // email
        $obj->setNotificationTemplate($this->notificationTemplate[10]); // calculation without logging
        $obj->setTitle($this->i18NObj->__('Calculation without logging'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans "We present calculation for the product" %} "{{productName}}" {% trans "that you have requested"%} :<br />{{ calculation }}<br /><br />{% trans "Calculation date" %}: {{ date }}<br /><br />{% trans "Best regards" %}.');
        $obj->save();
        $this->notification[10] = $obj;
        
        // email, package created
//        $obj = new Notification();
//        $obj->setNotificationType($this->notificationType[0]); // email
//        $obj->setNotificationTemplate($this->notificationTemplate[40]); // package created
//        $obj->setTitle($this->i18NObj->__('Package created'));
//        $obj->setContent('Package created content');
//        $obj->save();
//        $this->notification[20] = $obj;
        
        // email, package created, sms
//        $obj = new Notification();
//        $obj->setNotificationType($this->notificationType[10]); // sms
//        $obj->setNotificationTemplate($this->notificationTemplate[40]); // package created
//        $obj->setTitle($this->i18NObj->__('Package created'));
//        $obj->setContent('Package created content');
//        $obj->save();
//        $this->notification[30] = $obj;
        
        // email, package ready
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[0]); // email
        $obj->setNotificationTemplate($this->notificationTemplate[50]); // package ready
        $obj->setTitle($this->i18NObj->__('Package ready'));
        $obj->setContent('Package ready content');
        $obj->save();
        $this->notification[40] = $obj;
        
        // email, package ready, sms
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[10]); // sms
        $obj->setNotificationTemplate($this->notificationTemplate[50]); // package ready
        $obj->setTitle($this->i18NObj->__('Package ready'));
        $obj->setContent('Package ready content');
        $obj->save();
        $this->notification[50] = $obj;
        
        // email, register
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[0]); // email
        $obj->setNotificationTemplate($this->notificationTemplate[20]); // register
        $obj->setTitle($this->i18NObj->__('Register'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans "Your account has been created successfully." %}<br /><br />{% trans "Thank you." %}');
        $obj->save();
        $this->notification[60] = $obj;
        
        // email, register, sms
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[10]); // sms
        $obj->setNotificationTemplate($this->notificationTemplate[20]); // register
        $obj->setTitle($this->i18NObj->__('Register'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans "Your account has been created successfully." %}<br /><br />{% trans "Thank you." %}');
        $obj->save();
        $this->notification[70] = $obj;
        
        // email, register
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[0]); // email
        $obj->setNotificationTemplate($this->notificationTemplate[30]); // buy
        $obj->setTitle($this->i18NObj->__('Buy'));
        $obj->setContent('{% trans "Thank you for your order." %}<br /><br />{% trans "In a moment it will be verified by our office." %}<br /><br />{% trans "If any doubts or questions occur, we will contact to explain everything." %}<br /><br />{% trans "This procedure ensures that the print job will be with the highest quality." %}<br /><br />{% trans "Ordered products:" %}{% for order in orderColl %}<br />- {{ order.getName }} - {{ order.getPriceGross }} {{ currencySymbol }}{% endfor %}&nbsp;<br /><br />{% trans "Created at:" %} {{ createdAt }}<br />{% trans "Payment method:" %} {{ paymentType }}<br />{% trans "Delivery type:" %} {{ deliveryType }} ({{ deliveryGrossPrice }})<br />{% trans "Total price:" %}&nbsp;{{ totalGrossPrice }}<br /><br />{% trans %}Email sent from the service {{ companyName }} in order to support realization process.{% endtrans %}');
        $obj->save();
        $this->notification[80] = $obj;
        
        // email, register, sms
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[10]); // sms
        $obj->setNotificationTemplate($this->notificationTemplate[30]); // buy
        $obj->setTitle($this->i18NObj->__('Buy'));
        $obj->setContent('{% trans "Thank you for your order." %}<br /><br />{% trans "In a moment it will be verified by our office." %}<br /><br />{% trans "If any doubts or questions occur, we will contact to explain everything." %}<br /><br />{% trans "This procedure ensures that the print job will be with the highest quality." %}<br /><br />{% trans "Ordered products:" %}{% for order in orderColl %}<br />- {{ order.getName }} - {{ order.getPriceGross }} {{ currencySymbol }}{% endfor %}&nbsp;<br /><br />{% trans "Created at:" %} {{ createdAt }}<br />{% trans "Payment method:" %} {{ paymentType }}<br />{% trans "Delivery type:" %} {{ deliveryType }} ({{ deliveryGrossPrice }})<br />{% trans "Total price:" %}&nbsp;{{ totalGrossPrice }}<br /><br />{% trans %}Email sent from the service {{ companyName }} in order to support realization process.{% endtrans %}');
        $obj->save();
        $this->notification[90] = $obj;
        
        // email, new order
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[0]); // email
        $obj->setNotificationTemplate($this->notificationTemplate[60]); // new order
        $obj->setTitle($this->i18NObj->__('New order'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans %}Your order number: {{ orderId }}, "{{ orderName }}" is accepted for processing.{% endtrans %}<br /><br />{% trans "Thank you." %}');
        $obj->save();
        $this->notification[100] = $obj;
        
        // email, new order, sms
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[10]); // sms
        $obj->setNotificationTemplate($this->notificationTemplate[60]); // new order
        $obj->setTitle($this->i18NObj->__('New order'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans %}Your order number: {{ orderId }}, "{{ orderName }}" is accepted for processing.{% endtrans %}<br /><br />{% trans "Thank you." %}');
        $obj->save();
        $this->notification[110] = $obj;
        
        // email, lost password
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[0]); // email
        $obj->setNotificationTemplate($this->notificationTemplate[70]); // lost password
        $obj->setTitle($this->i18NObj->__('Lost password'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans "You have requested a new password for your account." %}<br /><br />{% trans "You can change your password here:" %}&nbsp;{{ passwordTokenLink }}<br /><br />{% trans "If you did not request a new password, another user may have tried to log in trying to use your password or email by mistake." %}<br /><br />{% trans "For more information, please contact your administrator." %}');
        $obj->save();
        $this->notification[120] = $obj;
        
        // email, lost password, sms
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[10]); // sms
        $obj->setNotificationTemplate($this->notificationTemplate[70]); // lost password
        $obj->setTitle($this->i18NObj->__('Lost password'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans "You have requested a new password for your account." %}<br /><br />{% trans "You can change your password here:" %}&nbsp;{{ passwordTokenLink }}<br /><br />{% trans "If you did not request a new password, another user may have tried to log in trying to use your password or email by mistake." %}<br /><br />{% trans "For more information, please contact your administrator." %}');
        $obj->save();
        $this->notification[130] = $obj;
        
        // email, client invitation
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[0]); // email
        $obj->setNotificationTemplate($this->notificationTemplate[80]); // lost password
        $obj->setTitle($this->i18NObj->__('Client invitation'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans %}You have been asked to {{ companyName }}.{% endtrans %}<br /><br />{% trans "Your account has been created successfully." %}<br /><br />{% trans "Link" %}:&nbsp;{{ shopLoginUrl }}<br /><br />{% trans "Login" %}:&nbsp;{{ login }}<br /><br />{% trans "Password" %}:&nbsp;{{ password }}<br /><br />{% trans "For more information, please contact your administrator." %}');
        $obj->save();
        $this->notification[140] = $obj;
        
        // email, client invitation, sms
        $obj = new Notification();
        $obj->setNotificationType($this->notificationType[10]); // sms
        $obj->setNotificationTemplate($this->notificationTemplate[80]); // lost password
        $obj->setTitle($this->i18NObj->__('Client invitation'));
        $obj->setContent('{% trans "Hello" %},<br /><br />{% trans %}You have been asked to {{ companyName }}.{% endtrans %}<br /><br />{% trans "Your account has been created successfully." %}<br /><br />{% trans "Link" %}:&nbsp;{{ shopLoginUrl }}<br /><br />{% trans "Login" %}:&nbsp;{{ login }}<br /><br />{% trans "Password" %}:&nbsp;{{ password }}<br /><br />{% trans "For more information, please contact your administrator." %}');
        $obj->save();
        $this->notification[150] = $obj;
        
        
        // =====================================  permissions  ========================================

        $this->logSection('migrate', 'adding permissions...');
        
        // admin
        $obj = new sfGuardGroup();
        $obj->setName(sfGuardGroupTable::$admin);
        $obj->setDescription($this->i18NObj->__('Administrator'));
        $obj->save();
        $this->sfGuardGroup[0] = $obj;
        
        // office
        $obj = new sfGuardGroup();
        $obj->setName(sfGuardGroupTable::$office);
        $obj->setDescription($this->i18NObj->__('Office'));
        $obj->save();
        $this->sfGuardGroup[10] = $obj;
        
        // finance
        $obj = new sfGuardGroup();
        $obj->setName(sfGuardGroupTable::$finance);
        $obj->setDescription($this->i18NObj->__('Finance'));
        $obj->save();
        $this->sfGuardGroup[20] = $obj;
        
        // graphic
        $obj = new sfGuardGroup();
        $obj->setName(sfGuardGroupTable::$graphic);
        $obj->setDescription($this->i18NObj->__('Graphic'));
        $obj->save();
        $this->sfGuardGroup[30] = $obj;
 

        // sfGuardPermission
      
        // panelAccess
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$panelAccess);
        $obj->setDescription('Access to MnumiPanel');
        $obj->save();
        $this->sfGuardPermission[0] = $obj;
     
        // userSuperUser
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userSuperUser);
        $obj->setDescription('User: Bypass user access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[10] = $obj;
        
        // userViewList
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userViewList);
        $obj->setDescription('User: View users');
        $obj->save();
        $this->sfGuardPermission[20] = $obj;
        
        // userCreate
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userCreate);
        $obj->setDescription('User: Create user');
        $obj->save();
        $this->sfGuardPermission[30] = $obj;
        
        // userEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userEdit);
        $obj->setDescription('User: Edit user');
        $obj->save();
        $this->sfGuardPermission[40] = $obj;
        
        // userGeneralInformation
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userGeneralInformation);
        $obj->setDescription('User: Edit user general information');
        $obj->save();
        $this->sfGuardPermission[50] = $obj;
        
        // userContact
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userContact);
        $obj->setDescription('User: Edit user contacts');
        $obj->save();
        $this->sfGuardPermission[60] = $obj;
        
        // userNotification
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userNotification);
        $obj->setDescription('User: Edit user notifications');
        $obj->save();
        $this->sfGuardPermission[70] = $obj;
        
        // userPasswordChange
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userPasswordChange);
        $obj->setDescription('User: Password change');
        $obj->save();
        $this->sfGuardPermission[80] = $obj;
        
        // userOfficePermissionChange
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userOfficePermissionChange);
        $obj->setDescription('User: Office permission change');
        $obj->save();
        $this->sfGuardPermission[90] = $obj;
        
        // userLoyaltyPointsAdding
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$userLoyaltyPointsAdding);
        $obj->setDescription('User: Loyalty points adding');
        $obj->save();
        $this->sfGuardPermission[100] = $obj;
       
        // orderSuperUser
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderSuperUser);
        $obj->setDescription('Order: Bypass order access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[110] = $obj;
        
        // orderViewList
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderViewList);
        $obj->setDescription('Order: View orders');
        $obj->save();
        $this->sfGuardPermission[120] = $obj;
        
        // orderCreate
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderCreate);
        $obj->setDescription('Order: Create order');
        $obj->save();
        $this->sfGuardPermission[130] = $obj;
        
        // orderEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderEdit);
        $obj->setDescription('Order: Edit order');
        $obj->save();
        $this->sfGuardPermission[140] = $obj;
        
        // orderBlockPrice
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderBlockPrice);
        $obj->setDescription('Order: Block price');
        $obj->save();
        $this->sfGuardPermission[150] = $obj;
        
        // orderSendCalculation
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderSendCalculation);
        $obj->setDescription('Order: Send calculation');
        $obj->save();
        $this->sfGuardPermission[160] = $obj;
        
        // orderViewChangelog
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderViewChangelog);
        $obj->setDescription('Order: View changelog');
        $obj->save();
        $this->sfGuardPermission[170] = $obj;
        
        // orderTrader
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderTrader);
        $obj->setDescription('Order: Attach trader/graphic to order');
        $obj->save();
        $this->sfGuardPermission[180] = $obj;
        
        // orderFile
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderFile);
        $obj->setDescription('Order: Upload file');
        $obj->save();
        $this->sfGuardPermission[190] = $obj;
        
        // orderDuplicate
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderDuplicate);
        $obj->setDescription('Order: Duplicate order');
        $obj->save();
        $this->sfGuardPermission[200] = $obj;
        
        // orderReminder
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderReminder);
        $obj->setDescription('Order: Create reminder');
        $obj->save();
        $this->sfGuardPermission[210] = $obj;
        
        // orderPricelistChange
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderPricelistChange);
        $obj->setDescription('Order: Pricelist change');
        $obj->save();
        $this->sfGuardPermission[220] = $obj;
        
        // orderStatus
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderStatus);
        $obj->setDescription('Order: Bypass order status access control (Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[230] = $obj;
        
        // orderStatusDraft
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderStatusDraft);
        $obj->setDescription('Order: Change status to "Draft"');
        $obj->save();
        $this->sfGuardPermission[240] = $obj;
        
        // orderStatusNew
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderStatusNew);
        $obj->setDescription('Order: Change status to "New order"');
        $obj->save();
        $this->sfGuardPermission[250] = $obj;
        
        // orderStatusRealization
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderStatusRealization);
        $obj->setDescription('Order: Change status to "Being processed"');
        $obj->save();
        $this->sfGuardPermission[260] = $obj;
        
        // orderStatusBindery
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderStatusBindery);
        $obj->setDescription('Order: Change status to "Bindery"');
        $obj->save();
        $this->sfGuardPermission[270] = $obj;
        
        // orderStatusReady
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderStatusReady);
        $obj->setDescription('Order: Change status to "Ready"');
        $obj->save();
        $this->sfGuardPermission[280] = $obj;
        
        // orderStatusDeleted
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderStatusDeleted);
        $obj->setDescription('Order: Change status to "Deleted"');
        $obj->save();
        $this->sfGuardPermission[290] = $obj;
        
        // orderStatusCalculation
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$orderStatusCalculation);
        $obj->setDescription('Order: Change status to "Calculation"');
        $obj->save();
        $this->sfGuardPermission[300] = $obj;
        
        // invoiceCostSuperUser
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceCostSuperUser);
        $obj->setDescription('Invoice cost: Bypass invoice cost access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[310] = $obj;
        
        // invoiceCostViewList
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceCostViewList);
        $obj->setDescription('Invoice cost: View invoice cost');
        $obj->save();
        $this->sfGuardPermission[320] = $obj;
        
        // invoiceCostCreate
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceCostCreate);
        $obj->setDescription('Invoice cost: Create invoice cost');
        $obj->save();
        $this->sfGuardPermission[330] = $obj;
        
        // invoiceCostEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceCostEdit);
        $obj->setDescription('Invoice cost: Edit invoice cost');
        $obj->save();
        $this->sfGuardPermission[340] = $obj;
        
        // invoiceCostPay
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceCostPay);
        $obj->setDescription('Invoice cost: Pay invoice cost');
        $obj->save();
        $this->sfGuardPermission[350] = $obj;
        
        // clientSuperUser
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientSuperUser);
        $obj->setDescription('Client: Bypass client access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[360] = $obj;
        
        // clientViewList
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientViewList);
        $obj->setDescription('Client: View clients');
        $obj->save();
        $this->sfGuardPermission[370] = $obj;
        
        // clientCreate
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientCreate);
        $obj->setDescription('Client: Create client');
        $obj->save();
        $this->sfGuardPermission[380] = $obj;
        
        // clientEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientEdit);
        $obj->setDescription('Client: Edit client');
        $obj->save();
        $this->sfGuardPermission[390] = $obj;
        
        // clientGeneralInformationEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientGeneralInformationEdit);
        $obj->setDescription('Client: Edit client general information');
        $obj->save();
        $this->sfGuardPermission[400] = $obj;
        
        // clientCreditEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientCreditEdit);
        $obj->setDescription('Client: Edit client pricelist and credit limit');
        $obj->save();
        $this->sfGuardPermission[410] = $obj;
        
        // clientAttachmentEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientAttachmentEdit);
        $obj->setDescription('Client: Edit client attachment');
        $obj->save();
        $this->sfGuardPermission[420] = $obj;
        
        // clientDeliveryEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientDeliveryEdit);
        $obj->setDescription('Client: Edit client delivery');
        $obj->save();
        $this->sfGuardPermission[430] = $obj;
        
        // clientTagEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientTagEdit);
        $obj->setDescription('Client: Edit client tag');
        $obj->save();
        $this->sfGuardPermission[440] = $obj;
        
        // clientTraderEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$clientTraderEdit);
        $obj->setDescription('Client: Edit client trader');
        $obj->save();
        $this->sfGuardPermission[450] = $obj;
        
        // invoiceSuperUser
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceSuperUser);
        $obj->setDescription('Invoice: Bypass invoice access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[460] = $obj;
        
        // invoiceViewList
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceViewList);
        $obj->setDescription('Invoice: View invoices');
        $obj->save();
        $this->sfGuardPermission[470] = $obj;
        
        // invoiceCreate
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceCreate);
        $obj->setDescription('Invoice: Create invoice');
        $obj->save();
        $this->sfGuardPermission[480] = $obj;
        
        // invoiceEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$invoiceEdit);
        $obj->setDescription('Invoice: Edit invoice');
        $obj->save();
        $this->sfGuardPermission[490] = $obj;
        
        // cashDeskSuperUser
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$cashDeskSuperUser);
        $obj->setDescription('Cash desk: Bypass cash desk access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[500] = $obj;
        
        // cashDeskViewList
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$cashDeskViewList);
        $obj->setDescription('Cash desk: View cash desk');
        $obj->save();
        $this->sfGuardPermission[510] = $obj;
        
        // cashDeskBlockEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$cashDeskBlockEdit);
        $obj->setDescription('Cash desk: Edit advance payment');
        $obj->save();
        $this->sfGuardPermission[520] = $obj;
        
        // cashDeskPaymentEdit
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$cashDeskPaymentEdit);
        $obj->setDescription('Cash desk: Edit payment');
        $obj->save();
        $this->sfGuardPermission[530] = $obj;
        
        // cashDeskReportAccess
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$cashDeskReportAccess);
        $obj->setDescription('Cash desk: Access to reports');
        $obj->save();
        $this->sfGuardPermission[540] = $obj;
        
        // cashDeskImportPayment
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$cashDeskImportPayment);
        $obj->setDescription('Cash desk: Import payments');
        $obj->save();
        $this->sfGuardPermission[550] = $obj;
        
        // permissionSuperUser
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$permissionSuperUser);
        $obj->setDescription('Permission: Bypass permission access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[560] = $obj;

        // permissionSystemStatus
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$systemStatus);
        $obj->setDescription('System status');
        $obj->save();
        $this->sfGuardPermission[570] = $obj;

        // permissionLicense
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$license);
        $obj->setDescription('License');
        $obj->save();
        $this->sfGuardPermission[580] = $obj;

        // permissionCoupon
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$coupon);
        $obj->setDescription('Coupons');
        $obj->save();
        $this->sfGuardPermission[590] = $obj;

        // permissionApiKey
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$apiKey);
        $obj->setDescription('API keys');
        $obj->save();
        $this->sfGuardPermission[600] = $obj;

        // permissionSystemConfiguration
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$systemConfiguration);
        $obj->setDescription('System configuration');
        $obj->save();
        $this->sfGuardPermission[610] = $obj;

        // permissionStatsSuperUser
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$statsSuperUser);
        $obj->setDescription('Stats: Bypass stats access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)');
        $obj->save();
        $this->sfGuardPermission[620] = $obj;

        // permissionStatsMnumiQL
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$statsMnumiQL);
        $obj->setDescription('Stats: MnumiQL access');
        $obj->save();
        $this->sfGuardPermission[630] = $obj;

        // permissionStatsEmployee
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$statsEmployee);
        $obj->setDescription('Stats: Employee');
        $obj->save();
        $this->sfGuardPermission[640] = $obj;

        // permissionStatsProduct
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$statsProduct);
        $obj->setDescription('Stats: Product');
        $obj->save();
        $this->sfGuardPermission[650] = $obj;

        // permissionStatsSale
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$statsSale);
        $obj->setDescription('Stats: Sale');
        $obj->save();
        $this->sfGuardPermission[660] = $obj;

        // permissionStatsAccountingReport
        $obj = new sfGuardPermission();
        $obj->setName(sfGuardPermissionTable::$statsAccountingReport);
        $obj->setDescription('Stats: Accounting report');
        $obj->save();
        $this->sfGuardPermission[670] = $obj;
        
        // sets relations sfGuardUser <-> sfGuardPermission

        // prepares group collection for graphic 
        $permissionGraphicColl = new Doctrine_Collection('sfGuardPermission');
        $permissionGraphicColl->add($this->sfGuardPermission[0]); // panelAccess
        $permissionGraphicColl->add($this->sfGuardPermission[120]); // orderViewList
        $permissionGraphicColl->add($this->sfGuardPermission[140]); // orderEdit
        $permissionGraphicColl->add($this->sfGuardPermission[180]); // orderTrader
        $permissionGraphicColl->add($this->sfGuardPermission[190]); // orderFile
        $permissionGraphicColl->add($this->sfGuardPermission[70]); // userNotification

        $this->sfGuardGroup[30]->setPermissions($permissionGraphicColl);
        $this->sfGuardGroup[30]->save();
        
        // prepares group collection for office
        $permissionOfficeColl = new Doctrine_Collection('sfGuardPermission');
        $permissionOfficeColl->add($this->sfGuardPermission[0]); // panelAccess
        $permissionOfficeColl->add($this->sfGuardPermission[120]); // orderViewList
        $permissionOfficeColl->add($this->sfGuardPermission[140]); // orderEdit
        $permissionOfficeColl->add($this->sfGuardPermission[180]); // orderTrader
        $permissionOfficeColl->add($this->sfGuardPermission[190]); // orderFile
        $permissionOfficeColl->add($this->sfGuardPermission[70]); // userNotification
        $permissionOfficeColl->add($this->sfGuardPermission[30]); // userCreate
        $permissionOfficeColl->add($this->sfGuardPermission[40]); // userEdit
        $permissionOfficeColl->add($this->sfGuardPermission[50]); // userGeneralInformation
        $permissionOfficeColl->add($this->sfGuardPermission[60]); // userContact
        $permissionOfficeColl->add($this->sfGuardPermission[80]); // userPasswordChange
        $permissionOfficeColl->add($this->sfGuardPermission[90]); // userOfficePermissionChange
        $permissionOfficeColl->add($this->sfGuardPermission[100]); // userLoyaltyPointsAdding
        $permissionOfficeColl->add($this->sfGuardPermission[360]); // clientSuperUser
        $permissionOfficeColl->add($this->sfGuardPermission[460]); // invoiceSuperUser
        $permissionOfficeColl->add($this->sfGuardPermission[110]); // orderSuperUser
        $permissionOfficeColl->add($this->sfGuardPermission[590]); // coupon
        $permissionOfficeColl->add($this->sfGuardPermission[620]); // statsSuperUser

        $this->sfGuardGroup[10]->setPermissions($permissionOfficeColl);
        $this->sfGuardGroup[10]->save();
        
        // prepares group collection for finance
        $permissionFinanceColl = new Doctrine_Collection('sfGuardPermission');
        $permissionFinanceColl->add($this->sfGuardPermission[0]); // panelAccess
        $permissionFinanceColl->add($this->sfGuardPermission[500]); // cashDeskSuperUser
        $permissionFinanceColl->add($this->sfGuardPermission[460]); // invoiceSuperUser
        $permissionFinanceColl->add($this->sfGuardPermission[310]); // invoiceCostSuperUser

        $this->sfGuardGroup[20]->setPermissions($permissionFinanceColl);
        $this->sfGuardGroup[20]->save();

        // ClientUserPermission
        
        // admin
        $obj = new ClientUserPermission();
        $obj->setName(ClientUserPermissionTable::$admin);
        $obj->setDescription($this->i18NObj->__('Admin permissions'));
        $obj->save();
        $this->clientUserPermission[0] = $obj;
        
        // user
        $obj = new ClientUserPermission();
        $obj->setName(ClientUserPermissionTable::$user);
        $obj->setDescription($this->i18NObj->__('User permissions'));
        $obj->save();
        $this->clientUserPermission[10] = $obj;

        // payU user
        $obj = new sfGuardUser();
        $obj->setId(-1);
        $obj->setEmailAddress('payu@payu.pl');
        $obj->setUsername('payU');
        $obj->setIsSuperAdmin(0);
        $obj->setIsActive(0);
        $obj->save();
    }
    
    protected function getFieldItemMaterial()
    {
        // gets MATERIAL fieldset id
        $query = "SELECT * FROM fieldset WHERE name = 'MATERIAL'";
        $result = $this->mysqli->query($query);
        $row = $result->fetch_array();

        $query = "SELECT * FROM fieldset WHERE root_id={$row['id']}";
        $result = $this->mysqli->query($query);
        
        $materialFieldsetArr = array();
        while($row = $result->fetch_array())
        {
            $materialFieldsetArr[] = $row['id'];
        }
        
        $whereIn = implode(',', $materialFieldsetArr);
        // gets field_items
        $query = "SELECT * FROM field_item fi WHERE fi.field_id IN ({$whereIn})";
        $result = $this->mysqli->query($query);

        return $result;
    }
    
    protected function getFieldItemSize()
    {
        // gets SIZE fieldset id
        $query = "SELECT * FROM fieldset WHERE name = 'SIZE'";
        $result = $this->mysqli->query($query);
        $row = $result->fetch_array();

        $query = "SELECT * FROM fieldset WHERE root_id={$row['id']}";
        $result = $this->mysqli->query($query);
        
        $sizeFieldsetArr = array();
        while($row = $result->fetch_array())
        {
            $sizeFieldsetArr[] = $row['id'];
        }
        
        $whereIn = implode(',', $sizeFieldsetArr);
        
        // gets field_printsize (v2) = field_item_size (v3)
        $query = "SELECT * FROM field_printsize fp INNER JOIN field_item fi 
            WHERE fp.field_item_id = fi.id AND fi.field_id IN ({$whereIn})";
        $result = $this->mysqli->query($query);

        return $result;
    }
    
    /**
     * Clears cache.
     */
    protected function clearCache()
    {
        foreach(array('prod', 'dev') as $env)
        {
            $cacheDir = sfConfig::get('sf_cache_dir') . '/' . sfConfig::get('sf_app') . '/'.$env.'/';
            $cache = new sfFileCache(array('cache_dir' => $cacheDir));
            $cache->clean();
        }
        
    }

}
