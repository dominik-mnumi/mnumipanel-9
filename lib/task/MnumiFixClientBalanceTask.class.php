<?php

class MnumiFixClientBalanceTask extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
        ));
        
        $this->namespace = 'mnumi';
        $this->name = 'fix-client-balance';
        $this->briefDescription = 'Fix client balance.';
        $this->detailedDescription = <<<EOF
The [mnumi:fix-client-balance|INFO] fix client balance field.
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->log('Begin');

        /** @var Client $client */
        foreach(ClientTable::getInstance()->findAll() as $client)
        {
            try {
                $client->calculateBalance();
                $client->save();
            }
            catch(Exception $e) {
                $this->logBlock('Problem with client: ' . $client->getId() . ' - error: ' . $e->getMessage(), 'ERROR');
            }
        }

        $this->log('Finished.');
    }

}
