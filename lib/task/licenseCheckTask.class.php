<?php

/*
 * This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * Description of licenseCheckTask
 *
 * @author jupeter
 */
class licenseCheckTask extends sfDoctrineBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            // add your own options here
        ));

        $this->namespace        = 'license';
        $this->name             = 'check';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [license:check|INFO] check MnumiCore3 license .
Call it with:

  [php symfony license:check|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        $license = priceTool::checkKey(); 
        
        if(!$license)
        {
            $this->logBlock('Your license is incorrect. Please update your License key. ', 'ERROR');
            exit(1);
        }
        

        $product = $license['product'];
        $type = priceTool::$licenseType[$license['type']];

        $this->logBlock('License type: ' . $product .' (' . $type . ')', 'INFO');
        $this->logBlock('Company name: ' . $license['name'], 'INFO');
        $this->logBlock('Expire:       ' . date('Y-m-d, H:i', $license['expire_at']), 'INFO');
        
        exit(0);
    }
}

?>
