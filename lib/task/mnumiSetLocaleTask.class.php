<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of mnumiSetLocale
 *
 * @author jupeter
 */
class mnumiSetLocaleTask extends sfBaseTask
{

    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
                // add your own options here
        ));

        $this->addArguments(array(
            new sfCommandArgument('lang', sfCommandArgument::REQUIRED, 'Language code (ex. "pl_PL", "en_US")'),
            new sfCommandArgument('currency', sfCommandArgument::REQUIRED, 'Currency (ex. "PLN", "EUR", "USD")'),
        ));

        $this->namespace           = 'mnumi';
        $this->name                = 'set-locale';
        $this->briefDescription    = '';
        $this->detailedDescription = <<<EOF
The [mnumi:set-locale|INFO] task save in configuration Default Culture and Currency.
Call example:

  [php symfony mnumi:set-locale pl_PL PLN"|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);

        $language = $arguments['lang'];
        $currency = $arguments['currency'];

        $language = $this->cleanupLanguage($language);
        $currency = $this->cleanupCurrency($currency);

        $this->setLanguage($language);
        $this->setCurrency($currency);

        exit(0);
    }

    private function setLanguage($value)
    {
        $appConfiguration = sfConfig::get('sf_app_config_dir') .'/settings.yml';

        if(!file_exists($appConfiguration))
        {
            $this->logBlock(sprintf('Configuration file (%s) does not exist.', $appConfiguration), 'ERROR');
            exit(1);
        }

        $config = sfYaml::load($appConfiguration);

        $config['all']['.settings']['default_culture'] = $value;

        $result = file_put_contents($appConfiguration, sfYaml::dump($config, 4));
        if($result === false)
        {
            $this->logBlock(sprintf('Could not save (%s) file.', $appConfiguration), 'ERROR');
            exit(1);
        }

        $this->log('Set default culture: ' . $value);
    }

    private function setCurrency($value)
    {
        $appConfiguration = sfConfig::get('sf_app_config_dir') .'/app.yml';

        if(!file_exists($appConfiguration))
        {
            $this->logBlock(sprintf('Configuration file (%s) does not exist.', $appConfiguration), 'ERROR');
            exit(1);
        }

        $config = sfYaml::load($appConfiguration);

        $config['all']['currency'] = $value;
        $config['all']['default_currency'] = $value;

        $result = file_put_contents($appConfiguration, sfYaml::dump($config, 4));
        if($result === false)
        {
            $this->logBlock(sprintf('Could not save (%s) file.', $appConfiguration), 'ERROR');
            exit(1);
        }

        $this->log('Set currency: ' . $value);
    }

    /**
     * Cleanup language value
     *
     * @param string $value
     *
     * @return string
     */
    private function cleanupLanguage($value)
    {
        $availableLanguages = array('pl_PL', 'en_US');

        if(!in_array($value, $availableLanguages)) {
            $this->logBlock(sprintf('Incorrect language: %s. Use: %s', $value, implode(', ', $availableLanguages)), 'ERROR');
            exit(1);
        }

        return $value;
    }

    /**
     * Cleanup currency value
     *
     * @param string $value
     *
     * @return string
     */
    private function cleanupCurrency($value)
    {
        $availableCurrencies = array('EUR', 'USD', 'PLN');

        if(!in_array($value, $availableCurrencies)) {
            $this->log(sprintf('Incorrect currency: %s. Use: %s', $value, implode(', ', $availableCurrencies)));
            exit(1);
        }

        return $value;
    }

}

?>
