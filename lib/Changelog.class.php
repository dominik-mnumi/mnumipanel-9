<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Changelog class for handling versionable tables
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class Changelog
{
    // array of versions fetched from database
    private $versions;
    
    // relations and settings
    private $relations;
    
    /**
     * Constructor
     * 
     * @param $tableName Table name that has versions
     * @param $itemId Id of item which we want to check
     * @param $relations Additional related tables that we want to check
     * 
     */
    function __construct($tableName, $itemId, $relations = null)
    {
        $this->relations = $relations;
        
        $connection = Doctrine_Manager::connection();
        $query = 'SELECT * from '.$tableName.'_version where id = '.$itemId;
        $statement = $connection->execute($query);
        $statement->execute();
        $this->versions['main'] = $statement->fetchAll(PDO::FETCH_ASSOC);

        if($relations)
        {
            foreach($relations as $tableName => $item)
            {
                $connection = Doctrine_Manager::connection();
                $query = 'SELECT * from '.$tableName.'_version where '.$item['id'].' = '.$itemId;
                $statement = $connection->execute($query);
                $statement->execute();
                $this->versions["$tableName"] = $statement->fetchAll(PDO::FETCH_ASSOC);
                
                //select deleted items
                $connection = Doctrine_Manager::connection();
                $query = 'SELECT * from '.$tableName.' where '.$item['id'].' = '.$itemId.' and deleted_at is not null';
                $statement = $connection->execute($query);
                $statement->execute();
                $this->versions["$tableName-deleted"] = $statement->fetchAll(PDO::FETCH_ASSOC);
            }
        }
    }
    
    /**
     * Returns versions differences that are ready to presentation
     * 
     * @return array
     */
    public function getDifferences()
    {
        $versionChanges = array();

        foreach($this->versions as $k => $versions)
        {
            $tempVersion = null;
            foreach($versions as $version)
            {
                $changes = array();
                //set editor and edit date
                $editor = Doctrine_Core::getTable('sfGuardUser')->find($version['editor_id']);
                if(!$editor)
                {
                    throw new Exception('No editor found for order.');
                }
                $changes['editor'] = $editor->getUsername();
                $changes['edited_at'] = $version['updated_at'];
                
                //for next versions
                if($version['version'] != 1)
                {
                    $diffs = array_diff_assoc($version, $tempVersion);
                    
                    // basic fields (special) that we do not count as difference
                    unset($diffs['updated_at'], $diffs['version'], $diffs['editor_id']);
                    
                    foreach($diffs as $key => $diff)
                    {
                        $changes[$key] = array('previous' => $tempVersion[$key], 'current' => $version[$key]);
                    }

                    $versionChanges[] = $changes;   
                }
                // if this is related table and version 1 (item added) or item is deleted
                elseif($k != 'main')
                {
                    if(strstr($k, 'deleted'))
                    {
                        $type = 'deleted';
                        $relKey = str_replace('-deleted', '', $k);
                    }
                    else
                    {
                        $type = 'added';
                        $relKey = $k;
                        if($version['version'] == 1)
                        $changes['edited_at'] = $version['created_at'];
                    }
                    
                    $valuesToShow = array();
                    foreach($this->relations[$relKey]['fields'] as $rel)
                    {
                        if($version["$rel"])
                        {
                            $valuesToShow["$rel"] = $version["$rel"];
                        }
                    }
                    $changes["$type".$version['id']] = array($type => $valuesToShow);
                    $versionChanges[] = $changes;
                }
                $tempVersion = $version;
            }
            
        }
        
        $versionChanges = $this->sortResults($versionChanges);
        $versionChanges = $this->mergeResults($versionChanges);
        $versionChanges = $this->cleanResults($versionChanges);

        return $versionChanges;
    }
    
    /**
     * Sorting result table by edit time
     * 
     * @return array
     */
    private function sortResults($versionChanges)
    {
        if(!function_exists('compareEditDate'))
        {
            function compareEditDate($a, $b)
            { 
                return strnatcmp($a['edited_at'], $b['edited_at']);
            }
        }
        
        usort($versionChanges, 'compareEditDate');
        return $versionChanges;
    }
    
    /**
     *  Merge different tables items with the same time and editor (probably saved in one action)
     *  
     *  @return array
     */
    private function mergeResults($versionChanges)
    {
        $tempEditedAt = $tempEditor = null;
        
        foreach($versionChanges as $key => $change)
        {
            if($change['edited_at'] == $tempEditedAt && $tempEditor == $change['editor'])
            {
                unset($change['edited_at']);
                unset($change['editor']);
                $versionChanges[$key] = array_merge($versionChanges[$key], $versionChanges[$key-1]);
                
                unset($versionChanges[$key-1]);
                continue;
            }
            $tempEditedAt = $change['edited_at'];
            $tempEditor = $change['editor'];
        }
        return $versionChanges;
    }
    
    /**
     * Clean results (when added and deleted is in the same change)
     */
    private function cleanResults($versionChanges)
    {
        foreach($versionChanges as $key => $change)
        {
            if(array_key_exists('added', $change) && array_key_exists('deleted', $change))
            {
                unset($versionChanges[$key]['added']);
            }
        }
        return $versionChanges;
    }
    
  
}