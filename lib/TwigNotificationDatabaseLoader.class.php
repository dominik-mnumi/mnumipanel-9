<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extends Twig_LoaderInterface to get notification template from database
 * basing on template name and type name.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class TwigNotificationDatabaseLoader implements Twig_LoaderInterface
{
    /**
     * Creates template name basing on database template name and type.
     * 
     * @param string $templateName
     * @param string $typeName
     * @return string 
     */
    public static function createTemplateName($templateName, $typeName)
    {
        return implode(';', array($templateName, $typeName));
    }
    
    /**
     * Returns source of template basing on name.
     * 
     * @param string $name
     * @return string
     * @throws Twig_Error_Loader 
     */
    public function getSource($name)
    {
        $paramArr = explode(';', $name);
        
        $notificationObj = NotificationTable::getInstance()
                ->getByTemplateNameAndTypeName($paramArr[0], $paramArr[1]);
        
        if(!$notificationObj)
        {
            throw new Twig_Error_Loader('The notification object does not exist.');
        }
        
        return $notificationObj->getContent();
    }

    public function getCacheKey($name)
    {
        return $name;
    }

    public function isFresh($name, $time)
    {
        return false;
    }

}