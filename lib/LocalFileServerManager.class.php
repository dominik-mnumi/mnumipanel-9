<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * LocalFileServerManager class.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class LocalFileServerManager
{
    private static $_localFileServerManagerInstance;
    private $dev;
    
    /**
     * Returns LocalFileServerManager instance.
     * 
     * @return LocalFileServerManager 
     */
    public static function getInstance($dev = false)
    {
        if(!self::$_localFileServerManagerInstance)
        {
            self::$_localFileServerManagerInstance = new self();
        }
        
        // sets dev environment if defined
        self::$_localFileServerManagerInstance->dev = $dev;
        
        return self::$_localFileServerManagerInstance;
    }
    
    /**
     * Returns local file server ip.
     * 
     * @return string
     */
    public function getHost()
    {
        $host = sfConfig::get('app_local_file_server_ip').'/';
        if($this->dev)
        {
            $host .= 'app_dev.php/';
        }
        
        return $host;
    }
    
    /**
     * Returns local file server 'has' request url.
     * 
     * @param string $filepath
     * @return string
     */
    public function getHasUrl($filepath = null)
    {
        $url = $this->getHost().'has';
        if($filepath)
        {
            $url .= '?filepath='.$filepath;
        }
        
        return $url;
    }
    
    /**
     * Returns local file server 'get' request url.
     * 
     * @param string $filepath
     * @return string
     */
    public function getGetUrl($filepath = null)
    {
        $url = $this->getHost().'get';
        if($filepath)
        {
            $url .= '?filepath='.$filepath;
        }
        
        return $url;
    }
    
    /**
     * Returns local file server 'getWaitingFileList' request url.
     * 
     * @param string $orderId
     * @return string
     */
    public function getGetWaitingFileListUrl($orderId = null)
    {
        $url = $this->getHost().'getWaitingFileList';
        if($orderId)
        {
            $url .= '?orderId='.$orderId;
        }
        
        return $url;
    }
    
    /**
     * Returns local file server 'upload' request url.
     * 
     * @param integer $orderId
     * @return string
     */
    public function getUploadUrl($orderId = null)
    {
        $url = $this->getHost().'upload';
        if($orderId)
        {
            $url .= '?orderId='.$orderId;
        }
        
        return $url;
    }
}

?>
