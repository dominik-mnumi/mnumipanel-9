<?php
/**
* sfValidatorIp checks if the value is an ip address.
*
*/
class sfValidatorIp extends sfValidatorRegex
{
    protected function configure($options = array(), $messages = array())
    {
        parent::configure($options, $messages);
        
        $this->setMessage('invalid', 'Field "IP address" is invalid');
        $this->setOption('pattern', '
            ~^
            ([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}  # ip address
            (\:([1-9][0-9]+))?
            $~ix'
        );
    }
}
