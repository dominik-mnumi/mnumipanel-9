<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorPostcodeAndCityGB
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki
 */

class sfValidatorPostcodeAndCityGB extends sfValidatorString
{
    /**
     * Extend doClean method. Replacing last "/" by " "
     *
     * @param string $value
     */
    protected function doClean($value)
    {
        $postcodeAndArray = explode(' ', $value, 2);

        if(2 != count($postcodeAndArray) || empty($postcodeAndArray[1]))
        {
            throw new sfValidatorError($this, 'Field "Postcode and city" is invalid. Format: Postcode City', array('value' => $value));
        }
        
        return parent::doClean($value);
    }
}