<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorRegexUsername
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Adam Marchewicz
 */

class sfValidatorRegexUsername extends sfValidatorRegex
{
    /**
     * Extend doClean method. Convert to lower case
     *
     * @param string $value
     */
    protected function doClean($value)
    {
        $value = trim(strtolower($value));
        
        return parent::doClean($value);
    }
}