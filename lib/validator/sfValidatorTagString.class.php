<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorTagString
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki
 */

class sfValidatorTagString extends sfValidatorString
{
    protected function configure($options = array(), $messages = array())
    {
        parent::configure($options, $messages);

        $this->addOption('separator', ' ');
    }

    /**
     * Extend doClean method. Replacing last "/" by " "
     *
     * @param string $value
     * @return string
     */
    protected function doClean($value)
    {
        $string = strtolower($value);

        //prepares string
        $replaceArray = array(' ', '--', '&quot;', '!', '@', '#', '$', '%', '^', '&', '*',
            '(', ')', '_', '+', '{', '}', '|', ':', '"', '<', '>', '?',
            '[', ']', '\\', ';', "'", ',', '.', '/', '*', '+', '~', '`',
            '=');
        $string = str_replace($replaceArray, $this->getOption('separator'), $string);
        
        return parent::doClean($string);
    }
}