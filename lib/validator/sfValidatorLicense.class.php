<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorLicense
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki
 */

class sfValidatorLicense extends sfValidatorString
{
    /**
     * Extends doClean method. 
     *
     * @param string $value
     */
    protected function doClean($value)
    {
        if(priceTool::checkKey($value) === false)
        {
            throw new sfValidatorError($this, 'Your licence key is invalid or not set.');
        }
     
        return parent::doClean($value);
    }
}
