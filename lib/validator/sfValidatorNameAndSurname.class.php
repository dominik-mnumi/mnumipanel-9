<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorNameAndSurname
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki
 */

class sfValidatorNameAndSurname extends sfValidatorString
{
    /**
     * Extend doClean method. Validates "Name Surname" format.
     *
     * @param string $value
     */
    protected function doClean($value)
    {
        $value = trim($value);
        $nameAndSurnameArray = explode(' ', $value);
        if(2 != count($nameAndSurnameArray) || empty($nameAndSurnameArray[1]))
        {
            throw new sfValidatorError($this, 'invalid', array('value' => $value));
        } 
        
        return parent::doClean($value);
    }
}