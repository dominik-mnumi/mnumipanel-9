<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version90 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropForeignKey('order_packages', 'order_packages_payment_id_payment_id');
        $this->createForeignKey('order_packages', 'order_packages_payment_id_payment_id_1', array(
             'name' => 'order_packages_payment_id_payment_id_1',
             'local' => 'payment_id',
             'foreign' => 'id',
             'foreignTable' => 'payment',
             'onUpdate' => 'cascade',
             'onDelete' => 'set null',
             ));
        $this->addIndex('order_packages', 'order_packages_payment_id', array(
             'fields' => 
             array(
              0 => 'payment_id',
             ),
             ));
    }

    public function down()
    {
        $this->createForeignKey('order_packages', 'order_packages_payment_id_payment_id', array(
             'name' => 'order_packages_payment_id_payment_id',
             'local' => 'payment_id',
             'foreign' => 'id',
             'foreignTable' => 'payment',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->dropForeignKey('order_packages', 'order_packages_payment_id_payment_id_1');
        $this->removeIndex('order_packages', 'order_packages_payment_id', array(
             'fields' => 
             array(
              0 => 'payment_id',
             ),
             ));
    }
}