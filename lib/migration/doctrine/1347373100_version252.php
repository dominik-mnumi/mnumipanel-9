<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version252 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->changeColumn('printsize', 'width', 'float', '', array(
             'notnull' => '1',
             ));
        $this->changeColumn('printsize', 'height', 'float', '', array(
             'notnull' => '1',
             ));
    }

    public function down()
    {
      
    }
}