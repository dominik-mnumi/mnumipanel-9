<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version100 extends Doctrine_Migration_Base
{
    public function up()
    {
        
        $this->createForeignKey('cash_desk', 'cash_desk_client_id_client_id', array(
             'name' => 'cash_desk_client_id_client_id',
             'local' => 'client_id',
             'foreign' => 'id',
             'foreignTable' => 'client',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('cash_desk', 'cash_desk_invoice_id_invoice_id', array(
             'name' => 'cash_desk_invoice_id_invoice_id',
             'local' => 'invoice_id',
             'foreign' => 'id',
             'foreignTable' => 'invoice',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('cash_desk', 'cash_desk_cash_report_id_cash_report_id', array(
             'name' => 'cash_desk_cash_report_id_cash_report_id',
             'local' => 'cash_report_id',
             'foreign' => 'id',
             'foreignTable' => 'cash_report',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('cash_desk', 'cash_desk_user_id_sf_guard_user_id', array(
             'name' => 'cash_desk_user_id_sf_guard_user_id',
             'local' => 'user_id',
             'foreign' => 'id',
             'foreignTable' => 'sf_guard_user',
             ));
        $this->createForeignKey('invoice', 'invoice_client_id_client_id_1', array(
             'name' => 'invoice_client_id_client_id_1',
             'local' => 'client_id',
             'foreign' => 'id',
             'foreignTable' => 'client',
             'onUpdate' => 'cascade',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('invoice', 'invoice_invoice_type_name_invoice_type_name', array(
             'name' => 'invoice_invoice_type_name_invoice_type_name',
             'local' => 'invoice_type_name',
             'foreign' => 'name',
             'foreignTable' => 'invoice_type',
             'onUpdate' => 'cascade',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('invoice', 'invoice_payment_id_payment_id', array(
             'name' => 'invoice_payment_id_payment_id',
             'local' => 'payment_id',
             'foreign' => 'id',
             'foreignTable' => 'payment',
             'onUpdate' => 'cascade',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('invoice_item', 'invoice_item_order_id_orders_id_1', array(
             'name' => 'invoice_item_order_id_orders_id_1',
             'local' => 'order_id',
             'foreign' => 'id',
             'foreignTable' => 'orders',
             'onUpdate' => 'cascade',
             'onDelete' => 'set null',
             ));
        $this->createForeignKey('invoice_item', 'invoice_item_tax_id_tax_id', array(
             'name' => 'invoice_item_tax_id_tax_id',
             'local' => 'tax_id',
             'foreign' => 'id',
             'foreignTable' => 'tax',
             'onUpdate' => 'cascade',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('order_packages', 'order_packages_carrier_id_carrier_id', array(
             'name' => 'order_packages_carrier_id_carrier_id',
             'local' => 'carrier_id',
             'foreign' => 'id',
             'foreignTable' => 'carrier',
             'onUpdate' => 'cascade',
             'onDelete' => 'set null',
             ));
       
    }

    public function down()
    {
        $this->createForeignKey('invoice', 'invoice_client_id_client_id', array(
             'name' => 'invoice_client_id_client_id',
             'local' => 'client_id',
             'foreign' => 'id',
             'foreignTable' => 'client',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('invoice', 'invoice_invoice_type_invoice_type_name', array(
             'name' => 'invoice_invoice_type_invoice_type_name',
             'local' => 'invoice_type',
             'foreign' => 'name',
             'foreignTable' => 'invoice_type',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('invoice_item', 'invoice_item_order_id_orders_id', array(
             'name' => 'invoice_item_order_id_orders_id',
             'local' => 'order_id',
             'foreign' => 'id',
             'foreignTable' => 'orders',
             'onUpdate' => 'set null',
             'onDelete' => 'set null',
             ));
        $this->dropForeignKey('cash_desk', 'cash_desk_client_id_client_id');
        $this->dropForeignKey('cash_desk', 'cash_desk_invoice_id_invoice_id');
        $this->dropForeignKey('cash_desk', 'cash_desk_cash_report_id_cash_report_id');
        $this->dropForeignKey('cash_desk', 'cash_desk_user_id_sf_guard_user_id');
        $this->dropForeignKey('invoice', 'invoice_client_id_client_id_1');
        $this->dropForeignKey('invoice', 'invoice_invoice_type_name_invoice_type_name');
        $this->dropForeignKey('invoice', 'invoice_payment_id_payment_id');
        $this->dropForeignKey('invoice_item', 'invoice_item_order_id_orders_id_1');
        $this->dropForeignKey('invoice_item', 'invoice_item_tax_id_tax_id');
        $this->dropForeignKey('order_packages', 'order_packages_carrier_id_carrier_id');
     
    }
}