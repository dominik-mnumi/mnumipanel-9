<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version190 extends Doctrine_Migration_Base
{
    public function up()
    {
        $group = sfGuardGroupTable::getInstance()->findOneByName('admin');
        $group->setDescription('Administrator');
        $group->save();
        
        
        $group = sfGuardGroupTable::getInstance()->findOneByName('office');
        if(!$group)
        {
            $group = new sfGuardGroup();
            $group->setName('office');
        }
        $group->setDescription('Office');
        $group->save();
        
        $group = sfGuardGroupTable::getInstance()->findOneByName('finance');
        if(!$group)
        {
          $group = new sfGuardGroup();
          $group->setName('finance');
        }
        $group->setDescription('Finance');
        $group->save();
    }

    public function down()
    {
       
    }
}