<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version427 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('notification_i18n', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'notification_id' => 
             array(
              'type' => 'integer',
              'length' => '4',
             ),
             'title' => 
             array(
              'type' => 'string',
              'length' => '200',
             ),
             'content' => 
             array(
              'type' => 'clob',
              'length' => '65535',
             ),
             'lang' => 
             array(
              'type' => 'string',
              'length' => '5',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'index1' => 
              array(
              'fields' => 
              array(
               0 => 'notification_id',
               1 => 'lang',
              ),
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_general_ci',
             'charset' => 'utf8',
             ));
    }

    public function postUp()
    {
        $stmt = Doctrine_Manager::getInstance()->connection();
        $configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'prod', true);
        $trans = sfContext::createInstance($configuration)->getI18N();

        $stmt->execute("INSERT INTO notification_i18n SELECT NULL as id, id as notification_id, title, content, '".$trans->getCulture()."' as lang FROM notification");
    }

    public function down()
    {
        $this->dropTable('notification_i18n');
    }
}