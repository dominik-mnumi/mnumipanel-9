<?php
/**
 * Adds product permissions
 */
class Version378 extends Doctrine_Migration_Base
{
    public function up()
    {
        $permissions = array();
        $permissions[sfGuardPermissionTable::$productSuperUser] = 'Product: Bypass product access control (View, edit and delete all content regardless of permission restrictions. Warning: Give to trusted roles only; this permission has security implications.)';
        $permissions[sfGuardPermissionTable::$productViewList]  = 'Product: View products';
        $permissions[sfGuardPermissionTable::$productCreate]    = 'Product: Create product';
        $permissions[sfGuardPermissionTable::$productEdit]      = 'Product: Edit product';
        $permissions[sfGuardPermissionTable::$productDuplicate] = 'Product: Duplicate product';
        $permissions[sfGuardPermissionTable::$productDelete]    = 'Product: Delete product';

        foreach($permissions as $name => $description) {
            $permissionObj = sfGuardPermissionTable::getInstance()->findOneByName($name);
            if(!$permissionObj) {
                $permissionObj = new sfGuardPermission();
                $permissionObj->setName($name);
                $permissionObj->setDescription($description);
                $permissionObj->save();
            }
        }
    }

    public function down()
    {
    }
}
