<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version66 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropTable('client_sf_guard_user_profile');
        
        $this->createTable('client_sf_guard_user_profile', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'client_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '4',
             ),
             'sf_guar_user_profile_user_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '8',
             ),
             'permission_id' => 
             array(
              'type' => 'integer',
              'length' => '8',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'permission_id' => 
              array(
              'fields' => 
              array(
               0 => 'permission_id',
              ),
              ),
              'fk_client_sf_guard_user_profile_client1' => 
              array(
              'fields' => 
              array(
               0 => 'client_id',
              ),
              ),
              'fk_client_sf_guard_user_profile_sf_guard_user_profile1' => 
              array(
              'fields' => 
              array(
               0 => 'sf_guar_user_profile_user_id',
              ),
              ),
              'cliuser' => 
              array(
              'fields' => 
              array(
               0 => 'client_id',
               1 => 'sf_guar_user_profile_user_id',
              ),
              'type' => 'unique',
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'charset' => 'utf8',
             ));
    }

    public function down()
    {
        $this->dropTable('client_sf_guard_user_profile');
    }
}