<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version181 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->changeColumn('invoice_cost', 'user_id', 'integer', '8', array(
             'notnull' => '1',
             ));
    }

    public function down()
    {

    }
}