<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version322 extends Doctrine_Migration_Base
{
    public function up()
    {
	    $permissionObj = sfGuardPermissionTable::getInstance()->findOneByName('order_cost');
	    if(!$permissionObj)
	    {
		    $permissionObj = new sfGuardPermission();
		    $permissionObj->setName('order_cost');
		    $permissionObj->setDescription('Order: Costs');
		    $permissionObj->save();
	    }
    }

    public function down()
    {

    }
}
