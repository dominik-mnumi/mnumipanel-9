<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version411 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('pricelist', 'currency_id', 'integer', '4', array(
             ));
    }

    public function down()
    {
        $this->removeColumn('pricelist', 'currency_id');
    }
}