<?php
/**
 * Adds product bypass permission for Admin Group
 */
class Version380 extends Doctrine_Migration_Base
{
    public function up()
    {
        $conn = Doctrine_Manager::connection();

        try
        {
            $conn->beginTransaction();

            $permission = sfGuardPermissionTable::getInstance()->findOneByName(sfGuardPermissionTable::$productSuperUser);
            $group = sfGuardGroupTable::getInstance()->findOneByName(sfGuardGroupTable::$admin);

            $groupPermission = new sfGuardGroupPermission();
            $groupPermission->setGroupId($group->getId());
            $groupPermission->setPermissionId($permission->getId());
            $groupPermission->save();

            $conn->commit();
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new Exception($e->getMessage());
        }
    }

    public function down()
    {
    }
}
