<?php
/**
 * Adds product permissions for groups which had system_configuration
 */
class Version379 extends Doctrine_Migration_Base
{
    public function up()
    {
        $productPermissionNames = array();
        $productPermissionNames[] = sfGuardPermissionTable::$productViewList;
        $productPermissionNames[] = sfGuardPermissionTable::$productCreate;
        $productPermissionNames[] = sfGuardPermissionTable::$productEdit;
        $productPermissionNames[] = sfGuardPermissionTable::$productDuplicate;
        $productPermissionNames[] = sfGuardPermissionTable::$productDelete;

        // System Configuration Permission
        $permission = sfGuardPermissionTable::getInstance()
            ->findOneByName(sfGuardPermissionTable::$systemConfiguration);

        // Groups containing System Configuration permission
        $groups = sfGuardGroupPermissionTable::getInstance()->findByPermissionId($permission->getId());

        $conn = Doctrine_Manager::connection();
        try
        {
            $conn->beginTransaction();

            foreach($productPermissionNames as $productPermissionName) {
                $permission = sfGuardPermissionTable::getInstance()->findOneByName($productPermissionName);

                foreach($groups as $group) {
                    $groupPermission = new sfGuardGroupPermission();
                    $groupPermission->setGroupId($group->group_id);
                    $groupPermission->setPermissionId($permission->getId());
                    $groupPermission->save();
                }
            }

            $conn->commit();
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new Exception($e->getMessage());
        }
    }

    public function down()
    {
    }
}
