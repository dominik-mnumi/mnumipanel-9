<?php
/**
 * This class has been NOT auto-generated by the Doctrine ORM Framework.
 */
class Version23 extends Doctrine_Migration_Base
{
    public function up()
    {        
        $this->createForeignKey('sf_guard_user_profile', 'sf_guard_user_profile_user_id_sf_guard_user_id', array(
             'name' => 'sf_guard_user_profile_user_id_sf_guard_user_id',
             'local' => 'user_id',
             'foreign' => 'id',
             'foreignTable' => 'sf_guard_user',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        
        $this->createForeignKey('client', 'client_last_address_id_client_address_id', array(
             'name' => 'client_last_address_id_client_address_id',
             'local' => 'last_address_id',
             'foreign' => 'id',
             'foreignTable' => 'client_address',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('client', 'client_pricelist_id_pricelist_id', array(
             'name' => 'client_pricelist_id_pricelist_id',
             'local' => 'pricelist_id',
             'foreign' => 'id',
             'foreignTable' => 'pricelist',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->createForeignKey('client', 'client_client_type_id_client_type_id', array(
             'name' => 'client_client_type_id_client_type_id',
             'local' => 'client_type_id',
             'foreign' => 'id',
             'foreignTable' => 'client_type',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));        
        $this->createForeignKey('client_address', 'client_address_client_id_client_id', array(
             'name' => 'client_address_client_id_client_id',
             'local' => 'client_id',
             'foreign' => 'id',
             'foreignTable' => 'client',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->createForeignKey('client_attachment', 'client_attachment_client_id_client_id', array(
             'name' => 'client_attachment_client_id_client_id',
             'local' => 'client_id',
             'foreign' => 'id',
             'foreignTable' => 'client',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->createForeignKey('client', 'cssu', array(
             'name' => 'cssu',
             'local' => 'sf_guard_user_profile_user_id',
             'foreign' => 'user_id',
             'foreignTable' => 'sf_guard_user_profile',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->createForeignKey('contact', 'cssu_1', array(
             'name' => 'cssu_1',
             'local' => 'sf_guard_user_profile_user_id',
             'foreign' => 'user_id',
             'foreignTable' => 'sf_guard_user_profile',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
    
       $this->addIndex('client', 'fk_company_sf_guard_user_profile1', array(
             'fields' => 
             array(
              0 => 'sf_guard_user_profile_user_id',
             ),
             ));
        $this->addIndex('client', 'fk_company_address1', array(
             'fields' => 
             array(
              0 => 'last_address_id',
             ),
             )); 
        $this->addIndex('client', 'fk_client_pricelist1', array(
             'fields' => 
             array(
              0 => 'pricelist_id',
             ),
             )); 
        $this->addIndex('client', 'fk_client_client_type1', array(
             'fields' => 
             array(
              0 => 'client_type_id',
             ),
             )); 
         
        $this->addIndex('client_address', 'fk_address_client1', array(
             'fields' => 
             array(
              0 => 'client_id',
             ),
             ));
         
        $this->addIndex('client_attachment', 'fk_client_attachment_client1', array(
             'fields' => 
             array(
              0 => 'client_id',
             ),
             ));
         
        $this->addIndex('contact', 'fk_contact_sf_guard_user_profile1', array(
             'fields' => 
             array(
              0 => 'sf_guard_user_profile_user_id',
             ),
             ));     
         
        $this->addIndex('sf_guard_user_profile', 'user_id', array(
             'fields' => 
             array(
              0 => 'user_id',
             ),
             )); 
        
       // $this->removeColumn('sf_guard_user_profile', 'id');
    }

    public function down()
    {
        /*
        $this->createPrimaryKey('sf_guard_user_profile', 
                array('id' => array(
                    'type' => 'integer', 
                    'autoincrement' => true,
                    'length' => '8')));   */
    
        $this->dropForeignKey('client', 'cssu');
        $this->dropForeignKey('client', 'client_pricelist_id_pricelist_id');
        $this->dropForeignKey('client', 'client_last_address_id_client_address_id');
        $this->dropForeignKey('client', 'client_client_type_id_client_type_id');
        $this->dropForeignKey('client_address', 'client_address_client_id_client_id');
        $this->dropForeignKey('client_attachment', 'client_attachment_client_id_client_id');
        $this->dropForeignKey('contact', 'cssu_1');
        $this->dropForeignKey('sf_guard_user_profile', 'sf_guard_user_profile_user_id_sf_guard_user_id');
        
        $this->removeIndex('client', 'fk_company_sf_guard_user_profile1', array(
             'fields' => 
             array(
              0 => 'sf_guard_user_profile_user_id',
             ),
             ));
        $this->removeIndex('client', 'fk_company_address1', array(
             'fields' => 
             array(
              0 => 'last_address_id',
             ),
             )); 
        $this->removeIndex('client', 'fk_client_pricelist1', array(
             'fields' => 
             array(
              0 => 'pricelist_id',
             ),
             )); 
        $this->removeIndex('client', 'fk_client_client_type1', array(
             'fields' => 
             array(
              0 => 'client_type_id',
             ),
             )); 
         
        $this->removeIndex('client_address', 'fk_address_client1', array(
             'fields' => 
             array(
              0 => 'client_id',
             ),
             ));
         
        $this->removeIndex('client_attachment', 'fk_client_attachment_client1', array(
             'fields' => 
             array(
              0 => 'client_id',
             ),
             ));
         
        $this->removeIndex('contact', 'fk_contact_sf_guard_user_profile1', array(
             'fields' => 
             array(
              0 => 'sf_guard_user_profile_user_id',
             ),
             ));     
         
        $this->removeIndex('sf_guard_user_profile', 'user_id', array(
             'fields' => 
             array(
              0 => 'user_id',
             ),
             )); 
                    
    }
}
 