<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version216 extends Doctrine_Migration_Base
{
    public function up()
    {
        $obj = CategoryTable::getInstance()->findOneByName('All');
        if($obj)
        {
            $obj->setName('Main');
            $obj->save();
        }
    }

    public function down()
    {

    }
}