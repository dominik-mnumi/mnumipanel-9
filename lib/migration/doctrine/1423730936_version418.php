<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version418 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createForeignKey('invoice_item', 'invoice_item_unit_of_measure_unit_id', array(
             'name' => 'invoice_item_unit_of_measure_unit_id',
             'local' => 'unit_of_measure',
             'foreign' => 'id',
             'foreignTable' => 'unit',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->addIndex('invoice_item', 'invoice_item_unit_of_measure', array(
             'fields' => 
             array(
              0 => 'unit_of_measure',
             ),
             ));
        $this->addIndex('invoice_item', 'fk_invoice_item_unit_idx', array(
             'fields' =>
             array(
              0 => 'unit_of_measure',
             ),
             ));
    }

    public function down()
    {
        $this->dropForeignKey('invoice_item', 'invoice_item_unit_of_measure_unit_id');
        $this->removeIndex('invoice_item', 'invoice_item_unit_of_measure', array(
             'fields' => 
             array(
              0 => 'unit_of_measure',
             ),
             ));
        $this->removeIndex('invoice_item', 'fk_invoice_item_unit_idx', array(
             'fields' =>
             array(
              0 => 'unit_of_measure',
             ),
             ));
    }
}