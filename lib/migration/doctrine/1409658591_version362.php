<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version362 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('client', 'wnt_ue', 'boolean', '25', array(
             'notnull' => '1',
             'default' => '0',
             ));
        $this->changeColumn('invoice_item', 'tax_value', 'integer', '4', array(
             ));
    }

    public function down()
    {
        $this->removeColumn('client', 'wnt_ue');
    }
}