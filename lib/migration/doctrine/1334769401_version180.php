<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version180 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createForeignKey('invoice_cost_payment', 'invoice_cost_payment_user_id_sf_guard_user_id', array(
             'name' => 'invoice_cost_payment_user_id_sf_guard_user_id',
             'local' => 'user_id',
             'foreign' => 'id',
             'foreignTable' => 'sf_guard_user',
             'onUpdate' => '',
             'onDelete' => 'set null',
             ));
        $this->addIndex('invoice_cost_payment', 'invoice_cost_payment_user_id', array(
             'fields' => 
             array(
              0 => 'user_id',
             ),
             ));
    }

    public function down()
    {
        $this->dropForeignKey('invoice_cost_payment', 'invoice_cost_payment_user_id_sf_guard_user_id');
        $this->removeIndex('invoice_cost_payment', 'invoice_cost_payment_user_id', array(
             'fields' => 
             array(
              0 => 'user_id',
             ),
             ));
    }
}