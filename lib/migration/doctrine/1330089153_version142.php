<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version142 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropForeignKey('custom_price', 'custom_price_product_field_id_product_field_id');
        $this->dropForeignKey('custom_price', 'custom_price_product_field_item_id_product_field_item_id');
        $this->removeIndex('custom_price', 'fk_custom_price_product_field1', array(
            'fields' =>
            array(
                0 => 'product_field_id',
            ),
        ));
        $this->removeIndex('custom_price', 'fk_custom_price_product_field_item1', array(
            'fields' =>
            array(
                0 => 'product_field_item_id',
            ),
        ));
        
        $this->removeColumn('custom_price', 'product_field_id');
        $this->removeColumn('custom_price', 'product_field_item_id');
        $this->addColumn('custom_price', 'fieldset_id', 'integer', '4', array(
             'notnull' => '1',
             ));
        $this->addColumn('custom_price', 'field_item_id', 'integer', '4', array(
             ));
    }

    public function down()
    {
        $this->addColumn('custom_price', 'product_field_id', 'integer', '4', array(
             'notnull' => '1',
             ));
        $this->addColumn('custom_price', 'product_field_item_id', 'integer', '4', array(
             ));
        $this->removeColumn('custom_price', 'fieldset_id');
        $this->removeColumn('custom_price', 'field_item_id');
    }
}