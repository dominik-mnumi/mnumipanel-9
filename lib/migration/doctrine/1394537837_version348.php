<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version348 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('invoice', 'service_realized_at', 'date', '25', array(
             ));
        $this->addColumn('invoice_version', 'service_realized_at', 'date', '25', array(
             ));
    }

    public function down()
    {
        $this->removeColumn('invoice', 'service_realized_at');
        $this->removeColumn('invoice_version', 'service_realized_at');
    }
}