<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version376 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->changeColumn('product_fixprice', 'cost', 'float', '18', array(
            'scale' => '3',
        ));
        $this->changeColumn('product_fixprice', 'price', 'float', '18', array(
            'scale' => '3',
        ));
    }

    public function down()
    {
    }
}
