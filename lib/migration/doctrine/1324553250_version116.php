<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version116 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->renameColumn('cash_desk', 'ballance', 'balance');
    }

    public function down()
    {
        $this->renameColumn('cash_desk', 'balance', 'ballance');
    }
}