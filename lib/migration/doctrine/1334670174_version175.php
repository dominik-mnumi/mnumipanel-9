<?php
/**
 * This class copies data to new field_item_price_quantity table from
 * field_item_price_range.
 */
class Version175 extends Doctrine_Migration_Base
{
    public function up()
    {
        // gets all FieldItemPriceRange
        $fieldItemPriceRangeColl = FieldItemPriceRangeTable::getInstance()->findAll();
        
        // prepares price array
        $priceArr = array(FieldItemPriceTypeTable::$price_simplex, FieldItemPriceTypeTable::$price_duplex,
            FieldItemPriceTypeTable::$price_page, FieldItemPriceTypeTable::$price_copy,
            FieldItemPriceTypeTable::$price_linear_metre, FieldItemPriceTypeTable::$price_square_metre,
            FieldItemPriceTypeTable::$price_item);
        
        // saves field item price types
        foreach($priceArr as $rec)
        {
            $fieldItemPriceTypeObj = new FieldItemPriceType();
            $fieldItemPriceTypeObj->setName($rec);
            $fieldItemPriceTypeObj->save(); 
        }
        
        // foreach price range
        foreach($fieldItemPriceRangeColl as $rec)
        {       
            // foreach price
            foreach($priceArr as $rec2)
            {
                $method = 'get'.sfInflector::camelize($rec2);
                if($rec->$method())
                {
                    $fieldItemPriceQuantityObj = new FieldItemPriceQuantity();
                    $fieldItemPriceQuantityObj->setFieldItemPrice($rec->getFieldItemPrice());
                    $fieldItemPriceQuantityObj->setQuantity($rec->getQuantity());
                    $fieldItemPriceQuantityObj->setPrice($rec->$method());
                    $fieldItemPriceQuantityObj->setFieldItemPriceTypeName($rec2);               
                    $fieldItemPriceQuantityObj->save();
                }
            }   
        }
    }

    public function down()
    {
        
    }
}