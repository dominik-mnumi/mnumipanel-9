<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version260 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createForeignKey('order_packages', 'order_packages_coupon_name_coupon_name', array(
             'name' => 'order_packages_coupon_name_coupon_name',
             'local' => 'coupon_name',
             'foreign' => 'name',
             'foreignTable' => 'coupon',
             'onUpdate' => 'cascade',
             'onDelete' => 'set null',
             ));
        $this->addIndex('order_packages', 'order_packages_coupon_name', array(
             'fields' => 
             array(
              0 => 'coupon_name',
             ),
             ));
        $this->addIndex('order_packages', 'fk_order_packages_coupon1', array(
             'fields' => 
             array(
              0 => 'coupon_name',
             ),
             ));
    }

    public function down()
    {
        $this->dropForeignKey('order_packages', 'order_packages_coupon_name_coupon_name');
        $this->removeIndex('order_packages', 'order_packages_coupon_name', array(
             'fields' => 
             array(
              0 => 'coupon_name',
             ),
             ));
        $this->removeIndex('order_packages', 'fk_order_packages_coupon1', array(
             'fields' => 
             array(
              0 => 'coupon_name',
             ),
             ));
    }
}