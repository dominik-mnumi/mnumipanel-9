<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version254 extends Doctrine_Migration_Base
{
    public function up()
    {
        $workflow = new OrderWorkflow();
        $workflow->setCurrentStatus('calculation');
        $workflow->setNextStatus('new');
        $workflow->setTitle('Create new order');
        $workflow->setBackward(0);
        $workflow->save();
    }

    public function down()
    {
        
    }
}