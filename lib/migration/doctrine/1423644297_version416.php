<?php
/**
 * @see PAN-1364
 */
class Version416 extends Doctrine_Migration_Base
{
    public function up()
    {
        $stmt = Doctrine_Manager::getInstance()->connection();
        $stmt->execute("INSERT INTO notification_template (id, name, help_information) VALUES (NULL , 'Invoice', NULL);");
    }

    public function down()
    {

    }
}