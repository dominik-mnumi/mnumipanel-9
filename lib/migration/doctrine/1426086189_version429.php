<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version429 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('loyalty_points', 'currency_id', 'integer', '4', array(
             ));
    }

    public function down()
    {
        $this->removeColumn('loyalty_points', 'currency_id');
    }
}