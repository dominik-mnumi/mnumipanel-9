<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version290 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('coupon', 'created_at', 'timestamp', '25', array(
             'notnull' => '1',
             ));
        $this->addColumn('coupon', 'updated_at', 'timestamp', '25', array(
             'notnull' => '1',
             ));
    }

    public function down()
    {
        $this->removeColumn('coupon', 'created_at');
        $this->removeColumn('coupon', 'updated_at');
    }
}