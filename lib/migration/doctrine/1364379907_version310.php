<?php

class Version310 extends Doctrine_Migration_Base
{
  public function up()
  {
      $configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'prod', true);
      $trans = sfContext::createInstance($configuration)->getI18N();

      $culture = sfConfig::get('sf_default_culture');

      $q = Doctrine_Manager::getInstance()->getCurrentConnection();

      $stringsToTranslate = array(
          '{% trans "Hello" %}' => 'Hello',
          '{% trans "We present calculation for the product" %}' => 'We present calculation for the product',
          '{% trans "that you have requested"%}' => 'that you have requested',
          '{% trans "You can place your order on our website" %}' => 'You can place your order on our website',
          '{% trans "Best regards" %}' => 'Best regards',
          '{% trans "Thank you for your order." %}' => 'Thank you for your order.',
          '{% trans "Your account has been created successfully." %}' => 'Your account has been created successfully.',
          '{% trans "Thank you." %}' => 'Thank you.',
          '{% trans "Login" %}' => 'Login',
          '{% trans "Password" %}' => 'Password',
          '{% trans "For more information, please contact your administrator." %}' => 'For more information, please contact your administrator.',
          '{% trans "Link" %}' => 'Link',
          '{% trans "In a moment it will be verified by our office." %}' => 'In a moment it will be verified by our office.',
          '{% trans "If any doubts or questions occur, we will contact to explain everything." %}' => 'If any doubts or questions occur, we will contact to explain everything.',
          '{% trans "This procedure ensures that the print job will be with the highest quality." %}' => 'This procedure ensures that the print job will be with the highest quality.',
          '{% trans "Ordered products:" %}' => 'Ordered products:',
          '{% trans "Payment method:" %}' => 'Payment method:',
          '{% trans "Delivery type:" %}' => 'Delivery type:',
          '{% trans "Total price:" %}' => 'Total price:',
          '{% trans %}Email sent from the service {{ companyName }} in order to support realization process.{% endtrans %}' =>
               'Email sent from the service {{ companyName }} in order to support realization process.',
          '{% trans "Calculation date" %}' => 'Calculation date',
          '{% trans "Created at:" %}' => 'Created at:',
          '{% trans %}Your order number: {{ orderId }}, "{{ orderName }}" is accepted for processing.{% endtrans %}' =>
              'Your order number: {{ orderId }}, "{{ orderName }}" is accepted for processing.',
          '{% trans "You have requested a new password for your account." %}' => 'You have requested a new password for your account.',
          '{% trans "You can change your password here:" %}' => 'You can change your password here:',
          '{% trans %}You have been asked to {{ companyName }}.{% endtrans %}' => 'You have been asked to {{ companyName }}.',
          '{% trans "If you did not request a new password, another user may have tried to log in trying to use your password or email by mistake." %}' =>
              'If you did not request a new password, another user may have tried to log in trying to use your password or email by mistake.'
      );

      foreach($stringsToTranslate as $text => $value)
      {
          $q->execute('UPDATE notification SET content = replace(content, \''.$text.'\', \''.$trans->__($value).'\')');
      }


  }

  public function down()
  {
  }
}
