<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version423 extends Doctrine_Migration_Base
{
    public function up()
    {
        $configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'prod', true);
        $trans = sfContext::createInstance($configuration)->getI18N();

        $close = $trans->__('Close');
        $backToReady = $trans->__('Back to Ready');

        $stmt = Doctrine_Manager::getInstance()->connection();

        $stmt->execute("INSERT INTO order_status VALUES ('closed', 'Closed', 1, 'closed_32.png') ON DUPLICATE KEY UPDATE name='closed'");

        // Add/Remove from Stock
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(29, 'ready', 'closed', '". $close . "', '0', '3', '0');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(30, 'closed', 'ready', '". $backToReady . "', '1', '3', '0');");

    }

    public function down()
    {
    }
}