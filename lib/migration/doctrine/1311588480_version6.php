<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version6 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('payment_pricelist', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'payment_id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'length' => '4',
             ),
             'pricelist_id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'length' => '4',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'fk_payment_pricelist_payment1' => 
              array(
              'fields' => 
              array(
               0 => 'payment_id',
              ),
              ),
              'fk_payment_pricelist_pricelist1' => 
              array(
              'fields' => 
              array(
               0 => 'pricelist_id',
              ),
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
              1 => 'payment_id',
              2 => 'pricelist_id',
             ),
             'charset' => 'utf8',
             ));
        $this->removeColumn('payment', 'details');
        $this->removeColumn('payment', 'module');
        $this->addColumn('payment', 'label', 'string', '100', array(
             'notnull' => '1',
             ));
        $this->addColumn('payment', 'description', 'clob', '65535', array(
             ));
        $this->addColumn('payment', 'apply_on_all_pricelist', 'boolean', '25', array(
             ));
        $this->addColumn('payment', 'deletable', 'boolean', '25', array(
             ));
        $this->changeColumn('payment', 'name', 'string', '50', array(
             'notnull' => '1',
             ));
        $this->changeColumn('payment', 'active', 'boolean', '25', array(
             ));
    }

    public function down()
    {
        $this->dropTable('payment_pricelist');
        $this->addColumn('payment', 'details', 'clob', '65535', array(
             ));
        $this->addColumn('payment', 'module', 'string', '45', array(
             'notnull' => '1',
             ));
        $this->removeColumn('payment', 'label');
        $this->removeColumn('payment', 'description');
        $this->removeColumn('payment', 'apply_on_all_pricelist');
        $this->removeColumn('payment', 'deletable');
    }
}