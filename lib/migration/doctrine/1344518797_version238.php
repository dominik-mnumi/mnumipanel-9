<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version238 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('product_wizard', 'active', 'boolean', '25', array(
             'notnull' => '1',
             'default' => '1',
             ));
    }

    public function down()
    {
        $this->removeColumn('product_wizard', 'active');
    }
}