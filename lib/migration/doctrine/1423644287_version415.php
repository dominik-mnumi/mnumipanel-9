<?php
/**
 * PAN-1410 Migrate costs from FieldItem to FieldItemPrice
 */
class Version415 extends Doctrine_Migration_Base
{
    public function up()
    {
        $stmt = Doctrine_Manager::getInstance()->connection();

        // Copy existing costs from FieldItem to FieldItemPrice
        $stmt->execute("UPDATE `field_item_price` fip INNER JOIN `field_item` fi ON fi.id = fip.field_item_id SET fip.cost = fi.cost;");
    }

    public function down()
    {
    }
}