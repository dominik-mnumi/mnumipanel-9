<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version347 extends Doctrine_Migration_Base
{
    public function up()
    {
        /** @var Invoice[] $invoices */
        $invoices = InvoiceTable::getInstance()->findAll();

        $sql = '';
        foreach($invoices as $invoice) {
            $sql .= sprintf('UPDATE invoice SET name = "%s" WHERE id = %d AND name = "";',
                $invoice->getFormattedNumber(), $invoice->getId()
            );
        }

        $stmt = Doctrine_Manager::getInstance()->connection();
        if (!empty($sql))
            $stmt->execute($sql);
    }

    public function down()
    {
    }
}