<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version69 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('client_sf_guard_user_profile', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'client_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '4',
             ),
             'sf_guard_user_profile_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '8',
             ),
             'permission_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '8',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'fk_client_sf_guard_user_profile_client1' => 
              array(
              'fields' => 
              array(
               0 => 'client_id',
              ),
              ),
              'cliuser' => 
              array(
              'fields' => 
              array(
               0 => 'client_id',
               1 => 'sf_guard_user_profile_id',
              ),
              'type' => 'unique',
              ),
              'fk_client_sf_guard_user_profile_sf_guard_user_profile1' => 
              array(
              'fields' => 
              array(
               0 => 'sf_guard_user_profile_id',
              ),
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'charset' => 'utf8',
             ));
        $this->createTable('sf_guard_user_profile', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '8',
             ),
             'user_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '8',
             ),
             'last_failed_login' => 
             array(
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'type' => 'InnoDB',
             'primary' => 
             array(
              0 => 'id',
             ),
             'charset' => 'utf8',
             ));
        $this->createTable('user_contact', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'sf_guard_user_profile_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '8',
             ),
             'notification_type_id' => 
             array(
              'type' => 'integer',
              'length' => '4',
             ),
             'value' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '100',
             ),
             'send_notification' => 
             array(
              'type' => 'boolean',
              'length' => '25',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'deleted_at' => 
             array(
              'notnull' => '',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'fk_contact_sf_guard_user_profile1' => 
              array(
              'fields' => 
              array(
               0 => 'sf_guard_user_profile_id',
              ),
              ),
              'fk_user_contact_notification_type1' => 
              array(
              'fields' => 
              array(
               0 => 'notification_type_id',
              ),
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'charset' => 'utf8',
             ));
    }

    public function down()
    {
        $this->dropTable('client_sf_guard_user_profile');
        $this->dropTable('sf_guard_user_profile');
        $this->dropTable('user_contact');
    }
}