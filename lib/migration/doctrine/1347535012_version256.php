<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version256 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('coupon', array(
             'name' => 
             array(
              'type' => 'string',
              'primary' => '1',
              'length' => '255',
             ),
             'type' => 
             array(
              'type' => 'enum',
              'values' => 
              array(
              0 => 'percent',
              1 => 'amount',
              ),
              'notnull' => '1',
              'length' => '',
             ),
             'value' => 
             array(
              'type' => 'float',
              'notnull' => '1',
              'length' => '',
             ),
             'apply_for_all_products' => 
             array(
              'type' => 'boolean',
              'notnull' => '1',
              'default' => '0',
              'length' => '25',
             ),
             'apply_for_all_pricelist' => 
             array(
              'type' => 'boolean',
              'notnull' => '1',
              'default' => '0',
              'length' => '25',
             ),
             'expire_at' => 
             array(
              'type' => 'date',
              'length' => '25',
             ),
             'one_time_usage' => 
             array(
              'type' => 'boolean',
              'notnull' => '1',
              'default' => '0',
              'length' => '25',
             ),
             'used' => 
             array(
              'type' => 'boolean',
              'notnull' => '1',
              'default' => '0',
              'length' => '25',
             ),
             ), array(
             'type' => 'InnoDB',
             'primary' => 
             array(
              0 => 'name',
             ),
             'charset' => 'utf8',
             ));
        $this->createTable('coupon_pricelist', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'coupon_name' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '255',
             ),
             'pricelist_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '4',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'fk_coupon_pricelist_coupon1' => 
              array(
              'fields' => 
              array(
               0 => 'coupon_name',
              ),
              ),
              'fk_coupon_pricelist_pricelist1' => 
              array(
              'fields' => 
              array(
               0 => 'pricelist_id',
              ),
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'charset' => 'utf8',
             ));
        $this->createTable('coupon_product', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'coupon_name' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '255',
             ),
             'product_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '4',
             ),
             ), array(
             'type' => 'InnoDB',
             'indexes' => 
             array(
              'fk_coupon_product_coupon1' => 
              array(
              'fields' => 
              array(
               0 => 'coupon_name',
              ),
              ),
              'fk_coupon_product_product1' => 
              array(
              'fields' => 
              array(
               0 => 'product_id',
              ),
              ),
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'charset' => 'utf8',
             ));
    }

    public function down()
    {
        $this->dropTable('coupon');
        $this->dropTable('coupon_pricelist');
        $this->dropTable('coupon_product');
    }
}