<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version342 extends Doctrine_Migration_Base
{
    public function up()
    {
        $workflow = new OrderWorkflow();
        $workflow->setCurrentStatus(OrderStatusTable::$draft);
        $workflow->setNextStatus(OrderStatusTable::$deleted);
        $workflow->setTitle('Delete');
        $workflow->setBackward(1);
        $workflow->setWorkflow(WorkflowTable::getInstance()->findOneByName('Default Order Workflow'));
        $workflow->save();

        $workflow = new OrderWorkflow();
        $workflow->setCurrentStatus(OrderStatusTable::$draft);
        $workflow->setNextStatus(OrderStatusTable::$deleted);
        $workflow->setTitle('Delete');
        $workflow->setBackward(1);
        $workflow->setWorkflow(WorkflowTable::getInstance()->findOneByName('Simple Order Workflow'));
        $workflow->save();
    }

    public function down()
    {

    }
}