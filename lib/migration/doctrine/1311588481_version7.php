<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version7 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createForeignKey('payment_pricelist', 'payment_pricelist_payment_id_payment_id', array(
             'name' => 'payment_pricelist_payment_id_payment_id',
             'local' => 'payment_id',
             'foreign' => 'id',
             'foreignTable' => 'payment',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->createForeignKey('payment_pricelist', 'payment_pricelist_pricelist_id_pricelist_id', array(
             'name' => 'payment_pricelist_pricelist_id_pricelist_id',
             'local' => 'pricelist_id',
             'foreign' => 'id',
             'foreignTable' => 'pricelist',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->addIndex('payment_pricelist', 'payment_pricelist_payment_id', array(
             'fields' => 
             array(
              0 => 'payment_id',
             ),
             ));
        $this->addIndex('payment_pricelist', 'payment_pricelist_pricelist_id', array(
             'fields' => 
             array(
              0 => 'pricelist_id',
             ),
             ));
    }

    public function down()
    {
        $this->dropForeignKey('payment_pricelist', 'payment_pricelist_payment_id_payment_id');
        $this->dropForeignKey('payment_pricelist', 'payment_pricelist_pricelist_id_pricelist_id');
        $this->removeIndex('payment_pricelist', 'payment_pricelist_payment_id', array(
             'fields' => 
             array(
              0 => 'payment_id',
             ),
             ));
        $this->removeIndex('payment_pricelist', 'payment_pricelist_pricelist_id', array(
             'fields' => 
             array(
              0 => 'pricelist_id',
             ),
             ));
    }
}