<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version182 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropForeignKey('invoice_cost', 'invoice_cost_client_id_client_id');
        $this->createForeignKey('invoice_cost', 'invoice_cost_client_id_client_id_1', array(
             'name' => 'invoice_cost_client_id_client_id_1',
             'local' => 'client_id',
             'foreign' => 'id',
             'foreignTable' => 'client',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->createForeignKey('invoice_cost', 'invoice_cost_user_id_sf_guard_user_id', array(
             'name' => 'invoice_cost_user_id_sf_guard_user_id',
             'local' => 'user_id',
             'foreign' => 'id',
             'foreignTable' => 'sf_guard_user',
             ));
        $this->addIndex('invoice_cost', 'invoice_cost_client_id', array(
             'fields' => 
             array(
              0 => 'client_id',
             ),
             ));
        $this->addIndex('invoice_cost', 'invoice_cost_user_id', array(
             'fields' => 
             array(
              0 => 'user_id',
             ),
             ));
    }

    public function down()
    {
        $this->createForeignKey('invoice_cost', 'invoice_cost_client_id_client_id', array(
             'name' => 'invoice_cost_client_id_client_id',
             'local' => 'client_id',
             'foreign' => 'id',
             'foreignTable' => 'client',
             'onUpdate' => 'no action',
             'onDelete' => 'no action',
             ));
        $this->dropForeignKey('invoice_cost', 'invoice_cost_client_id_client_id_1');
        $this->dropForeignKey('invoice_cost', 'invoice_cost_user_id_sf_guard_user_id');
        $this->removeIndex('invoice_cost', 'invoice_cost_client_id', array(
             'fields' => 
             array(
              0 => 'client_id',
             ),
             ));
        $this->removeIndex('invoice_cost', 'invoice_cost_user_id', array(
             'fields' => 
             array(
              0 => 'user_id',
             ),
             ));
    }
}