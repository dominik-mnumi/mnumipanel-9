<?php
/**
 * This class has been NOT auto-generated by the Doctrine ORM Framework
 * Fixes templates: buy.
 */
class Version267 extends Doctrine_Migration_Base
{
    public function up()
    {
        $stmt = Doctrine_Manager::getInstance()->connection(); 
        
        $templateNameArr = array(
            NotificationTemplateTable::$buy  => '{% trans "Thank you for your order." %}<br /><br />{% trans "In a moment it will be verified by our office." %}<br /><br />{% trans "If any doubts or questions occur, we will contact to explain everything." %}<br /><br />{% trans "This procedure ensures that the print job will be with the highest quality." %}<br /><br />{% trans "Ordered products:" %}{% for order in orderColl %}<br />- {{ order.getName }} - {{ order.getPriceGross }} {{ currencySymbol }}{% endfor %}&nbsp;<br /><br />{% trans "Created at:" %} {{ createdAt }}<br />{% trans "Payment method:" %} {{ paymentType }}<br />{% trans "Delivery type:" %} {{ deliveryType }} ({{ deliveryGrossPrice }})<br />{% trans "Total price:" %}&nbsp;{{ totalGrossPrice }}<br /><br />{% trans %}Email sent from the service {{ companyName }} in order to support realization process.{% endtrans %}'
        );
        

        foreach($templateNameArr as $templateName => $content)
        {        
            // checks if template "register" exists
            $results1 = $stmt->execute("SELECT * FROM notification_template WHERE name = '".$templateName."'")
                    ->fetchAll();

            // if template not exists then create
            if(!isset($results1[0]['id']))
            {
                $stmt->execute("INSERT INTO notification_template(name) VALUES('"
                        .$templateName."');");
            }

            // email
            $typeNameArr = array(NotificationTypeTable::$email, 
                NotificationTypeTable::$sms);
            
            foreach($typeNameArr as $typeName)
            {
                $results1 = $stmt->execute("SELECT * FROM notification WHERE 
                                            notification_template_id = 
                                            (SELECT id FROM notification_template WHERE name = '".$templateName."') 
                                            AND notification_type_id=
                                            (SELECT id FROM notification_type WHERE name = '".$typeName."')")
                        ->fetchAll();

                // if notification does not exist then create
                if(!isset($results1[0]['id']))
                {
                    $stmt->execute("INSERT INTO notification(notification_template_id, notification_type_id, title, content) VALUES(
                                            (SELECT id FROM notification_template WHERE name = '".$templateName."'),
                                            (SELECT id FROM notification_type WHERE name = '".$typeName."'),
                                            '".$templateName."',
                                            '".$content."')");
                }
                // otherwise update
                else
                {
                    $stmt->execute("UPDATE notification 
                                            SET 
                                            title = '".$templateName."',
                                            content = '".$content."'
                                            WHERE 
                                            notification_template_id = (SELECT id FROM notification_template WHERE name = '".$templateName."')
                                            AND
                                            notification_type_id = (SELECT id FROM notification_type WHERE name = '".$typeName."')");
                }
            }            
        }
    }

    public function down()
    {

    }
}
