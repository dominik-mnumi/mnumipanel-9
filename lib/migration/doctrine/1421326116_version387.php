<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version387 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('currency_exchange_rate', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '4',
             ),
             'currency_symbol' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '3',
             ),
             'rate' => 
             array(
              'type' => 'decimal',
              'notnull' => '1',
              'length' => '18',
             ),
             'date' => 
             array(
              'type' => 'timestamp',
              'notnull' => '1',
              'length' => '25',
             ),
             ), array(
             'type' => 'InnoDB',
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_general_ci',
             'charset' => 'utf8',
             ));
    }

    public function down()
    {
        $this->dropTable('currency_exchange_rate');
    }
}