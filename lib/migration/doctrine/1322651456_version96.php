<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version96 extends Doctrine_Migration_Base
{
    public function up()
    {
        Doctrine_Query::create()
          ->update('Order o')
          ->set('o.editor_id', '1')
          ->where('o.editor_id is null')
          ->orWhere('o.editor_id = 0')
          ->execute();
          
        $connection = Doctrine_Manager::connection();
        $query = 'update order_version set editor_id = 1 where editor_id is null or editor_id = 0';
        $statement = $connection->execute($query);
        $statement->execute();
        
        $this->changeColumn('orders', 'editor_id', 'integer', '8', array(
             'notnull' => '1',
             ));
        $this->changeColumn('order_version', 'editor_id', 'integer', '8', array(
             'notnull' => '1',
             ));
    }

    public function down()
    {

    }
}