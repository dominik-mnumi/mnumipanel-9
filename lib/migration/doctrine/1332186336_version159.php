<?php

/**
 * This class fixes products to make them compatibility with wizards.
 */
class Version159 extends Doctrine_Migration_Base
{
    public function up()
    {
        $productColl = ProductTable::getInstance()->findAll();

        foreach($productColl as $rec)
        {
            $obj = ProductFieldTable::getInstance()->createQuery('pf')
                    ->where('f.name = ?', 'WIZARD')
                    ->addWhere('pf.product_id = ?', $rec->getId())
                    ->innerJoin('pf.Fieldset f')
                    ->fetchOne();

            if(!$obj)
            {
                $productFieldObj = new ProductField();
                $productFieldObj->setFieldset(FieldsetTable::getInstance()->findOneByName('WIZARD'));
                $productFieldObj->setProduct($rec);
                $productFieldObj->save();
            }
        }

        // move wizard as child of OTHER
        FieldsetTable::getInstance()
                ->findOneByName('WIZARD')
                ->getNode()
                ->moveAsLastChildOf(FieldsetTable::getInstance()->findOneByName('OTHER'));
    }

    public function down()
    {
        
    }

}