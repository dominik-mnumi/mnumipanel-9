<?php
/**
 * Updates translation for order_file permission
 */
class Version381 extends Doctrine_Migration_Base
{
    public function up()
    {
        $permission = sfGuardPermissionTable::getInstance()->findOneByName(sfGuardPermissionTable::$orderFile);

        $conn = Doctrine_Manager::connection();

        try
        {
            $conn->beginTransaction();
            $permission->setDescription('Order: Upload file or add project');
            $permission->save();

            $conn->commit();
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new Exception($e->getMessage());
        }
    }

    public function down()
    {
    }
}
