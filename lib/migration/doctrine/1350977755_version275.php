<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version275 extends Doctrine_Migration_Base
{
    public function up()
    {
        $stmt = Doctrine_Manager::getInstance()->connection();
        
        // add default workflow for order and link it with order_workflow 
        $stmt->execute("INSERT INTO workflow (id, name, type) VALUES(1, 'Default Order Workflow', 'order');");
        $stmt->execute("UPDATE order_workflow set workflow_id = 1;");
        
        // add default workflow for package and link it with order_package_workflow
        $stmt->execute("INSERT INTO workflow (id, name, type) VALUES(2, 'Default Package Workflow', 'package');");
        $stmt->execute("UPDATE order_package_workflow set workflow_id = 2;");
        
        // update products to be link with default order wokflow
        $stmt->execute("UPDATE product set workflow_id = 1;");
        
        //setting foreign keys and indexes
        $this->createForeignKey('order_package_workflow', 'order_package_workflow_workflow_id_workflow_id', array(
            'name' => 'order_package_workflow_workflow_id_workflow_id',
            'local' => 'workflow_id',
            'foreign' => 'id',
            'foreignTable' => 'workflow',
            'onUpdate' => 'no action',
            'onDelete' => 'no action',
        ));
        $this->createForeignKey('order_workflow', 'order_workflow_workflow_id_workflow_id', array(
            'name' => 'order_workflow_workflow_id_workflow_id',
            'local' => 'workflow_id',
            'foreign' => 'id',
            'foreignTable' => 'workflow',
            'onUpdate' => 'no action',
            'onDelete' => 'no action',
        ));
        $this->createForeignKey('product', 'product_workflow_id_workflow_id', array(
            'name' => 'product_workflow_id_workflow_id',
            'local' => 'workflow_id',
            'foreign' => 'id',
            'foreignTable' => 'workflow',
            'onUpdate' => 'no action',
            'onDelete' => 'no action',
        ));
        $this->addIndex('order_package_workflow', 'order_package_workflow_workflow_id', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->addIndex('order_package_workflow', 'fk_order_package_workflow_workflow1', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->addIndex('order_workflow', 'order_workflow_workflow_id', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->addIndex('order_workflow', 'fk_order_workflow_workflow1', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->addIndex('product', 'product_workflow_id', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->addIndex('product', 'fk_product_workflow1', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        
    }

    public function down()
    {
        $this->dropForeignKey('order_package_workflow', 'order_package_workflow_workflow_id_workflow_id');
        $this->dropForeignKey('order_workflow', 'order_workflow_workflow_id_workflow_id');
        $this->dropForeignKey('product', 'product_workflow_id_workflow_id');
        $this->removeIndex('order_package_workflow', 'order_package_workflow_workflow_id', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->removeIndex('order_package_workflow', 'fk_order_package_workflow_workflow1', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->removeIndex('order_workflow', 'order_workflow_workflow_id', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->removeIndex('order_workflow', 'fk_order_workflow_workflow1', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->removeIndex('product', 'product_workflow_id', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
        $this->removeIndex('product', 'fk_product_workflow1', array(
            'fields' =>
            array(
                0 => 'workflow_id',
            ),
        ));
    }
}