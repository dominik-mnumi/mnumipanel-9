<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 * Class includes some neccessary methods to casting data. Extends built-in 
 * symfony sfInflector class. 
 */

/**
 * @author Marek Balicki
 */
class Cast extends sfInflector
{
    /**
     * Returns one dimensional from multidimensional array.
     * 
     * @param array $input
     * @param array $output
     * @return array 
     */
    public static function multiToOneDimension($input, $output = array())
    {
        // if input empty (array or simple string then return output unchanged)
        if(empty($input))
        {
            return $output;
        }
        
        // if permission is not array return as array
        if(!is_array($input))
        {
            $output[] = $input;
            return $output;
        }

        ksort($input);
        foreach($input as $value)
        {
            $key = count($output);
            if(is_array($value))
            {
                $output = self::multiToOneDimension($value, $output);
            }
            else
            {
                $output[$key] = $value;
            }
        }
        return $output;
    }
    
    /**
     * Returns string after dashes removal.
     * 
     * @param string $string
     * @return string 
     */
    public static function removeDash($string)
    {
        return str_replace('-', '', $string);
    }
    
    /**
     * Returns clean text string (for textarea).
     * 
     * @param string $string
     * @return string 
     */
    public static function html2Text($string)
    {
        $tagToSearchArr = array('<br>', '<br />');
        $replaceArr = array("\n", "\n");
        
        return str_replace($tagToSearchArr, $replaceArr, $string);
    }
    
    /**
     * Returns prepared choice array, 
     * e.g.
     * array(2 => 'name').
     * 
     * @param array $array 
     * @return array
     */
    public static function getChoiceArr($array, $valueColumnName, $keyColumnName = null)
    {
        $resultArr = array();
        foreach($array as $keyTmp => $rec)
        {
            $keyColumnName ? $key = $rec[$keyColumnName] : $key = $keyTmp;

            $resultArr[$key] = $rec[$valueColumnName];
        }
        
        return $resultArr;
    }
    
    /**
     * Returns string prepared for filename.
     * 
     * @param string $input
     * @return string 
     */
    public static function prepareFilename($input)
    {
        $string = self::tableize($input);
        $string = str_replace(' ', '_', $string);

        return $string;
    }

    /**
     * Extends camelize method. Removes spaces.
     *
     * @param string $string
     * @return string
     */
    public static function camelize($string)
    {
        $string = str_replace(' ', '_', $string);

        return parent::camelize($string);
    }

    /**
     * Return column values as array
     *
     * @param array $input
     * @param string $columnKey
     * @return type
     */

    public static function arrayColumn($input, $columnKey)
    {
        $resultArray = array();
        if (function_exists('array_column')) {
            $resultArray = array_column($input, $columnKey);
        }
        else
        {
            foreach ($input as $row) {

               if (is_array($row) && array_key_exists($columnKey, $row)) {
                    $resultArray[] = $row[$columnKey];
                }

            }
        }

        return $resultArray;
    }
}