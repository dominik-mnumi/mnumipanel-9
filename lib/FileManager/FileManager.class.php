<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of fileManage
 *
 * @author jupeter
 */
class FileManager
{
    // array of displayable image mime types
    public static $displayableMimeTypes = array(
            'image/gif',
            'image/jpeg',
            'image/png',
            'image/svg',
            'image/tiff',
            'application/pdf',
            'image/vnd.adobe.photoshop',
        );

    /**
     * Upload files by HTTP POST or array list
     * @example $files = array(
     *   'name',  // name of file
     *   'tmp_name', // temporary name (if upload) or full path of file (if rest)
     *   'size', // file size
     * )
     * @param array $files 
     * @param object $order
     */
    public static function uploadFiles($files, $order)
    {
        $contentType = '';
        
        $filesData = array();
        
        $upload_dir = $order->getFilePath();
        
        $order->preparePath();

        // Look for the content type header
        if(isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if(isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];

        $chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
        $chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;

        foreach($files as $file)
        {
            $filename = preg_replace('/[^\w\._]+/', '', $file['name']);
            $filepath = $upload_dir.$filename;
            $i = 1;
            
            while(file_exists($filepath))
            {
                $filename = $i."-".$filename;
                $filepath = $upload_dir.$filename;
                $i++;
            }

            if(strpos($contentType, "multipart") !== false)
            {
                if((isset($file['tmp_name']) && is_uploaded_file($file['tmp_name'])) 
                        || $forceCopy)
                {

                    // Open temp file
                    $out = fopen($filepath, $chunk == 0?"wb":"ab");
                    if($out)
                    {
                        // Read binary input stream and append it to temp file
                        $in = fopen($file['tmp_name'], "rb");

                        if($in)
                        {
                            while($buff = fread($in, 4096))
                                fwrite($out, $buff);
                        }

                        fclose($in);
                        fclose($out);
                        @unlink($file['tmp_name']);
                    }
                }
            }
            else
            {
                @unlink($filepath);

                if(is_uploaded_file($file["tmp_name"]))
                {
                    move_uploaded_file($file["tmp_name"], $filepath);
                }
                else
                {
                    copy($file["tmp_name"], $filepath);
                }
            }
            
            $filesData[] = array(
                'filename' => $filename,
                'notice' => '',
                'timestamp' => isset($file['timestamp']) ? $file['timestamp'] : null
            );

        }

        return $filesData;
    }

    /**
     * Download file from given url.
     * 
     * @param $url
     * @param $destinationName
     * @return array Array with file data
     * 
     */
    public static function download($url, $destinationName)
    {
        if(!$destinationName)
        {
            throw new Extension('Destination name required');
        }
        
        $timestamp = self::getRemoteFileModificationDate($url);
        
        $tempFolder = sfConfig::get('sf_data_dir').'/files/temp/';
        
        if (!is_dir($tempFolder))
        {
            mkdir($tempFolder);
            chmod($tempFolder, 0777);
        }
        
        $filepath = $tempFolder.$destinationName;
        
        file_put_contents($filepath, file_get_contents($url));
        
        return array('file' => array('name' => $destinationName, 
                                     'tmp_name' => $filepath, 
                                     'timestamp' => $timestamp));
    }
    
    /**
     * Returns file modification timestamp
     *
     * @param $url
     * @return integer file modification timestamp
     *
     */
    public static function getRemoteFileModificationDate($url)
    {
        $curl = curl_init($url);
        
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FILETIME, true);
        
        $result = curl_exec($curl);
        
        if ($result === false)
        {
          throw new Exception(curl_error($curl));
        }
        
        return curl_getinfo($curl, CURLINFO_FILETIME);
    }  
    
    /**
     * Copies file from one location to another.
     * 
     * @param string $sourceFilename
     * @param string $destinationFilename
     * @return boolean
     */
    public static function copyImage($sourceFilename, $destinationFilename)
    {
        // if file exists
        if(file_exists($destinationFilename))
        {
            return false;
        }

        $isUrl = (filter_var($sourceFilename, FILTER_VALIDATE_URL));

        if(!$isUrl && !file_exists($sourceFilename))
        {
            throw new FileManagerException('Source file does not exists: ' . $sourceFilename);
        }

        $destinationDir = dirname($destinationFilename);

        if(!is_dir($destinationDir))
        {
            $result = mkdir($destinationDir, 0777, true);

            if(!$result)
            {
                throw new FileManagerException('Destination directory is not writeable: ' . $destinationDir);
            }
        }

        copy($sourceFilename, $destinationFilename);
    }
  
    /**
     * Returns array of filenames matched to current order.
     * 
     * @param string $hotfolderDir
     * @param integer $orderId
     * @param array $statusArr
     * @return array
     */
    public static function findFilenamesToCopyHotfolder($hotfolderDir, $orderId,
            $statusArr, $currentStatus)
    {
        $pattern = '$'.implode('|', $statusArr).'$';

        foreach($statusArr as $rec)
        {
            $filenameSourceArr = glob($hotfolderDir.$rec.'/'.$orderId.'.*'); 
            $resultArr = array();
 
            foreach($filenameSourceArr as $key => $rec2)
            {
                $resultArr[$key] = array('source' => $rec2,
                                        'dest' => preg_replace($pattern, $currentStatus, $rec2));
            }
            
            if(!empty($resultArr))
            {
                return $resultArr;
            }     
        }
       
        return array();
    }

    /**
     * Get file's mime type
     *
     * @param string $filepath
     * @return string
     */
    public static function getMimeType($filepath)
    {
        $fi = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($fi, $filepath);
        finfo_close($fi);

        return $mime;
    }
}
