<?php

class MnumiRestServerClientException extends Exception
{
    /**
     * Method: doCreateClient() 
     */
    const E_CREATE_CLIENT = 'E_CREATE_CLIENT';
    
    /**
     * Method: doUpdateMyData() 
     */
    const E_CHANGE_CLIENT_NOT_EXISTS = 'E_CHANGE_CLIENT_NOT_EXISTS';
    
    /**
     * Method: doFavouriteOrder() 
     */
    const E_FAVOURITE_ORDER = 'E_FAVOURITE_ORDER';
    
    /**
     * Method: doGetSendCalculationContent() 
     */
    const E_GET_SEND_CALCULATION_CONTENT = 'E_GET_SEND_CALCULATION_CONTENT';
    
    /**
     * Method: doSendCalculation() 
     */
    const E_SEND_CALCULATION = 'E_SEND_CALCULATION';
    
    /**
     * Method: doGetClientData(), doUpdateClientData()
     */
    const E_EDIT_CLIENT_PERMISSIONS = 'E_EDIT_CLIENT_PERMISSIONS';
    
    /**
     * Method: doGetClientData()
     */
    const E_EDIT_CLIENT = 'E_EDIT_CLIENT';

    /**
     * Session is incorrect
     */
    const E_SESSION_INCORRECT = 'Incorrect or outdated session handle. User not logged in.';

}