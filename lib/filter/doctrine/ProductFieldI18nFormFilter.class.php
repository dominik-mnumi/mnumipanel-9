<?php

/**
 * ProductFieldI18n filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFieldI18nFormFilter extends BaseProductFieldI18nFormFilter
{
  public function configure()
  {
  }
}
