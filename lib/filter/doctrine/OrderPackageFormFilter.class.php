<?php

/**
 * OrderPackage filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderPackageFormFilter extends BaseOrderPackageFormFilter
{
  public function configure()
  {
      $packageStatusPaymentNameArray = array('', 'paid', 'non paid');
      $packageStatusNameArray = $this->getPackageStatusArr();

      $this->setWidgets(array(
          'carrier_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Carrier'), 'add_empty' => true)),
          'payment_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Payment'), 'add_empty' => true)),
          'payment_status'            => new sfWidgetFormSelect(array('choices' => $packageStatusPaymentNameArray, 'label' => 'Payment status')),
          'order_package_status_name' => new sfWidgetFormSelect(array('choices' => $packageStatusNameArray, 'label' => 'Package status')),
          'not_sent' => new sfWidgetFormInputCheckbox(array('label' => 'Not sent', 'value_attribute_value' => 1)),
          'sent_from_date' => new sfWidgetFormInput(array('label' => 'Sent from'), array('class' => 'datepicker')),
          'sent_to_date' => new sfWidgetFormInput(array('label' => 'Sent to'), array('class' => 'datepicker')),
      ));

      $this->setValidators(array(
          'carrier_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Carrier'), 'column' => 'id')),
          'payment_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Payment'), 'column' => 'id')),
          'payment_status'            => new sfValidatorChoice(array('choices' => array_keys($packageStatusPaymentNameArray))),
          'order_package_status_name' => new sfValidatorChoice(array('choices' => array_keys($packageStatusNameArray))),
          'sent_from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')),
          'sent_to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')),
          'not_sent' =>  new sfValidatorInteger(array('required' => false))
      ));

      $this->widgetSchema->setNameFormat('order_package_filters[%s]');
  }
  
  /**
   * Add custom query to payment_status column.
   *
   * @param Doctrine_Query $query
   * @param string $field
   * @param int $value
   */
  protected function addPaymentStatusColumnQuery(Doctrine_Query $query, $field, $value)
  {
      if($value == 1)
      {
          $query->andWhere('r.payment_status_name = ?', 'paid');
      }
      elseif($value == 2)
      {
          $query->andWhere('r.payment_status_name <> ?', 'paid');
      }
  }
  
  /**
   * Add custom query to order_package_status_name column.
   *
   * @param Doctrine_Query $query
   * @param string $field
   * @param int $value
   */
  protected function addOrderPackageStatusNameColumnQuery(Doctrine_Query $query, $field, $value)
  {
      if($value != '0')
      {
          $query->andWhere('r.order_package_status_name = ?', $value);
      }
  }
  
  /**
   * Returns package status in prepared array (for widget choice).
   * 
   * @return array 
   */
  protected function getPackageStatusArr()
  {
      $orderPackageColl = OrderPackageStatusTable::getInstance()->findAll();
      
      $orderPackageArr = array('');
      foreach($orderPackageColl as $rec)
      {
          $orderPackageArr[$rec->getName()] = $rec->getTitle();
      }    
          
      return $orderPackageArr;
  }

  /**
     * Add custom query sent_from_date.
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addSentFromDateColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('r.send_at > ?', $value);
        }
    }

    /**
     * Add custom query sent_to_date.
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addSentToDateColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('r.send_at < ?', $value);
        }
    }

    /**
     * Add custom query not_sent
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addNotSentColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('r.send_at IS NULL');
        }
    }
}