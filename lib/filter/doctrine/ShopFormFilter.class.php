<?php

/**
 * Shop filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ShopFormFilter extends BaseShopFormFilter
{
  public function configure()
  {
  }
}
