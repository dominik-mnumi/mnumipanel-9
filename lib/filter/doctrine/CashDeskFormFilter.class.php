<?php

/**
 * CashDesk filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CashDeskFormFilter extends BaseCashDeskFormFilter
{
    public function configure()
    {
        $typeOptArr = array('' => 'all', 'IN' => 'income', 'OUT' => 'outcome');
        $this->setWidgets(array(
            'payment_id' => new sfWidgetFormDoctrineChoice(
                    array('model' => $this->getRelatedModelName('Payment'),
                        'add_empty' => 'all')),
            'from_date' => new sfWidgetFormInput(
                    array('label' => ''),
                    array('class' => 'datepicker')),
            'to_date' => new sfWidgetFormInput(
                    array(),
                    array('class' => 'datepicker')),
            'type' => new sfWidgetFormSelect(
                    array('choices' => $typeOptArr,
                        'label' => 'Type')),
        ));

        $this->setValidators(array(
            'payment_id' => new sfValidatorDoctrineChoice(
                    array('required' => false,
                        'model' => $this->getRelatedModelName('Payment'),
                        'column' => 'id')),
            'from_date' => new sfValidatorDateTime(
                    array('required' => false,
                        'datetime_output' => 'Y-m-d 00:00:00')),
            'to_date' => new sfValidatorDateTime(
                    array('required' => false,
                        'datetime_output' => 'Y-m-d 23:59:59')),
            'type' => new sfValidatorChoice(
                    array('choices' => array_keys($typeOptArr),
                        'required' => false)),
        ));
        $this->widgetSchema->setNameFormat('cashDesk_filters[%s]');
    }

    /**
     * Below methods are used by symfony filter mechanisms which automatically 
     * adds query sections to final filter query (based on sfFormFilterDoctrine.class.php).
     */

    /**
     * Add custom query from_date.
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addFromDateColumnQuery(Doctrine_Query $query, $field,
            $value)
    {
        if($value)
        {
            $query->andWhere('created_at > ?', $value);
        }
    }

    /**
     * Add custom query to_date.
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addToDateColumnQuery(Doctrine_Query $query, $field,
            $value)
    {
        if($value)
        {
            $query->andWhere('created_at < ?', $value);
        }
    }

    /**
     * Add custom query invoice preliminary type. 
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addTypeColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('type = ?', $value);
        }
    }

}
