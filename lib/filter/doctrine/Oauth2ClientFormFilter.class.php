<?php

/**
 * Oauth2Client filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class Oauth2ClientFormFilter extends BaseOauth2ClientFormFilter
{
  public function configure()
  {
  }
}
