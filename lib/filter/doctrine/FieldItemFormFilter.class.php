<?php

/**
 * FieldItem filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FieldItemFormFilter extends BaseFieldItemFormFilter
{
    public function configure()
    {
        $depositoryAlerts = array(
            0                   => 'all',
            'alarm-level'       => 'alarm level',
            'near-alarm-level'  => 'near alarm level',
            'no-alarm'          => 'lack of alarm set',
        );

        $this->setWidgets(array(
            'name' => new sfWidgetFormInput(array(
            )),
            'availability_from' => new sfWidgetFormInput(array(
                'label' => 'Minimum stock level',
            ), array()),
            'availability_to' => new sfWidgetFormInput(array(
                'label' => 'Maximum stock level'
            ), array()),
            'availability_alert' => new sfWidgetFormSelect(array(
                'label' => 'Stock status',
                'choices' => $depositoryAlerts
            )),
        ));

        $this->setValidators(array(
            'name' => new sfValidatorPass(array(
                'required' => false
            )),
            'availability_from' => new sfValidatorNumberExtended(array(
                'required' => false,
            )),
            'availability_to' => new sfValidatorNumberExtended(array(
                'required' => false,
            )),
            'availability_alert' => new sfValidatorChoice(array(
                'choices' => array_keys($depositoryAlerts),
                'required' => false
            )),
        ));
        $this->widgetSchema->setNameFormat('fieldItem_filters[%s]');
    }


    /**
     * Initial view has results already filtered by getDepositoryFieldItemsQuery
     * in complexTablesActions::settingsList.
     * Using filter overwrites query, so we have to set it again to exclude
     * fields other than OTHER.
     */
    protected function doBuildQuery(array $values)
    {
        $onlyDepositoryFieldItemsQuery = FieldItemTable::getInstance()->getDepositoryFieldItemsQuery();
        $this->setQuery($onlyDepositoryFieldItemsQuery);

        return parent::doBuildQuery($values);
    }

    /**
     * When name is set, add it to custom query.
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @param string $value
     */
    protected function addNameColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('name LIKE ?', '%'.$value.'%');
        }
    }

    /**
     * When availability_from is set, add it to custom query.
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @param float $value
     */
    protected function addAvailabilityFromColumnQuery(Doctrine_Query $query, $field, $value)
    {
        $query->andWhere('availability >= ?', $value);
    }

    /**
     * When availability_to is set, add it to custom query.
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @param float $value
     */
    protected function addAvailabilityToColumnQuery(Doctrine_Query $query, $field, $value)
    {
        $query->andWhere('availability <= ?', $value);
    }

    /**
     * Add custom query depository_alert.
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @param string $value
     */
    protected function addAvailabilityAlertColumnQuery(Doctrine_Query $query, $field, $value)
    {
        switch($value) {
            case 'alarm-level':
                $query->andWhere('availability <= availability_alert');
                break;
            case 'near-alarm-level':
                $query->andWhere('availability <= availability_alert * 1.10');
                break;
            case 'no-alarm':
                $query->andWhere('availability_alert IS NULL');
                break;
            case 0:
            default:
                break;
        }
    }
}
