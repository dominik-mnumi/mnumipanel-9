<?php

/**
 * CarrierReportType filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CarrierReportTypeFormFilter extends BaseCarrierReportTypeFormFilter
{
  public function configure()
  {
  }
}
