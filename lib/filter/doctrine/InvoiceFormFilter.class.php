<?php

/**
 * Invoice filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceFormFilter extends BaseInvoiceFormFilter
{
    public function configure()
    {
        $invoiceStatusNameArray = array('all', 'paid invoices', 'unpaid invoices', 'canceled invoices');
        $invoiceTypeArray = array(0 => 'all',
            InvoiceTypeTable::$INVOICE => 'Invoice',
            InvoiceTypeTable::$PRELIMINARY_INVOICE => 'Preliminary invoice', 
            InvoiceTypeTable::$CORRECTION_INVOICE => 'Invoice correction',
            InvoiceTypeTable::$RECEIPT => 'Receipt');
        
        $this->setWidgets(array(
            'client_name' => new sfWidgetFormFilterInput(array('with_empty' => false)),
            'payment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Payment'), 'add_empty' => true)),
            'invoice_status_name' => new sfWidgetFormSelect(array('choices' => $invoiceStatusNameArray, 'label' => 'Invoice status')),
            'from_date' => new sfWidgetFormInput(array('label' => ''), array('class' => 'datepicker')),
            'to_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
            'type' => new sfWidgetFormSelect(array('choices' => $invoiceTypeArray, 'label' => 'Type')),
        ));

        $this->setValidators(array(
            'client_name' => new sfValidatorPass(array('required' => false)),
            'payment_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Payment'), 'column' => 'id')),
            'invoice_status_name' => new sfValidatorChoice(array('choices' => array_keys($invoiceStatusNameArray))),
            'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')),
            'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')),
            'type' => new sfValidatorChoice(array('choices' => array_keys($invoiceTypeArray))),
        ));
        $this->widgetSchema->setNameFormat('invoice_filters[%s]');
    }

    /**
     * Below methods are used by symfony filter mechanisms which automatically 
     * adds query sections to final filter query (based on sfFormFilterDoctrine.class.php).
     */
    
    /**
     * Add custom query to invoice_status_name column.
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @param boolean $is_payed
     */
    protected function addInvoiceStatusNameColumnQuery(Doctrine_Query $query, $field, $value)
    {
        $invoiceStatuses = array(1 => InvoiceStatusTable::$paid, 2 => InvoiceStatusTable::$unpaid, 3 => InvoiceStatusTable::$canceled);
        
        if(array_key_exists($value, $invoiceStatuses))
        {
            $query->andWhere('i.invoice_status_name = ?', $invoiceStatuses[$value]);
        }
    }

    /**
     * Add custom query from_date.
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addFromDateColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('i.created_at > ?', $value);
        }
    }

    /**
     * Add custom query to_date.
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addToDateColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('i.created_at < ?', $value);
        }
    }
    
    /**
     * Add custom query invoice preliminary type. 
     * 
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    protected function addTypeColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->andWhere('i.invoice_type_name = ?', $value);
        }
    }
}
