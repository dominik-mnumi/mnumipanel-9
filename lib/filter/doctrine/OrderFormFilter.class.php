<?php

/**
 * Order filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderFormFilter extends BaseOrderFormFilter
{

    protected $dateRange;

    public function configure()
    {
        $orderStatusNameArray[''] = '';
        $orderStatusNameArray += OrderStatusTable::getInstance()->getStatusArr();

        $ranges = array(
            'all' => 'all days',
            'cm' => 'current month',
            'pm' => 'previous month',
            'cq' => 'current quarter',
            'pq' => 'previous quarter',
            'cy' => 'current year',
            'py' => 'previous year',
            'custom' => 'exact date range',
        );

        $this->setWidgets(array(
            'range' => new sfWidgetFormChoice(array(
                'choices' => $ranges,
                'label' => 'Date of placing the order',
            )),
            'date_range' => new sfWidgetFormDateRange(array(
                'from_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
                'to_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
                'label' => 'Date of placing the order',
            ), array('class' => 'datepicker')),
            'shop_name' => new sfWidgetFormDoctrineChoice(array(
                'model' => $this->getRelatedModelName('Shop'),
                'add_empty' => 'all',
                'label' => 'Source',
                'query' => ShopTable::getInstance()->createQuery()
                    ->andWhere('type = ?', 'shop'),
            )),
            'order_status_name' => new sfWidgetFormChoice(array('choices' => $orderStatusNameArray)),
            'product_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => true)),
            'trader' => new sfWidgetFormDoctrineChoice(array('model' => 'sfGuardUser', 'label' => 'Trader', 'add_empty' => true,
                'query' => Doctrine::getTable('sfGuardUser')->getClientTraderQuery())),
            'to_invoice' => new sfWidgetFormInputCheckbox(
                array('label' => 'To invoice')),
        ));
        
        $this->setValidators(array(
            'shop_name' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Shop'), 'column' => 'name')),
            'order_status_name' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($orderStatusNameArray))),
            'product_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Product'), 'column' => 'id')),
            'trader' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'sfGuardUser', 'column' => 'id')),
            'range' => new sfValidatorChoice(array('choices' => array_keys($ranges))),
            'date_range' => new sfValidatorDateRange(array(
                'from_date' => new sfValidatorDate(array('required' => false, 'datetime_output' => 'Y-m-d')),
                'to_date' => new sfValidatorDate(array('required' => false, 'datetime_output' => 'Y-m-d')),
            )),
            'to_invoice' => new sfValidatorPass()
        ));
        $this->widgetSchema->setNameFormat('order_filters[%s]');

    }

    protected function doBuildQuery(array $values)
    {  
        /* PAN-1081
         * Set base filter query depends of order status filter.
         * By default deleted and draft orders aren't shown on list
         * with the exception of selecting the order status filter
         */
        $onlyActiveQuery = isset($values['order_status_name']) ? null : OrderTable::getInstance()->getActiveOrdersQuery();
        $this->setQuery(OrderTable::getInstance()->getLatestQuery($onlyActiveQuery));
        
        /* PAN-938 We have to get "to" dates granularity in seconds althought
         * widgets allows setting only days. This allows us to show all created
         * orders to current moment. Otherwise orders created a moment ago won't be
         * shown because of database granularity in seconds.
         */
        switch ($values['range']) {
            case 'cm': // current month
                $values['date_range'] = array(
                    'from' => date('Y-m-d H:i:s', mktime(0, 0, 0, date("m"), 1,   date("Y"))),
                    'to' => date('Y-m-d H:i:s'),
                );
                break;
            case 'pm': // previous month
                $values['date_range'] = array(
                    'from' => date('Y-m-d H:i:s', mktime(0, 0, 0, date("m")-1, 1,   date("Y"))),
                    'to' => date('Y-m-d H:i:s', mktime(0, 0, 0, date("m"), 0,   date("Y"))),
                );
                break;
            case 'cq': // current quarter
                $startMonth = date('m') - ((date('m')-1) % 3 );
                $values['date_range'] = array(
                    'from' => date('Y-m-d H:i:s', mktime(0, 0, 0, $startMonth, 1,   date("Y"))),
                    'to' => date('Y-m-d H:i:s', mktime(0, 0, 0, $startMonth+3, 0,   date("Y"))),
                );
                break;
            case 'pq': // previous quarter
                $startMonth = date('m') - 3 - ((date('m')-1) % 3 );
                $values['date_range'] = array(
                    'from' => date('Y-m-d H:i:s', mktime(0, 0, 0, $startMonth, 1,   date("Y"))),
                    'to' => date('Y-m-d H:i:s', mktime(0, 0, 0, $startMonth+3, 0,   date("Y"))),
                );
                break;
            case 'cy': // current year
                $values['date_range'] = array(
                    'from' => date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1,   date("Y"))),
                    'to' => date('Y-m-d H:i:s'),
                );
                $this->dateRange = date("Y");
                break;
            case 'py': // previous year
                $values['date_range'] = array(
                    'from' => date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1,   date("Y")-1)),
                    'to' => date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 0,   date("Y"))),
                );
                break;
            case 'all': // all days
                $youngestOrder = OrderTable::getInstance()->createQuery()
                                ->where('accepted_at IS NOT NULL')
                                ->orderBy('accepted_at ASC')
                                ->limit(1)
                                ->fetchOne();
                $values['date_range'] = array(
                    'from' => $youngestOrder ? $youngestOrder->getAcceptedAt('Y-m-d') : date('Y-m-d H:i:s'),
                    'to' => date('Y-m-d H:i:s'),
                );
                break;
            case 'custom': // custom date range
                /* Reformat "to" date, to display orders from current day too
                 * (explanation in comment at the start of this method).
                 */
                $to = getdate(strtotime($values['date_range']['to']));
                $values['date_range'] = array(
                    'from' => $values['date_range']['from'],
                    'to' => date('Y-m-d H:i:s', mktime(23, 59, 59, $to['mon'], $to['mday'], $to['year']))
                );
                break;
        }
        $this->dateRange =
            date('Y-m-d H:i:s', strtotime($values['date_range']['from'])) .
            ' - ' .
            date('Y-m-d H:i:s', strtotime($values['date_range']['to']));

        return parent::doBuildQuery($values);
    }

    /**
     * Add custom query from_date
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    public function addDateRangeColumnQuery(Doctrine_Query $query, $field, $value)
    {
        $orderStatus = $this->getValue('order_status_name');

        // get correct field name for filter based on status type (frontend or backend)
        $fieldName = (in_array($orderStatus, OrderStatusTable::$statusFrontend))
            ? 'created_at'
            : 'accepted_at';

        if(array_key_exists('from', $value) && $value['from'] != null)
        {
            $query->andWhere($fieldName . ' >= ?', $value['from']);
        }

        if(array_key_exists('to', $value) && $value['to'] != null)
        {
            $query->andWhere($fieldName . ' <= ?', $value['to']);
        }
    }

    /**
     * Add custom query from_date
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    public function addTraderColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->leftJoin('Order.OrderTraders ot ON Order.id = ot.order_id')
                    ->andWhere('ot.user_id = ?', $value);
        }
    }
    
    /**
     * Add custom query to_invoice.
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    public function addToInvoiceColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if($value)
        {
            $query->innerJoin('Order.OrderPackage op')
                    ->andWhere('op.to_book = ?', true)
                    ->andWhere('op.booked_at IS NULL');
        }
    }

    public function getDateRange()
    {
        return $this->dateRange;
    }

}
