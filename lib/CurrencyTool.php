<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CurrencyTool
 *
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */

class CurrencyTool
{
    /**
     * Formatting price by getSettingPricePrecision
     *
     * @param $price
     * @return string
     */
    public static function formatPrice($price)
    {
        $pricesion = mnumicoreConfiguration::getSettingPricePrecision();

        $floatPrice = round($price, $pricesion);

        return sprintf("%01." . $pricesion . "f", $floatPrice);
    }
}