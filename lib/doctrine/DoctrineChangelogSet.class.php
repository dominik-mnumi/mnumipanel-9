<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ChangelogSet class
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
abstract class DoctrineChangelogSet
{
    //available change types
    private $changeTypes = array('added', 'edited', 'deleted');
    
    // editor (sfGuardUser)
    private $editor;
    
    // edit date
    private $editedAt;
    
    // change type
    private $changeType;
    
    // changes list
    public $changes = array();
    
    /**
     * Constructor
     * 
     * @param $editor - sfGuard user instance
     * @param $editedAt - edit date
     * @param $changeType - type of change
     * 
     */
    function __construct(sfGuardUser $editor, $editedAt, $changeType)
    {
        if(!in_array($changeType, $this->changeTypes))
        {
            throw new Exception('Unknown change type');
        }
        
        $this->editor = $editor;
        $this->editedAt = $editedAt;
        $this->changeType = $changeType;
    }
    
    /**
     * Add new changes
     *
     * @param $fieldName - Name of field
     * @param $previousValue - previous value
     * @param $nextValue - next value
     *
     */
    public function add($fieldName, $previousValue, $nextValue)
    {
         $this->changes[] = array('name' => $fieldName, 'previousValue' => $previousValue, 'nextValue' => $nextValue);
    }
    
    /**
     * Fetch all results
     *
     * @return array
     */
    public function fetchAll()
    {
        return $this->changes;
    }
    
    /**
     * Returns editor object
     *
     * @return Doctrine_record sfGuardUser
     */
    public function getEditor()
    {
        return $this->editor;
    }
    
    /**
     * Returns edited date
     *
     * @param $format - date format
     * @return string
     */
    public function getEditedAt($format = null)
    {
        if($format)
        {
            return date('Y-m-d H:i:s', $this->editedAt);
        }
        return $this->editedAt;
    }
    
    /**
     * Returns change type
     *
     * @return string
     */
    public function getChangeType()
    {
        return $this->changeType;
    }
    
    
}