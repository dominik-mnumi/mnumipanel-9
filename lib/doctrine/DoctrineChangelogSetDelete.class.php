<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ChangelogSet class
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class DoctrineChangelogSetDelete extends DoctrineChangelogSet
{
    /**
     * Constructor
     *
     * @param $editor - sfGuard user instance
     * @param $editedAt - edit date
     *
     */
    function __construct(sfGuardUser $editor, $editedAt)
    {
        parent::__construct($editor, $editedAt, 'deleted');
    }
    
    /**
     * Add changes values
     */
    public function addValue($fieldName, $previousValue)
    {
        $this->add($fieldName, $previousValue, null);
    }
}