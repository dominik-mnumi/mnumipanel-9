<?php
/*
 * This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

class Doctrine_Template_Changelogable extends Doctrine_Template
{
    /**
     * Array of Changelog Options
     *
     * @var array
     */
    protected $_options = array(
        'columns' => array(),
        'relations' => array(
        )
    );
    
    protected $_plugin;
    
    /**
     * Get differences for object
     *
     * @var array
     */
    public function getDifferences()
    {        
        $changelog = new DoctrineChangelog($this->_options);
        return $changelog->getDifferences($this->getInvoker());
    }
}

