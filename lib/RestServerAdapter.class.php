<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * REST Server (API)
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class RestServerAdapter
{
    const E_MSG_SUCCESS = 'E_MSG_SUCCESS';
    
    /**
     * Get available delivery types
     * 
     * @param string $webapi_key
     * @return array
     */
    public function doGetDeliveryType($webapi_key, $delivery_id = '')
    {
        $this->checkWebApi($webapi_key);
        $deliveryTable = DeliveryTable::getInstance();

        if($delivery_id > 0)
        {
            $rows = $deliveryTable->findByActiveAndId(1, (int)$delivery_id);
        }
        else
        {
            $rows = $deliveryTable->findByActive(1);
        }

        if($rows->count() > 0)
        {
            return array('types' => $rows->toArray());
        }

        throw new Exception('Unable to get delivery types.');
    }

    public function doPricelist($webapi_key)
    {
        $this->checkWebApi($webapi_key);
        return array(
            'results' => PricelistTable::getInstance()->findAll()->toArray(),
        );
    }
    
    public function doPrintlabelQueue($connection_key, $print_id = null, $queue_name = 'pending')
    {
    	$this->checkConnectionKey($connection_key);
    	
    	// if we update
    	if($this->allowedMethod(array('POST')))
    	{
    		if($print_id == null)
    		{
    			throw new Exception('Print ID is not defined.');
    		}
    		
    		PrintlabelQueueTable::getInstance()->setAsPrinted($print_id);
    		
    		return array('results' => 1);
    	}
    	
    	$queue = PrintlabelQueueTable::getInstance()->loadQueue($queue_name);
    	
    	$results = array();
    	foreach($queue as $item)
    	{
    		$results[] = $item->asArray();
    	}
    	
    	return array('results' => $results);
    }

    public function doCategory($webapi_key, $parent = null)
    {
        $this->checkWebApi($webapi_key);

        $obj = ($parent != null)
            ? CategoryTable::getInstance()->fetchOneByIdOrSlug($parent)
            : CategoryTable::getInstance()->fetchRoot();

        if(!$obj)
        {
            throw new Exception('Selected wrong category');
        }

        $results = array();

        $node = $obj->getNode();

        if($node->hasChildren())
        {
            $parentId = $obj->getId();

            foreach($node->getChildren() as $row)
            {
                // get number of products for current category
                $productNb = $row->getProductsCount(true, $webapi_key);

                // Show category only when there is at least one product
                if ($productNb > 0) {
                    // gets main product data of current category
                    $mainProduct = ($productNb == 1) ? $row->getMainProduct($webapi_key) : null;

                    $results[$row->getId()] = array(
                        'id' => $row->getId(),
                        'parent_id' => $parentId,
                        'name' => strip_tags($row->getName()),
                        'link' => $row->getSlug(),
                        'photo' => $row->getPhoto(),
                        'updated_at' => $row->getUpdatedAt(),
                        'product_nb' => $productNb,
                        'mainProductData' => ($mainProduct != null) ? $mainProduct->toArray() : null,
                    );
                }
            }
        }

        return array(
            'results' => $results,
            'info' => array(
                'id' => $obj->id,
                'name' => $obj->name,
                'slug' => $obj->slug
            )
        );
    }
    
    /**
     * Returns list of all active categories.
     * 
     * @param string $webapi_key
     * @return array
     */
    public function doGetAllCategories($webapi_key)
    {
        $this->checkWebApi($webapi_key);

        $coll = CategoryTable::getInstance()->getActiveQuery()->execute();

        $resultArr = array();
   
        foreach($coll as $obj)
        {
            $resultArr[$obj->getId()] = array(
                'id' => $obj->getId(),
                'parent_id' => $obj->getId() ?: 0,
                'name' => strip_tags($obj->getName()),
                'link' => $obj->getSlug(),
                'photo' => $obj->getPhoto(),
                'updated_at' => $obj->getUpdatedAt(),
                'product_nb' => $obj->getProductsCount(true, $webapi_key),
            );    
        }

        return array(
            'results' => $resultArr
        );
    }

    /**
     * Returns list of all active products.
     * 
     * @param string $webapi_key
     * @return array
     */
    public function doGetAllProducts($webapi_key)
    {
        $this->checkWebApi($webapi_key);

        $coll = ProductTable::getInstance()->getActiveQuery()->execute();

        $resultArr = array();
   
        foreach($coll as $obj)
        {
            $resultArr[$obj->getId()] = array(
                'name' => strip_tags($obj->getName()),
                'description' => strip_tags($obj->getDescription()),
                'photo' => $obj->getPhoto(),
                'category_id' => $obj->getCategoryId(),
                'created_at' => $obj->getCreatedAt(),
                'updated_at' => $obj->getUpdatedAt(),
                'slug' => $obj->getSlug(),
                'external_link' => $obj->getExternalLink()
            );
        }

        return array(
            'results' => $resultArr
        );
    }

    /**
     * Get product by url (part for given product).
     *
     * @param string $webapi_key
     * @param string $product_link - slug
     * @param boolean $extended - get all field_items if is set
     * @param null $pricelistId
     * @param null $withDeleted
     * @return array array('fields'=> ..., 'product' => array (...))
     */
    public function doGetOrderFormField($webapi_key, $product_link, $extended = false, $pricelistId = null, $withDeleted = null)
    {
        $this->checkAllowedMethod('get');
        $this->checkWebApi($webapi_key);
        
        if(!OrderTable::getInstance()->canCreate())
        {
            return array(
                'fields' => null,
                'product' => null,
                'reject' => 'license',
            ); 
        }

        if($withDeleted)
        {
            $product = ProductTable::getInstance()->findOneBySlugWithDeleted($product_link);
        }
        else
        {
            $product = ProductTable::getInstance()->findActiveBySlug($product_link);
        }

        if(!$product)
        {
            return array(
                'fields' => null,
                'product' => null,
                'reject' => null,
            );
        }

        $fields = $product->getOrderFields();

        $isAvailableInStock = true;
        $fieldItemTable = FieldItemTable::getInstance();
        $emptyFieldId = $fieldItemTable->getEmptyField()->getId();// Override for missing: PAN-1423

        foreach ($fields as $fieldId => &$field) {
            // Every field which has field items or every OTHER field
            if ((count($field['field_param']) > 0) ||
                ($field['field_name'] == 'OTHER')) {

                /*
                 * Remove from Fieldset every FieldItem that is used in stock,
                 * and is not available.
                 */
                $fieldItems = $fieldItemTable->findByIds(array_keys($field['field_param']));
                foreach($fieldItems as $fieldItem) {
                    if ($fieldItem->isUsedInStock() && !$fieldItem->isAvailableInStock()) {
                        unset($fields[$fieldId]['field_param'][$fieldItem->getId()]);

                    }

                    // Override for missing: PAN-1423
                    if ($field['field_required'] &&
                        array_key_exists($emptyFieldId, $fields[$fieldId]['field_param'])) {
                        unset($fields[$fieldId]['field_param'][$emptyFieldId]);
                    }
                }
                /* If field is hidden in Shop, and its default value does not exist
                 * it means that it is unavailable in stock and has been removed in
                 * code above. Make entire product unavailable as this parameter
                 * cannot be changed to another one.
                 */
                if (($field['visible'] == false) &&
                    (!array_key_exists($field['field_def_val'], $field['field_param']))) {
                    $isAvailableInStock = false;
                }
                /* If user has empty options list, and he can't set any value for
                 * field, set product as unavailable.
                 */
                else if (count($field['field_param']) < 1) {
                    $isAvailableInStock = false;
                }
            }
        }

        // wizard section
        // product wizards
        $productWizardArray = $product->getProductActiveSortedWizards()->toArray();

        return array(
            'fields' => $fields,
            'product' => $product->toArray(),
            'wizards' => $productWizardArray,
            'reject' => null,
            'isAvailableInStock' => (int)$isAvailableInStock,
        );
    }

    /**
     * Gets available payment methods.
     * 
     * @param string $webapi_key 
     * @param integer $delivery_id optional if passed method get available payments for given delivery type
     * @return 
     */
    public function doGetDeliveryPayments($webapi_key, $delivery_id = '')
    {
        $this->checkAllowedMethod('post');
        $this->checkWebApi($webapi_key);

        if(empty($delivery_id))
        {
            $coll = PaymentTable::getInstance()
                    ->findByActive(1);

            if($coll->count() > 0)
            {
                return array('payments' => $coll->toArray());
            }
        }
        else
        {
            $dpColl = DeliveryPaymentTable::getInstance()
                        ->findByDeliveryId($delivery_id);

            $res = array();
            foreach($dpColl as $payment)
            {
                $res[] = array_merge($payment->toArray(), $payment->Payment->toArray());
            }

            $deliveryTable = DeliveryTable::getInstance();
            $row = $deliveryTable->findById($delivery_id);

            $delivery = array();
            ($row->count() == 1) ? $delivery = $row->toArray() : $delivery = false;

            return array('payments' => $res, 'delivery' => $delivery);
        }

        throw new Exception('Unable to get payments methods.');
    }

    /**
     * Check if payment fits to delivery (is linked in admin).
     * 
     * @param string $webapi_key
     * @param integer $payment_id
     * @param string $session_handle - parameter DEPRECATED - not used
     */
    public function doCheckPayment($webapi_key, $paymentId, $session_handle = null, $deliveryId = null)
    {
        $this->checkAllowedMethod('post');
        $this->checkWebApi($webapi_key);

        // gets payment
        $payment = PaymentTable::getInstance()->find($paymentId);

        if(!$payment instanceof Payment)
        {
            throw new Exception('Given payment type does not exists.');
        }

        if(!$payment->isRelatedWithCarrier($deliveryId))
        {
            return array('paymentDoesNotMatch' => '1');
        }

        return array('payment' => $payment->toArray());
    }

    /**
     * Gets all products from given category.
     *
     * @param string $webapi_key
     * @param string $category_link
     * @throws Exception
     * @exception Exception if category link is incorrect
     * @return array(
     *      'results' => array( ... ),
     *      'category' => array( ... ),
     * )
     */
    public function doProductList($webapi_key, $category_link)
    {
        $this->checkAllowedMethod('get');
        $this->checkWebApi($webapi_key);

        if(is_numeric($category_link))
        {
            $category = CategoryTable::getInstance()->find($category_link);
        }
        elseif(is_string($category_link))
        {
            $category = CategoryTable::getInstance()->findOneBySlug($category_link);
        }

        if(!$category || (!$category->active))
        {
            throw new Exception('Incorrect category link. Get category list by doCategory method. ');
        }

        $category_result = array(
            'name' => strip_tags($category->name),
            'slug' => $category->slug,
        );

        $products = $category->getAllActiveProducts(true, $webapi_key);

        $results = array();
        foreach($products as $product)
        {
            $results[] = array(
                'name' => strip_tags($product->name),
                'description' => strip_tags($product->description),
                'photo' => $product->photo,
                'category_id' => $product->category_id,
                'created_at' => $product->created_at,
                'updated_at' => $product->updated_at,
                'slug' => $product->slug,
                'external_link' => $product->external_link
            );
        }

        return array(
            'results' => $results,
            'category' => $category_result
        );
    }

    /**
     * Gets path category od current category
     *
     * @param $webapi_key
     * @param string $category
     * @return array
     */
    public function doGetPathCategory($webapi_key, $category)
    {
        if(is_numeric($category))
        {
            $catObj = CategoryTable::getInstance()->find($category);
        }
        elseif(is_string($category))
        {
            $catObj = CategoryTable::getInstance()->findOneBySlug($category);
        }

        $path = $catObj->getNode()->getAncestors();
        $path[] = $catObj;
        unset($path[0]);

        return array(
            'results' => $path->toArray()
        );
    }

    /**
     * Get calculation list
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @param string $product_name
     * @param array $fields
     * @throws Exception
     * @return array
     */
    public function doCalculation($webapi_key, $session_handle, $product_name = null, $fields = array())
    {
        //        $this->checkAllowedMethod('post');
        $this->checkWebApi($webapi_key);

        $product = ProductTable::getInstance()->findOneBySlug($product_name);

        if(!$product)
        {
            throw new Exception('Could not find product: ' . $product_name);
        }

        $order = new Order();
        $order->setProduct($product);

        $result = array();

        $chooseQuantity = $fields['QUANTITY']['value'];
        $staticQuantityOptions = $product->getStaticQuantityArray();
        if(!in_array($chooseQuantity, $staticQuantityOptions))
        {
            array_unshift($staticQuantityOptions, $chooseQuantity);
        }

        $current = null;

        $userSession = $this->checkSessionHandle($session_handle);
        $pricelistId = ($userSession) ? $userSession->getDefaultClientPricelist()->getId() : null;

        foreach($staticQuantityOptions as $quantity)
        {
            $fields['QUANTITY'] = array(
                'name'  => 'QUANTITY',
                'value' => $quantity
            );

            $calculation = $order->calculate($fields, $pricelistId);

            $isCurrent = (bool) ($chooseQuantity == $quantity);

            $result[] = array(
                'quantity' => $calculation['quantity']['value'],
                'summaryPriceNet' => $calculation['summaryPriceNet'],
                'summaryPriceGross' => $calculation['summaryPriceGross'],
                'current' => $isCurrent,
            );

            if ($isCurrent)  {
                $current = $calculation;
            }
        }

        $current['calculateAsCard'] = $order->hasCalculationAsCard(
            FieldItemTable::getInstance()->find($fields['SIDES']['value'])
        ) ? '1' : '0';

        return array('result' => array(
            'items' => $result,
            'current' => $current,
        ));
    }


    /**
     * Returns calculated price based on sent and default values.
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @param string|null $product_name
     * @param array $fields
     * @param integer|null $order_id
     * @return array array('results' => array('price' => ...,
     *                                        'price_tax' => ...,
     *                                        'price_items' => array('name' => ...,
     *                                                               'price' => ...),
     *                                        'name' => ...,
     *                                        'price' => ...))
     * @throws Exception
     */
    public function doCheckNewOrder($webapi_key, $session_handle,
            $product_name = null, $fields = array(), $order_id = null)
    {
//        $this->checkAllowedMethod('post');
        $this->checkWebApi($webapi_key);
        $userSession = $this->checkSessionHandle($session_handle);
        $results = array();

        if($order_id != null && $order_id != 0)
        {
            /** @var Order $order */
            $order = OrderTable::getInstance()->find($order_id);

            if(!$order) {
                throw new Exception('Could not load order id: ' . $order_id);
            }

            $results['tradersPrice'] = $order->getTradersAmount();
            $results['priceBlock'] = $order->getPriceBlock();

        }
        elseif($product_name != '')
        {
            $product = ProductTable::getInstance()->findOneBySlug($product_name);

            if(!$product)
            {
                throw new Exception('Could not find product: ' . $product_name);
            }

            $order = new Order();
            $order->setProduct($product);

            if($userSession) {
                $order->setClient($userSession->getSfGuardUser()->getDefaultClient());
            }
        }
        else
        {
            throw new Exception('One of fields: "product_name" or "order_id" is required.');
        }

        $pricelistId = ($userSession) ? $userSession->getDefaultClientPricelist()->getId() : null;

        $calculation = $this->calculate($order, $fields, $pricelistId);

        $results = array_merge($calculation, $results);

        return array('result' => $results);
    }

    /**
     * Creat calculation,
     * if order has blocked price, prices of items are merged with custom prices,
     * if not, standard order's calculation is called.
     *
     * @param Order $order
     * @param array $fields
     * @param int|null $pricelistId
     * @return array
     *
     * @deprecated see: PAN-1301
     */
    protected function calculate($order, $fields, $pricelistId) {

        $calculation = $order->calculate($fields, $pricelistId);

        if($order->getPriceBlock()) {

            $context = sfContext::getInstance();
            $decorator = new CalculationDecorator($context->getI18N());
            $priceItems = $decorator->getView($calculation);
            $priceItems = $order->mergeCalculatedFriendlyFormatWithCustomPrice($priceItems);

            $calculation['priceItems'] = $this->prepareCalculationPriceItems($priceItems);

            $calculation['summaryPriceNet'] = $order->getTotalAmount();
            $calculation['summaryPriceGross'] = $order->getTotalAmountGross();

        }

        return $calculation;
    }

    /**
     * Prepare calculation's price items array for shop's allowed format
     *
     * @param array $priceItems
     * @return array
     *
     * @deprecated see: PAN-1301
     */
    public function prepareCalculationPriceItems($priceItems) {

        $preparedPriceItems = array();

        foreach($priceItems as $item) {

            if(preg_match('/^\[([A-Z]+)\]/', $item['id'], $match)) {
                switch($match[1]) {
                    case 'MATERIAL':
                        $preparedPriceItems['MATERIAL'] = $item;
                        break;
                    case 'PRINT':
                        $preparedPriceItems['PRINT'] = $item;
                        break;
                    case 'OTHER':
                        if(isset($preparedPriceItems['OTHER']))
                        {
                            $preparedPriceItems['OTHER'][] = $item;
                        }
                        else {
                            $preparedPriceItems['OTHER'] = array($item);
                        }
                        break;
                }
            }
        }

        return $preparedPriceItems;
    }

    /**
     * Get array of available carriers
     * 
     * @param string $webapi_key
     * @return array
     */
    public function doCarriers($webapi_key)
    {
        $this->checkWebApi($webapi_key);
        return array(
            'results' => CarrierTable::getInstance()->getActiveCarriers(),
        );
    }
    
    /**
     * Gets array of all available client carriers.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param integer $payment_id
     * @return array
     */
    public function doClientCarriers($webapi_key, $session_handle, $payment_id = null)
    {
        $this->checkWebApi($webapi_key);

        // gets user session
        $userSession = $this->checkSessionHandle($session_handle);

        // if user is not logged in
        if(!$userSession->isUserLoggedIn())
        {
            throw new Exception("User is not logged in.");
        }

        return array(
            'results' => $userSession->getSfGuardUser()
                ->getDefaultClient()
                ->getAvailableCarriers($payment_id)
                ->toArray()
        );
    }

    /**
     * Get carrier details
     *
     * @param string $webapi_key
     * @param integer $carrierId
     */
    public function doGetCarrier($webapi_key, $carrierId)
    {
        $this->checkWebApi($webapi_key);
        $resultObj = CarrierTable::getInstance()->find($carrierId);

        if($resultObj)
        {
            return array('results' => $resultObj->toArray());
        }

        throw new Exception('Given carrier does not exists.');
    }

    /**
     * Gets array of filtered payment.
     *
     * @param string $webapi_key
     * @param integer $sessionHandle
     * @param integer $webapi_key
     * @param integer $pricelistId (@deprecated in v3.x)
     * @param integer $deliveryId (@deprecated in v3.x)
     * @return array
     */
    public function doGetPaymentFiltered($webapi_key, $pricelistId = null, $forOffice = null, $requiredCreditLimit = null, $deliveryId = null)
    {
        $this->checkWebApi($webapi_key);

        return array(
            'results' => PaymentTable::getInstance()->getFilteredPayment($pricelistId, $forOffice, $requiredCreditLimit, $deliveryId)
        );
    }

    /**
     * Gets array of all available client payments.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param integer $carrier_id
     * @return array
     */
    public function doClientPayments($webapi_key, $session_handle, $carrier_id = null)
    {
        $this->checkWebApi($webapi_key);

        // gets user session
        $userSession = $this->checkSessionHandle($session_handle);

        // if user is not logged in
        if(!$userSession->isUserLoggedIn())
        {
            throw new Exception("User is not logged in.");
        }

        $basketGrossPrice = $userSession->getBasketPackage()->getPriceGross(true, true);

        return array(
            'results' => $userSession->getSfGuardUser()
                ->getDefaultClient()
                ->getAvailablePayments($carrier_id, false, $basketGrossPrice)
                ->toArray()
        );
    }
    
    /**
     * Gets payment name by id.
     *
     * @param string $webapi_key
     * @param integer id
     * @return array
     */
    public function doCheckOnlinePayment($webapi_key, $id)
    {
        $this->checkWebApi($webapi_key);

        return array(
            'isOnlinePayment' => PaymentTable::getInstance()->checkPayUPayment($id),
        );
    }

    /**
     * Get id of default carrier and payment
     * 
     * @param string $webapi_key
     * @return array
     */
    public function doDefaultCarrierAndPayment($webapi_key)
    {
        $this->checkWebApi($webapi_key);
        return array(
            'carrierId' => CarrierTable::getInstance()->getDefaultCarrierId(),
            'paymentId' => PaymentTable::getInstance()->getDefaultPaymentId(),
        );
    }

    /**
     * Get array of payments
     * 
     * @param string $webapi_key
     * @return array
     */
    public function doPayments($webapi_key)
    {
        $this->checkWebApi($webapi_key);
        return array(
            'results' => PaymentTable::getInstance()->getActivePayments()
                ->toArray(),
        );
    }

    /**
     * Get payment details
     *
     * @param string $webapi_key
     * @param integer $paymentId
     */
    public function doGetPayment($webapi_key, $paymentId)
    {
        $this->checkWebApi($webapi_key);
        $resultObj = PaymentTable::getInstance()->find($paymentId);

        if($resultObj)
        {
            return array('results' => $resultObj->toArray());
        }

        throw new Exception('Given payment does not exists.');
    }

    /**
     * Login user with given email address and password
     *
     * @param string $webapi_key
     * @param string $email_or_username
     * @param string  $password
     * @param string $session_handle if not null then check session
     * @exception Exception if wrong user email and/or password
     * @return array(
     *      'code' => '',
     *      'session_handle' => '',
     *      'user_id' => '',
     *      'server_time' => '',
     * ) 
     */
    public function doLogin($webapi_key, $email_or_username, $password, $session_handle)
    {
        $this->checkWebApi($webapi_key);

        $userObj = sfGuardUserTable::getInstance()->retrieveByUsernameOrEmailAddress($email_or_username);
        if(!$userObj || !$userObj->checkPassword($password))
        {
            return array('code' => MnumiRestServerException::E_MSG_ERROR,
                'server_time' => time());
        }

        // sign in rest
        $session_handle = sfContext::getInstance()->getUser()->signInRest($session_handle, $userObj);
        
        return array(
            'code' => self::E_MSG_SUCCESS,
            'session_handle' => $session_handle,
            'user_id' => $userObj->getId(),         
            'server_time' => time()
        );
    }
    
    /**
     * Checks user data and if necessary create new basket or transfer orders.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param array $data
     * @throws Exception 
     */
    public function doCheckoutData($webapi_key, $session_handle, $data = array())
    {
        $this->checkWebApi($webapi_key);
        
        // gets rest session
        $restSessionObj = $this->checkSessionHandle($session_handle);
        
        // if user id is defined
        if(!empty($data['userId']))
        {           
            $userObj = sfGuardUserTable::getInstance()->find($data['userId']);
        }
        else
        {
            // gets user from session
            $userObj = $restSessionObj->getSfGuardUser();
        }

        // if user does not exist
        if(!$userObj)
        {
            throw new Exception(MnumiRestServerException::E_USER_DOES_NOT_EXIST);
        }

        // updates rest data
        $restSessionObj->updateRestData($userObj);
        
        return array(
            'code' => self::E_MSG_SUCCESS,
            'session_handle' => $session_handle,
            'user_id' => $userObj->getId(),
            'server_time' => time()
        );
    }

    /**
     * Logs out. Clears session.
     * 
     * @param type $session_handle
     * @return array
     * @throws Exception 
     */
    public function doLogout($session_handle)
    {
        // gets user session based on session handle
        $userSession = $this->checkSessionHandle($session_handle);

        if($userSession->isUserLoggedIn())
        {
            sfContext::getInstance()->getUser()->signOut($session_handle);

            return array(
                'code' => self::E_MSG_SUCCESS,
                'server_time' => time(),
            );
        }

        throw new MnumiRestServerException('Incorrect session handle.');
    }

    /**
     * Get user details
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @return array
     */
    public function doGetMyData($webapi_key, $session_handle)
    {
        $this->checkWebApi($webapi_key);
        $userSession = $this->checkSessionHandle($session_handle);
        $version = array('version' => '3.0');

        // if not user session
        if(!$userSession)
        {
            return $version;
        }
        
        $userData = array();
        $basket = array('no_items' => 0, 'items' => array(), 'no_itemsCalc' => 0);

        // if user is logged in
        if($userSession->isUserLoggedIn())
        {
            // gets user from session
            $userObj = $userSession->getSfGuardUser();

            // gets default client
            $defaultClient = $userObj->getDefaultClient();

            // user data
            $userData = array(
                'login' => $userObj->getUsername(),
                'email' => $userObj->getEmailAddress(),
                'display_name' => $userObj->getFriendlyUsername($userObj->getEmailAddress()),
                'contact_person' => $userObj->getFirstName().' '.$userObj->getLastName(),
                'first_name' => $userObj->getFirstName(),
                'last_name' => $userObj->getLastName(),
                'loyalty_points' => $userObj->getAvailablePointsCount(),
                'pricelist_id' => PricelistTable::getInstance()->getDefaultPricelist()->getId(),
                'credit_limit' => '',
                'id_no' => '',
                'mobile' => ($userObj->getDefaultContactByType('SMS'))
                    ? $userObj->getDefaultContactByType('SMS')->getValue() 
                    : '',
                'notification' => array('email' => $userObj->hasNotificationByType('E-mail'), 'sms' => $userObj->hasNotificationByType('SMS')),
            );

            // prepares invoice data array
            $userData['invoice'] = array(
                'name' => '',
                'address' => '',
                'postal_code' => '',
                'city' => '',
                'tax_id' => '');

            // prepares delivery data array
            $userData['delivery'] = array('name' => '',
                'address' => '',
                'postal_code' => '',
                'city' => '');
            
            // current client
            $currentClient = array(
                'id' => '',
                'fullname' => '');
            
            // if default client exists
            if($defaultClient)
            {
                // if default client exists overwrite some userData attributes
                if($defaultClient->getPricelistId())
                {
                    $userData['pricelist_id'] = $defaultClient->getPricelistId();
                }
                
                if($defaultClient->getCreditLimitAmount())
                {
                    $userData['credit_limit'] = $defaultClient->getCreditLimitAmount();
                }
                
                if($defaultClient->getTaxId())
                {
                    $userData['id_no'] = $defaultClient->getTaxId();
                }

                $c = new sfCultureInfo();
                $countryArr = $c->getCountries();

                // invoice
                $userData['invoice'] = array(
                    'name' => $defaultClient->getFullname(),
                    'country' => $defaultClient->getCountry(),
                    'country_full_name' => isset($countryArr[$defaultClient->getCountry()])?$countryArr[$defaultClient->getCountry()]:'',
                    'address' => $defaultClient->getStreet(),
                    'postal_code' => $defaultClient->getPostcode(),
                    'city' => $defaultClient->getCity(),
                    'tax_id' => $defaultClient->getTaxId(),
                );

                // if client has addresses then gets them
                $clientAddresses = array();
                foreach($defaultClient->getClientAddresses() as $rec)
                {
                    $clientAddresses[$rec->getId()] = array(
                            'id' => $rec->getId(),
                            'name' => $rec->getFullname(),
                            'address' => $rec->getStreet(),
                            'postal_code' => $rec->getPostcode(),
                            'city' => $rec->getCity(),
                            'country' => $rec->getCountry());                    
                }
                
                // current client addresses
                $userData['delivery'] = array(
                    'currentAddress' => $defaultClient->getLastAddressId(),
                    'addresses' => $clientAddresses,
                    'default_delivery_days' => sfConfig::get('app_default_delivery_days', 1));

                // gets client packages with "waiting" status
                $clientWaitingPackageArr = array();
                foreach($defaultClient->getOrderPackagesWithStatus(OrderPackageStatusTable::$waiting) as $rec)
                {
                    $clientWaitingPackageArr[] = array(
                        'id' => $rec->getId(),
                        'created_at' => $rec->getCreatedAt(),
                        'description' => $rec->getDescription(),
                        'transport_number' => $rec->getTransportNumber()
                    );
                }

                // current client
                $currentClient = array(
                    'id' => $defaultClient->getId(),
                    'fullname' => $defaultClient->getFullname(),
                    'waitingPackages' => $clientWaitingPackageArr);
            }
  
            // gets user clients
            $clientList = $userObj->getClientList();

            $userData['clientList'] = $clientList;
            $userData['currentClient'] = $currentClient;
            
            // number of calculation items
            $basket['no_itemsCalc'] = 0;
            
            // only if user have assigned client (otherwise it is impossible to select orders)
            if($userSession->getSfGuardUser()->getCurrentClient())
            {
                try
                {
                    $basket['no_itemsCalc'] = $userSession->getSfGuardUser()
                    ->getFilteredOrders(null, OrderStatusTable::$calculation)
                    ->count();
                }
                catch(Exception $e)
                {
                    // do nothing 
                }
            }
        }

        // basket data
        $basketPackageObj = $userSession->getBasketPackage();

        if($basketPackageObj)
        {
            $basketPackageObj->affectsOrdersByCoupon();

            $basket['cart_id'] = $basketPackageObj->getId();

            // gets basket order in array
            $basket['items'] = $basketPackageObj->getRestBasketOrderArray();
            
            // gets number of items
            $basket['no_items'] = count($basket['items']);

            $basketPackageObj->calculatePrice(
                $basketPackageObj->getOrdersBasketQuery()
            );

            // if not logged gets number of calculation items from basket
            if(!$userSession->isUserLoggedIn())
            {
                $basket['no_itemsCalc'] = $basketPackageObj->getCalculationItems()->count();
            }
            else
            {
                //all points will be used
                $basketPriceNet = $basketPackageObj->getOrderTotalAmount();

                if($basketPriceNet > $userObj->getAvailablePointsValue())
                {
                    $priceWithLoyaltyPoints = OrderTable::getInstance()->addTax(
                        $basketPriceNet - $userObj->getAvailablePointsValue()
                    );

                    $loyaltyPointsValue = $userObj->getAvailablePointsValueGross();
                }
                else
                {
                    $priceWithLoyaltyPoints = 0;
                    $loyaltyPointsValue = OrderTable::getInstance()->addTax($basketPriceNet);
                }
                
                // add delivery price
                $priceWithLoyaltyPoints += OrderTable::getInstance()->addTax($basketPackageObj->getDeliveryPrice());

                $basket += array('priceWithLoyaltyPoints' => $priceWithLoyaltyPoints,
                    'loyaltyPointsValue' => $loyaltyPointsValue);
            }
            
            // gets additional information about basket
            $basket['basketInfo'] = $basketPackageObj->getRestBasketInfo();

            $customTax = $basketPackageObj->getClient()->getWntUe() ? 0 : false;
            $basket += array(
                'totalBasketPrice' => OrderTable::getInstance()->addTax($basketPackageObj->getOrderBaseAmount(), $customTax),
                'deliveryPrice' => OrderTable::getInstance()->addTax($basketPackageObj->getDeliveryPrice(), $customTax),
                'paymentPrice' => OrderTable::getInstance()->addTax($basketPackageObj->getPaymentPrice(), $customTax),
                'totalPrice' => OrderTable::getInstance()->addTax($basketPackageObj->getSummaryBaseAmount(), $customTax),
                'totalPriceWithDiscount' => OrderTable::getInstance()->addTax($basketPackageObj->getSummaryTotalAmount(), $customTax),
                'discountValue' => OrderTable::getInstance()->addTax($basketPackageObj->getDiscountValue(), $customTax)
            );
            
            // checks if payU payment
            $onlinePayment = false;
            $paymentId = $basketPackageObj->getPaymentId();
            if($paymentId)
            {
                $onlinePayment = PaymentTable::getInstance()->checkPayUPayment($paymentId);
            }
                
            $basket['isOnlinePayment'] = $onlinePayment;
        }

        return array_merge(array('user_data' => $userData, 'basket' => $basket), $version);
    }

    /**
     * Registers new user.
     * 
     * @param string $webapi_key
     * @param array $data - input values: array(
     *    'contact_person'    => '',
     *    'company_name'      => '',
     *    'address'           => '',
     *    'postal_code'       => '',
     *    'city'              => '',
     *    'id_no'             => '',
     *    'email'             => '',
     *    'phone'             => '',
     *    'mobile'            => '',
     *    'password'          => '',
     *    'password_repeat'   => '',
     *    'country'           => ''
     * )
     * required fields for company: contact_person, company_name, id_no, email, password
     * required fields for private person: contact_person, email, password
     * 
     * @return mixed - if false, then status is failed, it true then status is success
     */
    public function doRegister($webapi_key, $data = array())
    {
        $this->checkWebApi($webapi_key);

        if(!isset($data['password']) || !isset($data['password_repeat']))
        {
            throw new MnumiRestServerException(MnumiRestServerException::E_REG_PSWD_MISS);
        }

        if($data['password'] != $data['password_repeat'])
        {
            throw new MnumiRestServerException(MnumiRestServerException::E_REG_PSWD_NOT_SAME);
        }

        // checks is email already registered
        if(sfGuardUserTable::getInstance()->findOneByEmailAddress($data['email']))
        {
            throw new MnumiRestServerException(MnumiRestServerException::E_REG_LOGIN_EXISTS);
        }

        //checks is NIP already registered
        if($data['id_no'] != '')
        {
            if(ClientTable::getInstance()->findOneByTaxId($data['id_no']))
            {
                throw new MnumiRestServerException(MnumiRestServerException::E_REG_NIP_EXISTS);
            }
        }

        // user section     
        // @todo separate firstname & lastname in studio
        $firstAndLastName = explode(' ', $data['contact_person']);

        // user values array
        $userArr = array('first_name' => $firstAndLastName[0],
            'last_name' => $firstAndLastName[1],
            'email_address' => $data['email'],
            'password' => $data['password']);

        // client values array
        $clientArr = array('fullname' => ($data['company_name']) ? : $data['contact_person'],
            'city' => $data['city'],
            'postcode' => $data['postal_code'],
            'street' => $data['address'],
            'tax_id' => $data['id_no'],
            'country' => $data['country']);

        // registers user
        $userObj = new sfGuardUser();
        if($userObj->restRegister($userArr, $clientArr))
        {
            if(!empty($data['mobile']))
            {
                $userObj->setMobilePhone($data['mobile'])->save();
            }
            // triggers the event
            Mnumi::getInstance()->getEventDispatcher()->notify(new sfEvent(null,
                            'user.create',
                            array('obj' => $userObj, 'webapi_key' => $webapi_key)));
            return true;
        }

        throw new MnumiRestServerException(MnumiRestServerException::E_REG_FAILED);
    }

    /**
     * Updates user data.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param array $data user data: array(
     *      // USER object
     *      'contact_person'    => '',
     *      'email'             => '',
     *      // to change password all password fields must be not empty
     *      'password'          => '',
     *      'password_change'   => '', // true
     *      'password_current'  => '',
     *
     *      // NOTIFICATIONS objects
     *      'n_email'           => '',
     *      'n_sms'             => ''
     *      
     *      // USER CONTACT object
     *      'mobile'            => '',
     * )
     * 
     * @return boolean
     */
    public function doUpdateMyData($webapi_key, $session_handle, $data = array(), $type = 'all')
    {
        $this->checkWebApi($webapi_key);
        $userSession = $this->checkSessionHandle($session_handle);

        // if user is not logged in
        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerException::E_USER_NOT_LOGGED_IN);
        }
        
        $conn = Doctrine_Manager::connection();
        try
        {
            $conn->beginTransaction();
        
            // gets user from session
            $userObj = $userSession->getSfGuardUser();
            
            $userFormData = $this->prepareUserFormData($data);
            
            $userForm = new RestUserForm($userObj, array('userFormData' => $userFormData));
            $userForm->bind($userFormData);
            
            if(!$userForm->isValid())
            {
                throw new MnumiRestServerException(MnumiRestServerException::E_USER_FORM_INVALID);
            }
            
            $userForm->save();
    
    
            // mobile phone    
            if(!empty($data['mobile']))
            {
                $mobileObj = $userObj->getDefaultContactOrCreateByType(NotificationTypeTable::$sms);
                $userContactForm = new RestUserContactMobilePhoneForm($mobileObj);
                $userContactForm->bind(array('value' => $data['mobile']));
                
                if(!$userContactForm->isValid())
                {
                    throw new MnumiRestServerException(MnumiRestServerException::E_MOBILE_FORM_INVALID);
                }
                
                $userContactForm->save();
            }
    
            // passwords
            if(!empty($data['password_change']))
            {
                $passwordChangeForm = new restChangePasswordForm($userObj);
                $passwordChangeForm->bind(array('password' => $data['password'], 'password_current' => $data['password_current']));
                
                if(!$passwordChangeForm->isValid())
                {
                    throw new MnumiRestServerException(MnumiRestServerException::E_CHANGE_DATA_CURRENT_PASS);
                }
                $passwordChangeForm->save();
            }

            $conn->commit();
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new MnumiRestServerException($e->getMessage());
        }

        return true;        
    }
    
    /**
     * Prepares form fields for restUser form
     * 
     * @param array $data
     * @return array
     */
    private function prepareUserFormData($data)
    {
        $mappingFormDataToRestUser = array(
            'contact_person' => 'first_and_last_name',
            'email' => 'email_address',
            'n_email' => 'email_notification',
            'n_sms' => 'sms_notification'
        );
        
        $userFormData = array();
      
        foreach($mappingFormDataToRestUser as $key => $value)
        {
            if(isset($data[$key]))
            { 
                $userFormData[$value] = $data[$key];
            }
        }
        
        return $userFormData;
    }

    /**
     * Proceed new order.
     *
     * @param $webapi_key
     * @param $product_name
     * @param $session_handle
     * @param array $fields
     * @param int $order_id
     * @param string $order_status
     * @throws MnumiRestServerException
     * @throws Exception
     * @internal param string $webapiKey
     * @internal param string $productSlug
     * @internal param string $sessionHandle
     * @internal param int $orderId If editing
     * @return array('praca_id' => ...)
     */
    public function doOrder($webapi_key, $product_name, $session_handle, $fields,
            $order_id = 0, $order_status = null)
    {        
        $this->checkWebApi($webapi_key);

        // gets product object
        $productObj = ProductTable::getInstance()->findOneBySlug($product_name);

        if(!$productObj instanceof Product)
        {
            throw new Exception('Product "'.$product_name.'" does not exists.');
        }
        
        // gets user session
        $userSession = $this->checkSessionHandle($session_handle);

        // rest session not set then create unlogged session
        if(!$userSession)
        {
            $userSession = RestSessionTable::getInstance()->registerRestLogin($session_handle);
        }
        
        // gets user object
        $userObj = $userSession->getSfGuardUser();

        if($userObj->getId())
        {
            // gets client object
            $clientObj = $userObj->getDefaultClient();
        }
        else
        {
            $userObj = null;
            $clientObj = null;
        }

        // save section
        $conn = Doctrine_Manager::connection();
        try
        {
            $conn->beginTransaction();

            // if order is defined
            if($order_id > 0)
            {
                $orderObj = $this->checkOrderPermissions($session_handle,
                        $order_id);
            }
            // if not - creates one
            else
            {
                $orderObj = new Order();
                $orderObj->setProduct($productObj);
                $orderObj->setClient($clientObj);
                $orderObj->setSfGuardUser($userObj);
                $orderObj->setName($productObj->getName());
                $orderObj->setEditor($userObj);
                $orderObj->setShopName($webapi_key);
            }

            // order status (must be set in this place)
            $order_status = $order_status ?: OrderStatusTable::$draft;
            $orderObj->setOrderStatusName($order_status);

            // gets and parse old attributes
            $oldFieldArr = $this->parseOrderAttributeCustomFields($orderObj->getAttributeValues());

            // prepares field array
            $fieldArr = $this->parseRequestCustomFields($fields);
            
            // fixes empty field 
            $fieldArr = $this->fixEmptyOtherFieldsToFormOrderArray($fieldArr, $orderObj);

            $orderAttributes = array();

            // foreach proper field
            foreach($fieldArr as $key => $rec)
            {             
                // if key has WIZARD
                if(preg_match('/WIZARD/', $key))
                {
                    $key = 'WIZARD';
                }

                $orderAttributes[$key] = $rec;
            }

            $orderObj->setBulkAttributes($orderAttributes);

            $orderObj->setOrderStatusName($order_status);

            // deletes attributes if unnecessary
            $toDeleteArr = array_diff(array_keys($oldFieldArr), array_keys($fieldArr));
            foreach($toDeleteArr as $key => $rec)
            {       
                // if wizard then deletion must be specified by unique value
                // because product field is only one - WIZARD
                if(preg_match('/WIZARD/', $rec))
                {            
                    // gets and deletes value from key - format: WIZARD_value
                    $tempArr = explode('_', $rec);
                    $rec2 = $tempArr[1];
                    $orderObj->deleteWizardAttribute($rec2);
                }
                else
                {         
                    $orderObj->deleteAttribute($rec);
                }
            }

            // customize object
            if(!empty($fields['SIZE_WIDTH']['value']) && !empty($fields['SIZE_HEIGHT']['value']))
            {
                $orderObj->setCustomizable($fields['SIZE_WIDTH']['value'], $fields['SIZE_HEIGHT']['value']);
            }
            else
            {
                $orderObj->deleteCustomizable();
            }

            // sets name for order
            $orderObj->setName($fields['NAME']['value']);

            // sets additional informations
            $orderObj->setInformations($fields['INFORMATIONS']['value']);

            // if package exists
            $basketPackageObj = $userSession->getBasketPackage();
            if($basketPackageObj)
            {
                $orderObj->setOrderPackage($basketPackageObj);
            }
            else
            {
                // In ::createAndConnectWithOrder() is additional Order::save()
                $basketPackageObj = new OrderPackage();
                $basketPackageObj->createAndConnectWithOrder($orderObj,
                        OrderPackageStatusTable::$basket, $userSession);
            }
            // saves
            $orderObj->save();

            if(!$orderObj->getPriceBlock()) {
                // save calculation
                $orderCalculation = $orderObj->calculate();

                // this methods must be run after save
                $orderObj->setPrices($orderCalculation['priceItems']);
                $orderObj->updatePrices();

                $orderObj->save();
            }

            $basketPackageObj->affectsOrdersByCoupon();

            $conn->commit();
        }
        catch(ImpossibleOrderChangeException $e)
        {
            $conn->rollback();
            throw $e;
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new MnumiRestServerException('Could not save in Rest:doOrder: '.$e->getMessage(), 0, $e);
        }

        return array(
            'praca_id' => $orderObj->getId(),
        );
    }

    /**
     * Delete order (item) from basket.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param integer $order_id
     * @param integer $basket_id
     * @exception Exception if unable to delete basket item
     * 
     * @return status = success if true
     */
    public function doDeleteFromBasket($webapi_key, $session_handle, $order_id, $basket_id)
    {
        $this->checkAllowedMethod('post');
        $this->checkWebApi($webapi_key);
        
        // gets user session
        $orderObj = $this->checkOrderPermissions($session_handle, $order_id);

        if($orderObj->delete())
        {
            return true;
        }
        
        throw new Exception('Unable to delete basket item.');
    }
    
    /**
     * Updates basket of logged user.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param array $data array(
     *      'invoice'   => array(
     *          'name'          => '',
     *          'address'       => '',
     *          'postal_code'   => '',
     *          'city'          => '',
     *          'id_no'         => '',
     *      ),
     *      'delivery'  => array(
     *          'id'            => '',
     *          'name'          => '',
     *          'address'       => '',
     *          'postal_code'   => '',
     *          'city'          => '',
     *          'info'          => '',
     *          'address_id'    => '',
     *          'carrier_id'    => ''
     *      ),
     *      'payment'   => array(
     *          'id'            => '',
     *          'module'        => '',
     *      ),
     *      'basketStatus'      => '',
     *      'basketId'          => '',
     *      'paymentStatus'     => ''
     * )
     * 
     * @exception Exception if user is not logged or if missed any of required field
     * @return status = success if true, otherwise throws exception
     */
    public function doUpdateBasket($webapi_key, $session_handle, $data)
    {
        $this->checkWebApi($webapi_key);

        // gets user session
        $userSession = $this->checkSessionHandle($session_handle);

        // if user is not logged in
        if(!$userSession->isUserLoggedIn())
        {
            throw new Exception("Cannot update basket data - user is not logged in.");
        }

        // gets user object
        $userObj = $userSession->getSfGuardUser();

        /** @var OrderPackage $basketPackageObj */
        $basketPackageObj = $userObj->getBasketPackage();
        
        if($basketPackageObj && !empty($data['basketId']))
        {
            throw new Exception('This basket cannot be reverted, because another one was created.');
        }
        
        // if package id is set then gets directly from database
        if(!$basketPackageObj && !empty($data['basketId']))
        {
            // gets basket to revert
            $basketPackageObj = OrderPackageTable::getInstance()->find($data['basketId']);
            if($basketPackageObj 
                    && $basketPackageObj->getUserId() != $userObj->getId())
            {
                throw new Exception('This basket cannot be reverted.');
            }
        }    
        
        // gets client object
        $clientObj = $basketPackageObj->getClient();
             
        // delivery data package
        // delivery carrier id
        if(isset($data['delivery']['carrier_id']))
        {
            $basketPackageObj->setCarrier(CarrierTable::getInstance()->find($data['delivery']['carrier_id']));      
        }

        // delivery address id
        if(isset($data['delivery']['address_id']))
        {
            $clientAddressObj = ClientAddressTable::getInstance()
                    ->find($data['delivery']['address_id']);
            
            if($clientAddressObj)
            {
                $basketPackageObj->setClientAddress($clientAddressObj);   
                $basketPackageObj->setDeliveryName($clientAddressObj->getFullname());
                $basketPackageObj->setDeliveryStreet($clientAddressObj->getStreet());
                $basketPackageObj->setDeliveryPostcode($clientAddressObj->getPostcode());
                $basketPackageObj->setDeliveryCity($clientAddressObj->getCity());
                $basketPackageObj->setDeliveryCountry($clientAddressObj->getCountry());
                //$basketPackageObj->setDescription($data['delivery']['info']);
            }
        }

        // sets package carrier id
        if(isset($data['delivery']['id']) && $data['delivery']['id'] > 0)
        {
            $carrierObj = ClientAddressTable::getInstance()
                    ->find($data['delivery']['id']);
            
            if($carrierObj)
            {
                $basketPackageObj->setCarrier($carrierObj);
            }  
        }
        // delivery date
        $deliveryAt = isset($data['delivery']['delivery_at']) ? $data['delivery']['delivery_at'] : null;
        if ($deliveryAt !== null)
        {
            $basketPackageObj->setDeliveryAt($deliveryAt);
        }

        // if invoice
        if(isset($data['invoice']['i_want_invoice']))
        {
            $basketPackageObj->setInvoiceName($clientObj->getFullname());
            $basketPackageObj->setInvoiceStreet($clientObj->getStreet());
            $basketPackageObj->setInvoicePostcode($clientObj->getPostcode());
            $basketPackageObj->setInvoiceCity($clientObj->getCity());
            $basketPackageObj->setInvoiceTaxId($clientObj->getTaxId());
            $basketPackageObj->setWantInvoice((boolean)$data['invoice']['i_want_invoice']);
        }
        
        // invoice data package
        if(isset($data['invoice']['id_no']) && !empty($data['invoice']))
        {
            $basketPackageObj->setInvoiceName($data['invoice']['name']);
            $basketPackageObj->setInvoiceStreet($data['invoice']['address']);
            $basketPackageObj->setInvoicePostcode($data['invoice']['postal_code']);
            $basketPackageObj->setInvoiceCity($data['invoice']['city']);
            $basketPackageObj->setInvoiceCountry($data['invoice']['country']);
            $basketPackageObj->setInvoiceTaxId($data['invoice']['id_no']);
            $basketPackageObj->setInvoiceInfo($data['invoice']['info']);
            $basketPackageObj->setWantInvoice((boolean)$data['invoice']['i_want_invoice']);
        }

        // payment data package
        if(isset($data['payment']['id']))
        {
            // gets available payments for current carrier 
            $paymentColl = $clientObj->getAvailablePayments(
                    $data['delivery']['carrier_id'], 
                    false);

            // prepares only id array
            $paymentIdArr = array();
            foreach($paymentColl as $rec)
            {
                $paymentIdArr[] = $rec->getId();
            }

            // if POST payment id is available for current carrier
            if(in_array($data['payment']['id'], $paymentIdArr))
            {
                $basketPackageObj->setPaymentId($data['payment']['id']);
            }   
            else
            {
                $basketPackageObj->setPaymentId(null);
            }
        }

        // basket package status
        if(isset($data['basketStatus']))
        {
            // if empty set as default - waiting
            if(empty($data['basketStatus']))
            {
                $basketStatus = OrderPackageStatusTable::$waiting;
            }
            else
            {
                $basketStatus = $data['basketStatus'];
            }

            // sets order package status
            $basketPackageObj->setOrderPackageStatusName($basketStatus);
            
            if($basketStatus == OrderPackageStatusTable::$waiting)
            {
                // sets order package items status       
                $basketPackageObj->setAllOrdersStatus(OrderStatusTable::$new);

                // sets notification for buy (after checkout and summarize)
                if($basketPackageObj->getOrderPackageStatusName() == OrderPackageStatusTable::$waiting)
                {
                    Mnumi::getInstance()->getEventDispatcher()->notify(new sfEvent(null,
                            'package.buy',
                            array('obj' => $basketPackageObj, 'webapi_key' => $webapi_key)));
                }

                // update client balance
                $clientObj->calculateBalance();
                $clientObj->save();
            }
            // reverts package to basket status
            elseif($basketStatus == OrderPackageStatusTable::$basket)
            {            
                // sets order package items status               
                $basketPackageObj->setAllOrdersStatus(OrderStatusTable::$draft);
            }    
        }

        // basket package status
        if(isset($data['paymentStatus']))
        {
            // if empty set as default - waiting
            if(empty($data['paymentStatus']))
            {
                $paymentStatus = PaymentStatusTable::$paid;
            }
            
            // sets order package basket status
            $basketPackageObj->setPaymentStatusName($paymentStatus);
        }
        
        // saves package
        $basketPackageObj->save();
      
        return true;
    }
    
    /**
     * Returns order info.
     * 
     * @param type $webapi_key
     * @param type $session_handle
     * @param type $order_id
     * @return array 
     */
    public function doGetOrderInfo($webapi_key, $session_handle, $order_id)
    {
        $this->checkWebApi($webapi_key);

        // gets order object and checks permission
//        die($session_handle);
        $orderObj = $this->checkOrderPermissions($session_handle, $order_id);

        if(!$orderObj)
        {
            return array(
                'order_id' => 0,
                'product_name' => 0,
                'fields' => array(),
            );
        }
       
        // gets files
        $filesColl = $orderObj->getFiles();

        // @TO CHECK
        $fileArr = array();
        foreach($filesColl as $fileObj)
        {
            $fileArr[] = array(
                'name' => $fileObj->getFilename(),
                'filename' => $fileObj->getFullFilePath(),
                'size' => filesize($fileObj->getFullFilePath()),
                'id' => $fileObj->getId(),
                'format' => $fileObj->getMetadataAttribute('format'),
                'colorspace' => $fileObj->getMetadataAttribute('colorspace'),
                'widthPx' => $fileObj->getMetadataAttribute('widthPx'),
                'heightPx' => $fileObj->getMetadataAttribute('heightPx'),
                'nrOfPages' => $fileObj->getMetadataAttribute('nrOfPages'),
                'displayable'=> $fileObj->isDisplayable()
            );
        }
             
        // wizard
        // gets wizards
        $wizardArr = $orderObj->getWizardsOrderAttributesAsArray();
      
        // gets product
        $productObj = $orderObj->getProductWithDeleted();
        
        // if product exists
        if(!$productObj)
        {  
            throw new MnumiRestServerException('No product found');
        }  
        
        $fieldArr = $this->prepareFieldArr($orderObj);

        return array(
            'order_id' => $orderObj->getId(),
            'product_name' => $productObj->getSlug(),
            'product_title' => $orderObj->getName(),
            'typ' => '',
            'fields' => $fieldArr,
            'files' => $fileArr,
            'wizards' => $wizardArr,
            'bodyPlusPrice' => $orderObj->getTotalAmount(),
            'bodyPlusPricePlusTax' => $orderObj->getTotalAmountGross(),
            'orderStatus' => $orderObj->getOrderStatusName()
        );          
    }
    
    /**
     * Gets invoice details based on invoice id.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param string $invoice_id
     * 
     * @return string 
     */
    public function doGetInvoiceInfo($webapi_key, $session_handle, $invoice_id)
    {
        $this->checkWebApi($webapi_key);
        
        // gets order object and checks permission
        $invoiceObj = $this->checkInvoicePermissions($session_handle, $invoice_id);

        // gets invoice content
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/rest/invoice/print/'.$invoiceObj->getId().'/'.sfConfig::get('sf_csrf_secret');

        return array('results' => $url);
    }

    /**
     * Load list of packages with payment type PayU and Unpaid
     *
     * @param $webapi_key
     * @param $session_handle
     * @throws MnumiRestServerException
     * return $array
     */
    public function doGetUnpaidPackages($webapi_key, $session_handle)
    {
//        $this->checkAllowedMethod('post');
        $this->checkWebApi($webapi_key);

        // gets user session
        $userSession = $this->checkSessionHandle($session_handle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        $results = array();

        // gets order collection of orders with ready
        $packages = $userSession->getSfGuardUser()->getUnpaidPackages();

        foreach($packages as $package)
        {
            $orders = $package->getOrdersQuery()->execute();


            if(count($orders) == 0)
            {
                continue;
            }

            $ordersIds = array();

            foreach($orders as $order)
            {

                $ordersIds[] = array(
                    'id' => $order->getId(),
                    'name' => $order->getName(),
                );
            }

            $package->calculatePrice($package->getOrdersDefaultQuery());

            $summaryTotalAmount = $package->getSummaryTotalAmount();

            $results[] = array(
                'id' => $package->getId(),
                'name' => $package->__toString(),
                'orders' => $ordersIds,
                'summaryNetPrice' => $summaryTotalAmount,
                'summaryGrossPrice' => OrderTable::getInstance()->addTax($summaryTotalAmount),
            );
        }

        return array('results' => $results, 'nbResults' => count($results));
    }

    /**
     * Gets orders by user id.
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @param string $orderStatus
     * @param integer $offset
     * @param integer $limit
     *
     * @throws MnumiRestServerException
     * @return array
     */
    public function doGetOrdersByUser($webapi_key, $session_handle, $orderStatus,
            $offset = 0, $limit = 10)
    {
        $this->checkAllowedMethod('post');
        $this->checkWebApi($webapi_key);

        // gets user session
        $userSession = $this->checkSessionHandle($session_handle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        // gets order collection of orders with ready 
        $orderColl = $userSession->getSfGuardUser()
                ->getFilteredOrders(null, $orderStatus, $offset, $limit);

        $results = array();
        foreach($orderColl as $orderObj)
        {
            // gets order package
            $orderPackageObj = $orderObj->getOrderPackage();        
            $results[] = array(
                'id' => $orderObj->getId(),
                'user_number' => $orderObj->getUserId(),
                'name' => $orderObj->getName(),
                'status_id' => $orderObj->getOrderStatusName(),
                'created_at' => $orderObj->getCreatedAt(),
                'hash' => '',
                'price_net' => $orderObj->getTotalAmount(),
                'price_gross' => $orderObj->getTotalAmountGross(),
                'favourite' => $orderObj->isFavouriteOf($userSession->getSfGuardUser()->getId()) ? 1 : 0,
                'thumbImage' => $orderObj->getThumb(),
                'delivery_id' => $orderPackageObj->getCarrierId(),
                'delivery_name' => $orderPackageObj->getDeliveryName(),
                'delivery_address' => $orderPackageObj->getClient()->getFullname()."\n".$orderPackageObj->getFullDeliveryAddress(),
                'package_payment_status' => $orderPackageObj->getPaymentStatusName(),
            );
        }
        return array('results' => $results,
            'nbResults' => $userSession->getSfGuardUser()
                ->getFilteredOrders(null, $orderStatus)
                ->count());
    }

    /**
     * Gets invoices by user id.
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @param integer $offset
     * @param integer $limit
     * @return array
     */
    public function doGetInvoicesByUser($webapi_key, $session_handle, 
            $params = array(), $offset = null, $limit = null)
    {
        if(sfConfig::get('app_default_disable_advanced_accountancy', false) === true)
        {
            return array('invoice' => 'disabled');
        }

        $this->checkAllowedMethod('post');
        $this->checkWebApi($webapi_key);

        // gets user session
        $userSession = $this->checkSessionHandle($session_handle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        // gets parameters
        $year = array_key_exists('year', $params) ? $params['year'] : null;
        $month = array_key_exists('month', $params) ? $params['month'] : null;

        // gets order collection of orders with ready 
        $invoiceColl = $userSession->getSfGuardUser()->getInvoices($year, $month, 
                $offset, $limit);

        $results = array();
        foreach($invoiceColl as $invoiceObj)
        {
            $results[] = array(
                'id' => $invoiceObj->getId(),
                'created_at' => $invoiceObj->getSellAt(),
                'number' => $invoiceObj->getNumber(),
                'tax_number' => $invoiceObj->getClientTaxId(),
                'type' => $invoiceObj->getInvoiceTypeName(),
                'client_name' => $invoiceObj->getClientName(),
                'client_contact_person' => '',
                'price_net' => $invoiceObj->getPriceNet(),
                'price_gross' => $invoiceObj->getPriceGross(),
                'payment_type_id' => $invoiceObj->getPaymentId(),
                'payment_type_label' => $invoiceObj->getPaymentLabel(),
                'payment_date' => $invoiceObj->getPaymentDateAt(),
                'date_of_payment' => $invoiceObj->getPayedAt(),
                'after_deadline' => (
                (($invoiceObj->getPayedAt() != null && ($invoiceObj->getPayedAt() != '0000-00-00 00:00:00'))) && (strtotime($invoiceObj->getPaymentDateAt()) < time())) ? true : false,
                'is_payed' => (($invoiceObj->getPayedAt() != null) && ($invoiceObj->getPayedAt() != '0000-00-00 00:00:00')),
                'status_label' => $invoiceObj->getInvoiceStatusName(),
            );
        }

        return array('results' => $results,
            'nbResults' => $userSession->getSfGuardUser()
                ->getInvoices($year, $month)
                ->count());
    }
    
     /**
     * Edits photo related to order.
     * 
     * @todo need tests
     * @param string $webapiKey
     * @param string $sessionHandle
     * @param integer $order_id
     * @param string $filePath
     * @return array('photo_id' => ..., 'photo_uri' => ...)
     */
    public function doPhotoEdit($webapi_key, $session_handle, $order_id, $file_path)
    {
        $this->checkWebApi($webapi_key);
        $orderObj = $this->checkOrderPermissions($session_handle, $order_id);

        if(!file_exists($file_path))
        {
            throw new Exception('Filename '.$file_path.' does not exists.');
        }
        
        try
        {
            $photo = $orderObj->importFile($file_path);
        }
        catch(Exception $e)
        {
            throw new MnumiRestServerException('Could not import filename: '.$e->getMessage());
        }

        return array(
            'photo_id' => $photo->getId(),
            'photo_uri' => $photo->getFilePath(),
        );
    }

    /**
     * Deletes photo related to order.
     * 
     * @todo need tests
     * @param string $webapiKey
     * @param string $sessionHandle
     * @param integer $order_id
     * @param integer $fileId Object File ID 
     * @return array('photo_id' => ...)
     */
    public function doPhotoDelete($webapi_key, $session_handle, $order_id, $file_id)
    {
        $this->checkWebApi($webapi_key);
        $orderObj = $this->checkOrderPermissions($session_handle, $order_id);

        $fileObj = $orderObj->findOneFileById($file_id);

        if(!$fileObj instanceOf File)
        {
            return array(
                'photo_id' => 0
            );
        }

        // deletes
        $fileObj->delete();

        return array(
            'photo_id' => $fileObj->getId()
        );
    }

    /**
     * Gets first image in order.
     * @param type $webapi_key
     * @param type $session_handle
     * @param type $order_id
     */
    public function doGetOrderFirstImage($webapi_key, $session_handle, $order_id)
    {
        // checks permissions
        $this->checkWebApi($webapi_key);
        $orderObj = $this->checkOrderPermissions($session_handle, $order_id);

        // if order does not exists
        if(!$orderObj)
        {
            return array(
                'order_id' => 0,
                'image_path' => null,
            );
        }

        // if no files
        if($orderObj->getFirstDisplayableFile())
        {
            return array(
                'order_id' => $orderObj->getId(),
                'image_path' => $orderObj->getFirstDisplayableFile()->getFullFilePath());
        }

        // if no files
        $wizardColl = $orderObj->getWizardOrderAttributes();
        if($wizardColl->count() > 0)
        {
            return array(
                'order_id' => $orderObj->getId(),
                'image_path' => WizardManager::getInstance()->generatePreviewUrl($wizardColl->getFirst()->getValue(),
                        90, 90),
                'wizard' => 1);
        }

        return array(
            'order_id' => $orderObj->getId(),
            'image_path' => null);
    }

    /**
     * Gets current user loyalty points basing on specified criteria.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param string $status
     * @param string $orderBy
     * @param string $sort
     * @param integer $per_page
     * @param integer $page
     * @return array
     * @throws Exception 
     */
    public function doGetLoyaltyPoints($webapi_key, $session_handle, $status = '', 
            $orderBy = 'created_at', $sort = 'asc', $per_page = 25, $page = 1)
    {
        $this->checkAllowedMethod('get');
        $this->checkWebApi($webapi_key);
        
        // gets user session
        $userSession = $this->checkSessionHandle($session_handle);
        
        // if user is not logged in
        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        // gets status
        $status = LoyaltyPointTable::$types[$status];
        
        // gets user object
        $userObj = $userSession->getSfGuardUser();

        // gets doctrine query
        $doctrineQuery = $userObj->getLoyaltyPointsSortedQuery($status, $orderBy, $sort);
        
        // counts points
        $allPointColl = $doctrineQuery->execute();
        $count = 0;
        foreach($allPointColl as $rec)
        {
            $count += $rec->getPoints();
        }
        
        // initializes pager
        $pager = new sfDoctrinePager('LoyaltyPoint', $per_page);
        $pager->setQuery($doctrineQuery);
        $pager->setPage($page);
        $pager->init();
        
        $pointArr = array();
        foreach($pager->getResults() as $rec)
        {
            $pointArr[] = array('pointsCount' =>$rec->getPoints(),
                'pointsStatus' => $rec->getStatus(),
                'description' => $rec->getDescription(),
                'created_at' => $rec->getCreatedAt());     
        }
        
        $result = array('totalPointsCount' => $count,
            'points' => $pointArr,
            'entryCount' => $doctrineQuery->count());
        
        return $result;
    }
    
    /**
     * Sets token to change user password. Sends to user email with link 
     * to set new password.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param string $login user login (email)
     * @exception Exception 
     * @return status=true if ok
     */
    public function doSetLostPassword($webapi_key, $session_handle, $login)
    {
        // gets shop object
        $this->checkWebApi($webapi_key);

        // checks if user does exist
        $userObj = sfGuardUserTable::getInstance()->retrieveByUsernameOrEmailAddress($login);
        if(!$userObj)
        {
            return array('code' => MnumiRestServerException::E_LOST_PASS_LOGIN,
                'server_time' => time());
        }
        
        // generates token in database
        $token = $userObj->generateLostPasswordToken();
        
        // triggers the event
        Mnumi::getInstance()->getEventDispatcher()->notify(new sfEvent(null,
                'user.password.lost.shop',
                array('obj' => $userObj, 'webapi_key' => $webapi_key)));
  
        return array('token' => $token);
    }
    
    /**
     * Update lost pasword if token is correct
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param string $token
     * @param string $password
     * @exception Exception 
     * @return bolean
     */
    public function doUpdateLostPassword($webapi_key, $session_handle, $token, $password)
    {
        $this->checkWebApi($webapi_key);
      
        // password and token cannot be empty
        if(!empty($token) && !empty($password))
        {
            $forgotPassObj = sfGuardForgotPasswordTable::getInstance()->getActive($token);
            if($forgotPassObj)
            {
                // gets user object
                $userObj = $forgotPassObj->getUser();
                $userObj->setPassword($password);
                $userObj->deleteOldUserForgotPassword();
                $userObj->save();
                
                return true;
            }
        }
        
        throw new MnumiRestServerException(MnumiRestServerException::LOST_PASSWORD_UPDATE_FAILED);
    }
    
    /**
     * Creates client connected with current user.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param array $data
     * @exception Exception 
     * @return integer client id
     */
    public function doCreateClient($webapi_key, $session_handle, $data)
    {
        $this->checkWebApi($webapi_key);
        $userSession = $this->checkSessionHandle($session_handle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        if((isset($data['contact_person']) ||  isset($data['company_name']))
            && isset($data['city']) && isset($data['postal_code'])
            && isset($data['address']))
        {
            // client values array
            $clientArr = array('fullname' => $data['company_name'] ?: $data['contact_person'],
                'city' => $data['city'],
                'postcode' => $data['postal_code'],
                'street' => $data['address'],
                'tax_id' => $data['id_no'],
                'country' => $data['country']);

            // gets user from session
            $userObj = $userSession->getSfGuardUser();

            // creates new client
            $clientObj = $userObj->createClient($clientArr);
            if($clientObj)
            {
                return array('clientId' => $clientObj->getId());
            }
        }
    }
 
    /**
     * 
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param array $data
     * @exception Exception 
     * @throws MnumiRestServerException 
     * @return array 
     */
    public function doFavouriteOrder($webapi_key, $session_handle, $data)
    {
        $this->checkWebApi($webapi_key);
        $userSession = $this->checkSessionHandle($session_handle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        $orderObj = OrderTable::getInstance()
                ->find($data);

        if($orderObj)
        {
            // gets user from session
            $userObj = $orderObj->getSfGuardUser();

            $orderId = $orderObj->getId();
            $userId = $userObj->getId();

            $orderUserFavouriteObj = $orderObj->isFavouriteOf($userId);

            // if order user favourite exists then deletes
            if($orderUserFavouriteObj)
            {
                $orderUserFavouriteObj->delete();

                return array('result' => 'deleted');
            }
            // otherwise create
            else
            {
                $orderUserFavouriteObj = new OrderUserFavourite();
                $orderUserFavouriteObj->setOrderId($orderId);
                $orderUserFavouriteObj->setUserId($userId);
                $orderUserFavouriteObj->save();

                return array('result' => 'created');
            }
        }
    }
    
    /**
     * Gets calculation content basing on data array.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param array $data
     * @param string $product_name
     * @param integer $pricelist_id
     * @exception Exception 
     * @return array
     */
    public function doGetSendCalculationContent($webapi_key, $session_handle,
            $data, $product_name = null, $pricelist_id = null)
    {
        $this->checkWebApi($webapi_key);
        $userSessionObj = $this->checkSessionHandle($session_handle);

        // notification for existing order
        if(isset($data['orderArr']['ORDER_ID']) && $data['orderArr']['ORDER_ID']['value'] > 0) {
            
            $order = OrderTable::getInstance()->findOneById($data['orderArr']['ORDER_ID']['value']);
            
            if (!$order instanceof Order) {
                throw new Exception(sprintf('Order with id: ["%s"] does not exists in database.',
                $data['orderArr']['ORDER_ID']['value']));
            }
            
        } else {
            
            //gets product
            $product = ProductTable::getInstance()->findOneBySlug($product_name);

            if(!$product instanceof Product)
            {
                throw new Exception(sprintf('Selected product "%s" does not exists in database.',
                    $product_name));
            }

            $order = new Order();
            $order->setProduct($product);

        }

        $shopUrlNotificationPreview = $order->getShopUrlOrderPreviewNotification();
        
        $priceReportArr = $order->calculate($data['orderArr'], $pricelist_id);

        $context = sfContext::getInstance();

        // decorator
        $decorator = new CalculationDecorator($context->getI18N());
        $calculation = $decorator->getOrderDetailsView($priceReportArr);

        // gets email item list

        $emailItemListPartial = $context->getController()
            ->getAction('order', 'edit')
            ->getPartial('order/orderEmail/emailRestCalculation',
                    array(
                        'priceReportArr' => $priceReportArr,
                        'calculation' => $calculation,
                    ));

        $myUserObj = sfContext::getInstance()->getUser();

        if($userSessionObj
            && $userSessionObj->isUserLoggedIn()
            &&
            NotificationTable::getInstance()->getByTemplateNameAndTypeName(
                NotificationTemplateTable::$calculation,
                NotificationTypeTable::$email))
        {
            // prepares calculation notification object (shortcodes)
            $calculationNotificationObj = new CalculationNotification(
                sfContext::getInstance(),
                null,
                array(
                    'calculation' => $emailItemListPartial,
                    'productName' => $data['orderArr']['NAME']['value'],
                    'userName' => $userSessionObj->getSfGuardUser()->__toString(),
                    'shopUrlOrderPreview' => $shopUrlNotificationPreview
                ));

            $calculationSendTemplate = TwigNotificationManager::getInstance($myUserObj)
                    ->getTwigTemplateObj(NotificationTemplateTable::$calculation,
                    NotificationTypeTable::$email)
                    ->render($calculationNotificationObj->getShortcodesAsArgArr());

            return array('response' => $calculationSendTemplate);
        }
        else
        {
            // prepares calculation notification object (shortcodes)
            $calculationNotificationObj = new CalculationWithoutLoggingNotification(
                sfContext::getInstance(),
                null,
                array(
                    'calculation' => $emailItemListPartial,
                    'productName' => $data['orderArr']['NAME']['value'],
                    'shopUrlOrderPreview' => $shopUrlNotificationPreview
                ));

            $calculationSendTemplate = TwigNotificationManager::getInstance($myUserObj)
                    ->getTwigTemplateObj(NotificationTemplateTable::$calculationWithoutLogging,
                    NotificationTypeTable::$email)
                    ->render($calculationNotificationObj->getShortcodesAsArgArr());

            return array('response' => $calculationSendTemplate);
        }

        throw new MnumiRestServerException(MnumiRestServerClientException::E_GET_SEND_CALCULATION_CONTENT);
    }
    
    /**
     * Sends calculation basing on data array.
     * 
     * @param string $webapi_key
     * @param string $session_handle
     * @param array $data
     * @exception Exception 
     * @return array
     */
    public function doSendCalculation($webapi_key, $session_handle, $data, 
            $product_name = null, $pricelist_id = null)
    {
        $this->checkWebApi($webapi_key);
        $userSessionObj = $this->checkSessionHandle($session_handle);
        
        // gets sender
        $sender = NotificationEmail::getSender($webapi_key);

        $result = array();
        if(isset($data['orderArr']['ORDER_ID']['value'])
            && $data['orderArr']['ORDER_ID']['value'] > 0) {

            $result['praca_id'] = (int) $data['orderArr']['ORDER_ID']['value'];

        }
        else {

            $result = $this->doOrder(
                        $webapi_key,
                        $data['emailArr']['productSlug'],
                        $session_handle,
                        $data['orderArr'],
                        0,
                        OrderStatusTable::$calculation);

        }

        // if order was not created
        if(empty($result['praca_id']))
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SEND_CALCULATION);
        }

        $mailerObj = sfContext::getInstance()->getMailer();
            
        $message = $mailerObj->compose(
                $sender,
                $data['emailArr']['to'],
                $data['emailArr']['subject']
        );
        
        $messageBody = nl2br($data['emailArr']['calculation']);
        $message->setBody($messageBody, 'text/html');

        if(!$mailerObj->send($message))
        {
            throw new Exception('Mailer could not send emails.');
        }
        
        $result['responseStatus'] = 'success';
        
        return array('response' => $result);
    }
    
        /**
     * Add coupon to package
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @param string $name
     * 
     * @return boolean
     */
    public function doAddCoupon($webapi_key, $session_handle, $name)
    {
        $this->checkWebApi($webapi_key);
      
        $userSession = $this->checkSessionHandle($session_handle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        $basketPackageObj = $userSession->getBasketPackage();
        
        $coupon = CouponTable::getInstance()->find(strtolower($name));
        if(!$coupon)
        {
            throw new MnumiRestServerException(MnumiRestServerException::COUPON_DOES_NOT_EXIST);
        }
        
        // check if any order and client are affected by this coupon
        if(!$basketPackageObj->isAffectedByCoupon($coupon))
        {
            throw new MnumiRestServerException(MnumiRestServerException::COUPON_DOES_NOT_AFFECT_BASKET);
        }
        
        // if coupon is one time usage and is already used
        if($coupon->getOneTimeUsage() && $coupon->getUsed())
        {
            throw new MnumiRestServerException(MnumiRestServerException::COUPON_IS_ALREADY_USED);
        }
        
        // if coupon is out of date
        if($coupon->getExpireAt() < date('Y-m-d'))
        {
            throw new MnumiRestServerException(MnumiRestServerException::COUPON_IS_OUT_OF_DATE);
        }

        $basketPackageObj->linkWithCoupon($coupon);

        return true;
    }

    /**
     * Get client data
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @param array $id
     * @exception Exception
     * @return array Client data
     */
    public function doGetClientData($webapi_key, $session_handle, $id)
    {
        $this->checkWebApi($webapi_key);
        $userSession = $this->checkSessionHandle($session_handle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        $clientObj = ClientTable::getInstance()->getClientByIdAndUserId($id, $userSession->getUserId());
        
        if(!$clientObj)
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_EDIT_CLIENT_PERMISSIONS);
        }
        
        return $clientObj->toArray();
    }
    
    /**
     * Validates web api key 
     * 
     * @param string $webapi_key
     * @return Shop
     */
    protected function checkWebApi($webapi_key)
    {
        $shopObj = ShopTable::getInstance()->checkValidShop($webapi_key);
        if(!$shopObj)
        {
            throw new Exception('Incorrect webapi key or key is out of date (key: '.$webapi_key.')');
        }
        MnumiI18n::setCulture($shopObj->getShopLang());
        
        return $shopObj;
    }
    
    /**
     * Validates connection key (used for Printlabel)
     *
     * @param string $connection_key
     * @throws Exception
     * @return true 
     */
    protected function checkConnectionKey($connection_key)
    {
    	if($connection_key != substr(sfConfig::get('sf_csrf_secret'), 0, 6))
    	{
    		throw new Exception('Incorrect connection key. Check your CSRF secret key, from settings.yml');
    	}
    
    	return true;
    }

    /**
     * Check if requested method is allowed
     * 
     * @param string/array $methods
     * @exception Exception
     * @return null
     */
    protected function checkAllowedMethod($methods)
    {
        if(!is_array($methods))
        {
            $methods = array($methods);
        }

        if(!$this->allowedMethod($methods))
        {
            throw new Exception('Incorrect request method type (allowed: '.implode(', ', $methods).')');
        }
    }

    /**
     * Check if request method (GET/POST/PUT) is allowed
     * 
     * @param array $methods
     * @return bool
     */
    protected function allowedMethod(array $methods)
    {
        // ignore for test
        if(!isset($_SERVER['REQUEST_METHOD']))
        {
            return true;
        }

        foreach($methods as $method)
        {
            if(strtoupper($method) == $_SERVER['REQUEST_METHOD'])
            {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Checks permissions to order change.
     *
     * @param string $sessionHandle
     * @param integer $orderId
     * @throws Exception
     * @return Order order instance
     */
    private function checkOrderPermissions($sessionHandle, $orderId)
    {
        // please keep in mind, that it's could be not logged in user (anonymous user)
        $userSession = $this->checkSessionHandle($sessionHandle);

        $orderObj = OrderTable::getInstance()->find($orderId);

        // if order exist
        if(!$orderObj instanceof Order)
        {
            throw new MnumiRestServerException('Order does not exists.');
        }

        // if user has permission to edit file
        if(!$orderObj->canEditByUser($userSession->getSfGuardUser(), $sessionHandle))
        {
            throw new MnumiRestServerException('User could not edit the order.');
        }

        return $orderObj;
    }

    /**
     * Checks permissions to order change.
     *
     * @param string $sessionHandle
     * @param integer $invoiceId
     * 
     * @throws Exception
     * 
     * @return Order order instance
     */
    private function checkInvoicePermissions($sessionHandle, $invoiceId)
    {
        $userSession = $this->checkSessionHandle($sessionHandle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        // gets invoice which belongs to current user
        $invoiceObj = $userSession->getSfGuardUser()->getInvoicesQuery()
                ->addWhere('i.id = ?', $invoiceId)
                ->limit(1)
                ->fetchOne();

        // if invoice exist
        if(!$invoiceObj instanceof Invoice)
        {
            throw new MnumiRestServerException('Invoice does not exists.');
        }

        return $invoiceObj;
    }
    
    /**
     * Check session handle
     * 
     * @param string $sessionHandle
     * @throws Exception
     * @return RestSession|false
     */
    private function checkSessionHandle($sessionHandle)
    {
        $restSessionObj = RestSessionTable::getInstance()->checkSession($sessionHandle);

        if(!$restSessionObj)
        {
            return false;
        }

        return $restSessionObj;
    }

    /**
     * Returns formatted array with fields which matches to OrderForm.
     * E.g.:
     * array(
     *      [0] => array
     *          (
     *              [id] => COUNT
     *              [value] => 1
     *          )
     *
     *      [1] => array
     *          (
     *              [id] => QUANTITY
     *              [value] => 1
     *          )
     *          .
     *          .
     *          .    
     *      [6] => array
     *          (
     *              [id] => OTHER_0
     *              [value] => 16
     *          )
     *
     *      [7] => array
     *          (
     *              [id] => INFORMATIONS
     *              [value] => array
     *                  (
     *                  )
     *
     *          )
     * 
     * @param Order $orderObj
     * @return string 
     */
    private function prepareFieldArr(Order $orderObj)
    {
        // gets order attributes
        $orderAttributeColl = $orderObj->getOrderAttributes();

        $fieldArr = array();
        $otherIndex = 0;
        $wizardIndex = 0;

        /** @var OrderAttribute $rec */
        foreach($orderAttributeColl as $rec)
        {
            $fieldId = $rec->getProductField()->getFieldset()->getName();
            $value = $rec->getCalculatedValue();

            $id = $fieldId;

            if($fieldId == 'OTHER')
            {
                $id = $fieldId . '_' . $otherIndex;
                $otherIndex++;
            }
            elseif($fieldId == 'WIZARD')
            {
                $id = $fieldId . '_' . $wizardIndex;
                $wizardIndex++;
            }

            $fieldArr[] = array('id' => $id, 'value' => $value);
        }
        
        // sets custom size (if exists)
        $customizableObj = $orderObj->getCustomizable();
        if($customizableObj)
        {
            $fieldArr[] = array('id' => 'SIZE_WIDTH', 'value' => $customizableObj->getWidth());
            $fieldArr[] = array('id' => 'SIZE_HEIGHT', 'value' => $customizableObj->getHeight());
        }
        
        $fieldArr[] = array('id' => 'INFORMATIONS', 'value' => $orderObj->getInformations());
        
        return $fieldArr;
    }

    /**
     * Returns parsed array (from REST, for Order -> setAttribute() method).
     * E.g.:
     * array(
     *       [COUNT] => 4
     *       [QUANTITY] => 1
     *       [SIZE] => 6
     *       [MATERIAL] => 1
     *       [PRINT] => 10
     *       [SIDES] => 3
     *       [74] => 16)
     * 
     * @param array $fieldArr
     * 
     * @return array $outputArr
     */
    private function parseRequestCustomFields(array $fieldArr)
    {
        // valid attributes
        $validFieldArr = array('COUNT', 'QUANTITY', 'SIZE', 'MATERIAL', 'PRINT',
            'SIDES', 'OTHER', 'WIZARD');

        // prepares output array
        $outputArr = array();
 
        // foreach proper field
        foreach($fieldArr as $key => $rec)
        {
            if(in_array($key, $validFieldArr))
            {
                // for WIZARD
                if($key == 'WIZARD')
                {
                    foreach($rec as $key2 => $rec2)
                    {
                        // different parameter for other (productFieldId needed)
                        $outputArr['WIZARD_'.$rec2['value']] = $rec2['value'];
                    }
                }    
                elseif($key == 'OTHER')
                {
                    foreach($rec as $key2 => $rec2)
                    {
                        // different parameter for other (productFieldId needed)
                        $outputArr[$key2] = $rec2['value'];
                    }
                }
                else
                {
                    $outputArr[$key] = $rec['value'];
                }
            }
        }
        
        return $outputArr;
    }
 
    /**
     * Returns array with prepared order attributes (parsed from Order -> getAttributeValues() format).
     * E.g.:
     * array(
     *       [COUNT] => 4
     *       [QUANTITY] => 1
     *       [SIZE] => 6
     *       [MATERIAL] => 1
     *       [PRINT] => 10
     *       [SIDES] => 3
     *       [74] => 16)
     * 
     * @param array $fieldArr
     * 
     * @return array 
     */
    private function parseOrderAttributeCustomFields(array $fieldArr)
    {
        // valid attributes
        $validFieldArr = array('COUNT', 'QUANTITY', 'SIZE', 'MATERIAL', 'PRINT',
            'SIDES', 'OTHER', 'WIZARD');
        
        // prepares output array
        $outputArr = array();
        
        foreach($fieldArr as $key => $rec)
        {
            $keyUpp = strtoupper($key);
            
            if(in_array($keyUpp, $validFieldArr))
            {      
                if(is_array($rec))
                {
                    // for OTHER
                    if($keyUpp == 'OTHER')
                    {
                        foreach($rec as $key2 => $rec2)
                        {
                            // different parameter for other (productFieldId needed)
                            $outputArr[$key2] = $rec2;
                        }
                    }   
                    
                    // for WIZARD
                    if($keyUpp == 'WIZARD')
                    {
                        foreach($rec as $key2 => $rec2)
                        {
                            // different parameter for other (productFieldId needed)
                            $outputArr['WIZARD_'.$rec2] = $rec2;
                        }
                    }        
                }
                else
                {
                    $outputArr[strtoupper($key)] = $rec;
                }
            }
        }
        
        return $outputArr;
    }
    
    /**
     * Returns fixed field array. Fixes OTHER empty fields when no value is
     * sent from form, e.g. unchecked checkbox in OTHER option.
     * 
     * @param array $fieldArr
     * @param Order $orderObj
     * @return array 
     */
    private function fixEmptyOtherFieldsToFormOrderArray($fieldArr, Order $orderObj)
    {
        // gets product other fields directly from product
        $productFieldColl = $orderObj->getProduct()->getProductOtherFields();
        $otherFieldArr = array();
        foreach($productFieldColl as $rec)
        {
            $otherFieldArr[] = array(
                'id' => $rec->getId(),
                'required' => $rec->getRequired(),
                'default_value' => $rec->getDefaultValue()
            );
        }

        // checks if sent array from form has this other key in product fields
        foreach($otherFieldArr as $rec)
        {       
            // OTHER key
            if(!in_array($rec['id'], array_keys($fieldArr)))
            {
                if ($rec['required']) 
                {
                    $fieldArr[$rec['id']] = $rec['default_value'];
                } 
                else 
                {
                    $fieldArr[$rec['id']] = FieldItemTable::getInstance()
                        ->getEmptyField()->getId();
                }
            }
        }  
        
        return $fieldArr;
    }
    
    /**
     * Remove coupon from package
     *
     * @param string $webapi_key
     * @param string $session_handle
     *
     * @return boolean
     */
    public function doRemoveCoupon($webapi_key, $session_handle)
    {
        $this->checkWebApi($webapi_key);
      
        $userSession = $this->checkSessionHandle($session_handle);

        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        $basketPackageObj = $userSession->getBasketPackage();
        
        $basketPackageObj->unlinkCoupon();
      
        return true;
    }
    
    /**
     * Updates client data
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @param string $clientId
     * @param array $data user data: array(
     *      // CLIENT object
     *      'id'              => '',
     *      'company_name'    => '',
     *      'city'            => '',
     *      'postal_code'     => '',
     *      'street'          => '',
     *      'id_no'           => '',
     * 
     *      // CLIENT ADDRESS
     *      's_company'         => '',
     *      's_city'            => '',
     *      's_postal_code'     => '',      
     *      's_address'         => '',
     * 
     *      // CREATE NEW CLIENT ADDRESS
     *      'clientAddress'     => 
     *          array('fullname' => '',
     *                'street' => '',
     *                'postcodeAndCity' => ''),
     * )
     *
     * @return array
     */
    public function doUpdateClientData($webapi_key, $session_handle, 
            $clientId = null, $data = array())
    {
        $this->checkWebApi($webapi_key);
        $userSession = $this->checkSessionHandle($session_handle);
      
        // if user is logged in
        if(!$userSession || !$userSession->isUserLoggedIn())
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_SESSION_INCORRECT);
        }

        // gets user from session
        $userObj = $userSession->getSfGuardUser();
        
        if($clientId)
        {
            $clientObj = ClientTable::getInstance()->getClientByIdAndUserId($clientId, $userSession->getUserId());
        }
        else
        {
            $clientObj = $userObj->getDefaultClient();
        }
        
        if(!$clientObj)
        {
            throw new MnumiRestServerException(MnumiRestServerClientException::E_EDIT_CLIENT_PERMISSIONS);
        }
        
        $conn = Doctrine_Manager::connection();
        try
        {
            $conn->beginTransaction();
            // CLIENT FORM SAVE
            $clientFormData = $this->prepareClientFormData($data);
            
            //if there is any data to set
            if($clientFormData)
            {
                $clientForm = new RestClientForm($clientObj, 
                        array('clientFormData' => $clientFormData));
                
                $clientForm->bind($clientFormData);
                
                if(!$clientForm->isValid())
                {
                    throw new MnumiRestServerException(MnumiRestServerException::E_CLIENT_FORM_INVALID);
                }
                
                $clientForm->save();
            }
            
            // Addresses form save
            
            // delivery address
            $deliveryArr = $this->prepareClientDeliveryData($data);
            
            if($deliveryArr)
            {
                // add client_id to delivery data
                $deliveryArr['client_id'] = $clientObj->getId();
                
                $deliveryAddressForm = new RestClientDeliveryForm($clientObj->getDefaultDelivery());
                
                $deliveryAddressForm->bind($deliveryArr);
            
                if(!$deliveryAddressForm->isValid())
                {
                    throw new MnumiRestServerException(MnumiRestServerException::E_CLIENT_ADDRESS_FORM_INVALID);
                }
                
                $deliveryAddressForm->save();
            }
            
            // new client address
            if(!empty($data['clientAddress']))
            {
                // if client address delete
                if(!empty($data['clientAddress']['deleteClientAddressId']))
                {
                    ClientAddressTable::getInstance()->find($data['clientAddress']['deleteClientAddressId'])->delete();
                }
                else
                {
                    $addressArr = $this->prepareClientAddressData($data, $clientObj);
                    $clientAddressForm = new RestClientAddressForm();

                    $clientAddressForm->bind($addressArr);
                    
                    if(!$clientAddressForm->isValid())
                    {
                        throw new MnumiRestServerException(MnumiRestServerException::E_CLIENT_ADDRESS_FORM_INVALID);
                    }
                    
                    $clientAddressForm->save();

                    // sets new client address as default
                    $basketPackageObj = $userObj->getBasketPackage();
                    
                    if($basketPackageObj)
                    {
                        $basketPackageObj->setClientAddress($clientAddressForm->getObject());
                    }
                    
                    // saves basket
                    $basketPackageObj->save();
                }
            }
            $conn->commit();
        }
        catch(Exception $e)
        {
            $conn->rollback();
            throw new MnumiRestServerException($e->getMessage());
        }

        // checks if sent array from form has this other key in product fields
        foreach($otherFieldArr as $rec)
        {       
            // OTHER key
            if(!in_array($rec, array_keys($fieldArr)))
            {
                $fieldArr[$rec] = FieldItemTable::getInstance()
                        ->getEmptyField()->getId();
            }
        }  
        
        return $fieldArr;
    }
    
    
    /**
     * Updates default client
     *
     * @param string $webapi_key
     * @param string $session_handle
     * @param int $clientId
     *
     * @return boolean
     */
   public function doChangeDefaultClient($webapi_key, $session_handle, $clientId)
   {
       $this->checkWebApi($webapi_key);
       $userSession = $this->checkSessionHandle($session_handle);
       
       // if user is logged in
       if(!$userSession || !$userSession->isUserLoggedIn())
       {
           throw new MnumiRestServerException(MnumiRestServerClientException::E_USER_NOT_LOGGED_IN);
       }
       
       $clientObj = ClientTable::getInstance()->find($clientId);
       if(!$clientObj)
       {
           throw new MnumiRestServerException(MnumiRestServerClientException::E_CHANGE_CLIENT_NOT_EXISTS);
       }
     
       // changes client (for basket and orders also)
       $userSession->getSfGuardUser()->changeClient($clientObj);
       
       return true;
   }
   
   /**
     * Prepares form fields for restClient form
     *
     * @param array $data
     * @return array
     */
    private function prepareClientFormData($data)
    {
        $mappingFormDataToRestUser = array(
            'company_name'   => 'fullname',
            'contact_person' => 'fullname',
            'city'           => 'city',
            'postal_code'    => 'postcode',
            'address'        => 'street',
            'id_no'          => 'tax_id',
            'country'        => 'country'
        );

        $clientFormData = array();

        foreach($mappingFormDataToRestUser as $key => $value)
        {
            if(isset($data[$key]))
            {
                $clientFormData[$value] = $data[$key];
            }
        }

        return $clientFormData;
    }
   
   /**
    * Prepares form fields for restClientDelivery form
    *
    * @param array $data
    * @return array
    */
   private function prepareClientDeliveryData($data)
   {
       $deliveryArr = array();
       if(!empty($data['s_city']) && !empty($data['s_postal_code']) 
               && !empty($data['s_address']) && !empty($data['s_company'])
               && !empty($data['s_country']))
       {
           $deliveryArr = array(
               'city' => $data['s_city'],
               'postcode' => $data['s_postal_code'],
               'street' => $data['s_address'],
               'fullname' => $data['s_company'],
               'country' => $data['s_country']
           );
       }
       
       return $deliveryArr;
   }
   
   /**
    * Prepares form fields for restClientAddress form
    *
    * @param array $data
    * @param Client $clientObj
    * @return array
    */
   private function prepareClientAddressData($data, Client $clientObj)
   {
       $addressArr = array(
           'postcodeAndCity' => $data['clientAddress']['postcodeAndCity'],
           'street' => $data['clientAddress']['street'],
           'fullname' => $data['clientAddress']['name'],
           'client_id' => $clientObj->getId(),
           'country' => $data['clientAddress']['country']
       );
     
       return $addressArr;
   }
   
   /**
    * Updates default client
    *
    * @param string $webapi_key
    * @param string $session_handle
    *
    * @return boolean
    */
   public function doPayWithLoyaltyPoints($webapi_key, $session_handle)
   {
       $this->checkWebApi($webapi_key);
       $userSession = $this->checkSessionHandle($session_handle);
     
       // if user is logged in
       if(!$userSession || !$userSession->isUserLoggedIn())
       {
           throw new MnumiRestServerException(MnumiRestServerClientException::E_USER_NOT_LOGGED_IN);
       }
        
        // gets user object
        $userObj = $userSession->getSfGuardUser();
        
        // gets basket package
        $basketPackageObj = $userObj->getBasketPackage();
        
        // pay basket with loyalty points
        $basketPackageObj->payWithLoyaltyPoints();

        return true;
   }
   
   /**
    * Returns basket real price including coupons and loyalty points
    *
    * @param string $webapi_key
    * @param string $session_handle
    *
    * @return int
    */
   public function doGetBasketRealPrice($webapi_key, $session_handle)
   {
       $this->checkWebApi($webapi_key);
       $userSession = $this->checkSessionHandle($session_handle);
     
       // if user is logged in
       if(!$userSession || !$userSession->isUserLoggedIn())
       {
           throw new MnumiRestServerException(MnumiRestServerClientException::E_USER_NOT_LOGGED_IN);
       }
     
       // gets basket package
       $basketPackageObj = $userSession->getSfGuardUser()->getBasketPackage();
       
       if(!$basketPackageObj)
       {
           throw new MnumiRestServerException(MnumiRestServerException::E_BASKET_DOES_NOT_EXISTS);
       }
     
       return array('price' => $basketPackageObj->getRealPrice());
   }

   /**
    * Validates delivery date
    *
    * @param string $date       format 'Y-m-d'
    *
    * @return string|null
    */
    private function validDeliveryAt($date)
    {
        $defaultDeliveryDays = sfConfig::get('app_default_delivery_days', 0);

        if(!$defaultDeliveryDays) return null;

        $today = new DateTime();
        $minDate = $today->modify('+' . $defaultDeliveryDays . ' day');
        $dateValidator = new sfValidatorDate(array(
            'min'   => $minDate->format('Y-m-d')
        ));

        try {
            $dateValidator->clean($date);
            return $date;
        }
        catch(sfValidatorError $e)
        {
            return null;
        }
    }

    /**
     * Give transport number and post order package
     *
     * @param string $webapi_key
     * @param integer $package_id
     * @param string $transport_number
     * @throws MnumiRestServerException
     */
    public function doPostPackage($webapi_key, $package_id, $transport_number) {

        $this->checkWebApi($webapi_key);

        $packageObj = OrderPackageTable::getInstance()->findOneById($package_id);

        if(!$packageObj instanceof OrderPackage) {
            throw new MnumiRestServerException('There is no order package with given ID: ' . $package_id);
        }

        if(!$packageObj->isCompleted()) {
            throw new MnumiRestServerException('Order package with ID: ' . $package_id . ' is not completed and can\'t be posted');
        }

        $packageObj->setTransportNumber($transport_number);
        $packageObj->save();

        Mnumi::getInstance()->getEventDispatcher()->notify(new sfEvent($this,
                                                        'package.posted',
                                                        array('obj' => $packageObj)));
    }

    /**
     * Update only quantity for order
     *
     * @param $webapi_key
     * @param $order_id
     * @param $page_count
     * @return bool
     * @throws MnumiRestServerException
     */
    public function doUpdateOrderPageCount($webapi_key, $order_id, $page_count) {
        $this->checkWebApi($webapi_key);
        $orderObj = OrderTable::getInstance()->find($order_id);
        if(!$orderObj instanceof Order) {
            throw new MnumiRestServerException('There is no order with given ID: ' . $order_id);
        }
        $attributes = $orderObj->getOrderAttributesAsArray();
        $attributes["QUANTITY"] = intval($page_count);
        $orderObj->setBulkAttributes($attributes);
        $orderObj->save();
        
        return true;
    }
}
