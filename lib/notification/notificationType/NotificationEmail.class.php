<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications Email
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class NotificationEmail extends NotificationSendType
{
    // notification object
    private $notificationType;
    
    /**
     * Compose message to send
     * 
     */
    function __construct(NotificationMessage $notification = null, $webapiKey = null)
    {
        parent::__construct($notification, $webapiKey);

        $notificationType = NotificationTypeTable::getInstance()->getEmail();
      
        if(!$notificationType)
        {
            throw new Exception('Notification type for email does not exist.');
        }
        
        $this->notificationType = $notificationType;
    }
    
    /**
     * Compose message to send
     * 
     * @return boolean
     */
    public function composeMessage($user, $message, $title = '', $bcc = null)
    {
        return $this->composeEmailAndSave($user, $message, $title,
            $this->notificationType, 
            self::getSenderEmail($this->webapiKey),
            self::getSenderName($this->webapiKey),
            $bcc
        );
    }

    /**
     * Sends email.
     *
     * @param sfMailer $mailer
     * @throws LogicException
     * @return boolean
     */
    public function send($mailer = null)
    {
        if(!$mailer instanceof sfMailer)
        {
            throw new LogicException('Incorrect sfMailer class object.');
        }

        $notification = $this->notification;

        if($notification && get_class($notification) == 'NotificationMessage')
        {
            try
            {
                $message = Swift_Message::newInstance()
                    ->setFrom(array($notification->getSender() => $notification->getSenderName()))
                    ->setTo(array($notification->getRecipient() => $notification->getRecipientName()))
                    ->setSubject($notification->getTitle())
                    ->setBody($notification->getMessagePlain());

                /**
                 * @see PAN-1204
                 */
                if($notification->getBcc())
                {
                    $message->setBcc($notification->getBcc());
                }
                $message->addPart($notification->getMessageHtml(), 'text/html');

                $mailer->send($message);

            }
            catch (Swift_ConnectionException $e)
            {
                $error = $e->getMessage(); 
            }
            catch (Swift_Message_MimeException $e) 
            {
                $error = $e->getMessage(); 
            }
            catch (Exception $e)
            {
                $error = $e->getMessage(); 
            }
            
            $notification->setSendAt(new Doctrine_Expression('NOW()'));
            
            if(isset($error))
            {
                $notification->setErrorMessage($error);
                $notification->save();
                return false;
            }
            $notification->save();
            return true;
        }
        
        return false;
    }

    public static function getSender($webapiKey)
    {
        return array(
            self::getSenderEmail($webapiKey) => self::getSenderName($webapiKey)
        );
    }

    /**
     * Get sender e-mail address
     *
     * @param string $webapiKey
     * @return string Email address
     */
    public static function getSenderEmail($webapiKey)
    {
        $result = sfConfig::get('app_notification_sender', 'info@system.com');

        $shopNotification = self::getShopNotification($webapiKey);

        if(is_array($shopNotification) && array_key_exists('sender', $shopNotification))
        {
            $result = $shopNotification['sender'];
        }

        return $result;
    }

    /**
     * Get sender e-mail address
     *
     * @param string $webapiKey
     * @return string Email address
     */
    public static function getSenderName($webapiKey)
    {
        $result = sfConfig::get('app_notification_sender_name', 'System Name');

        $shopNotification = self::getShopNotification($webapiKey);

        if(is_array($shopNotification) && array_key_exists('senderName', $shopNotification))
        {
            $result = $shopNotification['senderName'];
        }

        return $result;
    }

    /**
     * Get notification from shop configuration
     *
     * @param string $webapiKey
     * @return array|bool
     */
    private static function getShopNotification($webapiKey)
    {
        $shopKeyName = 'app_webapi_' . $webapiKey;

        if (sfConfig::has($shopKeyName)) {
            $shopConfiguration = sfConfig::get($shopKeyName);

            if (array_key_exists('notification', $shopConfiguration)) {
                $notifications = $shopConfiguration['notification'];

                return $notifications;
            }
        }

        return false;
    }
}

