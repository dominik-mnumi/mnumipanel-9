<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class InvoiceNotification extends Notifications
{
    public $notificationTemplateName = 'Invoice';

    /**
     * Creates instance of CalculationNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = array(), $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        // values are sample data
        $this->availableLocalShortcodeArr = array(
            'invoiceUrl' => array(
                'value' => sfConfig::get('app_shop_host', 'http://shop.tests.mnumi.com/').'/invoice/x',
                'desc' => 'invoice'
            )
        );

        if($obj && !$obj instanceof Invoice)
        {
            throw new Exception('Passed object is not instance of Order class.');
        }

        $this->setValuesOfAvailableLocalShortcodeArr();
    }
}
