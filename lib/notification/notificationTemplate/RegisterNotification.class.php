<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class RegisterNotification extends Notifications
{
    public $notificationTemplateName = 'Register';

    /**
     * Creates instance of RegisterNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @param string $webapiKey
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = array(), $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        $this->availableLocalShortcodeArr = array();

        if($obj && !$obj instanceof sfGuardUser)
        {
            throw new Exception('Passed object is not instance of sfGuardUser class.');
        }

        $this->setValuesOfAvailableLocalShortcodeArr();
    }

}
