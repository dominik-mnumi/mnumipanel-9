<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class LostPasswordPanelNotification extends Notifications
{
    public $notificationTemplateName = 'Lost password panel';

    /**
     * Creates instance of LostPasswordNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @param string $webapiKey
     *
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = array(), $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        // values are sample data
        $this->availableLocalShortcodeArr = array(
            'passwordTokenLink' => array(
                'value' => sfConfig::get('app_shop_host', 'http://shop.tests.mnumi.com/').'/forgot-password/1d450b6cf6ee985513d52321f810f7f4',
                'desc' => 'lost password token link'
            ),
            'passwordTokenLinkShop' => array(
                'value' => sfConfig::get('app_shop_host', 'http://shop.tests.mnumi.com/').'/forgot-password/1d450b6cf6ee985513d52321f810f7f4',
                'desc' => 'lost password token link (shop)'
            )
        );

        if($obj && !$obj instanceof sfGuardUser)
        {
            throw new Exception('Passed object is not instance of sfGuardUser class.');
        }

        $this->setValuesOfAvailableLocalShortcodeArr();
    }

}
