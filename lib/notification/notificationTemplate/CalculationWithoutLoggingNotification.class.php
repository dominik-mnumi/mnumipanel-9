<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class CalculationWithoutLoggingNotification extends Notifications
{
    public $notificationTemplateName = 'Calculation without logging';

    /**
     * Creates instance of CalculationWithoutLoggingNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @param string $webapiKey
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = array(), $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        // values are sample data
        $this->availableLocalShortcodeArr = array(
            'calculation' => array(
                'value' => ' - '.$this->i18NObj->__('Print type').': Print X - '.$this->myUserObj->formatCurrency(7)
                    .'<br />- '.$this->i18NObj->__('Other').': '.$this->i18NObj->__('Bindery').' - '.$this->myUserObj->formatCurrency(3.10)
                    .'<br /><br />RAZEM: '.$this->myUserObj->formatCurrency(10.10).' ('.$this->myUserObj->formatCurrency(12.42)
                    .' '.$this->i18NObj->__('incl. VAT').')',
                'desc' => 'prepared calculation content'
            ),
            'productName' => array(
                'value' => 'Flyers 1page',
                'desc' => 'product name'
            ),
            'shopUrlOrderPreview' => array(
                'value' => sfConfig::get('app_shop_host', 'http://shop.tests.mnumi.com/')
                    .sprintf(sfConfig::get('app_shop_url_orderpreview', 'order/%d/preview/%d'),
                        '99999',
                        '99999'),
                'desc' => 'shop url order preview'
            )
        );

        if($obj && !$obj instanceof Order)
        {
            throw new Exception('Passed object is not instance of Order class.');
        }

        $this->setValuesOfAvailableLocalShortcodeArr($externalArr);
    }
}
