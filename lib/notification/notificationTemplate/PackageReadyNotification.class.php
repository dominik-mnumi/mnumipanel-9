<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class PackageReadyNotification extends Notifications
{
    public $notificationTemplateName = 'Package ready';

    /**
     * Creates instance of PackageReadyNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @param string $webapiKey
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = array(), $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        // values are sample data
        $this->availableLocalShortcodeArr = array(
            'packageId' => array(
                'value' => '9999',
                'desc' => 'package id'
            ),
            'packageName' => array(
                'value' => '2014-01-01',
                'desc' => 'package name'
            )
        );

        if($obj && !$obj instanceof OrderPackage)
        {
            throw new Exception('Passed object is not instance of OrderPackage class.');
        }
        $this->setValuesOfAvailableLocalShortcodeArr();
    }
}
