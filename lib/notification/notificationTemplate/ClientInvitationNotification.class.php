<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class ClientInvitationNotification extends Notifications
{
    public $notificationTemplateName = 'Client invitation';

    /**
     * Creates instance of BuyNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @param string $webapiKey
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = array(), $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        // values are sample data
        $this->availableLocalShortcodeArr = array(
            'login' => array(
                'value' => 'john_login',
                'desc' => 'account login'
            ),
            'password' => array(
                'value' => 'john_password',
                'desc' => 'account password'
            ),
            'userName' => array(
                'value' => 'John Doe',
                'desc' => 'username'
            )
        );

        if($obj)
        {
            if(!$obj instanceof sfGuardUser)
            {
                throw new Exception('Passed object is not instance of sfGuardUser class.');
            }

            $this->setValuesOfAvailableLocalShortcodeArr();
        }
    }

    /**
     * Prepares availableLocalShortcodeArr.
     */
    public function setValuesOfAvailableLocalShortcodeArr($externalArr = null)
    {
        // gets first user and generates password
        $userObj = $this->obj;

        $password = sfGuardUserTable::getInstance()->generatePassword();
        $userObj->setPassword($password);
        $userObj->save();

        foreach($this->availableLocalShortcodeArr as $key => $rec)
        {
            $methodName = $this->getNotificationShortcodeMethodName($key);

            if($key == 'login' || $key == 'userName')
            {
                $this->availableLocalShortcodeArr[$key]['value'] = $userObj->getEmailAddress();
            }
            elseif($key == 'password')
            {
                $this->availableLocalShortcodeArr[$key]['value'] = $password;
            }
            elseif(method_exists($this->obj, $methodName))
            {
                $this->availableLocalShortcodeArr[$key]['value'] = $this->obj->$methodName();
            }
        }
    }

}
