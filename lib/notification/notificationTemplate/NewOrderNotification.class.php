<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class NewOrderNotification extends Notifications
{
    public $notificationTemplateName = 'New order';

    /**
     * Creates instance of NewOrderNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @param string $webapiKey
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = array(), $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        // values are sample data
        $this->availableLocalShortcodeArr = array(
            'orderId' => array(
                'value' => '9999',
                'desc' => 'order number'
            ),
            'orderName' => array(
                'value' => 'Flyers 1page',
                'desc' => 'order title'
            )
        );

        if($obj && !$obj instanceof Order)
        {
            throw new Exception('Passed object is not instance of Order class.');
        }

        $this->setValuesOfAvailableLocalShortcodeArr();
    }

}
