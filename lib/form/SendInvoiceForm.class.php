<?php

/*
 * This file is part of the MnumiCore3 package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SendInvoiceForm class
 *
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
class SendInvoiceForm extends BaseInvoiceForm
{

    public function configure()
    {
        parent::configure();

        $this->setWidget('to', new sfWidgetFormInputText());
        $this->setWidget('subject', new sfWidgetFormInputText());
        $this->setWidget('invoice', new sfWidgetFormTextarea(array(), array('id' => 'email_invoice_textarea')));

        $this->widgetSchema->setLabels(array(
                'to' => 'E-mail',
                'subject' => 'Subject',
            ));

        $this->setValidator('to', new sfValidatorEmail(array('required' => true),
                array('required' => 'Email required',
                    'invalid' => 'Email is invalid')));
        $this->setValidator('subject', new sfValidatorString(array('max_length' => 50,
                    'required' => true),
                array('required' => 'Subject required')));
        $this->setValidator('invoice', new sfValidatorString(array('required' => false)));

        $this->getWidgetSchema()->setNameFormat('send_invoice[%s]');

        $this->useFields(array('to', 'subject', 'invoice'));
    }
}
