<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FieldItemPrice form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */

class FieldItemPriceTabForm extends FieldItemPriceForm
{
    public function configure()
    {
        parent::configure();

        $defaultPricelistId = PricelistTable::getInstance()->getDefaultPricelist()->getId();

        $query = Doctrine::getTable('Pricelist')->createQuery('p')
            ->where('p.id <> ?', $defaultPricelistId);

        $this->setWidget('pricelist_id', new sfWidgetFormDoctrineChoice(
            array(
                'model' => 'Pricelist',
                'query' => $query,
                'add_empty' => true
            ),
            array(
                'class' => "selectPricelist"
            )
        ));
    }

}
