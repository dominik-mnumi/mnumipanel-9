<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SettingsPrintForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */

class SettingsPrintForm extends SettingsForm
{
    protected static $measureUnits = array('Per page','Square metre');
    
    public function configure()
    {
        parent::configure();
        
        $this->useFields(array('name', 'field_id', 'label', 'measure_unit_id'));
    }

    /**
     * Synchronize prices in pricelist
     * Loaded from doSave method
     *
     * @param FieldItemPrice $fieldItemPriceObj
     * @param array $pricelist
     * @param null $conn
     * @throws Exception
     */
    protected function doSavePrices($fieldItemPriceObj, $pricelist, $conn = null)
    {
        // foreach defined type
        foreach($pricelist as $typeKey => $type)
        {
            // check if type key is correct
            if($typeKey != FieldItemPriceTypeTable::$price_simplex && $typeKey == FieldItemPriceTypeTable::$price_duplex)
            {
                throw new Exception('Wrong type key for Print: ' .$typeKey);
            }

            $fieldItemPriceObj->synchronizePricesOfSimplexAndDuplex($type);
        }
    }
}