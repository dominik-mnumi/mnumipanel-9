<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FieldItemPriceRangeCollectionForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */

class FieldItemPriceRangeCollectionForm extends sfForm
{
    public function configure()
    {
        if(!$field_item_price = $this->getOption('field_item_price'))
        {
            throw new InvalidArgumentException('You must provide FieldItemPrice object');
        }
        for($i = 0; $i < $this->getOption('size', 1); $i++)
        {
            $fieldItemPriceRange = new FieldItemPriceRange();
            $fieldItemPriceRange->FieldItemPrice = $field_item_price;
            $form = new FieldItemPriceRangeForm($fieldItemPriceRange);
            $this->embedForm($i, $form);
        }
    }
}