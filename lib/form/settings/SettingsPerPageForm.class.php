<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SettingsPerPageForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class SettingsPerPageForm extends BaseForm
{
    protected static $selectOptions = array(10 => '10',25 => '25',50 => '50',100 => '100');
    
    public function configure()
    {
        $this->setWidgets(array(
            'categoryId'    => new sfWidgetFormInputHidden(),
            'options'        => new sfWidgetFormSelect(array('choices' => self::$selectOptions))
        ));
        $this->widgetSchema->setNameFormat('settingsPerPage[%s]');
     
        $this->setValidators(array(
            'categoryId'    => new sfValidatorInteger(array('required' => true)),
            'options'       => new sfValidatorChoice(array('choices' => array_keys(self::$selectOptions)))
        ));
    }
}