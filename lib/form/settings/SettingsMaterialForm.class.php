<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SettingsMaterialTypeForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */

class SettingsMaterialForm extends SettingsForm
{ 
    public function configure()
    {
        parent::configure();
            
        // gets field item
        $fieldItemObj = $this->getObject();
        
        $fieldItemMaterialObj = $fieldItemObj->getFieldItemMaterial()->count()
                ? $fieldItemObj->getFieldItemMaterial()->getFirst()
                : new FieldItemMaterial();
        $fieldItemMaterialObj->setFieldItem($fieldItemObj);

        // creates material form
        $fieldItemMaterialForm = new FieldItemMaterialForm($fieldItemMaterialObj);
        
        $printsizeId = $this->getOption('printsize_id');
        if($printsizeId)
        {
            $fieldItemMaterialForm->setDefault('printsize_id', $printsizeId);
        }

        $this->embedForm('material', $fieldItemMaterialForm); 
        
        $this->useFields(array(
            'name', 
            'field_id', 
            'label',
            'measure_unit_id',
            'material'));
    }
    
    public function isValid()
    {
        if(!parent::isValid())
        {
            return false;
        }

        // gets pricelist array from external
        $pricelistArr = $this->getOption('pricelistArr', array());

        $defaultPricelist = $pricelistArr[PricelistTable::getInstance()->getDefaultPricelist()->getId()];

        $validatorPricesRequired = new sfValidatorDefaultPricelistPricesRequired();
        $error = $validatorPricesRequired->clean($defaultPricelist);
        
        if($error)
        {
            $this->getErrorSchema()->addError($error);
            return false;
        }

        return true;
    }

    /**
     * Synchronize prices in pricelist
     * Loaded from doSave method
     *
     * @param FieldItemPrice $fieldItemPriceObj
     * @param array $pricelist
     * @param null $conn
     * @throws Exception
     */
    protected function doSavePrices($fieldItemPriceObj, $pricelist, $conn = null)
    {
        // foreach defined type
        foreach($pricelist as $typeKey => $prices)
        {
            if($typeKey != FieldItemPriceTypeTable::$price_item)
            {
                throw new Exception('Wrong type key for Material: ' .$typeKey);
            }

            $type = $fieldItemPriceObj->getFieldItem()->getMeasureUnit()->getFormalName();

            $fieldItemPriceObj->synchronizePricesOfType($type, $prices);

            // cleanup other settings
            if($type == FieldItemPriceTypeTable::$price_page)
            {
                $fieldItemPriceObj->synchronizePricesOfType(FieldItemPriceTypeTable::$price_square_metre, array());
            }
            else
            {
                $fieldItemPriceObj->synchronizePricesOfType(FieldItemPriceTypeTable::$price_page, array());
            }
        }
    }
}