<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FieldItemPriceCollection form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */

class FieldItemPriceCollectionForm extends sfForm
{
    public function configure()
    {
        if(!$field_item = $this->getOption('field_item'))
        {
            throw new InvalidArgumentException('You must provide FieldItem object');
        }
        
        for($i = 0; $i < $this->getOption('size', 1); $i++)
        {
            $fieldItemPrice = new FieldItemPrice();           
            $fieldItemPrice->FieldItem = $field_item;
            $form = new FieldItemPriceTabForm($fieldItemPrice);
            $this->embedForm($i, $form);
        }
    }
}