<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SettingsSizeForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */

class SettingsSizeForm extends SettingsForm
{ 
    public function configure()
    {
        parent::configure();

        // gets field item
        $fieldItemObj = $this->getObject();

        $fieldItemSizeObj = $fieldItemObj->getFieldItemSize()->count()
                ? $fieldItemObj->getFieldItemSize()->getFirst()
                : new FieldItemSize();
        $fieldItemSizeObj->setFieldItem($fieldItemObj);

        $this->embedForm('size', new FieldItemSizeForm($fieldItemSizeObj));
        
        $this->useFields(array('name', 'size'));
    }
}