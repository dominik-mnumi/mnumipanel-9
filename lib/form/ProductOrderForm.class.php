<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductOrderForm
 *
 * @package    mnumishop
 * @subpackage form
 * @author     Adam Marchewicz
 */
class ProductOrderForm extends BaseForm
{
    static $type = array(
        1 => 'string',
        4 => 'select',
        10 => 'size',
        20 => 'number'
    );
    
    protected $size_metric;
    
    // custom validators for fields
    protected $customValidators = array(
        'count' => 
            array('type' => 'integer',
                  'options' => array('required' => true), 
                  'messages' => array('required' => 'Field "count" is required', 'invalid' => 'Field "count" is not an integer.')),
        'quantity' => 
            array('type' => 'integer',
                  'options' => array('required' => true), 
                  'messages' => array('required' => 'Field "quantity" is required', 'invalid' => 'Field "quantity" is not an integer.'))
        );

    public function configure()
    {
        parent::configure();

        $fields = $this->getOption('productFields');

        $otherWidgetArray = array();

        $hasAcquiredStock = $this->getOption('hasAcquiredStock');
        $data = $this->getDefaults();
        foreach($fields as $row)
        {
            $fieldName  = $row['field_name'];
            $fieldParam = $row['field_param'];
            $fieldLabel = $row['field_title'];
            $fieldId    = $row['field_id'];
            $fieldDefaultValue = $row['field_def_val'];

            if($fieldName == 'OTHER')
            {
                // Filter results for orders which do not have already acquired resources
                $fieldParam = $this->getAvailableStockFields($fieldParam, $hasAcquiredStock, $data['other'][$fieldId]);

                $choiceArray = $this->getChoices($fieldParam);

                // if OTHER which have more than one option
                if(count($fieldParam) > 1 || $row['field_required'])
                {
                    $widget = new sfWidgetFormSelect(
                            array('choices' => $choiceArray,
                                'label' => $fieldLabel));

                    if(isset($row['only_closed_param']))
                    {
                        $widget->setAttribute('data-onlyclosed', $row['only_closed_param']);
                    }
                    
                    $otherWidgetArray[$row['field_id']] = $widget;
                }
                // for OTHER with one option
                else if(count($fieldParam) == 1)
                {
                    $value = array_keys($choiceArray);
                    $value = $value[0];

                    $otherWidgetArray[$row['field_id']] = new sfWidgetFormInputCheckbox(
                            array('label' => $fieldLabel,
                                'default' => false),
                            array('class' => 'mini-switch',
                                'value' => $value,
                                'val' => $value));
                }
            }
            /* General product fields. (sides, quantity, count, etc...) */
            else if($fieldName != 'WIZARD')
            {
                $type_id = (integer) $row['field_type'];
                $type = 'renderFormElement'.ucfirst(self::$type[$type_id]);
                $name = strtolower($fieldName);
                $this->$type($name, $row);
                if(!$this->hasDefault($name))
                {
                    $this->setDefault($name, $fieldDefaultValue);
                }
                $this->widgetSchema->setLabel($name, $fieldLabel);
            }
        }

        if(!empty($otherWidgetArray))
        {
            $this->setWidget('other', new sfWidgetFormSchema($otherWidgetArray));
            $this->setValidator('other', new sfValidatorPass());
        }

        $this->getWidgetSchema()->setNameFormat('order[%s]');
    }

    public function renderFormElementNumber($name, $row)
    {
        $attributes = array();
        if($row['field_min_val'] !== null)
        {
            $attributes['min'] = $row['field_min_val'];
        }
        if($row['field_max_val'] != 0 && $row['field_max_val'] !== null)
        {
            $attributes['max'] = $row['field_max_val'];
        }
        $input = new sfWidgetFormInputNumber(array(), $attributes);
        $this->setWidget($name, $input);
        $this->setValidator($name, new sfValidatorNumber($attributes));
    }

    public function renderFormElementString($name, $row)
    {
        $this->setWidget($name, new sfWidgetFormInputText());
        
        // custom validator (for different than string)
        if(array_key_exists($name, $this->customValidators))
        {
            $validatorClassName = 'sfValidator'.ucfirst($this->customValidators["$name"]['type']);
            $this->setValidator($name, new $validatorClassName($this->customValidators["$name"]['options'], $this->customValidators["$name"]['messages']));
        }
        else
        {
            $this->setValidator($name, new sfValidatorString());
        }
    }

    public function renderFormElementSelect($name, $row)
    {
        $widget = new sfWidgetFormSelect(
                array('choices' => $this->getChoices($row['field_param'])));        
        if(isset($row['only_closed_param']))
        {
            $widget->setAttribute('data-onlyclosed', $row['only_closed_param']);
        }
        $this->setWidget($name, $widget);
        $this->setValidator($name, new sfValidatorChoice(array('choices' => array_keys($row['field_param']))));
    }

    public function renderFormElementSize($name, $row)
    {
        $special_choiceid = false;
        foreach($row['field_param'] as $row_item)
        {
            if($row_item['type'] == 'special')
            {
                $special_choiceid = $row_item['id'];
            }
        }
        $choices = $this->getChoices($row['field_param']);

        if($special_choiceid)
        {
            // get special choice
            $special_choice_item = array($choices[$special_choiceid]);

            // remove from list of strings
            unset($choices[$special_choiceid]);

            // put as last of choices
            list($special_value) = $special_choice_item;
            $choices[$special_choiceid] = $special_value;
        }

        $this->setWidget($name, new sfWidgetFormSelect(array('choices' => $choices), array('class' => ($special_choiceid) ? 'with_special_choice' : '')));
        $this->setValidator($name, new sfValidatorChoice(array('choices' => array_keys($choices))));

        if($special_choiceid)
        {
            // width
            $this->setWidget($name.'_width',
                    new sfWidgetFormInputText(array('label' => 'Width'), 
                            array('class' => 'size size_width', 
                            )
                    )
            );
            $this->setValidator($name.'_width',
                    new sfValidatorString(array('required' => false)));

            // height
            $this->setWidget($name.'_height',
                    new sfWidgetFormInputText(array('label' => 'Height'), 
                            array('class' => 'size size_height',
                            )
                    )
            );
            $this->setValidator($name.'_height',
                    new sfValidatorString(array('required' => false)));
        }
    }
  
    private function getChoices($field_param)
    {
        $choices = array();
        $emptyField = false;

        foreach($field_param as $value)
        {
            if($value['name'] == FieldItemTable::$emptyField)
            {
                $emptyField = $value['id'];
                continue;
            }


            $choices[$value['id']] = $value['name'];
        }

        if($emptyField)
        {
            return array($emptyField => FieldItemTable::$emptyField) + $choices;
        }

        return $choices;
    }

    /**
     * Helper function to filter out unavailable FieldItems
     *
     * @param array
     * @param boolean Does Order is in state that it already acquired resources
     *                from Stock?
     * @param integer Current value for OrderAttribute field
     *
     * @return array Associative array keyed by FieldItem::Id of FieldItems
     *               available in stock or not using stock at all.
     */
    private function getAvailableStockFields($fieldParam, $hasAcquiredStock, $currentValue)
    {
        $availableFields = array();
        $fieldItemTable = FieldItemTable::getInstance();
        $fieldItems = $fieldItemTable->findByIds(array_keys($fieldParam));
        foreach ($fieldItems as $fieldItem) {
            $fieldId = $fieldItem->getId();
            if ($fieldItem->isUsedInStock()) {
                /* Display FieldItem if it is used in Stock and is Available to
                 * acquire OR Order is in state that it have to acquire values
                 * earlier and option is currently selected
                 */
                if ($fieldItem->isAvailableInStock() ||
                    ($hasAcquiredStock && $currentValue == $fieldId)) {
                    $availableFields[$fieldId] = $fieldParam[$fieldId];
                }
            }
            // Is not used in stock, add it
            else {
                $availableFields[$fieldId] = $fieldParam[$fieldId];
            }
        }
        return $availableFields;
    }
}
