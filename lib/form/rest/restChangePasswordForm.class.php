<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended restChangePasswordForm form
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class RestChangePasswordForm extends sfGuardUserForm
{
    public function configure()
    {
        parent::configure();
        
        $this->disableCSRFProtection();
        $this->useFields(array('password'));
        
        $this->setWidget('password_current', new sfWidgetFormInputHidden());
        $this->setValidator('password_current', new sfValidatorString(array('required' => true)));

        $this->getValidatorSchema()->setPostValidator(
            new sfValidatorCallback(array(
                    'callback' => array($this, 'validCurrentPassword'))
            )
        );
  
    }
    
    /**
     * Check if current password match
     * 
     * @param sfValidator $validator
     * @param array $values
     * @return array 
     */
    public function validCurrentPassword($validator, $values)
    {
        $obj = $this->getObject();

        if(!$obj->checkPassword($values['password_current']))
        {
            $error = new sfValidatorError($validator, 'Field "Current password" does not match');
            throw new sfValidatorErrorSchema($validator, array('password_old' => $error));
        }
        return $values;
    }
 
}

