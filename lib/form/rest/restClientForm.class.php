<?php
/*
 * This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * RestClientForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class RestClientForm extends BaseClientForm
{
    public function configure()
    {
        parent::configure();
        
        $data = $this->getOption('clientFormData');
        
        $this->disableCSRFProtection();
        $widgets = array();
        $validators = array();
        
        if(isset($data['fullname']))
        {
            $widgets['fullname'] = new sfWidgetFormInputHidden();
            $validators['fullname'] = new sfValidatorString(array('required' => true));
        }
        if(isset($data['city']))
        {
            $widgets['city'] = new sfWidgetFormInputHidden();
            $validators['city'] = new sfValidatorString(array('required' => false));
        }
        if(isset($data['postcode']))
        {
            $widgets['postcode'] = new sfWidgetFormInputHidden();
            $validators['postcode'] = new sfValidatorString(array('required' => true));
        }
        if(isset($data['street']))
        {
            $widgets['street'] = new sfWidgetFormInputHidden();
            $validators['street'] = new sfValidatorString(array('required' => false));
        }
        if(isset($data['tax_id']))
        {
            $widgets['tax_id'] = new sfWidgetFormInputHidden();
            $validators['tax_id'] = new sfValidatorString(array('required' => true)); // tax id validation moved to postValidator
        }
        if(isset($data['country']))
        {
            $widgets['country'] = new sfWidgetFormI18nChoiceCountry(); 
            $validators['country'] = new sfValidatorI18nChoiceCountry();
        }
        
        $this->setWidgets($widgets);
        $this->setValidators($validators);

        if(isset($data['tax_id']))
        {
            // validators
            $taxValidator = new sfValidatorSchemaTaxId(
                'tax_id',
                array(),
                array('invalid' => 'Field "Tax ID" is not valid')
            );

            // sets post validators
            $this->mergePostValidator(new sfValidatorAnd(
                    array($taxValidator))
            );
        }
    }
}
