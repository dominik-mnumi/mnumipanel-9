<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * RestUserForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class RestUserForm extends PluginsfGuardUserForm
{
    public function configure()
    {
        $this->disableCSRFProtection();
        
        $userFormData = $this->getOption('userFormData');
        $widgets = array();
        $validators = array();
        
        if(isset($userFormData['first_and_last_name']))
        {
            $widgets['first_and_last_name'] = new sfWidgetFormInputHidden();
            $validators['first_and_last_name'] = new sfValidatorString(array('required' => true));
        }
        
        if(isset($userFormData['email_address']))
        {
            $widgets['email_address'] = new sfWidgetFormInputHidden();
            $validators['email_address'] = new sfValidatorEmail(array('required' => true));
        }
        
        if(isset($userFormData['email_notification']))
        {
            $widgets['email_notification'] = new sfWidgetFormInputHidden();
            $validators['email_notification'] = new sfValidatorBoolean(array('required' => false));
        }
        
        if(isset($userFormData['sms_notification']))
        {
            $widgets['sms_notification'] = new sfWidgetFormInputHidden();
            $validators['sms_notification'] = new sfValidatorBoolean(array('required' => false));
        }
        
        $this->setWidgets($widgets);
        $this->setValidators($validators);
    }
}