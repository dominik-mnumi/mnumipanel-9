<?php
/*
 * This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * RestClientAddressForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class RestClientAddressForm extends BaseClientAddressForm
{
    public function configure()
    {
        parent::configure();
        
        $this->disableCSRFProtection();
        
        $this->setWidget('postcodeAndCity', new sfWidgetFormInputHidden());

        $this->setValidator('postcodeAndCity', new sfValidatorPostcodeAndCity(
                array('max_length' => 150, 'required' => true)
            )
        );
        
        $this->useFields(array('fullname', 'postcodeAndCity', 'street', 
            'client_id', 'country'));
    }   
}
