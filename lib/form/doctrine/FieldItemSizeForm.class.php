<?php

/**
 * FieldItemSize form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FieldItemSizeForm extends BaseFieldItemSizeForm
{
  public function configure()
  {
       $this->setWidget('width', new sfWidgetFormInputText(
              array(), 
              array('size' => 5)));
      $this->setWidget('height', new sfWidgetFormInputText(
              array(), 
              array('size' => 5)));
      
      $this->setValidator('width', new sfValidatorNumberExtended(
              array('required' => true), 
              array('required' => 'First field in print size (width) is required')));
      $this->setValidator('height', new sfValidatorNumberExtended(
              array('required' => true), 
              array('required' => 'Second field in print size (height) is required')));

      $this->getWidgetSchema()->setLabels(array(
          'width' => 'Print size',
          'height' => 'Print size'
      ));
      
      $this->useFields(array('width', 'height'));
  }
}
