<?php

/**
 * OrderWorklog form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderWorklogForm extends BaseOrderWorklogForm
{
  public function configure()
  {
      unset($this['created_at'], $this['updated_at'], $this['user_id'], $this['order_id'], $this['work'], $this['notice']);
      
      $this->setWidget('material', new sfWidgetFormDoctrineChoice(array('model' => 'FieldItem', 'add_empty' => false, 
          'query' => FieldItemTable::getInstance()->getFieldItemQueryByFieldsetName('MATERIAL'), 'method' => 'getFieldLabel')));
      
      $this->validatorSchema['value']->setMessage('invalid', 'Field "value" is invalid. Only numbers allowed.');

      $this->setValidator('material', new sfValidatorDoctrineChoice(array('model' =>'FieldItem', 
          'query' => FieldItemTable::getInstance()->getFieldItemQueryByFieldsetName('MATERIAL'))));
      
      $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
  }
  
  protected function doSave($con = null)
  {
      $material = FieldItemTable::getInstance()->findOneById($this->getValue('material'));
      
      $this->getObject()->setNotice($material->getFieldLabel());
      //save object
      parent::doSave($con);
      
      return $this->getObject();
  }
}
