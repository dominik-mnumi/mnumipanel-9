<?php

/**
 * PaymentPricelist form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PaymentPricelistForm extends BasePaymentPricelistForm
{
  public function configure()
  {
  }
}
