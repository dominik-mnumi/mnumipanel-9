<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductField form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFieldForm extends BaseProductFieldForm
{
    public function configure()
    {     
        //sets fieldset for form
        $fieldsetObj = $this->getOption('fieldsetObj');
        if(!empty($fieldsetObj))
        {
            $this->setFieldset($fieldsetObj);
        }
        
        //label checkbox 
        $this->setWidget('change_label_checkbox', new sfWidgetFormInputCheckbox(
            array('label' => 'Change label'),
            array('class' => 'change_checkbox_product_form')));

        //sets checkbox true if change label
        if('' != $this->getObject()->getLabel())
        {
            $this->getWidget('change_label_checkbox')->setOption('default', true);
        }

        $this->setValidator('change_label_checkbox', new sfValidatorPass());

        // Count min-max
        $this->setWidget('min', new sfWidgetFormInputNumber(array('label' => 'The minimum value')));
        $this->setWidget('max', new sfWidgetFormInputNumber(array('label' => 'The maximum value')));
        $this->setValidator('min', new sfValidatorNumber(array('required' => false, 'min' => 0)));
        $this->setValidator('max', new sfValidatorNumber(array('required' => false, 'min' => 0)));


        $this->getWidget('visible')->setLabel('Visible in the shop');
        $this->getWidget('required')->setLabel('Required');
        $this->getWidget('label')->setLabel('Change label');

        $this->getWidget('default_value')->setLabel('Default value');
        $this->getWidget('package_order')->setLabel('Package order (from Fixed prices)');  
        
        $this->getWidget('required')->setAttribute('class', 'requiredCheckbox'); 
    }

    /**
     * Conditional validator when change label is checked.
     * Used in inherited classes.
     *
     * @param sfValidator $validator
     * @param array $values
     * @return array
     */
    public function checkLabel($validator, $values)
    {
        if('OTHER' != $this->getFieldset()->getName())
        {
            if(!empty($values['change_label_checkbox']) && empty($values['label']))
            {
                $error['label'] = new sfValidatorError($validator, $this->getFieldset()->getLabel().' - "Change label" is required.');
                throw new sfValidatorErrorSchema($validator, $error);
            }
        }
        else
        {
            if(!empty($values['valid']) && empty($values['label']))
            {
                $error['label'] = new sfValidatorError($validator, $this->getFieldset()->getLabel().' - "Change label" is required.');
                throw new sfValidatorErrorSchema($validator, $error);
            }
        }
        return $values;
    }

    protected function doSave($con = null)
    {       
        return parent::doSave($con);
    }

    public function setFieldset($fieldsetObj)
    {
        $this->getObject()->setFieldset($fieldsetObj);
    }

    public function getFieldset()
    {
        return $this->getObject()->getFieldset();
    }

    public function setProduct($productObj)
    {
        $this->getObject()->setProduct($productObj);
    }

    public function getProduct()
    {
        return $this->getObject()->getProduct();
    }
    
    public function getFieldsetLabel()
    {
        return $this->getObject()->getFieldsetLabel();
    }
}
