<?php

/**
 * Order form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderForm extends BaseOrderForm
{

    public function configure()
    {
        parent::configure();

        unset($this['booked_at'], $this['edited_at'], $this['created_at'], $this['updated_at'], $this['booked_at'], $this['tax'],
              $this['to_book'], $this['client_id'], $this['user_id'], $this['order_status_name'], $this['shelf_id'],
              $this['editor_id'], $this['version'], $this['product_id'], $this['order_package_id'], $this['tax_value'],
              $this['shop_name'], $this['accepted_at'], $this['discount_value'], $this['discount_type'], $this['external_id'],
              $this['base_amount'], $this['base_cost'], $this['tax_value'], $this['traders_amount'], $this['price_net'],
              $this['discount_amount'], $this['total_amount'], $this['total_cost'], $this['custom_cost'], $this['client_order_number']
        );

        // sets class for notice widget
        $this->getWidget('notice')->setAttribute('class', 'full-width');
        $this->getWidget('pricelist_id')->setOption('add_empty', 'Default customer pricelist');

        $this->setWidget('name', new sfWidgetFormInputHidden());
        $this->setWidget('price_net', new sfWidgetFormInputHidden());
        $this->setValidator('name', new sfValidatorString(
            array(
                'required' => true,
                'max_length' => 100
            ),
            array(
                'required' => 'Order name required',
                'max_length' => 'Order name is too long (%max_length%)'
            )
        ));
        $this->setValidator('trader', new sfValidatorPass());
        $this->setValidator('wizard', new sfValidatorPass());
        $this->setValidator('cost', new sfValidatorPass());

        $this->widgetSchema->setLabel('informations', 'Additional informations');

        $defaults = $this->getObject()->getDefaults();

        $attributes = new ProductOrderForm(
            $defaults,
            array(
                'productFields' => $this->getObject()->getProductFields(),
                'hasAcquiredStock' => $this->getObject()->hasAcquiredStock(),
            )
        );

        $this->embedForm('attributes', $attributes);
    }

    /**
     * Saves embed forms - attributes.
     */
    public function saveEmbeddedForms($con = null, $forms = null)
    {
        $order = $this->getObject();

        $order->setPriceBlock(!empty($this->values['price_block']) ? true : false);

        $oldAttributes = $order->getOrderAttributes();
        $oldOtherIds = array();
        foreach($oldAttributes as $attribute)
        {
            if($attribute->getProductField()->getFieldset()->getName() == 'OTHER')
            {
                $oldOtherIds[] = $attribute->getProductField()->getId();
            }
        }

        $newValues = $this->getValues();

        // if others does exist
        $newValues['attributes']['other'] = isset($newValues['attributes']['other'])
            ? $newValues['attributes']['other']
            : array();
        $newOtherIds = array_keys($newValues['attributes']['other']);

        $customField = FieldItemTable::getInstance()->getCustomizableField();

        $saveAttributes = array();

        foreach($newValues['attributes'] as $key => $value)
        {
            if($key == 'other')
            {
                foreach($value as $otherKey => $otherValue)
                {
                    $saveAttributes[$otherKey] = $otherValue;
                    unset($oldOtherIds[$otherKey]);
                }
                continue;
            }

            // if customizable field
            if ($key == 'size') {
                if($value == $customField->getId())
                {
                    $order->setCustomizable($newValues['attributes']['size_width'], $newValues['attributes']['size_height']);
                }
                else
                {
                    $order->deleteCustomizable();
                }
            }

            // if extra fields but no customizable option then continue
            if(in_array($key, array('size_width', 'size_height')))
            {
                continue;
            }

            $saveAttributes[$key] = $value;
        }


        $order->setBulkAttributes($saveAttributes);

        // delete unchecked option
        foreach(array_diff($oldOtherIds, $newOtherIds) as $key)
        {
            $order->deleteAttribute($key);
        }

        $order->save();
    }

}
