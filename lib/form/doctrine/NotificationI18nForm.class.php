<?php

/**
 * NotificationI18n form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NotificationI18nForm extends BaseNotificationI18nForm
{
  public function configure()
  {
  }
}
