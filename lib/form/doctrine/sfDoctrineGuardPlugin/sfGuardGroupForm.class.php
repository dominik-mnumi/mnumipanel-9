<?php

/**
 * sfGuardGroup form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardGroupForm extends PluginsfGuardGroupForm
{
    public function configure()
    {       
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
        
        $this->getValidator('name')->setOption('required', true);
        $this->getValidator('name')->setMessage('required', 'Required');
        
        $this->getValidator('description')->setOption('required', true);
        $this->getValidator('description')->setMessage('required', 'Required');
        
        $this->useFields(array('name', 'description'));
    }
    
    /**
     * Prevent user list from save (we only want to save permissions)
     * Remove this method when you want to view user_list on form and save connections (group - user)
     * 
     * @param $con
     */
    public function saveUsersList($con = null)
    {
        return false;
    }
}
