<?php

/**
 * myGuardChangeUserPasswordForm for changing a users password
 *
 * @package    mnumicore
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfGuardChangeUserPasswordForm.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class myGuardChangeUserPasswordForm extends BasesfGuardChangeUserPasswordForm
{
    public function configure()
    {
        $this->widgetSchema['password']->setLabel('New password');
        $this->validatorSchema['password']->setMessage('required', 'Field "Password" required');
        $this->validatorSchema['password_again']->setMessage('required', 'Field "Confirm password" is required');
        $this->widgetSchema->setNameFormat('change_password[%s]');
    }
    
}