<?php

class myGuardFormSignin extends sfGuardFormSignin
{
  /**
   * @see sfForm
   */
  public function configure()
  {
      $this->getWidgetSchema()->setLabels(array(
          'username' => 'Username',
          'password' => 'Password',
          'remember' => ' Keep me logged in',
      ));
  }
}