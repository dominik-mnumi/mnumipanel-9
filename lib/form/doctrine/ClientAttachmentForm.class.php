<?php

/**
 * ClientAttachment form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ClientAttachmentForm extends BaseClientAttachmentForm
{

    public function configure()
    {               
        $this->getWidget('filename')->setAttribute('class', 'full-width');
        
        $this->setWidget('file', new sfWidgetFormInputFile());        
        $this->setValidator('file', new sfValidatorFile());
        
        // validators messages
        $this->setMessages('required');
        
        $this->getWidgetSchema()->setLabels(array(
            'filename' => 'Filename (if empty - original filename)'
        ));
        
        $this->useFields(array('filename', 'file'));
    }
 
    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);      
        $clientId = sfContext::getInstance()->getUser()->getAttribute('id', null, 'client');
        
        $fileObj = $this->getValue('file');
        if($fileObj)
        {                    
           $clientAttachmentPath = sfConfig::get('sf_upload_dir').'/client/'.$clientId.'/';
           $filename = sha1($fileObj->getOriginalName().microtime().rand());
           $extension = $fileObj->getExtension();

           // saves file
           if(!file_exists($clientAttachmentPath))
           {
               @mkdir($clientAttachmentPath, 0777, true);
           }

           $fileObj->save($clientAttachmentPath.$filename.$extension);  
           
           // if filename is empty sets original filename
           if(!$this->getValue('filename'))
           {
               $object->setFilename($fileObj->getOriginalName());
           }
           
           // unlink old file
           if(!$this->isNew())
           {
               unlink($this->getObject()->getFilepath());
           } 
           
           $object->setClientId($clientId);
           $object->setFilepath($clientAttachmentPath.$filename.$extension);            
        }
   
        return $object;
    }

}
