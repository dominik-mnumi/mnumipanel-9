<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Product form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductForm extends BaseProductForm
{
    public function configure()
    {
        //sets fieldset for form
        $fieldsetObj = $this->getOption('fieldsetObj');
        if(!empty($fieldsetObj))
        {
            $this->setFieldset($fieldsetObj);
        }

        $categoryId = $this->getOption('categoryId');

        $this->setWidget('description', new sfWidgetFormTextareaTinyMCE(array(
            'width' => '490',
            'height' => '300',
        )));

        $this->getWidget('name')->setLabel('Product name');
        $this->getWidget('description')->setLabel('Description');

        // gets select category tree choices
        $selectCategoryTree = CategoryTable::getInstance()->getSelectTreeChoices(true);
        $this->setWidget('category_id', new sfWidgetFormChoice(
            array('choices' => array('' => '') + $selectCategoryTree,
                'default' => $categoryId)));
        $this->setValidator('category_id', new sfValidatorChoice(
            array('choices' => array_keys($selectCategoryTree)),
            array('required' => 'Product category is required'))
        );

        // tax value
        $taxOptions = sfConfig::get('app_tax_options');
        $this->setWidget('tax_value', new sfWidgetFormChoice(array(
                    'choices' => array('' => 'Default tax rate from configuration') + $taxOptions,
                    'label' => 'Tax rate',
                )
            )
        );
        $this->setValidator('tax_value', new sfValidatorChoice(
                array('choices' => array_keys($taxOptions), 'required' => false)
        ));

        /*
         * Field "is_static_quantity" checkbox
         */

        $this->getWidget('is_static_quantity')->setLabel('Limit quantity choice to values from list');

        $this->setValidator('is_static_quantity', new sfValidatorBoolean());

        /*
         * Field "simple_calculation" checkbox
         */

        $this->getWidget('simple_calculation')->setLabel('Display the simple calculation');

        $this->setValidator('simple_calculation', new sfValidatorBoolean());

        /*
         * Field "static_quantity_options"
         */

        $this->setWidget('static_quantity_options', new sfWidgetFormInput(
                array(
                    'default' => $this->getObject()->getStaticQuantity(),
                    'label' => 'Choose the quantity',

                )
        ));

        $this->setValidator('static_quantity_options', new sfValidatorTagString(
            array(
                'required' => false,
                'separator' => ',',
            )
        ));

        // photo
        $this->setWidget('photo',
            new sfWidgetFormInputFileEditable(
                array('file_src' => $this->getObject()->getImage(190),
                    'is_image' => true,
                    'edit_mode' => $this->getObject()->getPhoto() ,
                    'delete_label' => 'Delete image')));
        $this->getWidget('photo')->setLabel('Photo');

        $this->setWidget('uploader_available', new sfWidgetFormInputCheckbox(array(), array('id' => 'uploader_available')));
        $this->getWidget('uploader_available')->setLabel('File uploader available in product order page');

        $this->getWidget('slug')->setLabel('Unique name (leave empty for autogenerate)');
        $this->setValidator('slug', new sfValidatorRegex(
            array('pattern' => '/^[\w0-9\-_]+$/', 'required' => false),
            array('invalid' => '"Slug" can contain only letters, numbers, dashes or underscores')));

        $this->getWidget('wizard_open')->setLabel('Open MnumiWizard after entrance to product');
        $this->getWidget('note')->setLabel('Print annotation (visible on report card)');

        // validators
        $this->getValidator('name')->setMessages(array('required' => 'Product name is required'));
        $this->setValidator('photo', new sfValidatorFile(array(
            'required' => false,
            'mime_types' => 'web_images'
        )));
        $this->setValidator('photo_delete', new sfValidatorBoolean());
        $this->getWidget('photo')->setLabel('Photo');

        $this->getWidget('file_required')->setLabel('File is required in product order page');

        // available shops
        $shopChoiceArray = array('__ALL__' => 'Apply for all shops')
            + Cast::getChoiceArr(ShopTable::getInstance()->getShops()->toArray(),
                'host', 'name');
        $this->setWidget('custom_shops', new sfWidgetFormChoice(
            array('multiple' => true,
                'choices' => $shopChoiceArray,
                'expanded' => true,
                'label' => 'Shops')));

        // Calculate as card radio buttons
        $this->setWidget('calculate_as_card', new sfWidgetFormChoice(array(
                'label' => 'Calculation algorithm',
                'expanded' => true,
                'choices'  => array(
                    0 => 'Calculate using card count',
                    1 => 'Calculate using page count'
                ),
            )));
        
        // sets apply for all pricelist
        if($this->getObject()->getApplyForAllShops())
        {
            $this->getWidget('custom_shops')->
                setDefault(array_keys(array('__ALL__' => '') + ProductTable::getShopArray($this->getObject()->getProductShops())));
        }
        else
        {
            $this->getWidget('custom_shops')->
                setDefault(array_keys(ProductTable::getShopArray($this->getObject()->getProductShops())));
        }

        $this->setValidator('custom_shops', new sfValidatorChoice(
            array('choices' => array_keys($shopChoiceArray),
                'required' => false,
                'multiple' => true)));

        // sets post validators
        $this->getValidatorSchema()->setPostValidator(
            new sfValidatorDoctrineUnique(
                array('model' => 'Product',
                    'column' => 'slug'),
                array('invalid' => 'This "Slug" already exists in database')));

        $this->useFields(array('name', 'description', 'photo', 'category_id',
            'active', 'external_link', 'slug', 'uploader_available',
            'file_required', 'wizard_open', 'custom_shops',
            'is_static_quantity', 'simple_calculation', 'static_quantity_options',
            'note', 'calculate_as_card', 'tax_value',
        ));
    }

    protected function doSave($con = null)
    {
        $file = $this->getValue('photo');
        if($file)
        {
            // gets absolute path to dir
            $fileDir = ProductTable::getDefaultImagePath(true);

            // if dir does not exist - create it
            if(!is_dir($fileDir))
            {
                mkdir($fileDir);
                chmod($fileDir, 0777);
            }

            $filename = sha1($file->getOriginalName().microtime().rand());
            $extension = $file->getExtension();
            $file->save($fileDir.$filename.$extension);
        }

        if(
            $this->getValue('is_static_quantity') == false
            &&
            $this->getValue('simple_calculation') == false
        )
        {
            $this->values['static_quantity_options'] = array();
        }

        // if no options, uncheck boolean fields is static quantity and simple calculation
        if($this->values['static_quantity_options'] == "")
        {
            $this->values['is_static_quantity'] = false;
            $this->values['simple_calculation'] = false;
        } else {
            $oldValues = explode(',', $this->values['static_quantity_options']);

            $this->values['static_quantity_options'] = '';

            foreach($oldValues as $value) {
                if(is_numeric($value)) {
                    if ($this->values['static_quantity_options'] != '') {
                        $this->values['static_quantity_options'] .= ',';
                    }
                    $this->values['static_quantity_options'] .= $value;
                }
            }
        }

        //save object
        parent::doSave($con);

        //if product is new
        if($this->isNew())
        {
            //saves first tab manually because of lack it in array
            $generalInformationTab = new ProductField();
            $generalInformationTab->setProduct($this->getObject());
            $generalInformationTab->setFieldset($this->getFieldset());
            $generalInformationTab->save();
        }

        // If uploader is unavailable file cant be required
        if(!$this->getObject()->getUploaderAvailable())
        {
            $this->getObject()->setFileRequired(0);
        }

        return $this->getObject();
    }

    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);
        $object->setPhoto(str_replace(ProductTable::getDefaultImagePath(true), '',
            $object->getPhoto()));

        // shops
        $productFormArr = $this->getValue('custom_shops')
            ? $this->getValue('custom_shops')
            : array();
        $object->setApplyForAllShops(in_array('__ALL__', $productFormArr));

        return $object;
    }

    public function setFieldset($fieldsetObj)
    {
        $this->fieldsetObj = $fieldsetObj;
    }

    public function getFieldset()
    {
        return $this->fieldsetObj;
    }

    public function setProductFieldset($productfieldsetObj)
    {
        $this->productFieldsetObj = $productfieldsetObj;
    }

    public function getProductFieldset()
    {
        return $this->productFieldsetObj;
    }

    public function getFieldsetLabel()
    {
        if(!empty($this->productFieldsetObj))
        {
            if($this->productFieldsetObj->getLabel() != '')
            {
                return $this->productFieldsetObj->getLabel();
            }
        }
        return $this->fieldsetObj->getLabel();
    }
}
