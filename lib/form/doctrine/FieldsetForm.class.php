<?php

/**
 * Fieldset form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FieldsetForm extends BaseFieldsetForm {

  public function configure() {
    unset(
        $this['lft'], $this['rgt'], $this['level'], $this['id'], $this['changeable'], $this['hidden']
    );
    
    $this->setWidget('root_id', new sfWidgetFormInputHidden(array(), array('class' => 'root_id_hidden')));
    $this->getValidator('label')->setMessage('required', 'Field "Label" is required');
    
    $this->setValidator('name', new sfValidatorString(array(
        'max_length' => 45, 
        'required' => true
    ), array('required' => 'Field "Name" is required')));

    $this->setValidator('root_id', new sfValidatorDoctrineChoiceNestedSet(array(
        'required' => true,
        'model' => 'Fieldset',
        'node' => $this->getObject(),
    ), array('required' => 'Root category is required')));

  }
  
  public function doSave($con = null)
  {
      parent::doSave($con);

      if($this->getValue('root_id'))
      {
          $parent = $this->getObject()->getTable()->findOneById($this->getValue('root_id'));
          if($this->isNew())
          { 
              $this->getObject()->getNode()->insertAsLastChildOf($parent);
          }
          else
          {
              $this->getObject()->getNode()->moveAsLastChildOf($parent);
          }
      }
      else
      {
          $categoryTree = $this->getObject()->getTable()->getTree();
          if ($this->isNew())
          {
              $categoryTree->createRoot($this->getObject());
          }
          else
          {
              $this->getObject()->getNode()->makeRoot($this->getObject()->getId());
          }
      }
  }


}
