<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductFixprice form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFixpriceForm extends BaseProductFixpriceForm
{
    public function configure()
    {
        $this->setWidget('pricelist_id', new sfWidgetFormInputHidden());

        $this->getWidget('quantity')->setAttribute('class', 'fixpriceInput');
        $this->getWidget('price')->setAttribute('class', 'fixpriceInput');
        $this->getWidget('cost')->setAttribute('class', 'fixpriceInput');
        
        //labels
        $this->getWidget('quantity')->setLabel('No. of packages');
        $this->getWidget('price')->setLabel('Package price');
        $this->getWidget('price')->setLabel('Cost');
        
        //validators
        $this->setValidator('price', new sfValidatorNumberExtended(array('required' => false)));
        $this->setValidator('cost', new sfValidatorNumberExtended(array('required' => false)));

        $this->validatorSchema->setPostValidator(new sfValidatorCallback(array('callback' => array($this, 'checkValues'))));
        
        $this->useFields(array('quantity', 'price', 'cost'));
    }

    public function checkValues($validator, $values)
    {
        if(!empty($values['quantity']) && empty($values['price']))
        {
            throw new sfValidatorError($validator, sfContext::getInstance()->getI18N()->__('Price for "Quantity" = %nr% is required', array('%nr%'=> $values['quantity'])));
        }
        
        if(empty($values['quantity']) && !empty($values['price']))
        {
            throw new sfValidatorError($validator, sfContext::getInstance()->getI18N()->__('Quantity for "Price" = %nr% is required', array('%nr%' => $values['price'])));
        }
     
        return $values;
    }
    
}
