<?php

/**
 * Payment form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PaymentForm extends BasePaymentForm
{
    public function configure()
    {
        $this->getWidget('name')->setAttribute('class', 'full-width');
        $this->getWidget('label')->setAttribute('class', 'full-width');
        $this->getWidget('cost')->setAttribute('class', 'full-width');
        $this->getWidget('income_pattern')->setAttribute('class', 'full-width');
        $this->getWidget('outcome_pattern')->setAttribute('class', 'full-width');
        $this->getWidget('free_payment_amount')->setAttribute('class', 'full-width');

        $this->setWidget('description', new sfWidgetFormTextareaTinyMCE(array(
                'width' => '490',
                'height' => '300',
            )));

        $pricelistChoiceArray = array('__ALL__' => 'Apply on all pricelists') + PricelistTable::getInstance()->asArray();
        $this->setWidget('custom_pricelists', new sfWidgetFormChoice(
                array('multiple' => true,
                    'choices' => $pricelistChoiceArray,
                    'expanded' => true)
        ));

        //sets apply on all pricelist
        if($this->getObject()->getApplyOnAllPricelist())
        {
            $this->getWidget('custom_pricelists')->
                setDefault(array_keys(array('__ALL__' => '') + PaymentTable::getPricelistArray($this->getObject()->getPaymentPricelists())));
        }
        else
        {
            $this->getWidget('custom_pricelists')->
                setDefault(array_keys(PaymentTable::getPricelistArray($this->getObject()->getPaymentPricelists())));
        }
        
        $carrierChoiceArray = array('__ALL__' => 'Apply on all carriers') + CarrierTable::getInstance()->asArray();
        $this->setWidget('custom_carriers', new sfWidgetFormChoice(
                array('multiple' => true,
                    'choices' => $carrierChoiceArray,
                    'expanded' => true)
        ));
        
        //sets apply on all carriers
        if($this->getObject()->getApplyOnAllCarriers())
        {
            $this->getWidget('custom_carriers')->
                setDefault(array_keys(array('__ALL__' => '') + PaymentTable::getCarrierArray($this->getObject()->getCarrierPayments())));
        }
        else
        {
            $this->getWidget('custom_carriers')->
                setDefault(array_keys(PaymentTable::getCarrierArray($this->getObject()->getCarrierPayments())));
        }
        
        //validators
        $this->setValidator('custom_pricelists', new sfValidatorChoice(
            array('choices' => array_keys($pricelistChoiceArray),
                'required' => false,
                'multiple' => true)));
        
        $this->setValidator('custom_carriers', new sfValidatorChoice(
            array('choices' => array_keys($carrierChoiceArray),
                'required' => false,
                'multiple' => true)));
        
        $this->setValidator('cost', new sfValidatorNumberExtended(
            array('required' => false),
            array('invalid' => 'Field "Cost" must be a number')));
        
        $this->setValidator('free_payment_amount', new sfValidatorNumberExtended(
            array('required' => false),
            array('invalid' => 'Field "Free payment amount" must be a number')));
            
        $this->getValidator('name')->setMessage('required', 'Field "Name" is required');
        $this->getValidator('label')->setMessage('required', 'Field "Label" is required');

        //labels
        $this->getWidgetSchema()->setLabels(array(
            'name' => 'Name',
            'label' => 'Label',
            'description' => 'Description',
            'active' => 'Active',
            'custom_pricelists' => 'Pricelist',
            'required_credit_limit' => 'Only for users who have credit limit and do not have overdue payments',
            'income_pattern' => 'Income pattern (e.g. IN yy/{number})',
            'outcome_pattern' => 'Outcome pattern (e.g. OUT yy/{number})',
            'deposit_enabled' => 'Deposit enabled'
        ));

        $this->useFields(array('name', 'label', 'description', 'active', 
            'custom_pricelists', 'for_office', 'custom_carriers', 'required_credit_limit', 
            'cost', 'free_payment_amount',
            'income_pattern', 'outcome_pattern', 'deposit_enabled'));

        if(!$this->isNew() && !$this->getObject()->getChangeable())
        {
            $this->getWidget('name')->setAttribute('readonly', 'readonly');
        }
    }

    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);
        
        $paymentArrayForm = ($this->getValue('custom_pricelists')) ? $this->getValue('custom_pricelists') : array();
        $object->setApplyOnAllPricelist(in_array('__ALL__', $paymentArrayForm));
        
        $paymentCarrierArrayForm = ($this->getValue('custom_carriers')) ? $this->getValue('custom_carriers') : array();
        $object->setApplyOnAllCarriers(in_array('__ALL__', $paymentCarrierArrayForm));

        return $object;
    }

     protected function doSave($con = null)
     {
        parent::doSave($con);

        
        
        
        return $this->getObject();
    }
}
