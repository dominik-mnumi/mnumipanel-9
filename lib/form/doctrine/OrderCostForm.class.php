<?php

/**
 * OrderCost form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderCostForm extends BaseOrderCostForm
{
	public function configure()
	{
		$this->disableCSRFProtection();

		$this->useFields(array('quantity', 'cost', 'order_id', 'field_item_id', 'description'));
	}
}
