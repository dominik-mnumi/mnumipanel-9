<?php

/**
 * Notification form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NotificationForm extends BaseNotificationForm
{

    public function configure()
    {
        $this->setWidget('content', new sfWidgetFormTextareaTinyMCE(array(
                    'width' => '490',
                    'height' => '300',
                    'config' => 'forced_root_block: false,
                        force_p_newlines: false,
                        force_br_newlines: true,'
                )));
        
        //changed to sfWidgetFormChoice because of i18N support
        $this->setWidget('notification_template_id', new sfWidgetFormChoice(array(
            'choices' => NotificationTemplateTable::getInstance()->getAllNotificationTemplateArray(),
        )));

        $this->getWidget('notification_type_id')->setOption('expanded', true);

        $this->getWidget('title')->setAttribute('class', 'full-width');

        /**
         * @see PAN-1204
         */
        $this->getWidget('bcc')->setLabel('BCC')->setAttribute('class', 'full-width');

        $this->getWidgetSchema()->setLabels(array('notification_template_id' => 'Template',
            'notification_type_id' => 'Type',
            'title' => 'Title',
            'content' => 'Content'));
        
        $this->getValidator('title')->setMessage('required', 'Field "Title" is required');  
        $this->getValidator('content')->setMessage('required', 'Field "Content" is required');

        $this->mergePostValidator(new sfValidatorCallback(array('callback' => array($this, 'validateTwigSyntax'))));
    }

    public function validateTwigSyntax($validator, $values, $arguments)
    {
        try
        {
            $loader = new Twig_Loader_String();
            $twig = new Twig_Environment($loader);
            $twig->addExtension(new Twig_Extensions_Extension_I18n());
            $twig->render($values['content'], array());
        }
        catch(Twig_Error_Syntax $e)
        {
            $message = 'Wrong syntax';
            throw new sfValidatorErrorSchema($validator, array(
                'content' => new sfValidatorError($validator, $message),
            ));
        }

        return $values;
    }

}
