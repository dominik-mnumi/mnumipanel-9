<?php

/**
 * Oauth2RefreshToken form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class Oauth2RefreshTokenForm extends BaseOauth2RefreshTokenForm
{
  public function configure()
  {
  }
}
