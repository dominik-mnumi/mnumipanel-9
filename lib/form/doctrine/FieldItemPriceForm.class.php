<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FieldItemPrice form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FieldItemPriceForm extends BaseFieldItemPriceForm
{
  public function configure()
  {
      $this->useFields(array('minimal_price', 'maximal_price', 'pricelist_id'));
      $this->setWidget('minimal_price', new sfWidgetFormInputText(array(), array('size' => 4)));
      $this->setWidget('maximal_price', new sfWidgetFormInputText(array(), array('size' => 4)));
      $this->setWidget('pricelist_id', new sfWidgetFormInputHidden());
      
      $this->setValidator('minimal_price', new sfValidatorNumberExtended(array('required' => false)));
      $this->setValidator('maximal_price', new sfValidatorNumberExtended(array('required' => false)));
      
      $this->validatorSchema->setPostValidator(new sfValidatorCallback(array('callback' => array($this, 'checkQuantityUnique'))));
      
      $this->setDefault('pricelist_id', PricelistTable::getInstance()->getDefaultPricelist()->getId());
      
      $newFiedItemPriceRangeForms = new FieldItemPriceRangeCollectionForm(null, array(
            'field_item_price' => $this->getObject(),
            'size'                  => 20
      ));

      $this->embedForm('newFieldPriceRangeForms', $newFiedItemPriceRangeForms);
      $this->embedRelation('FieldItemPriceRanges');
  }
  
  public function saveEmbeddedForms($con = null, $forms = null)
  {
      parent::saveEmbeddedForms($con, $forms);
      $this->cleanEmptyForms();  
  } 
  
  /**
   * cleanEmptyForms - delete unnecessary forms
   */
  public function cleanEmptyForms()
  {       
      $deleted = Doctrine_Query::create()
         ->delete()
         ->from('FieldItemPriceRange')
         ->where('quantity is null')
         ->andWhere('price_simplex is null')
         ->andWhere('price_duplex is null')
         ->andWhere('price_page is null')
         ->andWhere('price_copy is null')
         ->andWhere('price_linear_metre is null')
         ->andWhere('price_square_metre is null')
         ->execute();
  }
  
  /**
  * checkQuantityUnique - checks if there arent any duplicates of field_item_price_range quantity
  */
  public function checkQuantityUnique($validator, $values)
  {
      $fieldRages = array_merge($values['newFieldPriceRangeForms'], $values['FieldItemPriceRanges']);
      $quantities = array();
      
      foreach($fieldRages as $fr)
      {
          if($fr['quantity'] > 0)
          {
            $qty = (string)$fr['quantity'];
            if(array_key_exists("$qty", $quantities))
            {
                throw new sfValidatorError($validator, sfContext::getInstance()->getI18N()->__('Quantity in pricelist have to be unique.'));
            }
              
            $quantities["$qty"] = 1;
          }
      }
   
      return $values;
  }
}
