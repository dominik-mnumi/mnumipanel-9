<?php

/**
 * CustomPrice form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CustomPriceForm extends BaseCustomPriceForm
{
  public function configure()
  {
      $formNumber = $this->getOption('formNumber');
      $editable = $this->getOption('editable');
      
      // check if user can edit price value (price block)
      if(!$editable)
      {
          $this->setWidget('price', new sfWidgetFormInputHidden());
      }
      
      $this->getWidget('price')->setAttribute('class', 'custom_price');
      $this->setWidget('order_id', new sfWidgetFormInputHidden());
      $this->setWidget('fieldset_id', new sfWidgetFormInputHidden());
      $this->setWidget('field_item_id', new sfWidgetFormInputHidden());
      
      $this->widgetSchema->setNameFormat('custom_price['.$formNumber.'][%s]');
  }
  
  public function save($conn = null)
  {
      $orderId = $this->getValue('order_id');
      $fieldsetId = $this->getValue('fieldset_id');
      $fieldItemId = $this->getValue('field_item_id');
      $price = $this->getValue('price');
      
      $customPrice = CustomPriceTable::getInstance()->getCustomPriceForOrder($orderId, $fieldsetId, $fieldItemId);
      
      if($customPrice)
      {
          $customPrice->setPrice($price);
      }
      else
      {
          $customPrice = new CustomPrice();
          $customPrice->setOrderId($orderId);
          $customPrice->setFieldsetId($fieldsetId);
          $customPrice->setFieldItemId($fieldItemId);
          $customPrice->setPrice($price);
      }
      
      $customPrice->save();
 
      return $customPrice;
  }
}
