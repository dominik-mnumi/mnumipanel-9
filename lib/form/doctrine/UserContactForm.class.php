<?php

/**
 * UserContact form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class UserContactForm extends BaseUserContactForm
{
    public function configure()
    {      
        $this->getWidget('value')->setAttribute('class', 'full-width');
        $this->getWidget('send_notification')->setAttribute('class', 'mini-switch');

        //labels
        $this->getWidgetSchema()->setLabels(array(
            'username' => 'Contact type',
            'value' => 'Enter value',
            'send_notification' => 'Notification'
        ));
    }

}
