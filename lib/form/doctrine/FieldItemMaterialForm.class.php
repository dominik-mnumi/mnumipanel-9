<?php

/**
 * FieldItemMaterial form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FieldItemMaterialForm extends BaseFieldItemMaterialForm
{

    public function configure()
    {
        $this->getWidget('printsize_id')->setLabel('Print size');

        $this->getValidator('printsize_id')
                ->setMessage('required', 'Field "Print size" is required');

        $this->useFields(array('printsize_id'));
    }

}
