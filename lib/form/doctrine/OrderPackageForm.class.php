<?php

/**
 * OrderPackage form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderPackageForm extends BaseOrderPackageForm
{
    public function configure()
    {
        $postcode = $this->getObject()->getDeliveryPostcode();
        $city = $this->getObject()->getDeliveryCity();
        $postcodeAndCity = $postcode.' '.$city;
        
        $this->setWidget('postcodeAndCity', new sfWidgetFormInputText(
                array('default' => $postcodeAndCity), 
                array('class' => 'full-width')));
        
        $choiceArray = $this->getObject()->getClient()->getClientAddressChoiceArray() + array('' => 'other');

        $this->setWidget('client_address_id', new sfWidgetFormChoice(
                array('choices' => $choiceArray), 
                array('class' => 'full-width'))); 
        
        $this->setWidget('delivery_country', new sfWidgetFormI18nChoiceCountry(
                array('culture' => sfContext::getInstance()->getUser()->getCulture(),
                    'label' => 'Country'),
                array('class' => 'full-width')));   
        
        // prepares default for carrier
        $defaultCarrierId = $this->getWidget('carrier_id')->getDefault();
        if(!$defaultCarrierId)
        {
            $defaultCarrierId = sfConfig::get('app_default_delivery');
            $this->getWidget('carrier_id')->setDefault($defaultCarrierId);
        }
        
        // prepares default for payment
        $defaultPaymentId = $this->getWidget('payment_id')->getDefault();
        if(!$defaultPaymentId)
        {
            $defaultPaymentId = sfConfig::get('app_default_payment');
            $this->getWidget('payment_id')->setDefault($defaultPaymentId);
        }

        // attributes
        $this->getWidget('payment_id')->setAttribute('class', 'full-width payment-select');
        $this->getWidget('carrier_id')->setAttribute('class', 'full-width delivery-select');
        $this->getWidget('description')->setAttribute('class', 'full-width description-textarea'); 
        $this->getWidget('delivery_street')->setAttribute('class', 'full-width');
        
        $this->getWidget('carrier_id')->setOption('query', $this->getObject()
                ->getClient()
                ->getAvailableCarriersQuery());
        $this->getWidget('payment_id')->setOption('query', $this->getObject()
                ->getClient()
                ->getAvailablePaymentsQuery($defaultCarrierId));

        $this->setWidget('delivery_at', new sfWidgetFormInput(
                array('label' => 'Delivery date'),
                array('class' => 'modal-datepicker')));

        // filter min is set to '2014-01-01' for prevent strange dates
        // for example '0000-00-00'
        $this->setValidator('delivery_at', new sfValidatorDate(
                array('min' => '2014-01-01',
                    'required' => false)));

        $fullWidthWidgets = array(
            'invoice_name', 'invoice_street', 'invoice_city', 'invoice_postcode', 'invoice_tax_id'
        );

        foreach($fullWidthWidgets as $widget)
        {
            $this->getWidget($widget)->setAttribute('class', 'full-width');
        }

        $this->setWidget('invoice_country', new sfWidgetFormI18nChoiceCountry(
                array('culture' => sfContext::getInstance()->getUser()->getCulture(),
                    'label' => 'Country'),
                array('class' => 'full-width')));


        // validators
        $this->setValidator('postcodeAndCity', new sfValidatorPostcodeAndCity(
                array(
                    'max_length' => 150,
                    'required' => true)));
      
        $this->setValidator('client_address_id', new sfValidatorChoice(
                array('choices' => array_keys($choiceArray),
                    'required' => false)
                ));
        $this->setValidator('delivery_country', new sfValidatorI18nChoiceCountry());
        
        $this->getValidator('delivery_street')->setOption('required', true); 
        $this->getValidator('carrier_id')->setOption('query', $this->getObject()
                ->getClient()
                ->getAvailableCarriersQuery());
        $this->getValidator('payment_id')->setOption('query', $this->getObject()
                ->getClient()
                ->getAvailablePaymentsQuery());
           
        // labels
        $this->getWidgetSchema()->setLabels(array(
            'client_address_id' => 'Package delivery address',
            'payment_id' => 'Payment form',
            'carrier_id' => 'Carrier',
            'delivery_street' => 'Address (street/house)',
            'postcodeAndCity' => 'Postcode and city',
            'description' => 'Additional information',
            'delivery_country' => 'Country',
            'invoice_street' => 'Invoice street/no.',
            'invoice_country' => 'Invoice country',
        ));
    
    }

}
