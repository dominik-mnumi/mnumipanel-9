<?php

/**
 * Shop form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ShopForm extends BaseShopForm
{

    public function configure()
    {
        // name
        $this->setWidget('name', new sfWidgetFormInput(
                array('label' => 'Shop key'),
                array('class' => 'full-width')));

        // description
        $this->getWidget('description')->setAttribute('class', 'full-width');
        $this->getWidget('description')->setLabel('Description');
        $this->getValidator('description')->setOption('min_length', 5);
        $this->getValidator('description')->setMessage('required', 'Field "Description" is required');

        // valid_date
        $this->setWidget('valid_date', new sfWidgetFormInput(
                array('label' => 'Expiry date'),
                array('class' => 'datepicker')));

        $this->setValidator('valid_date', new sfValidatorRegex(
                array('pattern' => '|^[0-9]{4}[-][0-9]{2}[-][0-9]{2}$|',
                    'required' => false)));

        // sets date in right format
        if($this->getObject()->getValidDate())
        {
            $this->getWidget('valid_date')->setAttribute('value', date("Y-m-d", strtotime($this->getObject()->getValidDate())));
        }

        // host
        $this->getWidget('host')->setAttribute('class', 'full-width');
        $this->getWidget('host')->setLabel('Host');
        $this->setValidator('host', new sfValidatorHost(array('max_length' => 255)));
        $this->getValidator('host')->setMessage('required', 'Field "Host" is required');

        // type
        $this->setWidget('type', new sfWidgetFormChoice(
                array('choices' => array_combine(ShopTable::$typeArr, ShopTable::$typeArr),
                    'label' => 'Type',
                    'default' => ShopTable::$typeOther),
                array('class' => 'full-width')));
        $this->setValidator('type',
                new sfValidatorChoice(array(
                    'required' => true, 
                    'choices' => ShopTable::$typeArr)));
        
        $this->useFields(array('name', 'description', 'valid_date', 'host', 'type'));

        $this->fieldsToCut[] = 'name';
    }

    public function bind(array $values = array(), array $files = array())
    {
        $values = array_diff_key($values, array_flip($this->fieldsToCut));

        parent::bind($values, $files);
    }

    public function updateObject($values = null)
    {
        $name = $this->getObject()->getName();

        $object = parent::updateObject($values);

        $values = $this->getValues();

        if($this->isNew())
        {
            $object->setName(ShopTable::getInstance()->generateName());
        }
        else
        {
            $object->setName($name);
        }

        return $object;
    }
}
