<?php

/**
 * LoyaltyPoint form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class LoyaltyPointForm extends BaseLoyaltyPointForm
{
  public function configure()
  {
      $this->setWidget('user_id', new sfWidgetFormInputHidden());
      $this->setWidget('status', new sfWidgetFormInputHidden());
      
      $userId = $this->getOption('user_id');
      
      $this->setDefault('user_id', $userId);
      $this->setDefault('status', 'accepted');
      
      $this->setValidator('points', new sfValidatorInteger(array('required' => true), 
            array('required' => 'Please specify number of points', 'invalid' => 'Points must be a valid number')));
      
      $this->setValidator('description', new sfValidatorString(array('required' => true), 
            array('required' => 'Please specify description')));
      
      $this->useFields(array('status', 'points', 'description', 'user_id'));
      $this->widgetSchema->setNameFormat(get_class($this).'[%s]');  
  }
}
