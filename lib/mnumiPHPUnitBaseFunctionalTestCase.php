<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of mnumiPHPUnitBaseFunctionalTestCase
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
abstract class mnumiPHPUnitBaseFunctionalTestCase extends sfBasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    protected $requireLogin = false;
    protected $browser = false;
    
    /**
     * Dev hook for custom "setUp" stuff
     */
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common');
    }
    
    protected function getBrowser()
    {
        if($this->browser)
        {
            return $this->browser;
        }
        $this->browser = new sfTestFunctional(new sfBrowser());
        if($this->requireLogin)
        {
            $this->requireLogin();
        }
        return $this->browser;
        
    }
    
    private function requireLogin()
    {
        if ($this->requireLogin)
        {
            print 'Login as admin/admin...';
        
            $this->browser = $this->browser->get('/login')->
                with('response')->
                begin()->
                isStatusCode(401)->
                getResponseDom()->
                end()->
                click('Login', array('signin' => array(
                        'username' => 'admin',
                        'password' => 'admin',
                    )))->
                //
                followRedirect()->
                with('response')->
                begin()->
                  isStatusCode(302)->
                end()->
                //
                followRedirect()->
                //
                with('response')->
                begin()->
                  isStatusCode(302)->
                end();
             print "\nLogin: Done.";
        }
    }        
}

